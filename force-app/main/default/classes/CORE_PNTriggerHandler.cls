public class CORE_PNTriggerHandler extends CORE_TriggerHandler{
    
    public override void beforeInsert() {
        String key;
        for(CORE_Phone_Number__c pn : (List<CORE_Phone_Number__c>)Trigger.new){
            if(pn.CORE_PN_Type__c == 'CORE_PKL_Phone'){
                key = String.isBlank(String.valueOf(pn.CORE_PN_Phone__c))?'':String.valueOf(pn.CORE_PN_Phone__c);
            }else {
                 key = String.isBlank(String.valueOf(pn.CORE_PN_Pool_SID__c))?'':String.valueOf(pn.CORE_PN_Pool_SID__c);
            }
            String bu = String.isBlank(pn.CORE_PN_Business_Unit__c)?'':String.valueOf(pn.CORE_PN_Business_Unit__c);
                    
            String user = String.isBlank(pn.CORE_PN_User__c)?'':String.valueOf(pn.CORE_PN_User__c);
                    
            pn.CORE_PN_Unique_Key__c = key + bu + user;
        }   
    }
    
   
    
}