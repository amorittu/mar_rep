@isTest
private class CORE_LC_MassOppAcademicYearConfirm_TEST {

    private static final String VIEW_NAME = 'MyOpportunities';
    private static final String EXTERNALID = '1234';
    private static final String ACADEMIC_YEAR = 'CORE_PKL_2023-2024';

    @testSetup 
    static void createData(){
        Id profileId = [SELECT Id FROM Profile WHERE Name = 'CORE_API_Profile' LIMIT 1].Id;

        User usr = new User(
            Username = 'usertest@masstest.com',
            LastName = 'TEST_USER',
            Email = 'testemail@yopmail.com',
            Alias = 'T', 
            CommunityNickname = 'T',
            TimeZoneSidKey = 'Europe/Paris',
            LocaleSidKey = 'fr_FR_EURO',
            EmailEncodingKey = 'ISO-8859-1', 
            ProfileId = profileId,
            LanguageLocaleKey = 'fr'      
        );
        database.insert(usr, true);

        CORE_CatalogHierarchyModel catalogHierarchy = new CORE_DataFaker_CatalogHierarchy(EXTERNALID).catalogHierarchy;
        Account account = CORE_DataFaker_Account.getStudentAccount(EXTERNALID);
        //Opportunity opp3 = CORE_DataFaker_Opportunity.getOpportunity(catalogHierarchy.division.Id, catalogHierarchy.businessUnit.Id, account.Id, CORE_AOL_Constants.PKL_OPP_STAGENAME_PROSPECT,CORE_AOL_Constants.PKL_OPP_SUBSTATUS_PROSPECTAPPSTARTED);
        Opportunity opp1 = new Opportunity(
            Name ='test1',
            StageName = CORE_AOL_Constants.PKL_OPP_STAGENAME_PROSPECT,
            CORE_OPP_Sub_Status__c = CORE_AOL_Constants.PKL_OPP_SUBSTATUS_PROSPECTAPPSTARTED,
            CORE_OPP_Division__c = catalogHierarchy.division.Id,
            CORE_OPP_Business_Unit__c = catalogHierarchy.businessUnit.Id,
            CORE_OPP_Study_Area__c = catalogHierarchy.studyArea.Id,
            CORE_OPP_Curriculum__c = catalogHierarchy.curriculum.Id,
            CORE_OPP_Level__c = catalogHierarchy.level.Id,
            CORE_OPP_Intake__c = catalogHierarchy.intake.Id,
            CORE_OPP_Academic_Year__c = ACADEMIC_YEAR,
            AccountId = account.Id,
            CloseDate = Date.Today().addDays(2),
            OwnerId = UserInfo.getUserId(),
            CORE_OPP_Main_Opportunity__c = true,
            CORE_Main_Product_Interest__c = catalogHierarchy.product.Id
        );

        Opportunity opp2 = new Opportunity(
            Name ='test2',
            StageName = CORE_AOL_Constants.PKL_OPP_STAGENAME_PROSPECT,
            CORE_OPP_Sub_Status__c = CORE_AOL_Constants.PKL_OPP_SUBSTATUS_PROSPECTAPPSTARTED,
            CORE_OPP_Division__c = catalogHierarchy.division.Id,
            CORE_OPP_Business_Unit__c = catalogHierarchy.businessUnit.Id,
            CORE_OPP_Study_Area__c = catalogHierarchy.studyArea.Id,
            CORE_OPP_Curriculum__c = catalogHierarchy.curriculum.Id,
            CORE_OPP_Level__c = catalogHierarchy.level.Id,
            CORE_OPP_Intake__c = catalogHierarchy.intake.Id,
            CORE_OPP_Academic_Year__c = ACADEMIC_YEAR,
            AccountId = account.Id,
            CloseDate = Date.Today().addDays(2),
            OwnerId = UserInfo.getUserId()
        );
    
        database.insert(new List<Opportunity> { opp1, opp2  }, true);
        System.debug('inserting done opp product '+ opp1.CORE_Main_Product_Interest__c);

        Id pricebookId = CORE_DataFaker_PriceBook.getStandardPricebookId();
        PricebookEntry pbe1 = CORE_DataFaker_PriceBook.getPriceBookEntry(catalogHierarchy.product.Id, pricebookId, 100.0);

        CORE_PriceBook_BU__c pricebookBu = CORE_DataFaker_PriceBook.getPriceBookBuWithoutInsert(pricebookId, catalogHierarchy.businessUnit.Id, EXTERNALID);
        pricebookBu.CORE_Academic_Year__c = ACADEMIC_YEAR;

        database.insert(pricebookBu, true);

    }

    @isTest
    static void test_NoSetting(){

        User usr = [SELECT Id FROM User WHERE Username = 'usertest@masstest.com' LIMIT 1];

        System.runAs(usr){
            test.startTest();

            try{
                CORE_LC_MassOppAcademicYearConfirmation.getOrgSetting();
            }
            catch(Exception e){
                system.debug('@test_NoSetting > e: ' + e);
                system.assertEquals(Label.CORE_MAYC_MSG_NO_SETTINGS, e.getMessage());
            }

            test.stopTest();
        }
    }

    @isTest
    static void test_Sync(){
        Core_MassOppConfirmAcademicYear__c orgSetting = new Core_MassOppConfirmAcademicYear__c(
            CORE_BatchSize__c = 20, CORE_ListViewNameFilter__c = VIEW_NAME
        );
        database.insert(orgSetting, true);

        User usr = [SELECT Id FROM User WHERE Username = 'usertest@masstest.com' LIMIT 1];

        System.runAs(usr){

            StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
            mock.setStaticResource('CORE_MassOppClosing_VIEW_RESP');
            mock.setStatusCode(200);
            
            // Set the mock callout mode
            Test.setMock(HttpCalloutMock.class, mock);

            test.startTest();
            CORE_LC_MassOppAcademicYearConfirmation.SettingWrapper setting = CORE_LC_MassOppAcademicYearConfirmation.getOrgSetting();
            system.assertEquals(setting.listViewFilter, VIEW_NAME);

            List<CORE_LC_MassOppAcademicYearConfirmation.ListViewWrapper> listViews = CORE_LC_MassOppAcademicYearConfirmation.getListViewsInfo();
            system.assertEquals(listViews[0].viewName, VIEW_NAME);

            Map<String, String> theObj = new Map<String, String>();
            theObj.put('value',listViews[0].viewId);
            theObj.put('query',listViews[0].viewQuery);
            CORE_LC_MassOppAcademicYearConfirmation.CurrentViewWrapper view = new CORE_LC_MassOppAcademicYearConfirmation.CurrentViewWrapper();
            view.viewQuery = listViews[0].viewQuery;
            view.viewId = listViews[0].viewId;
            view.viewRecordsSize = 20;
            List<Opportunity> oppsList = database.query(view.viewQuery);
            //system.assertEquals(view.viewRecordsSize, oppsList.size());

            List<CORE_LC_MassOppAcademicYearConfirmation.BUWrapper> buWrapList = CORE_LC_MassOppAcademicYearConfirmation.getBUs(view);

            List<CORE_LC_MassOppAcademicYearConfirmation.PriceBookWrapper> pbWrapList = CORE_LC_MassOppAcademicYearConfirmation.getPriceBooks(view, buWrapList[0].buId, ACADEMIC_YEAR);

            String result = CORE_LC_MassOppAcademicYearConfirmation.runChangeAcademicYear(view, buWrapList[0].divId, buWrapList[0].buId, ACADEMIC_YEAR, pbWrapList[0].pbId);
            //system.assertEquals(null, result);
            test.stopTest();
            /*for(Opportunity opp : [SELECT Id, StageName, CORE_Change_Status__c FROM Opportunity WHERE Id IN :oppsList]){
                system.debug('@test_Sync > opp: ' + opp);
                system.assertEquals('Closed Won', opp.StageName);
            }*/

            
        }
    }

    @isTest
    static void test_Async(){
        Core_MassOppConfirmAcademicYear__c orgSetting = new Core_MassOppConfirmAcademicYear__c(
            CORE_BatchSize__c = 1, CORE_ListViewNameFilter__c = VIEW_NAME
        );
        database.insert(orgSetting, true);

        User usr = [SELECT Id FROM User WHERE Username = 'usertest@masstest.com' LIMIT 1];

        System.runAs(usr){

            StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
            mock.setStaticResource('CORE_MassOppClosing_VIEW_RESP');
            mock.setStatusCode(200);
            
            // Set the mock callout mode
            Test.setMock(HttpCalloutMock.class, mock);

            test.startTest();
            CORE_LC_MassOppAcademicYearConfirmation.SettingWrapper setting = CORE_LC_MassOppAcademicYearConfirmation.getOrgSetting();
            system.assertEquals(setting.listViewFilter, VIEW_NAME);

            List<CORE_LC_MassOppAcademicYearConfirmation.ListViewWrapper> listViews = CORE_LC_MassOppAcademicYearConfirmation.getListViewsInfo();
            system.assertEquals(listViews[0].viewName, VIEW_NAME);

            Map<String, String> theObj = new Map<String, String>();
            theObj.put('value',listViews[0].viewId);
            theObj.put('query',listViews[0].viewQuery);
            CORE_LC_MassOppAcademicYearConfirmation.CurrentViewWrapper view = new CORE_LC_MassOppAcademicYearConfirmation.CurrentViewWrapper();
            view.viewQuery = listViews[0].viewQuery;
            view.viewId = listViews[0].viewId;
            view.viewRecordsSize = 20;
            List<Opportunity> oppsList = database.query(view.viewQuery);

            //system.assertEquals(view.viewRecordsSize, oppsList.size());

            List<CORE_LC_MassOppAcademicYearConfirmation.BUWrapper> buWrapList = CORE_LC_MassOppAcademicYearConfirmation.getBUs(view);

            List<CORE_LC_MassOppAcademicYearConfirmation.PriceBookWrapper> pbWrapList = CORE_LC_MassOppAcademicYearConfirmation.getPriceBooks(view, buWrapList[0].buId, ACADEMIC_YEAR);

            String result = CORE_LC_MassOppAcademicYearConfirmation.runChangeAcademicYear(view, buWrapList[0].divId, buWrapList[0].buId, ACADEMIC_YEAR, pbWrapList[0].pbId);

            //String result = CORE_LC_MassOppAcademicYearConfirmation.runCloseOpps(view, 'Closed Won');
            //system.assertEquals('ASYNC', result);

            test.stopTest();
        }
    }

    @isTest
    static void test_Async_Error(){
        Core_MassOppConfirmAcademicYear__c orgSetting = new Core_MassOppConfirmAcademicYear__c(
            CORE_BatchSize__c = 10, CORE_ListViewNameFilter__c = VIEW_NAME
        );
        database.insert(orgSetting, true);

        User usr = [SELECT Id FROM User WHERE Username = 'usertest@masstest.com' LIMIT 1];

        System.runAs(usr){

            test.startTest();
            try{
                System.enqueueJob(new CORE_QU_MassOppAcademicYearConfirmation(null, null, null, null, null, null));
            }
            catch(Exception e){
                system.assertEquals('AuraHandledException', e.getTypeName());
            }

            test.stopTest();
        }
    }
}