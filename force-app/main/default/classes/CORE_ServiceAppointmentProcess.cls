public without sharing class CORE_ServiceAppointmentProcess {
    public static void execute(Set<Id> opportuniesIds){
        System.debug('CORE_ServiceAppointmentProcess : Start');
        List<Opportunity> opportunities = getOpportunities(opportuniesIds);

        Map<String, Opportunity> opportunitiesByExternalIds = new Map<String, Opportunity>();

        for(Opportunity opportunity : opportunities){
            if(!String.isEmpty(opportunity.CORE_Lead_Source_External_Id__c)){
                opportunitiesByExternalIds.put(opportunity.CORE_Lead_Source_External_Id__c, opportunity);
            }
        }

        Set<String> oppsExternalIds = opportunitiesByExternalIds.keySet();
        List<ServiceAppointment> serviceAppointments = getRelatedServiceAppointments(oppsExternalIds);
        
        Set<Id> serviceAppointmentsIds = (new Map<Id, ServiceAppointment>(serviceAppointments)).keySet();
        
        if(!serviceAppointments.isEmpty()){
            duplicationProcess(serviceAppointments, opportunitiesByExternalIds);
        }

        System.debug('CORE_ServiceAppointmentProcess : Delete');
    }

    private static void duplicationProcess(List<ServiceAppointment> serviceAppointments, Map<String, Opportunity> opportunitiesByExternalIds){
        List<ServiceAppointment> serviceAppointmentsToUpdate = new List<ServiceAppointment>();
        Set<String> oppsExternalIds = opportunitiesByExternalIds.keySet();

        Map<String,List<AssignedResource>> assignedResourcesByOppsExternalIds = getRelatedAssignedResources(oppsExternalIds);

        for(ServiceAppointment serviceAppointment : serviceAppointments){
            
            ServiceAppointment sa = serviceAppointment.clone(false,true,true,true);
            Opportunity relatedOpportunity = opportunitiesByExternalIds.get(serviceAppointment.CORE_Lead_form_acquisition_unique_key__c);
            sa.ParentRecordId = relatedOpportunity.Id;

            serviceAppointmentsToUpdate.add(sa);

        }

        Delete serviceAppointments;
        Insert serviceAppointmentsToUpdate;

        List<AssignedResource> assignedResourcesToDuplicate = new List<AssignedResource>();
        for(ServiceAppointment sa : serviceAppointmentsToUpdate){
            Id serviceAppointmentId = sa.Id;
            List<AssignedResource> assignedResources = assignedResourcesByOppsExternalIds.get(sa.CORE_Lead_form_acquisition_unique_key__c);

            for(AssignedResource assignedResource : assignedResources){
                AssignedResource ar = assignedResource.clone(false,true,true,true);
                ar.ServiceAppointmentId = serviceAppointmentId;
                assignedResourcesToDuplicate.add(ar);
            }

        }

        if(!assignedResourcesToDuplicate.isEmpty()){
            Insert assignedResourcesToDuplicate;
        }
    }

    private static List<Opportunity> getOpportunities(Set<Id> opportuniesIds){
        List<Opportunity> results = new List<Opportunity>();

        if(!opportuniesIds.isEmpty()){
            results = [Select Id, AccountId, CORE_Lead_Source_External_Id__c from Opportunity where Id in :opportuniesIds];
        }
        return results;
    }

    private static List<ServiceAppointment> getRelatedServiceAppointments(Set<String> opportunitiesExternalIds){
        List<ServiceAppointment> results = new List<ServiceAppointment>();

        if(!opportunitiesExternalIds.isEmpty()){
            results = [SELECT Id, OwnerId, AppointmentNumber, CreatedDate, CreatedById, LastModifiedDate, LastModifiedById, SystemModstamp, 
                LastViewedDate, LastReferencedDate, ParentRecordId, ParentRecordType, AccountId, WorkTypeId, ContactId, 
                Street, City, State, PostalCode, Country, Latitude, Longitude, GeocodeAccuracy, Address, 
                Description, EarliestStartTime, DueDate, Duration, ArrivalWindowStartTime, ArrivalWindowEndTime, 
                Status, SchedStartTime, SchedEndTime, ActualStartTime, ActualEndTime, ActualDuration, DurationType, 
                DurationInMinutes, ServiceTerritoryId, Subject, ParentRecordStatusCategory, StatusCategory, ServiceNote, 
                AppointmentType, Email, Phone, CancellationReason, AdditionalInformation, Comments, IsAnonymousBooking, 
                CORE_Salutation__c, CORE_Firstname__c, CORE_Lastname__c, CORE_Division__c, CORE_Business_Unit__c, CORE_Study_Area__c, 
                CORE_Curriculum__c, CORE_Level__c, CORE_Intake__c, CORE_Lead_form_acquisition_unique_key__c
                FROM ServiceAppointment 
                WHERE CORE_Lead_form_acquisition_unique_key__c IN :opportunitiesExternalIds];
        }

        return results;
    }

    /*private static List<Event> getRelatedEvents(Set<Id> serviceAppointmentsIds){
        Map<Id,List<Event>> eventssByServiceAppIds = new Map<Id,List<Event>>();
        for(Id serviceAppointmentsId : serviceAppointmentsIds){
            eventssByServiceAppIds.put(serviceAppointmentsId, new List<Event>());
        }

        List<Event> assignedResources = [SELECT Id, AssignedResourceNumber, CreatedDate, CreatedById, LastModifiedDate,
            LastModifiedById, SystemModstamp, 
            ServiceAppointmentId, ServiceResourceId, IsRequiredResource, Role, EventId, IsPrimaryResource
            FROM AssignedResource
            WHERE ServiceAppointmentId IN :serviceAppointmentsIds];

        for(AssignedResource assignedResource : assignedResources){
            eventssByServiceAppIds.get(assignedResource.ServiceAppointmentId).add(assignedResource);
        }

        return eventssByServiceAppIds;
    }*/

    private static Map<String,List<AssignedResource>> getRelatedAssignedResources(Set<String> opportunitiesExternalIds){
        Map<String, List<AssignedResource>> assignedResourcesByServiceAppIds = new Map<String,List<AssignedResource>>();

        if(opportunitiesExternalIds.isEmpty()){
            return assignedResourcesByServiceAppIds;
        }

        for(String  oppExternalId : opportunitiesExternalIds){
            assignedResourcesByServiceAppIds.put(oppExternalId, new List<AssignedResource>());
        }

        List<AssignedResource> assignedResources = [SELECT Id, AssignedResourceNumber, CreatedDate, CreatedById, LastModifiedDate,
            LastModifiedById, SystemModstamp, ServiceAppointment.CORE_Lead_form_acquisition_unique_key__c,
            ServiceAppointmentId, ServiceResourceId, IsRequiredResource, Role, EventId, IsPrimaryResource
            FROM AssignedResource
            WHERE ServiceAppointment.CORE_Lead_form_acquisition_unique_key__c IN :opportunitiesExternalIds];

        for(AssignedResource assignedResource : assignedResources){
            assignedResourcesByServiceAppIds.get(assignedResource.ServiceAppointment.CORE_Lead_form_acquisition_unique_key__c).add(assignedResource);
        }

        return assignedResourcesByServiceAppIds;
    }
}