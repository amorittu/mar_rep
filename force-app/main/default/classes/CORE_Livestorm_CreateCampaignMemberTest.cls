@IsTest
global class CORE_Livestorm_CreateCampaignMemberTest {
    @TestSetup
    public static void makeData(){
        CORE_Livestorm_DataFaker_InitiateTest.CreateCampaignWithMember();
    }

    @isTest
    public static void fullSuccessExecuteTest() {
        Test.setMock(HttpCalloutMock.class, new FullSuccessCreateCampaignMock());
        SchedulableContext ctx;

        Test.startTest();
        CORE_Livestorm_CreateCMMethod.execute(ctx);
        Test.stopTest();

        List<CampaignMember> campaignMembers = [SELECT Id,CORE_Livestorm_People_Id__c, CORE_Livestorm_People_Link__c FROM CampaignMember];
        List<Campaign> campaigns = [SELECT Id,CORE_Number_of_Member_in_Livestorm__c FROM Campaign];
        System.assertEquals(1, campaignMembers.size());

        CampaignMember campaignMember = campaignMembers.get(0);
        System.assertEquals('https://app.livestorm.co/p/2b7de38f-d2d9-4a73-beac-a6ed54d8a38b/live?s=6b3e4bde-2a75-4ed8-a4af-83a08a68a057', campaignMember.CORE_Livestorm_People_Link__c);
        System.assertEquals('6b3e4bde-2a75-4ed8-a4af-83a08a68a057', campaignMember.CORE_Livestorm_People_Id__c);
        System.assertEquals(1, campaigns.size());

        Campaign campaign = campaigns.get(0);
        System.assertEquals(1, campaign.CORE_Number_of_Member_in_Livestorm__c);
    }

    global class FullSuccessCreateCampaignMock implements HttpCalloutMock {
        global HttpResponse respond(HttpRequest req) {
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');

            if (req.getEndpoint().endsWith('events')){
                res.setBody('{"data": {"id": "2b7de38f-d2d9-4a73-beac-a6ed54d8a38b","attributes": {"registration_link": "https://app.livestorm.co/p/2b7de38f-d2d9-4a73-beac-a6ed54d8a38b"}}}');
                res.setStatusCode(201);
            }
            else if (req.getEndpoint().endsWith('sessions')){
                res.setBody('{"data": {"id": "6b3e4bde-2a75-4ed8-a4af-83a08a68a057","attributes": {"room_link": "https://app.livestorm.co/p/2b7de38f-d2d9-4a73-beac-a6ed54d8a38b/live?s=6b3e4bde-2a75-4ed8-a4af-83a08a68a057"}}}');
                res.setStatusCode(201);
            }
            else if (req.getEndpoint().endsWith('people')){
                res.setBody('{"data": {"id": "6b3e4bde-2a75-4ed8-a4af-83a08a68a057","attributes": {"registrant_detail": {"connection_link": "https://app.livestorm.co/p/2b7de38f-d2d9-4a73-beac-a6ed54d8a38b/live?s=6b3e4bde-2a75-4ed8-a4af-83a08a68a057"}}}}');
                res.setStatusCode(201);
            }

            return res;
        }
    }

    @isTest
    public static void fullSuccessWrongDataExecuteTest() {
        Test.setMock(HttpCalloutMock.class, new FullSuccessWrongDataCreateCampaignMock());
        SchedulableContext ctx;

        Test.startTest();
        CORE_Livestorm_CreateCMMethod.execute(ctx);
        Test.stopTest();

        List<CampaignMember> campaignMembers = [SELECT Id,CORE_Livestorm_People_Id__c, CORE_Livestorm_People_Link__c FROM CampaignMember];
        System.assertEquals(1, campaignMembers.size());

        CampaignMember campaign = campaignMembers.get(0);
        System.assertEquals(null, campaign.CORE_Livestorm_People_Link__c);
        System.assertEquals(null, campaign.CORE_Livestorm_People_Id__c);
    }

    global class FullSuccessWrongDataCreateCampaignMock implements HttpCalloutMock {
        global HttpResponse respond(HttpRequest req) {
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');

            if (req.getEndpoint().endsWith('events')){
                res.setBody('{"data": {"id": "2b7de38f-d2d9-4a73-beac-a6ed54d8a38b","attributes": {"registration_link": "https://app.livestorm.co/p/2b7de38f-d2d9-4a73-beac-a6ed54d8a38b"}}}');
                res.setStatusCode(201);
            }
            else if (req.getEndpoint().endsWith('sessions')){
                res.setBody('{"data": {"id": "6b3e4bde-2a75-4ed8-a4af-83a08a68a057","attributes": {"room_link": "https://app.livestorm.co/p/2b7de38f-d2d9-4a73-beac-a6ed54d8a38b/live?s=6b3e4bde-2a75-4ed8-a4af-83a08a68a057"}}}');
                res.setStatusCode(201);
            }
            else if (req.getEndpoint().endsWith('people')){
                res.setBody('{"data2": {"id": "6b3e4bde-2a75-4ed8-a4af-83a08a68a057","attributes": {"registrant_detail": {"connection_link": "https://app.livestorm.co/p/2b7de38f-d2d9-4a73-beac-a6ed54d8a38b/live?s=6b3e4bde-2a75-4ed8-a4af-83a08a68a057"}}}}');
                res.setStatusCode(201);
            }

            return res;
        }
    }

    @isTest
    public static void fullFailExecuteTest() {
        Test.setMock(HttpCalloutMock.class, new FullFailCreateCampaignMock());
        SchedulableContext ctx;

        Test.startTest();
        CORE_Livestorm_CreateCMMethod.execute(ctx);
        Test.stopTest();

        List<CampaignMember> campaignMembers = [SELECT Id,CORE_Livestorm_People_Id__c, CORE_Livestorm_People_Link__c FROM CampaignMember];
        System.assertEquals(1, campaignMembers.size());

        CampaignMember campaign = campaignMembers.get(0);
        System.assertEquals(null, campaign.CORE_Livestorm_People_Link__c);
        System.assertEquals(null, campaign.CORE_Livestorm_People_Id__c);
    }

    global class FullFailCreateCampaignMock implements HttpCalloutMock {
        global HttpResponse respond(HttpRequest req) {
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');

            if (req.getEndpoint().endsWith('events')){
                res.setBody('{"data": {"id": "2b7de38f-d2d9-4a73-beac-a6ed54d8a38b","attributes": {"registration_link": "https://app.livestorm.co/p/2b7de38f-d2d9-4a73-beac-a6ed54d8a38b"}}}');
                res.setStatusCode(201);
            }
            else if (req.getEndpoint().endsWith('sessions')){
                res.setBody('{"data": {"id": "6b3e4bde-2a75-4ed8-a4af-83a08a68a057","attributes": {"room_link": "https://app.livestorm.co/p/2b7de38f-d2d9-4a73-beac-a6ed54d8a38b/live?s=6b3e4bde-2a75-4ed8-a4af-83a08a68a057"}}}');
                res.setStatusCode(201);
            }
            else if (req.getEndpoint().endsWith('people')){
                res.setBody('{"data": {"id": "6b3e4bde-2a75-4ed8-a4af-83a08a68a057","attributes": {"registrant_detail": {"connection_link": "https://app.livestorm.co/p/2b7de38f-d2d9-4a73-beac-a6ed54d8a38b/live?s=6b3e4bde-2a75-4ed8-a4af-83a08a68a057"}}}}');
                res.setStatusCode(202);
            }

            return res;
        }
    }
}