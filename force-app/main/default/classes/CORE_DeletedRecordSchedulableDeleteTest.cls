@IsTest
public class CORE_DeletedRecordSchedulableDeleteTest {
    @IsTest
    public static void deleted_Record_Schedulable_Delete_Should_Be_Launch() {
        test.starttest();
        CORE_DeletedRecordSchedulableDelete myClass = new CORE_DeletedRecordSchedulableDelete();   
        String chron = '0 0 23 * * ?';        
        system.schedule('Test Sched', chron, myClass);
        test.stopTest();

        List<AsyncApexJob> jobsScheduled = [select Id, ApexClassID, ApexClass.Name, Status, JobType from AsyncApexJob where JobType = 'ScheduledApex'];
        System.assertEquals(1, jobsScheduled.size(), 'expecting one scheduled job');
        System.assertEquals('CORE_DeletedRecordSchedulableDelete', jobsScheduled[0].ApexClass.Name, 'expecting specific scheduled job');

        List<AsyncApexJob> jobsApexBatch = [select Id, ApexClassID, ApexClass.Name, Status, JobType from AsyncApexJob where JobType = 'BatchApex'];
        System.assertEquals(1, jobsApexBatch.size(), 'expecting one apex batch job');
        System.assertEquals('CORE_DeletedRecordBatchableDelete', jobsApexBatch[0].ApexClass.Name, 'expecting specific batch job');
    }
}