global class CORE_OnlineShop_Wrapper {

    global class ResultWrapperOpportunity {
		global Opportunity opportunity;

        global String status;
        global String message;
        
        global ResultWrapperOpportunity(){}

		global ResultWrapperOpportunity(Opportunity opportunity, String status){
            this.opportunity = opportunity;
            this.status = status;
		}

        global ResultWrapperOpportunity(String status, String message){
            this.status = status;
            this.message = message;
		}
	}

	global class InterestWrapper extends CORE_ObjectWrapper {
        global String businessUnit;
		global String studyArea;
		global String curriculum;
		global String level;
        global String intake;  
		global String product; 
		global Decimal quantity;
		global Decimal salesPrice;
    }
}