/**
* @author Zameer Khodabocus - Almavia
* @date 2022-01-20
* @group Queueable
* @description Class used by lwc, coreMassOppAcademicYearConfirmation, to change academic year of Opportunities in mass asynchronously
*/
public without sharing class CORE_QU_MassOppAcademicYearConfirmation implements Queueable {

    public Set<Id> oppsIdSet;
    public String divId;
    public String buId;
    public String academicYear;
    public String pbId;
    public List<String> resultsList;

    public CORE_QU_MassOppAcademicYearConfirmation(Set<Id> oppsIdSet, String divId, String buId, String academicYear, String pbId, List<String> resultsList){
        this.oppsIdSet = oppsIdSet;
        this.divId = divId;
        this.buId = buId;
        this.academicYear = academicYear;
        this.pbId = pbId;
        this.resultsList = resultsList;
    }

    public void execute(QueueableContext context){ 

        CORE_LC_MassOppAcademicYearConfirmation.SettingWrapper setting = CORE_LC_MassOppAcademicYearConfirmation.getOrgSetting();

        try {
            Set<Id> remOppsIdSet = oppsIdSet;
            Integer batchSize = (setting.batchSize != null) ? setting.batchSize :CORE_LC_MassOppAcademicYearConfirmation.defaultBatchSize;

            CORE_LC_MassOppAcademicYearConfirmation.AsyncWrapper asynWrap = CORE_LC_MassOppAcademicYearConfirmation.changeAcademicYear(oppsIdSet, divId, buId, academicYear, pbId, batchSize, resultsList, false);
            remOppsIdSet = asynWrap.remOppsIdSet; 
            resultsList = asynWrap.resultsList;

            if(remOppsIdSet.size() > 0
               && !test.isRunningTest()){//Test class does not support chaining jobs

                System.enqueueJob(new CORE_QU_MassOppAcademicYearConfirmation(remOppsIdSet, divId, buId, academicYear, pbId, resultsList));
            }
            //Job has finished
            else{
                database.update(new Core_MassOppConfirmAcademicYear__c(
                    Id = setting.settingId, CORE_JobInProgress__c = false
                ), true);

                CORE_MassOpportunityResultHandler resultHandler = new CORE_MassOpportunityResultHandler();
                resultHandler.settingName = CORE_LC_MassOppAcademicYearConfirmation.EMAIL_SETTING_NAME;
                resultHandler.resultsList = resultsList;

                resultHandler.sendMail();

            }
        }
        catch(Exception e){
            system.debug('Exception: ' + e.getMessage() + ' => ' + e.getStackTraceString());

            database.update(new Core_MassOppConfirmAcademicYear__c(
                Id = setting.settingId, CORE_JobInProgress__c = false
            ), true);

            CORE_MassOpportunityResultHandler resultHandler = new CORE_MassOpportunityResultHandler();
            resultHandler.settingName = CORE_LC_MassOppAcademicYearConfirmation.EMAIL_SETTING_NAME;
            resultHandler.resultsList = resultsList;
            resultHandler.exceptionError = 'APEX Exception Error: ' + e.getMessage() + ' => ' + e.getStackTraceString();

            resultHandler.sendMail();
        }
    }
}