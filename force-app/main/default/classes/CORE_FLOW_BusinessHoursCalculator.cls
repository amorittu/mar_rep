/**
 * Created By Fodil 06-05-2022
 */
public with sharing class CORE_FLOW_BusinessHoursCalculator {
    public class RequestWrapper {

        @InvocableVariable(label='Businesshour Id' description='the Id of Business Hour' required=true)
        public ID businessHourId;
    
        @InvocableVariable(label='Start Date' description='the Start date to add hours after it' required=true)
        public DateTime startDate;
    
        @InvocableVariable (label='number of hours' description='the number of hours to add after the start date' required=true)
        public Long hours;

      }
    
    @InvocableMethod(Label='Get Next DateTime after a set of Business Hours')
    public static List<DateTime> getNextDateTime(List<requestWrapper> requests) {
        List<DateTime> results = new List<DateTime>();
        for (requestWrapper req : requests) {
            results.add(CORE_BusinessHoursUtils.getNextDateTime(req.businessHourId,
            req.startDate ,
            req.hours));
        }
        return results;
      }
}