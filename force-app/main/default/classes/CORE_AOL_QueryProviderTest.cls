@IsTest
public class CORE_AOL_QueryProviderTest {
    private static final String AOL_EXTERNALID = 'setupAol';

    @TestSetup
    static void makeData(){
        CORE_CatalogHierarchyModel catalogHierarchy = new CORE_DataFaker_CatalogHierarchy(AOL_EXTERNALID).catalogHierarchy;
        Account account = CORE_DataFaker_Account.getStudentAccount(AOL_EXTERNALID);

        Opportunity  opportunity = CORE_DataFaker_Opportunity.getAOLOpportunity(
            catalogHierarchy.division.Id, 
            catalogHierarchy.businessUnit.Id,
            catalogHierarchy.studyArea.Id, 
            catalogHierarchy.curriculum.Id, 
            catalogHierarchy.level.Id,
            catalogHierarchy.intake.Id,
            account.Id, 
            CORE_AOL_Constants.PKL_OPP_STAGENAME_PROSPECT, 
            CORE_AOL_Constants.PKL_OPP_SUBSTATUS_PROSPECTAPPSTARTED, 
            AOL_EXTERNALID
        );

        Id pricebookId = CORE_DataFaker_PriceBook.getStandardPricebookId();
        PricebookEntry pbEntry = CORE_DataFaker_PriceBook.getPriceBookEntry(catalogHierarchy.product.Id, pricebookId, 10);
        OpportunityLineItem oppLineItem = CORE_DataFaker_Opportunity.getOpportunityLineItem(opportunity.Id, pbEntry.Id, AOL_EXTERNALID);
    }

    @IsTest
    private static void getOppLineItems_Should_return_opportunity_line_items_related_to_opportunity_and_product(){
        Opportunity opportunity = [SELECT Id FROM Opportunity WHERE CORE_OPP_Application_Online_ID__c = :AOL_EXTERNALID];
        Product2 product = [SELECT Id FROM Product2 WHERE ProductCode = :AOL_EXTERNALID];

        List<OpportunityLineItem> result = CORE_AOL_QueryProvider.getOppLineItems(opportunity.Id, product.Id);

        System.assertEquals(false, result.isEmpty());
    }

    @IsTest
    private static void getAttachments_Should_return_attachements_related_to_opportunity(){
        Opportunity opportunity = [SELECT Id FROM Opportunity WHERE CORE_OPP_Application_Online_ID__c = :AOL_EXTERNALID];
        
        ContentDocumentLink documentLink = CORE_DataFaker_ContentDocumentLink.getContentDocumentLink(opportunity.Id);

        List<ContentDocumentLink> result = CORE_AOL_QueryProvider.getAttachments(opportunity.Id);

        System.assertEquals(false, result.isEmpty());
    }

    @IsTest
    private static void getOpportunityContactRoles_Should_return_contactRoles_related_to_opportunity(){
        Opportunity opportunity = [SELECT Id,AccountId FROM Opportunity WHERE CORE_OPP_Application_Online_ID__c = :AOL_EXTERNALID];
        Account account = [SELECT Id,PersonContactId FROM Account Where Id = :opportunity.AccountId];

        OpportunityContactRole contactRole = CORE_DataFaker_OpportunityContactRole.getOpportunityContactRole(opportunity.Id, account.PersonContactId);

        List<OpportunityContactRole> result = CORE_AOL_QueryProvider.getOpportunityContactRoles(opportunity.Id);

        System.assertEquals(false, result.isEmpty());
    }

    @IsTest
    private static void getSubStatus_Should_return_subStatus_related_to_opportunity(){
        Opportunity opportunity = [SELECT Id,AccountId FROM Opportunity WHERE CORE_OPP_Application_Online_ID__c = :AOL_EXTERNALID];

        List<CORE_Sub_Status_Timestamp_History__c> result = CORE_AOL_QueryProvider.getSubStatus(opportunity.Id);

        System.assertEquals(false, result.isEmpty());
    }

    @IsTest
    private static void getStatus_Should_return_status_related_to_opportunity(){
        Opportunity opportunity = [SELECT Id,AccountId FROM Opportunity WHERE CORE_OPP_Application_Online_ID__c = :AOL_EXTERNALID];

        List<CORE_Status_Timestamp_History__c> result = CORE_AOL_QueryProvider.getStatus(opportunity.Id);

        System.assertEquals(false, result.isEmpty());
    }

    @IsTest
    private static void getPointOfContacts_Should_return_pointOfContacts_related_to_opportunity(){
        Opportunity opportunity = [SELECT Id,AccountId FROM Opportunity WHERE CORE_OPP_Application_Online_ID__c = :AOL_EXTERNALID];

        CORE_Point_of_Contact__c pointOfContact = CORE_DataFaker_PointOfContact.getPointOfContact(opportunity.Id, opportunity.AccountId, AOL_EXTERNALID);
        
        List<CORE_Point_of_Contact__c> result = CORE_AOL_QueryProvider.getPointOfContacts(opportunity.Id);

        System.assertEquals(false, result.isEmpty());
    }

    @IsTest
    private static void getExistingMainOpps_should_return_existing_main_opportunities_related_to_account_and_bu_and_ay(){
        Opportunity opportunity = [SELECT Id,AccountId, CORE_OPP_Academic_Year__c, CORE_OPP_Division__c, CORE_OPP_Business_Unit__r.CORE_Business_Unit_ExternalId__c FROM Opportunity WHERE CORE_OPP_Application_Online_ID__c = :AOL_EXTERNALID];
        Account account = [SELECT Id,PersonContactId FROM Account Where Id = :opportunity.AccountId];

        String aolExternalId = 'getExistingMainOppsId';
        Opportunity  opportunity2 = CORE_DataFaker_Opportunity.getAOLOpportunity(
            opportunity.CORE_OPP_Division__c, 
            opportunity.CORE_OPP_Business_Unit__c,
            opportunity.AccountId, 
            CORE_AOL_Constants.PKL_OPP_STAGENAME_PROSPECT, 
            CORE_AOL_Constants.PKL_OPP_SUBSTATUS_PROSPECTAPPSTARTED, 
            null
        );

        List<Opportunity> result = CORE_AOL_QueryProvider.getExistingMainOpps(opportunity.AccountId, 
            opportunity.CORE_OPP_Business_Unit__r.CORE_Business_Unit_ExternalId__c,
            opportunity.CORE_OPP_Academic_Year__c
        );

        System.assertEquals(false, result.isEmpty());
        System.assertEquals(opportunity2.Id, result.get(0).Id);
    }

    @IsTest
    private static void getAolOpportunities_should_return_opportunity_with_specified_aolExternalId(){
        Opportunity opportunity = [SELECT Id,AccountId, CORE_OPP_Academic_Year__c, CORE_OPP_Division__c, CORE_OPP_Business_Unit__c FROM Opportunity WHERE CORE_OPP_Application_Online_ID__c = :AOL_EXTERNALID];

        List<Opportunity> result = CORE_AOL_QueryProvider.getAolOpportunities(AOL_EXTERNALID);

        System.assertEquals(false, result.isEmpty());
        System.assertEquals(opportunity.Id, result.get(0).Id);
    }
}