public with sharing class CORE_TriggerUtils {
    @TestVisible
    private static Boolean bypassTrigger = false;

    public static void setBypassTrigger(){
        bypassTrigger = !bypassTrigger;
    }

    public static Boolean shouldRunTrigger() {
        return !bypassTrigger;
    }
}