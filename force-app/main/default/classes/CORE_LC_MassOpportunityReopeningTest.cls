/**
* @author Fodil BOUDJEDIEN - Almavia
* @date 2022-03-28
* @group LWC
* @description Test class of the lwc controller class ;
* @test class 
*/
@isTest
public without sharing class CORE_LC_MassOpportunityReopeningTest {
    @TestSetup
    static void makeData(){
        CORE_DataFaker_CatalogHierarchy catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test');
        Account account = CORE_DataFaker_Account.getStudentAccount('test');
        Opportunity opp = CORE_DataFaker_Opportunity.getOpportunity(catalogHierarchy.division.Id, 
            catalogHierarchy.businessUnit.Id, 
            catalogHierarchy.studyArea.Id,
            catalogHierarchy.curriculum.Id,
            catalogHierarchy.level.Id,
            catalogHierarchy.intake.Id,
            account.Id, 
            'Registered', 
            'Registered - Classical'
        );
        opp.CORE_OPP_Academic_Year__c = 'CORE_PKL_2021-2022';
        update opp;

        CORE_OpportunityDataMaker dataMaker = new CORE_OpportunityDataMaker();
        String captureChannel = 'CORE_PKL_Emailings';
        String nature = 'CORE_PKL_Online';
        String origin = 'Website';
        CORE_Point_of_Contact__c result = dataMaker.initPocDefaultValues(opp,nature,captureChannel,origin);
        insert result;

        // custom settings
        MassOpportunityReregistration__c settings = new MassOpportunityReregistration__c(SetupOwnerId = Userinfo.getOrganizationId());
        settings.Job_in_Progress__c=false;
        settings.ListView_Ending__c='';
        settings.Synchronous_Batch__c=200;
        settings.Batch_Size__c=200;
        insert settings;


    }

    @isTest
    public static void testGetSessionId(){
        String sessionId = CORE_LC_MassOpportunityReopening.getSessionID();
        System.assert(sessionId != '');
    }

    @isTest
    public static void testGetOrgSettings(){
        Map<String,Object> mapSettings = CORE_LC_MassOpportunityReopening.getOrgSetting();
        system.assert(mapSettings.get('JobInProgress') == false);
    }

    @isTest
    public static void testGetListViews(){
        List<ListView> listViews = CORE_LC_MassOpportunityReopening.getListViews();
    }

    @isTest
    public static void testGetListViewQuery(){
        ListView listviews = [SELECT Id FROM ListView where SobjectType='Opportunity' Limit 1];
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('CORE_MassOppReregistration_VIEW_RESP');
        mock.setStatusCode(200);
        
        // Set the mock callout mode
        Test.setMock(HttpCalloutMock.class, mock);
        Test.startTest();
        String query = CORE_LC_MassOpportunityReopening.getListViewQuery(listviews.Id);  
        Test.stopTest();
        System.assert(query.contains('SELECT'));
    }

    @isTest
    public static void testGetListViewRecordsCount(){
        ListView listViewRecord = [SELECT Id FROM ListView where SobjectType='Opportunity' Limit 1];
        CORE_Business_Unit__c bu = [Select Id from CORE_Business_Unit__c limit 1];
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('CORE_MassOppReregistration_VIEW_RESP');
        mock.setStatusCode(200);
        
        // Set the mock callout mode
        Test.setMock(HttpCalloutMock.class, mock);
        Test.startTest();
        CORE_LC_MassOpportunityReopening.getListViewRecordsCount(listViewRecord.Id);
        Test.stopTest();
    }
    @isTest
    public static void testGetBusinessUnits(){
        System.assertEquals(CORE_LC_MassOpportunityReopening.getBusinessUnits().size(),1);
    }
    @isTest
    public static void testGetCurrentAcademicYear(){
        String currentAY = CORE_LC_MassOpportunityReopening.getCurrentAcademicYear();
        if(Date.today().month()>=9){ // the picklist contains the current and next year
            System.assert(currentAY.contains(String.valueOf(Date.today().year())));
            System.assert(currentAY.contains(String.valueOf(Date.today().year()+1)));
        }else {// the picklist contains the current and previous year
            System.assert(currentAY.contains(String.valueOf(Date.today().year())));
            System.assert(currentAY.contains(String.valueOf(Date.today().year()-1)));
        }   
    }
    @isTest
    public static void testLaunchBatchSync(){
        ListView listViewRecord = [SELECT Id FROM ListView where SobjectType='Opportunity' Limit 1];
        CORE_Business_Unit__c bu = [Select Id from CORE_Business_Unit__c limit 1];
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('CORE_MassOppReregistration_VIEW_RESP');
        mock.setStatusCode(200);
        
        // Set the mock callout mode
        Test.setMock(HttpCalloutMock.class, mock);
        Test.startTest();
        CORE_LC_MassOpportunityReopening.launchBatch(1,'CORE_PKL_2021-2022',bu.Id,listViewRecord.Id);
        Test.stopTest();
    }

    @isTest
    public static void testLaunchBatchAsync(){
        ListView listViewRecord = [SELECT Id FROM ListView where SobjectType='Opportunity' Limit 1];
        CORE_Business_Unit__c bu = [Select Id from CORE_Business_Unit__c limit 1];
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('CORE_MassOppReregistration_VIEW_RESP');
        mock.setStatusCode(200);
        
        // Set the mock callout mode
        Test.setMock(HttpCalloutMock.class, mock);
        Test.startTest();
        CORE_LC_MassOpportunityReopening.launchBatch(201,'CORE_PKL_2021-2022',bu.Id,listViewRecord.Id);
        Test.stopTest();
    }

    @isTest
    public static void testGetOpportunitiesTotal(){
        ListView listViewRecord = [SELECT Id FROM ListView where SobjectType='Opportunity' Limit 1];
        CORE_Business_Unit__c bu = [Select Id from CORE_Business_Unit__c limit 1];
        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('CORE_MassOppReregistration_VIEW_RESP');
        mock.setStatusCode(200);
        
        // Set the mock callout mode
        Test.setMock(HttpCalloutMock.class, mock);
        Test.startTest();
        CORE_LC_MassOpportunityReopening.getOpportunitiesTotal(listViewRecord.Id,bu.Id,'CORE_PKL_2021-2022');
        Test.stopTest();
    }
}