@isTest
public class IT_EmailChecker_FC_Test {

    @TestSetup
    public static void makeData(){


        IT_Email_Receiver_Checker__mdt metadata = new IT_Email_Receiver_Checker__mdt();
        metadata.IT_Receiver_Address__c  = 'noreply@istitutomarangoni.com';
        metadata.IT_is_Included__c       = true;

        CORE_DataFaker_CatalogHierarchy catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test1');
        Account account = CORE_DataFaker_Account.getStudentAccount('CHK');

        Opportunity opportunity = CORE_DataFaker_Opportunity.getOpportunity(
                catalogHierarchy.division.Id,
                catalogHierarchy.businessUnit.Id,
                account.Id,
                'Applicant',
                'Applicant - Application Submitted / New'
        );

        opportunity.CORE_OPP_Intake__c = catalogHierarchy.intake.Id;
        opportunity.CORE_OPP_Curriculum__c = catalogHierarchy.curriculum.Id;
        opportunity.CORE_OPP_Study_Area__c = catalogHierarchy.studyArea.Id;
        opportunity.CORE_OPP_Level__c = catalogHierarchy.level.Id;
        update opportunity;

        IT_DataFaker_Objects.createExtendedInfo(account.Id, opportunity.CORE_OPP_Business_Unit__r.CORE_Brand__c, true);

        EmailMessage emailMessage = new EmailMessage();
        emailMessage.status         = '3';
        emailMessage.fromName       = 'TEST FROM';
        emailMessage.subject        = 'TEST SUBJECT';
        emailMessage.RelatedToId    = opportunity.Id;
        emailMessage.FromAddress    = 'test@null.it';
        emailMessage.ToAddress      = 'testTo@null.it';
        insert emailMessage;
    }

    @isTest
    public static void testBehavior() {

        Test.startTest();
            Account acc = [SELECT Id FROM Account WHERE FirstName LIKE '%CHK%' LIMIT 1];

            Opportunity opp = [SELECT Id FROM Opportunity WHERE AccountId =: acc.Id ];

            List<IT_EmailChecker_FC.FlowInputs> requests = new List<IT_EmailChecker_FC.FlowInputs>();

            IT_EmailChecker_FC.FlowInputs request = new IT_EmailChecker_FC.FlowInputs();
            request.account             = acc;
            request.opportunity         = opp;
            requests.add(request);

            List<IT_EmailChecker_FC.FlowOutputs> outcome = IT_EmailChecker_FC.checkEmails(requests); 
        Test.stopTest();
    }
}