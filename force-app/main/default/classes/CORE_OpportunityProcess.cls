/*******************************************************/
/**     @Author: Valentin Pitel                       **/
/**     @UnitTestClass: CORE_OpportunityProcessTest   **/
/*******************************************************/
/**  Descritption :                                   **/
/**  class used by flow in order to create opportunity**/
/**  and its related datas                            **/
/*******************************************************/
global class CORE_OpportunityProcess {

    @InvocableMethod
    global static List<Opportunity> createOpportunity(List<OpportunityProcessParameters> params){
        System.debug('createOpportunity : START');
        OpportunityProcessParameters param = params.get(0);
        CORE_OpportunityDataMaker dataMaker = new CORE_OpportunityDataMaker();
        System.debug('createOpportunity : param = ' + param);

        Account account = new Account(Id = param.accountId);
        System.debug('createOpportunity : account = ' + account);
        Id businessUnitId = String.isBlank(param.businessUnitId) ? null : param.businessUnitId;
        Id studyAreaId = String.isBlank(param.studyAreaId) ? null : param.studyAreaId;
        Id curriculumId = String.isBlank(param.curriculumId) ? null : param.curriculumId;
        Id levelId = String.isBlank(param.levelId) ? null : param.levelId;
        Id intakeId = String.isBlank(param.intakeId) ? null : param.intakeId;
        Id productId = String.isBlank(param.productId) ? null : param.productId;
        String retailValue = String.isBlank(param.retailAccountId) ? 'CORE_PKL_Direct' : 'CORE_PKL_Retail';
        Id retailId = String.isBlank(param.retailAccountId) ? null : param.retailAccountId;
        Opportunity opportunityValues = param.opportunity == null ? new Opportunity() : param.opportunity;

        System.debug('productId = ' + productId);
        String academicYear = param.academicYear;
        Id priceBookId;


        CORE_CatalogHierarchyModel catalogHierarchy = new CORE_CatalogHierarchyModel(
            null, 
            businessUnitId, 
            studyAreaId, 
            curriculumId, 
            levelId, 
            intakeId, 
            productId
        );

        System.debug('catalogHierarchy = '+ catalogHierarchy);

        if(!String.isBlank(param.priceBookId)){
            priceBookId = param.priceBookId;
            catalogHierarchy.priceBook2Id = priceBookId;
        }
        //init catalog hierarchy using productId, intakeId or curriculumId
        //!!!!!!!!!!!!!!!!!!!!!!!!   => SOQL x1   !!!!!!!!!!!!!!!!!!!!!!!!
        catalogHierarchy = CORE_OpportunityDataMaker.getCatalogHierarchy(catalogHierarchy,false);
        if(!String.isBlank(catalogHierarchy.intake.CORE_Academic_Year__c)){
            academicYear = catalogHierarchy.intake.CORE_Academic_Year__c;
        }
        /* academic Year according to Custom metadataType */
        CORE_Opportunity_creation_setting__mdt oppMDT = CORE_Opportunity_creation_setting__mdt.getInstance('Default_values');

        System.debug('catalogHierarchy.division = ' +  catalogHierarchy.division);
        System.debug('createOpportunity : catalogHierarchy = ' + catalogHierarchy);

        //check if a main opportunity already exist
        //!!!!!!!!!!!!!!!!!!!!!!!!   => SOQL x1   !!!!!!!!!!!!!!!!!!!!!!!!
        Boolean mainOpportunityExist = CORE_OpportunityUtils.isExistingMainOpportunity(catalogHierarchy.businessUnit.Id, account.Id, academicYear);

        System.debug('createOpportunity : mainOpportunityExist = ' + mainOpportunityExist);
        Opportunity opp = dataMaker.initOpportunity(account, academicYear, catalogHierarchy, mainOpportunityExist, opportunityValues);
        opp.CORE_OPP_Retail_Agency__c = retailId;
        /* Study mode filling */
        if(oppMDT.CORE_Study_mode__c && curriculumId != null){
            CORE_Curriculum__c curriculum = [SELECT Id,CORE_Study_Mode__c FROM CORE_Curriculum__c where Id =:curriculumId ];
            //The_Study_mode__mdt	studyModeMDT = The_Study_mode__mdt.getInstance('CORE_Study_mode');
            if(curriculum != null){
                opp.Study_mode__c = curriculum.CORE_Study_Mode__c;
            }
        }
        
        Insert opp;

        if(!String.IsBlank(opp.CORE_Main_Product_Interest__c) && !String.IsBlank(priceBookId)){
            PricebookEntry pricebookEntry = dataMaker.getProductPricebookEntryByPb(priceBookId, opp.CORE_Main_Product_Interest__c);

            if(pricebookEntry != NULL){
                OpportunityLineItem oppLineItem = dataMaker.initOppLineItem(opp, pricebookEntry);
                Insert oppLineItem;
            }
            //OpportunityLineItem oppLineItem = 
        }

        System.debug('createOpportunity : opp = ' + opp);
        CORE_Point_of_Contact__c poc = dataMaker.initPocDefaultValues(opp, param.nature, param.captureChannel, param.origin);
        poc.CORE_Enrollment_Channel__c = retailValue;
        poc.CORE_Instant_messaging_channel__c = param.instantMessagingChannel;
        poc.CORE_Insertion_Date__c = System.now();
        insert poc;

        System.debug('createOpportunity : poc = ' + poc);
        System.debug('createOpportunity : END');
        return new List<Opportunity> {opp};
    }

    global class OpportunityProcessParameters{
        @InvocableVariable(required=true)
        global Id accountId;

        @InvocableVariable(required=true)
        global String captureChannel;

        @InvocableVariable(required=true)
        global String nature;

        @InvocableVariable
        global String origin;

        @InvocableVariable
        global String instantMessagingChannel;

        @InvocableVariable
        global String businessUnitId;

        @InvocableVariable
        global String academicYear;

        @InvocableVariable
        global String studyAreaId;

        @InvocableVariable
        global String curriculumId;

        @InvocableVariable
        global String levelId;

        @InvocableVariable
        global String priceBookId;

        @InvocableVariable
        global String intakeId;

        @InvocableVariable
        global String retailAccountId;

        @InvocableVariable
        global String productId;
        
        @InvocableVariable
        global Opportunity opportunity;
    }
    
    class OpportunityException extends Exception {}
}