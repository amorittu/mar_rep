public class CORE_PointOfContactTriggerHandler extends CORE_TriggerHandler {

    private List<Id> oppId = new List<Id>();
    
    public CORE_PointOfContactTriggerHandler() {        
        for (CORE_Point_Of_Contact__c poc : (List<CORE_Point_Of_Contact__c>) Trigger.new){
            oppId.add(poc.CORE_Opportunity__c);
        }
    }

    public override void afterInsert() {
        System.debug('@CORE_PointOfContactTriggerHandler > afterInsert START');
        //On récupère tous les POC de chaque Opp ordonnée par Date (utile pour la suite)
        List<CORE_Point_Of_Contact__c> listPOC = [SELECT	CORE_Insertion_Date__c,
                                                  			Campaign_Medium__c,
                                                  			CORE_Campaign_Name__c,
                                                  			CORE_Campaign_Source__c,
                                                  			CORE_Campaign_Term__c,
                                                  			CORE_Capture_Channel__c,
                                                  			CORE_Enrollment_Channel__c,
                                                  			CORE_Insertion_Mode__c,
                                                  			CORE_Nature__c,
                                                  			CORE_Origin_Sales__c,
                                                  			CORE_Origin__c,
                                                  			CORE_GCLID__c,
                                                  			CORE_Form__c,
                                                  			CORE_Form_area__c,
                                                  			CORE_Opportunity__c
                                                  FROM CORE_Point_Of_Contact__c
                                                  WHERE CORE_Opportunity__c IN :oppId
                                                  AND CORE_Opportunity__r.CORE_Insertion_Date__c = null
                                                  ORDER BY CORE_Insertion_Date__c];
        
        Map<Id, CORE_Point_Of_Contact__c> mapLastPOCByOpp = new Map<Id, CORE_Point_Of_Contact__c>();
        for (CORE_Point_Of_Contact__c poc : listPOC){
            //Liste triée donc on prend le premier POC pour chaque Opp
            if (!mapLastPOCByOpp.containsKey(poc.CORE_Opportunity__c)){
                mapLastPOCByOpp.put(poc.CORE_Opportunity__c, poc);
            }
        }
        
        //On récupère toutes les données des Opp concernée
        Map<Id, Opportunity> mapOpp = new Map<Id, Opportunity>([SELECT	CORE_Insertion_Date__c,
                              									        CORE_Campaign_Medium__c,
                                        								CORE_Campaign_Name__c,
                                        								CORE_Campaign_Source__c,
                                        								CORE_Campaign_Term__c,
                                        								CORE_Capture_Channel__c,
                                        								CORE_Enrollment_Channel__c,
                                        								CORE_Insertion_Mode__c,
                                        								CORE_Nature__c,
                                        								CORE_Origin_Sales__c,
                                        								CORE_Origin__c,
                                        								CORE_GCLID__c,
                                        								Core_Form__c,
                                        								CORE_Form_area__c
                                        						FROM Opportunity
                                        						WHERE Id IN :oppId
                                                                AND CORE_Insertion_Date__c = null]);
        
        
        for (Opportunity opp : mapOpp.values()){
            CORE_Point_Of_Contact__c poc = mapLastPOCByOpp.get(opp.Id);
            if (poc != null){
                opp.CORE_Insertion_Date__c = poc.CORE_Insertion_Date__c;
                opp.CORE_Campaign_Medium__c = poc.Campaign_Medium__c;
                opp.CORE_Campaign_Name__c = poc.CORE_Campaign_Name__c;
                opp.CORE_Campaign_Source__c = poc.CORE_Campaign_Source__c;
                opp.CORE_Campaign_Term__c = poc.CORE_Campaign_Term__c;
                opp.CORE_Capture_Channel__c = poc.CORE_Capture_Channel__c;
                opp.CORE_Enrollment_Channel__c = poc.CORE_Enrollment_Channel__c;
                opp.CORE_Insertion_Mode__c = poc.CORE_Insertion_Mode__c;
                opp.CORE_Nature__c = poc.CORE_Nature__c;
                opp.CORE_Origin_Sales__c = poc.CORE_Origin_Sales__c;
                opp.CORE_Origin__c = poc.CORE_Origin__c;
                opp.CORE_GCLID__c = poc.CORE_GCLID__c;
                opp.Core_Form__c = poc.Core_Form__c;
                opp.CORE_Form_area__c = poc.CORE_Form_area__c;
            }
        }
        
        update mapOpp.values();
        
        System.debug('@CORE_PointOfContactTriggerHandler > afterInsert END');
    }
}