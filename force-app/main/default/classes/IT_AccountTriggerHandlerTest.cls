/**
 * Created by SaverioTrovatoNigith on 19/04/2022.
 */

@IsTest
private class IT_AccountTriggerHandlerTest {
	@IsTest
	static void testBehavior() {
        Account a = new Account();
        a.FirstName = 'test';
        a.LastName = 'testing';
        a.PersonEmail = 'test@test.com';
        a.CORE_Language__pc = 'Ita';
        insert a;

        Opportunity opp = new Opportunity();
        opp.AccountId = a.Id;
        opp.StageName = 'Prospect';
        opp.CloseDate = System.today();
        opp.CORE_OPP_Sub_Status__c = 'Prospect - New';
        insert opp;

        a.CORE_Language__pc = 'Eng';
        update a;           
	}

        /*********************************************************************************************************
         * @name			setSpamTasksToProperQueueTest
         * @author			Andrea Morittu <amorittu@atlantic-technologies.com>
         * @created			15 / 07 / 2022
         * @description		        test method of setSpamTasksToProperQueue

        **********************************************************************************************************/
        @IsTest
        public static void setSpamTasksToProperQueueTest() {
                Account a = new Account();
                a.FirstName = 'test';
                a.LastName = 'testing';
                a.PersonEmail = 'test@test.com';
                a.CORE_Language__pc = 'Ita';
                insert a;

                Task t = new Task();
                t.Subject = 'SpamTask';
                t.WhatId= a.Id;    
                //t.AccountId = a.Id;
                t.Status = 'CORE_PKL_Open';
                insert t;
                
                Test.startTest();
                        a.Type = 'Spam';
                        update a;
                Test.stopTest();
                        
	}


        /*********************************************************************************************************
         * @name			setSpamTasksToProperQueueTest
         * @author			Andrea Morittu <amorittu@atlantic-technologies.com>
         * @created			15 / 07 / 2022
         * @description		        test method of setSpamTasksToProperQueue

        **********************************************************************************************************/
        @IsTest
        public static void mergeAccount_Test() {
            PermissionSet ps = new PermissionSet();
            ps.Name = 'IT_AllowMergeAccount';
            ps.Label = 'IT_AllowMergeAccount';
            insert ps;
            SetupEntityAccess sea = new SetupEntityAccess();
            sea.ParentId = ps.Id;
            sea.SetupEntityId = [select Id from CustomPermission where DeveloperName = 'IT_AllowMergeAccount'][0].Id;
            insert sea;
            PermissionSetAssignment psa = new PermissionSetAssignment();
            psa.AssigneeId = UserInfo.getUserId();
            psa.PermissionSetId = ps.Id;
            insert psa;
            
            System.runAs( new User( Id = UserInfo.getUserId() ) ) {
                Id studentRTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Core_Student').getRecordTypeId();

                CORE_DataFaker_CatalogHierarchy catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test1');
                
                Account account = new Account(
                        FirstName = 'Test1',
                        LastName = 'Test1',
                        PersonEmail = 'test@test.com',
                        RecordTypeId = studentRTypeId
                );
                insert account;
                

		Opportunity opportunity = CORE_DataFaker_Opportunity.getOpportunity(
				catalogHierarchy.division.Id,
				catalogHierarchy.businessUnit.Id,
				account.Id,
				'Applicant',
				'Applicant - Application Submitted / New'
		);

		opportunity.CORE_OPP_Intake__c = catalogHierarchy.intake.Id;
		opportunity.CORE_OPP_Curriculum__c = catalogHierarchy.curriculum.Id;
		opportunity.CORE_OPP_Study_Area__c = catalogHierarchy.studyArea.Id;
		opportunity.CORE_OPP_Level__c = catalogHierarchy.level.Id;
		opportunity.IT_AdmissionOfficer__c = UserInfo.getUserId();
		update opportunity;

                CORE_Business_Unit__c businessUnit = catalogHierarchy.businessUnit;
                businessUnit.CORE_Business_Unit_ExternalId__c = 'NB.2';
                update businessUnit;

		IT_DataFaker_Objects.createExtendedInfo(account.Id, opportunity.CORE_OPP_Business_Unit__r.CORE_Brand__c, true);

		Id rtId = Schema.SObjectType.Task.getRecordTypeInfosByDeveloperName().get('CORE_Continuum_of_action').getRecordTypeId();

             
           
                Database.DMLOptions dml = new Database.DMLOptions();
                dml.DuplicateRuleHeader.AllowSave = true; 
                Account account2 = new Account(
                                        FirstName = 'Test2',
                                        LastName = 'Test2',
                                        PersonEmail = 'test@test.com',
                                        RecordTypeId = studentRTypeId
                                );
                Database.SaveResult sr = Database.insert(account2, dml); 
                Opportunity opportunity2 = CORE_DataFaker_Opportunity.getOpportunity(
				catalogHierarchy.division.Id,
				catalogHierarchy.businessUnit.Id,
				account2.Id,
				'Applicant',
				'Applicant - Application Submitted / New'
		);



		opportunity2.CORE_OPP_Intake__c = catalogHierarchy.intake.Id;
		opportunity2.CORE_OPP_Curriculum__c = catalogHierarchy.curriculum.Id;
		opportunity2.CORE_OPP_Study_Area__c = catalogHierarchy.studyArea.Id;
		opportunity2.CORE_OPP_Level__c = catalogHierarchy.level.Id;
		opportunity2.IT_AdmissionOfficer__c = UserInfo.getUserId();
		update opportunity2;

                CORE_Business_Unit__c businessUnit2 = catalogHierarchy.businessUnit;
                businessUnit.CORE_Business_Unit_ExternalId__c = 'IM.SH0';
                update businessUnit;

                
              
                Test.startTest();
                                Database.MergeResult[] results = Database.merge(account, new sObject[]{account2}, false);
                Test.stopTest();
			}
        }

}