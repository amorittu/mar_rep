@IsTest
public with sharing class CORE_AOL_OpportunityFeesPaidEnrolTest {
    private static final String AOL_EXTERNALID = 'setupAol';

    private static final String REQ_BODY = '{'
    + '"aolExternalId": "' + AOL_EXTERNALID + '",'
    + '"enrollmentFeesPaid": {'
    + '"received": true,'
    + '"paymentMethod": "CORE_PKL_Cash",'
    + '"receivedDate": "2022-03-29",'
    + '"acceptanceDate": "2022-03-29"'
    + '},'
    + '"additionalFields": {'
    + '"CORE_Campaign_Name__c": "test value"'
    + '},'
    + '"aolProgressionFields": {'
    + '"aolProgression": 50.09,'
    + '"documentProgression": "Started",'
    + '"firstDocumentUploadDate": "2022-03-29",'
    + '"lastAolEventDate": "2022-03-29T08:47:39.689Z",'
    + '"lastDeaAolLoginDate": "2022-03-29T08:47:39.689Z",'
    + '"isAol": true,'
    + '"isAolOnlineRegistered": true,'
    + '"isAolRetailRegistered": true'
    + '}'
    + '}';

    private static final String REQ_BODY_ERROR = '{'
    + '"aolExternalId": "' + AOL_EXTERNALID + '",'
    + '"enrollmentFeesPaid": {'
    + '"received": true,'
    + '"paymentMethod": "invalid_value",'
    + '"receivedDate": "2022-03-29",'
    + '"acceptanceDate": "2022-03-29"'
    + '},'
    + '"additionalFields": {'
    + '"CORE_Campaign_Name__c": "test value"'
    + '},'
    + '"aolProgressionFields": {'
    + '"aolProgression": 50.09,'
    + '"documentProgression": "Started",'
    + '"firstDocumentUploadDate": "2022-03-29",'
    + '"lastAolEventDate": "2022-03-29T08:47:39.689Z",'
    + '"lastDeaAolLoginDate": "2022-03-29T08:47:39.689Z",'
    + '"isAol": true,'
    + '"isAolOnlineRegistered": true,'
    + '"isAolRetailRegistered": true'
    + '}'
    + '}';

    private static final String REQ_BODY_ERROR_NO_ID = '{'
    + '"aolExternalId": "",'
    + '"enrollmentFeesPaid": {'
    + '"received": true,'
    + '"paymentMethod": "CORE_PKL_Cash",'
    + '"receivedDate": "2022-03-29",'
    + '"acceptanceDate": "2022-03-29"'
    + '},'
    + '"additionalFields": {'
    + '"CORE_Campaign_Name__c": "test value"'
    + '},'
    + '"aolProgressionFields": {'
    + '"aolProgression": 50.09,'
    + '"documentProgression": "Started",'
    + '"firstDocumentUploadDate": "2022-03-29",'
    + '"lastAolEventDate": "2022-03-29T08:47:39.689Z",'
    + '"lastDeaAolLoginDate": "2022-03-29T08:47:39.689Z",'
    + '"isAol": true,'
    + '"isAolOnlineRegistered": true,'
    + '"isAolRetailRegistered": true'
    + '}'
    + '}';

    private static final String REQ_BODY_ERROR_INVALID_ID = '{'
    + '"aolExternalId": "invalidID",'
    + '"enrollmentFeesPaid": {'
    + '"received": true,'
    + '"paymentMethod": "CORE_PKL_Cash",'
    + '"receivedDate": "2022-03-29",'
    + '"acceptanceDate": "2022-03-29"'
    + '},'
    + '"additionalFields": {'
    + '"CORE_Campaign_Name__c": "test value"'
    + '},'
    + '"aolProgressionFields": {'
    + '"aolProgression": 50.09,'
    + '"documentProgression": "Started",'
    + '"firstDocumentUploadDate": "2022-03-29",'
    + '"lastAolEventDate": "2022-03-29T08:47:39.689Z",'
    + '"lastDeaAolLoginDate": "2022-03-29T08:47:39.689Z",'
    + '"isAol": true,'
    + '"isAolOnlineRegistered": true,'
    + '"isAolRetailRegistered": true'
    + '}'
    + '}';

    @TestSetup
    static void makeData(){

        CORE_CountrySpecificSettings__c countrySetting = new CORE_CountrySpecificSettings__c(
                CORE_FieldPrefix__c = CORE_AOL_Constants.PREFIX_CORE
        );
        database.insert(countrySetting, true);

        CORE_CatalogHierarchyModel catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test').catalogHierarchy;
        Account account = CORE_DataFaker_Account.getStudentAccount('test');
        Opportunity  opportunity = CORE_DataFaker_Opportunity.getAOLOpportunity(catalogHierarchy.division.Id, 
            catalogHierarchy.businessUnit.Id, 
            account.Id, 
            CORE_AOL_Constants.PKL_OPP_STAGENAME_PROSPECT, 
            CORE_AOL_Constants.PKL_OPP_SUBSTATUS_PROSPECTAPPSTARTED, 
            AOL_EXTERNALID
        );
    }

    @IsTest
    public static void opportunityFeesPaidEnrollement_Should_Update_Opportunity_and_return_it() {
        CORE_AOL_Wrapper.AOLProgressionFields aolProgressionFields= CORE_DataFaker_AOL_Wrapper.getAolProgressionFieldsWrapper();
        CORE_AOL_Wrapper.enrollmentFeesPaid enrollementFeesPaid = CORE_DataFaker_AOL_Wrapper.getEnrollmentFeesPaidWrapper();

        CORE_AOL_GlobalWorker globalWorker = new CORE_AOL_GlobalWorker(aolProgressionFields,AOL_EXTERNALID);

        Opportunity opportunity = CORE_AOL_OpportunityFeesPaidEnrollement.opportunityFeesPaidEnrollementProcess(globalWorker, enrollementFeesPaid);

        System.assertEquals(enrollementFeesPaid.paymentMethod, opportunity.CORE_OPP_Down_Payment_Fee_Payment_Method__c);
        System.assertEquals(enrollementFeesPaid.received, opportunity.CORE_OPP_Down_Payment_Fee_Received__c);
        System.assertEquals(enrollementFeesPaid.receivedDate, opportunity.CORE_OPP_Down_Payment_Fee_Received_Date__c);
        System.assertEquals(enrollementFeesPaid.acceptanceDate, opportunity.CORE_Down_payment_acceptance_date__c);
    }

    @IsTest
    private static void execute_should_return_an_error_if_mandatory_missing(){
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/aol/opportunity/fees-paid/enrollement';

        req.httpMethod = 'PUT';
        req.requestBody = Blob.valueOf(REQ_BODY_ERROR_NO_ID);

        RestContext.request = req;
        RestContext.response= res;

        CORE_AOL_Wrapper.ResultWrapperGlobal result = CORE_AOL_OpportunityFeesPaidEnrollement.execute();

        System.assertEquals('KO', result.status);
    }

    @IsTest
    private static void execute_should_return_an_error_if_invalid_externalID(){
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/aol/opportunity/fees-paid/enrollement';

        req.httpMethod = 'PUT';
        req.requestBody = Blob.valueOf(REQ_BODY_ERROR_INVALID_ID);

        RestContext.request = req;
        RestContext.response= res;

        CORE_AOL_Wrapper.ResultWrapperGlobal result = CORE_AOL_OpportunityFeesPaidEnrollement.execute();

        System.assertEquals('KO', result.status);
    }

    @IsTest
    private static void execute_should_return_an_error_if_something_goes_wrong(){
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/aol/opportunity/fees-paid/enrollement';

        req.httpMethod = 'PUT';
        req.requestBody = Blob.valueOf(REQ_BODY_ERROR);

        RestContext.request = req;
        RestContext.response= res;

        CORE_AOL_Wrapper.ResultWrapperGlobal result = CORE_AOL_OpportunityFeesPaidEnrollement.execute();

        System.assertEquals('KO', result.status);
    }

       @IsTest
    private static void execute_should_return_a_success_if_all_updates_are_ok(){
       RestRequest req = new RestRequest();
       RestResponse res = new RestResponse();
       req.requestURI = '/services/apexrest/aol/opportunity/fees-paid/enrollement';

       req.httpMethod = 'PUT';
       req.requestBody = Blob.valueOf(REQ_BODY);

       RestContext.request = req;
       RestContext.response= res;

        CORE_AOL_Wrapper.ResultWrapperGlobal result = CORE_AOL_OpportunityFeesPaidEnrollement.execute();

        System.assertEquals('OK', result.status);
    }

}