global class CORE_Flow_AnonymizeText {

    @InvocableMethod
    global static List<String> anonymize(List<AnonymizeParameters> params){

        String anonimized = '';

        List<String> alphabetsList = new List<String>{
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'
        };

        Integer randomIndex;
        String alphabet;
        Integer currentIndex = alphabetsList.size();

        while (currentIndex != 0) {
            randomIndex = integer.valueOf(Math.floor(Math.random() * currentIndex));
            currentIndex -= 1;
            alphabet = alphabetsList[currentIndex];
            alphabetsList[currentIndex] = alphabetsList[randomIndex];
            alphabetsList[randomIndex] = alphabet;
        }

        for(Integer i = 0; i < params[0].textLength; i++){
            anonimized += alphabetsList[i];
        }

        return new List<String> { anonimized };
    }

    global class AnonymizeParameters {
        @InvocableVariable(required=true)
        global Integer textLength;
    }
}