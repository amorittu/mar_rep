/**
 * Created by Gabriele Vitanza on 02/11/2021
 * Test class: IT_ExtInfoComponentControllerTest
 */

public without sharing class IT_ExtInformationsComponentController {

    @AuraEnabled
    public static PageLayout getFieldsByLayout(String layoutName) {
        List<String> componentNameList = new List<String>{layoutName};

        List<Metadata.Metadata> layouts =
                Metadata.Operations.retrieve(Metadata.MetadataType.Layout, componentNameList);

        System.debug('layout retrived ' +layoutName + ' ' + layouts[0]);
        return new PageLayout((Metadata.Layout) layouts[0]);
    }


    public class PageLayout {
        @AuraEnabled
        public List<PageLayoutSection> Sections { get; set; }

        public PageLayout() {
            Sections = new List<PageLayoutSection>();
        }

        public PageLayout(Metadata.Layout layout) {
            this();

            for (Metadata.LayoutSection section : layout.layoutSections) {
                if (section.style != Metadata.LayoutSectionStyle.CustomLinks && section.detailHeading) {
                    Sections.add(new PageLayoutSection(section));
                }
            }
        }
    }

    public class PageLayoutSection {
        @AuraEnabled
        public List<PageLayoutSectionColumn> Columns { get; set; }

        @AuraEnabled
        public String Label { get; set; }

        public PageLayoutSection(Metadata.LayoutSection section) {
            Columns = new List<PageLayoutSectionColumn>();

            Label = section.label;

            for (Metadata.LayoutColumn column : section.layoutColumns) {
                Columns.add(new PageLayoutSectionColumn(column));
            }
        }
    }

    public class PageLayoutSectionColumn {
        @AuraEnabled
        public List<PageLayoutField> Fields { get; set; }

        public PageLayoutSectionColumn(Metadata.LayoutColumn column) {
            Fields = new List<PageLayoutField>();

            for (Metadata.LayoutItem item : column.layoutItems) {
                if(column.layoutItems != null) {
                    Fields.add(new PageLayoutField(item));
                }
            }
        }
    }

    public class PageLayoutField {
        @AuraEnabled
        public String APIName { get; set; }

        public PageLayoutField(Metadata.LayoutItem item) {
            APIName = item.field;
        }
    }

    @AuraEnabled
    public static List<IT_ExtendedInfo__c> getExtInformationsIds(Id recordId){
        List<IT_ExtendedInfo__c> itExtendedInfos = [
                SELECT Id, Name, IT_LastBusinessUnit__c, IT_LastBusinessUnit__r.Name, IT_InformationOfficer__c, IT_AdmissionOwner__c, IT_ScholarshipAwarded__c, IT_FirstContactDate__c, IT_IntakeOfinterest__c, IT_CustomerStatusHistorical__c, IT_CustomerSubStatusHistorical__c, IT_LastInboundcontactDate__c, IT_RequestOfScholarship__c, IT_StudyOfArea__c, IT_InterestedinaScholarship__c, IT_CustomerStatusActual__c, IT_CustomerSubStatusActual__c, IT_Brand__c, IT_StudyOfAreaName__c
                FROM IT_ExtendedInfo__c
                WHERE IT_Account__c = :recordId
        ];

        return itExtendedInfos;
    }

    @AuraEnabled
    public static Map<String,String> getLabelsFields(){
        Map<String,String> returnValue = IT_Utility.getLabelFromApiObject('IT_ExtendedInfo__c');


        return returnValue;
    }

    @AuraEnabled
    public static List<UserRecordAccess> getRecordsUserHasAccess(List<IT_ExtendedInfo__c> extInfoList ){



        List<Id> recordIdList = new List<Id>();
        for(IT_ExtendedInfo__c extInfo : extInfoList){
            recordIdList.add(extInfo.Id);
        }

        List<UserRecordAccess> UserRecordAccessList = [SELECT RecordId FROM UserRecordAccess WHERE UserId=:UserInfo.getUserId() AND HasReadAccess = true AND RecordId IN :recordIdList];

        return UserRecordAccessList;

    }

    @AuraEnabled
    public static WrapperPermission getWrapperMethod(){

        List<User> userList = [SELECT Id, UserRoleId, UserRole.DeveloperName FROM User WHERE UserRoleId != null AND Id = :UserInfo.getUserId()];



        WrapperPermission wPerm = null;

        if(userList.size()>0 && userList[0].UserRoleId!=null){
            Map<String,IT_Role_Type__mdt> getRoleType = IT_Utility.getRoleTypeMDT2();
            IT_Role_Type__mdt roleTypeMDT = getRoleType.get(userList[0].UserRole.DeveloperName);
            System.debug('getWrapper roleTypeMDT: ' + roleTypeMDT);
            if(roleTypeMDT!=null){
                wPerm = new WrapperPermission(roleTypeMDT);
            }
        }

        if(wPerm==null){
            wPerm = new WrapperPermission(null);
        }

        System.debug('getWrapper wPerm: ' + wPerm);
        return wPerm;

    }


    public class WrapperPermission{
        @AuraEnabled
        public Boolean isDomus {get;set;}
        @AuraEnabled
        public Boolean isMarangoni {get;set;}
        @AuraEnabled
        public Boolean isNABA {get;set;}


        public WrapperPermission(IT_Role_Type__mdt roleTypeMDT){
            String userProfileId = UserInfo.getProfileId();
            String sysAdminProfileId = System.Label.IT_IdSystemAdminProfile;
            System.debug('WrapperPermission userProfileId ' + userProfileId + ' sysAdminProfileId ' + sysAdminProfileId);
            if(userProfileId==sysAdminProfileId){
                this.isDomus = true;
                this.isMarangoni = true;
                this.isNABA = true;
            } else if(roleTypeMDT==null){
                this.isDomus = false;
                this.isMarangoni = false;
                this.isNABA = false;
            } else {
                this.isDomus = roleTypeMDT.IT_BrandDOMUS__c;
                this.isMarangoni = roleTypeMDT.IT_BrandMarangoni__c;
                this.isNABA = roleTypeMDT.IT_BrandNABA__c;
            }

        }
    }




}