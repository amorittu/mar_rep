//EScudo 27/01/22 -> test per mandare report a Agenzie esterne (servirebbe poi batch schedulato e rendere massivo l'invio)
public without sharing class IT_ReportExternalAgenciesController implements Schedulable{

	public static final String CLASS_NAME = IT_ReportExternalAgenciesController.class.getName();				// Amorittu 10/06/22 - Class Name variable
	public static final String[] ADRESSES_ADMINISTRATOR = new String[]{'escudo@atlantic-technologies.com'}; 	// Amorittu 10/06/22 - constant to set who receive the exception email
	
	/*
		SCHEDULE EVERY DAY AT 21:30
		String chron = '0 30 21 * * ?';
		System.schedule('Nightly Report Agencies Scheduler', chron, new IT_ReportExternalAgenciesController() );
	*/
	
	public void execute(SchedulableContext ctx) {
		generateReport();
	}

	//Generate report Based on parameter
	@future(callout=true)
	public static void generateReport() {

		try {

            Map<String, String> mapReportEmail = GetReportSettings();
			System.debug(CLASS_NAME + ' generateReport::mapReportEmail: ' + mapReportEmail);

			if (mapReportEmail.isEmpty()) {
				System.debug(CLASS_NAME + ' generateReport No setting defined');
				return;
			}

			List <Report> reportList = [SELECT Id, DeveloperName, Name FROM Report WHERE DeveloperName IN :mapReportEmail.keySet()];

			//List di email da inviare
			List<Messaging.SingleEmailMessage> mails = new List<Messaging.SingleEmailMessage>();

			if (reportList.size() > 0) {
				System.debug(CLASS_NAME + 'generateReport::reportList.size(): ' + reportList.size());

				for (Report report : reportList) {

					//Get Report Id
					String reportId = (String) report.get('Id');

					//Get Report Name
					String reportName = (String) report.get('Name');

					//Get Report DeveloperName
					String reportDevName = (String) report.get('DeveloperName');

					//Get Recipient Email
					List<String> emailRecipient = mapReportEmail.get(reportDevName).split(';');

					//Get instance Url
					String instanceName = Url.getSalesforceBaseUrl().toExternalForm();

					//Page to get report file
					String url = instanceName + '/servlet/PrintableViewDownloadServlet?isdtp=p1&reportId=' + reportId + '&excel=1';
					ApexPages.PageReference objPage = new ApexPages.PageReference(url);

					Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();

					//Attachment
					Messaging.EmailFileAttachment objMsgEmailAttach = new Messaging.EmailFileAttachment();
					objMsgEmailAttach.setFileName(reportName + '.xlsx');
					Blob pdfBody = Test.isRunningTest() ? Blob.valueOf('Unit.Test') : objPage.getContent();
					objMsgEmailAttach.setBody(pdfBody);
					objMsgEmailAttach.setContentType('application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');

					List<Messaging.EmailFileAttachment> attach = new List<Messaging.EmailFileAttachment>();
					attach.add(objMsgEmailAttach);
					email.setFileAttachments(attach);


					//email.setPlainTextBody('Hello, <br/><br/> Here is attached the report: ' + reportName + '.<br/><br/>Thank You.<br/>Admin');
					// Andrea Morittu 10/06/2022 - Set html format to print the body of email
					email.setHtmlBody('<!DOCTYPE html>'					+
					'<html>' 											+
						'<body>'										+
							'Hello, '									+
							'<br/>'										+
							'<br/> '									+
								'<div>Here is attached the report:' 	+ 
								'<h3>'									+
								reportName								+	
								'</h3>' 								+
								 '. </div>'								+	
							'<br/>'										+	
							'<br/>'										+
							'<div>Thank You.</div>'						+
							'<br/>'										+
							'Admin'										+
						'</body>'										+
					'</html>');			
					email.setToAddresses(emailRecipient);
					email.setUseSignature(false); // Andrea Morittu 10/06/2022 - Hide final signature of user
					email.setSenderDisplayName('Salesforce Admin');
					email.setSubject(reportName);

					/*EmailService service = new EmailService(email);
					service.body = 'Hello, <br/><br/> Here is attached the report: ' + reportName + '.<br/><br/>Thank You.<br/>Admin';
					service.isHtml = true;
					service.toAddresses = new List<String>();
					service.toAddresses.add(emailRecipient);
					//service.toAddresses.add('escudo@atlantic-technologies.com');
					//service.toAddresses.add('nrigatti@atlantic-technologies.com');
					service.displayName = 'Salesforce Admin';
					service.subject = reportName;
					service.sendMail();*/

					mails.add(email);
				}

				if(!Test.isRunningTest()) {
					Messaging.sendEmail(mails);
					System.debug(CLASS_NAME + 'mail has been fired');
				}
			}
		} catch (Exception ex) {
			System.debug(ex.getMessage());

			// Amorittu 10/06/22 START - Send an exception email to admin (see address inside )
			Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
			String [] toaddress= ADRESSES_ADMINISTRATOR;
			email.setSubject(CLASS_NAME + ' - Exception');
			email.setPlainTextBody( 'An error has occured inside ' + CLASS_NAME 
				+ '. The error is : ' + ex.getMessage() + ' at line number: ' + ex.getLineNumber() );
			email.setToAddresses(toaddress);
			Messaging.sendEmail(New Messaging.SingleEmailMessage[]{email});
			// Amorittu 10/06/22 END  - Send an exception email to admin (see address inside )

		}
	}

    public static Map<String, String> GetReportSettings(){

	    Map<String, String> mapReportEmail = new Map<String, String>();

	    List<IT_ManageReportExtAgencies__mdt> mdt = [
                SELECT DeveloperName, IT_ReportDeveloperName__c, IT_RecipientEmail__c
                FROM IT_ManageReportExtAgencies__mdt
                LIMIT 100
        ];

	    for (IT_ManageReportExtAgencies__mdt m : mdt) {
            mapReportEmail.put(m.IT_ReportDeveloperName__c, m.IT_RecipientEmail__c);
        }

        return mapReportEmail;
    }

}