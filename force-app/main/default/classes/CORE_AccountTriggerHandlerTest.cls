@IsTest
public class CORE_AccountTriggerHandlerTest {
    @TestSetup
    public static void makeData(){
        CORE_DataFaker_CatalogHierarchy catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test1');
        Account account = CORE_DataFaker_Account.getStudentAccountWithoutInsert('test1');
        account.CORE_LegacyCRMID__c = 'legacy1';
        account.CORE_SISStudentID__c = 'sis1';


        insert account;
        

        Opportunity opportunity = CORE_DataFaker_Opportunity.getOpportunity(catalogHierarchy.division.Id, 
            catalogHierarchy.businessUnit.Id, 
            catalogHierarchy.studyArea.Id,
            catalogHierarchy.curriculum.Id,
            catalogHierarchy.level.Id,
            catalogHierarchy.intake.Id, 
            account.Id, 
            'Lead', 
            'Lead - New'
        );
    }
    
    @IsTest
    public static void afterDelete_Should_Upsert_New_DeletedRecords() {
        List<Account> accounts = [Select Id, RecordTypeId, MasterRecordId, CORE_LegacyCRMID__c, CORE_SISStudentID__c,PersonContactId FROM Account];
        List<Opportunity> opportunities = [Select Id, RecordTypeId FROM Opportunity];
        Set<Id> recordTypeIdsOpportunity = new Set<Id> {opportunities.get(0).RecordTypeId};
        Set<Id> recordTypeIdsAccount = new Set<Id> {accounts.get(0).RecordTypeId};
        Map<Id, String> recordTypeDevNamesOpportunity = CORE_RecordTypeUtils.getDeveloperNameByIds('Opportunity', recordTypeIdsOpportunity);
        Map<Id, String> recordTypeDevNamesAccount = CORE_RecordTypeUtils.getDeveloperNameByIds('Account', recordTypeIdsAccount);
        


        Test.startTest();
        CORE_AccountTriggerHandler.afterDelete(accounts);
        Test.stopTest();

        List<CORE_Deleted_record__c> results = [Select Id, CORE_Object_Name__c, CORE_Record_Id__c, CORE_Record_Type__c, CORE_LegacyCRMID__c, CORE_SISStudentID__c,
            CORE_MasterId__c,CORE_MasterLegacyCRMID__c,CORE_ContactId__c, CORE_MasterSISStudentID__c FROM CORE_Deleted_record__c];
        System.assertEquals(2, results.size());
        
        System.debug('account contact '+ results.get(1).CORE_ContactId__c);

        System.assertEquals('CORE_PKL_Opportunity',results.get(0).CORE_Object_Name__c);
        System.assertEquals('CORE_PKL_Account',results.get(1).CORE_Object_Name__c);

        System.assertEquals(opportunities.get(0).Id, results.get(0).CORE_Record_Id__c);
        System.assertEquals(accounts.get(0).Id, results.get(1).CORE_Record_Id__c);
        System.assertEquals(accounts.get(0).CORE_LegacyCRMID__c, results.get(1).CORE_LegacyCRMID__c);
        System.assertEquals(accounts.get(0).CORE_SISStudentID__c, results.get(1).CORE_SISStudentID__c);
        System.assertEquals(null, results.get(1).CORE_MasterId__c);
        System.assertEquals(null, results.get(1).CORE_MasterLegacyCRMID__c);
        System.assertEquals(null, results.get(1).CORE_MasterSISStudentID__c);
        
        System.assertEquals(recordTypeDevNamesOpportunity.get(opportunities.get(0).RecordTypeId), results.get(0).CORE_Record_Type__c);
        System.assertEquals(recordTypeDevNamesAccount.get(accounts.get(0).RecordTypeId), results.get(1).CORE_Record_Type__c);
    }

    @IsTest
    public static void afterDelete_Should_Upsert_New_DeletedRecords_with_masterIds() {
        Account accountMaster = CORE_DataFaker_Account.getStudentAccountWithoutInsert('test2');
        accountMaster.CORE_LegacyCRMID__c = 'legacy2';
        accountMaster.CORE_SISStudentID__c = 'sis2';
        insert accountMaster;
        
        List<Account> accounts = [Select Id, RecordTypeId, MasterRecordId, CORE_LegacyCRMID__c, CORE_SISStudentID__c FROM Account where CORE_LegacyCRMID__c = 'legacy1'];

        List<Opportunity> opportunities = [Select Id, RecordTypeId FROM Opportunity];
        Set<Id> recordTypeIdsOpportunity = new Set<Id> {opportunities.get(0).RecordTypeId};
        Set<Id> recordTypeIdsAccount = new Set<Id> {accounts.get(0).RecordTypeId};
        Map<Id, String> recordTypeDevNamesOpportunity = CORE_RecordTypeUtils.getDeveloperNameByIds('Opportunity', recordTypeIdsOpportunity);
        Map<Id, String> recordTypeDevNamesAccount = CORE_RecordTypeUtils.getDeveloperNameByIds('Account', recordTypeIdsAccount);
        


        Test.startTest();
        merge accountMaster accounts;
        Test.stopTest();

        List<CORE_Deleted_record__c> results = [Select Id, CORE_Object_Name__c, CORE_Record_Id__c, CORE_Record_Type__c, CORE_LegacyCRMID__c, CORE_SISStudentID__c,
            CORE_MasterId__c,CORE_MasterLegacyCRMID__c, CORE_MasterSISStudentID__c FROM CORE_Deleted_record__c];

        //System.assertEquals(2, results.size());

        System.assertEquals('CORE_PKL_Account',results.get(0).CORE_Object_Name__c);

        System.assertEquals(accounts.get(0).Id, results.get(0).CORE_Record_Id__c);
        System.assertEquals(accounts.get(0).CORE_LegacyCRMID__c, results.get(0).CORE_LegacyCRMID__c);
        System.assertEquals(accounts.get(0).CORE_SISStudentID__c, results.get(0).CORE_SISStudentID__c);
        System.assertEquals(accountMaster.Id, results.get(0).CORE_MasterId__c);
        System.assertEquals(accountMaster.CORE_LegacyCRMID__c, results.get(0).CORE_MasterLegacyCRMID__c);
        System.assertEquals(accountMaster.CORE_SISStudentID__c, results.get(0).CORE_MasterSISStudentID__c);

        System.assertEquals(recordTypeDevNamesAccount.get(accounts.get(0).RecordTypeId), results.get(0).CORE_Record_Type__c);
    }
}