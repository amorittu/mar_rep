global class CORE_SalesAdmissionInboundEmail_Wrapper  {

	global class ResultWrapper {
		global Opportunity opportunity;

        global String status;
        global String message;
        
        global ResultWrapper(){}

		/*global ResultWrapper(Opportunity opportunity, String status){
            this.opportunity = opportunity;
            this.status = status;
		}*/

        global ResultWrapper(String status, String message){
            this.status = status;
            this.message = message;
		}
	}

	global class EmailWrapper extends CORE_ObjectWrapper {
        global String fromName;
		global String fromEmail;
		global String[] toEmail;
		global String[] ccEmails;
        global String subject;  
		global String htmlBody; 
		global String plainTextBody;
		global String emailDatetime;
		global String[] headers;
		global String messageID;
		global String[] inReplyTo;
		global String[] attachments;
    }
}