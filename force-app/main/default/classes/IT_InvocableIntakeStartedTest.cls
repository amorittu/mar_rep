/**
 * Created by Gabriele Vitanza on 11/01/2022.
 */

@IsTest
private class IT_InvocableIntakeStartedTest {
    @IsTest
    static void testBehavior() {
        CORE_DataFaker_CatalogHierarchy catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test1');

        IT_InvocableIntakeStarted.FlowInputs flowInput = new IT_InvocableIntakeStarted.FlowInputs();
        flowInput.intake = catalogHierarchy.intake;
        flowInput.academicYearStartMonth = '8';

        List<IT_InvocableIntakeStarted.FlowInputs> flowInputs = new List<IT_InvocableIntakeStarted.FlowInputs>{flowInput};
        IT_InvocableIntakeStarted.getNextBH(flowInputs);
    }
}