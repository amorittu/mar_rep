/**
 * Created by dgagliardi on 19/05/2022.
 */

@IsTest
private class IT_BatchExtendedInfoTaskTest {
	@TestSetup
	static void testSetup() {
		
		IT_UtilSetting__c usett = new IT_UtilSetting__c();
		usett.IT_DaysSystemModeBatch__c = 7;
		insert usett;

		CORE_DataFaker_CatalogHierarchy catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test1');
		Account account = CORE_DataFaker_Account.getStudentAccount('test1');
		Opportunity opportunity = CORE_DataFaker_Opportunity.getOpportunity(
				catalogHierarchy.division.Id,
				catalogHierarchy.businessUnit.Id,
				account.Id,
				'Applicant',
				'Applicant - Application Submitted / New'
		);

		opportunity.CORE_OPP_Intake__c = catalogHierarchy.intake.Id;
		opportunity.CORE_OPP_Curriculum__c = catalogHierarchy.curriculum.Id;
		opportunity.CORE_OPP_Study_Area__c = catalogHierarchy.studyArea.Id;
		opportunity.CORE_OPP_Level__c = catalogHierarchy.level.Id;
		opportunity.IT_AdmissionOfficer__c = UserInfo.getUserId();
		update opportunity;

		IT_DataFaker_Objects.createExtendedInfo(account.Id, opportunity.CORE_OPP_Business_Unit__r.CORE_Brand__c, true);

		Id rtId = Schema.SObjectType.Task.getRecordTypeInfosByDeveloperName().get('CORE_Continuum_of_action').getRecordTypeId();

		User u = [SELECT Id, UserType FROM User WHERE Id =: UserInfo.getUserId()];
		Task t = new Task();
		t.OwnerId = u.Id;
		t.WhatId = opportunity.Id;
		t.RecordTypeId = rtId;
		t.Status = 'CORE_PKL_Completed';
		t.CORE_TECH_CallTaskType__c = 'CORE_PKL_ProspectionCall';
		t.Core_Task_Result__c = 'IT_PKL_Reached_Enrolled';
		insert t;

	}

	@IsTest
	static void testBehaviorRunNext() {

		Test.startTest();
		IT_BatchExtendedInfoTaskCreated b = new IT_BatchExtendedInfoTaskCreated(true);
		Database.executeBatch(b);
		Test.stopTest();

	}

	@IsTest
	static void testBehavior() {

		Test.startTest();
		IT_BatchExtendedInfoTaskCreated b = new IT_BatchExtendedInfoTaskCreated();
		Database.executeBatch(b);
		Test.stopTest();

	}

	@IsTest
	static void testBehaviorLastModified() {

		Test.startTest();
		IT_BatchExtendedInfoTaskLastModified b = new IT_BatchExtendedInfoTaskLastModified();
		Database.executeBatch(b);
		Test.stopTest();

	}
    
    @IsTest
	static void testBehaviorRunNextAndAccount() {
		
        Account[] account = [SELECT Id FROM Account LIMIT 1];
        
		Test.startTest();
		IT_BatchExtendedInfoTaskCreated b = new IT_BatchExtendedInfoTaskCreated(true, new String [] {account.get(0).Id}); 
		Database.executeBatch(b);
		Test.stopTest();

	}
}