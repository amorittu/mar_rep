@IsTest
public with sharing class CORE_SIS_Opp_StatusEnrolled5Test {
    private static final String SIS_OPPORTUNITYID = 'setupSis';

    @TestSetup
    static void makeData(){
        CORE_CatalogHierarchyModel catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test').catalogHierarchy;
        Account account = CORE_DataFaker_Account.getStudentAccount('test');

        Opportunity  opportunity = CORE_DataFaker_Opportunity.getSisOpportunity(
            catalogHierarchy.division.Id, 
            catalogHierarchy.businessUnit.Id,
            catalogHierarchy.studyArea.Id, 
            catalogHierarchy.curriculum.Id, 
            catalogHierarchy.level.Id,
            catalogHierarchy.intake.Id,
            account.Id, 
            CORE_AOL_Constants.PKL_OPP_STAGENAME_PROSPECT, 
            CORE_AOL_Constants.PKL_OPP_SUBSTATUS_PROSPECTAPPSTARTED, 
            SIS_OPPORTUNITYID
        );
    }

    @IsTest
    private static void execute_should_return_an_error_if_all_upsert_are_not_ok(){
   

        CORE_SIS_Wrapper.ResultWrapperGlobal result = CORE_SIS_Opp_StatusEnrolled5.execute(null);

        System.assertEquals('KO', result.status);
        
        result = CORE_SIS_Opp_StatusEnrolled5.execute('ABC53');

        System.assertEquals('KO', result.status);

       
    }

    @IsTest
    private static void execute_should_return_a_success_if_all_upsert_are_ok(){
        Opportunity opp = [Select Id, StageName, CORE_OPP_Sub_Status__c from Opportunity];

        System.assertNotEquals(CORE_SIS_Constants.PKL_OPP_STAGENAME_ENROLLED, opp.StageName);
        System.assertNotEquals(CORE_SIS_Constants.PKL_OPP_SUBSTATUS_ENROLLED_EXIT_AFTER_COURSE_START_WITHDRAWAL_PERIOD, opp.CORE_OPP_Sub_Status__c);

        CORE_SIS_Wrapper.ResultWrapperGlobal result = CORE_SIS_Opp_StatusEnrolled5.execute(SIS_OPPORTUNITYID);

        System.assertEquals('OK', result.status);
        opp = [Select Id, StageName, CORE_OPP_Sub_Status__c from Opportunity];

        System.assertEquals(CORE_SIS_Constants.PKL_OPP_STAGENAME_ENROLLED, opp.StageName);
        System.assertEquals(CORE_SIS_Constants.PKL_OPP_SUBSTATUS_ENROLLED_EXIT_AFTER_COURSE_START_WITHDRAWAL_PERIOD, opp.CORE_OPP_Sub_Status__c);
    }
}