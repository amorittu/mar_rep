global class CORE_Flow_CompareOppStatusWithBUStatus {

    @InvocableMethod
    global static List<Boolean> compareStatusLevel(List<OppParameters> params) {

        if (params != null) {

            try {
                Map<String, Decimal> statusLevelMap = getOppStatusLevels();

                Decimal oppStatusLevel = statusLevelMap.get(params[0].oppStatus);
                Decimal buStatusLevel = statusLevelMap.get(params[0].buStatus);

                if(oppStatusLevel >= buStatusLevel){
                    return new List<Boolean> { true };
                }
                else{
                    return new List<Boolean> { false };
                }
            } catch (Exception e) {
                system.debug('Error: ' + e.getMessage() + ' => ' + e.getStackTraceString());

                throw new OppException('Error: ' + e.getMessage() + ' => ' + e.getStackTraceString());
            }
        }

        return null;
    }

    public static Map<String, Decimal> getOppStatusLevels(){
        Map<String, Decimal> statusLevelMap = new Map<String, Decimal>();

        for(OpportunityStage oppStatus : [SELECT Id, DefaultProbability, ApiName, MasterLabel FROM OpportunityStage WHERE IsActive = TRUE]){
            statusLevelMap.put(oppStatus.ApiName, oppStatus.DefaultProbability);
        }

        system.debug('@getOppStatusLevels > statusLevelMap: ' + JSON.serializePretty(statusLevelMap));

        return statusLevelMap;
    }

    global class OppParameters {

        @InvocableVariable(required=true)
        global String oppStatus;

        @InvocableVariable(required=true)
        global String buStatus;

    }

    class OppException extends Exception {}
}