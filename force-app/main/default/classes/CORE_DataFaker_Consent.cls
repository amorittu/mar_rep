@IsTest
public class CORE_DataFaker_Consent {
    public CORE_Consent__c Initiate_Consent(CORE_CatalogHierarchyModel catalogHierarchy, Id accountId, Id divisionId,
    Id businessUnitId, String type, String channel, Boolean optOut, Boolean optIn, String scope){
        CORE_Consent__c consent = new CORE_Consent__c();
        consent.CORE_Person_Account__c = accountId;
        consent.CORE_Division__c = divisionId == null ? catalogHierarchy.division.Id : divisionId;
        consent.CORE_Business_Unit__c = businessUnitId == null ? catalogHierarchy.businessUnit.Id : businessUnitId;
        consent.CORE_Consent_type__c = type;
        consent.CORE_Consent_channel__c = channel;
        consent.CORE_Opt_out__c = optOut;
        consent.CORE_Opt_in__c = optIn;
        consent.CORE_Scope__c = scope;

        return consent;
    }
}