/**
* @author Fodil BOUDJEDIEN - Almavia
* @date 2022-03-25
* @group Opportunity Mass re-registration (Single) et (Massive)
* @description Test class of CORE_MassOpportunityReopeningBatch ;
* @test class 
*/
@isTest
public with sharing class CORE_MassOpportunityReopeningBatchTest {
    @TestSetup
    static void makeData(){
        CORE_DataFaker_CatalogHierarchy catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test');
        Account account = CORE_DataFaker_Account.getStudentAccount('test');
        Opportunity opp = CORE_DataFaker_Opportunity.getOpportunity(catalogHierarchy.division.Id, 
            catalogHierarchy.businessUnit.Id, 
            catalogHierarchy.studyArea.Id,
            catalogHierarchy.curriculum.Id,
            catalogHierarchy.level.Id,
            catalogHierarchy.intake.Id,
            account.Id, 
            'Registered', 
            'Registered - Classical'
        );
        opp.CORE_OPP_Academic_Year__c = 'CORE_PKL_2021-2022';
        update opp;

        CORE_OpportunityDataMaker dataMaker = new CORE_OpportunityDataMaker();
        String captureChannel = 'CORE_PKL_Emailings';
        String nature = 'CORE_PKL_Online';
        String origin = 'Website';
        CORE_Point_of_Contact__c result = dataMaker.initPocDefaultValues(opp,nature,captureChannel,origin);
        insert result;

        // insert custom settings
        MassOpportunityReregistration__c settings = new MassOpportunityReregistration__c(SetupOwnerId = Userinfo.getOrganizationId());
        settings.Job_in_Progress__c=false;
        settings.ListView_Ending__c='';
        settings.Synchronous_Batch__c=200;
        settings.Batch_Size__c=200;
        insert settings;
    }

    @isTest 
    public static void testLaunchBatch(){
        CORE_BUSINESS_UNIT__c  bu = [SELECT Id FROM CORE_BUSINESS_UNIT__c Limit 1];
        Test.startTest();
        Database.executeBatch(new CORE_MassOpportunityReopeningBatch('CORE_PKL_2021-2022', bu.Id,'' ));
        Test.stopTest();
        List<Opportunity> reregisteredOpportunities = [SELECT Id FROM Opportunity where CORE_Re_registered_opportunity__c = null];
        List<CORE_Point_of_Contact__c> pointsOfContact= [SELECT Id FROM CORE_Point_of_Contact__c];
        System.assertEquals(reregisteredOpportunities.size(), 1);
        System.assertEquals(pointsOfContact.size(), 2);
    }

}