public without sharing class CORE_LFA_OpportunityManagement {
    public enum OpportunityType {MAIN_INTEREST, SECOND_INTEREST, THIRD_INTEREST}

    public static Boolean isExistingInterestValues(CORE_LeadAcquisitionBuffer__c bufferRecord, OpportunityType opportunityType){
        Boolean isExistingValue = false;

        switch on opportunityType {
            when MAIN_INTEREST {
                if(!String.IsEmpty(bufferRecord.CORE_MI_OPPTDivision__c) ||
                    !String.IsEmpty(bufferRecord.CORE_MI_OPPTBusiness_Unit__c) ||
                    !String.IsEmpty(bufferRecord.CORE_MI_OPPTStudy_Area__c)){
                        isExistingValue = true;
                }
            }
            when SECOND_INTEREST {
                if(!String.IsEmpty(bufferRecord.CORE_SI_OPPTDivision__c) ||
                    !String.IsEmpty(bufferRecord.CORE_SI_OPPTBusiness_Unit__c) ||
                    !String.IsEmpty(bufferRecord.CORE_SI_OPPTStudy_Area__c)){
                        isExistingValue = true;
                }
            }
            when THIRD_INTEREST {
                if(!String.IsEmpty(bufferRecord.CORE_TI_OPPTDivision__c) ||
                    !String.IsEmpty(bufferRecord.CORE_TI_OPPTBusiness_Unit__c) ||
                    !String.IsEmpty(bufferRecord.CORE_TI_OPPTStudy_Area__c)){
                        isExistingValue = true;
                }
            }
        }

        return isExistingValue;
    }
    public static CORE_LeadFormAcquisitionWrapper.OpportunityWrapper getOpportunityWrapperFromBufferRecord(CORE_LeadAcquisitionBuffer__c bufferRecord, OpportunityType opportunityType){
        CORE_LeadFormAcquisitionWrapper.OpportunityWrapper opportunityWrapper = new CORE_LeadFormAcquisitionWrapper.OpportunityWrapper();
        switch on opportunityType {
            when MAIN_INTEREST {
                opportunityWrapper.academicYear = bufferRecord.CORE_MI_OPPAcademicYear__c;
                opportunityWrapper.division = bufferRecord.CORE_MI_OPPTDivision__c;
                opportunityWrapper.businessUnit = bufferRecord.CORE_MI_OPPTBusiness_Unit__c;
                opportunityWrapper.studyArea = bufferRecord.CORE_MI_OPPTStudy_Area__c;
                opportunityWrapper.curriculum = bufferRecord.CORE_MI_OPPTCurriculum__c;
                opportunityWrapper.level = bufferRecord.CORE_MI_OPPTLevel__c;
                opportunityWrapper.intake = bufferRecord.CORE_MI_OPPTIntake__c;
                opportunityWrapper.applicationFormShippedByMail = bufferRecord.CORE_MI_ApplicationShippedByMail__c;
                opportunityWrapper.brochureShippedByMail = bufferRecord.CORE_MI_BrochureShippedByMail__c;
                opportunityWrapper.isSuspect = bufferRecord.CORE_MI_IsSuspect__c == null ? false : bufferRecord.CORE_MI_IsSuspect__c;
                opportunityWrapper.leadSourceExternalId = bufferRecord.CORE_MI_LeadSourceExternalId__c;
                opportunityWrapper.learningMaterial = bufferRecord.CORE_MI_LearningMaterial__c;
                opportunityWrapper.applicationOnlineId = bufferRecord.CORE_MI_AOL_ExternalId__c;
                opportunityWrapper.isAolRetailRegistered = bufferRecord.CORE_MI_AOL_IsRetailRegistered__c  == null ? false : bufferRecord.CORE_MI_AOL_IsRetailRegistered__c;
                opportunityWrapper.funding = bufferRecord.CORE_MI_OPP_Funding__c;
                opportunityWrapper.thirdPartyId = bufferRecord.CORE_MI_Retail_Agency__c;
                if(!String.isBlank(bufferRecord.CORE_MI_OnlineShop_ExternalId__c)){
                    opportunityWrapper.onlineShopId = bufferRecord.CORE_MI_OnlineShop_ExternalId__c;
                    opportunityWrapper.product = bufferRecord.CORE_MI_Product__c;
                    opportunityWrapper.salesPrice = bufferRecord.CORE_MI_Sales_Price__c;
                    opportunityWrapper.quantity = bufferRecord.CORE_MI_Quantity__c;
                }
            }
            when SECOND_INTEREST {
                opportunityWrapper.academicYear = bufferRecord.CORE_SI_OPPAcademicYear__c;
                opportunityWrapper.division = bufferRecord.CORE_SI_OPPTDivision__c;
                opportunityWrapper.businessUnit = bufferRecord.CORE_SI_OPPTBusiness_Unit__c;
                opportunityWrapper.studyArea = bufferRecord.CORE_SI_OPPTStudy_Area__c;
                opportunityWrapper.curriculum = bufferRecord.CORE_SI_OPPTCurriculum__c;
                opportunityWrapper.level = bufferRecord.CORE_SI_OPPTLevel__c;
                opportunityWrapper.intake = bufferRecord.CORE_SI_OPPTIntake__c;
                opportunityWrapper.applicationFormShippedByMail = bufferRecord.CORE_SI_ApplicationShippedByMail__c;
                opportunityWrapper.brochureShippedByMail = bufferRecord.CORE_SI_BrochureShippedByMail__c;
                opportunityWrapper.isSuspect = bufferRecord.CORE_SI_IsSuspect__c == null ? false : bufferRecord.CORE_SI_IsSuspect__c;
                opportunityWrapper.leadSourceExternalId = bufferRecord.CORE_SI_LeadSourceExternalId__c;
                opportunityWrapper.learningMaterial = bufferRecord.CORE_SI_LearningMaterial__c;
                opportunityWrapper.applicationOnlineId = bufferRecord.CORE_SI_AOL_ExternalId__c;
                opportunityWrapper.isAolRetailRegistered = bufferRecord.CORE_SI_AOL_IsRetailRegistered__c  == null ? false : bufferRecord.CORE_SI_AOL_IsRetailRegistered__c;
                opportunityWrapper.funding = bufferRecord.CORE_SI_OPP_Funding__c;
                opportunityWrapper.onlineShopId = bufferRecord.CORE_SI_OnlineShop_ExternalId__c;
            }
            when THIRD_INTEREST {
                opportunityWrapper.academicYear = bufferRecord.CORE_TI_OPPAcademicYear__c;
                opportunityWrapper.division = bufferRecord.CORE_TI_OPPTDivision__c;
                opportunityWrapper.businessUnit = bufferRecord.CORE_TI_OPPTBusiness_Unit__c;
                opportunityWrapper.studyArea = bufferRecord.CORE_TI_OPPTStudy_Area__c;
                opportunityWrapper.curriculum = bufferRecord.CORE_TI_OPPTCurriculum__c;
                opportunityWrapper.level = bufferRecord.CORE_TI_OPPTLevel__c;
                opportunityWrapper.intake = bufferRecord.CORE_TI_OPPTIntake__c;
                opportunityWrapper.applicationFormShippedByMail = bufferRecord.CORE_TI_ApplicationShippedByMail__c;
                opportunityWrapper.brochureShippedByMail = bufferRecord.CORE_TI_BrochureShippedByMail__c;
                opportunityWrapper.isSuspect = bufferRecord.CORE_TI_IsSuspect__c == null ? false : bufferRecord.CORE_TI_IsSuspect__c;
                opportunityWrapper.leadSourceExternalId = bufferRecord.CORE_TI_LeadSourceExternalId__c;
                opportunityWrapper.learningMaterial = bufferRecord.CORE_TI_LearningMaterial__c;
                opportunityWrapper.applicationOnlineId = bufferRecord.CORE_TI_AOL_ExternalId__c;
                opportunityWrapper.isAolRetailRegistered = bufferRecord.CORE_TI_AOL_IsRetailRegistered__c  == null ? false : bufferRecord.CORE_TI_AOL_IsRetailRegistered__c;
                opportunityWrapper.funding = bufferRecord.CORE_TI_OPP_Funding__c;
                opportunityWrapper.onlineShopId = bufferRecord.CORE_TI_OnlineShop_ExternalId__c;
            }
        }
        
        return opportunityWrapper;
    }

    public static CORE_LeadFormAcquisitionWrapper.PointOfContactWrapper getPocWrapperFromBufferRecord(CORE_LeadAcquisitionBuffer__c bufferRecord){
        CORE_LeadFormAcquisitionWrapper.PointOfContactWrapper pocWrapper = new CORE_LeadFormAcquisitionWrapper.PointOfContactWrapper();
        
        pocWrapper.creationDate = bufferRecord.CORE_POC_CreatedDate__c != null ? String.valueOf(bufferRecord.CORE_POC_CreatedDate__c.getTime()) : null;
        pocWrapper.sourceId = bufferRecord.CORE_POC_SourceId__c;
        pocWrapper.nature = bufferRecord.CORE_POC_Nature__c;
        pocWrapper.captureChannel = bufferRecord.CORE_POC_CaptureChannel__c;
        pocWrapper.origin = bufferRecord.CORE_POC_Origin__c;
        pocWrapper.salesOrigin = bufferRecord.CORE_POC_SalesOrigin__c;
        pocWrapper.campaignMedium = bufferRecord.CORE_POC_CampaignMedium__c;
        pocWrapper.campaignSource = bufferRecord.CORE_POC_CampaignSource__c;
        pocWrapper.campaignTerm = bufferRecord.CORE_POC_CampaignTerm__c;
        pocWrapper.campaignContent = bufferRecord.CORE_POC_CampaignContent__c;
        pocWrapper.campaignName = bufferRecord.CORE_POC_CampaignName__c;
        pocWrapper.device = bufferRecord.CORE_POC_Device__c;
        pocWrapper.firstVisitDate = bufferRecord.CORE_POC_FirstVisit__c;
        pocWrapper.firstPage = bufferRecord.CORE_POC_FirstPage__c;
        pocWrapper.ipAddress = bufferRecord.CORE_POC_IPAddress__c;
        pocWrapper.latitute = bufferRecord.CORE_POC_Latitude__c;
        pocWrapper.longitude = bufferRecord.CORE_POC_Longitude__c;
        pocWrapper.proactivePrompt = bufferRecord.CORE_POC_Proactive_Prompt__c;
        pocWrapper.proactiveEngaged = bufferRecord.CORE_POC_Proactive_Engaged__c;
        pocWrapper.chatWith = bufferRecord.CORE_POC_Chat_With__c;
        pocWrapper.conversionPage = bufferRecord.CORE_POC_Conversion_page__c;
        pocWrapper.gclid = bufferRecord.CORE_GCLID__c;
        pocWrapper.form = bufferRecord.CORE_Form__c;
        pocWrapper.formArea = bufferRecord.CORE_Form_area__c;
        pocWrapper.cityGeoloc = bufferRecord.CORE_POC_City_Geoloc__c;
        pocWrapper.countryGeoloc = bufferRecord.CORE_POC_Country_Geoloc__c;
        pocWrapper.stateGeoloc = bufferRecord.CORE_POC_State_Geoloc__c;
        return pocWrapper;
    }

    public static Map<Id, CatalogBuffer> getCatalogHierarchiesFromBufferRecords(List<CORE_LeadAcquisitionBuffer__c> bufferRecords){
        List<CatalogBuffer> catalogBuffers = new List<CatalogBuffer> ();
        Map<Id, CatalogBuffer> result = new Map<Id, CatalogBuffer>();

        for(CORE_LeadAcquisitionBuffer__c bufferRecord : bufferRecords){
            CatalogBuffer catalogBuffer = new CatalogBuffer();
            catalogBuffer.bufferRecordId = bufferRecord.Id;
            //if(!String.isEmpty(bufferRecord.CORE_MI_OPPTBusiness_Unit__c) && !String.isEmpty(bufferRecord.CORE_MI_OPPTStudy_Area__c)){
            if(!String.isEmpty(bufferRecord.CORE_MI_OPPTBusiness_Unit__c)){
                catalogBuffer.mainInterest = getCatalogHierarchyFromBufferRecord(bufferRecord, OpportunityType.MAIN_INTEREST);
            }
            //if(!String.isEmpty(bufferRecord.CORE_SI_OPPTBusiness_Unit__c) && !String.isEmpty(bufferRecord.CORE_SI_OPPTStudy_Area__c)){
            if(!String.isEmpty(bufferRecord.CORE_SI_OPPTBusiness_Unit__c)){
                catalogBuffer.secondInterest = getCatalogHierarchyFromBufferRecord(bufferRecord, OpportunityType.SECOND_INTEREST);
            }
            //if(!String.isEmpty(bufferRecord.CORE_TI_OPPTBusiness_Unit__c) && !String.isEmpty(bufferRecord.CORE_TI_OPPTStudy_Area__c)){
            if(!String.isEmpty(bufferRecord.CORE_TI_OPPTBusiness_Unit__c)){
                catalogBuffer.thirdInterest = getCatalogHierarchyFromBufferRecord(bufferRecord, OpportunityType.THIRD_INTEREST);
            }

            catalogBuffers.add(catalogBuffer);
            //System.debug('catalogBuffer.bufferRecordId = ' + catalogBuffer.bufferRecordId );
            //System.debug('catalogBuffer.mainInterest = ' + catalogBuffer.mainInterest );
            //System.debug('catalogBuffer.secondInterest = ' + catalogBuffer.secondInterest );
            //System.debug('catalogBuffer.thirdInterest = ' + catalogBuffer.thirdInterest );
        }

        catalogBuffers = CORE_OpportunityDataMaker.getCatalogHierarchies(catalogBuffers);

        for(CatalogBuffer catalogBuffer : catalogBuffers){
            //System.debug('current catalogBuffer.bufferRecordId = ' + catalogBuffer.bufferRecordId );
            //System.debug('current catalogBuffer.mainInterest = ' + catalogBuffer.mainInterest );
            //System.debug('current catalogBuffer.thirdInterest = ' + catalogBuffer.thirdInterest );
            result.put(catalogBuffer.bufferRecordId,catalogBuffer);
        }

        return result;
    }


    public static CORE_CatalogHierarchyModel getCatalogHierarchyFromBufferRecord(CORE_LeadAcquisitionBuffer__c bufferRecord, OpportunityType opportunityType){
        CORE_CatalogHierarchyModel catalogHierarchy = new CORE_CatalogHierarchyModel();
        catalogHierarchy.useExternalIds = false;
        switch on opportunityType {
            when MAIN_INTEREST {
                catalogHierarchy.division.Id = bufferRecord.CORE_MI_OPPTDivision__c;
                catalogHierarchy.businessUnit.Id = bufferRecord.CORE_MI_OPPTBusiness_Unit__c;
                catalogHierarchy.studyArea.Id = bufferRecord.CORE_MI_OPPTStudy_Area__c;
                
                if(!String.isEmpty(bufferRecord.CORE_MI_OPPTCurriculum__c)){
                    catalogHierarchy.curriculum.Id = bufferRecord.CORE_MI_OPPTCurriculum__c;
                }
                if(!String.isEmpty(bufferRecord.CORE_MI_OPPTLevel__c)){
                    catalogHierarchy.level.Id = bufferRecord.CORE_MI_OPPTLevel__c;
                }
                if(!String.isEmpty(bufferRecord.CORE_MI_OPPTLevel__c)){
                    catalogHierarchy.intake.Id = bufferRecord.CORE_MI_OPPTIntake__c;
                }
                if(!String.isEmpty(bufferRecord.CORE_MI_Product__c)){
                    catalogHierarchy.product.Id = bufferRecord.CORE_MI_Product__c;
                }
            }
            when SECOND_INTEREST {
                catalogHierarchy.division.Id = bufferRecord.CORE_SI_OPPTDivision__c;
                catalogHierarchy.businessUnit.Id = bufferRecord.CORE_SI_OPPTBusiness_Unit__c;
                catalogHierarchy.studyArea.Id = bufferRecord.CORE_SI_OPPTStudy_Area__c;
                
                if(!String.isEmpty(bufferRecord.CORE_SI_OPPTCurriculum__c)){
                    catalogHierarchy.curriculum.Id = bufferRecord.CORE_SI_OPPTCurriculum__c;
                }
                if(!String.isEmpty(bufferRecord.CORE_SI_OPPTLevel__c)){
                    catalogHierarchy.level.Id = bufferRecord.CORE_SI_OPPTLevel__c;
                }
                if(!String.isEmpty(bufferRecord.CORE_SI_OPPTIntake__c)){
                    catalogHierarchy.intake.Id = bufferRecord.CORE_SI_OPPTIntake__c;
                }
            }
            when THIRD_INTEREST {
                catalogHierarchy.division.Id = bufferRecord.CORE_TI_OPPTDivision__c;
                catalogHierarchy.businessUnit.Id = bufferRecord.CORE_TI_OPPTBusiness_Unit__c;
                catalogHierarchy.studyArea.Id = bufferRecord.CORE_TI_OPPTStudy_Area__c;
                
                if(!String.isEmpty(bufferRecord.CORE_TI_OPPTCurriculum__c)){
                    catalogHierarchy.curriculum.Id = bufferRecord.CORE_TI_OPPTCurriculum__c;
                }
                if(!String.isEmpty(bufferRecord.CORE_TI_OPPTLevel__c)){
                    catalogHierarchy.level.Id = bufferRecord.CORE_TI_OPPTLevel__c;
                }
                if(!String.isEmpty(bufferRecord.CORE_TI_OPPTIntake__c)){
                    catalogHierarchy.intake.Id = bufferRecord.CORE_TI_OPPTIntake__c;
                }
            }
        }

        return catalogHierarchy;
    }

    public static Opportunity getMainOpportunityFromAcademicYear(String academicYear, List<Opportunity> opportunities){      
        for(Opportunity opportunity: opportunities){
            if(opportunity.CORE_OPP_Academic_Year__c == academicYear){
                return opportunity;
            }
        }

        return null;      
    }

    public static List<Opportunity> getMainOpportunityFromBusinessUnit(Id businessUnitId, List<Opportunity> opportunities){
        List<Opportunity> results = new List<Opportunity>();
        
        for(Opportunity opportunity: opportunities){
            if(opportunity.CORE_OPP_Business_Unit__c == businessUnitId){
                results.add(opportunity);
            }
        }

        return results;
    }


    public class CatalogBuffer {
        public Id bufferRecordId;
        public CORE_CatalogHierarchyModel mainInterest;
        public CORE_CatalogHierarchyModel secondInterest;
        public CORE_CatalogHierarchyModel thirdInterest;
    }

    public class OpportunityUpserted{
        public Id bufferRecordId;
        public Id oppId;
        public OpportunityType type;
    }
}