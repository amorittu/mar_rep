/**
 * Created by ftrapani on 21/10/2021.
 */
global without sharing class IT_InvocableCreateSendEmailDirectFlow {
    @InvocableMethod
    global static List<FlowOutputs> getOutputs(List<FlowInputs> request){
        System.debug('START IT_InvocableCreateSendEmailDirectFlow');
        try{
            System.debug('IT_InvocableCreateSendEmailDirectFlow request ' + request);

            FlowInputs flowInput = request!=null && request[0]!=null ? request[0] : null;
            
            //EScudo, 03/11/21: changed Opportunity with OpportunityId
            if(flowInput!=null && String.isNotBlank(flowInput.templateName) && flowInput.OpportunityId!=null){
                Map<String,String> senderEmailMap = IT_Utility.getSenderEmail();
                Opportunity opportunityRecord = [SELECT Id, AccountId, Account.PersonEmail, Account.PersonContactId, CORE_OPP_Business_Unit__r.CORE_Brand__c FROM Opportunity WHERE Id = :flowInput.opportunityId];

                String emailAddress = senderEmailMap.get(opportunityRecord.CORE_OPP_Business_Unit__r.CORE_Brand__c);
                System.debug('IT_InvocableCreateSendEmailDirectFlow opportunityRecord.IT_Brand__c ' + opportunityRecord.CORE_OPP_Business_Unit__r.CORE_Brand__c);
                List<OrgWideEmailAddress> oweaList = new List<OrgWideEmailAddress>();
                if(String.isNotBlank(emailAddress)){
                    oweaList  = [select Id from OrgWideEmailAddress where Address = :emailAddress];
                }

                Messaging.SingleEmailMessage message = new Messaging.SingleEmailMessage();
                message.setTargetObjectId(opportunityRecord.Account.PersonContactId);
                if(oweaList.size()>0){
                    message.setOrgWideEmailAddressId(oweaList[0].Id);
                } else {
                    message.setSenderDisplayName('Company Information');
                    message.setReplyTo('no-reply@company.com');
                }

                message.setUseSignature(false);
                message.setBccSender(false);
                message.setSaveAsActivity(true);
                EmailTemplate emailTemplate = [Select Id,Subject,Description,HtmlValue,DeveloperName,Body from EmailTemplate where DeveloperName = :flowInput.templateName];
                message.setTemplateID(emailTemplate.Id);
                message.setWhatId(opportunityRecord.Id); //This is important for the merge fields in template to work
                message.toAddresses = new String[] { opportunityRecord.Account.PersonEmail};
                Messaging.SingleEmailMessage[] messages = new List<Messaging.SingleEmailMessage> {message};
                Messaging.SendEmailResult[] results = Messaging.sendEmail(messages);

                if (results[0].success) {
                    System.debug('IT_InvocableCreateSendEmailDirectFlow The email was sent successfully.');
                } else {
                    System.debug('IT_InvocableCreateSendEmailDirectFlow The email failed to send: ' +  results[0].errors[0].message);
                }

            }
        } catch (Exception ex){
            System.debug('IT_InvocableCreateSendEmailDirectFlow exception ' + ex.getStackTraceString());
        }


        return null;
    }

    global class FlowInputs{

        @InvocableVariable (required=false)
        global String templateName;
//test escudo, switch to Id
        @InvocableVariable (required=false)
        global String opportunityId;

    }

    global class FlowOutputs{

        @InvocableVariable
        global Opportunity returnOpty;

    }
}