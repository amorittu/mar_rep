@RestResource(urlMapping='/core/consents')
global class CORE_WS_Consents {

    public static Integer limitPerPage = 2000;

    @HttpGet
    global static CORE_WS_Wrapper.ResultWrapperConsents getConsents(){

        String urlCallback = '';
        RestRequest	request = RestContext.request;
        system.debug('@getConsents > request: ' + request);
        String lastDate = request.params.get(CORE_WS_Constants.PARAM_LASTDATE);
        String lastId = request.params.get(CORE_WS_Constants.PARAM_LAST_ID);

        CORE_WS_Wrapper.ResultWrapperConsents result = checkMandatoryParameters(lastDate);
        system.debug('@getConsents > result: ' + result);
        if(result != null){
            return result;
        }

        try{
            List<CORE_Consent__c> consentsList = CORE_WS_QueryProvider.getConsentsListByDate(lastDate, lastId, limitPerPage);
            system.debug('@getConsents > consentsList: ' + JSON.serializePretty(consentsList));

            if(consentsList.size() == limitPerPage){
                urlCallback = URL.getOrgDomainUrl()+'/services/apexrest/sis/consents?'
                        + CORE_WS_Constants.PARAM_LASTDATE + '=' + lastDate
                        + '&' + CORE_WS_Constants.PARAM_LAST_ID + '=' + consentsList.get(consentsList.size() - 1).Id;
            }

            result = new CORE_WS_Wrapper.ResultWrapperConsents(consentsList.size(), urlCallback, consentsList, CORE_WS_Constants.SUCCESS_STATUS);

        } catch(Exception e){
            system.debug('@getConsents > Exception: ' + e.getMessage() + ' at ' + e.getStackTraceString());
            result = new CORE_WS_Wrapper.ResultWrapperConsents(CORE_WS_Constants.ERROR_STATUS, e.getMessage() + ' at ' + e.getStackTraceString());
        }

        return result;
    }

    private static CORE_WS_Wrapper.ResultWrapperConsents checkMandatoryParameters(String lastDate){
        List<String> missingParameters = new List<String>();
        Boolean isMissingLastDate = CORE_SIS_GlobalWorker.isParameterMissing(lastDate);

        if(isMissingLastDate){
            missingParameters.add(CORE_WS_Constants.PARAM_LASTDATE);
        }

        return getMissingParameters(missingParameters);
    }

    public static CORE_WS_Wrapper.ResultWrapperConsents getMissingParameters(List<String> missingParameters){
        String errorMsg = CORE_WS_Constants.MSG_MISSING_PARAMETERS;

        if(missingParameters.size() > 0){
            errorMsg += missingParameters;

            return new CORE_WS_Wrapper.ResultWrapperConsents(CORE_WS_Constants.ERROR_STATUS, errorMsg);
        }

        return null;
    }
}