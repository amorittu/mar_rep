@RestResource(urlMapping='/sis/account/*')
global class CORE_SIS_Account_Update {

    public static Set<String> overrideFieldsSet;//used mainly by test class for code coverage

    @HttpPut
    global static CORE_SIS_Wrapper.ResultWrapperAccount updateAccount(){

        RestRequest	request = RestContext.request;

        // Initiating logs
        Long dt1 = DateTime.now().getTime();
        Long dt2;
        CORE_LogManager logSettings;
        CORE_LOG__c log = new CORE_LOG__c(CORE_API_Name__c = 'SIS');
        Boolean isErrorLog = false;
        logSettings = new CORE_LogManager('SIS');

        CORE_SIS_Wrapper.ResultWrapperAccount result = null;

        try{
            CORE_CountrySpecificSettings__c countrySetting = CORE_CountrySpecificSettings__c.getOrgDefaults();

            String paramId = request.requestURI.substringAfterLast('/');

            Map<String, Object> paramsMap = (Map<String, Object>)JSON.deserializeUntyped(request.requestBody.toString());

            Account acc = CORE_SIS_QueryProvider.getAccount(paramId, null, new List<String>());
            if(acc == null){
                result = new CORE_SIS_Wrapper.ResultWrapperAccount(CORE_SIS_Constants.ERROR_STATUS, 'Account not found');
				return result;
            }
            List<String> validFieldsSet = CORE_SIS_QueryProvider.ACC_QUERY_FIELDS.split(',');

            if(overrideFieldsSet != null){
                validFieldsSet.addAll(overrideFieldsSet);
            }

            for(String param : paramsMap.keySet()){
                if(validFieldsSet.contains(param)
                   || (countrySetting != null
                       && countrySetting.CORE_FieldPrefix__c != null
                       && countrySetting.CORE_FieldPrefix__c != CORE_SIS_Constants.PREFIX_CORE
                       && param.startsWith(countrySetting.CORE_FieldPrefix__c))){

                    acc = (Account)CORE_SIS_GlobalWorker.assignValue(acc, param, paramsMap.get(param));
                }
                else{
                    
                    result = new CORE_SIS_Wrapper.ResultWrapperAccount(CORE_SIS_Constants.ERROR_STATUS, CORE_SIS_Constants.MSG_INVALID_FIELDS + ': '+ param);
                    return result;
                }
            }

            database.update(acc, true);

            result = new CORE_SIS_Wrapper.ResultWrapperAccount(acc, CORE_SIS_Constants.SUCCESS_STATUS);

        } catch(Exception e){
            system.debug('@updateAccount > Exception error: ' + e.getMessage() + ' at ' + e.getStackTraceString());
            result = new CORE_SIS_Wrapper.ResultWrapperAccount(CORE_SIS_Constants.ERROR_STATUS, e.getMessage() + ' at ' + e.getStackTraceString());
        } finally{
            dt2 = DateTime.now().getTime();
            System.debug('CORE_AOL_AcademicDiplomaHistory(urlMapping=\'/aol/academic-diploma-history\') : T0TAL TIME OF EXECUTION = ' + (dt2-dt1) + ' ms');
            // creating log
            log.CORE_Processing_Duration__c =(dt2-dt1);
            log.CORE_HTTP_Status_Code__c = result.status == 'KO' ? 400 : result.status == 'OK' ? 200 : 500;
            log.CORE_Sent_Body__c = JSON.serialize(result);
            log.CORE_Received_Body__c =  request.requestBody.toString();
            
            if(!logSettings.isNone()){
                if(logSettings.isAllLogged()){ // is All logged
                    CORE_LogManager.createLog(log);
                }else if(logSettings.isErrorLogged() && isErrorLog){ // is Error
                    CORE_LogManager.createLog(log);
                }
            }
        }

        return result;
    }
}