@isTest
public class IT_EmailPlatformEventTriggerTest {
	@isTest
    public static void afterInsertTest() {
        Test.setMock(HttpCalloutMock.class, new IT_MockMarketingCloudIntegration('token'));
        Test.startTest();
        IT_EmailPlatformEvent__e PE = new IT_EmailPlatformEvent__e();

        PE.IT_BusinessUnitExternalId__c             = 'NB.MI0';            
        PE.IT_AccountLanguagePC__c                  = 'English';        
        PE.IT_AccountLanguageWebsite__c             = 'English';            
        PE.IT_AccountLanguageOfRelationship__c      = 'English';                    
        PE.IT_CountryRelevance__c                   = 'A';        
        PE.IT_StudentFirstName__c                   = 'TestN1';        
        PE.IT_StudentLastName__c                    = 'TestS1';        
        PE.IT_StudentEmail__c                       = 'TestN1TestS1@null.aa';    
        PE.IT_TaskId__c                             = '123456789012345678';
        PE.IT_SubscriberKey__c                      = '923456789012345678';    
        PE.IT_EventDefinitionKey__c                 = 'APIEvent-f82bb383-b351-d6c3-b969-a3dda66cbcd2';        
        PE.IT_COAProcess__c                         = 'IT_PKL_LeadNew';
        PE.IT_DivisionExternalId__c                 = 'NB';        
        PE.IT_EmailTypology__c                      = 'TY';    
        PE.IT_TaskResult__c                         = 'Test';
        PE.IT_OwnerTelephone__c                     = 'Test';
        PE.IT_OwnerMobile__c                        = 'Test';
        PE.IT_OwnerName__c                          = 'Test';
        PE.IT_MacroArea__c                          = 'Test';
        PE.IT_MailingCountry__c                     = 'Test';
        PE.IT_COAProcess__c                     	= 'Test';

        List<IT_EmailPlatformEvent__e> PEList = new List<IT_EmailPlatformEvent__e>();
        PEList.add(PE);
        Database.SaveResult[] sr = EventBus.publish(PEList);
        
        Test.stopTest();
    }
}