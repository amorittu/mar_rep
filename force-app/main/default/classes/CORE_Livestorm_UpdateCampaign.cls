public class CORE_Livestorm_UpdateCampaign  implements Database.Batchable<SObject>, Database.AllowsCallouts {
    Set<Id> campaignIds;

    public CORE_Livestorm_UpdateCampaign(Set<Id> c){
        System.debug('CORE_Livestorm_UpdateCampaign : START');
        campaignIds = c;
    }

    public Database.QueryLocator start(Database.BatchableContext bc){
        return Database.getQueryLocator([SELECT Id, Name, CORE_Campaign_Full_Name__c, CORE_Livestorm_Event_Id__c, CORE_Livestorm_Session_Id__c, Description, CORE_Timezone__c, CORE_Start_Date_and_hour__c, CORE_Business_Unit__c, CORE_Is_Up_to_Date_With_Livestorm__c
        FROM Campaign where Id in :this.campaignIds]);
    }

    public void execute(Database.BatchableContext bc, List<Campaign> campaigns){
        System.debug('CORE_Livestorm_UpdateCampaign.execute() : START');

        Map<Id,CORE_Business_Unit__c> orderBusinessUnit = CORE_Livestorm_Helper.getBuFromCampaign(campaigns);

        for(Campaign campaign : campaigns){
            System.debug('Update element in livestorm for : ' + CORE_Livestorm_Helper.getName(campaign));

            CORE_Livestorm__mdt livestormMetadata = CORE_Livestorm_Helper.GetMetadata(orderBusinessUnit, campaign);

            if (livestormMetadata.CORE_Is_Livestorm_Active__c){
                try {
                    if (!updateEvent(campaign, livestormMetadata)){
                        System.debug('Event : ' + campaign.CORE_Livestorm_Event_Id__c + ' updated');
                        try{
                            if (!updateSession(campaign, campaign.CORE_Livestorm_Session_Id__c, livestormMetadata)){
                                System.debug('Session : ' + campaign.CORE_Livestorm_Session_Id__c + ' updated');
                                campaign.CORE_Is_Up_to_Date_With_Livestorm__c = true;
                            }
                            else{
                                campaign.CORE_Is_Up_to_Date_With_Livestorm__c = false;
                            }
                        } catch (Exception e){
                            System.debug('The following exception has occurred during session update : ' + e.getMessage());
                        }
                    } else{
                        System.debug('Cannot update session because event were not update correctly');
                        campaign.CORE_Is_Up_to_Date_With_Livestorm__c = false;
                    }
                } catch (Exception e) {
                    System.debug('The following exception has occurred during event update : ' + e.getMessage());
                }
            }
            else {
                System.debug('Not updating event cause Livestorm is not active');
            }
        }
        Database.update(campaigns, false);
        System.debug('CORE_Livestorm_UpdateCampaign.execute() : END');
    }

    public void finish(Database.BatchableContext bc){
        System.debug('CORE_Livestorm_UpdateCampaign END.');
    }

    public Boolean updateEvent(Campaign campaign, CORE_Livestorm__mdt livestormMetadata){
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        
        string fullEndPoint = livestormMetadata.CORE_Default_End_Point__c + livestormMetadata.CORE_Update_Event_End_Point__c;
        fullEndPoint = String.format(fullEndPoint, new List<String>{campaign.CORE_Livestorm_Event_Id__c});

        request.setMethod('PATCH');
        request.setEndpoint(fullEndPoint);
        request.setHeader('Accept', 'application/vnd.api+json');
        request.setHeader('Authorization', livestormMetadata.CORE_Authorization__c);
        request.setHeader('Content-Type', 'application/vnd.api+json');
        

        request.setBody('{"data": {"type": "events","attributes": {"owner_id": "'+ livestormMetadata.CORE_Owner_Id__c +'","title": "' + CORE_Livestorm_Helper.getName(campaign) + '", "status": "published", "description": "<p>' + (campaign.Description != null ? campaign.Description : '') + '</p> "}}}');
        
        Long dt1 = DateTime.now().getTime();
        DateTime dt1Date = DateTime.now();

        HttpResponse response = http.send(request);

        long dt2 = DateTime.now().getTime(); 
        DateTime dt2Date = DateTime.now();
        System.debug('http update event execution time = ' + (dt2-dt1) + ' ms');

        if(response.getStatusCode() != 200) {
            System.debug('Event :The status code returned was not expected: ' + response.getStatusCode() + ' ' + response.getStatus());
            return true;
        } else {
            return false;
        }
    }

    public Boolean updateSession(Campaign campaign, String sessionId, CORE_Livestorm__mdt livestormMetadata){
        System.debug('Start updating Session for ' + campaign.CORE_Campaign_Full_Name__c + ' with ls id  ' + sessionId);

        Http http = new Http();
        HttpRequest request = new HttpRequest();

                
        string fullEndPoint = livestormMetadata.CORE_Default_End_Point__c + livestormMetadata.CORE_Update_Session_End_Point__c;
        fullEndPoint = String.format(fullEndPoint, new List<String>{sessionId});
        
        request.setMethod('PATCH');
        request.setEndpoint(fullEndPoint);
        request.setHeader('Accept', 'application/vnd.api+json');
        request.setHeader('Authorization', livestormMetadata.CORE_Authorization__c);
        request.setHeader('Content-Type', 'application/vnd.api+json');

        request.setBody('{"data": {"type": "sessions","attributes": {"estimated_started_at": "' + CORE_Livestorm_Helper.GetSessionStartDate(campaign) + '","timezone": "' + CORE_Livestorm_Helper.getTimezone(campaign) + '"}}}');
        System.debug('{"data": {"type": "sessions","attributes": {"estimated_started_at": "' + CORE_Livestorm_Helper.GetSessionStartDate(campaign) + '","timezone": "' + CORE_Livestorm_Helper.getTimezone(campaign) + '"}}}');
        Long dt1 = DateTime.now().getTime();
        DateTime dt1Date = DateTime.now();

        HttpResponse response = http.send(request);

        long dt2 = DateTime.now().getTime(); 
        DateTime dt2Date = DateTime.now();
        System.debug('http update session execution time = ' + (dt2-dt1) + ' ms');

        if(response.getStatusCode() != 200) {
            System.debug('Session : The status code returned was not expected: ' + response.getStatusCode() + ' ' + response.getStatus());
            return true;
        } else {
            return false;
        }
    }
}