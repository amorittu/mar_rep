public class CORE_OnlineShop_QueryProvider {

    public static Set<String> oppsFieldsSet = new Set<String> {
        'CORE_OPP_OnlineShop_Id__c','CORE_OPP_SIS_Opportunity_Id__c','CORE_OPP_SIS_Student_Academic_Record_ID__c','StageName','CORE_OPP_Sub_Status__c',
        'CORE_OPP_Application_Online__c','CORE_OPP_Application_Online_ID__c','CORE_OPP_Application_Online_Advancement__c','CORE_OPP_Application_started__c',
        'CORE_OPP_Application_completed__c','CORE_OPP_Application_Fee_Received__c','CORE_OPP_Application_Fee_Payment_Method__c',
        'CORE_OPP_Application_Fee_Received_Date__c','CORE_Application_Fee_Transaction_Id__c','CORE_OPP_Acceptance_Application_Fee_Date__c',
        'CORE_OPP_Application_fee_amount__c','CORE_OPP_Application_Fee_Bank__c','CORE_OPP_Application_Fee_Sender__c','CORE_OPP_Admission_Test_Fee_Needed__c',
        'CORE_OPP_Admission_Test_Fee_Received__c','CORE_OPP_AdmissionTestFee_Payment_Method__c','CORE_OPP_AdmissionTest_Fee_Received_Date__c',
        'CORE_OPP_Application_Reviewer_1__c','CORE_OPP_Application_Reviewer_1_Decision__c','CORE_OPP_Application_Reviewer_2__c',
        'CORE_OPP_Application_Reviewer_2_Decision__c','CORE_OPP_Application_Final_Decision__c','CORE_OPP_Down_Payment_Fee_Received__c',
        'CORE_OPP_Down_Payment_Fee_Payment_Method__c','CORE_OPP_Down_Payment_Fee_Received_Date__c','CORE_OPP_Registration_Form__c',
        'CORE_OPP_Registration_Form_Date__c','CORE_OPP_Tuition_Fee_Received__c','CORE_OPP_Tuition_Fee_Payment_Type__c','CORE_OPP_Tuition_Fee_Payment_Method__c',
        'CORE_OPP_Tuition_Fee_Received_Date__c','CORE_OPP_Registration_Date__c','CORE_Showed_up_to_classes__c','CORE_OPP_Scholarship_Requested__c',
        'CORE_OPP_Scholarship_Assigned__c','CORE_OPP_Scholarship_Holder__c','CORE_OPP_Scholarship_Type__c','Id', 'CORE_OPP_Study_Area__c', 'CORE_OPP_Curriculum__c',
		'CORE_OPP_Level__c', 'CORE_OPP_Intake__c', 'CORE_Main_Product_Interest__c'
    };

    public static Opportunity getOpportunity(String onlineShopId){

        String whereIdCondition = 'CORE_OPP_OnlineShop_Id__c = :onlineShopId';

        Set<String> allFields = new Set<String>();
        String queryFields = '';

        for(String f : oppsFieldsSet){
            if(!allFields.contains(f)){
                queryFields += (queryFields != '') ? ',' + f :f;
            }

            allFields.add(f);
        }

        List<String> queryParams = new List<String> {
            queryFields,
            whereIdCondition
        };

        String queryString = String.format('SELECT {0} FROM Opportunity WHERE {1}', queryParams);
        List<Opportunity> opportunities = Database.query(queryString);
        if(opportunities.size()==1){
            return opportunities.get(0);
        }
        else{
            return null ;
        }
    }

	public static CORE_Intake__c getIntakeFromExternalId(String intakeExternalId){
        CORE_Intake__c intake;

        List<CORE_Intake__c> intakes = [SELECT Id, CORE_Intake_ExternalId__c, CORE_Academic_Year__c
										FROM CORE_Intake__c 
										WHERE CORE_Intake_ExternalId__c = :intakeExternalId];

        if(intakes.size() > 0){
            intake = intakes.get(0);
        }

        return intake;
    }

	public static List<OpportunityLineItem> getOppLineItems(Id opportunityId, Id productId){
        List<OpportunityLineItem> oppLineItems = [SELECT Id
            FROM OpportunityLineItem
            WHERE OpportunityId = :opportunityId AND Product2Id = :productId 
            ORDER BY LastModifiedDate DESC];

        return oppLineItems;
    }
}