@isTest
public class IT_EmailMessageQueuableTest {
	@TestSetup
    public static void makeData() {
       CORE_DataFaker_CatalogHierarchy catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test1');
        PricebookEntry pbe = new PricebookEntry(Product2Id=catalogHierarchy.product.Id, Pricebook2Id=Test.getStandardPricebookId(), UnitPrice=0, IsActive=true);
        insert pbe;

        Account account = CORE_DataFaker_Account.getStudentAccount('test1');
        Opportunity opportunity = CORE_DataFaker_Opportunity.getOpportunity(
                catalogHierarchy.division.Id,
                catalogHierarchy.businessUnit.Id,
                account.Id,
                'Applicant',
                'Applicant - Application Submitted / New'
        ); 

        opportunity.Pricebook2Id = Test.getStandardPricebookId();
        opportunity.CORE_OPP_Level__c = catalogHierarchy.level.Id;
        opportunity.CORE_OPP_Curriculum__c = catalogHierarchy.curriculum.Id;
        opportunity.CORE_OPP_Study_Area__c = catalogHierarchy.studyArea.Id;
        opportunity.CORE_OPP_Intake__c = catalogHierarchy.intake.Id;
        update opportunity;

        IT_ExtendedInfo__c extendedInfo = IT_DataFaker_Objects.createExtendedInfo(account.id, opportunity.IT_Brand__c, true); 
        
        EmailMessage emMsg = new EmailMessage();
        emMsg.HtmlBody = 'test';
		insert emMsg;
    }
    
    @isTest
    public static void testBehavior() {
        List<Opportunity> opportunityList = [SELECT Id,AccountId FROM Opportunity];
    	Map<String, EmailMessage> relatedToId_EmailMessageMap = new Map<String, EmailMessage>([SELECT id FROM EmailMessage]);
    	Map<Id, Account> relatedAccounts = new Map<Id, Account>([SELECT Id FROM Account]);

        Test.startTest();
        IT_EmailMessageQueuable jobToEnq = new IT_EmailMessageQueuable(opportunityList,relatedAccounts,relatedToId_EmailMessageMap);
        System.enqueueJob(jobToEnq);
        Test.stopTest();
    }
}