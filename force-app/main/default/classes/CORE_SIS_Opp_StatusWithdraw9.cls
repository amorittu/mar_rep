@RestResource(urlMapping='/sis/opportunity/status/withdraw/reoriented')
global without sharing class CORE_SIS_Opp_StatusWithdraw9  {
    private static final String SUCCESS_MESSAGE = 'Opportunity status and sub-status updated with success';
	 @HttpPut
    global static CORE_SIS_Wrapper.ResultWrapperGlobal execute(String sisOpportunityId ) {
        
        // Initiating logs
        Long dt1 = DateTime.now().getTime();
        Long dt2;
        CORE_LogManager logSettings;
        CORE_LOG__c log = new CORE_LOG__c(CORE_API_Name__c = 'SIS');
        Boolean isErrorLog = false;
        logSettings = new CORE_LogManager('SIS');

        CORE_SIS_Wrapper.ResultWrapperGlobal result = checkMandatoryParameters(sisOpportunityId);

        try{
            if(result != null){
                //missing parameter(s)
                return result;
            }
            CORE_SIS_GlobalWorker sisWorker = new CORE_SIS_GlobalWorker(sisOpportunityId);
            Opportunity opportunity = sisWorker.sisOpportunity;

            if(opportunity == null){
                result = new CORE_SIS_Wrapper.ResultWrapperGlobal(
                    CORE_SIS_Constants.ERROR_STATUS, 
                    String.format(CORE_SIS_Constants.MSG_OPPORTUNITY_NOT_FOUND, new List<String>{sisOpportunityId})
                );
            } else {
                sisWorker.sisOpportunity = getOppWithChangedStatus(sisWorker.sisOpportunity);
            	update sisWorker.sisOpportunity;
                result = new CORE_SIS_Wrapper.ResultWrapperGlobal(CORE_SIS_Constants.SUCCESS_STATUS, SUCCESS_MESSAGE);
            }

        } catch(Exception e){
            result = new CORE_SIS_Wrapper.ResultWrapperGlobal(CORE_SIS_Constants.ERROR_STATUS, e.getMessage() + ' at ' + e.getStackTraceString());
        } finally{
            dt2 = DateTime.now().getTime();
            System.debug('CORE_AOL_AcademicDiplomaHistory(urlMapping=\'/aol/academic-diploma-history\') : T0TAL TIME OF EXECUTION = ' + (dt2-dt1) + ' ms');
            // creating log
            log.CORE_Processing_Duration__c =(dt2-dt1);
            log.CORE_HTTP_Status_Code__c = result.status == 'KO' ? 400 : result.status == 'OK' ? 200 : 500;
            log.CORE_Sent_Body__c = JSON.serialize(result);
            log.CORE_Received_Body__c =  '{"sisOpportunityId" : ' + sisOpportunityId + '"}';
            
            if(!logSettings.isNone()){
                if(logSettings.isAllLogged()){ // is All logged
                    CORE_LogManager.createLog(log);
                }else if(logSettings.isErrorLogged() && isErrorLog){ // is Error
                    CORE_LogManager.createLog(log);
                }
            }
        }

        return result;
    }
        private static Opportunity getOppWithChangedStatus(Opportunity opportunity){
        opportunity.StageName = CORE_SIS_Constants.PKL_OPP_STAGENAME_WITHDRAW;
        opportunity.CORE_OPP_Sub_Status__c = CORE_SIS_Constants.PKL_OPP_SUBSTATUS_WITHDRAW_REORIENTED;

        return opportunity;
    }
    
    private static CORE_SIS_Wrapper.ResultWrapperGlobal checkMandatoryParameters(String sisOpportunityId){
        List<String> missingParameters = new List<String>();
        Boolean isMissingSisId = CORE_SIS_GlobalWorker.issisOpportunityIdParameterMissing(sisOpportunityId);

        if(isMissingSisId){
            missingParameters.add('sisOpportunityId');
        }
        
        return CORE_SIS_GlobalWorker.getMissingParameters(missingParameters);
    }
}