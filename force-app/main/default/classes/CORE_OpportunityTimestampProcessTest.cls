@IsTest
public class CORE_OpportunityTimestampProcessTest {
    private static final boolean IS_SUSPECT_STATUS_EXIST = false;
    
    @TestSetup
    public static void makeData(){
        CORE_DataFaker_CatalogHierarchy catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test1');
        Account account = CORE_DataFaker_Account.getStudentAccount('test1');

        Opportunity opportunity = CORE_DataFaker_Opportunity.getOpportunity(catalogHierarchy.division.Id, 
            catalogHierarchy.businessUnit.Id, 
            catalogHierarchy.studyArea.Id,
            catalogHierarchy.curriculum.Id,
            catalogHierarchy.level.Id,
            catalogHierarchy.intake.Id, 
            account.Id, 
            'Lead', 
            'Lead - New'
        );
    }

    
    @IsTest
    public static void insertionProcess_Should_upsert_new_timestamps() {
        List<Opportunity> opportunities = [Select Id,CORE_Lead_Source_External_Id__c, CORE_Re_Orientation_Internally__c,CORE_AOL_Re_Orientation__c, StageName, CORE_IsFromDeferal__c, CORE_OPP_Sub_Status__c,IsClosed, LastModifiedDate, CORE_OPP_Legacy_CRM_Id__c, RecordTypeId FROM Opportunity];

        List<CORE_Sub_Status_Timestamp_History__c> subStatusHistory = [SELECT Id FROM CORE_Sub_Status_Timestamp_History__c];
        List<CORE_Status_Timestamp_History__c> statusHistory = [SELECT Id FROM CORE_Status_Timestamp_History__c];

        delete subStatusHistory;
        delete statusHistory;

        subStatusHistory = [SELECT Id FROM CORE_Sub_Status_Timestamp_History__c];
        statusHistory = [SELECT Id FROM CORE_Status_Timestamp_History__c];
        System.assertEquals(0, subStatusHistory.size());
        System.assertEquals(0, statusHistory.size());

        Test.startTest();
        CORE_OpportunityTimestampProcess.insertionProcess(opportunities);
        Test.stopTest();

        subStatusHistory = [SELECT Id FROM CORE_Sub_Status_Timestamp_History__c];
        statusHistory = [SELECT Id FROM CORE_Status_Timestamp_History__c];
        System.assert(0 < subStatusHistory.size());
    }

    @IsTest
    public static void updateProcess_Should_upsert_new_timestamps() {
        Id oppEnroll_RTId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('CORE_Enrollment').getRecordTypeId();

        List<Opportunity> opportunities = [Select Id,OwnerId,CORE_Main_Product_Interest__c,CORE_OPP_Study_Area__c,CORE_OPP_Intake__c,CORE_OPP_Level__c,CORE_OPP_Division__c,CORE_OPP_Business_Unit__c,CORE_OPP_Curriculum__c, StageName, CORE_OPP_Sub_Status__c, LastModifiedDate, Opportunity.Study_mode__c, RecordTypeId FROM Opportunity];
        Opportunity newOpportunity = new Opportunity(
            id = opportunities.get(0).Id,
            StageName = 'Prospect',
            CORE_OPP_Sub_Status__c = 'Prospect - new',
            OwnerId=opportunities.get(0).OwnerId,
            RecordTypeId = oppEnroll_RTId
        );

        List<CORE_Sub_Status_Timestamp_History__c> subStatusHistory = [SELECT Id FROM CORE_Sub_Status_Timestamp_History__c];
        List<CORE_Status_Timestamp_History__c> statusHistory = [SELECT Id FROM CORE_Status_Timestamp_History__c];

        delete subStatusHistory;
        delete statusHistory;

        subStatusHistory = [SELECT Id FROM CORE_Sub_Status_Timestamp_History__c];
        statusHistory = [SELECT Id FROM CORE_Status_Timestamp_History__c];
        System.assertEquals(0, subStatusHistory.size());
        System.assertEquals(0, statusHistory.size());

        Test.startTest();
        CORE_OpportunityTimestampProcess.updateProcess(opportunities,new List<Opportunity>{newOpportunity});
        Test.stopTest();

        subStatusHistory = [SELECT Id FROM CORE_Sub_Status_Timestamp_History__c];
        statusHistory = [SELECT Id FROM CORE_Status_Timestamp_History__c];
        System.assert(0 < subStatusHistory.size());
    }

    @IsTest
    public static void Constructor_Should_Initialise_Map_And_List_Of_Status_And_SubStatus() {
        //CORE_OpportunityTimestampProcess timestampProcess = new CORE_OpportunityTimestampProcess();
        CORE_OpportunityTimestampProcess.StatusPositions statusPos = new CORE_OpportunityTimestampProcess.StatusPositions();

        //System.assertNotEquals(null, timestampProcess.statusValues, 'statusValues should not be null');
        System.assertNotEquals(null, statusPos.positionsByStatus, 'positionsByStatus should not be null');
        System.assertNotEquals(null, statusPos.statusByPositions, 'statusByPositions should not be null');
    }

    @IsTest
    public static void insertSubStatusTimestamp_Should_insert_subStatusTimestamp_record_based_on_opportunity_datas() {
        List<CORE_Sub_Status_Timestamp_History__c> timestampHistory = [SELECT Id FROM CORE_Sub_Status_Timestamp_History__c];
        delete timestampHistory;

        List<Opportunity> opportunities = [Select Id, StageName, CORE_OPP_Sub_Status__c, LastModifiedDate, RecordTypeId FROM Opportunity];
        CORE_OpportunityTimestampProcess timestampProcess = new CORE_OpportunityTimestampProcess();

        Test.startTest();
            for(Opportunity opportunity : opportunities){
                opportunity.CORE_OPP_Sub_Status__c = 'Lead - Show';
            }

            timestampProcess.insertSubStatusTimestamp(opportunities);
        Test.stopTest();
        timestampHistory = [SELECT Id, CORE_Modification_Sub_status_Date_time__c, CORE_Status__c, CORE_Sub_status__c 
            FROM CORE_Sub_Status_Timestamp_History__c
            WHERE CORE_Opportunity__c = :opportunities.get(0).Id];


        System.assertEquals(opportunities.get(0).LastModifiedDate , timestampHistory.get(0).CORE_Modification_Sub_status_Date_time__c, 'CORE_Modification_Sub_status_Date_time__c should equals to the opportunity value');
        System.assertEquals(opportunities.get(0).StageName , timestampHistory.get(0).CORE_Status__c, 'CORE_Status__c should equals to the opportunity value');
        System.assertEquals(opportunities.get(0).CORE_OPP_Sub_Status__c , timestampHistory.get(0).CORE_Sub_status__c, 'CORE_Sub_status__c should equals to the opportunity value');
    }

    @IsTest
    public static void initSubStatusTimestampRecord_Should_Initialise_SubStatusTimestamp() {
        List<Opportunity> opportunities = [Select Id, StageName, CORE_OPP_Sub_Status__c, LastModifiedDate, RecordTypeId FROM Opportunity];
        CORE_OpportunityTimestampProcess timestampProcess = new CORE_OpportunityTimestampProcess();

        Test.startTest();
        CORE_Sub_Status_Timestamp_History__c subStatusTimeStamp = timestampProcess.initSubStatusTimestampRecord(opportunities.get(0));
        Test.stopTest();

        System.assertEquals(opportunities.get(0).Id , subStatusTimeStamp.CORE_Opportunity__c, 'CORE_Opportunity__c should equals to the opportunity value');
        System.assertEquals(opportunities.get(0).LastModifiedDate , subStatusTimeStamp.CORE_Modification_Sub_status_Date_time__c, 'CORE_Modification_Sub_status_Date_time__c should equals to the opportunity value');
        System.assertEquals(opportunities.get(0).StageName , subStatusTimeStamp.CORE_Status__c, 'CORE_Status__c should equals to the opportunity value');
        System.assertEquals(opportunities.get(0).CORE_OPP_Sub_Status__c , subStatusTimeStamp.CORE_Sub_status__c, 'CORE_Sub_status__c should equals to the opportunity value');
    }

    @IsTest
    public static void initStatusTimestampRecord_Should_Initialise_StatusTimestamp() {
        List<Opportunity> opportunities = [Select Id, StageName, CORE_OPP_Sub_Status__c, LastModifiedDate, RecordTypeId FROM Opportunity];
        CORE_OpportunityTimestampProcess timestampProcess = new CORE_OpportunityTimestampProcess();

        Test.startTest();
        CORE_Status_Timestamp_History__c statusTimeStamp = timestampProcess.initStatusTimestampRecord(opportunities.get(0), opportunities.get(0).StageName);
        Test.stopTest();

        System.assertEquals(opportunities.get(0).Id , statusTimeStamp.CORE_Opportunity__c, 'CORE_Opportunity__c should equals to the opportunity value');
        System.assertEquals(opportunities.get(0).LastModifiedDate , statusTimeStamp.CORE_Modification_Status_Date_time__c, 'CORE_Modification_Status_Date_time__c should equals to the opportunity value');
        System.assertEquals(opportunities.get(0).StageName , statusTimeStamp.CORE_Status__c, 'CORE_Status__c should equals to the opportunity value');
    }

    @IsTest
    public static void getSubStatusTimeStampHistories_Should_Return_a_new_SubStatus_Timestamp_History_record_when_the_SubStatus_does_not_already_exist() {
        Id oppEnroll_RTId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('CORE_Enrollment').getRecordTypeId();

        List<Opportunity> opportunities = [Select Id, StageName, CORE_OPP_Sub_Status__c, LastModifiedDate, RecordTypeId FROM Opportunity];
        Opportunity opportunity = opportunities.get(0);
        
        CORE_OpportunityTimestampProcess timestampProcess = new CORE_OpportunityTimestampProcess();

        List<CORE_Sub_Status_Timestamp_History__c> existingTimeStamps = new List<CORE_Sub_Status_Timestamp_History__c>();
        DateTime dtNow = System.now();
        CORE_Sub_Status_Timestamp_History__c existingTimeStamp = new CORE_Sub_Status_Timestamp_History__c(
            CORE_Modification_Sub_status_Date_time__c = dtNow,
            CORE_Status__c = opportunity.StageName,
            CORE_Sub_status__c = opportunity.CORE_OPP_Sub_Status__c,
            CORE_Opportunity__c = opportunity.Id
        );
        insert existingTimeStamp;

        existingTimeStamps.add(existingTimeStamp);
        opportunity.CORE_OPP_Sub_Status__c = 'Lead - Show';
        Test.startTest();
        CORE_Sub_Status_Timestamp_History__c statusTimeStamp = timestampProcess.getSubStatusTimeStampHistories(opportunity, existingTimeStamps);
        Test.stopTest();

        System.assertEquals(null , statusTimeStamp.Id, 'Id of timestamp should be null because it is a new one');
        System.assertEquals(opportunity.StageName , statusTimeStamp.CORE_Status__c, 'CORE_Status__c should be equals to the opportunity status');
        System.assertEquals(opportunity.CORE_OPP_Sub_Status__c , statusTimeStamp.CORE_Sub_status__c, 'CORE_Sub_status__c should be equals to the opportunity sub status');
        System.assertEquals(opportunity.LastModifiedDate , statusTimeStamp.CORE_Modification_Sub_status_Date_time__c, 'CORE_Modification_Sub_status_Date_time__c should be equals to the opportunity Last modified date');
    }

    @IsTest
    public static void getExistingStatusTimestamps_Should_Return_a_list_of_status_timestamp_related_to_opportunities() {
        Id oppEnroll_RTId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('CORE_Enrollment').getRecordTypeId();

        List<Opportunity> opportunities = [Select Id, StageName, CORE_OPP_Sub_Status__c, LastModifiedDate, RecordTypeId FROM Opportunity];

        Opportunity opportunity = opportunities.get(0);
        
        CORE_OpportunityTimestampProcess timestampProcess = new CORE_OpportunityTimestampProcess();
        opportunity.StageName = 'Prospect';
        opportunity.CORE_OPP_Sub_Status__c = 'Prospect - New';
        opportunity.CORE_Change_Status__c = true;
        Update opportunity;

        Test.startTest();
        List<CORE_Status_Timestamp_History__c> statusTimeStamps = timestampProcess.getExistingStatusTimestamps(new Set<Id> {opportunity.Id});
        Test.stopTest();

        System.debug('statusTimeStamps = ' + statusTimeStamps);
        System.assertEquals(IS_SUSPECT_STATUS_EXIST ? 3 : 2 , statusTimeStamps.size(), '3 record should be return (3 for inital opportunity insertion)');

        for(CORE_Status_Timestamp_History__c timestamp : statusTimeStamps){
            System.assertEquals(opportunity.Id , timestamp.CORE_Opportunity__c, 'Record should be related to the opportunity');
        }
    }

    @IsTest
    public static void upsertStatusTimestamp_Should_Insert_new_timestamps_when_status_is_changed_to_higher_position() {
        Id oppEnroll_RTId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('CORE_Enrollment').getRecordTypeId();

        List<Opportunity> opportunities = [Select Id, StageName, CORE_OPP_Sub_Status__c, LastModifiedDate, RecordTypeId FROM Opportunity];
        Opportunity oldOpportunity = opportunities.get(0);
        Opportunity newOpportunity = new Opportunity(
            id = oldOpportunity.Id,
            StageName = 'Registered',
            CORE_OPP_Sub_Status__c = 'Registered - Awaiting Funding',
            RecordTypeId = oppEnroll_RTId
        );
        CORE_OpportunityTimestampProcess timestampProcess = new CORE_OpportunityTimestampProcess();

        Datetime dtNow = System.now();

        //newOpportunity.CORE_OPP_Sub_Status__c = 'Registered - Awaiting Funding';
        List<CORE_Status_Timestamp_History__c> existingTimestamps = [Select ID, CORE_Status__c FROM CORE_Status_Timestamp_History__c WHERE CORE_Opportunity__c = :oldOpportunity.Id];
        System.assertEquals(IS_SUSPECT_STATUS_EXIST ? 3 : 2, existingTimestamps.size());

        Test.startTest();
        timestampProcess.upsertStatusTimestamp(new List<Opportunity> {oldOpportunity}, new List<Opportunity> {newOpportunity});
        Test.stopTest();

        List<CORE_Status_Timestamp_History__c> timestampsAfterUpsert = [Select ID, CORE_Status__c, CORE_Modification_Status_Date_time__c FROM CORE_Status_Timestamp_History__c WHERE CORE_Opportunity__c = :oldOpportunity.Id];
        System.assertEquals(IS_SUSPECT_STATUS_EXIST ? 6 : 5 , timestampsAfterUpsert.size(), '3 timestamps should be inserted (new status and those between the first status and the new one');
    }

    @IsTest
    public static void upsertStatusTimestamp_Should_Upsert_1_timestamp_when_new_status_is_close() {
        Id oppEnroll_RTId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('CORE_Enrollment').getRecordTypeId();

        List<Opportunity> opportunities = [Select Id, StageName, CORE_OPP_Sub_Status__c,IsClosed, LastModifiedDate, RecordTypeId FROM Opportunity];
        Opportunity oldOpportunity = opportunities.get(0);
        Opportunity newOpportunity = oldOpportunity.clone();
        newOpportunity.Id = null;
        newOpportunity.StageName = 'Closed Lost';
        newOpportunity.CORE_OPP_Sub_Status__c = 'Lead - Exit - Abandoned';
        newOpportunity.Name = 'test';
        newOpportunity.CloseDate = System.today();
        newOpportunity.RecordTypeId = oppEnroll_RTId;
        insert newOpportunity;
        newOpportunity = [Select Id, StageName, CORE_OPP_Sub_Status__c,IsClosed, LastModifiedDate, RecordTypeId FROM Opportunity where Id = :newOpportunity.Id];
        CORE_OpportunityTimestampProcess timestampProcess = new CORE_OpportunityTimestampProcess();

        Datetime dtNow = System.now();

        List<CORE_Status_Timestamp_History__c> existingTimestamps = [Select ID, CORE_Status__c FROM CORE_Status_Timestamp_History__c WHERE CORE_Opportunity__c = :oldOpportunity.Id];
        System.assertEquals(IS_SUSPECT_STATUS_EXIST ? 3 : 2, existingTimestamps.size());

        List<CORE_Status_Timestamp_History__c> duplicateTimesStamps = [Select ID, CORE_Status__c FROM CORE_Status_Timestamp_History__c WHERE CORE_Opportunity__c = :newOpportunity.Id];
        delete duplicateTimesStamps;

        duplicateTimesStamps = new List<CORE_Status_Timestamp_History__c>();
        for(CORE_Status_Timestamp_History__c newTimeStamp : existingTimestamps){
            newTimeStamp.Id = null;
            newTimeStamp.CORE_Opportunity__c = newOpportunity.Id;
            duplicateTimesStamps.add(newTimeStamp);
        }
        insert duplicateTimesStamps;

        Test.startTest();
        timestampProcess.upsertStatusTimestamp(new List<Opportunity> {oldOpportunity}, new List<Opportunity> {newOpportunity});
        Test.stopTest();

        List<CORE_Status_Timestamp_History__c> timestampsAfterUpsert = [Select ID, CORE_Status__c, CORE_Modification_Status_Date_time__c FROM CORE_Status_Timestamp_History__c WHERE CORE_Opportunity__c = :newOpportunity.Id];
        System.assertEquals(IS_SUSPECT_STATUS_EXIST ? 4 : 3 , timestampsAfterUpsert.size(), '1 timestamps should be inserted (new close status only');
    }

    @IsTest
    public static void upsertStatusTimestamp_Should_Update_existing_timestamps_when_status_is_changed_to_lower_position() {
        Id oppEnroll_RTId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('CORE_Enrollment').getRecordTypeId();

        List<Opportunity> opportunities = [Select Id, StageName, CORE_OPP_Sub_Status__c, LastModifiedDate, RecordTypeId FROM Opportunity];
        Opportunity oldOpportunity = opportunities.get(0);
        Opportunity newOpportunity = new Opportunity(
            id = oldOpportunity.Id,
            StageName = 'Prospect',
            CORE_OPP_Sub_Status__c = 'Prospect - new',
            RecordTypeId = oppEnroll_RTId
        );
        CORE_OpportunityTimestampProcess timestampProcess = new CORE_OpportunityTimestampProcess();

        Datetime dtNow = System.now();
        //newOpportunity.StageName = 'Registered';
        //newOpportunity.CORE_OPP_Sub_Status__c = 'Registered - Awaiting Funding';
        List<CORE_Status_Timestamp_History__c> existingTimestamps = [Select ID, CORE_Status__c FROM CORE_Status_Timestamp_History__c WHERE CORE_Opportunity__c = :oldOpportunity.Id];
        System.assertEquals(IS_SUSPECT_STATUS_EXIST ? 3 : 2, existingTimestamps.size());

        Test.startTest();
        timestampProcess.upsertStatusTimestamp(new List<Opportunity> {oldOpportunity}, new List<Opportunity> {newOpportunity});
        Test.stopTest();

        List<CORE_Status_Timestamp_History__c> timestampsAfterUpsert = [Select ID, CORE_Status__c, CORE_Modification_Status_Date_time__c FROM CORE_Status_Timestamp_History__c WHERE CORE_Opportunity__c = :oldOpportunity.Id];
        System.assertEquals(IS_SUSPECT_STATUS_EXIST ? 3 : 2 , timestampsAfterUpsert.size());
    }

    @IsTest
    public static void getStatusTimeStampHistories_Should_return_all_timetamps_which_have_to_be_upserted() {
        Id oppEnroll_RTId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('CORE_Enrollment').getRecordTypeId();

        List<Opportunity> opportunities = [Select Id, StageName, CORE_OPP_Sub_Status__c, LastModifiedDate, RecordTypeId FROM Opportunity];
        Opportunity oldOpportunity = opportunities.get(0);
        Opportunity newOpportunity = new Opportunity(
            id = oldOpportunity.Id,
            StageName = 'Prospect',
            CORE_OPP_Sub_Status__c = 'Prospect - new',
            RecordTypeId = oppEnroll_RTId
        );
        CORE_OpportunityTimestampProcess timestampProcess = new CORE_OpportunityTimestampProcess();

        Datetime dtNow = System.now();
        //newOpportunity.StageName = 'Registered';
        //newOpportunity.CORE_OPP_Sub_Status__c = 'Registered - Awaiting Funding';
        List<CORE_Status_Timestamp_History__c> existingTimestamps = [Select ID, CORE_Status__c FROM CORE_Status_Timestamp_History__c WHERE CORE_Opportunity__c = :oldOpportunity.Id];
        System.assertEquals(IS_SUSPECT_STATUS_EXIST ? 3 : 2, existingTimestamps.size());

        Test.startTest();
        List<CORE_Status_Timestamp_History__c> timestamps = timestampProcess.getStatusTimeStampHistories(oldOpportunity, newOpportunity,existingTimestamps);

        System.assertEquals(1 , timestamps.size());

        oldOpportunity.StageName = 'Prospect';
        newOpportunity = new Opportunity(
            id = oldOpportunity.Id,
            StageName = 'Lead',
            CORE_OPP_Sub_Status__c = 'Lead - new',
            RecordTypeId = oppEnroll_RTId
        );
        timestamps = timestampProcess.getStatusTimeStampHistories(oldOpportunity, newOpportunity,existingTimestamps);
        System.assertEquals(1 , timestamps.size());

        oldOpportunity.StageName = 'Lead';
        newOpportunity = new Opportunity(
            id = oldOpportunity.Id,
            StageName = 'Registered',
            CORE_OPP_Sub_Status__c = 'Registered - new',
            RecordTypeId = oppEnroll_RTId
        );
        timestamps = timestampProcess.getStatusTimeStampHistories(oldOpportunity, newOpportunity,existingTimestamps);
        System.assertEquals(3 , timestamps.size());

        Test.stopTest();

    }
}