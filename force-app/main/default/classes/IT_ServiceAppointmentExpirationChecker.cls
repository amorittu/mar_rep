public without sharing class IT_ServiceAppointmentExpirationChecker implements Database.Batchable<sObject>, Database.Stateful {
    public static final String CLASS_NAME = IT_ServiceAppointmentExpirationChecker.class.getName();
    public static final String[] DISCRIMINATING_STATUSES = new String[]{'No Show', 'Show', 'Canceled', 'Cannot Complete' };

    //INPUT PARAMETERS START
    String[] serviceAppointmentIds = new String[]{};
    //INPUT PARAMETERS END

    public IT_ServiceAppointmentExpirationChecker(String serviceAppointmentId) {
        this.serviceAppointmentIds.add(serviceAppointmentId);
    }

    public IT_ServiceAppointmentExpirationChecker(String[] serviceAppointmentIds) {
        this.serviceAppointmentIds = serviceAppointmentIds;
    }

    public IT_ServiceAppointmentExpirationChecker() {
        
    }

    // OUTPUT PARAMS START
    public Map<String, IT_MainObjectExpirationChecker__mdt> customMetadataStructure = new Map<String, IT_MainObjectExpirationChecker__mdt>();
    public Map<String, IT_ObjectExpirationCheckerBU__mdt> childrenMetadataStructure = new Map<String, IT_ObjectExpirationCheckerBU__mdt>();

    public Database.QueryLocator start(Database.BatchableContext BC){
        
        IT_MainObjectExpirationChecker__mdt[] mainMetadatas = [
            SELECT Id, 
                IT_DaysNumber__c, 
                IT_FieldChecker__c, 
                IT_DivisionExternalId__c,
                IT_ObjectIdentifier__c, 
                DeveloperName
            FROM IT_MainObjectExpirationChecker__mdt
            WHERE IT_ObjectIdentifier__c = 'ServiceAppointment'
        ];

        for (IT_MainObjectExpirationChecker__mdt mainMetadata : mainMetadatas) {
            this.customMetadataStructure.put(mainMetadata.IT_DivisionExternalId__c, mainMetadata);
        }

        IT_ObjectExpirationCheckerBU__mdt[] childrenMetadataBusinessUnits = [
            SELECT Id, 
                IT_BusinessUnitExternalId__c, 
                IT_DaysNumber__c
            FROM IT_ObjectExpirationCheckerBU__mdt
        ];

        for (IT_ObjectExpirationCheckerBU__mdt buMetadata : childrenMetadataBusinessUnits) {
            this.childrenMetadataStructure.put(buMetadata.IT_BusinessUnitExternalId__c, buMetadata);
        }
        
        String soql = 'SELECT Id, Status, CORE_TECH_Relatd_Opportunity__r.IT_DivisionExternalId__c, ' +
            ' CORE_TECH_Relatd_Opportunity__r.IT_BusinessUnitExternalId__c, SchedStartTime ' +
            ' FROM ServiceAppointment ';
        if ( serviceAppointmentIds.isEmpty() ) {
            soql += ' WHERE Status NOT IN: DISCRIMINATING_STATUSES AND SchedEndTime < TODAY AND SchedStartTime != NULL ';
        } else {
            soql += ' WHERE Id IN: serviceAppointmentIds ';
        }

        System.debug(CLASS_NAME + ' - soql : ' + JSON.serialize(soql));
        System.debug(CLASS_NAME + ' - mainMetadatas : ' + JSON.serialize(mainMetadatas));
        System.debug(CLASS_NAME + ' - childrenMetadataBusinessUnits : ' + JSON.serialize(childrenMetadataBusinessUnits));
        return Database.getQueryLocator(soql);
    }


    public void execute(Database.BatchableContext BC, List<ServiceAppointment> scope){
        System.debug(CLASS_NAME + ' - execute fired ');
        
        
        Map<String, ServiceAppointment[]> campaign_cms = new Map<String, ServiceAppointment[]>();

        ServiceAppointment[] serviceAppointmentsToUpdate = new ServiceAppointment[]{};

        for(ServiceAppointment sa : scope){
            System.debug(CLASS_NAME + ' - sa is : ' + sa );
            System.debug(CLASS_NAME + ' - customMetadataStructure is : ' + this.customMetadataStructure.get(sa?.CORE_TECH_Relatd_Opportunity__r?.IT_DivisionExternalId__c) );
            System.debug(CLASS_NAME + ' - sa.CORE_Opportunity__r.IT_DivisionExternalId__c is : ' + sa.CORE_TECH_Relatd_Opportunity__r.IT_DivisionExternalId__c );
            if (customMetadataStructure.containsKey(sa?.CORE_TECH_Relatd_Opportunity__r?.IT_DivisionExternalId__c) ) {
                Date dateChecker = Date.valueOf(sa.SchedStartTime);
                Integer numberOfDays = 0;
                if (customMetadataStructure.get(sa.CORE_TECH_Relatd_Opportunity__r.IT_DivisionExternalId__c).IT_FieldChecker__c  == 'Division') {
                    System.debug(CLASS_NAME + ' - IT_FieldChecker__c == Division ');
                    numberOfDays = Integer.valueOf(customMetadataStructure.get(sa.CORE_TECH_Relatd_Opportunity__r.IT_DivisionExternalId__c).IT_DaysNumber__c) ;
                } else {
                    System.debug(CLASS_NAME + ' - IT_FieldChecker__c == BusinessUnit ');
                    numberOfDays = Integer.valueOf(childrenMetadataStructure.get(sa.CORE_TECH_Relatd_Opportunity__r.IT_BusinessUnitExternalId__c).IT_DaysNumber__c);
                }

                Boolean isServiceAppointmentObsolete = calculateDays(dateChecker, numberOfDays );
                System.debug(CLASS_NAME + ' - isServiceAppointmentObsolete : ' + isServiceAppointmentObsolete);    
                if (isServiceAppointmentObsolete == true) {
                    serviceAppointmentsToUpdate.add(sa);
                }
            }
        }
        if (! serviceAppointmentsToUpdate.isEmpty() ) {
            for (ServiceAppointment sa : serviceAppointmentsToUpdate) {
                sa.Status = 'No Show';
            }
            System.debug(CLASS_NAME + ' - serviceAppointmentsToUpdate : ' + JSON.serialize(serviceAppointmentsToUpdate));
            update serviceAppointmentsToUpdate;
        }
        System.debug(CLASS_NAME + ' - execute finished ');
    }

    public Boolean calculateDays(Date dateChecker, Integer numberOfDays) {
        Date finalDate = dateChecker.addDays(numberOfDays);
        System.debug(CLASS_NAME + ' - finalDate ' + finalDate);
        if (finalDate < System.today() ) {
            return true;
        }
        return false;
    }

    public void finish(Database.BatchableContext BC){

    }
}