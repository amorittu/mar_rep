public without sharing class CORE_CUBE_QueryProvider {
    static final List<String> statusOpportunity = new List<String>{'Suspect', 'Prospect', 'Lead', 'Applicant', 'Admitted', 'Registered', 'Enrolled', 'Withdraw'};
    static final List<String> status = new List<String>{'Closed Won', 'Closed Lost'};
    static final List<String> excludedDomains = new List<String>{'yopmail.com'};

    static final String ACCOUNT_RECORDTYPE = 'Core_Student';
    static final String OPPORTUNITY_RECORDTYPE = 'CORE_Enrollment';
    static final List<String> DELETED_RECORDTYPES = new List<String> {'Core_Student','CORE_Enrollment'};

    public static List<Account> getAccountList(DateTime myLastSuccessDate){
        List<Account> accountList;
        if(myLastSuccessDate != null){
            accountList = [SELECT Id,Salutation,PersonMailingCountry,CORE_LegacyCRMID__c,PersonEmail,CORE_PersonEmailDomain__c,CORE_Main_Nationality__pc,FirstName,	CORE_Gender__pc,CORE_Country_of_birth__pc,CreatedDate,LastModifiedDate  
                FROM Account 
                WHERE IsPersonAccount = true 
                AND RecordType.DeveloperName = :ACCOUNT_RECORDTYPE 
                AND CORE_PersonEmailDomain__c NOT IN :excludedDomains 
                AND ((LastModifiedDate >= :myLastSuccessDate AND LastModifiedDate < TODAY) OR (CreatedDate >= :myLastSuccessDate AND CreatedDate < TODAY))
            ];
        } else {
            accountList = [SELECT Id,Salutation,CORE_LegacyCRMID__c,PersonEmail,CORE_PersonEmailDomain__c,PersonMailingCountry,CORE_Main_Nationality__pc,FirstName,	CORE_Gender__pc,CORE_Country_of_birth__pc,CreatedDate,LastModifiedDate  
                FROM Account 
                WHERE IsPersonAccount = true
                AND RecordType.DeveloperName = :ACCOUNT_RECORDTYPE 
                AND CORE_PersonEmailDomain__c NOT IN :excludedDomains 
            ];
        }

        return accountList;
    }

    public static List<Opportunity> getOpportunityList(DateTime myLastSuccessDate){
        List<Opportunity> opportunityList;

        if(myLastSuccessDate != null){
            opportunityList = [SELECT Id,CORE_OPP_Legacy_CRM_Id__c,CORE_OPP_Main_Opportunity__c,RecordType.Name,CORE_OPP_Academic_Year__c,Study_mode__c,CreatedDate,LastModifiedDate,CORE_OPP_Application_Online__c,AccountId,CORE_OPP_Business_Unit__r.CORE_Business_Unit_ExternalId__c,CORE_OPP_Study_Area__r.CORE_Study_Area_ExternalId__c,CORE_OPP_Curriculum__r.CORE_Curriculum_ExternalId__c,CORE_OPP_Level__r.CORE_Level_ExternalId__c,CORE_OPP_Intake__r.CORE_Intake_ExternalId__c,CORE_Reach_Date__c,CORE_OPP_Agent_Perception__c 
                FROM Opportunity 
                WHERE StageName IN :statusOpportunity 
                AND recordtype.DeveloperName = :OPPORTUNITY_RECORDTYPE 
                AND ((LastModifiedDate >= :myLastSuccessDate AND LastModifiedDate < TODAY) OR (CreatedDate >= :myLastSuccessDate AND CreatedDate < TODAY))
            ];
        } else {
            opportunityList = [SELECT Id,CORE_OPP_Legacy_CRM_Id__c,CORE_OPP_Main_Opportunity__c,RecordType.Name,CORE_OPP_Academic_Year__c,Study_mode__c,CreatedDate,LastModifiedDate,CORE_OPP_Application_Online__c,AccountId,CORE_OPP_Business_Unit__r.CORE_Business_Unit_ExternalId__c,CORE_OPP_Study_Area__r.CORE_Study_Area_ExternalId__c,CORE_OPP_Curriculum__r.CORE_Curriculum_ExternalId__c,CORE_OPP_Level__r.CORE_Level_ExternalId__c,CORE_OPP_Intake__r.CORE_Intake_ExternalId__c,CORE_Reach_Date__c,CORE_OPP_Agent_Perception__c 
                FROM Opportunity 
                WHERE recordtype.DeveloperName = :OPPORTUNITY_RECORDTYPE
            ];
        }

        return opportunityList;
    }

    public static List<CORE_Status_Timestamp_History__c> getStatusTimestampList(DateTime myLastSuccessDate){
        List<CORE_Status_Timestamp_History__c> statusTimestampList;

        if(myLastSuccessDate != null){
            statusTimestampList = [SELECT Id,CORE_Opportunity__c,CORE_Status__c,CORE_Modification_Status_Date_time__c,CreatedDate,LastModifiedDate 
                FROM CORE_Status_Timestamp_History__c 
                WHERE CORE_Status__c NOT IN :status
                AND CORE_Opportunity__r.StageName IN :statusOpportunity
                AND CORE_Opportunity__r.recordtype.DeveloperName= :OPPORTUNITY_RECORDTYPE
                AND ((CORE_Opportunity__r.LastModifiedDate >= :myLastSuccessDate AND CORE_Opportunity__r.LastModifiedDate < TODAY) OR (CORE_Opportunity__r.CreatedDate >= :myLastSuccessDate AND CORE_Opportunity__r.CreatedDate < TODAY))
            ];
        } else {
            statusTimestampList = [SELECT Id,CORE_Opportunity__c,CORE_Status__c,CORE_Modification_Status_Date_time__c,CreatedDate,LastModifiedDate 
                FROM CORE_Status_Timestamp_History__c 
                WHERE CORE_Status__c NOT IN :status
                AND CORE_Opportunity__r.recordtype.DeveloperName= :OPPORTUNITY_RECORDTYPE
            ];
        }

        return statusTimestampList;
    }

    public static List<CORE_Sub_Status_Timestamp_History__c> getSubStatusTimestampList(DateTime myLastSuccessDate){
        List<CORE_Sub_Status_Timestamp_History__c> subStatusTimestampList;

        if(myLastSuccessDate != null){
            subStatusTimestampList = [SELECT Id,CORE_Opportunity__c,CORE_Status__c,CORE_Sub_status__c,CORE_Modification_Sub_status_Date_time__c,CreatedDate,LastModifiedDate 
                FROM CORE_Sub_Status_Timestamp_History__c 
                WHERE CORE_Status__c NOT IN :status
                AND CORE_Opportunity__r.StageName IN :statusOpportunity
                AND CORE_Opportunity__r.recordtype.DeveloperName= :OPPORTUNITY_RECORDTYPE
                AND ((CORE_Opportunity__r.LastModifiedDate >= :myLastSuccessDate AND CORE_Opportunity__r.LastModifiedDate < TODAY) OR (CORE_Opportunity__r.CreatedDate >= :myLastSuccessDate AND CORE_Opportunity__r.CreatedDate < TODAY))
            ];
        } else {
            subStatusTimestampList = [SELECT Id,CORE_Opportunity__c,CORE_Status__c,CORE_Sub_status__c,CORE_Modification_Sub_status_Date_time__c,CreatedDate,LastModifiedDate 
                FROM CORE_Sub_Status_Timestamp_History__c 
                WHERE CORE_Status__c NOT IN :status
                AND CORE_Opportunity__r.recordtype.DeveloperName= :OPPORTUNITY_RECORDTYPE
            ];
        }

        return subStatusTimestampList;
    }

    
    public static List<CORE_Point_of_Contact__c> getPointContactList(DateTime myLastSuccessDate){
        List<CORE_Point_of_Contact__c> pointContactList;

        if(myLastSuccessDate != null){
            pointContactList = [SELECT Id,CORE_Opportunity__c,CORE_Source__c,CORE_Insertion_Mode__c,CORE_Nature__c,CORE_Enrollment_Channel__c,CORE_Capture_Channel__c,CORE_Origin__c,Campaign_Medium__c,CORE_Campaign_Source__c,CORE_Campaign_Term__c,CORE_Campaign_Content__c,CORE_Campaign_Name__c,CORE_Device__c,CORE_First_visit_datetime__c,CORE_First_page__c,CORE_IP_address__c,CORE_Latitude__c,CORE_Longitude__c,CreatedDate,LastModifiedDate, CORE_Proactive_Prompt__c, CORE_Proactive_Engaged__c, CORE_Chat_With__c,CORE_Insertion_Date__c 
                FROM CORE_Point_of_Contact__c 
                WHERE CORE_Opportunity__r.StageName IN :statusOpportunity
                AND CORE_Opportunity__r.recordtype.DeveloperName= :OPPORTUNITY_RECORDTYPE
                AND ((LastModifiedDate >= :myLastSuccessDate AND LastModifiedDate < TODAY) OR (CreatedDate >= :myLastSuccessDate AND CreatedDate < TODAY))
            ];
        } else {
            pointContactList = [SELECT Id,CORE_Opportunity__c,CORE_Source__c,CORE_Insertion_Mode__c,CORE_Nature__c,CORE_Enrollment_Channel__c,CORE_Capture_Channel__c,CORE_Origin__c,Campaign_Medium__c,CORE_Campaign_Source__c,CORE_Campaign_Term__c,CORE_Campaign_Content__c,CORE_Campaign_Name__c,CORE_Device__c,CORE_First_visit_datetime__c,CORE_First_page__c,CORE_IP_address__c,CORE_Latitude__c,CORE_Longitude__c,CreatedDate,LastModifiedDate, CORE_Proactive_Prompt__c, CORE_Proactive_Engaged__c, CORE_Chat_With__c,CORE_Insertion_Date__c  
                FROM CORE_Point_of_Contact__c
                WHERE CORE_Opportunity__r.recordtype.DeveloperName= :OPPORTUNITY_RECORDTYPE
            ];
        }

        return pointContactList;
    }

    public static List<CORE_Division__c> getDivisionList(DateTime myLastSuccessDate){
        List<CORE_Division__c> divisionList;

        if(myLastSuccessDate != null){
            divisionList = [SELECT Id,CORE_Division_ExternalId__c,CORE_Division_code__c,Name,CreatedDate,LastModifiedDate 
                FROM CORE_Division__c 
                WHERE ((LastModifiedDate >= :myLastSuccessDate AND LastModifiedDate < TODAY) OR (CreatedDate >= :myLastSuccessDate AND CreatedDate < TODAY))

            ];
        } else {
            divisionList = [SELECT Id,CORE_Division_ExternalId__c,CORE_Division_code__c,Name,CreatedDate,LastModifiedDate 
                FROM CORE_Division__c
            ];
        }

        return divisionList;
    }
    
    public static List<CORE_Business_Unit__c> getBusinessUnitList(DateTime myLastSuccessDate){
        List<CORE_Business_Unit__c> businessUnitList;

        if(myLastSuccessDate != null){
            businessUnitList = [SELECT Id,CORE_Business_Unit_ExternalId__c,CORE_Business_Unit_code__c,Name,CORE_Parent_Division__r.CORE_Division_ExternalId__c,CreatedDate,LastModifiedDate 
                FROM CORE_Business_Unit__c 
                WHERE ((LastModifiedDate >= :myLastSuccessDate AND LastModifiedDate < TODAY) OR (CreatedDate >= :myLastSuccessDate AND CreatedDate < TODAY))
            ];
        } else {
            businessUnitList = [SELECT Id,CORE_Business_Unit_ExternalId__c,CORE_Business_Unit_code__c,Name,CORE_Parent_Division__r.CORE_Division_ExternalId__c,CreatedDate,LastModifiedDate 
                FROM CORE_Business_Unit__c
            ];
        }

        return businessUnitList;
    }

    public static List<CORE_Study_Area__c> getStudyAreaList(DateTime myLastSuccessDate){
        List<CORE_Study_Area__c> studyAreaList;

        if(myLastSuccessDate != null){
            studyAreaList = [SELECT Id,CORE_Study_Area_ExternalId__c,CORE_Study_Area_code__c,Name,CORE_Parent_Business_Unit__r.CORE_Business_Unit_ExternalId__c,CreatedDate,LastModifiedDate 
                FROM CORE_Study_Area__c 
                WHERE ((LastModifiedDate >= :myLastSuccessDate AND LastModifiedDate < TODAY) OR (CreatedDate >= :myLastSuccessDate AND CreatedDate < TODAY))
            ];
        } else {
            studyAreaList = [SELECT Id,CORE_Study_Area_ExternalId__c,CORE_Study_Area_code__c,Name,CORE_Parent_Business_Unit__r.CORE_Business_Unit_ExternalId__c,CreatedDate,LastModifiedDate 
                FROM CORE_Study_Area__c
            ];
        }

        return studyAreaList;
    }

    public static List<CORE_Curriculum__c> getCurriculumList(DateTime myLastSuccessDate){
        List<CORE_Curriculum__c> curriculumList;

        if(myLastSuccessDate != null){
            curriculumList = [SELECT Id,CORE_Curriculum_ExternalId__c,CORE_Curriculum_code__c,Name,CORE_Parent_Study_Area__r.CORE_Study_Area_ExternalId__c,CreatedDate,LastModifiedDate 
                FROM CORE_Curriculum__c 
                WHERE ((LastModifiedDate >= :myLastSuccessDate AND LastModifiedDate < TODAY) OR (CreatedDate >= :myLastSuccessDate AND CreatedDate < TODAY))
            ];
        } else {
            curriculumList = [SELECT Id,CORE_Curriculum_ExternalId__c,CORE_Curriculum_code__c,Name,CORE_Parent_Study_Area__r.CORE_Study_Area_ExternalId__c,CreatedDate,LastModifiedDate 
                FROM CORE_Curriculum__c
            ];
        }

        return curriculumList;
    }

    public static List<CORE_Level__c> getLevelList(DateTime myLastSuccessDate){
        List<CORE_Level__c> levelList;

        if(myLastSuccessDate != null){
            levelList = [SELECT Id,CORE_Level_ExternalId__c,CORE_Level_code__c,Name,CORE_Parent_Curriculum__r.CORE_Curriculum_ExternalId__c,CORE_Study_Cycle__c,CORE_Year__c,CreatedDate,LastModifiedDate 
                FROM CORE_Level__c 
                WHERE ((LastModifiedDate >= :myLastSuccessDate AND LastModifiedDate < TODAY) OR (CreatedDate >= :myLastSuccessDate AND CreatedDate < TODAY))
            ];
        } else {
            levelList = [SELECT Id,CORE_Level_ExternalId__c,CORE_Level_code__c,Name,CORE_Parent_Curriculum__r.CORE_Curriculum_ExternalId__c,CORE_Study_Cycle__c,CORE_Year__c,CreatedDate,LastModifiedDate 
                FROM CORE_Level__c
            ];
        }

        return levelList;
    }

    public static List<CORE_Intake__c> getIntakeList(DateTime myLastSuccessDate){
        List<CORE_Intake__c> intakeList;

        if(myLastSuccessDate != null){
            intakeList = [SELECT Id,CORE_Intake_ExternalId__c,CORE_Intake_code__c,Name,CORE_Parent_Level__r.CORE_Level_ExternalId__c,CORE_Session__c,CORE_Academic_Year__c,CreatedDate,LastModifiedDate 
                FROM CORE_Intake__c 
                WHERE ((LastModifiedDate >= :myLastSuccessDate AND LastModifiedDate < TODAY) OR (CreatedDate >= :myLastSuccessDate AND CreatedDate < TODAY))
            ];
        } else {
            intakeList = [SELECT Id,CORE_Intake_ExternalId__c,CORE_Intake_code__c,Name,CORE_Parent_Level__r.CORE_Level_ExternalId__c,CORE_Session__c,CORE_Academic_Year__c,CreatedDate,LastModifiedDate 
                FROM CORE_Intake__c
            ];
        }

        return intakeList;
    }

    public static List<CORE_Deleted_Record__c> getDelList(DateTime myLastSuccessDate){
        List<CORE_Deleted_Record__c> delList;

        if(myLastSuccessDate != null){
            delList = [SELECT Id,CORE_Record_Id__c,CORE_Object_Name__c,CORE_Deletion_Date__c 
                FROM CORE_Deleted_Record__c 
                WHERE CORE_Record_Type__c in :DELETED_RECORDTYPES
                AND CORE_Deletion_Date__c >= :myLastSuccessDate
                AND CORE_Deletion_Date__c < TODAY
            ];
        } else {
            delList = [SELECT Id,CORE_Record_Id__c,CORE_Object_Name__c,CORE_Deletion_Date__c 
                FROM CORE_Deleted_Record__c
                WHERE CORE_Record_Type__c in :DELETED_RECORDTYPES
            ];
        }

        return delList;
    }
}