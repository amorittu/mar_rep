public with sharing class CORE_CUBE_CreateCsv4 implements Schedulable{

    public void execute(SchedulableContext ctx) {
        DateTime myLastSuccess= CORE_CUBE_CreateCsv.getLastSuccesDate();

        DateTime dtBeforeFileGeneration1 = System.now();
        String pc = CORE_CUBE_CreateCsv.getListPointOfContact(myLastSuccess);
        DateTime dtAfterFileGeneration1 = System.now();

        DateTime dtBeforeFileGeneration2 = System.now();
        String div = CORE_CUBE_CreateCsv.getListDivision(myLastSuccess);
        DateTime dtAfterFileGeneration2 = System.now();

        DateTime dtBeforeFileGeneration3 = System.now();
        String bu = CORE_CUBE_CreateCsv.getListBusinessUnit(myLastSuccess);
        DateTime dtAfterFileGeneration3 = System.now();

        CORE_CUBE_CreateCsv.sendToEndpoint('Point_of_Contact',pc, dtBeforeFileGeneration1, dtAfterFileGeneration1);
        CORE_CUBE_CreateCsv.sendToEndpoint('Division',div, dtBeforeFileGeneration2, dtAfterFileGeneration2);
        CORE_CUBE_CreateCsv.sendToEndpoint('Business_Unit',bu, dtBeforeFileGeneration3, dtAfterFileGeneration3);
    }
}