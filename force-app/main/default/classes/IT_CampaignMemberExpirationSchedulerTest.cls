@isTest
public class IT_CampaignMemberExpirationSchedulerTest {
    
    @TestSetup
    public static void makeData() {
        CORE_DataFaker_CatalogHierarchy catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('NB');
        Account account = CORE_DataFaker_Account.getStudentAccount('test1');
        Opportunity opportunity = CORE_DataFaker_Opportunity.getOpportunity(
                catalogHierarchy.division.Id,
                catalogHierarchy.businessUnit.Id,
                account.Id,
                'Applicant',
                'Applicant - Application Submitted / New'
        );

        opportunity.CORE_OPP_Intake__c = catalogHierarchy.intake.Id;
        opportunity.CORE_OPP_Curriculum__c = catalogHierarchy.curriculum.Id;
        opportunity.CORE_OPP_Study_Area__c = catalogHierarchy.studyArea.Id;
        opportunity.CORE_OPP_Level__c = catalogHierarchy.level.Id;
        opportunity.IT_AdmissionOfficer__c = UserInfo.getUserId();
        update opportunity;

        CORE_Business_Unit__c businessUnit = catalogHierarchy.businessUnit;
        businessUnit.CORE_Business_Unit_ExternalId__c = 'NB.2';
        update businessUnit;
        
        Campaign campaign = new Campaign();
        campaign.Name = 'TEST';
        campaign.CORE_External_Id__c = 'TESTT';
        campaign.Status = 'Planned';
        campaign.StartDate = System.today();
        campaign.IsActive = true;
        campaign.Type = IT_CampaignMemberTriggerHandler.OPEN_DAY_PROCESS;
        insert campaign;
        Campaign cm11451 = [SELECT Id, startDate FROM Campaign WHERE Id =: campaign.Id ];
        System.debug('BEFORE CM INSERT: ' + cm11451);   
        System.debug('test class campaign: ' + campaign);
        System.debug('test class campaign.StartDate: ' + campaign.StartDate);
        
        CampaignMember campaignMember = new CampaignMember();
        campaignMember.CampaignId = campaign.Id;
        campaignMember.ContactId = [SELECT Id FROM Contact WHERE AccountId = :account.Id].Id;
        campaignMember.CORE_Opportunity__c = opportunity.Id;
        insert campaignMember;
        
        Campaign cm1112 = [SELECT Id, startDate FROM Campaign WHERE Id =: campaign.Id ];
        System.debug('AFTER CM INSERT: ' + cm1112);   
        
        CampaignMember[] campaignMember2 = [SELECT Id, CampaignId, Campaign.startDate FROM CampaignMember WHERE Id =: campaignMember.Id ];
        for(CampaignMember cm2 : campaignMember2 ) {
            
            System.debug('test class cm2 : ' + cm2);
            System.debug('test class cm2.Campaign.StartDate : ' + cm2.Campaign.StartDate);
            System.debug('test class cm2.CampaignId : ' + cm2.CampaignId);
        }
        IT_MainObjectExpirationChecker__mdt mdt = new IT_MainObjectExpirationChecker__mdt();
        mdt.IT_DaysNumber__c         = 1;
        mdt.IT_FieldChecker__c       = 'Division';
        mdt.IT_ObjectIdentifier__c   = 'CampaignMember';
        mdt.IT_DivisionExternalId__c = 'NB';
        mdt.DeveloperName            = 'NB_CM';
       
        
        IT_ObjectExpirationCheckerBU__mdt mdt2 = new IT_ObjectExpirationCheckerBU__mdt();
        mdt2.IT_BusinessUnitExternalId__c   = 'NB.2';
        mdt2.IT_MainObjectExpirationChecker__c = mdt.Id;
        mdt2.IT_DaysNumber__c               = 2;
    }

    @isTest 
    public static void testBehavior() {
        CampaignMember cm = [SELECT Id, CampaignId, Campaign.StartDate FROM CampaignMember ];
        Campaign campaign = [SELECT id, StartDate FROM Campaign WHERE Id =: cm.CampaignId ];

        System.debug('test class campaign: ' + campaign);
        System.debug('test class campaign: ' + campaign.StartDate);
        
        Test.startTest();

        IT_CampaignMemberExpirationScheduler scheduler = new IT_CampaignMemberExpirationScheduler();
            String chron = '0 0 23 * * ?';        
            System.schedule('Test Sched', chron, scheduler);
        Test.stopTest();
    }
}