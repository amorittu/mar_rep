@RestResource(urlMapping='/aol/opportunity/interest')
global without sharing class CORE_AOL_ChangeInterest {
    private static final String SUCCESS_MESSAGE = 'AOL Change interest & progression fiedls upserted with success';
    
    @TestVisible
    private CORE_OpportunityDataMaker dataMaker;
    @TestVisible
    private CORE_AOL_GlobalWorker aolWorker;
    @TestVisible
    private String aolExternalId;
    @TestVisible
    private CORE_AOL_Wrapper.InterestWrapper interest;
    @TestVisible
    private List<String> errorStatus;


    public CORE_AOL_ChangeInterest(CORE_AOL_GlobalWorker aolWorker, String aolExternalId, CORE_AOL_Wrapper.InterestWrapper interest){
        this.dataMaker = new CORE_OpportunityDataMaker();
        this.aolWorker = aolWorker;
        this.aolExternalId = aolExternalId;
        this.interest = interest;
        this.errorStatus = new List<String> {
            CORE_AOL_Constants.PKL_OPP_STAGENAME_APPLICANT,
            CORE_AOL_Constants.PKL_OPP_STAGENAME_ADMITTED,
            CORE_AOL_Constants.PKL_OPP_STAGENAME_REGISTERED,
            CORE_AOL_Constants.PKL_OPP_STAGENAME_ENROLLED,
            CORE_AOL_Constants.PKL_OPP_STAGENAME_WITHDRAW
        };

    }

    @HttpPut
    global static CORE_AOL_Wrapper.ResultWrapper execute(String aolExternalId, 
                                    CORE_AOL_Wrapper.InterestWrapper interest,
                                    CORE_AOL_Wrapper.AOLProgressionFields aolProgressionFields) {
                               
        Savepoint sp = Database.setSavepoint();

        CORE_AOL_Wrapper.ResultWrapper result = checkMandatoryParameters(aolExternalId, interest);

        // Initiating logs
        Long dt1 = DateTime.now().getTime();
        Long dt2;
        CORE_LogManager logSettings;
        CORE_LOG__c log = new CORE_LOG__c(CORE_API_Name__c = 'AOL');
        Boolean isErrorLog = false;
        logSettings = new CORE_LogManager('AOL');

        try{
            if(result != null){
                //missing parameter(s)
                return result;
            }
            CORE_AOL_GlobalWorker aolWorker = new CORE_AOL_GlobalWorker(aolProgressionFields,aolExternalId);
            Opportunity opportunity = aolWorker.aolOpportunity;

            if(opportunity == null){
                if(isExistingAolExternalIdInLeadBuffer(aolExternalId)){
                    result = new CORE_AOL_Wrapper.ResultWrapper(
                        CORE_AOL_Constants.ERROR_STATUS, 
                        CORE_AOL_Constants.MSG_OPPORTUNITY_NOT_EXIST_YET,
                        null
                    );
                } else {
                    result = new CORE_AOL_Wrapper.ResultWrapper(
                        CORE_AOL_Constants.ERROR_STATUS, 
                        CORE_AOL_Constants.MSG_OPPORTUNITY_NOT_EXIST,
                        null
                    );
                }

            } else {
                CORE_AOL_ChangeInterest process = new CORE_AOL_ChangeInterest(aolWorker, aolExternalId, interest);
                result = process.changeInterest();

                /*
                CORE_AOL_Wrapper.OpportunityWrapper oppWrapper = new CORE_AOL_Wrapper.OpportunityWrapper();
                oppWrapper.Id = newOpportunity.Id;
                oppWrapper.applicationOnlineId = newOpportunity.CORE_OPP_Application_Online_ID__c;
                oppWrapper.sisOpportunityId = newOpportunity?.CORE_OPP_SIS_Opportunity_Id__c;
                oppWrapper.sisStudentAcademicRecordId = newOpportunity?.CORE_OPP_SIS_Student_Academic_Record_ID__c;
                oppWrapper.accountId = newOpportunity.AccountId;

                result = new CORE_AOL_Wrapper.ResultWrapper(CORE_AOL_Constants.SUCCESS_STATUS, SUCCESS_MESSAGE, oppWrapper);*/
            }
        } catch(Exception e){
            result = new CORE_AOL_Wrapper.ResultWrapper(CORE_AOL_Constants.ERROR_STATUS, e.getMessage() + ' at ' + e.getStackTraceString(),null);
            Database.rollback(sp);
        } finally{
            dt2 = DateTime.now().getTime();
            System.debug('CORE_AOL_AcademicDiplomaHistory(urlMapping=\'/aol/academic-diploma-history\') : T0TAL TIME OF EXECUTION = ' + (dt2-dt1) + ' ms');
            // creating log
            log.CORE_Processing_Duration__c =(dt2-dt1);
            log.CORE_HTTP_Status_Code__c = result.statut == 'KO' ? 400 : result.statut == 'OK' ? 200 : 500;
            log.CORE_Sent_Body__c = JSON.serialize(result);
            String reponseJSON = '{"aolExternalId" : "' + aolExternalId + '",';
            reponseJSON += '"interest" : ' + JSON.serialize(interest) + ',"aolProgressionFields" : ' + JSON.serialize(aolProgressionFields) + '}';
            log.CORE_Received_Body__c =  reponseJSON;
            
            if(!logSettings.isNone()){
                if(logSettings.isAllLogged()){ // is All logged
                    CORE_LogManager.createLog(log);
                }else if(logSettings.isErrorLogged() && isErrorLog){ // is Error
                    CORE_LogManager.createLog(log);
                }
            }
        }

        return result;
    }

    public CORE_AOL_Wrapper.ResultWrapper changeInterest(){
        CORE_AOL_Wrapper.ResultWrapper result = new CORE_AOL_Wrapper.ResultWrapper();
        Opportunity opportunity = this.aolWorker.aolOpportunity;

        //creation
        if(this.errorStatus.contains(opportunity.StageName) || opportunity.IsClosed){
            result.statut = CORE_AOL_Constants.ERROR_STATUS;
            result.message = CORE_AOL_Constants.MSG_OPPORTUNITY_CHANGE_INTEREST_LOCKED_STATUS;

            return result;
        }

        CORE_CatalogHierarchyModel catalogHierarchy = getCatalogHierarchy(this.interest);

        //reorientation
        if(catalogHierarchy.businessUnit.Id != opportunity.CORE_OPP_Business_Unit__c){
            result.statut = CORE_AOL_Constants.ERROR_STATUS;
            result.message = CORE_AOL_Constants.MSG_OPPORTUNITY_CHANGE_INTEREST_DIFFERENT_BU;
            
            return result;
        }

        CORE_Intake__c intake = CORE_AOL_GlobalWorker.getIntakeFromExternalId(this.interest.intake);
        if(intake == null){
            throw new IllegalArgumentException('Intake not found.');
        }

        //deferal
        if(catalogHierarchy.intake.CORE_Academic_Year__c != opportunity.CORE_OPP_Academic_Year__c){
            result.statut = CORE_AOL_Constants.ERROR_STATUS;
            result.message = CORE_AOL_Constants.MSG_OPPORTUNITY_CHANGE_INTEREST_DIFFERENT_AY;
            
            return result;
        }

        //same opp
        if(catalogHierarchy.studyArea.Id == opportunity.CORE_OPP_Study_Area__c 
            && catalogHierarchy.curriculum.Id == opportunity.CORE_OPP_Curriculum__c
            && catalogHierarchy.level.Id == opportunity.CORE_OPP_Level__c
            && catalogHierarchy.intake.Id == opportunity.CORE_OPP_Intake__c
        ){
            result.statut = CORE_AOL_Constants.SUCCESS_STATUS;
            result.message = CORE_AOL_Constants.MSG_OPPORTUNITY_CHANGE_INTEREST_ALREADY_EXIST;
            result.opportunity = new CORE_AOL_Wrapper.OpportunityWrapper(opportunity);
            this.aolWorker.updateAolProgressionFields();

            return result;
        }

        Id oldProductId = opportunity.CORE_Main_Product_Interest__c;

        //CHANGE INTEREST
        opportunity = this.dataMaker.initOpportunityCatalog(opportunity, catalogHierarchy);

        //opp is update with progression fields
        this.aolWorker.updateOppIsNeeded = true;
        this.aolWorker.updateAolProgressionFields();

        //remove old line item
        if(!String.isBlank(oldProductId)){
            List<OpportunityLineItem> oppLineItemsToDelete = CORE_AOL_QueryProvider.getOppLineItems(opportunity.Id, oldProductId);
            if(!oppLineItemsToDelete.isEmpty()){
                Delete oppLineItemsToDelete;
            }
        }
        
        //insert new line item
        CORE_AOL_ChangeInterest.getOppLineItem(this.datamaker, opportunity, false);

        result.statut = CORE_AOL_Constants.SUCCESS_STATUS;
        result.message = SUCCESS_MESSAGE;
        result.opportunity = new CORE_AOL_Wrapper.OpportunityWrapper(opportunity);

        return result;
    }

    public static CORE_CatalogHierarchyModel getCatalogHierarchy(CORE_AOL_Wrapper.InterestWrapper interest){
        CORE_CatalogHierarchyModel catalogHierarchy = new CORE_CatalogHierarchyModel(
            null, 
            interest.businessUnit, 
            interest.studyArea,
            interest.curriculum,
            interest.level,
            interest.intake
        );

        catalogHierarchy = CORE_OpportunityDataMaker.getCatalogHierarchy(catalogHierarchy,true);

        return catalogHierarchy;
    }

    @TestVisible
    public static OpportunityLineItem getOppLineItem(CORE_OpportunityDataMaker dataMaker, Opportunity opportunity, Boolean checkExistingLineItems){
        OpportunityLineItem oppLineItem;

        Map<Id,Id> buByProducts = new Map<Id,Id>();

        //no need to continue because there is no products
        if(String.IsBlank(opportunity.CORE_Main_Product_Interest__c)){
            return oppLineItem;
        }

        if(checkExistingLineItems){
            List<OpportunityLineItem> existingLineItems = CORE_AOL_QueryProvider.getOppLineItems(opportunity.Id, opportunity.CORE_Main_Product_Interest__c);

            if(!existingLineItems.isEmpty()){
                return oppLineItem;
            }
        }

        if(!String.IsBlank(opportunity.CORE_Main_Product_Interest__c) && !String.IsBlank(opportunity.CORE_OPP_Business_Unit__c)){
            buByProducts.put(opportunity.CORE_Main_Product_Interest__c,opportunity.CORE_OPP_Business_Unit__c);
        }


        Map<Id,PricebookEntry> pricebookEntries = dataMaker.getPricebookEntryByProducts(buByProducts);
        System.debug('@getOppLineItem > pricebookEntries = '+ pricebookEntries);

        PricebookEntry pricebookEntry = pricebookEntries.get(opportunity.CORE_Main_Product_Interest__c);
        System.debug('@getOppLineItem > pricebookEntries = '+ pricebookEntry);

        if(pricebookEntry != null){
            oppLineItem = dataMaker.initOppLineItem(opportunity, pricebookEntry);
            System.debug('@getOppLineItem > oppLineItem = '+ oppLineItem);
        }

        if(oppLineItem != null){
            insert oppLineItem;
        }
        
        return oppLineItem;
    }

    private static boolean isExistingAolExternalIdInLeadBuffer(String aolExternalId){
        return !CORE_AOL_QueryProvider.getLeadBuffer(aolExternalId).isEmpty();
    }

    @TestVisible
    private static CORE_AOL_Wrapper.ResultWrapper checkMandatoryParameters(String aolExternalId, CORE_AOL_Wrapper.InterestWrapper interest){
        List<String> missingParameters = new List<String>();
        Boolean isMissingAolId = CORE_AOL_GlobalWorker.isAolExternalIdParameterMissing(aolExternalId);

        if(isMissingAolId){
            missingParameters.add('aolExternalId');
        }
        
        missingParameters.addAll(getInterestMissingParameters(interest));

        CORE_AOL_Wrapper.ResultWrapperGlobal resultTmp = CORE_AOL_GlobalWorker.getMissingParameters(missingParameters);

        if(resultTmp == null){
            return null;
        } else {
            CORE_AOL_Wrapper.ResultWrapper result = new CORE_AOL_Wrapper.ResultWrapper(resultTmp.status, resultTmp.message, null);
            return result;
        }
    }

    public static List<String> getInterestMissingParameters(CORE_AOL_Wrapper.InterestWrapper interest){
        List<String> mandatoryParameters = new List<String> {'businessUnit', 'studyArea', 'curriculum', 'level', 'intake'};
        String wrapperName = 'interest';

        List<String> missingParameters = new List<String>();

        if(interest == null){
            missingParameters.add(wrapperName);
            return missingParameters;
        }

        for(String mandatoryParameter : mandatoryParameters){
            if(String.IsBlank((String) interest.get(mandatoryParameter))){
                missingParameters.add(String.format('{0}.{1}', new List<Object>{wrapperName, mandatoryParameter}));
            }
        }
        
        return missingParameters;
    }
}