/**
 * Created by SaverioTrovatoNigith on 19/04/2022.
 */

@IsTest
private class IT_InvocableCheckStudentQuestionTest {
    @TestSetup
    static void setup(){
        Task t = new Task();
        t.Description = 'Student Question : Test';
        insert t;
    }
	@IsTest
	static void testBehavior() {
        List<Task> tasks = [SELECT Id,Description FROM Task LIMIT 1];
        List<IT_InvocableCheckStudentQuestion.FlowInputs> inputs = new List<IT_InvocableCheckStudentQuestion.FlowInputs>();
        Test.startTest();
        IT_InvocableCheckStudentQuestion.FlowInputs input = new IT_InvocableCheckStudentQuestion.FlowInputs();
        input.incomingTask = tasks[0];
        inputs.add(input);
        IT_InvocableCheckStudentQuestion.getOutputs(inputs);
        Test.stopTest();
	}
}