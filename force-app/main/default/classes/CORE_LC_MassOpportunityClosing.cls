/**
* @author Zameer Khodabocus - Almavia
* @date 2021-12-13
* @group Lightning Web Component
* @description Class used by lwc, coreMassOppClosing, to close won/lost opportunities in mass
* @test class CORE_LC_MassOpportunityClosing_TEST
*/
public without sharing class CORE_LC_MassOpportunityClosing {

    public static final String EMAIL_SETTING_NAME = 'Mass_Opportunity_Closing';
    public static final Integer defaultBatchSize = 100;

    public class SettingWrapper {
        @AuraEnabled public String settingId { get; set; }
        @AuraEnabled public Integer batchSize { get; set; }
        @AuraEnabled public Boolean jobInProgress { get; set; }
        @AuraEnabled public String listViewFilter { get; set; }
    }

    public class ListViewWrapper {
        @AuraEnabled public String viewId { get; set; }
        @AuraEnabled public String viewLabel { get; set; }
        @AuraEnabled public String viewName { get; set; }
        @AuraEnabled public String viewQuery { get; set; }
    }

    public class CurrentViewWrapper {
        @AuraEnabled public String viewId { get; set; }
        @AuraEnabled public String viewQuery { get; set; }
        @AuraEnabled public Integer viewRecordsSize { get; set; }
    }

    public CORE_LC_MassOpportunityClosing(){

    }

    @RemoteAction
    public static List<ListViewWrapper> getListViewsInfo(){
        SettingWrapper orgSetting = getOrgSetting();
        //system.debug('@getListViews > orgSetting: ' + orgSetting);
        //system.debug('@getListViews > userSetting: ' + userSetting);

        List<ListViewWrapper> listViewsList = new List<ListViewWrapper>();
        String filter = '';
        String query = 'SELECT Id, Name, DeveloperName, SobjectType FROM ListView WHERE SobjectType = \'Opportunity\'';

        if(orgSetting.listViewFilter != null){
            filter = '%' + orgSetting.listViewFilter + '%';
            query += ' AND DeveloperName LIKE :filter';
        }

        //Get list views of object
        for(ListView lv : database.query(query)){
            ListViewWrapper newLV = new ListViewWrapper();

            newLV.viewId = lv.Id;
            newLV.viewLabel = lv.Name;
            newLV.viewName = lv.DeveloperName;

            HttpRequest req = new HttpRequest();
            req.setEndpoint('callout:CORE_MassOpportunityEndpoint' + '/Opportunity/listviews/' + lv.Id + '/describe');
            //req.setEndpoint('https://ggecs--mvpdev.my.salesforce.com/services/data/v50.0/sobjects/Opportunity/listviews/' + lv.Id + '/describe');
            req.setHeader('Authorization', 'Bearer ' + userinfo.getSessionId());
            req.setMethod('GET');
            Http http = new Http();
            HttpResponse res = http.send(req);

            if(res.getStatusCode() == 200){
                Map<String, Object> tokenRespMap = (Map<String, Object>)JSON.deserializeUntyped(res.getBody());

                newLV.viewQuery = (String)tokenRespMap.get('query');//.replaceAll('\'', '"');
            }

            listViewsList.add(newLV);
        }

        system.debug('@getOppListViews > listViewsList: ' + JSON.serializePretty(listViewsList));

        return listViewsList;
    }

    @AuraEnabled (cacheable=true)
    public static SettingWrapper getOrgSetting(){

        SettingWrapper settingWrap = new SettingWrapper();

        CORE_MassOpportunityClosing__c orgsetting = CORE_MassOpportunityClosing__c.getOrgDefaults();

        if(orgsetting.Id == null){
            AuraHandledException e = new AuraHandledException(Label.CORE_MOC_MSG_NO_SETTINGS);
            e.setMessage(Label.CORE_MOC_MSG_NO_SETTINGS);
            throw e;
        }

        settingWrap.settingId = orgsetting.Id;
        settingWrap.batchSize = (orgsetting.CORE_BatchSize__c != null) ? orgsetting.CORE_BatchSize__c.intValue() :defaultBatchSize;
        settingWrap.listViewFilter = orgsetting.CORE_ListViewNameFilter__c;
        settingWrap.jobInProgress = orgsetting.CORE_JobInProgress__c;

        return settingWrap;
    }

    /*@AuraEnabled (cacheable=true)
    public static List<ListViewWrapper> getListViews(){
        try {

            SettingWrapper orgSetting = getOrgSetting();
            //system.debug('@getListViews > orgSetting: ' + orgSetting);
            //system.debug('@getListViews > userSetting: ' + userSetting);

            List<ListViewWrapper> listViewsList = new List<ListViewWrapper>();
            String filter = '';
            String query = 'SELECT Id, Name, DeveloperName, SobjectType FROM ListView WHERE SobjectType = \'Opportunity\'';

            if(orgSetting.listViewFilter != null){
                filter = '%' + orgSetting.listViewFilter + '%';
                query += ' AND DeveloperName LIKE :filter';
            }

            //Get list views of object
            for(ListView lv : database.query(query)){
                ListViewWrapper newLV = new ListViewWrapper();

                newLV.viewId = lv.Id;
                newLV.viewLabel = lv.Name;
                newLV.viewName = lv.DeveloperName;

                listViewsList.add(newLV);
            }

            //system.debug('@getOppListViews > listViewsList: ' + JSON.serializePretty(listViewsList));

            return listViewsList;

        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }*/

    @AuraEnabled (cacheable=true)
    public static CurrentViewWrapper getListViewData(Object view) {
        //system.debug('@getListViewData > view: ' + view);

        try{
            Map<String, Object> viewsMap = (Map<String, Object>)JSON.deserializeUntyped(JSON.serialize(view));
            system.debug('@getListViewData > viewsMap: ' + viewsMap);

            CurrentViewWrapper newCurrentView = new CurrentViewWrapper();
            newCurrentView.viewId = (String)viewsMap.get('value');
            system.debug('@getListViewData > newCurrentView.viewId: ' + newCurrentView.viewId);
            newCurrentView.viewQuery = ((String)viewsMap.get('query')).unescapeHtml4();
            system.debug('@getListViewData > newCurrentView.viewQuery: ' + newCurrentView.viewQuery);

            newCurrentView.viewRecordsSize = database.query(newCurrentView.viewQuery).size();

            return newCurrentView;
        }
        catch(Exception e){
            throw new AuraHandledException(e.getMessage() + ' => ' + e.getStackTraceString());
        }
    }
    /*@AuraEnabled (cacheable=true)
    public static CurrentViewWrapper getListViewData(String viewId) {

	    HttpRequest req = new HttpRequest();
        req.setEndpoint('callout:CORE_MassOpportunityEndpoint' + '/Opportunity/listviews/' + viewId + '/describe');
        //req.setEndpoint('https://ggecs--mvpdev.my.salesforce.com/services/data/v50.0/sobjects/Opportunity/listviews/' + viewId + '/describe');
        req.setHeader('Authorization', 'Bearer ' + userinfo.getSessionId());
        req.setMethod('GET');
        Http http = new Http();
        HttpResponse res = http.send(req);

        if(res.getStatusCode() == 200){
            Map<String, Object> tokenRespMap = (Map<String, Object>)JSON.deserializeUntyped(res.getBody());

            String query = ((String)tokenRespMap.get('query'));//.toUpperCase()
            /*Set<String> columnsSet = new Set<String>();

            for(Object col : (List<Object>)tokenRespMap.get('columns')){
                Map<String, Object> colsMap = (Map<String, Object>)JSON.deserializeUntyped((String)JSON.serialize(col));
                columnsSet.add(((String)colsMap.get('fieldNameOrPath')).toUpperCase());
            }*

            CurrentViewWrapper newCurrentView = new CurrentViewWrapper();
            newCurrentView.viewId = viewId;
            newCurrentView.viewQuery = query;
            newCurrentView.viewRecordsSize = database.query(query).size();

            return newCurrentView;
        }
        else{
            throw new AuraHandledException(res.getStatusCode() + ' => ' + res.getStatus());
        }
    }*/

    @AuraEnabled
    public static String runCloseOpps(CurrentViewWrapper viewSelected, String status){
        system.debug('@closeOpps > viewSelected: ' + viewSelected);

        try {

            SettingWrapper setting = getOrgSetting();
            system.debug('@closeOpps > setting: ' + setting);

            Map<Id, Opportunity> oppsMap = new Map<Id, Opportunity>(
                ((List<Opportunity>)database.query(viewSelected.viewQuery))
            );

            List<String> resultsList = new List<String> {
                String.valueOf(oppsMap.size()), '0', '0', ''
            };

            if(setting.batchSize >= viewSelected.viewRecordsSize){//Run sync. update
                closeOpps(oppsMap.values(), status, resultsList, true);

                return null;
            }
            else{//Run async. update

                database.update(new CORE_MassOpportunityClosing__c(
                    Id = setting.settingId, CORE_JobInProgress__c = true
                ), true);

                System.enqueueJob(new CORE_QU_MassOpportunityClosing(oppsMap.keySet(), status, resultsList));

                return 'ASYNC';
            }
            
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    public static void closeOpps(List<Opportunity> oppsList, String status, List<String> resultsList, Boolean sendEmail){
        List<Opportunity> updatedOppsList = new List<Opportunity>();

        for(Opportunity opp : oppsList){
            updatedOppsList.add(
                new Opportunity(
                    Id = opp.Id,
                    StageName = status,
                    CORE_Change_Status__c = true
                )
            );
        }

        List<Database.SaveResult> results = database.update(updatedOppsList, false);
        
        CORE_MassOpportunityResultHandler resultHandler = new CORE_MassOpportunityResultHandler();
        resultHandler.settingName = EMAIL_SETTING_NAME;
        resultHandler.results = results;
        resultHandler.recordsList = oppsList;
        resultHandler.resultsList = resultsList;
        resultHandler.sendEmail = sendEmail;
        resultHandler.handleResults();
    }
}