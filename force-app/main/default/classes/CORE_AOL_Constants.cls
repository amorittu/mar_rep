public with sharing class CORE_AOL_Constants {

    //CORE Prefix
    public static final String PREFIX_CORE = 'CORE_';

    /* OPPORTUNITY OBJECT */
    //recordTypes
    public static final String OPP_RECORDTYPE_ENROLLMENT = 'CORE_Enrollment';
    //StageName
    public static final String PKL_OPP_STAGENAME_PROSPECT = 'Prospect';
    public static final String PKL_OPP_STAGENAME_LEAD = 'Lead';
    public static final String PKL_OPP_STAGENAME_APPLICANT = 'Applicant';
    public static final String PKL_OPP_STAGENAME_ADMITTED = 'Admitted';
    public static final String PKL_OPP_STAGENAME_REGISTERED = 'Registered';
    public static final String PKL_OPP_STAGENAME_ENROLLED = 'Enrolled';
    public static final String PKL_OPP_STAGENAME_WITHDRAW = 'Withdraw';
    
    //Sub-status
    public static final String PKL_OPP_SUBSTATUS_PROSPECTNEW = 'Prospect - New';
    public static final String PKL_OPP_SUBSTATUS_PROSPECTAPPSTARTED = 'Prospect - Application Started';
    public static final String PKL_OPP_SUBSTATUS_LEADAPPSTARTED = 'Lead - Application Started';
    public static final String PKL_OPP_SUBSTATUS_APPLICANT_APP_SUBMITTED = 'Applicant - Application Submitted / New';
    public static final String PKL_OPP_SUBSTATUS_APPLICANT_APP_PROGRESSING = 'Applicant - Application Incomplete / Progressing';
    public static final String PKL_OPP_SUBSTATUS_APPLICANT_APP_COMPLETED = 'Applicant - Application Completed / Eligible';
    public static final String PKL_OPP_SUBSTATUS_ADMITTED_ADMIT = 'Admitted - Admit';
    public static final String PKL_OPP_SUBSTATUS_REGISTERED_CONTRACT_RECEIVED = 'Registered - Contract received';
    public static final String PKL_OPP_SUBSTATUS_REGISTERED_FEES_PAYMENTS = 'Registered - Tuition Fees payments';
    public static final String PKL_OPP_SUBSTATUS_REGISTERED_FEES_FULLY_PAID = 'Registered - Fees Fully Paid';
    public static final String PKL_OPP_SUBSTATUS_REGISTERED_AWAITING_VISA = 'Registered - Awaiting Visa';
    public static final String PKL_OPP_SUBSTATUS_REGISTERED_VISA_DENIAL = 'Registered - Exit - Visa denial';
    public static final String PKL_OPP_SUBSTATUS_ENROLLED_CLASSICAL = 'Enrolled - Classical';
    public static final String PKL_OPP_SUBSTATUS_REORIENTED = '{0} - Exit - Reoriented';
    public static final String PKL_OPP_SUBSTATUS_TERM_DEFERRAL = '{0} - Exit - Term Deferral';

    /* Product object */
    //CORE_Product_Type__c
    public static final String PKL_PRODUCT_TYPE = 'CORE_PKL_Short_Course';

    /* Acquisition Point of contact OBJECT */
    //CORE_Enrollment_Channel__c
    public static final String PKL_ACQPOC_ENROLCHANNEL_DIRECT = 'CORE_PKL_Direct';
    public static final String PKL_ACQPOC_ENROLCHANNEL_RETAIL = 'CORE_PKL_Retail';

    //CORE_Insertion_Mode__c
    public static final String PKL_ACQPOC_INSERTMODE_AUTOMATIC = 'CORE_PKL_Automatic';

    //CORE_Nature__c
    public static final String PKL_ACQPOC_NATURE_ONLINE = 'CORE_PKL_Online';

    //CORE_Capture_Channel__c
    public static final String PKL_ACQPOC_CAPCHANNEL_AOL = 'CORE_PKL_AOL';//AOL value missing

    /* API values */
    //API PARAM Labels
    public static final String PARAM_ADDITIONAL_FIELDS = 'additionalFields';
    public static final String PARAM_AOL_EXTERNALID = 'aolExternalId';
    public static final String PARAM_AOL_SCHOLARSHIP = 'scholarship';
    public static final String PARAM_AOL_DOC_SENDING = 'documentSendingFields';
    public static final String PARAM_AOL_SELECTION = 'selectionTest';
    public static final String PARAM_AOL_APP_FEES_PAID = 'applicationFeesPaid';
    public static final String PARAM_AOL_ENROLL_FEES_PAID = 'enrollmentFeesPaid';
    public static final String PARAM_AOL_TUITION_FEES_PAID = 'tuitionFeesPaid';
    public static final String PARAM_AOL_ACCOUNT_CIVIL_STATUS = 'account';
    public static final String PARAM_AOL_ACCOUNT_FOREIGN_LANG = 'foreignLanguages';
    public static final String PARAM_ACADEMIC_DIPLOMA_HISTORY = 'academicDiplomaHistory';
    public static final String PARAM_AOL_DOC = 'aolDocuments';
    public static final String PARAM_AOL_APP_DOC_URL = 'onlineApplicationDocumentUrl';
    public static final String PARAM_AOL_PROG_FIELDS = 'aolProgressionFields';

    //API SUCCESS STATUS 
    public static final String SUCCESS_STATUS = 'OK';

    //API SUCCESS STATUS 
    public static final String ERROR_STATUS = 'KO';
    public static final String MSG_MISSING_PARAMETERS = 'Missing mandatory parameter : ';

    //API STATUS OPP EXIST
    public static final String OPPORTUNITY_EXIST = 'Yes';
    public static final String OPPORTUNITY_PENDING = 'Pending';
    public static final String OPPORTUNTIY_NOT_EXIST = 'No';

    //OPPORTUNITY NOT FOUND msg
    public static final String MSG_OPPORTUNITY_NOT_FOUND = 'Opportunity not found with aolExternId = "{0}"';   
    public static final String MSG_OPPORTUNITY_PENDING = 'Opportunity with aolExternId = "{0}" Pending';
    public static final String MSG_OPPORTUNITY_NOT_EXIST = 'Opportunity not exist in the CRM';
    public static final String MSG_OPPORTUNITY_NOT_EXIST_YET = 'Opportunity not exist yet in the CRM';
    public static final String MSG_OPPORTUNITY_CHANGE_INTEREST_LOCKED_STATUS = 'Opportunity locked status upper Lead. AOL has to create a new Opportunity';
    public static final String MSG_OPPORTUNITY_CHANGE_INTEREST_DIFFERENT_BU = 'Business Unit is différent from selected Opportunity. AOL has to create a new Opportunity';
    public static final String MSG_OPPORTUNITY_CHANGE_INTEREST_DIFFERENT_AY = 'Intake AcademicYear is différent from selected Opportunity. AOL has to create a new Opportunity';
    public static final String MSG_OPPORTUNITY_CHANGE_INTEREST_ALREADY_EXIST = 'Same Opportunity already exist in CRM';
}