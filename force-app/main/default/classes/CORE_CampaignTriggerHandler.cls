public class CORE_CampaignTriggerHandler {
    
    private static final String PKL_STATUS_POSTPONED = 'Postponed';
    private static final String PKL_STATUS_PLANNED = 'Planned';
    
    public static void handleTrigger(List<Campaign> oldRecords, List<Campaign> newRecords, System.TriggerOperation triggerEvent) {
        System.debug('CORE_CampaignHandleTrigger : START');
        switch on triggerEvent {
            when BEFORE_INSERT {
                beforeInsert(newRecords);
            }
            when AFTER_INSERT {
                afterInsert(newRecords);
            }
            when BEFORE_UPDATE {
                Map<Id,Campaign> oldRecordsMap = new Map<Id,Campaign> (oldRecords);
                beforeUpdate(oldRecordsMap, newRecords);
            }
            when AFTER_UPDATE {
             //   afterUpdate(oldRecords, newRecords); ESCUDO, issue with CPU time limit exceeded for Lead Acquisition
            }
        }
        System.debug('CORE_CampaignHandleTrigger : END');
    }

    @TestVisible
    private static void beforeInsert(List<Campaign> newRecords) {

        String timezone = [SELECT Id, TimeZoneSidKey FROM Organization].TimeZoneSidKey;

        for(Campaign campaign : newRecords){
            if(campaign.CORE_Start_Date_and_hour__c != null){
                if(campaign.CORE_Timezone__c != null){
                    timezone = campaign.CORE_Timezone__c.substring(campaign.CORE_Timezone__c.lastIndexOf('(') + 1, campaign.CORE_Timezone__c.lastIndexOf(')'));
                }

                campaign.CORE_Start_hour_Text__c = campaign.CORE_Start_Date_and_hour__c.format('kk:mm', timezone);
            }

            if(campaign.CORE_End_Date_and_hour__c != null){
                if(campaign.CORE_Timezone__c != null){
                    timezone = campaign.CORE_Timezone__c.substring(campaign.CORE_Timezone__c.lastIndexOf('(') + 1, campaign.CORE_Timezone__c.lastIndexOf(')'));
                }

                campaign.CORE_End_hour_Text__c = campaign.CORE_End_Date_and_hour__c.format('kk:mm', timezone);
            }
        }
    }

    @TestVisible
    private static void afterInsert(List<Campaign> newRecords) {
        System.debug('CORE_CampaignHandleTrigger After Insert : START');

        Set<Id> onlineCampaignIds = new Set<Id>();

        for(Campaign campaign : newRecords){
            if(campaign.CORE_Online_event__c && campaign.CORE_Livestorm_Event_Id__c == null){
                onlineCampaignIds.add(campaign.Id);
            }
        }

        System.debug(onlineCampaignIds.size() + ' campaigns are created');
        if (onlineCampaignIds.size() > 0){
            CORE_Livestorm_CreateCampaign campaignBatchableCreate = new CORE_Livestorm_CreateCampaign(onlineCampaignIds);
            database.executebatch(campaignBatchableCreate, 25);
        }

        System.debug('CORE_CampaignHandleTrigger After Insert : END');
    }

    @TestVisible
    private static void beforeUpdate(Map<Id, Campaign> oldRecordsMap, List<Campaign> newRecords) {

        String timezone = [SELECT Id, TimeZoneSidKey FROM Organization].TimeZoneSidKey;

        for(Campaign campaign : newRecords){
            Campaign oldCampaign = oldRecordsMap.get(campaign.Id);

            if(oldCampaign.CORE_Start_Date_and_hour__c != campaign.CORE_Start_Date_and_hour__c
               || oldCampaign.CORE_Timezone__c != campaign.CORE_Timezone__c){

                if(campaign.CORE_Start_Date_and_hour__c != null){
                    if(campaign.CORE_Timezone__c != null){
                        timezone = campaign.CORE_Timezone__c.substring(campaign.CORE_Timezone__c.lastIndexOf('(') + 1, campaign.CORE_Timezone__c.lastIndexOf(')'));
                    }

                    campaign.CORE_Start_hour_Text__c = campaign.CORE_Start_Date_and_hour__c.format('kk:mm', timezone);
                }
                else{
                    campaign.CORE_Start_hour_Text__c = null;
                }
            }

            if(oldCampaign.CORE_End_Date_and_hour__c != campaign.CORE_End_Date_and_hour__c
                || oldCampaign.CORE_Timezone__c != campaign.CORE_Timezone__c) {

                if(campaign.CORE_End_Date_and_hour__c != null) {
                    if(campaign.CORE_Timezone__c != null) {
                        timezone = campaign.CORE_Timezone__c.substring(campaign.CORE_Timezone__c.lastIndexOf('(') + 1, campaign.CORE_Timezone__c.lastIndexOf(')'));
                    }

                    campaign.CORE_End_hour_Text__c = campaign.CORE_End_Date_and_hour__c.format('kk:mm', timezone);
                }
                else{
                    campaign.CORE_End_hour_Text__c = null;
                }
            }
        }
        
        //Fin du premier process
        //Début du remplacement du flow "CORE Event Postpon - V5"
        for (Campaign camp : newRecords){
            Campaign oldCampaign = oldRecordsMap.get(camp.Id);
	        if (camp.Status == PKL_STATUS_PLANNED && camp.CORE_Start_Date_and_hour__c != oldCampaign.CORE_Start_Date_and_hour__c){
    	        camp.Status = PKL_STATUS_POSTPONED;
        	    camp.CORE_End_Date_before_postpon__c = oldCampaign.CORE_End_Date_and_hour__c;
            	camp.CORE_Start_Date_before_postpon__c = oldCampaign.CORE_Start_Date_and_hour__c;
        	}
        }
    }
/*
    @TestVisible
    private static void afterUpdate(List<Campaign> oldRecords, List<Campaign> newRecords) {
        System.debug('CORE_CampaignHandleTrigger After Update : START');

        Set<Id> onlineCampaignIds = new Set<Id>();

        for(Campaign campaign : newRecords){
            if(campaign.CORE_Online_event__c){
                for(Campaign oldCampaign : oldRecords){
                    if(oldCampaign.Id == campaign.Id && oldCampaign.CORE_Livestorm_Event_Id__c != null &&
                    (oldCampaign.CORE_Start_Date_and_hour__c != campaign.CORE_Start_Date_and_hour__c ||
                    oldCampaign.CORE_Timezone__c != campaign.CORE_Timezone__c ||
                    oldCampaign.CORE_Campaign_Full_Name__c != campaign.CORE_Campaign_Full_Name__c ||
                    oldCampaign.Name != campaign.Name ||
                    oldCampaign.Description != campaign.Description)){
                        onlineCampaignIds.add(campaign.Id);
                    }
                }
            }
        }

        System.debug(onlineCampaignIds.size() + ' campaigns are updated');
        if (onlineCampaignIds.size() > 0){
            CORE_Livestorm_UpdateCampaign campaignBatchableUpdated = new CORE_Livestorm_UpdateCampaign(onlineCampaignIds);
            database.executebatch(campaignBatchableUpdated, 25);
        }

        Map<Id,Campaign> oldCampaigns = new Map<Id,Campaign> (oldRecords);
        Set<Id> onlineCampaignStatus = new Set<Id>();

        for(Campaign campaign : newRecords){
            Campaign oldCampaign = oldCampaigns.get(campaign.Id);
            if(campaign.CORE_Online_event__c && campaign.status == 'Aborted' && oldCampaign.status != 'Aborted'){
                onlineCampaignStatus.add(campaign.Id);
            }
        }

        System.debug(onlineCampaignStatus.size() + ' campaigns status are updated');
        if (onlineCampaignStatus.size() > 0){
            CORE_Livestorm_DeleteSession campaignBatchableDeleted = new CORE_Livestorm_DeleteSession(onlineCampaignStatus);
            database.executebatch(campaignBatchableDeleted, 25);
        }
        
        //Fin du premier process
		//Début du remplacement du flow "CORE Update Campaing Status on related campaign members  - V2" et du flow "CORE Event Postpon - V5" (fusion des deux ici)
		
        //On récup toutes campaign members
        List<CampaignMember> allCampaignMemberList = [SELECT Id, CampaignId, CORE_Campaign_Statut__c FROM CampaignMember WHERE CampaignId IN :newRecords];
        
        //On place dans une map le status de chaque campaign
        Map<Id, String> mapCampaignStatus = new Map<Id, String>();
        for (Campaign camp : newRecords){
            mapCampaignStatus.put(camp.Id, camp.Status);
        }
        
        //On va updater les campaigns members et les ajouter à la liste à update
        List<CampaignMember> campMemToUpdate = new List<CampaignMember>();
        for (CampaignMember campMem : allCampaignMemberList){
            if (campMem.CORE_Campaign_Statut__c != mapCampaignStatus.get(campMem.CampaignId)){
                campMem.CORE_Campaign_Statut__c = mapCampaignStatus.get(campMem.CampaignId);
                campMemToUpdate.add(campMem);
            }
        }
        
        update campMemToUpdate;
        
        System.debug('CORE_CampaignHandleTrigger After Update : END');
    }
*/
}