global class IT_InvocableIntakeStarted {
    
    @InvocableMethod
    global static List<FlowOutputs> getNextBH(List<FlowInputs> request){
        Boolean toReturn = IT_Utility.isIntakeStarted(request[0].intake, request[0].academicYearStartMonth );
        
        CORE_Intake__c intake = request[0].intake;

        FlowOutputs flOutput= new FlowOutputs();
        flOutput.isStarted = toReturn;

        List<FlowOutputs> returnList = new List<FlowOutputs>();
        returnList.add(flOutput);
        return returnList;
//intake, opty.CORE_OPP_Business_Unit__r.IT_AcademicYearStartMonth__c
    }

    global class FlowInputs{

        @InvocableVariable (required=true)
        global CORE_Intake__c intake;

        @InvocableVariable (required=true)
        global String academicYearStartMonth;

    }

    global class FlowOutputs{

        @InvocableVariable
        global Boolean isStarted;

    }

}