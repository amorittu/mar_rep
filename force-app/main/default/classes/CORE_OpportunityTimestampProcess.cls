public class CORE_OpportunityTimestampProcess {
    @TestVisible
    private List<String> statusValues;

    @TestVisible
    private Map<Id,StatusPositions> statusPositionsByRecordType;

    @TestVisible
    class StatusPositions{
        @TestVisible
        Map<String,Integer> positionsByStatus;

        @TestVisible
        Map<Integer,String> statusByPositions;

        public StatusPositions(){
            this.positionsByStatus = new Map<String,Integer>();
            this.statusByPositions = new Map<Integer,String>();
        }
    }

    public CORE_OpportunityTimestampProcess() {
        this.statusPositionsByRecordType = new Map<Id,StatusPositions>();
        //CORE_Timestamp_process_setting__mdt timestampSettings = CORE_Timestamp_process_setting__mdt.getInstance('Default_value');
        CORE_Timestamp_process_setting__mdt timestampSettings = [SELECT Id, CORE_Opportunity_status_order__c FROM CORE_Timestamp_process_setting__mdt WHERE DeveloperName = 'Default_value' LIMIT 1];

        System.debug('@ORE_OpportunityTimestampProcess> timestampSettings.CORE_Opportunity_status_order__c = '+ timestampSettings.CORE_Opportunity_status_order__c);

        Map<String,Object> statusByRecordTypes = (Map<String,Object>) JSON.deserializeUntyped(timestampSettings.CORE_Opportunity_status_order__c);
        System.debug('@ORE_OpportunityTimestampProcess> statusByRecordTypes = '+ statusByRecordTypes);

        for(String recordTypeApiName : statusByRecordTypes.keySet()){
            Id recordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get(recordTypeApiName).getRecordTypeId();

            StatusPositions statusPositions = new StatusPositions(); 
            Integer i = 0;
            for(Object statusValue : (List<Object>) statusByRecordTypes.get(recordTypeApiName)){
                statusPositions.positionsByStatus.put((String) statusValue,i);
                statusPositions.statusByPositions.put(i,(String) statusValue);
                i++;
            }
            System.debug('positionsByStatus = ' + JSON.serialize(statusPositions.positionsByStatus));
            System.debug('statusByPositions = ' + JSON.serialize(statusPositions.statusByPositions));
            this.statusPositionsByRecordType.put(recordTypeId,statusPositions);
        }
    }

    public static void insertionProcess(List<Opportunity> opportunities){
        CORE_OpportunityTimestampProcess timestampProcess = new CORE_OpportunityTimestampProcess();
        List<Opportunity> oppsWithoutDeferal = CORE_OppotunityTriggerHandler.getOpportunitiesWhithoutDeferal(opportunities);

        if(!oppsWithoutDeferal.isEmpty()){
            timestampProcess.upsertStatusTimestamp(new List<Opportunity>(),oppsWithoutDeferal);
            timestampProcess.insertSubStatusTimestamp(oppsWithoutDeferal);
        }

        createTimestampFromCloseImport(opportunities);
    }

    public static void updateProcess(List<Opportunity> oldOpportunities, List<Opportunity> newOpportunities){
        Map<Id,Opportunity> oldMapOpportunities = new Map<Id,Opportunity>(oldOpportunities);
        List<Opportunity> opportunitiesWithStatusChanged = CORE_OppotunityTriggerHandler.getOpportunitiesWithChangedStatus(oldMapOpportunities, newOpportunities);
        List<Opportunity> opportunitiesWithSubStatusChanged = CORE_OppotunityTriggerHandler.getOpportunitiesWithChangedSubStatus(oldMapOpportunities, newOpportunities);

        CORE_OpportunityTimestampProcess timestampProcess = new CORE_OpportunityTimestampProcess();
        
        timestampProcess.upsertStatusTimestamp(oldOpportunities,opportunitiesWithStatusChanged);
        timestampProcess.upsertSubStatusTimeStamp(oldOpportunities,opportunitiesWithSubStatusChanged);

        //createTimestampFromCloseImport(newOpportunities);
    }

    private static void createTimestampFromCloseImport(List<Opportunity> opportunities){
        List<CORE_Status_Timestamp_History__c> timestampToCreate = new List<CORE_Status_Timestamp_History__c>();
        for(Opportunity opportunity : opportunities){
            if(opportunity.CORE_OPP_Legacy_CRM_Id__c != null && (opportunity.StageName == CORE_OppotunityTriggerHandler.CLOSED_WON || opportunity.StageName == CORE_OppotunityTriggerHandler.CLOSED_LOST)){
                string[] splitted = opportunity.CORE_OPP_Sub_Status__c.split(' -');
                string myStatus=splitted[0];
                for(string status : CORE_OppotunityTriggerHandler.ORDER_STATUS){
                    CORE_Status_Timestamp_History__c myTimeStamp = new CORE_Status_Timestamp_History__c();
                    myTimeStamp.CORE_Opportunity__c = opportunity.Id;
                    myTimeStamp.CORE_Status__c=status;
                    myTimeStamp.CORE_Modification_Status_Date_time__c=opportunity.CloseDate;
                    timestampToCreate.add(myTimeStamp);
                    if(status==myStatus){
                        break;
                    }
                }
            }
            system.debug('timestampToCreate list '+timestampToCreate);
        }

        if(!timestampToCreate.isEmpty()){
            insert timestampToCreate;
        }
    }

    /*
    private void insertStatusTimestamp(List<Opportunity> newOpportunities){
        List<CORE_Status_Timestamp_History__c> statusTimeStamps = new List<CORE_Status_Timestamp_History__c>();

        for(Opportunity opportunity : newOpportunities){
            CORE_Status_Timestamp_History__c statusTimeStamp = initStatusTimestampRecord(opportunity, opportunity.StageName);
            
            statusTimeStamps.add(statusTimeStamp);
        }
        
        insert statusTimeStamps;
    }
    */
    public void insertSubStatusTimestamp(List<Opportunity> newOpportunities){
        List<CORE_Sub_Status_Timestamp_History__c> subStatusTimeStamps = new List<CORE_Sub_Status_Timestamp_History__c>();

        for(Opportunity opportunity : newOpportunities){
            CORE_Sub_Status_Timestamp_History__c subStatusTimeStamp = initSubStatusTimestampRecord(opportunity);

            subStatusTimeStamps.add(subStatusTimeStamp);
        }

        insert subStatusTimeStamps;
    }

    @TestVisible
    private CORE_Sub_Status_Timestamp_History__c initSubStatusTimestampRecord(Opportunity opportunity){
        CORE_Sub_Status_Timestamp_History__c subStatusTimeStamp = new CORE_Sub_Status_Timestamp_History__c(
            CORE_Opportunity__c = opportunity.Id,
            CORE_Modification_Sub_status_Date_time__c = opportunity.LastModifiedDate,
            CORE_Status__c = opportunity.StageName,
            CORE_Sub_status__c = opportunity.CORE_OPP_Sub_Status__c
        );

        return subStatusTimeStamp;
    }

    @TestVisible
    private CORE_Status_Timestamp_History__c initStatusTimestampRecord(Opportunity opportunity, String stageName){
        CORE_Status_Timestamp_History__c statusTimeStamp = new CORE_Status_Timestamp_History__c(
            CORE_Opportunity__c = opportunity.Id,
            CORE_Modification_Status_Date_time__c = opportunity.LastModifiedDate,
            CORE_Status__c = stageName
        );

        return statusTimeStamp;
    }

    @TestVisible
    private List<CORE_Sub_Status_Timestamp_History__c> getExistingSubStatusTimestamps(Set<Id> opportunityIds){
        List<CORE_Sub_Status_Timestamp_History__c> existingTimeStamps = new List<CORE_Sub_Status_Timestamp_History__c>();
        
        if(!opportunityIds.isEmpty()){
            existingTimeStamps = [SELECT Id, CORE_Status__c, CORE_Sub_status__c, CORE_Opportunity__c
                FROM CORE_Sub_Status_Timestamp_History__c 
                WHERE CORE_Opportunity__c in :opportunityIds];
        }
        
        return existingTimeStamps;
    }
    
    public void upsertSubStatusTimeStamp(List<Opportunity> oldOpportunities, List<Opportunity> opportunitiesWithChanges){
        Map<Id,Opportunity> oldMapOpportunities = new Map<Id,Opportunity>(oldOpportunities);

        Set<Id> opportunitiesWithChangesIds = (new Map<Id,Opportunity>(opportunitiesWithChanges)).keySet();

        List<CORE_Sub_Status_Timestamp_History__c> existingTimeStamps = getExistingSubStatusTimestamps(opportunitiesWithChangesIds);

        Map<Id, List<CORE_Sub_Status_Timestamp_History__c>> existingTimeStampsByOpp = new Map<Id, List<CORE_Sub_Status_Timestamp_History__c>>();

        //init map
        for(Opportunity opportunity : opportunitiesWithChanges)  {
            existingTimeStampsByOpp.put(opportunity.Id,new List<CORE_Sub_Status_Timestamp_History__c>());
        }

        //fill map with existing values
        for(CORE_Sub_Status_Timestamp_History__c timestampHistory : existingTimeStamps){
            existingTimeStampsByOpp.get(timestampHistory.CORE_Opportunity__c).add(timestampHistory);
        }

        List<CORE_Sub_Status_Timestamp_History__c> timestampsToUpsert = new List<CORE_Sub_Status_Timestamp_History__c>();
        for(Opportunity newOpportunity : opportunitiesWithChanges){
            Opportunity oldOpportunity = oldMapOpportunities.get(newOpportunity.Id);
            List<CORE_Sub_Status_Timestamp_History__c> existingTimeStampForOpp = existingTimeStampsByOpp.get(newOpportunity.Id);

            CORE_Sub_Status_Timestamp_History__c timestampsToUpsertForCurrentOpp = getSubStatusTimeStampHistories(
                newOpportunity, 
                existingTimeStampForOpp
            );

            if(timestampsToUpsertForCurrentOpp != null){
                timestampsToUpsert.add(timestampsToUpsertForCurrentOpp);
            }
        }

        if(!timestampsToUpsert.isEmpty()){
            upsert timestampsToUpsert;
        }

    }

    @TestVisible
    private CORE_Sub_Status_Timestamp_History__c getSubStatusTimeStampHistories(Opportunity newOpportunity, List<CORE_Sub_Status_Timestamp_History__c> existingTimeStamps){
        Map<String, CORE_Sub_Status_Timestamp_History__c> existingSubStatusTmp = new Map<String, CORE_Sub_Status_Timestamp_History__c>();
        for (CORE_Sub_Status_Timestamp_History__c existingTimeStamp : existingTimeStamps){
            existingSubStatusTmp.put(existingTimeStamp.CORE_Sub_status__c, existingTimeStamp);
        }
        Set<String> existingSubStatus = existingSubStatusTmp.keySet();

        CORE_Sub_Status_Timestamp_History__c timestampToUpsert;
        /*if(existingSubStatus.contains(newOpportunity.CORE_OPP_Sub_Status__c)){
            timestampToUpsert = existingSubStatusTmp.get(newOpportunity.CORE_OPP_Sub_Status__c);
            timestampToUpsert.CORE_Modification_Sub_status_Date_time__c = newOpportunity.LastModifiedDate;
        } else {
            timestampToUpsert = initSubStatusTimestampRecord(newOpportunity);
        }*/

        timestampToUpsert = initSubStatusTimestampRecord(newOpportunity);
        return timestampToUpsert;
    }

    @TestVisible
    private List<CORE_Status_Timestamp_History__c> getExistingStatusTimestamps(Set<Id> opportunityIds){
        List<CORE_Status_Timestamp_History__c> existingTimeStamps = new List<CORE_Status_Timestamp_History__c>();
        
        if(!opportunityIds.isEmpty()){
            existingTimeStamps = [SELECT Id, CORE_Status__c, CORE_Opportunity__c
                FROM CORE_Status_Timestamp_History__c 
                WHERE CORE_Opportunity__c in :opportunityIds];
        }
        
        return existingTimeStamps;
    }

    public void upsertStatusTimestamp(List<Opportunity> oldOpportunities, List<Opportunity> opportunitiesWithChanges){
        Map<Id,Opportunity> oldMapOpportunities = new Map<Id,Opportunity>(oldOpportunities);

        Set<Id> opportunitiesWithChangesIds = (new Map<Id,Opportunity>(opportunitiesWithChanges)).keySet();

        List<CORE_Status_Timestamp_History__c> existingTimeStamps = getExistingStatusTimestamps(opportunitiesWithChangesIds);

        Map<Id, List<CORE_Status_Timestamp_History__c>> existingTimeStampsByOpp = new Map<Id, List<CORE_Status_Timestamp_History__c>>();

        //init map
        for(Opportunity opportunity : opportunitiesWithChanges)  {
            existingTimeStampsByOpp.put(opportunity.Id,new List<CORE_Status_Timestamp_History__c>());
        }

        for(CORE_Status_Timestamp_History__c timestampHistory : existingTimeStamps){
            existingTimeStampsByOpp.get(timestampHistory.CORE_Opportunity__c).add(timestampHistory);
        }

        List<CORE_Status_Timestamp_History__c> timestampsToUpsert = new List<CORE_Status_Timestamp_History__c>();
        for(Opportunity newOpportunity : opportunitiesWithChanges){
            Opportunity oldOpportunity = oldMapOpportunities.get(newOpportunity.Id);
            List<CORE_Status_Timestamp_History__c> existingTimeStampForOpp = existingTimeStampsByOpp.get(newOpportunity.Id);

            List<CORE_Status_Timestamp_History__c> timestampsToUpsertForCurrentOpp = getStatusTimeStampHistories(
                oldOpportunity, 
                newOpportunity, 
                existingTimeStampForOpp
            );

            if(timestampsToUpsertForCurrentOpp != null && timestampsToUpsertForCurrentOpp.size() > 0){
                timestampsToUpsert.addAll(timestampsToUpsertForCurrentOpp);
            }
        }
        System.debug('timestampsToUpsert = ' + timestampsToUpsert);
        if(!timestampsToUpsert.isEmpty()){
            upsert timestampsToUpsert;
        }
    }

    @TestVisible
    private List<CORE_Status_Timestamp_History__c> getStatusTimeStampHistories(Opportunity oldOpportunity, Opportunity newOpportunity, List<CORE_Status_Timestamp_History__c> existingTimeStamps){
        Id recordTypeId = newOpportunity.recordTypeId;
        StatusPositions statusPositions = statusPositionsByRecordType.get(recordTypeId);
        Map<String,Integer> positionsByStatus = statusPositions.positionsByStatus;
        Map<Integer,String> statusByPositions = statusPositions.statusByPositions;

        List<CORE_Status_Timestamp_History__c> timestampsToUpsert = new List<CORE_Status_Timestamp_History__c>();
        Integer oldStatusPosition = oldOpportunity == null ? -1 : positionsByStatus.get(oldOpportunity.StageName);
        Integer newStatusPosition = positionsByStatus.get(newOpportunity.StageName);

        if(oldStatusPosition==null){
            oldStatusPosition = -1;
        }
        System.debug('oldStatusPosition = '+ oldStatusPosition);
        System.debug('newStatusPosition = '+ newStatusPosition);
        Map<Integer, CORE_Status_Timestamp_History__c> existingStageNameTmp = new Map<Integer, CORE_Status_Timestamp_History__c>();
        for (CORE_Status_Timestamp_History__c existingTimeStamp : existingTimeStamps){
            existingStageNameTmp.put(positionsByStatus.get(existingTimeStamp.CORE_Status__c), existingTimeStamp);
        }

        Set<Integer> existingPositions = existingStageNameTmp.keySet().isEmpty() ? new Set<Integer>() : existingStageNameTmp.keySet();

        if(newOpportunity.IsClosed){
            CORE_Status_Timestamp_History__c timestampToUpsert;

            if(existingPositions.contains(newStatusPosition)){
                timestampToUpsert = existingStageNameTmp.get(newStatusPosition);
                timestampToUpsert.CORE_Status__c = statusByPositions.get(newStatusPosition);
                timestampToUpsert.CORE_Modification_Status_Date_time__c = newOpportunity.LastModifiedDate;
            } else {
                timestampToUpsert = initStatusTimestampRecord(newOpportunity, statusByPositions.get(newStatusPosition));
            }

            timestampsToUpsert.add(timestampToUpsert);

            return timestampsToUpsert;
        }

        System.debug('existingPositions = '+ existingPositions);

        if(oldStatusPosition < newStatusPosition){
            for(Integer i = oldStatusPosition + 1; i <= newStatusPosition ; i++){
                CORE_Status_Timestamp_History__c timestampToUpsert;

                if(existingPositions.contains(i)){
                    timestampToUpsert = existingStageNameTmp.get(i);
                    timestampToUpsert.CORE_Status__c = statusByPositions.get(i);
                    timestampToUpsert.CORE_Modification_Status_Date_time__c = newOpportunity.LastModifiedDate;
                } else {
                    timestampToUpsert = initStatusTimestampRecord(newOpportunity, statusByPositions.get(i));
                }

                timestampsToUpsert.add(timestampToUpsert);
            }

        } else if(oldStatusPosition > newStatusPosition){
            for(Integer i = oldStatusPosition; i > newStatusPosition ; i--){
                CORE_Status_Timestamp_History__c timestampToUpdate;
                System.debug('i = '+ i);
                if(existingPositions.contains(i)){
                    timestampToUpdate = existingStageNameTmp.get(i);
                    timestampToUpdate.CORE_Modification_Status_Date_time__c = null;
                } else {
                    timestampToUpdate = initStatusTimestampRecord(newOpportunity, statusByPositions.get(i));
                    timestampToUpdate.CORE_Modification_Status_Date_time__c = null;
                }
               
                timestampsToUpsert.add(timestampToUpdate);
            }
            /*CORE_Status_Timestamp_History__c timestampToUpdate;
            if(existingPositions.contains(newStatusPosition)){
                timestampToUpdate = existingStageNameTmp.get(newStatusPosition);
                timestampToUpdate.CORE_Modification_Status_Date_time__c = newOpportunity.LastModifiedDate;
            } else {
                timestampToUpdate = initStatusTimestampRecord(newOpportunity, this.statusByPositions.get(newStatusPosition));
            }
            timestampsToUpsert.add(timestampToUpdate);*/
        }

        return timestampsToUpsert;
    }
}