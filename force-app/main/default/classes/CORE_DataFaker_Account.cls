@IsTest
public class CORE_DataFaker_Account {
    public static Account getStudentAccount(String uniqueKey){
        Account account = getStudentAccountWithoutInsert(uniqueKey);

        Insert account;
        return account;
    }

    public static Account getStudentAccountWithoutInsert(String uniqueKey){
        Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Core_Student').getRecordTypeId();

        Account account = new Account(
            FirstName = 'FirstName '+ uniqueKey,
            LastName = 'LastName '+ uniqueKey,
            PersonEmail = 'account.test.' + uniqueKey + '@test.com',
            RecordTypeId = recordTypeId
        );

        return account;
    }

    public static Account getStudentAccount(String uniqueKey,DateTime createdDate, DateTime modifiedDate){
        Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('Core_Student').getRecordTypeId();

        Account account = new Account(
            FirstName = 'FirstName '+ uniqueKey,
            LastName = 'LastName '+ uniqueKey,
            PersonEmail = 'account.test.' + uniqueKey + '@test.com',
            RecordTypeId = recordTypeId,
            CreatedDate = createdDate,
            LastModifiedDate = modifiedDate
        );

        Insert account;
        return account;
    }
}