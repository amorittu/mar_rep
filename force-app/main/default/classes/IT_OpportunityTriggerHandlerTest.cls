/**
 * Created by Gabriele Vitanza on 11/11/2021.
 */

@IsTest
private class IT_OpportunityTriggerHandlerTest {
	@TestSetup
	static void testSetup() {
		CORE_DataFaker_CatalogHierarchy catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test1');
		Account account = CORE_DataFaker_Account.getStudentAccount('test1');
		Opportunity opportunity = CORE_DataFaker_Opportunity.getOpportunity(
				catalogHierarchy.division.Id,
				catalogHierarchy.businessUnit.Id,
				account.Id,
				'Applicant',
				'Applicant - Application Submitted / New'
		);

		opportunity.CORE_OPP_Intake__c = catalogHierarchy.intake.Id;
		opportunity.CORE_OPP_Curriculum__c = catalogHierarchy.curriculum.Id;
		opportunity.CORE_OPP_Study_Area__c = catalogHierarchy.studyArea.Id;
		opportunity.CORE_OPP_Level__c = catalogHierarchy.level.Id;
		opportunity.IT_AdmissionOfficer__c = UserInfo.getUserId();
		update opportunity;

		IT_DataFaker_Objects.createExtendedInfo(account.Id, opportunity.CORE_OPP_Business_Unit__r.CORE_Brand__c, true);

		Id rtId = Schema.SObjectType.Task.getRecordTypeInfosByDeveloperName().get('CORE_Continuum_of_action').getRecordTypeId();

		Task t = new Task();
		t.WhatId = opportunity.Id;
		t.RecordTypeId = rtId;
		t.Status = 'CORE_PKL_Open';
		insert t;
	}

	@IsTest
	static void testBehavior() {
		// PE - START (To cover Flow IT_TriggeredThankYouEmail)
		Test.setMock(HttpCalloutMock.class, new IT_MockMarketingCloudIntegration('token'));
		IT_EmailPlatformEvent__e PE = new IT_EmailPlatformEvent__e();

		PE.IT_BusinessUnitExternalId__c = 'NB.MI0';
		PE.IT_AccountLanguagePC__c = 'English';
		PE.IT_AccountLanguagePC__c = 'English';
		PE.IT_AccountLanguageWebsite__c = 'English';
		PE.IT_AccountLanguageOfRelationship__c = 'English';
		PE.IT_CountryRelevance__c = 'A';
		PE.IT_StudentFirstName__c = 'TestN1';
		PE.IT_StudentLastName__c = 'TestS1';
		PE.IT_StudentEmail__c = 'TestN1TestS1@null.aa';
		PE.IT_TaskId__c = '123456789012345678';
		PE.IT_SubscriberKey__c = '923456789012345678';
		PE.IT_EventDefinitionKey__c = 'APIEvent-f82bb383-b351-d6c3-b969-a3dda66cbcd2';
		PE.IT_COAProcess__c = 'IT_PKL_LeadNew';
		PE.IT_DivisionExternalId__c = 'NB';
		PE.IT_EmailTypology__c = 'TY';

		List<IT_EmailPlatformEvent__e> PEList = new List<IT_EmailPlatformEvent__e>();
		PEList.add(PE);
		Database.SaveResult[] sr = EventBus.publish(PEList);
		// PE - END
		Blob b = Crypto.generateAesKey(128);
		String h = EncodingUtil.convertToHex(b);
		String uid = h.substring(0, 8);

		String email = uid + '-' + 'test' + '@test.com';
		Profile standardUserProfile = [SELECT Id FROM Profile WHERE Name = 'System Administrator'];
		UserRole roleInfo = [SELECT Id FROM UserRole WHERE DeveloperName = 'IT_NABA_Roma_Telesales'];
		UserRole roleAdm = [SELECT Id FROM UserRole WHERE DeveloperName = 'IT_NABA_Roma_Sales_Admission_Manager'];
		Account acc = [SELECT Id FROM Account LIMIT 1];
		User u1 = new User(
				Alias = uid,
				Email = email,
				EmailEncodingKey = 'UTF-8',
				LastName = 'Testing-' + 'test',
				LanguageLocaleKey = 'en_US',
				LocaleSidKey = 'en_US',
				ProfileId = standardUserProfile.Id,
				UserRoleId = roleInfo.Id,
				TimeZoneSidKey = 'Europe/Paris',
				Username = email
		);

		User u2 = new User(
				Alias = 'test2',
				Email = 'test2@test.com',
				EmailEncodingKey = 'UTF-8',
				LastName = 'Testing-' + 'test',
				LanguageLocaleKey = 'en_US',
				LocaleSidKey = 'en_US',
				ProfileId = standardUserProfile.Id,
				UserRoleId = roleAdm.Id,
				TimeZoneSidKey = 'Europe/Paris',
				Username = 'test2@test.com.tets'
		);

		List<User> usersToInsert = new List<User>{
				u1, u2
		};
		System.runAs(new User(Id = UserInfo.getUserId())) {
			insert usersToInsert;
		}

		Test.startTest();
		Opportunity opty = [SELECT Id, CORE_Capture_Channel__c, CORE_Brand__c FROM Opportunity LIMIT 1];
		opty.CORE_Capture_Channel__c = 'CORE_PKL_Web_form';
		opty.IT_FirstAPOCEntered__c = true;
		opty.IT_NewOwner__c = u1.Id;
		opty.IT_SkipOpportunityTrigger__c = true;
		update opty;

		IT_DataFaker_Objects.createExtendedInfo(acc.Id, opty.CORE_Brand__c, true);

		opty.IT_NewOwner__c = u2.Id;
		opty.IT_SkipOpportunityTrigger__c = false;
        opty.IT_OpportunityAssigned__c = true;
		System.runAs(u2) {
			update opty;
		}
		Test.stopTest();
	}

	@IsTest
	static void testSubStatusHistoryLogging() {
		Opportunity opty = [SELECT Id, StageName, CORE_OPP_Sub_Status__c FROM Opportunity LIMIT 1];
		opty.CORE_OPP_Sub_Status__c = 'Applicant - No Show';
		update opty;


		opty.StageName = 'Admitted';
		opty.CORE_OPP_Sub_Status__c = 'Admitted - Admit';
		update opty;
	}

	@isTest
	public static void testQueueableMethod() {
        Test.setMock(HttpCalloutMock.class, new IT_MockMarketingCloudIntegration('token'));
        IT_EmailPlatformEvent__e PE = new IT_EmailPlatformEvent__e();

		PE.IT_BusinessUnitExternalId__c = 'NB.MI0';
		PE.IT_AccountLanguagePC__c = 'English';
		PE.IT_AccountLanguagePC__c = 'English';
		PE.IT_AccountLanguageWebsite__c = 'English';
		PE.IT_AccountLanguageOfRelationship__c = 'English';
		PE.IT_CountryRelevance__c = 'A';
		PE.IT_StudentFirstName__c = 'TestN1';
		PE.IT_StudentLastName__c = 'TestS1';
		PE.IT_StudentEmail__c = 'TestN1TestS1@null.aa';
		PE.IT_TaskId__c = '123456789012345678';
		PE.IT_SubscriberKey__c = '923456789012345678';
		PE.IT_EventDefinitionKey__c = 'APIEvent-f82bb383-b351-d6c3-b969-a3dda66cbcd2';
		PE.IT_COAProcess__c = 'IT_PKL_LeadNew';
		PE.IT_DivisionExternalId__c = 'NB';
		PE.IT_EmailTypology__c = 'TY';

		List<IT_EmailPlatformEvent__e> PEList = new List<IT_EmailPlatformEvent__e>();
		PEList.add(PE);
		Database.SaveResult[] sr = EventBus.publish(PEList);
        
		CORE_OpportunityDataMaker dataMaker = new CORE_OpportunityDataMaker();
		Opportunity opportunity = [SELECT Id, AccountId, CORE_OPP_Business_Unit__c, Account.PersonMailingCountry, CORE_Main_Product_Interest__c, CORE_Capture_Channel__c, Account.CORE_Language_website__pc FROM Opportunity];
		Test.startTest();
		String captureChannel = 'CORE_PKL_Email';
		String nature = 'CORE_PKL_Online';
		String origin = 'origin';
		CORE_Point_of_Contact__c pc = dataMaker.initPocDefaultValues(opportunity, nature, captureChannel, origin);
		insert pc;
		Test.stopTest();
	}

	/*@isTest
	public static void firePlatformEventTest() {
		Test.setMock(HttpCalloutMock.class, new IT_MockMarketingCloudIntegration('token'));
		IT_EmailPlatformEvent__e PE = new IT_EmailPlatformEvent__e();

		PE.IT_BusinessUnitExternalId__c = 'NB.MI0';
		PE.IT_AccountLanguagePC__c = 'English';
		PE.IT_AccountLanguagePC__c = 'English';
		PE.IT_AccountLanguageWebsite__c = 'English';
		PE.IT_AccountLanguageOfRelationship__c = 'English';
		PE.IT_CountryRelevance__c = 'A';
		PE.IT_StudentFirstName__c = 'TestN1';
		PE.IT_StudentLastName__c = 'TestS1';
		PE.IT_StudentEmail__c = 'TestN1TestS1@null.aa';
		PE.IT_TaskId__c = '123456789012345678';
		PE.IT_SubscriberKey__c = '923456789012345678';
		PE.IT_EventDefinitionKey__c = 'APIEvent-f82bb383-b351-d6c3-b969-a3dda66cbcd2';
		PE.IT_COAProcess__c = 'IT_PKL_LeadNew';
		PE.IT_DivisionExternalId__c = 'NB';
		PE.IT_EmailTypology__c = 'TY';

		List<IT_EmailPlatformEvent__e> PEList = new List<IT_EmailPlatformEvent__e>();
		PEList.add(PE);
		Database.SaveResult[] sr = EventBus.publish(PEList);
		CORE_OpportunityDataMaker dataMaker = new CORE_OpportunityDataMaker();
		Opportunity opportunity = [SELECT Id, AccountId, CORE_OPP_Business_Unit__c, Account.PersonMailingCountry, CORE_Main_Product_Interest__c, CORE_Capture_Channel__c, Account.CORE_Language_website__pc FROM Opportunity];
		Test.startTest();
		opportunity.IT_OpportunityAssigned__c = true;
//            firePlatformEvent
		update opportunity;
		Test.stopTest();

	}*/

	@isTest
	public static void fireTaskReassigmentMethodTest() {
		Test.setMock(HttpCalloutMock.class, new IT_MockMarketingCloudIntegration('token'));
		IT_EmailPlatformEvent__e PE = new IT_EmailPlatformEvent__e();
		User notCurrentUser = [SELECT Id FROM User WHERE Id != :UserInfo.getUserId() LIMIT 1];

		PE.IT_BusinessUnitExternalId__c = 'NB.MI0';
		PE.IT_AccountLanguagePC__c = 'English';
		PE.IT_AccountLanguagePC__c = 'English';
		PE.IT_AccountLanguageWebsite__c = 'English';
		PE.IT_AccountLanguageOfRelationship__c = 'English';
		PE.IT_CountryRelevance__c = 'A';
		PE.IT_StudentFirstName__c = 'TestN1';
		PE.IT_StudentLastName__c = 'TestS1';
		PE.IT_StudentEmail__c = 'TestN1TestS1@null.aa';
		PE.IT_TaskId__c = '123456789012345678';
		PE.IT_SubscriberKey__c = '923456789012345678';
		PE.IT_EventDefinitionKey__c = 'APIEvent-f82bb383-b351-d6c3-b969-a3dda66cbcd2';
		PE.IT_COAProcess__c = 'IT_PKL_LeadNew';
		PE.IT_DivisionExternalId__c = 'NB';
		PE.IT_EmailTypology__c = 'TY';

		List<IT_EmailPlatformEvent__e> PEList = new List<IT_EmailPlatformEvent__e>();
		PEList.add(PE);
		Database.SaveResult[] sr = EventBus.publish(PEList);
		CORE_OpportunityDataMaker dataMaker = new CORE_OpportunityDataMaker();
		List<Opportunity> opportunities = [SELECT Id, AccountId, CORE_OPP_Business_Unit__c, Account.PersonMailingCountry, CORE_Main_Product_Interest__c, CORE_Capture_Channel__c, Account.CORE_Language_website__pc FROM Opportunity LIMIT 2];
		List<Opportunity> opportunities2 = new List<Opportunity>();
		Map <Id, Opportunity> oldOpportunities = new Map<Id, Opportunity>();
		for (Opportunity opp : opportunities) {
			oldOpportunities.put(opp.Id, opp);
			opp.OwnerId = notCurrentUser.Id;
			opp.IT_OpportunityAssigned__c = true;
			opportunities2.add(opp);
		}

		Test.startTest();
		update opportunities2;
		Test.stopTest();

	}
    @isTest
	public static void populatePastOpportunity_Test() {
		CORE_OpportunityDataMaker dataMaker = new CORE_OpportunityDataMaker();
		Opportunity opportunity = [SELECT Id, AccountId, CORE_OPP_Business_Unit__c, Account.PersonMailingCountry, CORE_Main_Product_Interest__c, CORE_Capture_Channel__c, Account.CORE_Language_website__pc FROM Opportunity];

		Test.startTest();
		opportunity.CORE_OPP_Academic_Year__c = 'CORE_PKL_2021-2022';
        update opportunity;
        
		opportunity.CORE_OPP_Academic_Year__c = 'CORE_PKL_2022-2023';
        update opportunity;
		Test.stopTest();

	}
    @isTest
    public static void setAssignationInCaseOfManual_Test() {
        CORE_OpportunityDataMaker dataMaker = new CORE_OpportunityDataMaker();
		Opportunity opportunity = [SELECT Id, AccountId, CORE_OPP_Business_Unit__c, Account.PersonMailingCountry, CORE_Main_Product_Interest__c, CORE_Capture_Channel__c, Account.CORE_Language_website__pc FROM Opportunity];

		Test.startTest();
		opportunity.CORE_Insertion_Mode__c = 'CORE_PKL_Manual';
		update opportunity;
		Test.stopTest();
    }
	@isTest
	public static void checkQueueMembers_Test() {
		
		Group g1 = new Group(Name='Test Group', type='Queue');
		insert g1;
        System.runAs(new User(Id=UserInfo.getUserId())) {
            CORE_DataFaker_CatalogHierarchy catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test21');
			Account account = CORE_DataFaker_Account.getStudentAccount('test21');
        
            
            //Associating queue with group AND to the Case object
            QueuesObject testQueue = new QueueSObject(QueueID = g1.id, SobjectType = 'IT_Office__c');
            insert testQueue;
            GroupMember gm = new GroupMember();
            gm.GroupId = testQueue.QueueId;
            gm.UserOrGroupId = UserInfo.getUserId();
            insert gm;
            
            QueueSobject q1 = [Select Id,Queue.Name, Queue.ID from QueueSobject WHERE QueueId=: g1.Id ORDER BY Queue.Name] ;

		
		IT_Office__c office = new IT_Office__c();
		office.IT_Type__c = 'Orientation';
		office.IT_Email__c = 'invalidDrs@null.aa';
		office.OwnerId = q1.queue.Id;
        office.IT_UndergraduateBusinessHours__c = [SELECT Id FROM BusinessHours LIMIT 1].Id;
		insert office;
        System.debug('office.OwnerId is ' + office.OwnerId );
        
        Opportunity opportunity = new Opportunity(
            Name ='test',
            StageName = 'Lead',
            CORE_OPP_Sub_Status__c = 'Lead - New',
            CORE_OPP_Division__c = catalogHierarchy.division.Id,
            CORE_OPP_Business_Unit__c = catalogHierarchy.businessUnit.Id,
            CORE_OPP_Academic_Year__c = 'CORE_PKL_2023-2024',
            AccountId = account.Id,
            CloseDate = Date.Today().addDays(2),
            OwnerId = UserInfo.getUserId(),
            CORE_OPP_Main_Opportunity__c = true,
            IT_InformationQueue__c = office.Id
        );
            insert opportunity;
        
        Test.startTest();
            opportunity.IT_OfficeInCharge__c = 'Information';
            update opportunity;
        Test.stopTest();
        }
	}
	
}