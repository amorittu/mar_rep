public class CORE_OpportunityMetadata {
    @TestVisible
    private CORE_Opportunity_creation_setting__mdt opportunityMetadata;

    @TestVisible
    private CORE_Consent_Setting__mdt consentMetadata;
    
    public String statusSuspect{get;set;}
    public String subStatusSuspect{get;set;}
    public String statusFull{get;set;}
    public String subStatusFull{get;set;}
    public String statusPartial{get;set;}
    public String subStatusPartial{get;set;}
    public Id oppRecordTypeId{get;set;}
    public Date closeDate{get;set;}
    public String pocInsertionMode{get;set;}
    public String consentScope{get;set;}
    public Boolean aolUpdateExisting{get;set;}
    public String defaultAcademicYear{get;set;}

    public CORE_OpportunityMetadata() {
        getMetadata();
        initializeAttributes();
    }

    @TestVisible
    private void getMetadata(){
        if(Test.isRunningTest()){
            this.consentMetadata = new CORE_Consent_Setting__mdt(
                CORE_Scope__c = 'CORE_PKL_BusinessUnit'
           );

           this.opportunityMetadata = new CORE_Opportunity_creation_setting__mdt(
                CORE_OPP_Full_mode_Status__c = 'Lead',
                CORE_OPP_Full_mode_SubStatus__c = 'Lead - New',
                CORE_OPP_Partial_mode_Status__c = 'Prospect',
                CORE_OPP_Partial_mode_SubStatus__c = 'Prospect - New',
                CORE_OPP_RecordType__c = 'CORE_Enrollment',
                CORE_OPP_Close_date_nb_year__c = 5.0,
                CORE_OPP_Close_date_nb_days__c = 0.0,
                CORE_POC_Insertion_Mode__c = 'CORE_PKL_Manual',
                CORE_OPP_Suspect_mode_Status__c = 'Suspect',
                CORE_OPP_Suspect_mode_SubStatus__c = 'Suspect - New',
                CORE_OPP_AOL_Update_Existing__c = true,
                CORE_Default_AY_for_Manual_OPP_Creation__c = 'CORE_PKL_2023-2024'
           );
        }
        
        if(this.opportunityMetadata == null){
            this.opportunityMetadata = [SELECT CORE_OPP_RecordType__c, CORE_OPP_Close_date_nb_year__c,
            CORE_OPP_Close_date_nb_days__c, 
            CORE_OPP_Full_mode_Status__c, CORE_OPP_Full_mode_SubStatus__c,
            CORE_OPP_Partial_mode_Status__c, CORE_OPP_Partial_mode_SubStatus__c,
            CORE_POC_Insertion_Mode__c, CORE_OPP_Suspect_mode_Status__c, CORE_OPP_Suspect_mode_SubStatus__c,
            CORE_OPP_AOL_Update_Existing__c, CORE_Default_AY_for_Manual_OPP_Creation__c
            FROM CORE_Opportunity_creation_setting__mdt LIMIT 1];
        }

        if(this.consentMetadata == null){
            this.consentMetadata = [SELECT CORE_Scope__c
            FROM CORE_Consent_Setting__mdt LIMIT 1];
        }
    }

    @TestVisible
    private void initializeAttributes(){
        this.statusSuspect = this.opportunityMetadata.CORE_OPP_Suspect_mode_Status__c;
        this.subStatusSuspect = this.opportunityMetadata.CORE_OPP_Suspect_mode_SubStatus__c;
        this.statusFull = this.opportunityMetadata.CORE_OPP_Full_mode_Status__c;
        this.subStatusFull = this.opportunityMetadata.CORE_OPP_Full_mode_SubStatus__c;
        this.statusPartial = this.opportunityMetadata.CORE_OPP_Partial_mode_Status__c;
        this.subStatusPartial = this.opportunityMetadata.CORE_OPP_Partial_mode_SubStatus__c;
        this.oppRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get(this.opportunityMetadata.CORE_OPP_RecordType__c).getRecordTypeId();
        //functionnal rule : https://ggeedu.atlassian.net/wiki/spaces/GSSGD/pages/2087813274/Automatic+creation+process
        //General rule : Close date
        Date todayDate = Date.today();
        this.closeDate = todayDate.addYears((Integer) this.opportunityMetadata.CORE_OPP_Close_date_nb_year__c);
        this.closeDate = this.closeDate.addDays((Integer) this.opportunityMetadata.CORE_OPP_Close_date_nb_days__c);
        this.pocInsertionMode = this.opportunityMetadata.CORE_POC_Insertion_Mode__c;
        this.consentScope = this.consentMetadata.CORE_Scope__c;
        this.aolUpdateExisting = this.opportunityMetadata.CORE_OPP_AOL_Update_Existing__c;
        this.defaultAcademicYear = this.opportunityMetadata.CORE_Default_AY_for_Manual_OPP_Creation__c;
    }
}