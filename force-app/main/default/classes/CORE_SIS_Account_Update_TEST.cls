@isTest
private class CORE_SIS_Account_Update_TEST {

    private static final String INTEREST_EXTERNALID = 'setup';

    @testSetup 
    static void createData(){
        Id profileId = [SELECT Id FROM Profile WHERE Name = 'CORE_API_Profile' LIMIT 1].Id;

        User usr = new User(
            Username = 'usertest@aoltest.com',
            LastName = 'TEST_USER',
            Email = 'testemail@yopmail.com',
            Alias = 'T', 
            CommunityNickname = 'T',
            TimeZoneSidKey = 'Europe/Paris',
            LocaleSidKey = 'fr_FR_EURO',
            EmailEncodingKey = 'ISO-8859-1', 
            ProfileId = profileId,
            LanguageLocaleKey = 'fr'      
        );
        database.insert(usr, true);

        Account account = CORE_DataFaker_Account.getStudentAccount(INTEREST_EXTERNALID);

    }

    @isTest
    static void test_UpdateAcc(){
        User usr = [SELECT Id FROM User WHERE Username = 'usertest@aoltest.com' LIMIT 1];

        List<PermissionSetAssignment> psaList = new List<PermissionSetAssignment>();

        for(PermissionSet ps : [SELECT Id FROM PermissionSet WHERE Name = 'CORE_API_SIS' OR Name = 'CORE_LeadFormAcquisition']){
            psaList.add(
                new PermissionSetAssignment(AssigneeId = usr.id, PermissionSetId = ps.Id)
            );
        }
        
        database.insert(psaList, true);

        System.runAs(usr){

            CORE_SIS_Account_Update.overrideFieldsSet = new Set<String> {
                'CORE_Last_inbound_contact_date__c', 'CORE_Number_of_inbound_contact__c'
            };

            Account acc = [SELECT Id FROM Account LIMIT 1];

            String reqBody = '{'
            + '"Lastname": "TEST_LASTNAME",'
            + '"PersonBirthdate": "2022-03-03",'
            + '"PersonEmail": "user@example.com",'
            + '"CORE_Last_inbound_contact_date__c": "2022-03-03T09:49:59.217Z",'
            + '"CORE_Number_of_inbound_contact__c": 10'
            + '}';

            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
            req.requestURI = '/services/apexrest/sis/account/' + acc.Id;

            req.httpMethod = 'PUT';
            req.requestBody = Blob.valueOf(reqBody);

            RestContext.request = req;
            RestContext.response= res;

            test.startTest();
            CORE_SIS_Wrapper.ResultWrapperAccount results = CORE_SIS_Account_Update.updateAccount();
            test.stopTest();

            System.assertEquals(date.newInstance(2022, 3, 3), results.account.PersonBirthdate);
        }
    }

    @isTest
    static void test_UpdateAccInvalidField(){
        User usr = [SELECT Id FROM User WHERE Username = 'usertest@aoltest.com' LIMIT 1];

        List<PermissionSetAssignment> psaList = new List<PermissionSetAssignment>();

        for(PermissionSet ps : [SELECT Id FROM PermissionSet WHERE Name = 'CORE_API_SIS' OR Name = 'CORE_LeadFormAcquisition']){
            psaList.add(
                new PermissionSetAssignment(AssigneeId = usr.id, PermissionSetId = ps.Id)
            );
        }
        
        database.insert(psaList, true);

        System.runAs(usr){

            Account acc = [SELECT Id FROM Account LIMIT 1];

            String reqBody = '{'
            + '"Lastname": "TEST_LASTNAME",'
            + '"PersonBirthdate": "2022-03-03",'
            + '"CORE_GA_User_ID__c": "12345"'
            + '}';

            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
            req.requestURI = '/services/apexrest/sis/account/' + acc.Id;

            req.httpMethod = 'PUT';
            req.requestBody = Blob.valueOf(reqBody);

            RestContext.request = req;
            RestContext.response= res;

            test.startTest();
            CORE_SIS_Wrapper.ResultWrapperAccount results = CORE_SIS_Account_Update.updateAccount();
            test.stopTest();

            System.assertEquals(CORE_SIS_Constants.ERROR_STATUS, results.status);
        }
    }
}