@IsTest
public with sharing class CORE_ServiceAppointmentTriggerTest {
    @TestSetup
    static void makeData(){
        CORE_FLEX_Settings__c cs = new CORE_FLEX_Settings__c();
        Blob cryptoKey = Crypto.generateAesKey(256);
        Blob data = Blob.valueOf('password');
        Blob encryptedData = Crypto.encryptWithManagedIV('AES256', cryptoKey, data);

        cs.UrlFlex__c='https://project.twil.io';
        cs.TokenFlex__c=EncodingUtil.base64Encode(encryptedData);
        cs.KeySecret__c= EncodingUtil.base64Encode(cryptoKey);
        cs.AccountSid__c='ACxx';

        database.insert(cs, true);

        CORE_DataFaker_CatalogHierarchy catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test1');
        Account account = CORE_DataFaker_Account.getStudentAccount('test1');
        Opportunity opportunity = CORE_DataFaker_Opportunity.getOpportunity(catalogHierarchy.division.Id, 
            catalogHierarchy.businessUnit.Id, 
            catalogHierarchy.studyArea.Id,
            catalogHierarchy.curriculum.Id,
            catalogHierarchy.level.Id,
            catalogHierarchy.intake.Id, 
            account.Id, 
            'Lead', 
            'Lead - New'
        ); 
    }

    @IsTest
    public static void insert_with_account_should_prefil_account_fields() {
        Account account = [SELECT Id, FirstName, LastName, Salutation FROM Account];
        Datetime dtNow = DateTime.Now();

        Test.startTest();
        ServiceAppointment sa = CORE_DataFaker_ServiceAppointment.getServiceAppointment(account.Id, dtNow);
        Test.stopTest();

        ServiceAppointment result = [SELECT ID, CORE_Firstname__c, CORE_Lastname__c, CORE_Salutation__c FROM ServiceAppointment where Id = :sa.Id];

        System.assertEquals(account.FirstName, result.CORE_Firstname__c);
        System.assertEquals(account.LastName, result.CORE_Lastname__c);
        System.assertEquals(account.Salutation, result.CORE_Salutation__c);
    }

    @IsTest
    public static void insert_with_Opportunity_should_prefil_catalog_fields() {
        Opportunity opportunity = [SELECT Id, CORE_OPP_Division__c, CORE_OPP_Business_Unit__c, CORE_OPP_Study_Area__c, CORE_OPP_Curriculum__c, CORE_OPP_Level__c, CORE_OPP_Intake__c FROM Opportunity];
        Datetime dtNow = DateTime.Now();

        Test.startTest();
        ServiceAppointment sa = CORE_DataFaker_ServiceAppointment.getServiceAppointment(opportunity.Id, dtNow);
        Test.stopTest();

        ServiceAppointment result = [SELECT ID, CORE_Division__c, CORE_Business_Unit__c, CORE_Study_Area__c, CORE_Curriculum__c, CORE_Level__c, CORE_Intake__c FROM ServiceAppointment where Id = :sa.Id];

        System.assertEquals(opportunity.CORE_OPP_Division__c, result.CORE_Division__c);
        System.assertEquals(opportunity.CORE_OPP_Business_Unit__c, result.CORE_Business_Unit__c);
        System.assertEquals(opportunity.CORE_OPP_Study_Area__c, result.CORE_Study_Area__c);
        System.assertEquals(opportunity.CORE_OPP_Curriculum__c, result.CORE_Curriculum__c);
        System.assertEquals(opportunity.CORE_OPP_Level__c, result.CORE_Level__c );
        System.assertEquals(opportunity.CORE_OPP_Intake__c, result.CORE_Intake__c);
    }

    @IsTest
    public static void delete_when_parent_is_lead_new_should_delete_the_lead() {
        Lead lead = CORE_DataFaker_Lead.getLead('test');
        Datetime dtNow = DateTime.Now();

        ServiceAppointment sa = CORE_DataFaker_ServiceAppointment.getServiceAppointment(lead.Id, dtNow);
        Test.startTest();
        delete sa;
        Test.stopTest();

        List<ServiceAppointment> sas = [SELECT Id FROM ServiceAppointment];
        List<Lead> leads = [SELECT Id FROM Lead];
        System.assertEquals(true, sas.isEmpty());
        System.assertEquals(true, leads.isEmpty());
    }

    @IsTest
    public static void delete_when_parent_is_lead_with_status_after_new_should_not_delete_the_lead() {
        Lead lead = CORE_DataFaker_Lead.getLeadWithoutInsert('test');
        lead.status = 'Qualified';
        Insert lead;
        Datetime dtNow = DateTime.Now();

        ServiceAppointment sa = CORE_DataFaker_ServiceAppointment.getServiceAppointment(lead.Id, dtNow);
        Test.startTest();
        delete sa;
        Test.stopTest();

        List<ServiceAppointment> sas = [SELECT Id FROM ServiceAppointment];
        List<Lead> leads = [SELECT Id FROM Lead];
        System.assertEquals(true, sas.isEmpty());
        System.assertEquals(false, leads.isEmpty());
    }

    @IsTest
    public static void test_video_appointment_activated() {
        CORE_CountrySpecificSettings__c setting = new CORE_CountrySpecificSettings__c(
           CORE_TwilioCreateVideoMeeting__c = true,
           CORE_TwilioErrorEmailRecipients__c = 'test@test.com'
        );
        database.insert(setting, true);

        Opportunity opportunity = [SELECT Id FROM Opportunity];

        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('CORE_Twilio_Video_Appointment_Booking_SUCCESS_RESP');
        mock.setStatusCode(200);

        // Set the mock callout mode
        Test.setMock(HttpCalloutMock.class, mock);

        Datetime dtNow = DateTime.Now();

        ServiceResource serviceRes = CORE_DataFaker_ServiceAppointment.getServiceResource(Userinfo.getUserId(), 'test_user');

        ServiceAppointment sa = CORE_DataFaker_ServiceAppointment.getServiceAppointmentWithoutInsert(opportunity.Id, dtNow);
        sa.AppointmentType = 'Video';

        Test.startTest();
        database.insert(sa, true);

        List<AssignedResource> ar = [Select Id FROM AssignedResource];
        if(ar.isEmpty()){
            AssignedResource assignedRes = CORE_DataFaker_ServiceAppointment.getAssignedResourceWithoutInsert(sa.Id, serviceRes.Id);
            database.insert(assignedRes, true);
        }
        Test.stopTest();

        List<ServiceAppointment> sas = [SELECT Id, CORE_VideoMeetingId__c FROM ServiceAppointment WHERE Id = :sa.Id];
        System.assertEquals('1kIWSq', sas[0].CORE_VideoMeetingId__c);

    }

    @IsTest
    public static void test_video_appointment_deactivated() {
        CORE_CountrySpecificSettings__c setting = new CORE_CountrySpecificSettings__c(
                CORE_TwilioCreateVideoMeeting__c = false,
                CORE_TwilioErrorEmailRecipients__c = 'test@test.com'
        );
        database.insert(setting, true);

        Opportunity opportunity = [SELECT Id FROM Opportunity];

        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('CORE_Twilio_Video_Appointment_Booking_SUCCESS_RESP');
        mock.setStatusCode(200);

        // Set the mock callout mode
        Test.setMock(HttpCalloutMock.class, mock);

        Datetime dtNow = DateTime.Now();

        ServiceResource serviceRes = CORE_DataFaker_ServiceAppointment.getServiceResource(Userinfo.getUserId(), 'test_user');

        ServiceAppointment sa = CORE_DataFaker_ServiceAppointment.getServiceAppointmentWithoutInsert(opportunity.Id, dtNow);
        sa.AppointmentType = 'Video';

        Test.startTest();
        database.insert(sa, true);

        List<AssignedResource> ar = [Select Id FROM AssignedResource];
        if(ar.isEmpty()){
        	AssignedResource assignedRes = CORE_DataFaker_ServiceAppointment.getAssignedResourceWithoutInsert(sa.Id, serviceRes.Id);
            database.insert(assignedRes, true);
        }
        Test.stopTest();

        List<ServiceAppointment> sas = [SELECT Id, CORE_VideoMeetingId__c FROM ServiceAppointment WHERE Id = :sa.Id];
        System.assertEquals(null, sas[0].CORE_VideoMeetingId__c);

    }

    @IsTest
    public static void test_video_appointment_api_error() {
        CORE_CountrySpecificSettings__c setting = new CORE_CountrySpecificSettings__c(
                CORE_TwilioCreateVideoMeeting__c = true,
                CORE_TwilioErrorEmailRecipients__c = 'test@test.com'
        );
        database.insert(setting, true);

        Opportunity opportunity = [SELECT Id FROM Opportunity];

        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('CORE_Twilio_Video_Appointment_Booking_ERROR_RESP');
        mock.setStatusCode(400);

        // Set the mock callout mode
        Test.setMock(HttpCalloutMock.class, mock);

        Datetime dtNow = DateTime.Now();

        ServiceResource serviceRes = CORE_DataFaker_ServiceAppointment.getServiceResource(Userinfo.getUserId(), 'test_user');

        ServiceAppointment sa = CORE_DataFaker_ServiceAppointment.getServiceAppointmentWithoutInsert(opportunity.Id, dtNow);
        sa.AppointmentType = 'Video';

        Test.startTest();
        database.insert(sa, true);

        List<AssignedResource> ar = [Select Id FROM AssignedResource];
        if(ar.isEmpty()){
        	AssignedResource assignedRes = CORE_DataFaker_ServiceAppointment.getAssignedResourceWithoutInsert(sa.Id, serviceRes.Id);
        	database.insert(assignedRes, true);
        }
        
        Test.stopTest();

        List<ServiceAppointment> sas = [SELECT Id, CORE_VideoMeetingId__c, Status FROM ServiceAppointment WHERE Id = :sa.Id];
        System.assertEquals(null, sas[0].CORE_VideoMeetingId__c);
        System.assertEquals('CORE_PKL_Error', sas[0].Status);

        CORE_LOG__c log = [SELECT Id, CORE_HTTP_Status_Code__c FROM CORE_LOG__c WHERE CORE_RecordId__c = :sa.Id LIMIT 1];
        System.assertEquals(400, log.CORE_HTTP_Status_Code__c);
    }

    @IsTest
    public static void test_video_appointment_apex_error() {
        CORE_CountrySpecificSettings__c setting = new CORE_CountrySpecificSettings__c(
                CORE_TwilioCreateVideoMeeting__c = true,
                CORE_TwilioErrorEmailRecipients__c = 'test@test.com'
        );
        database.insert(setting, true);

        Opportunity opportunity = [SELECT Id FROM Opportunity];

        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('CORE_Twilio_Video_Appointment_Booking_SUCCESS_RESP');
        mock.setStatusCode(200);

        // Set the mock callout mode
        Test.setMock(HttpCalloutMock.class, mock);

        Datetime dtNow = DateTime.Now();

        ServiceAppointment sa = CORE_DataFaker_ServiceAppointment.getServiceAppointmentWithoutInsert(opportunity.Id, dtNow);
        sa.AppointmentType = 'Video';

        Test.startTest();
        database.insert(sa, true);
        Test.stopTest();

        List<ServiceAppointment> sas = [SELECT Id, CORE_VideoMeetingId__c FROM ServiceAppointment WHERE Id = :sa.Id];
        System.assertEquals(null, sas[0].CORE_VideoMeetingId__c);

        CORE_LOG__c log = [SELECT Id, CORE_ApexExceptionError__c FROM CORE_LOG__c WHERE CORE_RecordId__c = :sa.Id LIMIT 1];
        System.assertNotEquals(null, log.CORE_ApexExceptionError__c);
    }

    @IsTest
    public static void test_video_appointment_meeting_update() {
        CORE_CountrySpecificSettings__c setting = new CORE_CountrySpecificSettings__c(
                CORE_TwilioCreateVideoMeeting__c = true,
                CORE_TwilioErrorEmailRecipients__c = 'test@test.com'
        );
        database.insert(setting, true);

        Opportunity opportunity = [SELECT Id FROM Opportunity];

        StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
        mock.setStaticResource('CORE_Twilio_Video_Appointment_Booking_SUCCESS_RESP');
        mock.setStatusCode(200);

        // Set the mock callout mode
        Test.setMock(HttpCalloutMock.class, mock);

        Datetime dtNow = DateTime.Now();

        ServiceResource serviceRes = CORE_DataFaker_ServiceAppointment.getServiceResource(Userinfo.getUserId(), 'test_user');

        ServiceAppointment sa = CORE_DataFaker_ServiceAppointment.getServiceAppointmentWithoutInsert(opportunity.Id, dtNow);
        sa.AppointmentType = 'Video';
        sa.CORE_VideoMeetingId__c = 'test';
        database.insert(sa, true);

        List<AssignedResource> ar = [Select Id FROM AssignedResource];
        if(ar.isEmpty()){
        	AssignedResource assignedRes = CORE_DataFaker_ServiceAppointment.getAssignedResourceWithoutInsert(sa.Id, serviceRes.Id);
        	database.insert(assignedRes, true);
        }
        Test.startTest();

        ServiceAppointment sas = [SELECT Id, CORE_VideoMeetingId__c, SchedStartTime, SchedEndTime FROM ServiceAppointment WHERE Id = :sa.Id];
        sas.SchedStartTime = sas.SchedStartTime.addHours(1);
        sas.SchedEndTime = sas.SchedStartTime.addHours(2);
        database.update(sas, true);

        Test.stopTest();

        System.assertNotEquals(null, sas.CORE_VideoMeetingId__c);
    }
}