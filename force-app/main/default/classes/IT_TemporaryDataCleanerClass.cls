public without sharing class IT_TemporaryDataCleanerClass implements Database.Batchable<sObject> {
    
    //CONSTANTS START
    public static final String CLASS_NAME = IT_TemporaryDataCleanerClass.class.getName();
    //CONSTANTS END
    
    public String[] taskIds = new String[]{};

    public IT_TemporaryDataCleanerClass() {

    }

    public IT_TemporaryDataCleanerClass(String[] recordIds) {
        this.taskIds = recordIds;
    }


    public Database.QueryLocator start(Database.BatchableContext BC){

        if (! taskIds.isEmpty() ) {
            return Database.getQueryLocator([
                SELECT Id, WhatId, CORE_Is_Incoming__c, CORE_Start_Date__c, 
                    CreatedDate, CompletedDateTime, CORE_Business_Hours__c, 
                    CORE_Resolution_Time__c, CORE_Business_Hours__r.Name
                FROM Task 
                WHERE Id IN: taskIds
                ]
            );
        }

        return Database.getQueryLocator([
            SELECT Id, WhatId, CORE_Is_Incoming__c, CORE_Start_Date__c, 
                CreatedDate, CompletedDateTime, CORE_Business_Hours__c, 
                CORE_Resolution_Time__c, CORE_Business_Hours__r.Name
            FROM Task 
            WHERE Status = 'CORE_PKL_Completed' 
                AND ( 
                    (CORE_Start_Date__c != null AND CompletedDateTime != null ) 
                    AND 
                    CORE_Resolution_Time__c = null
                )
            ]
        );
    }

    public void execute(Database.BatchableContext BC, List<Task> scope){
        Map<Id, Task[]> opportunity_TaskMap = new Map<Id, Task[]>();
        for (Task task : scope) {
            if(opportunity_TaskMap.containsKey(task.WhatId)) {
                opportunity_TaskMap.get(task.WhatId).add(task);
            } else {
                opportunity_TaskMap.put(task.WhatId, new Task[]{task});
            }
        }

        if (! opportunity_TaskMap.keySet().isEmpty() ) {
            Opportunity[] opportunities = [SELECT Id, CORE_OPP_Business_Hours__c FROM Opportunity WHERE Id IN: opportunity_TaskMap.keySet()];

            Task[] tasksToUpdate = new Task[]{};
            for(Opportunity opp : opportunities) {
                Task[] relatedTasks = opportunity_TaskMap.get(opp.Id);
                for (Task taskToUpdate : relatedTasks) {
                    if (String.isNotBlank(opp.CORE_OPP_Business_Hours__c)  ) {
						if (taskToUpdate.CORE_Business_Hours__c != opp.CORE_OPP_Business_Hours__c ) {
							taskToUpdate.CORE_Business_Hours__c = opp.CORE_OPP_Business_Hours__c;
						}
						taskToUpdate.CORE_Resolution_Time__c = calculateResolutionTime(taskToUpdate, opp.CORE_OPP_Business_Hours__c);

						tasksToUpdate.add(taskToUpdate);
					}
                }
            }

            if (! tasksToUpdate.isEmpty() ) {
                System.debug(CLASS_NAME + ' - execute -  tasksToUpdate are : ' + JSON.serialize(tasksToUpdate) );
                Database.update(tasksToUpdate, false);
            }
        }

        
    }

    public static Integer calculateResolutionTime(Task currentRecord, String businessHourId){
        Long diffBetween =0;
        if(currentRecord.CORE_Is_Incoming__c==true || (currentRecord.CORE_Is_Incoming__c==false && currentRecord.CORE_Start_Date__c==null)){
            diffBetween = System.BusinessHours.diff(businessHourId, currentRecord.CreatedDate, currentRecord.CompletedDateTime);
        }else{
            diffBetween = System.BusinessHours.diff(businessHourId, currentRecord.CORE_Start_Date__c, currentRecord.CompletedDateTime);
        }
        System.debug('diffBetween '+diffBetween);
        Long seconds = (Long)diffBetween / 1000;
        return Integer.valueOf(seconds / 60) ;
    }

    public void finish(Database.BatchableContext BC){
    
    }
}