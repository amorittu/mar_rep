/**
* @author Fodil BOUDJEDIEN - Almavia
* @date 2022-03-24
* @group Opportunity Mass re-registration (Single) et (Massive)
* @description Test class of CORE_OpportunityReregistration ;
* @test class 
*/
@isTest
public with sharing class CORE_OpportunityReregistrationTest {

    @TestSetup
    static void makeData(){
        CORE_DataFaker_CatalogHierarchy catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test');
        Account account = CORE_DataFaker_Account.getStudentAccount('test');
        Opportunity Opportunity = CORE_DataFaker_Opportunity.getOpportunity(catalogHierarchy.division.Id, 
            catalogHierarchy.businessUnit.Id, 
            catalogHierarchy.studyArea.Id,
            catalogHierarchy.curriculum.Id,
            catalogHierarchy.level.Id,
            catalogHierarchy.intake.Id,
            account.Id, 
            'Registered', 
            'Registered - Classical'
        );
        Opportunity opp = new Opportunity(id=Opportunity.Id,CORE_OPP_Academic_Year__c = 'CORE_PKL_2021-2022');
        update opp;

    }


    @isTest
    public static void testCORE_OpportunityReregistration() {
        CORE_OpportunityReregistration.InputWrapper input = new CORE_OpportunityReregistration.InputWrapper();
        Opportunity opp = [SELECT Id,CORE_OPP_Academic_Year__c,CORE_OPP_Business_Unit__c FROM Opportunity limit 1];
        input.enrolledOpportunity = opp;
        CORE_Business_Unit__c bu = [SELECT id FROM CORE_Business_Unit__c limit 1];
        input.businessUnitId = bu.Id; 
        input.academicYear = 'CORE_PKL_2021-2022'; 
        List<CORE_OpportunityReregistration.OutputWrapper> returns = CORE_OpportunityReregistration.invoke(new List<CORE_OpportunityReregistration.InputWrapper>{input});
    }
}