@IsTest
public class CORE_AsyncLFAWorkerOnlineShopTest {
    @TestSetup
    static void makeData(){
        String uniqueKey = 'testMakeData';
        String mobile = '+33646454548';
        String phone = '+33246454645';
        String email = 'testMakeData@Test.com';
        CORE_CatalogHierarchyModel catalog1 = new CORE_DataFaker_CatalogHierarchy(uniqueKey + '1').catalogHierarchy;
        CORE_CatalogHierarchyModel catalog2 = new CORE_DataFaker_CatalogHierarchy(uniqueKey + '2').catalogHierarchy;
        

        CORE_TriggerUtils.setBypassTrigger();

        CORE_DataFaker_LeadAcquisitionBuffer.getBuffer(uniqueKey + '1', mobile, phone, email, catalog1);
        CORE_LeadAcquisitionBuffer__c buffer = CORE_DataFaker_LeadAcquisitionBuffer.getBuffer(uniqueKey + '2', mobile, phone, email, catalog2);
        buffer.CORE_MI_OnlineShop_ExternalId__c = 'testOnlineShop2';
        update buffer;
        CORE_TriggerUtils.setBypassTrigger();
    }

    @IsTest
    public static void constructor_Should_initialize_attributes() {
        List<CORE_LeadAcquisitionBuffer__c> buffers = [Select Id, CORE_Status__c FROM CORE_LeadAcquisitionBuffer__c];
        CORE_AsyncLFAWorker worker = new CORE_AsyncLFAWorkerOnlineShop(buffers);

        System.assertNotEquals(null, worker.bufferRecords);
        System.assertNotEquals(0, worker.bufferRecords.size());
        System.assertNotEquals(null, worker.bufferRecordByIds);
        System.assertNotEquals(null, worker.buffersInError);
        System.assertNotEquals(null, worker.dmlOptions);
        System.assertNotEquals(null, worker.accountUpserted);
        System.assertNotEquals(null, worker.opportunityUpserted);
        System.assertNotEquals(null, worker.bufferIdsByOpportunityIds);
        System.assertNotEquals(null, worker.availableAcademicYears);
        System.assertNotEquals(0, worker.availableAcademicYears.size());
        System.assertNotEquals(null, worker.tasksToCreate);
        System.assertNotEquals(null, worker.dataMaker);
    }

    @IsTest
    public static void execute_should_upsert_account_and_opportunities_and_consents_and_pocs_and_tasks_and_campaignmember() {
        String uniqueKey = 'testExecute';
        String mobile = '+33600000000';
        String phone = '+33200000000';
        String email = 'testExecute@Test.com';

        CORE_Intake__c intake = [Select Id FROM CORE_Intake__c limit 1];
        Product2 product = [Select Id FROM Product2 limit 1];
        CORE_CatalogHierarchyModel catalog3 = new CORE_DataFaker_CatalogHierarchy(uniqueKey + '3').catalogHierarchy;
        CORE_CatalogHierarchyModel catalog4 = new CORE_DataFaker_CatalogHierarchy(uniqueKey + '4').catalogHierarchy;
        catalog3.intake.Id = intake.Id;
        catalog3.product.Id = product.Id;

        id pricebookId = CORE_DataFaker_PriceBook.getStandardPricebookId();
        PricebookEntry pbe1 = CORE_DataFaker_PriceBook.getPriceBookEntry(catalog3.product.Id, pricebookId, 100.0);
        PricebookEntry pbe2 = CORE_DataFaker_PriceBook.getPriceBookEntry(catalog4.product.Id, pricebookId, 2000.0);
        CORE_PriceBook_BU__c pbu1 = CORE_DataFaker_PriceBook.getPriceBookBu(pricebookId, catalog3.businessUnit.Id, uniqueKey + '3');
        CORE_PriceBook_BU__c pbu2 = CORE_DataFaker_PriceBook.getPriceBookBu(pricebookId, catalog4.businessUnit.Id, uniqueKey + '4');

        Campaign campaign = CORE_DataFaker_Campaign.getCampaign(uniqueKey);

        Account account = CORE_DataFaker_Account.getStudentAccount('test1');
        account.personEmail = email;
        account.Phone = phone;
        Update account;
        Opportunity opportunity1 = CORE_DataFaker_Opportunity.getOpportunity(catalog3.division.Id, catalog3.businessUnit.Id, account.Id, 'Prospect', 'Prospect - New');
        opportunity1.CORE_OPP_OnlineShop_Id__c = 'testOnlineShop2';
        Opportunity opportunity2 = CORE_DataFaker_Opportunity.getOpportunity(catalog4.division.Id, catalog4.businessUnit.Id, account.Id, 'Prospect', 'Prospect - New');
        //opportunity2.CORE_OPP_OnlineShop_Id__c = 'testOnlineShop2';

        update opportunity1;

        CORE_TriggerUtils.setBypassTrigger();
        CORE_LeadAcquisitionBuffer__c buffer1 = CORE_DataFaker_LeadAcquisitionBuffer.getBuffer(uniqueKey + '3', mobile, phone, email, catalog3);
        buffer1 = CORE_DataFaker_LeadAcquisitionBuffer.getBufferWithCampaignMember(buffer1, campaign.Id);
        buffer1.CORE_MI_OnlineShop_ExternalId__c = 'testOnlineShop2';
        CORE_LeadAcquisitionBuffer__c buffer2 = CORE_DataFaker_LeadAcquisitionBuffer.getBuffer(uniqueKey + '4', mobile, phone, email, catalog4);
        buffer2 = CORE_DataFaker_LeadAcquisitionBuffer.getBufferWithCampaignMember(buffer2, campaign.Id);
        buffer1.CORE_MI_OnlineShop_ExternalId__c = 'testOnlineShop1';
        List<CORE_LeadAcquisitionBuffer__c> buffers = new List<CORE_LeadAcquisitionBuffer__c> {buffer1, buffer2};
        Update buffers; 

        CORE_TriggerUtils.setBypassTrigger();

        CORE_AsyncLFAWorker worker = new CORE_AsyncLFAWorkerOnlineShop(buffers);

        Test.startTest();
        worker.execute();
        Test.stopTest();

        List<Account> accounts = [SELECT Id FROM Account];
        List<Opportunity> opportunities = [SELECT Id FROM Opportunity];
        List<OpportunityLineItem> opportunityLineItem = [SELECT Id FROM OpportunityLineItem];
        List<CORE_Point_of_Contact__c> pocs = [SELECT Id FROM CORE_Point_of_Contact__c];
        List<CORE_Consent__c> consents = [SELECT Id FROM CORE_Consent__c];
        List<CampaignMember> campaignMembers = [SELECT Id FROM CampaignMember];
        List<Task> tasks = [SELECT Id FROM Task WHERE Type = 'CORE_PKL_Web_Form'];


        System.assertEquals(false, accounts.isEmpty());
        System.assertEquals(false, opportunities.isEmpty());
        System.assertEquals(false, opportunityLineItem.isEmpty());
        System.assertEquals(false, pocs.isEmpty());
        System.assertEquals(false, consents.isEmpty());
        System.assertEquals(true, campaignMembers.isEmpty());
        System.assertEquals(false, tasks.isEmpty());

        List<CORE_LeadAcquisitionBuffer__c> buffersAfterUpdate = [Select Id, CORE_Status__c FROM CORE_LeadAcquisitionBuffer__c where CORE_Status__c != :CORE_AsyncLFAWorker.PENDING_STATUS];
        System.assertEquals(buffers.size(), buffersAfterUpdate.size());

        for(CORE_LeadAcquisitionBuffer__c bufferAfterUpdate : buffersAfterUpdate){
            System.assertNotEquals(null, bufferAfterUpdate.CORE_Status__c);
            System.assertNotEquals(CORE_AsyncLFAWorker.PENDING_STATUS, bufferAfterUpdate.CORE_Status__c);
        }
    }
}