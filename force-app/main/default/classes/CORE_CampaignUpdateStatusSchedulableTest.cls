@IsTest
public class CORE_CampaignUpdateStatusSchedulableTest {
    @IsTest
    public static void campaign_update_status_Schedulable_Should_Be_Launch() {
        test.starttest();
        CORE_CampaignUpdateStatusSchedulable myClass = new CORE_CampaignUpdateStatusSchedulable();   
        String chron = '0 0 2 * * ?';        
        system.schedule('Test Sched', chron, myClass);
        test.stopTest();

        List<AsyncApexJob> jobsScheduled = [select Id, ApexClassID, ApexClass.Name, Status, JobType from AsyncApexJob where JobType = 'ScheduledApex'];
        System.assertEquals(1, jobsScheduled.size(), 'expecting one scheduled job');
        System.assertEquals('CORE_CampaignUpdateStatusSchedulable', jobsScheduled[0].ApexClass.Name, 'expecting specific scheduled job');

        List<AsyncApexJob> jobsApexBatch = [select Id, ApexClassID, ApexClass.Name, Status, JobType from AsyncApexJob where JobType = 'BatchApex'];
        System.assertEquals(1, jobsApexBatch.size(), 'expecting one apex batch job');
        System.assertEquals('CORE_CampaignUpdateStatusBatchable', jobsApexBatch[0].ApexClass.Name, 'expecting specific batch job');
    }
}