@IsTest
public class CORE_RecordTypeUtilsTest {
    @IsTest
    public static void getDeveloperNameByIds_Should_Return_map_of_recordTypeDevName_by_ids() {
        String devName1 = 'CORE_Enrollment';
        String devName2 = 'CORE_Re_registration';
        Id recordTypeId1 = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get(devName1).getRecordTypeId();
        Id recordTypeId2 = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get(devName2).getRecordTypeId();

        Test.startTest();
        Map<Id, String> developerNameByIds = CORE_RecordTypeUtils.getDeveloperNameByIds('Opportunity', new Set<Id> {recordTypeId1,recordTypeId2});
        Test.stopTest();

        System.assertEquals(2, developerNameByIds.keySet().Size());
        System.assertEquals(devName1, developerNameByIds.get(recordTypeId1));
        System.assertEquals(devName2, developerNameByIds.get(recordTypeId2));
    }
}