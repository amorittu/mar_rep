/**
 * Created by ftrapani on 16/11/2021.
 */
@IsTest
public with sharing class IT_InvocableNextBusinessHoursTest {

    @IsTest
    static void testBehavior(){
        IT_InvocableNextBusinessHours.FlowInputs flowInput = new IT_InvocableNextBusinessHours.FlowInputs();
        flowInput.businessHoursId = [SELECT Id FROM BusinessHours WHERE IsDefault=true LIMIT 1].Id;
        flowInput.targetTime = System.now();
        flowInput.hours = 2;
        List<IT_InvocableNextBusinessHours.FlowInputs> flowInputs = new List<IT_InvocableNextBusinessHours.FlowInputs>{flowInput};
        IT_InvocableNextBusinessHours.getNextBH(flowInputs);
    }
}