@IsTest
public class CORE_LeadFormAcquInputValidatorTest {

    @IsTest
    public static void Constructor_Should_initialize_attrubutes_and_picklists() {
		DateTime apiCallDate = System.now();
		CORE_LeadFormAcquisitionWrapper.GlobalWrapper inputParameters = CORE_DataFaker_LeadFormAcquWrapper.getGlobalWrapper();
		Boolean countryPicklistIsActivated = CORE_SobjectUtils.doesFieldExist('User', 'Countrycode');

		Test.startTest();
        CORE_LeadFormAcquistionInputValidator lfaiv = new CORE_LeadFormAcquistionInputValidator(inputParameters, apiCallDate, countryPicklistIsActivated);
		Test.stopTest();

		System.assertEquals(apiCallDate, lfaiv.apiCallDate);
		System.assertEquals(inputParameters, lfaiv.inputParameters);


		System.assertNotEquals(null, lfaiv.pklSalutation);
		System.assert(lfaiv.pklSalutation.size() > 0);
		//System.assertNotEquals(null, lfaiv.pklStateCode);
		//System.assert(lfaiv.pklStateCode.size() > 0);
		//System.assertNotEquals(null, lfaiv.pklCountryCode);
		//System.assert(lfaiv.pklCountryCode.size() > 0);
		System.assertNotEquals(null, lfaiv.pklLanguageWebsite);
		System.assert(lfaiv.pklLanguageWebsite.size() > 0);
		System.assertNotEquals(null, lfaiv.pklMainNationality);
		System.assert(lfaiv.pklMainNationality.size() > 0);
		System.assertNotEquals(null, lfaiv.pklConsentType);
		System.assert(lfaiv.pklConsentType.size() > 0);
		System.assertNotEquals(null, lfaiv.pklConsentChannel);
		System.assert(lfaiv.pklConsentChannel.size() > 0);
		System.assertNotEquals(null, lfaiv.pklPocCaptureChannel);
		System.assert(lfaiv.pklPocCaptureChannel.size() > 0);
		System.assertNotEquals(null, lfaiv.pklPocCampaignMedium);
		System.assert(lfaiv.pklPocCampaignMedium.size() > 0);
		System.assertNotEquals(null, lfaiv.pklPocDevice);
		System.assert(lfaiv.pklPocDevice.size() > 0);
		System.assertNotEquals(null, lfaiv.pklPocNature);
		System.assert(lfaiv.pklPocNature.size() > 0);
		System.assertNotEquals(null, lfaiv.pklOppAcademicYear);
		System.assert(lfaiv.pklOppAcademicYear.size() > 0);
		System.assertNotEquals(null, lfaiv.pklLearningMaterial);
		System.assert(lfaiv.pklLearningMaterial.size() > 0);
    }

	@IsTest
    public static void getFieldsErrors_should_return_list_of_errorWrapper() {
		DateTime apiCallDate = System.now();
		CORE_LeadFormAcquisitionWrapper.GlobalWrapper inputParameters = CORE_DataFaker_LeadFormAcquWrapper.getGlobalWrapper();
		Boolean countryPicklistIsActivated = CORE_SobjectUtils.doesFieldExist('User', 'Countrycode');
		CORE_LeadFormAcquistionInputValidator lfaiv = new CORE_LeadFormAcquistionInputValidator(inputParameters, apiCallDate, countryPicklistIsActivated);
		Test.startTest();
        List<CORE_LeadFormAcquisitionWrapper.ErrorWrapper> result = lfaiv.getFieldsErrors();
		System.debug('result = '+ result);
		System.assertNotEquals(null, result);
		System.assertEquals(0, result.size());

		inputParameters.personAccount = null;
		lfaiv = new CORE_LeadFormAcquistionInputValidator(inputParameters, apiCallDate, countryPicklistIsActivated);
		result = lfaiv.getFieldsErrors();
		System.assertNotEquals(null, result);
		System.assertEquals(1, result.size());

		inputParameters.mainInterest.academicYear = 'error';
		lfaiv = new CORE_LeadFormAcquistionInputValidator(inputParameters, apiCallDate, countryPicklistIsActivated);
		result = lfaiv.getFieldsErrors();
		System.assertNotEquals(null, result);
		System.assertEquals(2, result.size());

		inputParameters.secondInterest.academicYear = 'error';
		lfaiv = new CORE_LeadFormAcquistionInputValidator(inputParameters, apiCallDate, countryPicklistIsActivated);
		result = lfaiv.getFieldsErrors();
		System.assertNotEquals(null, result);
		System.assertEquals(2, result.size());

		Test.stopTest();
    }

	@IsTest
    public static void getMissingParameters_should_return_null_when_mandatory_fields_are_all_good() {
		DateTime apiCallDate = System.now();
		CORE_LeadFormAcquisitionWrapper.GlobalWrapper inputParameters = CORE_DataFaker_LeadFormAcquWrapper.getGlobalWrapper();
		Boolean countryPicklistIsActivated = CORE_SobjectUtils.doesFieldExist('User', 'Countrycode');

		CORE_LeadFormAcquistionInputValidator lfaiv = new CORE_LeadFormAcquistionInputValidator(inputParameters, apiCallDate, countryPicklistIsActivated);

		Test.startTest();
        CORE_LeadFormAcquisitionWrapper.ErrorWrapper result = lfaiv.getMissingParameters();

		System.assertEquals(null, result);
		Test.stopTest();
    }

	@IsTest
    public static void getMissingParameters_should_return_an_errorWrapper_when_mandatory_fields_are_missing() {
		DateTime apiCallDate = System.now();
		CORE_LeadFormAcquisitionWrapper.GlobalWrapper inputParameters = CORE_DataFaker_LeadFormAcquWrapper.getGlobalWrapper();
		inputParameters.personAccount = null;
		Boolean countryPicklistIsActivated = CORE_SobjectUtils.doesFieldExist('User', 'Countrycode');

		CORE_LeadFormAcquistionInputValidator lfaiv = new CORE_LeadFormAcquistionInputValidator(inputParameters, apiCallDate, countryPicklistIsActivated);

		Test.startTest();
        CORE_LeadFormAcquisitionWrapper.ErrorWrapper result = lfaiv.getMissingParameters();

		System.assertNotEquals(null, result);
		System.assertEquals(CORE_LeadFormAcquistionInputValidator.MANDATORY_FIELDS_CODE, result.errorCode);
		System.assertEquals(CORE_LeadFormAcquistionInputValidator.MANDATORY_FIELDS_MSG, result.message);
		System.assertEquals(apiCallDate, result.apiCallDate);
		System.assertEquals(1, result.fields.size());
		System.assertEquals('personAccount', result.fields.get(0));
		Test.stopTest();
    }

	@IsTest
    public static void getPicklistErrors_should_return_null_when_all_picklist_values_are_valid() {
		DateTime apiCallDate = System.now();
		CORE_LeadFormAcquisitionWrapper.GlobalWrapper inputParameters = CORE_DataFaker_LeadFormAcquWrapper.getGlobalWrapper();
		Boolean countryPicklistIsActivated = CORE_SobjectUtils.doesFieldExist('User', 'Countrycode');

		CORE_LeadFormAcquistionInputValidator lfaiv = new CORE_LeadFormAcquistionInputValidator(inputParameters, apiCallDate, countryPicklistIsActivated);

		Test.startTest();
        CORE_LeadFormAcquisitionWrapper.ErrorWrapper result = lfaiv.getPicklistErrors();

		System.assertEquals(null, result);
		Test.stopTest();
    }

	@IsTest
    public static void getPicklistErrors_should_return_an_errorWrapper_when_a_picklist_value_is_invalid() {
		DateTime apiCallDate = System.now();
		CORE_LeadFormAcquisitionWrapper.GlobalWrapper inputParameters = CORE_DataFaker_LeadFormAcquWrapper.getGlobalWrapper();
		inputParameters.personAccount.salutation = 'error';
		Boolean countryPicklistIsActivated = CORE_SobjectUtils.doesFieldExist('User', 'Countrycode');

		CORE_LeadFormAcquistionInputValidator lfaiv = new CORE_LeadFormAcquistionInputValidator(inputParameters, apiCallDate, countryPicklistIsActivated);

		Test.startTest();
        CORE_LeadFormAcquisitionWrapper.ErrorWrapper result = lfaiv.getPicklistErrors();

		System.assertNotEquals(null, result);
		System.assertEquals(CORE_LeadFormAcquistionInputValidator.INVALID_PICKLIST_CODE, result.errorCode);
		System.assertEquals(CORE_LeadFormAcquistionInputValidator.INVALID_PICKLIST_MSG, result.message);
		System.assertEquals(apiCallDate, result.apiCallDate);
		System.assertEquals(1, result.fields.size());
		System.assertEquals('personAccount.salutation', result.fields.get(0));

		inputParameters.personAccount.languageWebsite = 'error';
		lfaiv = new CORE_LeadFormAcquistionInputValidator(inputParameters, apiCallDate, countryPicklistIsActivated);
		result = lfaiv.getPicklistErrors();

		System.assertNotEquals(null, result);
		System.assertEquals(2, result.fields.size());
		System.assertEquals('personAccount.languageWebsite', result.fields.get(1));

		inputParameters.consents.get(0).consentType = 'error';
		lfaiv = new CORE_LeadFormAcquistionInputValidator(inputParameters, apiCallDate, countryPicklistIsActivated);
		result = lfaiv.getPicklistErrors();

		System.assertNotEquals(null, result);
		System.assertEquals(3, result.fields.size());
		System.assertEquals('consents[' + 0 + '].consentType', result.fields.get(2), countryPicklistIsActivated);
		
		inputParameters.consents.get(0).channel = 'error';
		lfaiv = new CORE_LeadFormAcquistionInputValidator(inputParameters, apiCallDate, countryPicklistIsActivated);
		result = lfaiv.getPicklistErrors();

		System.assertNotEquals(null, result);
		System.assertEquals(4, result.fields.size());
		System.assertEquals('consents[' + 0 + '].channel', result.fields.get(3));

		inputParameters.pointOfContact.captureChannel = 'error';
		lfaiv = new CORE_LeadFormAcquistionInputValidator(inputParameters, apiCallDate, countryPicklistIsActivated);
		result = lfaiv.getPicklistErrors();

		System.assertNotEquals(null, result);
		System.assertEquals(5, result.fields.size());
		System.assertEquals('pointOfContact.captureChannel', result.fields.get(4));

		inputParameters.pointOfContact.campaignMedium = 'error';
		lfaiv = new CORE_LeadFormAcquistionInputValidator(inputParameters, apiCallDate, countryPicklistIsActivated);
		result = lfaiv.getPicklistErrors();

		System.assertNotEquals(null, result);
		System.assertEquals(6, result.fields.size());
		System.assertEquals('pointOfContact.campaignMedium', result.fields.get(5));

		inputParameters.pointOfContact.device = 'error';
		lfaiv = new CORE_LeadFormAcquistionInputValidator(inputParameters, apiCallDate, countryPicklistIsActivated);
		result = lfaiv.getPicklistErrors();

		System.assertNotEquals(null, result);
		System.assertEquals(7, result.fields.size());
		System.assertEquals('pointOfContact.device', result.fields.get(6));

		inputParameters.pointOfContact.nature = 'error';
		lfaiv = new CORE_LeadFormAcquistionInputValidator(inputParameters, apiCallDate, countryPicklistIsActivated);
		result = lfaiv.getPicklistErrors();

		System.assertNotEquals(null, result);
		System.assertEquals(8, result.fields.size());
		System.assertEquals('pointOfContact.nature', result.fields.get(7));
		
		Test.stopTest();
    }

	@IsTest
    public static void getOppPicklistErrors_should_return_empty_list_when_academicYear_is_empty_or_with_available_value() {
		DateTime apiCallDate = System.now();
		CORE_LeadFormAcquisitionWrapper.GlobalWrapper inputParameters = CORE_DataFaker_LeadFormAcquWrapper.getGlobalWrapper();
		Boolean countryPicklistIsActivated = CORE_SobjectUtils.doesFieldExist('User', 'Countrycode');

		CORE_LeadFormAcquistionInputValidator lfaiv = new CORE_LeadFormAcquistionInputValidator(inputParameters, apiCallDate, countryPicklistIsActivated);
		CORE_LeadFormAcquisitionWrapper.OpportunityWrapper opportunity = inputParameters.mainInterest;
		opportunity.academicYear = null; 
		Test.startTest();
        List<String> result = lfaiv.getOppPicklistErrors(opportunity,'mainInterest');

		System.assertNotEquals(null, result);
		System.assertEquals(0, result.size());

		opportunity.academicYear = 'CORE_PKL_2022-2023'; 
		result = lfaiv.getOppPicklistErrors(opportunity,'mainInterest');

		System.assertNotEquals(null, result);
		System.assertEquals(0, result.size());

		result = lfaiv.getOppPicklistErrors(null,'mainInterest');

		System.assertNotEquals(null, result);
		System.assertEquals(0, result.size());
		Test.stopTest();
    }

	@IsTest
    public static void getOppPicklistErrors_should_return_list_of_picklist_when_academicYear_is_invalid() {
		DateTime apiCallDate = System.now();
		CORE_LeadFormAcquisitionWrapper.GlobalWrapper inputParameters = CORE_DataFaker_LeadFormAcquWrapper.getGlobalWrapper();
		Boolean countryPicklistIsActivated = CORE_SobjectUtils.doesFieldExist('User', 'Countrycode');

		CORE_LeadFormAcquistionInputValidator lfaiv = new CORE_LeadFormAcquistionInputValidator(inputParameters, apiCallDate, countryPicklistIsActivated);
		CORE_LeadFormAcquisitionWrapper.OpportunityWrapper opportunity = inputParameters.mainInterest;
		opportunity.academicYear = 'test'; 
		Test.startTest();
        List<String> result = lfaiv.getOppPicklistErrors(opportunity,'mainInterest');

		System.assertNotEquals(null, result);
		System.assertEquals(1, result.size());
		System.assertEquals('mainInterest.academicYear' , result.get(0));
		Test.stopTest();
    }

	@IsTest
    public static void getOppPicklistErrors_should_return_empty_list_when_learningMaterial_is_empty_or_with_available_value() {
		DateTime apiCallDate = System.now();
		CORE_LeadFormAcquisitionWrapper.GlobalWrapper inputParameters = CORE_DataFaker_LeadFormAcquWrapper.getGlobalWrapper();
		Boolean countryPicklistIsActivated = CORE_SobjectUtils.doesFieldExist('User', 'Countrycode');

		CORE_LeadFormAcquistionInputValidator lfaiv = new CORE_LeadFormAcquistionInputValidator(inputParameters, apiCallDate, countryPicklistIsActivated);
		CORE_LeadFormAcquisitionWrapper.OpportunityWrapper opportunity = inputParameters.mainInterest;
		opportunity.academicYear = null; 
		Test.startTest();
        List<String> result = lfaiv.getOppPicklistErrors(opportunity,'mainInterest');

		System.assertNotEquals(null, result);
		System.assertEquals(0, result.size());

		opportunity.learningMaterial = 'CORE_PKL_Paper'; 
		result = lfaiv.getOppPicklistErrors(opportunity,'mainInterest');

		System.assertNotEquals(null, result);
		System.assertEquals(0, result.size());

		result = lfaiv.getOppPicklistErrors(null,'mainInterest');

		System.assertNotEquals(null, result);
		System.assertEquals(0, result.size());
		Test.stopTest();
    }

	@IsTest
    public static void getOppPicklistErrors_should_return_list_of_picklist_when_learningMaterial_is_invalid() {
		DateTime apiCallDate = System.now();
		CORE_LeadFormAcquisitionWrapper.GlobalWrapper inputParameters = CORE_DataFaker_LeadFormAcquWrapper.getGlobalWrapper();
		Boolean countryPicklistIsActivated = CORE_SobjectUtils.doesFieldExist('User', 'Countrycode');

		CORE_LeadFormAcquistionInputValidator lfaiv = new CORE_LeadFormAcquistionInputValidator(inputParameters, apiCallDate, countryPicklistIsActivated);
		CORE_LeadFormAcquisitionWrapper.OpportunityWrapper opportunity = inputParameters.mainInterest;
		opportunity.learningMaterial = 'test'; 
		Test.startTest();
        List<String> result = lfaiv.getOppPicklistErrors(opportunity,'mainInterest');

		System.assertNotEquals(null, result);
		System.assertEquals(1, result.size());
		System.assertEquals('mainInterest.learningMaterial' , result.get(0));
		Test.stopTest();
    }

	@IsTest
    public static void getAccountMissingParameters_Should_return_empty_list_when_no_errors_are_found() {
		DateTime apiCallDate = System.now();
		CORE_LeadFormAcquisitionWrapper.GlobalWrapper inputParameters = CORE_DataFaker_LeadFormAcquWrapper.getGlobalWrapper();
		//CORE_LeadFormAcquistionInputValidator lfaiv = new CORE_LeadFormAcquistionInputValidator(inputParameters, apiCallDate);
		CORE_LeadFormAcquisitionWrapper.PersonAccountWrapper account = inputParameters.personAccount;
		
		Test.startTest();
        List<String> result = CORE_LeadFormAcquistionInputValidator.getAccountMissingParameters(account);

		System.assertNotEquals(null, result);
		System.assertEquals(0, result.size());
		Test.stopTest();
    }
	
	@IsTest
    public static void getAccountMissingParameters_Should_return_list_of_errors_when_mandatory_parameters_are_not_found() {
		DateTime apiCallDate = System.now();
		String jsonObject = 'personAccount';

		CORE_LeadFormAcquisitionWrapper.GlobalWrapper inputParameters = CORE_DataFaker_LeadFormAcquWrapper.getGlobalWrapper();
		//CORE_LeadFormAcquistionInputValidator lfaiv = new CORE_LeadFormAcquistionInputValidator(inputParameters, apiCallDate);

		CORE_LeadFormAcquisitionWrapper.PersonAccountWrapper account = inputParameters.personAccount;
		CORE_LeadFormAcquisitionWrapper.PersonAccountWrapper accountTmp = account;
		Test.startTest();
        List<String> result = CORE_LeadFormAcquistionInputValidator.getAccountMissingParameters(null);

		System.assertNotEquals(null, result);
		System.assertEquals(1, result.size());
		System.assertEquals(jsonObject, result.get(0));

		accountTmp.creationDate = null;
		result = CORE_LeadFormAcquistionInputValidator.getAccountMissingParameters(accountTmp);

		System.assertNotEquals(null, result);
		System.assertEquals(1, result.size());
		System.assertEquals(jsonObject + '.creationDate', result.get(0));

		accountTmp.lastName = null;
		result = CORE_LeadFormAcquistionInputValidator.getAccountMissingParameters(accountTmp);

		System.assertNotEquals(null, result);
		System.assertEquals(2, result.size());
		System.assertEquals(jsonObject + '.creationDate', result.get(0));
		System.assertEquals(jsonObject + '.lastName', result.get(1));

		accountTmp.firstName = null;
		result = CORE_LeadFormAcquistionInputValidator.getAccountMissingParameters(accountTmp);

		System.assertNotEquals(null, result);
		System.assertEquals(3, result.size());
		System.assertEquals(jsonObject + '.creationDate', result.get(0));
		System.assertEquals(jsonObject + '.lastName', result.get(1));
		System.assertEquals(jsonObject + '.firstName', result.get(2));

		accountTmp.mobilePhone = null;
		accountTmp.phone = null;
		accountTmp.emailAddress = null;
		result = CORE_LeadFormAcquistionInputValidator.getAccountMissingParameters(accountTmp);

		System.assertNotEquals(null, result);
		System.assertEquals(6, result.size());
		System.assertEquals(jsonObject + '.creationDate', result.get(0));
		System.assertEquals(jsonObject + '.lastName', result.get(1));
		System.assertEquals(jsonObject + '.firstName', result.get(2));
		System.assertEquals(jsonObject + '.mobilePhone', result.get(3));
		System.assertEquals(jsonObject + '.phone', result.get(4));
		System.assertEquals(jsonObject + '.emailAddress', result.get(5));
		Test.stopTest();
    }

	@IsTest
    public static void getConsentsMissingParameters_Should_return_empty_list_when_no_errors_are_found() {
		DateTime apiCallDate = System.now();
		String jsonObject = 'consents';

		CORE_LeadFormAcquisitionWrapper.GlobalWrapper inputParameters = CORE_DataFaker_LeadFormAcquWrapper.getGlobalWrapper();
		//CORE_LeadFormAcquistionInputValidator lfaiv = new CORE_LeadFormAcquistionInputValidator(inputParameters, apiCallDate);

		List<CORE_LeadFormAcquisitionWrapper.ConsentWrapper> consents = inputParameters.consents;

		Test.startTest();
        List<String> result = CORE_LeadFormAcquistionInputValidator.getConsentsMissingParameters(consents, true);

		System.assertNotEquals(null, result);
		System.assertEquals(0, result.size());

		Test.stopTest();
    }

	@IsTest
    public static void getConsentsMissingParameters_Should_return_list_of_errors_when_mandatory_parameters_are_missing() {
		DateTime apiCallDate = System.now();
		String jsonObject = 'consents';

		CORE_LeadFormAcquisitionWrapper.GlobalWrapper inputParameters = CORE_DataFaker_LeadFormAcquWrapper.getGlobalWrapper();
		//CORE_LeadFormAcquistionInputValidator lfaiv = new CORE_LeadFormAcquistionInputValidator(inputParameters, apiCallDate);

		List<CORE_LeadFormAcquisitionWrapper.ConsentWrapper> consents = inputParameters.consents;
		
		CORE_LeadFormAcquisitionWrapper.ConsentWrapper consent2 = (CORE_LeadFormAcquisitionWrapper.ConsentWrapper) JSON.deserialize(JSON.serialize(consents.get(0)), CORE_LeadFormAcquisitionWrapper.ConsentWrapper.class);

		consents.add(consent2);
		Test.startTest();
        List<String> result = CORE_LeadFormAcquistionInputValidator.getConsentsMissingParameters(null, true);

		System.assertNotEquals(null, result);
		System.assertEquals(1, result.size());
		System.assertEquals(jsonObject, result.get(0));

		result = CORE_LeadFormAcquistionInputValidator.getConsentsMissingParameters(new List<CORE_LeadFormAcquisitionWrapper.ConsentWrapper>(), true);

		System.assertNotEquals(null, result);
		System.assertEquals(1, result.size());
		System.assertEquals(jsonObject, result.get(0));

		Integer currentConsent = 1;
		consents.get(currentConsent).consentType = null;
		result = CORE_LeadFormAcquistionInputValidator.getConsentsMissingParameters(consents, true);

		System.assertNotEquals(null, result);
		System.assertEquals(1, result.size());
		String missingParameter = String.format('{0}[{1}].{2}',new List<Object>{jsonObject, currentConsent, 'consentType'});
		System.assertEquals(missingParameter, result.get(0));

		consents.get(currentConsent).channel = null;
		result = CORE_LeadFormAcquistionInputValidator.getConsentsMissingParameters(consents, true);

		System.assertNotEquals(null, result);
		System.assertEquals(2, result.size());
		missingParameter = String.format('{0}[{1}].{2}',new List<Object>{jsonObject, currentConsent, 'channel'});
		System.assertEquals(missingParameter, result.get(1));


		consents.get(currentConsent).description = null;
		result = CORE_LeadFormAcquistionInputValidator.getConsentsMissingParameters(consents, true);

		System.assertNotEquals(null, result);
		System.assertEquals(3, result.size());
		missingParameter = String.format('{0}[{1}].{2}',new List<Object>{jsonObject, currentConsent, 'description'});
		System.assertEquals(missingParameter, result.get(2));

		consents.get(currentConsent).division = null;
		result = CORE_LeadFormAcquistionInputValidator.getConsentsMissingParameters(consents, true);

		System.assertNotEquals(null, result);
		System.assertEquals(3, result.size());

		consents.get(currentConsent).businessUnit = null;
		result = CORE_LeadFormAcquistionInputValidator.getConsentsMissingParameters(consents, true);

		System.assertNotEquals(null, result);
		System.assertEquals(4, result.size());
		missingParameter = String.format('{0}[{1}].{2}',new List<Object>{jsonObject, currentConsent, 'businessUnit'});
		System.assertEquals(missingParameter, result.get(3));
		Test.stopTest();
    }

	@IsTest
    public static void getOpportunityMissingParameters_Should_return_empty_list_when_no_errors_are_found() {
		DateTime apiCallDate = System.now();
		String jsonObject = 'mainInterest';

		CORE_LeadFormAcquisitionWrapper.GlobalWrapper inputParameters = CORE_DataFaker_LeadFormAcquWrapper.getGlobalWrapper();
		//CORE_LeadFormAcquistionInputValidator lfaiv = new CORE_LeadFormAcquistionInputValidator(inputParameters, apiCallDate);

		CORE_LeadFormAcquisitionWrapper.OpportunityWrapper opportunity = inputParameters.mainInterest;

		Test.startTest();
        List<String> result = CORE_LeadFormAcquistionInputValidator.getOpportunityMissingParameters(opportunity, true, true, jsonObject);

		System.assertNotEquals(null, result);
		System.assertEquals(0, result.size());

		result = CORE_LeadFormAcquistionInputValidator.getOpportunityMissingParameters(null, false, true, jsonObject);

		System.assertNotEquals(null, result);
		System.assertEquals(0, result.size());

		Test.stopTest();
    }

	@IsTest
    public static void getOpportunityMissingParameters_Should_return_list_of_errors_when_mandatory_parameters_are_missing() {
		DateTime apiCallDate = System.now();
		String jsonObject = 'mainInterest';

		CORE_LeadFormAcquisitionWrapper.GlobalWrapper inputParameters = CORE_DataFaker_LeadFormAcquWrapper.getGlobalWrapper();
		//CORE_LeadFormAcquistionInputValidator lfaiv = new CORE_LeadFormAcquistionInputValidator(inputParameters, apiCallDate);

		CORE_LeadFormAcquisitionWrapper.OpportunityWrapper opportunity = inputParameters.mainInterest;

		Test.startTest();
		
        List<String> result = CORE_LeadFormAcquistionInputValidator.getOpportunityMissingParameters(null, true, true, jsonObject);

		System.assertNotEquals(null, result);
		System.assertEquals(1, result.size());
		System.assertEquals(jsonObject, result.get(0));


		opportunity.division = null;
		result = CORE_LeadFormAcquistionInputValidator.getOpportunityMissingParameters(opportunity, false, true, jsonObject);

		System.assertNotEquals(null, result);
		System.assertEquals(0, result.size());

		opportunity.businessUnit = null;
		result = CORE_LeadFormAcquistionInputValidator.getOpportunityMissingParameters(opportunity, false, true, jsonObject);

		System.assertNotEquals(null, result);
		System.assertEquals(1, result.size());
		System.assertEquals(jsonObject + '.businessUnit', result.get(0));

		opportunity.studyArea = null;
		result = CORE_LeadFormAcquistionInputValidator.getOpportunityMissingParameters(opportunity, false, true, jsonObject);

		System.assertNotEquals(null, result);
		System.assertEquals(1, result.size());

		opportunity.isSuspect = false;
		result = CORE_LeadFormAcquistionInputValidator.getOpportunityMissingParameters(opportunity, true, true, jsonObject);

		System.assertNotEquals(null, result);
		System.assertEquals(1, result.size());
		System.assertEquals(jsonObject + '.businessUnit', result.get(0));
		Test.stopTest();
    }

	@IsTest
    public static void getPOCMissingParameters_Should_return_empty_list_when_no_errors_are_found() {
		DateTime apiCallDate = System.now();
		CORE_LeadFormAcquisitionWrapper.GlobalWrapper inputParameters = CORE_DataFaker_LeadFormAcquWrapper.getGlobalWrapper();
		//CORE_LeadFormAcquistionInputValidator lfaiv = new CORE_LeadFormAcquistionInputValidator(inputParameters, apiCallDate);

		CORE_LeadFormAcquisitionWrapper.PointOfContactWrapper pointOfContact = inputParameters.pointOfContact;

		Test.startTest();
        List<String> result = CORE_LeadFormAcquistionInputValidator.getPOCMissingParameters(pointOfContact, true);

		System.assertNotEquals(null, result);
		System.assertEquals(0, result.size());
		Test.stopTest();
    }

	@IsTest
    public static void getPOCMissingParameters_Should_return_list_of_error_when_mandatory_fields_are_missing() {
		DateTime apiCallDate = System.now();
		String jsonObject = 'pointOfContact';

		CORE_LeadFormAcquisitionWrapper.GlobalWrapper inputParameters = CORE_DataFaker_LeadFormAcquWrapper.getGlobalWrapper();
		//CORE_LeadFormAcquistionInputValidator lfaiv = new CORE_LeadFormAcquistionInputValidator(inputParameters, apiCallDate);

		CORE_LeadFormAcquisitionWrapper.PointOfContactWrapper pointOfContact = inputParameters.pointOfContact;

		Test.startTest();
        List<String> result = CORE_LeadFormAcquistionInputValidator.getPOCMissingParameters(null, true);

		System.assertNotEquals(null, result);
		System.assertEquals(1, result.size());
		System.assertEquals(jsonObject, result.get(0));

		pointOfContact.creationDate = null;
		result = CORE_LeadFormAcquistionInputValidator.getPOCMissingParameters(pointOfContact, true);

		System.assertNotEquals(null, result);
		System.assertEquals(1, result.size());
		System.assertEquals(jsonObject + '.creationDate', result.get(0));

		pointOfContact.nature = null;
		result = CORE_LeadFormAcquistionInputValidator.getPOCMissingParameters(pointOfContact, true);

		System.assertNotEquals(null, result);
		System.assertEquals(2, result.size());
		System.assertEquals(jsonObject + '.nature', result.get(1));

		pointOfContact.captureChannel = null;
		result = CORE_LeadFormAcquistionInputValidator.getPOCMissingParameters(pointOfContact, true);

		System.assertNotEquals(null, result);
		System.assertEquals(3, result.size());
		System.assertEquals(jsonObject + '.captureChannel', result.get(2));

		pointOfContact.origin = null;
		result = CORE_LeadFormAcquistionInputValidator.getPOCMissingParameters(pointOfContact, true);

		System.assertNotEquals(null, result);
		System.assertEquals(4, result.size());
		System.assertEquals(jsonObject + '.origin', result.get(3));
		Test.stopTest();
    }

	@IsTest
    public static void getCampaignSubscriptionMissingParameters_Should_return_empty_list_when_no_errors_are_found() {
		DateTime apiCallDate = System.now();
		CORE_LeadFormAcquisitionWrapper.GlobalWrapper inputParameters = CORE_DataFaker_LeadFormAcquWrapper.getGlobalWrapper();
		//CORE_LeadFormAcquistionInputValidator lfaiv = new CORE_LeadFormAcquistionInputValidator(inputParameters, apiCallDate);

		CORE_LeadFormAcquisitionWrapper.CampaignSubscriptionWrapper campaignSubscription = inputParameters.campaignSubscription;

		Test.startTest();
        List<String> result = CORE_LeadFormAcquistionInputValidator.getCampaignSubscriptionMissingParameters(null);

		System.assertNotEquals(null, result);
		System.assertEquals(0, result.size());
		
		result = CORE_LeadFormAcquistionInputValidator.getCampaignSubscriptionMissingParameters(campaignSubscription);

		System.assertNotEquals(null, result);
		System.assertEquals(0, result.size());
		Test.stopTest();
    }
	
	@IsTest
    public static void getCampaignSubscriptionMissingParameters_Should_return_list_of_errors_when_mandatory_fields_are_missing() {
		String jsonObjectName = 'campaignSubscription';

		DateTime apiCallDate = System.now();
		CORE_LeadFormAcquisitionWrapper.GlobalWrapper inputParameters = CORE_DataFaker_LeadFormAcquWrapper.getGlobalWrapper();
		//CORE_LeadFormAcquistionInputValidator lfaiv = new CORE_LeadFormAcquistionInputValidator(inputParameters, apiCallDate);

		CORE_LeadFormAcquisitionWrapper.CampaignSubscriptionWrapper campaignSubscription = inputParameters.campaignSubscription;

		Test.startTest();

		campaignSubscription.campaignId = null;
        List<String> result = CORE_LeadFormAcquistionInputValidator.getCampaignSubscriptionMissingParameters(campaignSubscription);

		System.assertNotEquals(null, result);
		System.assertEquals(1, result.size());
		System.assertEquals(jsonObjectName  + '.campaignId', result.get(0));
		Test.stopTest();
    }
}