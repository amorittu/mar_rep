/**
* @author Fodil BOUDJEDIEN - Almavia
* @date 2022-03-31
* @group LogManager
* @description Schedule class which is planned to execute CORE_LogManagerCleanerBatch for each API mentionned in the Custom metadata Types
* @test class CORE_LogManagerCleanerSchedulerTest
*/
public without sharing class CORE_LogManagerCleanerScheduler implements Schedulable {
    public void execute(SchedulableContext SC) {
        
        List<CORE_Log_Settings__mdt> settings =  CORE_Log_Settings__mdt.getAll().values();
        
        for (CORE_Log_Settings__mdt setting : settings){
            // if set is cleaner active
            if(setting.CORE_is_Cleaner_Active__c){
                Database.executeBatch(new CORE_LogManagerCleanerBatch(setting.CORE_Logged_API__c));
           }
        }
    }
}