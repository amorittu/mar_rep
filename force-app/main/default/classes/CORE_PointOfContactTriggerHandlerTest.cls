@IsTest
public class CORE_PointOfContactTriggerHandlerTest {
    
    @TestSetup
    public static void makeData(){
        CORE_DataFaker_CatalogHierarchy catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test1');
        Account account = CORE_DataFaker_Account.getStudentAccount('test1');
        Opportunity opportunity = CORE_DataFaker_Opportunity.getOpportunity(catalogHierarchy.division.Id, 
            catalogHierarchy.businessUnit.Id, 
            catalogHierarchy.studyArea.Id,
            catalogHierarchy.curriculum.Id,
            catalogHierarchy.level.Id,
            catalogHierarchy.intake.Id, 
            account.Id, 
            'Lead', 
            'Lead - New'
        );
    }
    
    
    @IsTest
    public static void insert_POC_update_first_poc_Opportunity_once() {
        Account account = [Select Id FROM Account LIMIT 1];
        Opportunity opportunity = [Select   Id,
                                            CORE_Insertion_Date__c,
                                            CORE_Campaign_Medium__c,
                                            CORE_Campaign_Name__c,
                                            CORE_Campaign_Source__c,
                                            CORE_Campaign_Term__c,
                                            CORE_Capture_Channel__c,
                                            CORE_Enrollment_Channel__c,
                                            CORE_Insertion_Mode__c,
                                            CORE_Nature__c,
                                            CORE_Origin_Sales__c,
                                            CORE_Origin__c,
                                            CORE_GCLID__c,
                                            Core_Form__c,
                                            CORE_Form_area__c
                                   FROM Opportunity];
        
        //Opp Insertion Date doit être null initialement
        System.assertEquals(null, opportunity.CORE_Insertion_Date__c);
        System.assertEquals(null, opportunity.CORE_Campaign_Medium__c);
        System.assertEquals(null, opportunity.CORE_Campaign_Name__c);
        System.assertEquals(null, opportunity.CORE_Campaign_Source__c);
        System.assertEquals(null, opportunity.CORE_Campaign_Term__c);
        System.assertEquals(null, opportunity.CORE_Capture_Channel__c);
//        System.assertEquals(null, opportunity.CORE_Enrollment_Channel__c);
        System.assertEquals(null, opportunity.CORE_Insertion_Mode__c);
        System.assertEquals(null, opportunity.CORE_Nature__c);
        System.assertEquals(null, opportunity.CORE_Origin_Sales__c);
        System.assertEquals(null, opportunity.CORE_Origin__c);
        System.assertEquals(null, opportunity.CORE_GCLID__c);
        System.assertEquals(null, opportunity.Core_Form__c);
        System.assertEquals(null, opportunity.CORE_Form_area__c);

        Test.startTest();
        //Creer et insert un nouveau POC
        CORE_Point_Of_Contact__c poc = CORE_DataFaker_PointOfContact.getUniquePointOfContact(opportunity.id, account.id, 'POCExternalIDTest');
        Test.stopTest();
        
        //On récupère l'opp mise à jour
        Opportunity updatedOpportunity = [Select    Id,
                                                    CORE_Insertion_Date__c,
                                                    CORE_Campaign_Medium__c,
                                                    CORE_Campaign_Name__c,
                                                    CORE_Campaign_Source__c,
                                                    CORE_Campaign_Term__c,
                                                    CORE_Capture_Channel__c,
                                                    CORE_Enrollment_Channel__c,
                                                    CORE_Insertion_Mode__c,
                                                    CORE_Nature__c,
                                                    CORE_Origin_Sales__c,
                                                    CORE_Origin__c,
                                                    CORE_GCLID__c,
                                                    Core_Form__c,
                                                    CORE_Form_area__c
                                          FROM Opportunity 
                                          WHERE Id = :opportunity.Id];
        
        System.assertEquals(poc.CORE_Insertion_Date__c, UpdatedOpportunity.CORE_Insertion_Date__c);
        System.assertEquals(poc.Campaign_Medium__c, UpdatedOpportunity.CORE_Campaign_Medium__c);
        System.assertEquals(poc.CORE_Campaign_Name__c, UpdatedOpportunity.CORE_Campaign_Name__c);
        System.assertEquals(poc.CORE_Campaign_Source__c, UpdatedOpportunity.CORE_Campaign_Source__c);
        System.assertEquals(poc.CORE_Campaign_Term__c, UpdatedOpportunity.CORE_Campaign_Term__c);
        System.assertEquals(poc.CORE_Capture_Channel__c, UpdatedOpportunity.CORE_Capture_Channel__c);
        System.assertEquals(poc.CORE_Enrollment_Channel__c, UpdatedOpportunity.CORE_Enrollment_Channel__c);
        System.assertEquals(poc.CORE_Insertion_Mode__c, UpdatedOpportunity.CORE_Insertion_Mode__c);
        System.assertEquals(poc.CORE_Nature__c, UpdatedOpportunity.CORE_Nature__c);
        System.assertEquals(poc.CORE_Origin_Sales__c, UpdatedOpportunity.CORE_Origin_Sales__c);
        System.assertEquals(poc.CORE_Origin__c, UpdatedOpportunity.CORE_Origin__c);
        //System.assertEquals(poc.CORE_GCLID__c, UpdatedOpportunity.CORE_GCLID__c);
        //System.assertEquals(poc.Core_Form__c, UpdatedOpportunity.Core_Form__c);
        //System.assertEquals(poc.CORE_Form_area__c, UpdatedOpportunity.CORE_Form_area__c);
    }
    
    @IsTest
    public static void insert_second_POC_shouldnt_update_first_poc_Opportunity() {
        Account account = [Select Id FROM Account LIMIT 1];
        Opportunity opportunity = [Select Id, CORE_Insertion_Date__c FROM Opportunity];
        opportunity.CORE_Insertion_Date__c = System.Now().addDays(-1);
        update opportunity;
        opportunity = [Select   Id,
                                CORE_Insertion_Date__c,
                                CORE_Campaign_Medium__c,
                                CORE_Campaign_Name__c,
                                CORE_Campaign_Source__c,
                                CORE_Campaign_Term__c,
                                CORE_Capture_Channel__c,
                                CORE_Enrollment_Channel__c,
                                CORE_Insertion_Mode__c,
                                CORE_Nature__c,
                                CORE_Origin_Sales__c,
                                CORE_Origin__c,
                                CORE_GCLID__c,
                                Core_Form__c,
                                CORE_Form_area__c
                       FROM Opportunity
                       WHERE Id = :opportunity.Id];
        
        //Opp Insertion Date doit être non-null initialement, le reste null pour les test
        System.assertNotEquals(null, opportunity.CORE_Insertion_Date__c);
        System.assertEquals(null, opportunity.CORE_Campaign_Medium__c);
        System.assertEquals(null, opportunity.CORE_Campaign_Name__c);
        System.assertEquals(null, opportunity.CORE_Campaign_Source__c);
        System.assertEquals(null, opportunity.CORE_Campaign_Term__c);
        System.assertEquals(null, opportunity.CORE_Capture_Channel__c);
//        System.assertEquals(null, opportunity.CORE_Enrollment_Channel__c);
        System.assertEquals(null, opportunity.CORE_Insertion_Mode__c);
        System.assertEquals(null, opportunity.CORE_Nature__c);
        System.assertEquals(null, opportunity.CORE_Origin_Sales__c);
        System.assertEquals(null, opportunity.CORE_Origin__c);
        System.assertEquals(null, opportunity.CORE_GCLID__c);
        System.assertEquals(null, opportunity.Core_Form__c);
        System.assertEquals(null, opportunity.CORE_Form_area__c);

        Test.startTest();
        //Creer et insert un nouveau POC
        CORE_Point_Of_Contact__c poc = CORE_DataFaker_PointOfContact.getUniquePointOfContact(opportunity.id, account.id, 'POCExternalIDTest');
        Test.stopTest();
        
        //On récupère l'opp mise à jour
        Opportunity updatedOpportunity = [Select    Id,
                                                    CORE_Insertion_Date__c,
                                                    CORE_Campaign_Medium__c,
                                                    CORE_Campaign_Name__c,
                                                    CORE_Campaign_Source__c,
                                                    CORE_Campaign_Term__c,
                                                    CORE_Capture_Channel__c,
                                                    CORE_Enrollment_Channel__c,
                                                    CORE_Insertion_Mode__c,
                                                    CORE_Nature__c,
                                                    CORE_Origin_Sales__c,
                                                    CORE_Origin__c,
                                                    CORE_GCLID__c,
                                                    Core_Form__c,
                                                    CORE_Form_area__c
                                          FROM Opportunity 
                                          WHERE Id = :opportunity.Id];
        
        //L'opp ne doit pas avoir été mise à jour avec l'insertion du POC
        System.assertNotEquals(poc.CORE_Insertion_Date__c, UpdatedOpportunity.CORE_Insertion_Date__c);
        System.assertNotEquals(poc.Campaign_Medium__c, UpdatedOpportunity.CORE_Campaign_Medium__c);
        System.assertNotEquals(poc.CORE_Campaign_Name__c, UpdatedOpportunity.CORE_Campaign_Name__c);
        System.assertNotEquals(poc.CORE_Campaign_Source__c, UpdatedOpportunity.CORE_Campaign_Source__c);
        System.assertNotEquals(poc.CORE_Campaign_Term__c, UpdatedOpportunity.CORE_Campaign_Term__c);
        System.assertNotEquals(poc.CORE_Capture_Channel__c, UpdatedOpportunity.CORE_Capture_Channel__c);
        //System.assertNotEquals(poc.CORE_Enrollment_Channel__c, UpdatedOpportunity.CORE_Enrollment_Channel__c);
        //System.assertNotEquals(poc.CORE_Insertion_Mode__c, UpdatedOpportunity.CORE_Insertion_Mode__c);
        //System.assertNotEquals(poc.CORE_Nature__c, UpdatedOpportunity.CORE_Nature__c);
        //System.assertNotEquals(poc.CORE_Origin_Sales__c, UpdatedOpportunity.CORE_Origin_Sales__c);
        //System.assertNotEquals(poc.CORE_Origin__c, UpdatedOpportunity.CORE_Origin__c);
        //System.assertNotEquals(poc.CORE_GCLID__c, UpdatedOpportunity.CORE_GCLID__c);
        //System.assertNotEquals(poc.Core_Form__c, UpdatedOpportunity.Core_Form__c);
        //System.assertNotEquals(poc.CORE_Form_area__c, UpdatedOpportunity.CORE_Form_area__c);
    }

}