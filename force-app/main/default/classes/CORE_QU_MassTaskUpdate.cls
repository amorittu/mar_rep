public without sharing class CORE_QU_MassTaskUpdate implements Queueable {

    public Set<Id> tasksListId;
    public String buId;
    public String academicYear;
    public String lossreason;
    public String subtype;
    public String result;
    public String status;
    public List<String> resultsList;

    public CORE_QU_MassTaskUpdate(Set<Id> tasksListId, String buId, 
    String academicYear,String subtype,String status,String result,String lossreason,
    List<String> resultsList){
        this.tasksListId = tasksListId;
        this.buId = buId;
        this.academicYear = academicYear;
        this.subtype = subtype;
        this.status = status;
        this.result = result;
        this.lossreason = lossreason;
        this.resultsList = resultsList;
    }

    public void execute(QueueableContext context){ 

        CORE_LC_MassTaskUpdate.SettingWrapper setting = CORE_LC_MassTaskUpdate.getOrgSetting();

        try {
        List<Task> updatetasksList = new List<Task>();
        
        List<Opportunity> oppsId= [SELECT Id,CORE_OPP_Business_Unit__c,CORE_Opp_Academic_Year__c FROM Opportunity WHERE CORE_Opp_Business_Unit__c = :buId AND CORE_Opp_Academic_Year__c = :academicYear];
        Integer batchSize = (setting.batchSize != null) ? setting.batchSize :CORE_LC_MassTaskUpdate.defaultBatchSize;
        System.debug('starting opportunities  '+ oppsId);
        String query = 'SELECT Id, WhatId,Status'
                     + ' FROM Task'
                     + ' WHERE Id IN :tasksListId AND WhatId IN :oppsId AND CORE_TECH_CallTaskType__c = :subtype'
                     +' LIMIT :batchSize';
        String querytorem = 'SELECT Id, WhatId,Status'
                     + ' FROM Task'
                     + ' WHERE Id IN :tasksListId AND WhatId IN :oppsId AND CORE_TECH_CallTaskType__c = :subtype';
        Set<Id> remTasksIdSet = new Set<Id>();
        for(Task task : database.query(querytorem)){
            remTasksIdSet.add(task.Id);
        }
        System.debug('query '+query);
        for(Task task : database.query(query)){
            Task tache = new Task( Id = task.Id);
            tache.Status = status;
            if(status == 'CORE_PKL_Completed'){
                tache.Core_Task_Result__c = result;
            }
            if((lossreason != null) && (!String.IsBlank(lossreason)) ){
                tache.CORE_Loss_Reason__c = lossreason;
            }
            updatetasksList.add(tache);
            remTasksIdSet.remove(task.Id);
        }
        List<Database.SaveResult> results = database.update(updatetasksList, false);
        List<Task> taches = [SELECT Id, WhatId,Status,Core_Task_Result__c  FROM Task];
        CORE_MassTaskUpdateHandler resultHandler = new CORE_MassTaskUpdateHandler();
        resultHandler.settingName = CORE_LC_MassTaskUpdate.EMAIL_SETTING_NAME;
        resultHandler.resultsList = resultsList;
        
            if(remTasksIdSet.size() > 0
               && !test.isRunningTest()){//Test class does not support chaining jobs

                System.enqueueJob(new CORE_QU_MassTaskUpdate(remTasksIdSet,buId, academicYear,subtype,status,result,lossreason,resultsList));
            }
            //Job has finished
            else{
                database.update(new CORE_MassTasksUpdate__c(
                   Id = setting.settingId, CORE_JobInProgress__c = false
                ), true);
                resultHandler.sendMail();
            }
        }
        catch(Exception e){
            system.debug('Exception: ' + e.getMessage() + ' => ' + e.getStackTraceString());

            database.update(new CORE_MassTasksUpdate__c(
                Id = setting.settingId, CORE_JobInProgress__c = false
            ), true);

            CORE_MassTaskUpdateHandler resultHandler = new CORE_MassTaskUpdateHandler();
            resultHandler.settingName = CORE_LC_MassTaskUpdate.EMAIL_SETTING_NAME;
            resultHandler.resultsList = resultsList;
            resultHandler.exceptionError = 'APEX Exception Error: ' + e.getMessage() + ' => ' + e.getStackTraceString();

            resultHandler.sendMail();
        }
    }
}