@isTest
private class CORE_Flow_DuplicateOppTimestamps_TEST {

    private static final String INTEREST_EXTERNALID = 'setup';
    private static final String ACADEMIC_YEAR = 'CORE_PKL_2023-2024';

    @testSetup
    static void createData(){
        Id profileId = [SELECT Id FROM Profile WHERE Name = 'system administrator' LIMIT 1].Id;

        User usr = new User(
            Username = 'usertest@testflow.com',
            LastName = 'TEST_USER',
            Email = 'testemail@yopmail.com',
            Alias = 'T',
            CommunityNickname = 'T',
            TimeZoneSidKey = 'Europe/Paris',
            LocaleSidKey = 'fr_FR_EURO',
            EmailEncodingKey = 'ISO-8859-1',
            ProfileId = profileId,
            LanguageLocaleKey = 'fr'
        );
        database.insert(usr, true);

        System.runAs(usr){
            CORE_CatalogHierarchyModel catalogHierarchy = new CORE_DataFaker_CatalogHierarchy(INTEREST_EXTERNALID).catalogHierarchy;
            Account account = CORE_DataFaker_Account.getStudentAccount(INTEREST_EXTERNALID);

            Id oppEnroll_RTId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get(CORE_AOL_Constants.OPP_RECORDTYPE_ENROLLMENT).getRecordTypeId();
            Product2 product = [Select Id, ProductCode from Product2 WHERE ProductCode = 'setup' LIMIT 1];

            Opportunity opp0 = new Opportunity(
                    Name ='test_0',
                    StageName = CORE_AOL_Constants.PKL_OPP_STAGENAME_PROSPECT,
                    CORE_OPP_Sub_Status__c = CORE_AOL_Constants.PKL_OPP_SUBSTATUS_PROSPECTAPPSTARTED,
                    CORE_OPP_Division__c = catalogHierarchy.division.Id,
                    CORE_OPP_Business_Unit__c = catalogHierarchy.businessUnit.Id,
                    CORE_OPP_Study_Area__c = catalogHierarchy.studyArea.Id,
                    CORE_OPP_Curriculum__c = catalogHierarchy.curriculum.Id,
                    CORE_OPP_Level__c = catalogHierarchy.level.Id,
                    CORE_OPP_Intake__c = catalogHierarchy.intake.Id,
                    CORE_OPP_Academic_Year__c = ACADEMIC_YEAR,
                    AccountId = account.Id,
                    CloseDate = Date.Today().addDays(2),
                    OwnerId = UserInfo.getUserId(),
                    RecordTypeId = oppEnroll_RTId,
                    CORE_OPP_OnlineShop_Id__c = '12345',
                    CORE_Main_Product_Interest__c = product.Id
            );

            Opportunity opp1 = new Opportunity(
                    Name ='test_1',
                    StageName = CORE_AOL_Constants.PKL_OPP_STAGENAME_APPLICANT,
                    CORE_OPP_Sub_Status__c = CORE_AOL_Constants.PKL_OPP_SUBSTATUS_APPLICANT_APP_PROGRESSING,
                    CORE_OPP_Division__c = catalogHierarchy.division.Id,
                    CORE_OPP_Business_Unit__c = catalogHierarchy.businessUnit.Id,
                    CORE_OPP_Study_Area__c = catalogHierarchy.studyArea.Id,
                    CORE_OPP_Curriculum__c = catalogHierarchy.curriculum.Id,
                    CORE_OPP_Level__c = catalogHierarchy.level.Id,
                    CORE_OPP_Intake__c = catalogHierarchy.intake.Id,
                    CORE_OPP_Academic_Year__c = ACADEMIC_YEAR,
                    AccountId = account.Id,
                    CloseDate = Date.Today().addDays(2),
                    OwnerId = UserInfo.getUserId(),
                    RecordTypeId = oppEnroll_RTId,
                    CORE_OPP_OnlineShop_Id__c = '123456',
                    CORE_Main_Product_Interest__c = product.Id
            );

            database.insert(new List<Opportunity> { opp0, opp1 }, true);

            CORE_OpportunityTimestampProcess timestampProcess = new CORE_OpportunityTimestampProcess();

            CORE_Sub_Status_Timestamp_History__c subStatusTimeStamp = timestampProcess.initSubStatusTimestampRecord(opp1);
        }

    }

    @IsTest
    static void duplicateSubStatusTimestamps_TEST(){
        User usr = [SELECT Id FROM User WHERE Username = 'usertest@testflow.com' LIMIT 1];

        System.runAs(usr){

            Opportunity opp0 = null;
            Opportunity opp1 = null;

            for(Opportunity opp : [SELECT Id, StageName FROM Opportunity]){
                system.debug('opp: ' + opp);
                if(opp.StageName == CORE_AOL_Constants.PKL_OPP_STAGENAME_PROSPECT){
                    opp1 = opp;
                }
                else{
                    opp0 = opp;
                }
            }

            List<CORE_Flow_DuplicateOppTimestamps.OppTimestampParameters> params = new List<CORE_Flow_DuplicateOppTimestamps.OppTimestampParameters>();

            CORE_Flow_DuplicateOppTimestamps.OppTimestampParameters param = new CORE_Flow_DuplicateOppTimestamps.OppTimestampParameters();
            param.oldOppId = opp1.Id;
            param.newOppId = opp0.Id;
            params.add(param);

            CORE_Flow_DuplicateOppTimestamps.duplicateSubStatusTimestamps(params);
        }
    }
}