@isTest
public class IT_SPcoaBookingQueueableTest {
	
    @TestSetup
    public static void makeData() {
        CORE_DataFaker_CatalogHierarchy catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test1');

        Account account = CORE_DataFaker_Account.getStudentAccount('test1');
        Opportunity opportunity = CORE_DataFaker_Opportunity.getOpportunity(
                catalogHierarchy.division.Id,
                catalogHierarchy.businessUnit.Id,
                account.Id,
                'Applicant',
                'Applicant - Application Submitted / New'
        );

        opportunity.Pricebook2Id = Test.getStandardPricebookId();
        opportunity.CORE_OPP_Intake__c = catalogHierarchy.intake.Id;
        opportunity.CORE_OPP_Level__c = catalogHierarchy.level.Id;
        opportunity.CORE_OPP_Curriculum__c = catalogHierarchy.curriculum.Id;
        opportunity.CORE_OPP_Study_Area__c = catalogHierarchy.studyArea.Id;
        opportunity.CORE_Capture_Channel__c = 'CORE_PKL_Web_form';

        update opportunity;

        Opportunity opportunity2 = CORE_DataFaker_Opportunity.getOpportunity(
                catalogHierarchy.division.Id,
                catalogHierarchy.businessUnit.Id,
                account.Id,
                'Applicant',
                'Applicant - Application Submitted / New'
        );

        IT_ExtendedInfo__c extendedInfo = IT_DataFaker_Objects.createExtendedInfo(account.id, opportunity.IT_Brand__c, false);
        extendedInfo.IT_LastContactInformationDate__c = Datetime.newInstance(2022, 1, 2, 0, 0 , 0);
        extendedInfo.IT_LastContactAdmissionDate__c = Datetime.newInstance(2022, 1, 1, 0, 0 , 0);
        System.debug('extendedInfo.IT_LastContactInformationDate__c:: ' + extendedInfo.IT_LastContactInformationDate__c);
        System.debug('extendedInfo.IT_LastContactAdmissionDate__c:: ' + extendedInfo.IT_LastContactAdmissionDate__c);
        insert extendedInfo;

        System.debug('extendedInfo inserted:: ' + [SELECT Id, IT_LastContactInformationDate__c, IT_LastContactAdmissionDate__c FROM IT_ExtendedInfo__c]);

        IT_OpportunityAssignmentRule__c oar = new IT_OpportunityAssignmentRule__c();
        oar.IT_BusinessUnit__c = opportunity.CORE_OPP_Business_Unit__c;
        oar.IT_Country__c = opportunity.account.PersonMailingCountryCode;
        oar.IT_MainCountry__c = opportunity.Account.PersonMailingCountry;
        oar.IT_CaptureChannel__c = opportunity.CORE_Capture_Channel__c;
        insert oar;
        
 		IT_OpportunityAssignmentRule__c oar2 = new IT_OpportunityAssignmentRule__c();
        oar2.IT_BusinessUnit__c = opportunity2.CORE_OPP_Business_Unit__c;
        oar2.IT_Country__c = opportunity2.account.PersonMailingCountryCode;
        oar2.IT_MainCountry__c = opportunity2.Account.PersonMailingCountry;
        oar2.IT_CaptureChannel__c = opportunity2.CORE_Capture_Channel__c;
        insert oar2;
            
        IT_CountryRelevance__c cr = new IT_CountryRelevance__c();
        cr.IT_BusinessUnit__c = opportunity.CORE_OPP_Business_Unit__c;
        cr.IT_CountryISO__c = opportunity.Account.PersonMailingCountry;
        insert cr;
        
        ServiceAppointment sa = new ServiceAppointment();
        sa.Status = 'None';
        sa.CORE_Business_Unit__c = catalogHierarchy.businessUnit.Id;
        sa.CORE_Division__c = catalogHierarchy.division.Id;
        sa.ParentRecordId =  opportunity2.Id;
        sa.CORE_TECH_Relatd_Opportunity__c = opportunity2.Id;
        sa.SchedStartTime = System.now();
		insert sa;
    }
    
    @IsTest
    static void testBehavior(){
        ServiceAppointment sa = [SELECT Id FROM ServiceAppointment LIMIT 1];
        Set<Id> servAppntIDS = new Set<Id>{sa.Id};

        Test.startTest();
        	System.enqueueJob(new IT_SPcoaBookingQueueable(servAppntIDS));
        Test.stopTest();
    }
    
    /*@IsTest
    static void testBehavior2(){
        CORE_DataFaker_CatalogHierarchy catalogHierarchy2 = new CORE_DataFaker_CatalogHierarchy('test2');
        Opportunity opportunity3 = CORE_DataFaker_Opportunity.getOpportunity(
                catalogHierarchy2.division.Id,
                catalogHierarchy2.businessUnit.Id,
                [SELECT Id FROM Account LIMIT 1].Id,
                'Applicant',
                'Applicant - Application Submitted / New'
        );

        opportunity3.CORE_OPP_Intake__c = catalogHierarchy2.intake.Id;
        opportunity3.CORE_OPP_Level__c = catalogHierarchy2.level.Id;
        opportunity3.CORE_OPP_Curriculum__c = catalogHierarchy2.curriculum.Id;
        opportunity3.CORE_OPP_Study_Area__c = catalogHierarchy2.studyArea.Id;
        opportunity3.CORE_Capture_Channel__c = 'CORE_PKL_Web_form';
        update opportunity3;

        CORE_Business_Unit__c bu = new CORE_Business_Unit__c(Id = catalogHierarchy2.businessUnit.Id);
        bu.CORE_Brand__c = 'CORE_PKL_Istituto_Marangoni';
        update bu;

        IT_OpportunityAssignmentRule__c oar = new IT_OpportunityAssignmentRule__c();
        oar.IT_COANotApplied__c = true;
        oar.IT_BusinessUnit__c = opportunity3.CORE_OPP_Business_Unit__c;
        oar.IT_MainCountry__c = opportunity3.Account.PersonMailingCountry;
        oar.IT_CaptureChannel__c = opportunity3.CORE_Capture_Channel__c;
        oar.IT_InformationOfficer__c = UserInfo.getUserId();
        oar.IT_COANotApplied__c = true;
        insert oar;
	
        
        
        Map<Opportunity, IT_OpportunityAssignmentRule__c> optyOARMap = new Map<Opportunity, IT_OpportunityAssignmentRule__c>{opportunity3 => oar};
        List<Opportunity> optyList = new List<Opportunity>{opportunity3};
        //IT_QueueAssignOwnerStartFlow.checkCOAnotApplied(optyList, optyOARMap);
		
        ServiceAppointment sa = new ServiceAppointment();
        sa.Status = 'None';
        sa.CORE_Business_Unit__c = catalogHierarchy2.businessUnit.Id;
        sa.CORE_Division__c = catalogHierarchy2.division.Id;
        sa.ParentRecordId =  opportunity3.Id;
        sa.CORE_TECH_Relatd_Opportunity__c = opportunity3.Id;
        sa.SchedStartTime = System.now();
		insert sa;
        
        Set<Id> servAppntIDS = new Set<Id>{sa.Id};

        Test.startTest();
        System.enqueueJob(new IT_SPcoaBookingQueueable(servAppntIDS));
        Test.stopTest();
    }*/
}