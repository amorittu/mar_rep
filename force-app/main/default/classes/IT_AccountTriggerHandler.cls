/**
 * Created by ftrapani on 11/11/2021.
 */

public without sharing class IT_AccountTriggerHandler extends TriggerHandler {
    public static final String CLASS_NAME = IT_AccountTriggerHandler.class.getName();
    public static Boolean fireSpamTasksChangeQueue = true;
    public static Boolean fireDivisionDeleteCheck = true;
    public static Boolean fireMasterRecordIdcheck = true; 

   
    public void beforeInsert(List<Account> newRecordsList) {
		setCountryFields(newRecordsList, null);
    }
    public void beforeUpdate(List<Account> newRecordsList,Map<Id,Account> oldRecordsMap) {
		setCountryFields(newRecordsList, oldRecordsMap);
    }

    /*********************************************************************************************************
     * @name			beforeDelete
     * @author			Andrea Morittu <amorittu@atlantic-technologies.com>
     * @created			03 / 08 / 2022
     * @description		TK-000485
     * @param			Trigger.Old
     * @return			NA
    **********************************************************************************************************/
    public void beforeDelete(List<Account> oldRecordsList) {
		System.debug(CLASS_NAME + ' - beforeDelete fired!' );
        if (fireDivisionDeleteCheck == true) {
            // checkIfAccountHasMultipleDivision(oldRecordsList);
        }
        System.debug(CLASS_NAME + ' - beforeDelete finished!' );
    }

    public void afterInsert(List<Account> newRecordsList) {

    }

    public void afterUpdate(List<Account> newRecordsList,Map<Id,Account> oldRecordsMap) {
        upsertExtendedInfo(newRecordsList,oldRecordsMap);
        
        // TK-000270 - 15/07/2022 START
        if (fireSpamTasksChangeQueue) {
            setSpamTasksToProperQueue(newRecordsList,oldRecordsMap);
        }
        // TK-000270 - 15/07/2022 END
    }

    
    /*********************************************************************************************************
     * @name			setSpamTasksToProperQueue
     * @author			Andrea Morittu <amorittu@atlantic-technologies.com>
     * @created			15 / 07 / 2022
     * @description		TK-000270 - In case of spam, other or test, put tasks to a particular queue
     * @param			
     * @return			
    **********************************************************************************************************/
    public void setSpamTasksToProperQueue(List<Account> newRecordsList, Map<Id,Account> oldRecordsMap) {
        System.debug(CLASS_NAME + ' - setSpamTasksToProperQueue Fired! ');
        String[] spamAccountsIds = new String[]{};
        for (Account currentAccount : newRecordsList) {
            Account oldAccount = oldRecordsMap.get(currentAccount.Id);
            if (oldAccount.Type != currentAccount.Type && String.isNotBlank(currentAccount.Type) && (currentAccount.Type == 'Spam' || currentAccount.Type == 'Test' || currentAccount.Type == 'Other' )  ) {
                spamAccountsIds.add(currentAccount.Id);
            }
        }
        System.debug(CLASS_NAME + ' - spamAccountsIds is: ' + spamAccountsIds);
        
        if ( ! spamAccountsIds.isEmpty() ) {
            Task[] tasksToReassign = [SELECT Id, OwnerId FROM Task WHERE AccountId IN: spamAccountsIds ];
            System.debug(CLASS_NAME + ' - tasksToReassign: ' + tasksToReassign);
            if ( ! tasksToReassign.isEmpty() ) {
                Group[] spamGroup = [SELECT Id FROM Group WHERE Type = 'Queue' AND DeveloperName = 'NotToBeQueued' LIMIT 1];
                
                String spamGroupid = String.valueOf(spamGroup.get(0).Id) ;
                System.debug(CLASS_NAME + ' - spamGroup ' + spamGroup);
                Task[] spamTasksToUpdate = new Task[]{};

                for (Task spamTask : tasksToReassign) {
                    spamTask.OwnerId = spamGroupid;
                    spamTasksToUpdate.add(spamTask);
                }
                System.debug(CLASS_NAME + ' - spamTasksToUpdate ' + spamTasksToUpdate);
                try {
                    update spamTasksToUpdate;
                    fireSpamTasksChangeQueue = false;
                } catch(Exception exc) {
                    System.debug(CLASS_NAME + ' - An Exception has occured : ' + exc.getMessage() + ' - at line : ' + exc.getLineNumber() );
                    fireSpamTasksChangeQueue = false;
                }
            }
        }
    }
    
    public void setCountryFields(List<Account> newAccounts, Map<Id,Account> oldAccounts) {
        System.debug('IT_AccountTriggerHandler::setCountryFields::start');
        List<Account> accsToUpdate = new List<Account>();
        Boolean isInsert = (oldAccounts == null);
        for(Account acct: newAccounts) {
            //TODO: update with billingcountrycode and personmailingcode when available
            if(isInsert || (acct.PersonMailingCountryCode != oldAccounts.get(acct.Id).PersonMailingCountryCode && String.isNotBlank(acct.PersonMailingCountryCode)) ||
                    (acct.BillingCountryCode != oldAccounts.get(acct.Id).BillingCountryCode && String.isNotBlank(acct.BillingCountryCode))) {
                accsToUpdate.add(acct);
            }
        }
        System.debug('acsToUpdate: '+accsToUpdate);
        if (!accsToUpdate.isEmpty()) {
            try {
                //max 300 lines
                List<IT_Country_Language_Mapping__mdt> countryMappings = [
                        SELECT Id, IT_MacroArea__c, IT_CountryISO__c
                        FROM IT_Country_Language_Mapping__mdt
                        WHERE IT_CountryISO__c != NULL AND
                        IT_MacroArea__c != NULL 
                ];

                for (Account acct : accsToUpdate) {
                    for (IT_Country_Language_Mapping__mdt country : countryMappings) {
                        if (String.isNotBlank(acct.BillingCountryCode) && acct.BillingCountryCode == country.IT_CountryISO__c) {
                            acct.IT_BillingMacroArea__c = country.IT_MacroArea__c;
                            System.debug('IT_AccountTriggerHandler::setCountryFields::setting billing macro area');
                        }
                        if (String.isNotBlank(acct.PersonMailingCountryCode) && acct.PersonMailingCountryCode == country.IT_CountryISO__c) {
                            acct.IT_MacroArea__c = country.IT_MacroArea__c;
                            System.debug('IT_AccountTriggerHandler::setCountryFields::setting macro area');
                        }
                    }
                }
            } catch (Exception exc) {
                System.debug('Error setting macroarea on Accounts: ' + exc);
            }
        }
        System.debug('IT_AccountTriggerHandler::setCountryFields::end');
    }

    public void upsertExtendedInfo(List<Account> newRecordsList,Map<Id,Account> oldRecordsMap){
        List<Account> accountToProcess =  new List<Account>();
        for(Account accountRecord : newRecordsList){
            Account oldRecord = oldRecordsMap!=null? oldRecordsMap.get(accountRecord.Id) : null;
            if(oldRecord!= null && oldRecord.CORE_Language__pc != accountRecord.CORE_Language__pc){
                accountToProcess.add(accountRecord);
            }
        }

        List<Account> accList = [SELECT Id, CORE_Language__pc , CORE_Language_website__pc, (SELECT Id, CORE_OPP_Business_Unit__r.CORE_Brand__c FROM Opportunities ORDER BY CreatedDate), (SELECT Id, Name, IT_Account__c, IT_Brand__c, IT_NavigationLanguage__c FROM Extended_Informations__r) FROM Account WHERE Id IN :accountToProcess];

        List<IT_ExtendedInfo__c> extInfoUpdateList = new List<IT_ExtendedInfo__c>();
        for(Account accountRecord : accList ){
            List<Opportunity> optyList = accountRecord.Opportunities;
            Opportunity lastOpty = null;
            if(optyList.size()>0){
                lastOpty = optyList[0];
            }
            List<IT_ExtendedInfo__c> extInfoList = accountRecord!=null ? accountRecord.Extended_Informations__r : null;
            Map<String,IT_ExtendedInfo__c> brandExtInfoMap = new Map<String,IT_ExtendedInfo__c>();
            if(extInfoList.size()>0){
                for(IT_ExtendedInfo__c extInfo : extInfoList){
                    brandExtInfoMap.put(extInfo.IT_Brand__c,extInfo);
                } 

            }
            IT_ExtendedInfo__c extInfoFound = brandExtInfoMap!=null && brandExtInfoMap.size()>0 && lastOpty!=null ? brandExtInfoMap.get(lastOpty.CORE_OPP_Business_Unit__r.CORE_Brand__c) : null;
            if(extInfoFound!=null){
                extInfoFound.IT_NavigationLanguage__c = accountRecord.CORE_Language_website__pc!=null?accountRecord.CORE_Language_website__pc : extInfoFound.IT_NavigationLanguage__c ;
                extInfoUpdateList.add(extInfoFound);
            }

        }

        if(extInfoUpdateList.size()>0){

            //update extInfoUpdateList;
            List<Database.UpsertResult> results = Database.upsert( extInfoUpdateList, false );

            for(Database.UpsertResult resultRecord : results) {
                if(!resultRecord.isSuccess()){
                    List<Database.Error> errors = resultRecord.getErrors();
                    System.debug('IT_AccountTriggerHandler upsert error: ' + resultRecord + ', errors ' + errors);
                }
            }
        }
    }

    public void afterDelete(List<Account> oldRecordsList) {
		System.debug(CLASS_NAME + ' - afterDelete fired!' );
        if (fireMasterRecordIdcheck == true) {
            allowDeletitionOfAccount(oldRecordsList);
        }
        System.debug(CLASS_NAME + ' - afterDelete finished!' );
    }

    public void allowDeletitionOfAccount(List<Account> oldRecordsList) {
        System.debug(CLASS_NAME + ' - checkMasterRecordId fired!' );

        Boolean systemAdminAllowedToSkipMergeChecks = doesTheUserHaveTheCustomPermission('IT_AllowMergeAccount');
        
        Map<String, Account[]> masterAccountId_duplicatedRecordsMap = new Map<String, Account[]>(); 

        String[] childrenRecords = new String[]{};
        Map<String, String[]> master_deletedRecords = new Map<String, String[]>();
        for (Account acc : oldRecordsList) {
            if ( String.isNotBlank(acc.MasterRecordId)  ) {
                if (masterAccountId_duplicatedRecordsMap.containsKey(acc.MasterRecordId) ) {
                    masterAccountId_duplicatedRecordsMap.get(acc.MasterRecordId).add(acc);
                } else {
                    masterAccountId_duplicatedRecordsMap.put(acc.MasterRecordId, new Account[] {acc} );
                    childrenRecords.add(acc.Id);
                }
            }
        }
        System.debug(CLASS_NAME + ' - childrenRecords: ' + JSON.serialize(childrenRecords) );
        System.debug(CLASS_NAME + ' - masterAccountId_duplicatedRecordsMap: ' + JSON.serialize(masterAccountId_duplicatedRecordsMap)  );
        Map<String, String[]> master_divisions = new Map<String, String[]>();
        if ( ! masterAccountId_duplicatedRecordsMap.isEmpty() ) {
            Opportunity[] opportunityList = [
                SELECT Id, AccountId, Account.MasterRecordId,
                    CORE_OPP_Division__c, 
                    CORE_OPP_Division__r.CORE_Division_ExternalId__c 
                FROM Opportunity 
                WHERE (
                    AccountId IN: childrenRecords 
                        OR 
                    AccountId IN: masterAccountId_duplicatedRecordsMap.keySet() 
                ) 
                // ALL ROWS
            ];
            for (Opportunity opportunity : opportunityList ) {
                if ( String.isBlank(opportunity.Account.MasterRecordId) ) {
                    if (master_divisions.containsKey(opportunity.AccountId) ) {
                        if(!(master_divisions.get(opportunity.AccountId).contains(opportunity.CORE_OPP_Division__r.CORE_Division_ExternalId__c)) ) {
                            master_divisions.get(opportunity.AccountId).add(opportunity.CORE_OPP_Division__r.CORE_Division_ExternalId__c);
                        }
                    } else {
                        master_divisions.put(opportunity.AccountId, new String[] {opportunity.CORE_OPP_Division__r.CORE_Division_ExternalId__c});
                    }
                } else {
                    if (master_divisions.containsKey(opportunity.Account.MasterRecordId)) {
                        if (! master_divisions.get(opportunity.Account.MasterRecordId).contains(opportunity.CORE_OPP_Division__r.CORE_Division_ExternalId__c) ) {
                            master_divisions.get(opportunity.Account.MasterRecordId).add(opportunity.CORE_OPP_Division__r.CORE_Division_ExternalId__c);
                        }
                    } else { 
                        master_divisions.put(opportunity.Account.MasterRecordId,  new String[] {opportunity.CORE_OPP_Division__r.CORE_Division_ExternalId__c});
                    }
                }
            }

            System.debug(CLASS_NAME + ' - master_divisions : ' + master_divisions);
            Set<String> survivorsAccountIds = new Set<String>();
            for (Account acc : oldRecordsList ) {
                if (master_divisions.containsKey(acc.MasterRecordId) && master_divisions.get(acc.MasterRecordId).size() > 1 ) {
                    survivorsAccountIds.clear();
                    if (! systemAdminAllowedToSkipMergeChecks) {
                        acc.addError(System.Label.IT_DeletionAccountMultipleDivisionError);
                    }
                } else {
                    survivorsAccountIds.add(acc.MasterRecordId);
                }
            }
            if ( ! survivorsAccountIds.isEmpty() && ! systemAdminAllowedToSkipMergeChecks ) {
                IT_ExtendedInfo__c[] extendedInfo = [SELECT Id FROM IT_ExtendedInfo__c WHERE IT_Account__c IN: survivorsAccountIds];
                delete extendedInfo;
                Database.executeBatch(
                    new IT_BatchExtendedInfoOpportunityCreated(true, new List<String>(survivorsAccountIds) )
                );
                fireMasterRecordIdcheck = false;
            }
        }
    }

    public static Boolean doesTheUserHaveTheCustomPermission(String inputPermissionApiName) {
        return FeatureManagement.checkPermission(inputPermissionApiName);
    }
}