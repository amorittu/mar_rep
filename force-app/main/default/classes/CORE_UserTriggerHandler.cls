/**
 * @author Valentin Pitel
 * @create date 2022-04-13 10:50:03
 * @modify date 2022-04-13 10:50:03
 * @desc handler used to call Twilio APIs
 */
public class CORE_UserTriggerHandler extends CORE_TriggerHandler {
    @TestVisible
    private Map<Id, User> oldMap;
    @TestVisible
    private List<User> newRecords;

    @TestVisible
    private OmniChannelConfig__mdt omniConfig;

    public CORE_UserTriggerHandler() {
        this.oldMap = Trigger.oldMap != null ? (Map<Id, User>) Trigger.oldMap : new Map<Id, User>();
        this.newRecords = Trigger.new != null ? (List<User>) Trigger.new : new List<User>();

        if(Test.isRunningTest()){
            this.omniConfig = new OmniChannelConfig__mdt(
                MasterLabel = 'UNIT_TEST',
                DeveloperName = 'UNIT_TEST',
                CORE_Active_User_Deprovisioning__c = false,
                CORE_Active_User_Provisioning__c = false
            );
        } else {
            this.omniConfig = OmniChannelConfig__mdt.getInstance('Default_Value');
        }
    }

    public override void afterInsert() {
        System.debug('@CORE_UserTriggerHandler > afterInsert START');

        //Twilio process
        if(this.omniConfig.CORE_Active_User_Provisioning__c){
            userProvisioningProcess(this.newRecords, null);
        }

        System.debug('@CORE_UserTriggerHandler > afterInsert END');
    }

    public override void afterUpdate() {
        System.debug('@CORE_UserTriggerHandler > afterUpdate START');

        //Twilio process
        if(this.omniConfig.CORE_Active_User_Provisioning__c || this.omniConfig.CORE_Active_User_Deprovisioning__c){
            userProvisioningProcess(this.newRecords, this.oldMap);
        }
        System.debug('@CORE_UserTriggerHandler > afterUpdate END');
    }

    private void userProvisioningProcess(List<User> newUsers, Map<Id, User> oldUsers){
        List<User> usersAvailableForTwilio = getAvailableUsersForTwilio(newUsers,oldUsers);
        if(!usersAvailableForTwilio.isEmpty()){
            System.debug('@CORE_UserTriggerHandler > CORE_FLEX_User.updateWorker START');
            CORE_FLEX_User.executeWorkerProcess(this.newRecords, this.oldMap, this.omniConfig);
        }
    }

    private List<User> getAvailableUsersForTwilio(List<User> newUsers, Map<Id, User> oldUsers){
        List<User> usersAvailableForTwilio = new List<User>();

        // User creation
        if(oldUsers == null){
            if(this.omniConfig.CORE_Active_User_Provisioning__c){
                for(User newUser : newUsers){
                    if(newUser.IsActive && newUser.CallCenterId != null){
                        usersAvailableForTwilio.add(newUser);
                    }
                }
            }
        } else {
            //User modification
            for(User newUser : newUsers){
                User oldUser = oldUsers.get(newUser.Id);
                System.debug('newUser.IsActive = ' + newUser.IsActive);
                System.debug('oldUser.IsActive = ' + oldUser.IsActive);
                System.debug('newUser.CallCenterId = ' + newUser.CallCenterId);
                System.debug('oldUser.CallCenterId = ' + oldUser.CallCenterId);
                System.debug('newUser.Email = ' + newUser.Email);
                System.debug('oldUser.Email = ' + oldUser.Email);
                if(this.omniConfig.CORE_Active_User_Deprovisioning__c && (
                    (!newUser.IsActive && newUser.IsActive != oldUser.IsActive) || 
                    (newUser.CallCenterId == null && newUser.CallCenterId != oldUser.CallCenterId)
                    )
                ){
                    usersAvailableForTwilio.add(newUser);
                } else if(this.omniConfig.CORE_Active_User_Provisioning__c && (
                    (newUser.Email != oldUser.Email) ||
                    (newUser.IsActive && newUser.IsActive != oldUser.IsActive) ||
                    (newUser.CallCenterId != null && newUser.CallCenterId != oldUser.CallCenterId)
                    )
                ){
                    usersAvailableForTwilio.add(newUser);
                }
            }
        }

        return usersAvailableForTwilio;
    }
}