global class IT_CheckCourseStartDateInvocable {
    @InvocableMethod
    global static List<FlowOutputs> getNextBH(List<FlowInputs> request){
        CORE_Intake__c incomingIntake           = request[0].intake;

        CORE_Business_Unit__c businessUnit = [SELECT Id, IT_AcademicYearStartMonth__c FROM CORE_Business_Unit__c WHERE Id =: request[0].businessUnitId ]; 

        

        Date finalDate = IT_Utility.courseStartDate(request[0].intake, businessUnit.IT_AcademicYearStartMonth__c );

        FlowOutputs flOutput= new FlowOutputs();
        flOutput.dateToReturn = finalDate ;

        List<FlowOutputs> returnList = new List<FlowOutputs>();
        returnList.add(flOutput);
        return returnList;
    }

    global class FlowInputs{

        // @InvocableVariable (required=false)
        // global Opportunity opportunity;

        @InvocableVariable (required=true)
        global CORE_Intake__c intake;

        @InvocableVariable
        global String businessUnitId;

    }

    global class FlowOutputs{

        @InvocableVariable
        global Date dateToReturn;

    }
}