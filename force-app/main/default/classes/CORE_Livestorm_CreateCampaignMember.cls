public class CORE_Livestorm_CreateCampaignMember implements Database.Batchable<SObject>, Database.AllowsCallouts{
    Set<Id> campaignMemberIds;

    public CORE_Livestorm_CreateCampaignMember(Set<Id> c){
        System.debug('CORE_Livestorm_CreateCampaignMember : START');
        campaignMemberIds = c;
    }

    public Database.QueryLocator start(Database.BatchableContext bc){
        return Database.getQueryLocator([SELECT Id, CampaignId, Email, FirstName, LastName
        FROM CampaignMember where Id in :this.campaignMemberIds]);
    }

    public void execute(Database.BatchableContext bc, List<CampaignMember> campaignMembers){
        System.debug('CORE_Livestorm_CreateCampaignMember.execute() : START');

        List<Id> campaignIds = new List<Id>();

        for(CampaignMember campaignMember : campaignMembers){
            campaignIds.add(campaignMember.CampaignId);
        }

        List<Campaign> campaigns = [SELECT Id, CORE_Livestorm_Session_Id__c, CORE_Business_Unit__c FROM CAMPAIGN WHERE Id in :campaignIds];
        Map<Id,Campaign> orderCampaign = new Map<Id,Campaign> (campaigns);

        Map<Id,CORE_Business_Unit__c> orderBusinessUnit = CORE_Livestorm_Helper.getBuFromCampaign(campaigns);

        List<CampaignMember> campaignMemberToUpdates = new List<CampaignMember>();

        for(CampaignMember campaignMember : campaignMembers){
            System.debug('Insert new element in livestorm for : ' + campaignMember.CampaignId);

            Campaign relatedCampaign = orderCampaign.get(campaignMember.CampaignId);
            CORE_Livestorm__mdt livestormMetadata = CORE_Livestorm_Helper.GetMetadata(orderBusinessUnit, relatedCampaign);

            if (livestormMetadata.CORE_Is_Livestorm_Active__c){
                try{
                    CORE_Livestorm_HttpCreationResult newMember = registerSomeoneToAnSession(campaignMember, getSessionId(campaigns, campaignMember.CampaignId), livestormMetadata);
    
                    if (!newMember.error){
                        System.debug('Event : ' + newMember.id + ' created with link : '  + newMember.link);
                        campaignMember.CORE_Livestorm_People_Id__c = newMember.id;
                        campaignMember.CORE_Livestorm_People_Link__c = newMember.link;

                        campaignMemberToUpdates.add(campaignMember);        
                    } else{
                        System.debug('An error occured during people creation');
                    }
                } catch (Exception e) {
                    System.debug('The following exception has occurred during people creation : ' + e.getMessage());
                }
            }
        }
        List<Database.saveResult> results = Database.update(campaignMemberToUpdates, false);
        
        for(Database.saveResult result : results){
            if(!result.isSuccess()){
            System.debug(JSON.serialize(result.getErrors()));
			}
        }

        System.debug('CORE_Livestorm_CreateCampaign.execute() : END');
    }

    public String getSessionId(List<Campaign> campaigns, String campaignId){
        String toReturn = null;

        for (Campaign campaign : campaigns){
            if (campaign.Id == campaignId){
                toReturn = campaign.CORE_Livestorm_Session_Id__c;
                break;
            }
        }

        return toReturn;
    }

    public CORE_Livestorm_HttpCreationResult registerSomeoneToAnSession(CampaignMember campaignMember, String sessionId, CORE_Livestorm__mdt livestormMetadata){
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        
        string fullEndPoint = livestormMetadata.CORE_Default_End_Point__c + livestormMetadata.CORE_Create_People_End_Point__c;
        fullEndPoint = String.format(fullEndPoint, new List<String>{sessionId});

        request.setMethod('POST');
        request.setEndpoint(fullEndPoint);
        request.setHeader('Accept', 'application/vnd.api+json');
        request.setHeader('Authorization', livestormMetadata.CORE_Authorization__c);
        request.setHeader('Content-Type', 'application/vnd.api+json');

        request.setBody('{"data": {"type": "people","attributes": {"fields": [{"id": "email","value": "' + campaignMember.Email + '"},{"id": "first_name","value": "' + campaignMember.FirstName + '"},{"id": "last_name","value": "' + campaignMember.LastName + '"}]}}}');
        
        Long dt1 = DateTime.now().getTime();
        DateTime dt1Date = DateTime.now();

        HttpResponse response = http.send(request);

        long dt2 = DateTime.now().getTime(); 
        DateTime dt2Date = DateTime.now();
        System.debug('http execution time = ' + (dt2-dt1) + ' ms');

        CORE_Livestorm_HttpCreationResult createMemberResult = new CORE_Livestorm_HttpCreationResult();

        if(response.getStatusCode() != 201) {
            System.debug('Event :The status code returned was not expected: ' + response.getStatusCode() + ' ' + response.getStatus());
            createMemberResult.error = true;
        } else {
            System.debug(response.getBody());
            Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());
            Map<String, Object> data = (Map<String, Object>) results.get('data');
            Map<String, Object> attributes = (Map<String, Object>) data.get('attributes');
            Map<String, Object> registrantDetail = (Map<String, Object>) attributes.get('registrant_detail');

            createMemberResult.error = false;
            createMemberResult.link = (String)registrantDetail.get('connection_link');
            createMemberResult.id = (String) data.get('id');
        }

        return createMemberResult;
    }

    public void finish(Database.BatchableContext bc){
        System.debug('CORE_Livestorm_CreateCampaignMember END.');
    }
}