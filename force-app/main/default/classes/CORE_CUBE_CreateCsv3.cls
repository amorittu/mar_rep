public with sharing class CORE_CUBE_CreateCsv3 implements Schedulable{
    
    public void execute(SchedulableContext ctx) {
        DateTime myLastSuccess= CORE_CUBE_CreateCsv.getLastSuccesDate();

        DateTime dtBeforeFileGeneration1 = System.now();
        String intake = CORE_CUBE_CreateCsv.getListIntake(myLastSuccess);
        DateTime dtAfterFileGeneration1 = System.now();

        DateTime dtBeforeFileGeneration2 = System.now();
        String pick = CORE_CUBE_CreateCsv.createAllPicklist();
        DateTime dtAfterFileGeneration2 = System.now();

        DateTime dtBeforeFileGeneration3 = System.now();
        String del = CORE_CUBE_CreateCsv.getListDeleted(myLastSuccess);
        DateTime dtAfterFileGeneration3 = System.now();

        CORE_CUBE_CreateCsv.sendToEndpoint('Intake',intake, dtBeforeFileGeneration1, dtAfterFileGeneration1);
        CORE_CUBE_CreateCsv.sendToEndpoint('Picklist',pick, dtBeforeFileGeneration2, dtAfterFileGeneration2);
        CORE_CUBE_CreateCsv.sendToEndpoint('Deleted_Objects',del, dtBeforeFileGeneration3, dtAfterFileGeneration3);
    }
}