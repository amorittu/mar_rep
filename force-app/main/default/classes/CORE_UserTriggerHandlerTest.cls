@IsTest
public class CORE_UserTriggerHandlerTest {
    private static final String USER_UNIQUE_KEY = 'testSetupCORE_UserTriggerHandlerTest';       
    
    @TestSetup
    static void makeData(){
        User userTest = CORE_DataFaker_User.getUser(USER_UNIQUE_KEY);
        userTest.CallCenterId = [Select Id From CallCenter limit 1].Id;
        update userTest;

        CORE_FLEX_Settings__c cs = new CORE_FLEX_Settings__c();
		Blob cryptoKey = Crypto.generateAesKey(256);
		Blob data = Blob.valueOf('password12');
		Blob encryptedData = Crypto.encryptWithManagedIV('AES256', cryptoKey, data);

		cs.UrlFlex__c='https://project.twil.io12';
		cs.TokenFlex__c=EncodingUtil.base64Encode(encryptedData);
		cs.KeySecret__c= EncodingUtil.base64Encode(cryptoKey);
        cs.AccountSid__c='ACxx12';
        
		insert cs;
    }
    
    @IsTest
    public static void constructor_should_init_metadata() {
        CORE_UserTriggerHandler handler = new CORE_UserTriggerHandler();
        System.assertNotEquals(null, handler.omniConfig);
        System.assertNotEquals(null, handler.oldMap);
        System.assertNotEquals(null, handler.newRecords);
    }

    @IsTest
    public static void afterInsert_should_not_call_userProvisioningProcess_when_metada_is_not_activated() {
        CORE_UserTriggerHandler handler = new CORE_UserTriggerHandler();
        String exceptionMessage;
        Integer nbQueue = 0;
        try{
            test.startTest();
            handler.afterInsert();
            nbQueue = Limits.getQueueableJobs();
            test.stopTest();
        } catch(Exception e){
            exceptionMessage = e.getMessage();
        } finally{
            System.assertEquals(null, exceptionMessage);
            System.assertEquals(0, nbQueue);
        }
    }

    @IsTest
    public static void afterInsert_should_call_userProvisioningProcess_when_metada_is_activated() {
        String lastnameFilter = '%' + USER_UNIQUE_KEY + '%';
        User userTest = [SELECT Id,IsActive, FirstName, LastName, Name, CallcenterId, Email, CORE_FLEX_Twilio_SID__c FROM user WHERE LastName LIKE :lastnameFilter ];
        
        CORE_UserTriggerHandler handler = new CORE_UserTriggerHandler();
        handler.omniConfig.CORE_Active_User_Provisioning__c = true;
        
        handler.newRecords = new List<User>{userTest};
        handler.oldMap = null;

        String exceptionMessage;
        Integer nbQueue = 0;
        try{
            Test.startTest();
            handler.afterInsert();
            nbQueue = Limits.getQueueableJobs();
            Test.stopTest();
            
        } catch(Exception e){
            exceptionMessage = e.getMessage();
        } finally{
            System.assertEquals(null, exceptionMessage);
            System.assert(nbQueue == 1 || nbQueue == 2);
        }
    }

    @IsTest
    public static void afterUpdate_should_not_call_userProvisioningProcess_when_metada_is_not_activated() {
        CORE_UserTriggerHandler handler = new CORE_UserTriggerHandler();
        String exceptionMessage;
        Integer nbQueue = 0;
        try{
            test.startTest();
            handler.afterUpdate();
            nbQueue = Limits.getQueueableJobs();
            test.stopTest();
        } catch(Exception e){
            exceptionMessage = e.getMessage();
        } finally{
            System.assertEquals(null, exceptionMessage);
            System.assertEquals(0, nbQueue);
        }
    }

    @IsTest
    public static void afterUpdate_should_call_userProvisioningProcess_when_provisioning_is_activated() {
        String lastnameFilter = '%' + USER_UNIQUE_KEY + '%';
        User userTest = [SELECT Id,IsActive, FirstName, LastName, Name, CallcenterId, Email, CORE_FLEX_Twilio_SID__c FROM user WHERE LastName LIKE :lastnameFilter ];
        
        CORE_UserTriggerHandler handler = new CORE_UserTriggerHandler();
        handler.omniConfig.CORE_Active_User_Provisioning__c = true;
        //userTest.IsActive = false;
        handler.newRecords = new List<User>{userTest};
        User oldUser = userTest.clone(true, true, true, true);
        oldUser.CallCenterId = null;

        handler.oldMap = new Map<Id, User>{oldUser.Id => oldUser};

        String exceptionMessage;
        Integer nbQueue = 0;
        try{
            Test.startTest();
            handler.afterUpdate();
            nbQueue = Limits.getQueueableJobs();
            Test.stopTest();
            
        } catch(Exception e){
            exceptionMessage = e.getMessage();
        } finally{
            System.assertEquals(null, exceptionMessage);
            System.assert(nbQueue == 1 || nbQueue == 2);
        }
    }

    @IsTest
    public static void afterUpdate_should_call_userProvisioningProcess_when_deprovisioning_is_activated() {
        String lastnameFilter = '%' + USER_UNIQUE_KEY + '%';
        User userTest = [SELECT Id,IsActive, FirstName, LastName, Name, CallcenterId, Email, CORE_FLEX_Twilio_SID__c FROM user WHERE LastName LIKE :lastnameFilter ];
        
        CORE_UserTriggerHandler handler = new CORE_UserTriggerHandler();
        handler.omniConfig.CORE_Active_User_Deprovisioning__c = true;
        userTest.IsActive = false;
        
        handler.newRecords = new List<User>{userTest};
        User oldUser = userTest.clone(true, true, true, true);
        oldUser.IsActive = true;
        handler.oldMap = new Map<Id, User>{oldUser.Id => oldUser};

        String exceptionMessage;
        Integer nbQueue = 0;
        try{
            Test.startTest();
            handler.afterUpdate();
            nbQueue = Limits.getQueueableJobs();
            Test.stopTest();
        } catch(Exception e){
            exceptionMessage = e.getMessage();
        } finally{
            System.assertEquals(null, exceptionMessage);
            System.assert(nbQueue == 1 || nbQueue == 2);
        }
    }
}