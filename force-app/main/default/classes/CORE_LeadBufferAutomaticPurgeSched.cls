public without sharing class CORE_LeadBufferAutomaticPurgeSched implements Schedulable {
    public void execute(SchedulableContext sC) {
        CORE_LeadBufferAutomaticPurge leadBufferAutomaticPurge = new CORE_LeadBufferAutomaticPurge();
        database.executebatch(leadBufferAutomaticPurge);
     }
}