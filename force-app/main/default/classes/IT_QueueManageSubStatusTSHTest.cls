/**
 * Created by SaverioTrovatoNigith on 19/04/2022.
 */

@IsTest
private class IT_QueueManageSubStatusTSHTest {
    @IsTest
    static void testBehavior1() {
        List<Opportunity> oldOppList = new List<Opportunity>();
        List<Opportunity> newOppList = new List<Opportunity>();
        Opportunity opp = new Opportunity();
        opp.Description = 'Old';
        opp.StageName = 'Prospect';
        opp.CloseDate = System.today();
        opp.CORE_OPP_Sub_Status__c = 'Prospect - New';
        oldOppList.add(opp);
        insert oldOppList;

        for (Opportunity o : oldOppList) {
            o.Description = 'New';
            newOppList.add(o);
        }
        update newOppList;

        Map<Id, Opportunity> oldMap = new Map<Id, Opportunity>(oldOppList);


        String stringList = JSON.serialize(newOppList);
        String stringMap = JSON.serialize(oldMap);

        Test.startTest();
        IT_QueueManageSubStatusTSH jobToEnq = new IT_QueueManageSubStatusTSH(stringList, stringMap);
        System.enqueueJob(jobToEnq);
        Test.stopTest();
    }

    @IsTest
    static void testBehavior2() {
        List<Opportunity> oldOppList = new List<Opportunity>();
        List<Opportunity> newOppList = new List<Opportunity>();
        Opportunity opp = new Opportunity();
        opp.Description = 'Old';
        opp.StageName = 'Prospect';
        opp.CloseDate = System.today();
        opp.CORE_OPP_Sub_Status__c = 'Prospect - New';
        oldOppList.add(opp);
        insert oldOppList;

        for (Opportunity o : oldOppList) {
            o.StageName = 'Withdraw';
            newOppList.add(o);
        }
        update newOppList;

        Map<Id, Opportunity> oldMap = new Map<Id, Opportunity>();

        String stringList = JSON.serialize(newOppList);
        String stringMap = JSON.serialize(oldMap);

        Test.startTest();
        IT_QueueManageSubStatusTSH jobToEnq = new IT_QueueManageSubStatusTSH(stringList, stringMap);
        System.enqueueJob(jobToEnq);
        Test.stopTest();
    }

    @IsTest
    static void testBehavior3() {
        List<Opportunity> oldOppList = new List<Opportunity>();
        Opportunity opp = new Opportunity();
        opp.StageName = 'Prospect';
        opp.CloseDate = System.today();
        opp.CORE_OPP_Sub_Status__c = 'Prospect - New';
        oldOppList.add(opp);
        insert oldOppList;

        Map<Id, Opportunity> oldMap = new Map<Id, Opportunity>();
        oldMap.put(opp.Id, opp);
        String stringMap = JSON.serialize(oldMap);

        opp.StageName = 'Withdraw';
        opp.CORE_OPP_Sub_Status__c = 'Withdraw - Term Deferral';
        update opp;

        List<Opportunity> newOppList = new List<Opportunity>();
        newOppList.add(opp);
        String stringList = JSON.serialize(newOppList);

        Test.startTest();
        IT_QueueManageSubStatusTSH jobToEnq = new IT_QueueManageSubStatusTSH(stringList, stringMap);
        System.enqueueJob(jobToEnq);
        Test.stopTest();
    } 
}