/*************************************************************************************************************
 * @name			IT_EmailMessageTriggerHandler
 * @author			Andrea Morittu <amorittu@atlantic-technologies.com>
 * @created			22 / 04 / 2022
 * @description		Description of your code
 *
 * Changes (version)
 * -----------------------------------------------------------------------------------------------------------
 * 				No.		Date			Author					Description
 * 				----	------------	--------------------	----------------------------------------------
 * @version		1.0		2022-04-22		Andrea Morittu			Changes desription
 *
**************************************************************************************************************/
public class IT_EmailMessageTriggerHandler extends TriggerHandler {
    public static final String CLASS_NAME = IT_EmailMessageTriggerHandler.class.getName();
    public static Boolean fireFurtherContact = true;

    /*********************************************************************************************************
     * @name			onAfterInsert
     * @author			Andrea Morittu <amorittu@atlantic-technologies.com>
     * @created			22 / 04 / 2022
     * @description		after insert method (it comes from trigger)
     * @param			List<EmailMessage> : list of records insered
     * @return			void
    **********************************************************************************************************/
    public void onAfterInsert(List<EmailMessage> newRecordsList) {
        System.debug(CLASS_NAME + 'onAfterInsert has been fired! -- START ');

        if (fireFurtherContact) {
            fireInboundEmailFurtherContact(newRecordsList);
        }
        System.debug(CLASS_NAME + 'onAfterInsert has been fired! -- END ');

    }

    /*********************************************************************************************************
     * @name			fireInboundEmailFurtherContact
     * @author			Andrea Morittu <amorittu@atlantic-technologies.com>
     * @created			22 / 04 / 2022
     * @description		method to fire the Further Contact Logic on email sent 
     * @param			List<EmailMessage> : list of records insered
     * @return			void
    **********************************************************************************************************/
    public static void fireInboundEmailFurtherContact(List<EmailMessage> newRecordsList) {
        EmailMessage[] COA_InboundEmailMessages = new EmailMessage[]{};

        Map<String, EmailMessage> relatedToId_EmailMessageMap = new Map<String, EmailMessage>();

        for (EmailMessage emailMessage : newRecordsList) {
            if ( emailMessage.Incoming == true && String.isNotBlank(emailMessage.ActivityId) ) {
                relatedToId_EmailMessageMap.put(emailMessage.relatedToId, emailMessage);
            }
        }
        Map<Opportunity, EmailMessage> opp_emailMessage = new Map<Opportunity, EmailMessage>();

        if (!relatedToId_EmailMessageMap.keySet().isEmpty() ) {

            //IT_CoaProcessnumber__c -> This field is set up whenever a new COA is created. So, in case of 0 i know it's a lead new, in case of > 0 it'll be a Further Contact
            Opportunity[] relatedOpportunities = [ 
                SELECT Id, RecordTypeId, CORE_OPP_Business_Hours__c, IT_CountryRelevance__c,    // OPPORTUNITY FIELDS START
                IT_OfficeInCharge__c, IT_COAInProgress__c, 
                CORE_Capture_Channel__c, CORE_OPP_Business_Unit__c, CORE_OPP_Intake__c, IT_CoaProcessNumber__c ,
                IT_CountAcquisitionPointOfContact__c, IT_OpportunityAssigned__c , 
                IT_InformationQueue__c, IT_OrientationQueue__c, IT_AdmissionQueue__c,
                IT_InformationQueue__r.OwnerId, IT_OrientationQueue__r.OwnerId, IT_AdmissionQueue__r.OwnerId,
                OwnerId, CORE_OPP_Division__c, CORE_OPP_Agent_Perception__c,                    // OPPORTUNITY FIELDS END
                AccountId, IT_ServiceCall__c 
                FROM Opportunity
                WHERE Id IN: relatedToId_EmailMessageMap.keySet() 
                AND (IT_CoaProcessnumber__c > 0 )
            ];
            
            Set<Id> accountIDS = new Set<Id>();
            for (Opportunity opportunity : relatedOpportunities) {
                accountIDS.add(opportunity.AccountId);
            }

            Map<Id, Account> relatedAccounts = new Map<Id, Account>([
                SELECT Phone, Type, CORE_Mobile_2__pc,              // ACCOUNT FIELDS START
                    CORE_Mobile_3__pc, CORE_Personal_Assistant_Phone__pc, 
                    PersonEmail, PersonContactId, PersonMobilePhone,
                    CORE_Personal_Assistant_Phone_Opt_Out__pc,
                    CORE_Phone2__pc, CORE_Phonetic_name__pc, 
                    CORE_Professional_Phone__pc, CORE_TECH_All_Phones__c,
                    CORE_Whatsapp__pc
                FROM Account
                WHERE Id IN: accountIDS 
            ]);

            if ( ! relatedOpportunities.isEmpty() && !Test.isRunningTest() ) {
                System.enqueueJob(new IT_EmailMessageQueuable(relatedOpportunities, relatedAccounts, relatedToId_EmailMessageMap) );
            }
        }
    }
}