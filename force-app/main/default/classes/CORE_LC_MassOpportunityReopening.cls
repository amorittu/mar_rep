/**
* @author Fodil BOUDJEDIEN - Almavia
* @date 2022-03-24
* @group 
* @description Class used by lwc 
* @test class 
*/
public without sharing class CORE_LC_MassOpportunityReopening {

    @AuraEnabled(cacheable=true)
    public static String getSessionID(){ // getting session id using a dedicated apex page
        String content ='';
        if(! Test.isRunningTest() ){
             content = Page.CORE_VFP_SessionIDPage.getContent().toString();
        }else {// for test
             content = 'START_SESSION_IDabcdefghtEND_SESSION_ID';
        }

        return content.substringBetween('START_SESSION_ID', 'END_SESSION_ID').trim();
    }

    @AuraEnabled(cacheable=true)
    public static Map<String,Object> getOrgSetting(){
        try {
            MassOpportunityReregistration__c settings = MassOpportunityReregistration__c.getOrgDefaults();

            return new Map<String,Object>{'JobInProgress'=>settings.Job_in_Progress__c};
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled(cacheable=true)
    public static List<ListView> getListViews(){
        try {
                MassOpportunityReregistration__c settings = MassOpportunityReregistration__c.getOrgDefaults();
                
                String developerNameEnding = '%'+settings.ListView_Ending__c+'%';
                return [SELECT Id, Name, DeveloperName FROM ListView Where SobjectType = 'Opportunity' AND DeveloperName Like :developerNameEnding ];

        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    public static String getListViewQuery (id listViewId){
        String query;
        HttpRequest req = new HttpRequest();
        req.setEndpoint('callout:CORE_MassOpportunityEndpoint' + '/Opportunity/listviews/' + listViewId + '/describe');
        req.setHeader('Authorization', 'Bearer ' + CORE_LC_MassOpportunityReopening.getSessionId());
        req.setMethod('GET');
        Http http = new Http();
        HttpResponse res = http.send(req);

        if(res.getStatusCode() == 200){
            Map<String, Object> tokenRespMap = (Map<String, Object>)JSON.deserializeUntyped(res.getBody());

            query = (String)tokenRespMap.get('query');//.replaceAll('\'', '"');
        }
        return query;
    }

    private static String getQueryWhereCondition(String query) {
        return query.substringAfter(' WHERE ').substringBefore(' ORDER BY ');
    }

    @AuraEnabled(cacheable=true)
    public static List<Opportunity> getListViewRecordsCount( id listViewId){
        try {
            String query = getListViewQuery(listViewId);
            System.debug('@getListViewRecordsCount > query'+query);
            String whereQuery = query.substringAfter(' WHERE ');
            String countQuery='SELECT count(id) Total  FROM Opportunity ';
            if(whereQuery != ''){
                countQuery+= ' WHERE '+whereQuery.substringBefore(' ORDER BY ');
            }
            System.debug('@getListViewRecordsCount > countQuery'+countQuery);
            
            return Database.query(countQuery);

        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }
    
    @AuraEnabled
    public static List<CORE_Business_Unit__c> getBusinessUnits(){
        try {
            return [SELECT Id, Name FROM CORE_Business_Unit__c];
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled
    public static String getCurrentAcademicYear(){
        try {
            List<String> allAcademicYears = CORE_PicklistUtils.getPicklistValues('Opportunity', 'CORE_OPP_Academic_Year__c');
            String currentYear = '';
            if(Date.today().month()>=9){
                currentYear = String.valueOf(Date.today().year());
            }else{
                currentYear = String.valueOf(Date.today().year()-1);
            }

            String currentPickValue = '';

            for(String pickval : allAcademicYears ){
                if(pickval.contains(currentYear+'-')){
                    currentPickValue = pickval;
                }
            }

            return currentPickValue;

        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    @AuraEnabled
    public static String launchBatch(Integer recordSize,String academicYear,String businessUnitId, id listViewId){
        String typeOfExecution = '';
        Savepoint sp;
        CORE_MassOpportunityResultHandler resultHandler = new CORE_MassOpportunityResultHandler();
        resultHandler.settingName = 'Mass_Opportunity_Reregistration';
        List<String> resultsList = new  List<String>();
        try {
            MassOpportunityReregistration__c settings = MassOpportunityReregistration__c.getOrgDefaults();
            Integer synchornousBatch = Math.round((settings.Synchronous_Batch__c == null)? 200 : settings.Synchronous_Batch__c);
            Integer batchSize = Math.round((settings.Batch_Size__c == null)? 200 : settings.Synchronous_Batch__c);
            if( recordSize <= synchornousBatch ){
                typeOfExecution =  'SYNC';
                List<Opportunity> opportunities = (List<Opportunity>) Database.query(CORE_MassOpportunityReopeningHandler.getMassOpportunityReregistrationQuery(businessUnitId,academicYear,getQueryWhereCondition(getListViewQuery(listViewId))));
                sp = Database.setSavePoint();
                for(Opportunity opp :opportunities){
                    resultsList.add('(Name : '+opp.Name+', Id: '+opp.Id+' ) ');
                }

                CORE_MassOpportunityReopeningHandler.handleOpportunitiesReregistration(academicYear,opportunities,businessUnitId);
            } else {
                typeOfExecution =  'ASYNC';
                Id jobId = Database.executeBatch(new CORE_MassOpportunityReopeningBatch(academicYear,businessUnitId,getQueryWhereCondition(getListViewQuery(listViewId))),batchSize);
                settings.Job_in_Progress__c = true;
                update settings;
            }
            
        } catch (Exception e) {
            if(resultsList.size()>0){
                resultHandler.resultsList = new List<String>{String.join(resultsList, '\n')};
                resultHandler.sendMail();
                Database.rollback(sp);
            }
            
            throw new AuraHandledException(e.getMessage());
        }
        return typeOfExecution;
    }

    @AuraEnabled
    public static List<Opportunity> getOpportunitiesTotal(Id listViewId, String buId, String aY){
        try {
            String acceptableStatuses = 'Registered;Enrolled;Re-registered;Re-enrolled;Closed Won;Year Out';            
            List<String> acceptableStatusesList = acceptableStatuses.split(';');
            String query = 'SELECT count(id) Total FROM OPPORTUNITY';

            String whereQuery = '';
            whereQuery = ' WHERE (RecordType.Name = \'Re-registration\' OR RecordType.Name = \'Enrollment\') AND CORE_OPP_Business_Unit__c = :buId AND CORE_OPP_Academic_Year__c= :aY ' + ' AND StageName IN :acceptableStatusesList '
            + ' AND (CORE_Technical_Sub_Statut__c = false OR (CORE_OPP_Sub_Status__c Like \'%Exit - In Exchange%\' AND StageName IN (\'Re-enrolled\',\'Re-registered\',\'Closed Won\',\'Year Out\'))) '+' AND CORE_Re_registered_opportunity__c = null ';

            String listviewWhereQuery = getQueryWhereCondition(CORE_LC_MassOpportunityReopening.getListViewQuery(listViewId));
            if(listviewWhereQuery != ''){
                whereQuery+= ' AND ( '+listviewWhereQuery.substringBefore(' ORDER BY ') +' )';
            }
           system.debug('where query : '+wherequery);
            return Database.query(query+whereQuery);
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }
}