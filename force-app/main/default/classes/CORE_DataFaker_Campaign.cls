@IsTest
public class CORE_DataFaker_Campaign {
    public static Campaign getCampaign(String uniqueKey) {
        Campaign campaign = new Campaign(
            Name = 'campaign test - '+ uniqueKey,
            CORE_External_Id__c = uniqueKey,
            IsActive = true,
            Type = 'Other'
        );

        insert campaign;
        return campaign;
    }
    public static Campaign getCampaignWithoutInsert(String uniqueKey, Boolean isOnline, String Status, String buId, String timeZone, Datetime endDate) {
        Campaign campaign = new Campaign(
            Name = 'campaign test - '+ uniqueKey,
            CORE_Campaign_Full_Name__c = 'campaign full name test - ' + uniqueKey,
            Status = Status,//'Planned'
            CORE_External_Id__c = uniqueKey,
            CORE_Livestorm_Event_Id__c = uniqueKey,
            IsActive = true,
            Type = 'Other',
            CORE_Online_event__c = isOnline,
            CORE_Business_Unit__c = buId,
            CORE_Timezone__c = timeZone,
            CORE_End_Date_and_hour__c = endDate
        );
        return campaign;
    }
    
    public static Campaign getCampaign(String uniqueKey, Boolean isOnline, String Status, String buId, String timeZone, Datetime startDate, Datetime endDate) {
        Campaign campaign = new Campaign(
            Name = 'campaign test - '+ uniqueKey,
            CORE_Campaign_Full_Name__c = 'campaign full name test - ' + uniqueKey,
            Status = Status,//'Planned'
            CORE_External_Id__c = uniqueKey,
            CORE_Livestorm_Event_Id__c = uniqueKey,
            IsActive = true,
            Type = 'Other',
            CORE_Online_event__c = isOnline,
            CORE_Business_Unit__c = buId,
            CORE_Timezone__c = timeZone,
            CORE_Start_Date_and_Hour__c = startDate,
            CORE_End_Date_and_hour__c = endDate
        );

        insert campaign;
        return campaign;
    }

    public static CampaignMember getCampaignMember(Id campaignId, Id contactId) {
        CampaignMember campaignMember = new CampaignMember(
            CampaignId = campaignId,
            ContactId = contactId
        );

        insert campaignMember;
        return campaignMember;
    }
}