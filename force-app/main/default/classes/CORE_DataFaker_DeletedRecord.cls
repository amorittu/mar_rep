@IsTest
public class CORE_DataFaker_DeletedRecord {
    public static void insertDeletedRecord(Datetime deletionDate, String recordId){
        CORE_Deleted_record__c deletedRecord = new CORE_Deleted_record__c(
            CORE_Deletion_Date__c = deletionDate,
            CORE_Object_Name__c = 'CORE_PKL_Account',
            CORE_Record_Id__c = recordId
        );

        Insert deletedRecord;
    }
}