@isTest
private class CORE_LC_MassOpportunityClosing_TEST {

    private static final String VIEW_NAME = 'MyOpportunities';

    @TestSetup
    static void createData(){
        
        Id profileId = [SELECT Id FROM Profile WHERE Name = 'Core Telesales' LIMIT 1].Id;//Core Head of sales

        User usr = new User(
            Username = 'usertest@masstest.com',
            LastName = 'TEST_USER',
            Email = 'testemail@yopmail.com',
            Alias = 'T', 
            CommunityNickname = 'T',
            TimeZoneSidKey = 'Europe/Paris',
            LocaleSidKey = 'fr_FR_EURO',
            EmailEncodingKey = 'ISO-8859-1', 
            ProfileId = profileId,
            LanguageLocaleKey = 'fr'      
        );
        database.insert(usr, true);

        System.runAs(usr){
            Account account = CORE_DataFaker_Account.getStudentAccount('test1');

            CORE_Division__c division = new CORE_Division__c(
                CORE_Division_code__c = '1234',
                CORE_Default_Academic_Year__c = 'CORE_PKL_2021-2022'
            );
            database.insert(division, true);

            CORE_Business_Unit__c bu = new CORE_Business_Unit__c(
                CORE_BU_Technical_User__c = userinfo.getUserId(),
                CORE_Business_Unit_code__c = '1234',
                CORE_Parent_Division__c = division.Id
            );
            database.insert(bu, true);

            CORE_Study_Area__c studyArea = new CORE_Study_Area__c(
                CORE_Parent_Business_Unit__c = bu.Id,
                CORE_Study_Area_code__c = '1234'
            );
            database.insert(studyArea, true);

            CORE_Curriculum__c curriculum = new CORE_Curriculum__c(
                CORE_Parent_Study_Area__c = studyArea.Id,
                CORE_Curriculum_code__c = '1234',
                CORE_Curriculum_Name__c = '1234'
            );
            database.insert(curriculum, true);

            CORE_Level__c level = new CORE_Level__c(
                CORE_Level_code__c = '1234',
                CORE_Parent_Curriculum__c = curriculum.Id,
                CORE_Year__c = 'CORE_PKL_BA_1',
                CORE_Study_Cycle__c = 'CORE_PKL_BTS'
            );
            database.insert(level, true);

            List<CORE_Intake__c> intakesList = new List<CORE_Intake__c>();

            for(Integer i = 0; i < 13; i++){
                String month = (i == 0) ? 'January' :(i == 1) ? 'February' :(i == 2) ? 'March' :(i == 3) ? 'April' 
                             : (i == 4) ? 'May' :(i == 5) ? 'June' :(i == 6) ? 'July' :(i == 7) ? 'August' :(i == 8) ? 'September' :(i == 9) ? 'October'
                             : (i == 10) ? 'November' :(i == 11) ? 'December' :'ERROR';

                String name = (i == 12) ? '2021ERROR' :'2021 ' + month;

                CORE_Intake__c intake = new CORE_Intake__c(
                    Name = name,
                    CORE_Academic_Year__c = 'CORE_PKL_2021-2022',
                    CORE_Intake_code__c = '1234',
                    CORE_Parent_Level__c = level.Id,
                    CORE_Session__c = 'October'
                );

                intakesList.add(intake);
            }
            database.insert(intakesList, true);

            List<Opportunity> oppsList = new List<Opportunity>();
            Integer intakeIndex = 0;

            for(Integer i = 0; i < 20; i++){
                Opportunity opportunity = new Opportunity(
                    Name ='test' + i,
                    StageName = 'Lead',
                    CORE_OPP_Sub_Status__c = 'Lead - New',
                    CORE_OPP_Division__c = division.Id,
                    CORE_OPP_Business_Unit__c = bu.Id,
                    CORE_OPP_Study_Area__c = studyArea.Id,
                    CORE_OPP_Curriculum__c = curriculum.Id,
                    CORE_OPP_Level__c = level.Id,
                    CORE_OPP_Intake__c = intakesList[intakeIndex].Id,
                    AccountId = account.Id,
                    CloseDate = Date.Today().addDays(2)
                );

                oppsList.add(opportunity);
                intakeIndex++;

                if(intakeIndex == 12){
                    intakeIndex = 0;
                }
            }

            database.insert(oppsList, true);
        }
    }

    @isTest
    static void test_NoSetting(){

        User usr = [SELECT Id FROM User WHERE Username = 'usertest@masstest.com' LIMIT 1];

        System.runAs(usr){
            test.startTest();

            try{
                CORE_LC_MassOpportunityClosing.getOrgSetting();
            }
            catch(Exception e){
                system.debug('@test_NoSetting > e: ' + e);
                system.assertEquals(Label.CORE_MOC_MSG_NO_SETTINGS, e.getMessage());
            }

            test.stopTest();
        }
    }

    @isTest
    static void test_Sync(){
        CORE_MassOpportunityClosing__c orgSetting = new CORE_MassOpportunityClosing__c(
            CORE_BatchSize__c = 20, CORE_ListViewNameFilter__c = VIEW_NAME
        );
        database.insert(orgSetting, true);

        User usr = [SELECT Id FROM User WHERE Username = 'usertest@masstest.com' LIMIT 1];

        System.runAs(usr){

            StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
            mock.setStaticResource('CORE_MassOppClosing_VIEW_RESP');
            mock.setStatusCode(200);
            
            // Set the mock callout mode
            Test.setMock(HttpCalloutMock.class, mock);

            test.startTest();
            CORE_LC_MassOpportunityClosing.SettingWrapper setting = CORE_LC_MassOpportunityClosing.getOrgSetting();
            system.assertEquals(setting.listViewFilter, VIEW_NAME);

            List<CORE_LC_MassOpportunityClosing.ListViewWrapper> listViews = CORE_LC_MassOpportunityClosing.getListViewsInfo();
            system.assertEquals(listViews[0].viewName, VIEW_NAME);

            Map<String, String> theObj = new Map<String, String>();
            theObj.put('value',listViews[0].viewId);
            theObj.put('query',listViews[0].viewQuery);
            CORE_LC_MassOpportunityClosing.CurrentViewWrapper view = new CORE_LC_MassOpportunityClosing.CurrentViewWrapper();
            view.viewQuery = listViews[0].viewQuery;
            view.viewId = listViews[0].viewId;
            view.viewRecordsSize = 20;
            List<Opportunity> oppsList = database.query(view.viewQuery);
            system.assertEquals(view.viewRecordsSize, oppsList.size());

            String result = CORE_LC_MassOpportunityClosing.runCloseOpps(view, 'Closed Won');
            //system.assertEquals(null, result);
            test.stopTest();
            for(Opportunity opp : [SELECT Id, StageName, CORE_Change_Status__c FROM Opportunity WHERE Id IN :oppsList]){
                system.debug('@test_Sync > opp: ' + opp);
                system.assertEquals('Closed Won', opp.StageName);
            }

            
        }
    }

    @isTest
    static void test_Async(){
        CORE_MassOpportunityClosing__c orgSetting = new CORE_MassOpportunityClosing__c(
            CORE_BatchSize__c = 10, CORE_ListViewNameFilter__c = VIEW_NAME
        );
        database.insert(orgSetting, true);

        User usr = [SELECT Id FROM User WHERE Username = 'usertest@masstest.com' LIMIT 1];

        System.runAs(usr){

            StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
            mock.setStaticResource('CORE_MassOppClosing_VIEW_RESP');
            mock.setStatusCode(200);
            
            // Set the mock callout mode
            Test.setMock(HttpCalloutMock.class, mock);

            test.startTest();
            CORE_LC_MassOpportunityClosing.SettingWrapper setting = CORE_LC_MassOpportunityClosing.getOrgSetting();
            system.assertEquals(setting.listViewFilter, VIEW_NAME);

            List<CORE_LC_MassOpportunityClosing.ListViewWrapper> listViews = CORE_LC_MassOpportunityClosing.getListViewsInfo();
            system.assertEquals(listViews[0].viewName, VIEW_NAME);

            Map<String, String> theObj = new Map<String, String>();
            theObj.put('value',listViews[0].viewId);
            theObj.put('query',listViews[0].viewQuery);
            CORE_LC_MassOpportunityClosing.CurrentViewWrapper view = new CORE_LC_MassOpportunityClosing.CurrentViewWrapper();
            view.viewQuery = listViews[0].viewQuery;
            view.viewId = listViews[0].viewId;
            view.viewRecordsSize = 20;
            List<Opportunity> oppsList = database.query(view.viewQuery);

            system.assertEquals(view.viewRecordsSize, oppsList.size());

            String result = CORE_LC_MassOpportunityClosing.runCloseOpps(view, 'Closed Won');
            system.assertEquals('ASYNC', result);

            test.stopTest();
        }
    }

    @isTest
    static void test_Async_Error(){
        CORE_MassOpportunityClosing__c orgSetting = new CORE_MassOpportunityClosing__c(
            CORE_BatchSize__c = 10, CORE_ListViewNameFilter__c = VIEW_NAME
        );
        database.insert(orgSetting, true);

        User usr = [SELECT Id FROM User WHERE Username = 'usertest@masstest.com' LIMIT 1];

        System.runAs(usr){

            test.startTest();
            try{
                System.enqueueJob(new CORE_QU_MassOpportunityClosing(null, '', null));
            }
            catch(Exception e){
                system.assertEquals('AuraHandledException', e.getTypeName());
            }

            test.stopTest();
        }
    }
}