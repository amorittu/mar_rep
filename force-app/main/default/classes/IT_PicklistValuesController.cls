/**
 * Created by mmarinelli on 14/07/2021.
 * TestClass: PicklistValuesControllerTest
 */

public with sharing class IT_PicklistValuesController {

    public class MyPickListInfo
    {
        public String validFor;
    }

    public class PickListValues
    {
        @AuraEnabled
        public String label;
        @AuraEnabled
        public String value;

        public PickListValues(){
        }

        public PickListValues(String val, String lab){
            label = lab;
            value = val;
        }
    }

    public static Map<Schema.PicklistEntry, List<Schema.PicklistEntry>> getFieldDependencies(String objectName, String controllingField, String dependentField)
    {
        Map<Schema.PicklistEntry, List<Schema.PicklistEntry>> controllingInfo = new Map<Schema.PicklistEntry, List<Schema.PicklistEntry>>();

        Schema.SObjectType objType = Schema.getGlobalDescribe().get(objectName);

        Schema.DescribeSObjectResult describeResult = objType.getDescribe();
        Schema.DescribeFieldResult controllingFieldInfo = describeResult.fields.getMap().get(controllingField).getDescribe();
        Schema.DescribeFieldResult dependentFieldInfo = describeResult.fields.getMap().get(dependentField).getDescribe();

        List<Schema.PicklistEntry> controllingValues = controllingFieldInfo.getPicklistValues();
        List<Schema.PicklistEntry> dependentValues = dependentFieldInfo.getPicklistValues();

        for(Schema.PicklistEntry currControllingValue : controllingValues)
        {
            // System.debug('ControllingField: Label:' + currControllingValue.getLabel());
            controllingInfo.put(currControllingValue, new List<Schema.PicklistEntry>());
        }

        for(Schema.PicklistEntry currDependentValue : dependentValues)
        {
            String jsonString = JSON.serialize(currDependentValue);

            MyPickListInfo info = (MyPickListInfo) JSON.deserialize(jsonString, MyPickListInfo.class);

            String hexString = EncodingUtil.convertToHex(EncodingUtil.base64Decode(info.validFor)).toUpperCase();

            // System.debug('DependentField: Label:' + currDependentValue.getLabel() + ' ValidForInHex:' + hexString + ' JsonString:' + jsonString);

            Integer baseCount = 0;

            for(Integer curr : hexString.getChars())
            {
                Integer val = 0;

                if(curr >= 65)
                {
                    val = curr - 65 + 10;
                }
                else
                {
                    val = curr - 48;
                }

                if((val & 8) == 8)
                {
                    // System.debug('Dependent Field: ' + currDependentValue.getLabel() + ' Partof ControllingField:' + controllingValues[baseCount + 0].getLabel());
                    controllingInfo.get(controllingValues[baseCount + 0]).add(currDependentValue);
                }
                if((val & 4) == 4)
                {
                    // System.debug('Dependent Field: ' + currDependentValue.getLabel() + ' Partof ControllingField:' + controllingValues[baseCount + 1].getLabel());
                    controllingInfo.get(controllingValues[baseCount + 1]).add(currDependentValue);
                }
                if((val & 2) == 2)
                {
                    // System.debug('Dependent Field: ' + currDependentValue.getLabel() + ' Partof ControllingField:' + controllingValues[baseCount + 2].getLabel());
                    controllingInfo.get(controllingValues[baseCount + 2]).add(currDependentValue);
                }
                if((val & 1) == 1)
                {
                    // System.debug('Dependent Field: ' + currDependentValue.getLabel() + ' Partof ControllingField:' + controllingValues[baseCount + 3].getLabel());
                    controllingInfo.get(controllingValues[baseCount + 3]).add(currDependentValue);
                }

                baseCount += 4;
            }
        }

        //System.debug('ControllingInfo: ' + controllingInfo);

        return controllingInfo;
    }

    public static Map<String, List<PickListValues>> getWrapperToDisplay(String objectName, String controllingField, String dependentField){
        Map<Schema.PicklistEntry, List<Schema.PicklistEntry>> pickMap = getFieldDependencies(objectName, controllingField, dependentField);
        Map<String, List<PickListValues>> outMap = new Map<String, List<PickListValues>>();

        for(Schema.PicklistEntry controlling : pickMap.KeySet()){
            List<Schema.PicklistEntry> controlledList = pickMap.get(controlling);
            List<PickListValues> controlledListWrapper = new List<PickListValues>();

            for(Schema.PicklistEntry controlled : controlledList){
                PickListValues wrap = new PickListValues(controlled.getValue(), controlled.getLabel());
                controlledListWrapper.add(wrap);
            }
            outMap.put(controlling.getValue(), controlledListWrapper );

        }

        return outMap;
    }
/*
@AuraEnabled
public static String getProfileName(){
    String profId = UserInfo.getProfileId();
    Profile prf = [SELECT Name FROM Profile WHERE Id =:profId];
    return prf.Name;
}
*/
    @AuraEnabled
    public static String getWrapperToDisplayJSON(String objectName, String controllingField, String dependentField){
        return JSON.serialize(getWrapperToDisplay(objectName, controllingField, dependentField));
    }

    @AuraEnabled
    public static String getPickListValuesIntoList(String objectType, String selectedField){
        List<PickListValues> pickListValuesList = new List<PickListValues>();
        try {
            Schema.SObjectType convertToObj = Schema.getGlobalDescribe().get(objectType);
            Schema.DescribeSObjectResult res = convertToObj.getDescribe();
            Schema.DescribeFieldResult fieldResult = res.fields.getMap().get(selectedField).getDescribe();
            List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
            for( Schema.PicklistEntry pickListVal : ple){
                if(pickListVal.isActive()){
                    PickListValues pick = new PickListValues(pickListVal.getValue(), pickListVal.getLabel());
                    //pickListValuesList.put(pickListVal.getValue(),pickListVal.getLabel());
                    pickListValuesList.add(pick);
                }
            }
        } catch (Exception e){
            System.debug(e);
        }
        return JSON.serialize(pickListValuesList);
    }
}