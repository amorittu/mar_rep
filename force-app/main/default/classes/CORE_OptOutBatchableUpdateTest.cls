@IsTest
private class CORE_OptOutBatchableUpdateTest {
    
    @TestSetup
    public static void setup(){
        CORE_Consent_Setting__mdt consentMdt = [SELECT CORE_Scope__c FROM CORE_Consent_Setting__mdt LIMIT 1];
        Account account = CORE_DataFaker_Account.getStudentAccount('test');
        CORE_CatalogHierarchyModel catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test2').catalogHierarchy;
		CORE_Consent__c consent1 = new CORE_Consent__c();
        consent1.CORE_Person_Account__c = account.Id;
        consent1.CORE_Scope__c = consentMdt.CORE_Scope__c;
        consent1.CORE_Opt_out__c = true;
        consent1.CORE_Consent_type__c = 'CORE_PKL_Global_consent';
        consent1.CORE_Division__c = catalogHierarchy.division.Id;
        consent1.CORE_Business_Unit__c = catalogHierarchy.businessUnit.Id;
        consent1.CORE_Consent_channel__c = 'CORE_PKL_Email';
        
        CORE_Consent__c consent2 = new CORE_Consent__c();
        consent2.CORE_Person_Account__c = account.Id;
        consent2.CORE_Scope__c = consentMdt.CORE_Scope__c;
        consent2.CORE_Opt_in__c = true;
        consent2.CORE_Consent_type__c = 'CORE_PKL_Global_consent';
        consent2.CORE_Division__c = catalogHierarchy.division.Id;
        consent2.CORE_Business_Unit__c = catalogHierarchy.businessUnit.Id;
        consent2.CORE_Consent_channel__c = 'CORE_PKL_Instagram';
        CORE_Consent__c consent3 = new CORE_Consent__c();
        consent3.CORE_Person_Account__c = account.Id;
        consent3.CORE_Scope__c = consentMdt.CORE_Scope__c;
        consent3.CORE_Opt_out__c = true;
        consent3.CORE_Consent_type__c = 'CORE_PKL_Authorisation_to_be_contacted_by_channel';
        consent3.CORE_Division__c = catalogHierarchy.division.Id;
        consent3.CORE_Business_Unit__c = catalogHierarchy.businessUnit.Id;
        consent3.CORE_Consent_channel__c = 'CORE_PKL_SMS';
        CORE_Consent__c consent4 = new CORE_Consent__c();
        consent4.CORE_Person_Account__c = account.Id;
        consent4.CORE_Scope__c = consentMdt.CORE_Scope__c;
        consent4.CORE_Opt_out__c = true;
        consent4.CORE_Consent_type__c = 'CORE_PKL_Authorisation_to_be_contacted_by_channel_marketing';
        consent4.CORE_Consent_channel__c = 'CORE_PKL_Tiktok';
        consent4.CORE_Division__c = catalogHierarchy.division.Id;
        consent4.CORE_Business_Unit__c = catalogHierarchy.businessUnit.Id;
        
       Opportunity opportunity = CORE_DataFaker_Opportunity.getOpportunity(
            catalogHierarchy.division.Id, 
            catalogHierarchy.businessUnit.Id, 
            account.Id, 
            'Prospect', 
            'Prospect - New'
        );
        opportunity.CORE_Opt_out_channels__c = 'CORE_PKL_Facebook_Messenger;CORE_PKL_Instagram;CORE_PKL_Linkedin';
        opportunity.CORE_Opt_out_channels_Marketing__c = 'CORE_PKL_Facebook_Messenger;CORE_PKL_Instagram;CORE_PKL_Linkedin;CORE_PKL_SMS';
        update opportunity;
        CORE_ConsentTriggerHandler.testScope = consentMdt.CORE_Scope__c;
        insert consent1;
        insert consent2;
        insert consent3;
        insert consent4;
    }
    

    @IsTest
    public static void OptOutChannelsAndChannelsMarketingShouldBeUpdated() {
        //List<CORE_Consent__c> consents = [SELECT Id,CORE_Person_Account__c,CORE_Scope__c,CORE_Consent_type__c,CORE_Consent_channel__c,CORE_Opt_in__c,CORE_Opt_out__c,CORE_Division__c,CORE_Business_Unit__c,CORE_Brand__c FROM CORE_Consent__c  WHERE (CORE_Consent__c.CreatedDate >= TODAY OR CORE_Consent__c.LastModifiedDate >= TODAY) ];
        test.starttest();
        CORE_OptOutBatchableUpdate optOutBatchableUpdate = new CORE_OptOutBatchableUpdate();
        //System.debug('batch execution');
		//optOutBatchableUpdate.execute(null,consents);
		database.executebatch(optOutBatchableUpdate);
        test.stopTest();
        Opportunity opportunity = [SELECT ID,CORE_Opt_out_channels__c, CORE_Opt_out_channels_Marketing__c FROM Opportunity];
    
    System.assert(opportunity.CORE_Opt_out_channels__c.Contains('CORE_PKL_Email'));
   System.assert(opportunity.CORE_Opt_out_channels_Marketing__c.Contains('CORE_PKL_Email'));
    System.assert(!opportunity.CORE_Opt_out_channels__c.Contains('CORE_PKL_Instagram'));
   System.assert(!opportunity.CORE_Opt_out_channels_Marketing__c.Contains('CORE_PKL_Instagram'));
    System.assert(opportunity.CORE_Opt_out_channels__c.Contains('CORE_PKL_SMS'));
    System.assert(opportunity.CORE_Opt_out_channels_Marketing__c.Contains('CORE_PKL_Tiktok'));
    }
}