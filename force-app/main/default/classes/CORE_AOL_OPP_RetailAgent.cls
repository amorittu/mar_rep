@RestResource(urlMapping='/aol/opportunity/retailAgent')
global without sharing class CORE_AOL_OPP_RetailAgent {

    public static final String SUCCESS_MESSAGE = 'Retail Agency & progression fiedls updated with success';

    @HttpPut
    global static CORE_AOL_Wrapper.ResultWrapperGlobal execute(String aolExternalId,
                                                                CORE_AOL_Wrapper.ExternalThirdParty externalThirdParty,
                                                                CORE_AOL_Wrapper.AOLProgressionFields aolProgressionFields) {

        // Initiating logs
        Long dt1 = DateTime.now().getTime();
        Long dt2;
        CORE_LogManager logSettings;
        CORE_LOG__c log = new CORE_LOG__c(CORE_API_Name__c = 'AOL');
        Boolean isErrorLog = false;
        logSettings = new CORE_LogManager('AOL');

        CORE_AOL_Wrapper.ResultWrapperGlobal result = checkMandatoryParameters(aolExternalId);

        try{
            if(result != null){
                //missing parameter(s)
                return result;
            }
    
            CORE_AOL_GlobalWorker aolWorker = new CORE_AOL_GlobalWorker(aolProgressionFields,aolExternalId);
            Opportunity opportunity = aolWorker.aolOpportunity;

            if(opportunity == null){
                result = new CORE_AOL_Wrapper.ResultWrapperGlobal(
                        CORE_AOL_Constants.ERROR_STATUS,
                        String.format(CORE_AOL_Constants.MSG_OPPORTUNITY_NOT_FOUND, new List<String>{aolExternalId})
                );
            } else {
                retailAgentProcess(aolWorker.aolOpportunity, externalThirdParty);

                aolWorker.updateOppIsNeeded = true;
                aolWorker.updateAolProgressionFields();
                result = new CORE_AOL_Wrapper.ResultWrapperGlobal(CORE_AOL_Constants.SUCCESS_STATUS, SUCCESS_MESSAGE);
            }

        } catch(Exception e){
            result = new CORE_AOL_Wrapper.ResultWrapperGlobal(CORE_AOL_Constants.ERROR_STATUS, e.getMessage() + ' at ' + e.getStackTraceString());
        } finally{
            dt2 = DateTime.now().getTime();
            System.debug('CORE_AOL_AcademicDiplomaHistory(urlMapping=\'/aol/academic-diploma-history\') : T0TAL TIME OF EXECUTION = ' + (dt2-dt1) + ' ms');
            // creating log
            log.CORE_Processing_Duration__c =(dt2-dt1);
            log.CORE_HTTP_Status_Code__c = result.status == 'KO' ? 400 : result.status == 'OK' ? 200 : 500;
            log.CORE_Sent_Body__c = JSON.serialize(result);
            log.CORE_Received_Body__c =  '{"aolExternalId : "' + aolExternalId + '",' + JSON.serialize(externalThirdParty) + ',' + JSON.serialize(aolProgressionFields) + '}';
            
            if(!logSettings.isNone()){
                if(logSettings.isAllLogged()){ // is All logged
                    CORE_LogManager.createLog(log);
                }else if(logSettings.isErrorLogged() && isErrorLog){ // is Error
                    CORE_LogManager.createLog(log);
                }
            }
        }

        return result;
    }

    public static Opportunity retailAgentProcess(Opportunity opportunity, CORE_AOL_Wrapper.ExternalThirdParty retailAgency){

        opportunity.CORE_OPP_Retail_Agency__c = (retailAgency.retailAgency != null && retailAgency.retailAgency != '')
                                              ? ((Account)CORE_AOL_QueryProvider.getRelatedObj('Account', 'Id', 'CORE_LegacyCRMID__c', retailAgency.retailAgency)).Id
                                              : opportunity.CORE_OPP_Retail_Agency__c;

        opportunity.CORE_RetailAgencyBusinessContact__c = (retailAgency.retailAgencyBusinessContact != null && retailAgency.retailAgencyBusinessContact != '')
                                                        ? ((Contact)CORE_AOL_QueryProvider.getRelatedObj('Contact', 'Id', 'CORE_LegacyContactCRMID__c', retailAgency.retailAgencyBusinessContact)).Id
                                                        : opportunity.CORE_RetailAgencyBusinessContact__c;

        return opportunity;
    }

    private static CORE_AOL_Wrapper.ResultWrapperGlobal checkMandatoryParameters(String aolExternalId){
        List<String> missingParameters = new List<String>();
        Boolean isMissingAolId = CORE_AOL_GlobalWorker.isAolExternalIdParameterMissing(aolExternalId);

        if(isMissingAolId){
            missingParameters.add(CORE_AOL_Constants.PARAM_AOL_EXTERNALID);
        }

        return CORE_AOL_GlobalWorker.getMissingParameters(missingParameters);
    }
}