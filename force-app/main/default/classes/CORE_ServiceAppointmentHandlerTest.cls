@IsTest
public with sharing class CORE_ServiceAppointmentHandlerTest {
    private static final dateTime DT_NOW = DateTime.Now();

    @TestSetup
    static void makeData(){
        CORE_DataFaker_CatalogHierarchy catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test1');
        Account account = CORE_DataFaker_Account.getStudentAccount('test1');

        Opportunity opportunity1 = CORE_DataFaker_Opportunity.getOpportunity(catalogHierarchy.division.Id, 
            catalogHierarchy.businessUnit.Id, 
            catalogHierarchy.studyArea.Id,
            catalogHierarchy.curriculum.Id,
            catalogHierarchy.level.Id,
            catalogHierarchy.intake.Id, 
            account.Id, 
            'Lead', 
            'Lead - New'
        );
        opportunity1.CORE_Lead_Source_External_Id__c = 'test1';

        Opportunity opportunity2 = CORE_DataFaker_Opportunity.getOpportunity(catalogHierarchy.division.Id, 
            catalogHierarchy.businessUnit.Id, 
            catalogHierarchy.studyArea.Id,
            catalogHierarchy.curriculum.Id,
            catalogHierarchy.level.Id,
            catalogHierarchy.intake.Id, 
            account.Id, 
            'Lead', 
            'Lead - New'
        );
        opportunity2.CORE_Lead_Source_External_Id__c = 'test2';

        Opportunity opportunity3 = CORE_DataFaker_Opportunity.getOpportunity(catalogHierarchy.division.Id, 
            catalogHierarchy.businessUnit.Id, 
            catalogHierarchy.studyArea.Id,
            catalogHierarchy.curriculum.Id,
            catalogHierarchy.level.Id,
            catalogHierarchy.intake.Id, 
            account.Id, 
            'Lead', 
            'Lead - New'
        );
        opportunity3.CORE_Lead_Source_External_Id__c = 'test3';

        Update new List<Opportunity>{opportunity1, opportunity2, opportunity3};
    }

    @IsTest
    public static void serviceAppointmentProcess_Should_Delete_Leads_with_eligible_status_and_replace_by_the_associated_opportunity_if_exist() {
        Opportunity opportunity = [SELECT Id, CORE_Lead_Source_External_Id__c FROM Opportunity limit 1];
        Lead lead = CORE_DataFaker_Lead.getLeadWithoutInsert('test1');
        lead.status = 'New';
        Insert lead;

        ServiceAppointment sa = CORE_DataFaker_ServiceAppointment.getServiceAppointmentWithoutInsert(lead.Id, DT_NOW);

        List<ServiceAppointment> serviceAppointments = new List<ServiceAppointment> {sa};

        List<Lead> leads = [SELECT ID FROM Lead];
        System.assertEquals(false, leads.isEmpty());
        System.assertEquals(lead.Id, sa.ParentRecordId);

        Test.startTest();
        sa.CORE_Lead_form_acquisition_unique_key__c = null;
        CORE_ServiceAppointmentTriggerHandler.serviceAppointmentProcess(serviceAppointments);
        leads = [SELECT ID FROM Lead];
        System.assertEquals(lead.Id, sa.ParentRecordId);
        System.assertEquals(false, leads.isEmpty());

        sa.CORE_Lead_form_acquisition_unique_key__c = 'something different';
        CORE_ServiceAppointmentTriggerHandler.serviceAppointmentProcess(serviceAppointments);
        leads = [SELECT ID FROM Lead];
        System.assertEquals(lead.Id, sa.ParentRecordId);
        System.assertEquals(false, leads.isEmpty());

        sa.CORE_Lead_form_acquisition_unique_key__c = opportunity.CORE_Lead_Source_External_Id__c;

        CORE_ServiceAppointmentTriggerHandler.serviceAppointmentProcess(serviceAppointments);
        leads = [SELECT ID FROM Lead];
        System.assertEquals(opportunity.Id, sa.ParentRecordId);
        System.assertEquals(true, leads.isEmpty());

        Test.stopTest();
    }

    @IsTest
    public static void getOpportunitiesWhithExternalIds_Should_Return_a_map_of_opportunity_founds_by_lead_external_id() {
        List<Opportunity> opportunities = [SELECT Id, CORE_Lead_Source_External_Id__c FROM Opportunity];
        Opportunity opportunity1 = opportunities.get(0);
        Opportunity opportunity2 = opportunities.get(1);

        Lead lead = CORE_DataFaker_Lead.getLeadWithoutInsert('test1');
        lead.status = 'New';
        Insert lead;

        ServiceAppointment sa1 = CORE_DataFaker_ServiceAppointment.getServiceAppointmentWithoutInsert(lead.Id, DT_NOW);
        sa1.CORE_Lead_form_acquisition_unique_key__c = opportunity1.CORE_Lead_Source_External_Id__c;

        ServiceAppointment sa2 = CORE_DataFaker_ServiceAppointment.getServiceAppointmentWithoutInsert(lead.Id, DT_NOW);
        sa2.CORE_Lead_form_acquisition_unique_key__c = opportunity2.CORE_Lead_Source_External_Id__c;

        ServiceAppointment sa3 = CORE_DataFaker_ServiceAppointment.getServiceAppointmentWithoutInsert(lead.Id, DT_NOW);
        sa3.CORE_Lead_form_acquisition_unique_key__c = 'another value';

        ServiceAppointment sa4 = CORE_DataFaker_ServiceAppointment.getServiceAppointmentWithoutInsert(lead.Id, DT_NOW);
        sa4.CORE_Lead_form_acquisition_unique_key__c = null;

        List<ServiceAppointment> serviceAppointments = new List<ServiceAppointment> {sa1, sa2, sa3, sa4};

        Test.startTest();
        Map<String, Opportunity> oppsByLeadExternalIds = CORE_ServiceAppointmentTriggerHandler.getOpportunitiesWhithExternalIds(serviceAppointments);
        Test.stopTest();

        System.assertEquals(2, oppsByLeadExternalIds.size());
        System.assertEquals(true, oppsByLeadExternalIds.keySet().contains(opportunity1.CORE_Lead_Source_External_Id__c));
        System.assertEquals(true, oppsByLeadExternalIds.keySet().contains(opportunity2.CORE_Lead_Source_External_Id__c));

        for(String leadExternalId : oppsByLeadExternalIds.keySet()){
            Opportunity oppTmp = oppsByLeadExternalIds.get(leadExternalId);
            System.assert(oppTmp.Id == opportunity1.Id || oppTmp.Id == opportunity2.Id);
        }

    }

    @IsTest
    public static void beforeInsert_should_execute_serviceAppointmentProcess_and_fill_custom_fields_related_to_account_and_opportunity() {
        Opportunity opportunity = [SELECT Id, CORE_OPP_Division__c, CORE_OPP_Business_Unit__c, CORE_OPP_Study_Area__c, CORE_OPP_Curriculum__c, CORE_OPP_Level__c, CORE_OPP_Intake__c, CORE_Lead_Source_External_Id__c FROM Opportunity limit 1];
        Account account = [SELECT Id, FirstName, LastName, Salutation FROM Account limit 1];

        Lead lead = CORE_DataFaker_Lead.getLeadWithoutInsert('test1');
        lead.status = 'New';
        Insert lead;

        ServiceAppointment sa1 = CORE_DataFaker_ServiceAppointment.getServiceAppointmentWithoutInsert(lead.Id, DT_NOW);
        sa1.CORE_Lead_form_acquisition_unique_key__c = opportunity.CORE_Lead_Source_External_Id__c;
        sa1.CORE_Division__c = null;
        sa1.CORE_Business_Unit__c = null; 
        sa1.CORE_Study_Area__c = null;
        sa1.CORE_Curriculum__c = null;
        sa1.CORE_Level__c = null;
        sa1.CORE_Intake__c = null;
        sa1.CORE_Firstname__c = null;
        sa1.CORE_Lastname__c = null;
        sa1.CORE_Salutation__c = null;

        List<ServiceAppointment> serviceAppointments = new List<ServiceAppointment> {sa1};

        Test.startTest();
        CORE_ServiceAppointmentTriggerHandler.beforeInsert(serviceAppointments);
        

        //serviceAppointmentProcess
        List<Lead> leads = [SELECT ID FROM Lead];
        System.assertEquals(opportunity.Id, sa1.ParentRecordId);
        System.assertEquals(true, leads.isEmpty());

        //opportunity fields
        System.assertEquals(opportunity.CORE_OPP_Division__c, sa1.CORE_Division__c);
        System.assertEquals(opportunity.CORE_OPP_Business_Unit__c, sa1.CORE_Business_Unit__c);
        System.assertEquals(opportunity.CORE_OPP_Study_Area__c, sa1.CORE_Study_Area__c);
        System.assertEquals(opportunity.CORE_OPP_Curriculum__c, sa1.CORE_Curriculum__c);
        System.assertEquals(opportunity.CORE_OPP_Level__c, sa1.CORE_Level__c );
        System.assertEquals(opportunity.CORE_OPP_Intake__c, sa1.CORE_Intake__c);

        //account fields
        System.assertEquals(account.FirstName, sa1.CORE_Firstname__c);
        System.assertEquals(account.LastName, sa1.CORE_Lastname__c);
        System.assertEquals(account.Salutation, sa1.CORE_Salutation__c);

        sa1.ParentRecordId = account.Id;
        sa1.CORE_Lead_form_acquisition_unique_key__c = null;

        sa1.CORE_Division__c = null;
        sa1.CORE_Business_Unit__c = null; 
        sa1.CORE_Study_Area__c = null;
        sa1.CORE_Curriculum__c = null;
        sa1.CORE_Level__c = null;
        sa1.CORE_Intake__c = null;
        sa1.CORE_Firstname__c = null;
        sa1.CORE_Lastname__c = null;
        sa1.CORE_Salutation__c = null;
        CORE_ServiceAppointmentTriggerHandler.beforeInsert(serviceAppointments);

        //opportunity fields
        System.assertEquals(null, sa1.CORE_Division__c);
        System.assertEquals(null, sa1.CORE_Business_Unit__c);
        System.assertEquals(null, sa1.CORE_Study_Area__c);
        System.assertEquals(null, sa1.CORE_Curriculum__c);
        System.assertEquals(null, sa1.CORE_Level__c );
        System.assertEquals(null, sa1.CORE_Intake__c);

        //account fields
        System.assertEquals(account.FirstName, sa1.CORE_Firstname__c);
        System.assertEquals(account.LastName, sa1.CORE_Lastname__c);
        System.assertEquals(account.Salutation, sa1.CORE_Salutation__c);
        Test.stopTest();
    }

    @IsTest
    public static void afterDelete_should_delete_eligible_lead() {
        Opportunity opportunity = [SELECT Id, CORE_OPP_Division__c, CORE_OPP_Business_Unit__c, CORE_OPP_Study_Area__c, CORE_OPP_Curriculum__c, CORE_OPP_Level__c, CORE_OPP_Intake__c, CORE_Lead_Source_External_Id__c FROM Opportunity limit 1];

        Lead lead = CORE_DataFaker_Lead.getLeadWithoutInsert('test1');
        lead.status = 'New';

        Lead lead2 = CORE_DataFaker_Lead.getLeadWithoutInsert('test2');
        lead2.status = 'Qualified';
        Insert new List<Lead> {lead,lead2};

        ServiceAppointment sa1 = CORE_DataFaker_ServiceAppointment.getServiceAppointmentWithoutInsert(lead.Id, DT_NOW);
        ServiceAppointment sa2 = CORE_DataFaker_ServiceAppointment.getServiceAppointmentWithoutInsert(opportunity.Id, DT_NOW);
        ServiceAppointment sa3 = CORE_DataFaker_ServiceAppointment.getServiceAppointmentWithoutInsert(lead2.Id, DT_NOW);

        List<ServiceAppointment> oldServiceAppointments = new List<ServiceAppointment> {sa1,sa2, sa3};
        
        Test.startTest();
        insert oldServiceAppointments;
        CORE_ServiceAppointmentTriggerHandler.afterDelete(oldServiceAppointments);
        Test.stopTest();

        List<Lead> leads = [SELECT ID FROM Lead];
        System.assertEquals(1, leads.size());
        System.assertEquals(lead2.Id, leads.get(0).Id);

    }

    @IsTest
    public static void getAccountPhone_should_return_phone_of_account(){
        Account account = [SELECT Id, FirstName, LastName, Salutation FROM Account limit 1];
        account.Phone = null;
        account.CORE_Phone2__pc = null;
        account.PersonMobilePhone = null;
        account.CORE_Mobile_2__pc = null;
        account.CORE_Mobile_3__pc = null;

        Test.startTest();
        String result = CORE_ServiceAppointmentTriggerHandler.getAccountPhone(account);
        System.assertEquals('', result);
        
        account.CORE_Mobile_3__pc = '+33600000001';
        result = CORE_ServiceAppointmentTriggerHandler.getAccountPhone(account);
        System.assertEquals(account.CORE_Mobile_3__pc, result);

        account.CORE_Mobile_2__pc = '+33600000002';
        result = CORE_ServiceAppointmentTriggerHandler.getAccountPhone(account);
        System.assertEquals(account.CORE_Mobile_2__pc, result);
        
        account.PersonMobilePhone = '+33600000003';
        result = CORE_ServiceAppointmentTriggerHandler.getAccountPhone(account);
        System.assertEquals(account.PersonMobilePhone, result);

        account.CORE_Phone2__pc = '+33600000004';
        result = CORE_ServiceAppointmentTriggerHandler.getAccountPhone(account);
        System.assertEquals(account.CORE_Phone2__pc, result);

        account.Phone = '+33600000005';
        result = CORE_ServiceAppointmentTriggerHandler.getAccountPhone(account);
        System.assertEquals(account.Phone, result);
        Test.stopTest();
    }

    @IsTest
    public static void getAccountEmail_should_return_email_of_account(){
        Account account = [SELECT Id, FirstName, LastName, Salutation FROM Account limit 1];
        account.PersonEmail = null;
        account.CORE_Email_2__pc = null;
        account.CORE_Email_3__pc = null;
        account.CORE_Student_Email__pc = null;

        Test.startTest();
        String result = CORE_ServiceAppointmentTriggerHandler.getAccountEmail(account);
        System.assertEquals('', result);
        
        account.CORE_Student_Email__pc = 'test.email1@live.fr';
        result = CORE_ServiceAppointmentTriggerHandler.getAccountEmail(account);
        System.assertEquals(account.CORE_Student_Email__pc, result);

        account.CORE_Email_3__pc = 'test.email1@live.fr';
        result = CORE_ServiceAppointmentTriggerHandler.getAccountEmail(account);
        System.assertEquals(account.CORE_Email_3__pc, result);
        
        account.CORE_Email_2__pc = 'test.email1@live.fr';
        result = CORE_ServiceAppointmentTriggerHandler.getAccountEmail(account);
        System.assertEquals(account.CORE_Email_2__pc, result);

        account.PersonEmail = 'test.email1@live.fr';
        result = CORE_ServiceAppointmentTriggerHandler.getAccountEmail(account);
        System.assertEquals(account.PersonEmail, result);

        Test.stopTest();
    }
}