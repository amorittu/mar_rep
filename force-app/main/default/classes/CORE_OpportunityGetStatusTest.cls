@IsTest
public class CORE_OpportunityGetStatusTest {
    @IsTest
    public static void getStatusFromLead_Should_return_a_list_of_8_Status(){
        List<string> startStatus = new List<String>{'Lead'};
        List<List<String>> status = CORE_OpportunityGetStatus.getNextStatus(startStatus);
        System.assertEquals(8,status.get(0).size(),'Sould be 8 status');
    }

    @IsTest
    public static void getStatusFromAdmitted_Should_return_a_list_of_6_Status(){
        List<string> startStatus = new List<String>{'Admitted'};
        List<List<String>> status = CORE_OpportunityGetStatus.getNextStatus(startStatus);
        System.assertEquals(6,status.get(0).size(),'Sould be 6 status');
    }
    
    @IsTest
    public static void getStatusFromCloseLost_Should_return_a_list_of_1_Status(){
        List<string> startStatus = new List<String>{'Closed Lost'};
        List<List<String>> status = CORE_OpportunityGetStatus.getNextStatus(startStatus);
        System.assertEquals(1,status.get(0).size(),'Sould be 1 status');
    }
}