public with sharing class CORE_SobjectUtils {
    public static final String OBJ_ACCOUNT = 'ACCOUNT';
    public static final String OBJ_OPPORTUNITY = 'OPPORTUNITY';
    public static final String OBJ_LEAD = 'LEAD';
    public static final String OBJ_TASK = 'TASK';
    public static final String OBJ_UNKNOWN = 'UNKNOWN';


    public static String getSobjectType(Id objectId) {
        String sObjectType = 'UNKNOWN';

        if(objectId != null){
            sObjectType = objectId.getSObjectType().getDescribe().getName().toUppercase();
        }
        
        return sObjectType;
    }

    public static Schema.SObjectField getSObjectField(String objectApiName, String fieldName) {
        return ((SObject)Type.forName('Schema',objectApiName).newInstance())
            .getSObjectType()
            .getDescribe(SObjectDescribeOptions.DEFERRED)
            .fields
            .getMap()
            .get(fieldName);
    }

    public static Boolean isOpportunityId(Id objectId){
        return getSobjectType(objectId) == OBJ_OPPORTUNITY;
    }
    
    public static Boolean isLeadId(Id objectId){
        return getSobjectType(objectId) == OBJ_LEAD;
    }

    public static Boolean isAccountId(Id objectId){
        return getSobjectType(objectId) == OBJ_ACCOUNT;
    }

    public static Boolean doesFieldExist(String objName, string fieldName)
    {
        try {
            SObject so = Schema.getGlobalDescribe().get(objName).newSObject();
            return so.getSobjectType().getDescribe().fields.getMap().containsKey(fieldName);
        }
        catch(Exception ex) {}
         
        return false;
    }
    
}