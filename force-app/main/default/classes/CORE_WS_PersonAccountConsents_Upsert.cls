@RestResource(urlMapping='/core/person-account/upsert/consents')
global class CORE_WS_PersonAccountConsents_Upsert {

    public static Integer limitPerPage = 2000;
    private static Set<String> mandatoryParams = new Set<String>{ 'CORE_Consent_type__c', 'CORE_Consent_channel__c', 'CORE_Business_Unit__c', 'CORE_Scope__c' };

    @HttpPut
    global static CORE_WS_Wrapper.ResultWrapperConsents upsertPAConsents(){

        RestRequest	request = RestContext.request;
        system.debug('@upsertPAConsents > request: ' + request);
        String paId = request.params.get(CORE_WS_Constants.PARAM_PA_ID);
        Map<String, Object> paramMap = (Map<String, Object>)JSON.deserializeUntyped(request.requestBody.toString());
        List<Object> paramsList = (List<Object>)JSON.deserializeUntyped((String)JSON.serialize(paramMap.get('consents')));
        CORE_WS_Wrapper.ResultWrapperConsents result = checkMandatoryParameters(paId, paramsList);
        system.debug('@upsertPAConsents > result: ' + result);
        if(result != null){
            return result;
        }

        try{
            List<CORE_Consent__c> consentsUpsertList = new List<CORE_Consent__c>();
            List<CORE_Consent__c> consentsList = CORE_WS_QueryProvider.getConsentsListByPersonAccountId(paId, null, limitPerPage);

            Map<String, CORE_Consent__c> consentsMap = new Map<String, CORE_Consent__c>();

            for(CORE_Consent__c consent : consentsList){
                String key = consent.CORE_Scope__c + '_' + consent.CORE_Consent_type__c + '_' + consent.CORE_Consent_channel__c;
                consentsMap.put(key, consent);
            }

            system.debug('@upsertPAConsents > consentsMap: ' + consentsMap);

            for(Object paramObj : paramsList){
                Map<String, Object> paramsMap = (Map<String, Object>)JSON.deserializeUntyped((String)JSON.serialize(paramObj));
                String key = (String)paramsMap.get('CORE_Scope__c') + '_' + (String)paramsMap.get('CORE_Consent_type__c') + '_' + (String)paramsMap.get('CORE_Consent_channel__c');
                system.debug('@upsertPAConsents > key: ' + key);
                CORE_Consent__c consent = new CORE_Consent__c();
                system.debug('@upsertPAConsents > consent: ' + consent);

                consent.Id = consentsMap.containsKey(key) ? consentsMap.get(key).Id :null;

                if(!consentsMap.containsKey(key)){
                    consent.CORE_Person_Account__c = paId;
                }

                consent = (paramsMap.containsKey('CORE_Consent_type__c')) ? (CORE_Consent__c)CORE_WS_GlobalWorker.assignValue(consent, 'CORE_Consent_type__c', paramsMap.get('CORE_Consent_type__c')) :consent;
                consent = (paramsMap.containsKey('CORE_Consent_channel__c')) ? (CORE_Consent__c)CORE_WS_GlobalWorker.assignValue(consent, 'CORE_Consent_channel__c', paramsMap.get('CORE_Consent_channel__c')) :consent;
                consent = (paramsMap.containsKey('CORE_Consent_description__c')) ? (CORE_Consent__c)CORE_WS_GlobalWorker.assignValue(consent, 'CORE_Consent_description__c', paramsMap.get('CORE_Consent_description__c')) :consent;
                consent = (paramsMap.containsKey('CORE_Opt_in__c')) ? (CORE_Consent__c)CORE_WS_GlobalWorker.assignValue(consent, 'CORE_Opt_in__c', paramsMap.get('CORE_Opt_in__c')) :consent;
                consent = (paramsMap.containsKey('CORE_Opt_in_date__c')) ? (CORE_Consent__c)CORE_WS_GlobalWorker.assignValue(consent, 'CORE_Opt_in_date__c', paramsMap.get('CORE_Opt_in_date__c')) :consent;
                consent = (paramsMap.containsKey('CORE_Opt_out__c')) ? (CORE_Consent__c)CORE_WS_GlobalWorker.assignValue(consent, 'CORE_Opt_out__c', paramsMap.get('CORE_Opt_out__c')) :consent;
                consent = (paramsMap.containsKey('CORE_Opt_out_date__c')) ? (CORE_Consent__c)CORE_WS_GlobalWorker.assignValue(consent, 'CORE_Opt_out_date__c', paramsMap.get('CORE_Opt_out_date__c')) :consent;
                consent = (paramsMap.containsKey('CORE_Double_Opt_in_date__c')) ? (CORE_Consent__c)CORE_WS_GlobalWorker.assignValue(consent, 'CORE_Double_Opt_in_date__c', paramsMap.get('CORE_Double_Opt_in_date__c')) :consent;

                if(paramsMap.containsKey('CORE_Business_Unit__c')
                   && paramsMap.get('CORE_Business_Unit__c') != ''
                   && paramsMap.get('CORE_Business_Unit__c') != null){

                    CORE_Business_Unit__c bu = new CORE_Business_Unit__c(
                        CORE_Business_Unit_ExternalId__c = (String)paramsMap.get('CORE_Business_Unit__c')
                    );

                    consent.CORE_Business_Unit__r =  bu;
                }

                if(paramsMap.containsKey('CORE_Division__c')
                   && paramsMap.get('CORE_Division__c') != ''
                   && paramsMap.get('CORE_Division__c') != null){

                    CORE_Division__c division = new CORE_Division__c(
                        CORE_Division_ExternalId__c = (String)paramsMap.get('CORE_Division__c')
                    );

                    consent.CORE_Division__r =  division;
                }

                consent.CORE_Scope__c = paramsMap.containsKey('CORE_Scope__c') ? (String)paramsMap.get('CORE_Scope__c') :consent.CORE_Consent_type__c;

                consentsUpsertList.add(consent);
            }

            system.debug('@upsertPAConsents > consentsUpsertList: ' + JSON.serializePretty(consentsUpsertList));

            database.upsert(consentsUpsertList, true);

            result = new CORE_WS_Wrapper.ResultWrapperConsents(consentsUpsertList.size(), null, consentsUpsertList, CORE_WS_Constants.SUCCESS_STATUS);

        } catch(Exception e){
            system.debug('@upsertPAConsents > Exception: ' + e.getMessage() + ' at ' + e.getStackTraceString());
            result = new CORE_WS_Wrapper.ResultWrapperConsents(CORE_WS_Constants.ERROR_STATUS, e.getMessage() + ' at ' + e.getStackTraceString());
        }

        return result;
    }

    private static CORE_WS_Wrapper.ResultWrapperConsents checkMandatoryParameters(String paId, List<Object> paramsList){
        List<String> missingParameters = new List<String>();

        if(CORE_WS_GlobalWorker.isParameterMissing(paId)){
            missingParameters.add(CORE_WS_Constants.PARAM_PA_ID);
        }

        for(Object paramObj : paramsList){
            Map<String, Object> paramsMap = (Map<String, Object>)JSON.deserializeUntyped((String)JSON.serialize(paramObj));

            for(String f : mandatoryParams){
                if(paramsMap.containsKey(f)
                   && paramsMap.get(f) != null
                   && paramsMap.get(f) != ''){

                    continue;
                }
                else{
                    missingParameters.add(f);
                }
            }
        }

        return getMissingParameters(missingParameters);
    }

    public static CORE_WS_Wrapper.ResultWrapperConsents getMissingParameters(List<String> missingParameters){
        String errorMsg = CORE_WS_Constants.MSG_MISSING_PARAMETERS;

        if(missingParameters.size() > 0){
            errorMsg += missingParameters;

            return new CORE_WS_Wrapper.ResultWrapperConsents(CORE_WS_Constants.ERROR_STATUS, errorMsg);
        }

        return null;
    }
}