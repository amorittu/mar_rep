/**
 * Created by ftrapani on 26/10/2021.
 * Test class: IT_InvocableNextBusinessHoursTest
 */

global class IT_InvocableNextBusinessHours {

    public static final Long ONE_HOUR_MILLISECOND = 3600000; // CONSTANT --> 1H = 3600000 ms

    @InvocableMethod
    global static List<FlowOutputs> getNextBH(List<FlowInputs> request){
        // Get the default business hours
		
      
System.debug(request[0].targetTime);
System.debug(request[0].businessHoursId);
System.debug(request[0].hours);
        
        Integer inputHours = 0;
        if(request[0].hours!=null){
            inputHours = request[0].hours;
        }

        Datetime    startDT               = request[0].targetTime;
        Long        endTimeMillisecond    = inputHours * ONE_HOUR_MILLISECOND; 
        DateTime checker = BusinessHours.nextStartDate(request[0].businessHoursId, request[0].targetTime  );
        System.debug('### checker : ' + checker);

        Datetime    finalNextDateTime     = BusinessHours.addGMT(request[0].businessHoursId, checker, endTimeMillisecond);
        System.debug('### finalNextDateTimeis : ' + finalNextDateTime);
        System.debug('### finalNextDateTimeGTMT is : ' + finalNextDateTime.timeGmt());

        FlowOutputs flOutput= new FlowOutputs();
        flOutput.nextDateTime = finalNextDateTime;

        List<FlowOutputs> returnList = new List<FlowOutputs>();
        returnList.add(flOutput);

        return returnList;

    }
    global class FlowInputs{

        @InvocableVariable (required=true)
        global Datetime targetTime;

        @InvocableVariable (required=true)
        global Id businessHoursId;

        @InvocableVariable
        global Integer hours;

    }

    global class FlowOutputs{

        @InvocableVariable
        global Datetime nextDateTime;

    }

}