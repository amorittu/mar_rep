/**
 * Created by SaverioTrovatoNigith on 19/04/2022.
 */

@IsTest
private class IT_PicklistValuesControllerTest {
	@IsTest
	static void testBehavior() {
        Test.startTest();
        IT_PicklistValuesController.getFieldDependencies('Opportunity','StageName','CORE_OPP_Sub_Status__c');
        IT_PicklistValuesController.getWrapperToDisplay('Opportunity','StageName','CORE_OPP_Sub_Status__c');
        IT_PicklistValuesController.getWrapperToDisplayJSON('Opportunity','StageName','CORE_OPP_Sub_Status__c');
        IT_PicklistValuesController.getPickListValuesIntoList('Opportunity','StageName');
        Test.stopTest();
	}
}