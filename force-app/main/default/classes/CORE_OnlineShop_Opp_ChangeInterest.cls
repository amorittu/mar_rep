@RestResource(urlMapping='/online-shop/opportunity/interest')
global class CORE_OnlineShop_Opp_ChangeInterest  {

	@HttpPut
    global static CORE_OnlineShop_Wrapper.ResultWrapperOpportunity changeInterest(){

        RestRequest	request = RestContext.request;
        CORE_OnlineShop_Wrapper.ResultWrapperOpportunity result = null;

        try{
            CORE_CountrySpecificSettings__c countrySetting = CORE_CountrySpecificSettings__c.getOrgDefaults();

            Map<String, Object> paramsMap = (Map<String, Object>)JSON.deserializeUntyped(request.requestBody.toString());

            String onlineShopId = (String)paramsMap.get(CORE_OnlineShop_Constants.PARAM_ONLINESHOP_ID);

            result = checkMandatoryParameters(onlineShopId);

            if(result != null){
                return result;
            }

            Opportunity opp = CORE_OnlineShop_QueryProvider.getOpportunity(onlineShopId);
            //system.debug('@updateOpportunity > oppsList GET: ' + JSON.serializePretty(oppsList[0]));

            CORE_OnlineShop_Wrapper.InterestWrapper interest = (CORE_OnlineShop_Wrapper.InterestWrapper)JSON.deserialize((String)JSON.serialize(paramsMap.get('interest')), CORE_OnlineShop_Wrapper.InterestWrapper.class);

			result = changeInterest(interest, opp);

        } catch(Exception e){
            system.debug('@getOpportunities > exception error: ' + e.getMessage() + ' at ' + e.getStackTraceString());
            result = new CORE_OnlineShop_Wrapper.ResultWrapperOpportunity(CORE_OnlineShop_Constants.ERROR_STATUS, e.getMessage() + ' at ' + e.getStackTraceString());
        }

        return result;
    }

    private static CORE_OnlineShop_Wrapper.ResultWrapperOpportunity checkMandatoryParameters(String onlineShopId){
        List<String> missingParameters = new List<String>();
        Boolean isMissingId = CORE_SIS_GlobalWorker.isParameterMissing(onlineShopId);

        if(isMissingId){
            missingParameters.add(CORE_OnlineShop_Constants.PARAM_ONLINESHOP_ID);
        }
        
        return getMissingParameters(missingParameters);
    }

    public static CORE_OnlineShop_Wrapper.ResultWrapperOpportunity getMissingParameters(List<String> missingParameters){
        String errorMsg = CORE_OnlineShop_Constants.MSG_MISSING_PARAMETERS;

        if(missingParameters.size() > 0){
            errorMsg += missingParameters;

            return new CORE_OnlineShop_Wrapper.ResultWrapperOpportunity(CORE_OnlineShop_Constants.ERROR_STATUS, errorMsg);
        }
        
        return null;
    }

	public static CORE_OnlineShop_Wrapper.ResultWrapperOpportunity changeInterest(CORE_OnlineShop_Wrapper.InterestWrapper interest, Opportunity opportunity){
		CORE_OpportunityDataMaker dataMaker = new CORE_OpportunityDataMaker();
        CORE_OnlineShop_Wrapper.ResultWrapperOpportunity result = null;

        CORE_CatalogHierarchyModel catalogHierarchy = getCatalogHierarchy(interest);

        /*if(catalogHierarchy.businessUnit.Id != opportunity.CORE_OPP_Business_Unit__c){
            result.statut = CORE_OnlineShop_Constants.ERROR_STATUS;
            //result.message = CORE_AOL_Constants.MSG_OPPORTUNITY_CHANGE_INTEREST_DIFFERENT_BU;
            
            return result;
        }*/

        CORE_Intake__c intake = CORE_OnlineShop_QueryProvider.getIntakeFromExternalId(interest.intake);

        if(intake == null){
            throw new IllegalArgumentException('Intake not found.');
        }

        //same opp
        if(catalogHierarchy.studyArea.Id == opportunity.CORE_OPP_Study_Area__c 
            && catalogHierarchy.curriculum.Id == opportunity.CORE_OPP_Curriculum__c
            && catalogHierarchy.level.Id == opportunity.CORE_OPP_Level__c
            && catalogHierarchy.intake.Id == opportunity.CORE_OPP_Intake__c
        ){
            //result.statut = CORE_OnlineShop_Constants.SUCCESS_STATUS;
            //result.message = CORE_AOL_Constants.MSG_OPPORTUNITY_CHANGE_INTEREST_ALREADY_EXIST;
            //result.opportunity = new CORE_AOL_Wrapper.OpportunityWrapper(opportunity);

            return result;
        }

        Id oldProductId = opportunity.CORE_Main_Product_Interest__c;

        //CHANGE INTEREST
        opportunity = dataMaker.initOpportunityCatalog(opportunity, catalogHierarchy);

        //remove old line item
        if(!String.isBlank(oldProductId)){
            List<OpportunityLineItem> oppLineItemsToDelete = CORE_OnlineShop_QueryProvider.getOppLineItems(opportunity.Id, oldProductId);

            if(!oppLineItemsToDelete.isEmpty()){
                Database.delete(oppLineItemsToDelete, true);
            }
        }
        
        //insert new line item
        getOppLineItem(datamaker, opportunity, false, interest);

		result = new CORE_OnlineShop_Wrapper.ResultWrapperOpportunity(opportunity, CORE_OnlineShop_Constants.SUCCESS_STATUS);

        return result;
    }

	public static CORE_CatalogHierarchyModel getCatalogHierarchy(CORE_OnlineShop_Wrapper.InterestWrapper interest){
        CORE_CatalogHierarchyModel catalogHierarchy = new CORE_CatalogHierarchyModel(
            null, 
            interest.businessUnit, 
            interest.studyArea,
            interest.curriculum,
            interest.level,
            interest.intake
        );

        catalogHierarchy = CORE_OpportunityDataMaker.getCatalogHierarchy(catalogHierarchy, true);

        return catalogHierarchy;
    }

    public static OpportunityLineItem getOppLineItem(CORE_OpportunityDataMaker dataMaker, Opportunity opportunity, 
	                                                 Boolean checkExistingLineItems, CORE_OnlineShop_Wrapper.InterestWrapper interest){
        OpportunityLineItem oppLineItem;

        Map<Id,Id> buByProducts = new Map<Id,Id>();

        //no need to continue because there is no products
        if(String.IsBlank(opportunity.CORE_Main_Product_Interest__c)){
            return oppLineItem;
        }

        if(checkExistingLineItems){
            List<OpportunityLineItem> existingLineItems = CORE_OnlineShop_QueryProvider.getOppLineItems(opportunity.Id, opportunity.CORE_Main_Product_Interest__c);

            if(!existingLineItems.isEmpty()){
                return oppLineItem;
            }
        }

        if(!String.IsBlank(opportunity.CORE_Main_Product_Interest__c) 
		   && !String.IsBlank(opportunity.CORE_OPP_Business_Unit__c)){

            buByProducts.put(opportunity.CORE_Main_Product_Interest__c, opportunity.CORE_OPP_Business_Unit__c);
        }

        Map<Id, PricebookEntry> pricebookEntries = dataMaker.getPricebookEntryByProducts(buByProducts);

        PricebookEntry pricebookEntry = pricebookEntries.get(opportunity.CORE_Main_Product_Interest__c);

        if(pricebookEntry != null){
            oppLineItem = dataMaker.initOppLineItem(opportunity, pricebookEntry);
			oppLineItem.Quantity = interest.quantity;
			oppLineItem.UnitPrice = interest.salesPrice;
        }

        if(oppLineItem != null){
            Database.insert(oppLineItem, true);
        }
        
        return oppLineItem;
    }
}