@IsTest
public class CORE_DataFaker_PriceBook {

    public static Id getStandardPricebookId(){
        Id pricebookId = Test.getStandardPricebookId();

        return pricebookId;
    }

    public static PriceBook2 getPricebook(String uniqueKey) {
        String name = 'Price Book - ' + uniqueKey;
        Pricebook2 priceBook = new Pricebook2(
            Name = name,
            Description = 'This is the ' + name + ' description.',
            IsActive = true
        );

        insert priceBook;

        return priceBook;
    }

    public static PricebookEntry getPriceBookEntry(Id productId, Id pricebookId, Decimal unitPrice){
        PricebookEntry pricebookEntry = new PricebookEntry(
            Pricebook2Id = pricebookId,
            Product2Id = productId,
            UnitPrice = unitPrice,
            IsActive = true
        );

        insert pricebookEntry;

        return pricebookEntry;
    }

    public static CORE_PriceBook_BU__c getPriceBookBu(Id pricebookId, Id businessUnitId, String uniqueKey){
        CORE_PriceBook_BU__c pricebookBu = getPriceBookBuWithoutInsert(pricebookId,businessUnitId,uniqueKey);

        insert pricebookBu;

        return pricebookBu;
    }

    public static CORE_PriceBook_BU__c getPriceBookBuWithoutInsert(Id pricebookId, Id businessUnitId, String uniqueKey){
        CORE_PriceBook_BU__c pricebookBu = new CORE_PriceBook_BU__c(
            CORE_Is_default__c = true,
            CORE_Price_Book__c = pricebookId,
            CORE_Business_Unit__c = businessUnitId,
            CORE_External_Id__c = uniqueKey,
            CORE_Selling_Business_Unit__c = getSellingBu(uniqueKey).Id,
            CORE_Academic_Year__c = getAcademicYear()
        );

        return pricebookBu;
    }

    public static CORE_Selling_Business_Unit__c getSellingBu(String uniqueKey){
        CORE_Selling_Business_Unit__c sellingBu = new CORE_Selling_Business_Unit__c(
            CORE_Selling_Business_Unit_External_ID__c = uniqueKey,
            Name = uniqueKey
        );

        insert sellingBu;

        return sellingBu;
    }

    private static String getAcademicYear(){
        CORE_Opportunity_creation_setting__mdt oppCreationSettings = CORE_Opportunity_creation_setting__mdt.getInstance('Default_values');
        String defaultAcademicYear = oppCreationSettings?.CORE_Default_AY_for_Manual_OPP_Creation__c;
        if(!String.isEmpty(defaultAcademicYear)){
            return defaultAcademicYear;
        }

        List<String> allAcademicYears = CORE_PicklistUtils.getPicklistValues('Opportunity', 'CORE_OPP_Academic_Year__c');
        String currentAcademicYear = CORE_OpportunityUtils.getAcademicYear(
            defaultAcademicYear,
            null, 
            null, 
            System.now(), 
            allAcademicYears
        );

        return currentAcademicYear;
    }
}