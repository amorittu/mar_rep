/**
* @author Fodil BOUDJEDIEN - Almavia
* @date 2022-03-24
* @group 
* @description Class used by Flow, coreMassOppClosing, to Reopen Opportunities in mass
* @test class 
*/
public without sharing class CORE_MassOpportunityReopeningHandler {
    public static String OPP_REREGISTRATION_RECORDTYPE_DEVNAME = 'CORE_Re_registration';

    /* get the next academic year of the initial academic Year (API NAME picklist value) */
    public static String getNextAcademicYear(String initialAcademicYear){
        String nextAcademicYear = null;
        List<String> allAcademicYears = CORE_PicklistUtils.getPicklistValues('Opportunity', 'CORE_OPP_Academic_Year__c');
        String endYear = initialAcademicYear.substringAfterLast('-');
        for(String pickval : allAcademicYears){
            String startVal = pickval.substringBeforeLast('-');
            if((startVal).contains(endYear)){ // means that the next Acadelic Year start with the end year of the precedent one.
                nextAcademicYear = pickval;
                break;
            }
        }
        return nextAcademicYear;
    }

    // create a reregistration opportunities from a list of opportunities having the same business unit and academic year
    public static List<Opportunity> handleOpportunitiesReregistration(String academicYear,List<Opportunity> opportunities, Id businessUnitId){
        Map<Id,CORE_Point_of_Contact__c> mapOpportunityIdToPointOFContact = new Map<Id,CORE_Point_of_Contact__c>();
        Id reregistrationRT = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get(CORE_MassOpportunityReopeningHandler.OPP_REREGISTRATION_RECORDTYPE_DEVNAME).getRecordTypeId();
        List<String> usedRecordTypes = new List<String>{'Continuum of action',''};
        Map<Id,Opportunity> mapEnrolledOppTonewOpp= new Map<Id,Opportunity>();
        List<CORE_Point_of_Contact__c> pointsOfContact = new List<CORE_Point_of_Contact__c>(); // pointsOfContact to be created
        List<Opportunity> enrolledOpportunities= (List<Opportunity>) opportunities;
        Map<Id,List<Id>> mapAccountIdToOpportunities= new Map<Id,List<Id>>();// map account Id to the ist of the opps Ids related to It
        
        for(Opportunity enrolledOpp : enrolledOpportunities){
            Account acct = new Account(id=enrolledOpp.AccountId);
            System.debug('@handleOpportunitiesReregistration > id Opportunity '+enrolledOpp.Id);
            System.debug('@handleOpportunitiesReregistration > CORE Business Unit '+enrolledOpp.CORE_OPP_Business_Unit__r.CORE_BU_Technical_User__c);
            CORE_OpportunityDataMaker dataMaker = new CORE_OpportunityDataMaker();
            CORE_CatalogHierarchyModel hierarchyModel = new CORE_CatalogHierarchyModel();
            hierarchyModel.division = new CORE_Division__c (id=enrolledOpp.CORE_OPP_Division__c);
            hierarchyModel.businessUnit = new CORE_Business_Unit__c(id=enrolledOpp.CORE_OPP_Business_Unit__c);
            Opportunity newOpp = dataMaker.initOpportunity(acct,CORE_MassOpportunityReopeningHandler.getNextAcademicYear(enrolledOpp.CORE_OPP_Academic_Year__c),hierarchyModel,false);           
            newOpp.RecordTypeId=reregistrationRT;
            // assign the business Unit
            newOpp.OwnerId = enrolledOpp.CORE_OPP_Business_Unit__r.CORE_BU_Technical_User__c;

            // if opp eligible for re-renrollement 
            if (enrolledOpp.CORE_Not_eligible_for_re_registration__c == true){
                newOpp.CORE_Not_eligible_for_re_registration__c=true;
                newOpp.CORE_Not_eligible_reason__c = enrolledOpp.CORE_Not_eligible_reason__c ;
            }
            if(enrolledOpp.CORE_Failed_year__c){
                // if it failed years
                newOpp.CORE_Is_a_repeating_student__c = true;
                newOpp.StageName = 'Up';
                newOpp.CORE_OPP_Sub_Status__c = 'Up - New';
                newOpp.CORE_OPP_Level__c = enrolledOpp.CORE_OPP_Level__c;
                newOpp.CORE_OPP_Study_Area__c = enrolledOpp.CORE_OPP_Study_Area__c;
                newOpp.CORE_OPP_Curriculum__c = enrolledOpp.CORE_OPP_Curriculum__c;
                newOpp.CORE_OPP_Intake__c = enrolledOpp.CORE_OPP_Intake__c;
                newOpp.CORE_Main_Product_Interest__c = enrolledOpp.CORE_Main_Product_Interest__c;
                newOpp.Pricebook2Id = enrolledOpp.Pricebook2Id;

            }
            else{
                if(enrolledOpp.CORE_OPP_Level__r.CORE_Next_level_for_re_enrollement__c == null){// next level doesn'tvexist
                List<String> levelFilters = new List<String>{'CORE_PKL_M2','CORE_PKL_Doctorat','CORE_PKL_BTS_2','CORE_PKL_Not_Classifiable'};
                    if( levelFilters.contains(enrolledOpp.CORE_OPP_Level__r.CORE_Year__c)){ // level of M1, Doctorat, BTS 2 , Not Classified 
                        newOpp.StageName = 'OutGoing';
                        newOpp.CORE_OPP_Sub_Status__c = 'Outgoing (upselling) - New';
                        system.debug('@handleOpportunitiesReregistration > Point of contact '+enrolledOpp.Point_of_contact__r+' size : '+enrolledOpp.Point_of_contact__r.size());
                        
                    }else { // Other level
                        newOpp.StageName = 'Up';
                        newOpp.CORE_OPP_Sub_Status__c = 'Up - New';
                        newOpp.CORE_OPP_Study_Area__c = enrolledOpp.CORE_OPP_Study_Area__c;
                        newOpp.CORE_OPP_Curriculum__c = enrolledOpp.CORE_OPP_Curriculum__c;
                    }
                }else {// next level exist
                    newOpp.StageName = 'Up';
                    newOpp.CORE_OPP_Sub_Status__c = 'Up - New';
                    newOpp.CORE_OPP_Study_Area__c = enrolledOpp.CORE_OPP_Level__r?.CORE_Next_level_for_re_enrollement__r?.CORE_Parent_Curriculum__r.CORE_Parent_Study_Area__c;
                    newOpp.CORE_OPP_Curriculum__c = enrolledOpp.CORE_OPP_Level__r?.CORE_Next_level_for_re_enrollement__r?.CORE_Parent_Curriculum__c;
                    newOpp.CORE_OPP_Level__c = enrolledOpp.CORE_OPP_Level__r?.CORE_Next_level_for_re_enrollement__c;
                }
            }

            if(enrolledOpp.Point_of_contact__r != null && enrolledOpp.Point_of_contact__r.size() != 0){

                mapOpportunityIdToPointOFContact.put(enrolledOpp.Id,enrolledOpp.Point_of_contact__r[0]);
            }
            mapEnrolledOppTonewOpp.put(enrolledOpp.Id,newOpp);

            // put the opps into the map
            if(!mapAccountIdToOpportunities.containsKey(enrolledOpp.AccountId)) {
                mapAccountIdToOpportunities.put(enrolledOpp.AccountId,new List<Id>{enrolledOpp.Id});
            }
            else {
                mapAccountIdToOpportunities.get(enrolledOpp.AccountId).add(enrolledOpp.Id);
            }
        }
        
        List<Opportunity> opportunitiesOfnextYear = [SELECT id,AccountId,CORE_OPP_Academic_Year__c FROM Opportunity where AccountId in :mapAccountIdToOpportunities.keySet() AND CORE_OPP_Academic_Year__c = :CORE_MassOpportunityReopeningHandler.getNextAcademicYear(academicYear) AND CORE_OPP_Business_Unit__c = :businessUnitId and CORE_OPP_Main_Opportunity__c = true ];

        // inserting new Opportunity

        for(Opportunity opp: opportunitiesOfnextYear){
            if(mapAccountIdToOpportunities.containsKey(opp.AccountId)){
                for(ID oppId : mapAccountIdToOpportunities.get(opp.AccountId)){
                    mapEnrolledOppTonewOpp.get(oppId).CORE_OPP_Main_Opportunity__c = true;
                }
            }
        }
        insert mapEnrolledOppTonewOpp.values();

        // update the reregistered field inside the opportunity
        for (Opportunity opp : enrolledOpportunities){
            opp.CORE_Re_registered_opportunity__c=mapEnrolledOppTonewOpp.get(opp.Id).Id;
        }
        update enrolledOpportunities;
    
        // points of Contact cloned and associated to the new Opportunity
        for (Id oppKey : mapEnrolledOppTonewOpp.keySet()){
            system.debug('@handleOpportunitiesReregistration > oppKey '+oppKey);
            system.debug('@handleOpportunitiesReregistration > key set '+mapOpportunityIdToPointOfContact.keyset());

            if(mapOpportunityIdToPointOfContact.containsKey(oppKey)){
                system.debug('@handleOpportunitiesReregistration > exist debug');
                CORE_Point_of_Contact__c pointOfContactTmp = mapOpportunityIdToPointOfContact.get(oppKey).clone(false, false, false, false);
                pointOfContactTmp.Id = null;
                pointOfContactTmp.CORE_Opportunity__c = mapEnrolledOppTonewOpp.get(oppKey).Id;
                pointOfContactTmp.CORE_External_Id__c = null;
                pointOfContactTmp.CORE_IsDuplicated__c = true;
                pointsOfContact.add(pointOfContactTmp);
                system.debug('@handleOpportunitiesReregistration > point of Contact : '+pointOfContactTmp.CORE_Opportunity__c);

            }

              //  mapOpportunityIdToPointOfContact.get(oppKey)

            //pointsOfContact.addAll(CORE_OpportunityUtils.getDuplicatedPointOfContacts(new List<CORE_Point_of_Contact__c>{mapOpportunityIdToPointOfContact.get(oppKey)},mapEnrolledOppTonewOpp.get(oppKey).Id));
        }
        insert pointsOfContact;
        return mapEnrolledOppTonewOpp.values();
    }


    // Get the query of Opportunities to be re-registered including businessUnitId, Academic year and also if there is any further conditions typically got from list view
    public static String getMassOpportunityReregistrationQuery(String businessUnitId,String academicYear,String listviewWhereConditions){

        String acceptableStatuses = '(\'Registered\',\'Enrolled\',\'Re-enrolled\',\'Re-registered\',\'Closed Won\',\'Year Out\')';
        List<String> pointOFContactfields = new List<String>(Schema.getGlobalDescribe().get('CORE_Point_of_Contact__c').getDescribe().fields.getMap().keySet());
        String pocFieldsString = String.join(pointOFContactfields, ',');
        String query='SELECT Id,Name,OwnerId,CORE_OPP_Main_Opportunity__c,RecordTypeId,CORE_OPP_Study_Area__c,CORE_OPP_Curriculum__c, CORE_OPP_Business_Unit__c,CORE_OPP_Business_Unit__r.CORE_BU_Technical_User__c,CORE_Main_Product_Interest__c,CORE_OPP_Intake__c,CORE_OPP_Division__c,CORE_OPP_Academic_Year__c ,CORE_Not_eligible_for_re_registration__c,' +
        'AccountId , CORE_OPP_Level__c , Pricebook2Id, CORE_Failed_year__c ,CORE_Is_a_repeating_student__c ,CORE_Not_eligible_reason__c, CORE_OPP_Level__r.CORE_Next_level_for_re_enrollement__r.CORE_Parent_Curriculum__c , '+ 
        'CORE_OPP_Level__r.CORE_Next_level_for_re_enrollement__r.CORE_Parent_Curriculum__r.CORE_Parent_Study_Area__c, CORE_OPP_Level__r.CORE_Next_level_for_re_enrollement__c,CORE_OPP_Level__r.CORE_Year__c,' +
        ' (SELECT '+pocFieldsString+' FROM Point_of_contact__r ORDER BY CreatedDAte ASC LIMIT 1) '+
        'FROM Opportunity WHERE (RecordType.Name = \'Re-registration\' OR RecordType.Name = \'Enrollment\') AND CORE_OPP_Business_Unit__c = \''+ String.escapeSingleQuotes(businessUnitId)+'\'  AND CORE_OPP_Academic_Year__c = \''+String.escapeSingleQuotes(academicYear)+'\'';
        query += ' AND StageName IN '+acceptableStatuses;
        query += ' AND (CORE_Technical_Sub_Statut__c = false OR (CORE_OPP_Sub_Status__c Like \'%Exit - In Exchange%\' AND StageName IN (\'Re-enrolled\',\'Re-registered\',\'Closed Won\',\'Year Out\'))) ';
        query += ' AND CORE_Re_registered_opportunity__c = null ';
        if (listviewWhereConditions != '' && listviewWhereConditions != null){
            query+= ' AND ( '+listviewWhereConditions+' )';
        }
        return query;
    }
}