@RestResource(urlMapping='/sis/opportunity/status/enrolled/exit/visa-denial')
global without sharing class CORE_SIS_Opp_StatusEnrolled11  {
    private static final String SUCCESS_MESSAGE = 'Opportunity status and sub-status updated with success';
	 @HttpPut
    global static CORE_SIS_Wrapper.ResultWrapperGlobal execute(String sisOpportunityId ) {

        CORE_SIS_Wrapper.ResultWrapperGlobal result = checkMandatoryParameters(sisOpportunityId);
        if(result != null){
            //missing parameter(s)
            return result;
        }

        try{
            CORE_SIS_GlobalWorker sisWorker = new CORE_SIS_GlobalWorker(sisOpportunityId);
            Opportunity opportunity = sisWorker.sisOpportunity;

            if(opportunity == null){
                result = new CORE_SIS_Wrapper.ResultWrapperGlobal(
                    CORE_SIS_Constants.ERROR_STATUS, 
                    String.format(CORE_SIS_Constants.MSG_OPPORTUNITY_NOT_FOUND, new List<String>{sisOpportunityId})
                );
            } else {
                sisWorker.sisOpportunity = getOppWithChangedStatus(sisWorker.sisOpportunity);
            	update sisWorker.sisOpportunity;
                result = new CORE_SIS_Wrapper.ResultWrapperGlobal(CORE_SIS_Constants.SUCCESS_STATUS, SUCCESS_MESSAGE);
            }

        } catch(Exception e){
            result = new CORE_SIS_Wrapper.ResultWrapperGlobal(CORE_SIS_Constants.ERROR_STATUS, e.getMessage() + ' at ' + e.getStackTraceString());
        }

        return result;
    }
        private static Opportunity getOppWithChangedStatus(Opportunity opportunity){
        opportunity.StageName = CORE_SIS_Constants.PKL_OPP_STAGENAME_ENROLLED;
        opportunity.CORE_OPP_Sub_Status__c = CORE_SIS_Constants.PKL_OPP_SUBSTATUS_ENROLLED_EXIT_VISA_DENIAL;

        return opportunity;
    }
    
    private static CORE_SIS_Wrapper.ResultWrapperGlobal checkMandatoryParameters(String sisOpportunityId){
        List<String> missingParameters = new List<String>();
        Boolean isMissingSisId = CORE_SIS_GlobalWorker.issisOpportunityIdParameterMissing(sisOpportunityId);

        if(isMissingSisId){
            missingParameters.add('sisOpportunityId');
        }
        
        return CORE_SIS_GlobalWorker.getMissingParameters(missingParameters);
    }
}