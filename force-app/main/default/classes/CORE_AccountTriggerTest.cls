@IsTest
public class CORE_AccountTriggerTest {
    @TestSetup
    public static void makeData(){
        CORE_DataFaker_CatalogHierarchy catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test1');
        Account account = CORE_DataFaker_Account.getStudentAccount('test1');
    }

    @IsTest
    public static void delete_account_should_upsert_deletedObject() {
        List<Account> accounts = [Select Id FROM Account];

        Test.startTest();
        delete accounts;
        Test.stopTest();

        List<CORE_Deleted_record__c> results = [Select Id from CORE_Deleted_record__c];

        System.assert(0 < results.size());
    }
}