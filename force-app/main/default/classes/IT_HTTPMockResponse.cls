/**
 * Created by Gabriele Vitanza on 28/12/2021.
 */

@IsTest
public class IT_HTTPMockResponse implements HttpCalloutMock{
    private Integer code;
    private String status;
    private String body;
    private Map<String, String> responseHeaders;

    public IT_HTTPMockResponse(Integer code, String status, String body,
            Map<String, String> responseHeaders) {

        this.code = code;
        this.status = status;
        this.body = body;

        if (responseHeaders == null) {
            this.responseHeaders = new Map<String, String>();
        } else {
            this.responseHeaders = responseHeaders;
        }
    }
    public HttpResponse respond(HttpRequest req) {

        HttpResponse res = new HttpResponse();
        for (String key : responseHeaders.keySet()) {
            res.setHeader(key, responseHeaders.get(key));
        }

        res.setBody(body);
        res.setStatusCode(code);
        res.setStatus(status);

        return res;
    }
}