/**
* @author Fodil BOUDJEDIEN - Almavia
* @date 2022-03-31
* @group LogManager
* @description Manager class for Logging using the custom metadataType CORE_Log_Settings__mdt and the Object CORE_LOG__c
* @test class CORE_LogManagerTest
*/
public without sharing class CORE_LogManagerCleanerBatch implements Database.Batchable<sObject> {
    public String apiName='';
    public CORE_LogManagerCleanerBatch(String apiName){
        this.apiName= apiName;
    }

    public Database.QueryLocator start(Database.BatchableContext BC){
        String API_NAME = this.apiName;
        CORE_LogManager logSettings = new CORE_LogManager(API_NAME);
        String query = ' SELECT ID FROM CORE_LOG__c where CORE_API_NAME__c = :API_NAME AND CreatedDate < LAST_N_DAYS:'+Integer.valueOf(logSettings.getCustomMetadataType().CORE_Time_Storage__c)+' ';
        return Database.getQueryLocator(query);
     }
  
     public void execute(Database.BatchableContext BC, List<sObject> scope){
        
        delete Scope;

      }
  
     public void finish(Database.BatchableContext BC){

     }
}