@isTest
private class CORE_MassTaskUpdateHandlerTest {

    @isTest
    static void test_InsertAccounts(){
        List<Account> accounts =  new List<Account> {
            CORE_DataFaker_Account.getStudentAccountWithoutInsert('TEST_1'),
            CORE_DataFaker_Account.getStudentAccountWithoutInsert('XXX'),
            CORE_DataFaker_Account.getStudentAccountWithoutInsert('TEST_2')
        };

        List<Database.SaveResult> results = database.insert(
           accounts, false
        );
        System.debug('results'+ results);

        CORE_MassTaskUpdateHandler resultHandler = new CORE_MassTaskUpdateHandler();
        resultHandler.settingName = 'DO_NOT_DELETE_OR_MODIFY';
        resultHandler.results = results;
        resultHandler.recordsList = accounts;
        resultHandler.sendEmail = true;
        resultHandler.exceptionError = 'NO_EXCEPTION';

        resultHandler.handleResults();
    }
    
    @isTest
    static void test_UpsertAccounts(){
        Account acc1 = CORE_DataFaker_Account.getStudentAccountWithoutInsert('TEST_1');
		insert acc1;
        
        List<Account> accounts =  new List<Account> {
            acc1,
            CORE_DataFaker_Account.getStudentAccountWithoutInsert('XXX'),
            CORE_DataFaker_Account.getStudentAccountWithoutInsert('TEST_2')
        };

        List<Database.UpsertResult> results = database.upsert(
           accounts, false
        );

        CORE_MassTaskUpdateHandler resultHandler = new CORE_MassTaskUpdateHandler();
        resultHandler.settingName = 'DO_NOT_DELETE_OR_MODIFY';
        resultHandler.upsertResults = results;
        resultHandler.recordsList = accounts;
        resultHandler.sendEmail = true;
        resultHandler.exceptionError = 'NO_EXCEPTION';

        resultHandler.handleResults();
    }
}