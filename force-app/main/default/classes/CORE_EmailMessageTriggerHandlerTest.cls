@IsTest
public class CORE_EmailMessageTriggerHandlerTest {
    @TestSetup
    static void makeData(){
        Group queue = new Group(
            Email = 'toAddress@test.com',
            Type = 'Queue',
            Name = 'TestQueue'
        );
        insert queue;
        QueuesObject q1 = new QueueSObject(QueueID = queue.id, SobjectType = 'Task');
        insert q1;
    }

    @IsTest
    public static void handleTrigger_should_update_task_status_to_close_if_task_operation_is_after_insert() {
        Account account = CORE_DataFaker_Account.getStudentAccount('pkey');
        CORE_DataFaker_CatalogHierarchy catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test');
        Opportunity opportunity = CORE_DataFaker_Opportunity.getOpportunity(catalogHierarchy.division.Id, 
            catalogHierarchy.businessUnit.Id, 
            catalogHierarchy.studyArea.Id,
            catalogHierarchy.curriculum.Id,
            catalogHierarchy.level.Id,
            catalogHierarchy.intake.Id, 
            account.Id, 
            'Lead', 
            'Lead - New'
        );

        List<Group> queues = [Select Id, Email from Group where Name = 'TestQueue' and Type = 'Queue'];
        System.assertEquals(1, queues.size());

        EmailMessage emailMessage = new EmailMessage();
        emailMessage.MessageIdentifier = 'MessageId';
        emailMessage.Incoming = true;
        emailMessage.Status = '0'; // = new 
        emailMessage.HtmlBody = 'bodyTest html';
        emailMessage.TextBody = 'bodyTest';
        
        emailMessage.Subject = 'outboundSubject';

        emailMessage.ToAddress = 'test@test.fr';
        
        emailMessage.FromAddress = queues.get(0).Email;
        emailMessage.FromName = 'generic email';
        emailMessage.RelatedToId = opportunity.Id;
        String threadIdentifier = 'testThreadIdentifier';
        emailMessage.ThreadIdentifier = threadIdentifier;
        insert emailMessage;

        EmailMessage emailMessage2 = emailMessage.clone(false,false,false,false);
        emailMessage2.Status = '3';
        emailMessage2.Incoming = false;

        Test.startTest();
        insert emailMessage2;

        Test.stopTest();

        List<EmailMessage> relatedEmailMessages = [SELECT Id, ActivityId, CORE_TECH_TaskId__c 
            FROM EmailMessage 
            WHERE ThreadIdentifier = :threadIdentifier AND Incoming = true AND Id != :emailMessage2.Id];

            System.debug('relatedEmailMessages = '+ relatedEmailMessages);
        EmailMessage relatedEmailMessage = relatedEmailMessages.get(0);
        Id taskId = relatedEmailMessage.ActivityId == null ? relatedEmailMessage.CORE_TECH_TaskId__c : relatedEmailMessage.ActivityId;

        Task task = [SELECT Id, IsClosed, Status, Core_Task_Result__c, CORE_TECH_CallTaskType__c FROM Task WHERE Id = :taskId];
        
        System.assertEquals(true, task.IsClosed);
        System.assertEquals(CORE_EmailMessageTriggerHandler.TASK_STATUS_CLOSED, task.Status);
        System.assertEquals(CORE_EmailMessageTriggerHandler.TASK_RESULT_ANSWERED, task.Core_Task_Result__c);
        System.assertEquals(CORE_EmailMessageTriggerHandler.TASK_TYPE, task.CORE_TECH_CallTaskType__c);
    }

    @isTest
    public static void beforeDelete_Should_return_an_error_if_user_have_not_custom_permission(){
        EmailMessage emailMessage = new EmailMessage();
        emailMessage.MessageIdentifier = 'MessageId';
        emailMessage.Incoming = true;
        emailMessage.Status = '0'; // = new 
        emailMessage.HtmlBody = 'bodyTest html';
        emailMessage.TextBody = 'bodyTest';
        
        emailMessage.Subject = 'outboundSubject';

        emailMessage.ToAddress = 'test@test.fr';
        
        emailMessage.FromName = 'generic email';
        String threadIdentifier = 'testThreadIdentifier';
        emailMessage.ThreadIdentifier = threadIdentifier;
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Core Telesales'];
        User u =  CORE_DataFaker_User.getUserWithoutInsert('testUser', p.Id);

        System.runAs(u) {
            insert emailMessage;
            Boolean expectedExceptionThrown =  false;
            try{
                delete emailMessage;
            } catch(Exception e){
                expectedExceptionThrown =  e.getMessage().contains(Label.CORE_EmailMessage_DeleteError) ? true : false;
            }
            System.AssertEquals(expectedExceptionThrown, true);
        }
    }

    @isTest
    public static void beforeDelete_Should_not_return_an_error_if_user_have_custom_permission(){
        EmailMessage emailMessage = new EmailMessage();
        emailMessage.MessageIdentifier = 'MessageId';
        emailMessage.Incoming = true;
        emailMessage.Status = '0'; // = new 
        emailMessage.HtmlBody = 'bodyTest html';
        emailMessage.TextBody = 'bodyTest';
        
        emailMessage.Subject = 'outboundSubject';

        emailMessage.ToAddress = 'test@test.fr';
        
        emailMessage.FromName = 'generic email';
        String threadIdentifier = 'testThreadIdentifier';
        emailMessage.ThreadIdentifier = threadIdentifier;

        insert emailMessage;
        Boolean expectedExceptionThrown =  false;
        try{
            delete emailMessage;
        } catch(Exception e){
            expectedExceptionThrown =  true;
        }
        System.AssertEquals(expectedExceptionThrown, false);
    }
}