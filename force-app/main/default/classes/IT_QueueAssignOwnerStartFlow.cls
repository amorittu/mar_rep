/**
 * Created by ftrapani on 26/10/2021.
 */

public without sharing class IT_QueueAssignOwnerStartFlow implements Queueable {
    public static final String CLASS_NAME = IT_QueueAssignOwnerStartFlow.class.getName();

    public static final String TO_PROCESS_KEY = 'toProcess';
    public static final String NOT_TO_PROCESS_KEY = 'notToProcess';

    List<Opportunity> newRecordsList;
    Set<Id> opportunityIdSet;
    public IT_QueueAssignOwnerStartFlow(Set<Id> opportunityIdSet) {
        this.opportunityIdSet = opportunityIdSet;
        this.newRecordsList = [SELECT Id, OwnerId, CORE_OPP_Division__c, CORE_OPP_Division__r.CORE_Division_code__c, CORE_OPP_Business_Unit__c, CORE_OPP_Business_Unit__r.CORE_Brand__c,
                IT_COAInProgress__c, IT_CoaProcessNumber__c, CORE_OPP_Retail_Agency__c, CORE_Enrollment_Channel__c,
                CORE_OPP_Business_Unit__r.CORE_Business_Unit_ExternalId__c, CORE_OPP_Level__r.CORE_Study_Cycle__c, CORE_OPP_Study_Area__c, 
                AccountId, Account.PersonMailingCountryCode, Account.CORE_Language_website__pc, CORE_Capture_Channel__c, IT_OfficeInCharge__c,
                IT_OrientationQueue__c, IT_OrientationQueue__r.OwnerId, IT_AdmissionQueue__c, IT_AdmissionQueue__r.OwnerId, IT_InformationQueue__c, IT_InformationQueue__r.OwnerId
        FROM Opportunity WHERE Id IN:opportunityIdSet];
    }
// TO DO. Prendere i task request from

    public void execute(QueueableContext context) {
        System.debug('IT_QueueAssignOwnerStartFlow START');
        List<Id> getAllAccount = new List<Id>(); //AccountId
        Map<Id,Id> optyIdAccountIdMap = new  Map<Id,Id>();
        List<Id> allDivisionList = new List<Id>(); //CORE_OPP_Division__c
        List<Id> allBUList = new List<Id>(); //CORE_OPP_Business_Unit__c
        List<String> allProductList = new List<String>(); //CORE_OPP_Level__r.CORE_Study_Cycle__c //ESCudo, 12/05, changed list id with list string
        List<String> allCountryList = new List<String>(); //Account.PersonMailingCountryCode Escudo, 12/05/2022, changed from PersonMailingCountry
        List<String> allCaptureChannel = new List<String>(); //CORE_Capture_Channel__c
        Map<Id,String> optyMatrixMap = new Map<Id,String>();
        List<String> allLanguageList = new List<String>(); //Account.CORE_Language_website__pc
        Map<String,String> captureChannel = IT_Utility.getCapturechannel4AssignOwner();

        for(Opportunity optyRecord : newRecordsList){
            //allDivisionList.add(optyRecord.CORE_OPP_Division__c);
            allBUList.add(optyRecord.CORE_OPP_Business_Unit__c);
            allProductList.add(optyRecord.CORE_OPP_Level__r.CORE_Study_Cycle__c);
            getAllAccount.add(optyRecord.AccountId);
            optyIdAccountIdMap.put(optyRecord.Id,optyRecord.AccountId);
            allCountryList.add(optyRecord.Account.PersonMailingCountryCode);
            allCaptureChannel.add(optyRecord.CORE_Capture_Channel__c);
            if(optyRecord.Account.CORE_Language_website__pc!=null){
                allLanguageList.add(optyRecord.Account.CORE_Language_website__pc);
            }


            //String mDivision = verifyNull(optyRecord.CORE_OPP_Division__c);
            String mBU = verifyNull(optyRecord.CORE_OPP_Business_Unit__c)!=''? verifyNull(optyRecord.CORE_OPP_Business_Unit__r.CORE_Business_Unit_ExternalId__c) : '';
            String mCountry = verifyNull(optyRecord.Account.PersonMailingCountryCode);
            String mProduct = verifyNull(optyRecord.CORE_OPP_Level__r.CORE_Study_Cycle__c);
            String mCC = verifyNull(optyRecord.CORE_Capture_Channel__c);
            String mLanguage = verifyNull(optyRecord.Account.CORE_Language_website__pc);

            String matrixAssignmentOwner = '';
            if(optyRecord.CORE_OPP_Level__r.CORE_Study_Cycle__c!=null){
                // matrixAssignmentOwner = mBU+'%'+mCountry+'%'+mCC+'%'+mLanguage+'%'+mProduct;
                matrixAssignmentOwner = mBU+'%'+mCountry+'%'+mCC+'%'+mProduct;
            } else {
                // matrixAssignmentOwner = mBU+'%'+mCountry+'%'+mCC+'%'+mLanguage;
                matrixAssignmentOwner = mBU+'%'+mCountry+'%'+mCC+'%';
            }
            optyMatrixMap.put(optyRecord.Id,matrixAssignmentOwner);
        }
        //EScudo, 29/10/21, changed IT_CountryRelevance__c with IT_Relevance__c
        //EScudo, 12/05/22, changed IT_CountryISO__c with IT_Country_Iso2__c
        Map<Id,IT_CountryRelevance__c> CRMap = new Map<Id,IT_CountryRelevance__c>([SELECT id, IT_BusinessUnit__c, IT_Country_Iso2__c, IT_ReminderCall__c,
                IT_PushCall__c, IT_Relevance__c, IT_ServiceCall__c
        FROM IT_CountryRelevance__c
        WHERE IT_BusinessUnit__c IN :allBUList
        OR IT_Country_Iso2__c IN :allCountryList]);
        System.debug('IT_QueueAssignOwnerStartFlow allBUList ' + allBUList);
        System.debug('IT_QueueAssignOwnerStartFlow allCountryList ' + allCountryList);
        System.debug('IT_QueueAssignOwnerStartFlow allCaptureChannel ' + allCaptureChannel);
        System.debug('IT_QueueAssignOwnerStartFlow allLanguageList ' + allLanguageList);

        String queryStringOAR = 'SELECT Id, IT_Country__c, IT_COANotApplied__c, IT_BusinessUnit__r.CORE_Business_Unit_ExternalId__c, IT_ExternalId__c , IT_AdmissionOfficer__c, IT_AdmissionQueue__c, IT_InformationOfficer__c,IT_InformationQueue__c, IT_OrientationOfficer__c, IT_OrientationQueue__c, IT_Division__c, IT_Division__r.CORE_Division_code__c, IT_BusinessUnit__c, IT_Level__c, IT_NavigationLanguage__c, IT_CaptureChannel__c, IT_RetailAgency__c FROM IT_OpportunityAssignmentRule__c WHERE IT_BusinessUnit__c IN :allBUList AND IT_Country__c IN :allCountryList AND IT_CaptureChannel__c IN :allCaptureChannel ';
        // queryStringOAR+= allLanguageList!=null ;
// && allLanguageList.size()>0 ? ' AND IT_NavigationLanguage__c IN :allLanguageList' : '' -> Removed languages because we should insert new assign rule records
        List<IT_OpportunityAssignmentRule__c> OARList = Database.query(queryStringOAR);


        System.debug('IT_QueueAssignOwnerStartFlow OARMap ' + OARList);
        Map<String,IT_OpportunityAssignmentRule__c > oarProductMap = new  Map<String,IT_OpportunityAssignmentRule__c >();
        Map<String,IT_OpportunityAssignmentRule__c > oarNoProductMap = new  Map<String,IT_OpportunityAssignmentRule__c >();
        Map<String,IT_OpportunityAssignmentRule__c > oarProductLabelMap = new  Map<String,IT_OpportunityAssignmentRule__c >();
        Map<String,IT_OpportunityAssignmentRule__c > oarNoProductLabelMap = new  Map<String,IT_OpportunityAssignmentRule__c >();

        for(IT_OpportunityAssignmentRule__c OAR : OARList){
            String productMatrix = getMatrix('product',OAR);
            String notProductMatrix = getMatrix('noProduct',OAR);
            String productCountryLabelMatrix = getMatrix('productCL',OAR);
            String notProductCountryLabelMatrix = getMatrix('noProductCL',OAR);

            oarProductMap.put(productMatrix,OAR);
            oarNoProductMap.put(notProductMatrix,OAR);
            oarProductLabelMap.put(productCountryLabelMatrix,OAR);
            oarNoProductLabelMap.put(notProductCountryLabelMatrix,OAR);

        }

        System.debug('IT_QueueAssignOwnerStartFlow oarProductMap ' + oarProductMap);
        System.debug('IT_QueueAssignOwnerStartFlow oarNoProductMap ' + oarNoProductMap);
        System.debug('IT_QueueAssignOwnerStartFlow oarProductLabelMap ' + oarProductLabelMap);
        System.debug('IT_QueueAssignOwnerStartFlow oarNoProductLabelMap ' + oarNoProductLabelMap);



        Map<Opportunity,IT_OpportunityAssignmentRule__c> optyOARMap = new  Map<Opportunity,IT_OpportunityAssignmentRule__c>();
        for(Opportunity optyRecord : newRecordsList){
            if(CRMap!=null && CRMap.size()>0){
                for(IT_CountryRelevance__c CRM : CRMap.values()){
                    if(CRM.IT_BusinessUnit__c == optyRecord.CORE_OPP_Business_Unit__c && CRM.IT_Country_Iso2__c== optyRecord.Account.PersonMailingCountryCode){
                        optyRecord.IT_PushCall__c = CRM.IT_PushCall__c;
                        optyRecord.IT_ServiceCall__c = CRM.IT_ServiceCall__c; // 21/12 --> Further Contact
                        optyRecord.IT_CountryRelevance__c = CRM.IT_Relevance__c;
                        optyRecord.IT_ReminderCall__c = CRM.IT_ReminderCall__c;
                    }
                }
            }
            IT_OpportunityAssignmentRule__c OARfound = new IT_OpportunityAssignmentRule__c();

            String matrixValue = optyMatrixMap.get(optyRecord.Id);
            System.debug('IT_QueueAssignOwnerStartFlow matrixValue ' + matrixValue);
            if(String.isNotBlank(matrixValue)){

                IT_OpportunityAssignmentRule__c OAR = new IT_OpportunityAssignmentRule__c();
                Boolean foundMatrix = false;
                if(oarProductMap!=null && oarProductMap.containsKey(matrixValue)){
                    OARfound = oarProductMap.get(matrixValue);
                    System.debug('IT_QueueAssignOwnerStartFlow oarProductMap.get(matrixValue) ' + oarProductMap.get(matrixValue));
                    foundMatrix = true;
                } else if(oarProductLabelMap!=null && oarProductLabelMap.containsKey(matrixValue)) {
                    OARfound = oarProductLabelMap.get(matrixValue);
                    System.debug('IT_QueueAssignOwnerStartFlow oarProductLabelMap.get(matrixValue) ' + oarProductLabelMap.get(matrixValue));
                    foundMatrix = true;
                } else if(oarNoProductMap!=null && oarNoProductMap.containsKey(matrixValue)) {
                    OARfound = oarNoProductMap.get(matrixValue);
                    System.debug('IT_QueueAssignOwnerStartFlow oarNoProductMap.get(matrixValue) ' + oarNoProductMap.get(matrixValue));
                    foundMatrix = true;
                } else if(oarNoProductLabelMap!=null && oarNoProductLabelMap.containsKey(matrixValue)) {
                    OARfound = oarNoProductLabelMap.get(matrixValue);
                    System.debug('IT_QueueAssignOwnerStartFlow oarNoProductLabelMap.get(matrixValue) ' + oarNoProductLabelMap.get(matrixValue));
                    foundMatrix = true;
                }


                if(foundMatrix){
                    System.debug('IT_QueueAssignOwnerStartFlow OARfound ' + OARfound);
                    optyOARMap.put(optyRecord,OARfound);
                } else {
                    String newmatrixValue = matrixValue.substring(0, matrixValue.lastIndexOf('%'));
                    // newmatrixValue+='%'; // TEMPORARY FIX
                    System.debug('IT_QueueAssignOwnerStartFlow NOT found ');
                    System.debug('IT_QueueAssignOwnerStartFlow newmatrixValue ' + newmatrixValue);
                    Boolean foundNewMatrix = false;
                    if(oarNoProductMap!=null && oarNoProductMap.containsKey(newmatrixValue)) {
                        OARfound = oarNoProductMap.get(newmatrixValue);
                        foundNewMatrix = true;
                    } else if(oarNoProductLabelMap!=null && oarNoProductLabelMap.containsKey(newmatrixValue)) {
                        OARfound = oarNoProductLabelMap.get(newmatrixValue);
                        foundNewMatrix = true;
                    }

                    if(foundNewMatrix){
                        System.debug('IT_QueueAssignOwnerStartFlow OARfound ' + OARfound);
                        optyOARMap.put(optyRecord,OARfound);
                    }
                }

            }

        }

        List<Id> optyListToExclude = new List<Id>(); // 28/01 --> Excluding opportunity to run flow if IT_COANotApplied__c from Assignment rule is true
        if(optyOARMap!=null && optyOARMap.size()>0){
            Map<String, List<Opportunity>> populateUserQueueMap = populateUserQueue(optyOARMap,newRecordsList,captureChannel);
            System.debug('populateUserQueueMap is : ' + JSON.serialize(populateUserQueueMap));
            newRecordsList =  populateUserQueueMap.get(TO_PROCESS_KEY);
            // 28/01 --> Excluding opportunity to run flow if IT_COANotApplied__c from Assignment rule is true START
            if ( populateUserQueueMap.containsKey(NOT_TO_PROCESS_KEY) ) {
                for(Opportunity opt : populateUserQueueMap.get(NOT_TO_PROCESS_KEY)) {
                    optyListToExclude.add(opt.Id);
                }
            }
            // 28/01 --> Excluding opportunity to run flow if IT_COANotApplied__c from Assignment rule is true END
        }

        System.debug('IT_QueueAssignOwnerStartFlow optyOARMap:: ' + optyOARMap);
        newRecordsList = verifyOtherOpty(newRecordsList,optyIdAccountIdMap,optyOARMap,captureChannel);

        //update newRecordsList;
        List<Database.UpsertResult> results = Database.upsert( newRecordsList, false );

        for(Database.UpsertResult resultRecord : results) {
            if(!resultRecord.isSuccess()){
                List<Database.Error> errors = resultRecord.getErrors();
                System.debug('IT_QueueAssignOwnerStartFflow upsert error: ' + resultRecord + ', errors ' + errors);
            }
        }
        //query because flow need fields in object

        Task[] tasksToAutoComplete = [  SELECT Id, Status FROM Task WHERE WhatId IN: opportunityIdSet
        AND  RecordType.DeveloperName = 'Master'
        AND Type = 'CORE_PKL_Web_Form'
        AND Status = 'CORE_PKL_Open'
        ];

        for (Task tsk : tasksToAutoComplete) {
            tsk.Status = 'CORE_PKL_Completed';
        }
        update tasksToAutoComplete;
        //List<Task> taskList = [SELECT Id FROM Task WHERE WhatId IN :opportunityIdSet ];
        //System.debug('IT_QueueAssignOwnerStartFlow taskList ' + taskList);
        //delete taskList;


        // 27/01 --> Excluding opportunity to run flow if IT_COANotApplied__c from Assignment rule is true END

        List<String> fields = new List<String>(Opportunity.SObjectType.getDescribe().fields.getMap().keySet());
        String queryString = 'SELECT ' + String.join(fields, ',')
                + ', IT_InformationQueue__r.OwnerId, IT_AdmissionQueue__r.OwnerId '
                + ' FROM Opportunity'
                + ' WHERE Id IN :newRecordsList';
        // 27/01 --> Excluding opportunity to run flow if IT_COANotApplied__c from Assignment rule is true START
        if (optyListToExclude.size() > 0) {
            queryString +=  ' AND Id NOT IN :optyListToExclude';
        }
        // 27/01 --> Excluding opportunity to run flow if IT_COANotApplied__c from Assignment rule is true END

        List<Opportunity> optyList = Database.query(queryString);

        Map<String, Object> flowParams = new Map<String, Object >();
        flowParams.put('OpportunityList',optyList);
        flowParams.put('input_coaTaskRecordType', Schema.SObjectType.Task.getRecordTypeInfosByDeveloperName().get('CORE_Continuum_of_action').getRecordTypeId() );

        // Flow.Interview.ITCOASortingFlows flow = new Flow.Interview.ITCOASortingFlows(opportunityMap);
        if (!Test.isRunningTest() &&  ! flowParams.isEmpty()) {
            System.debug(CLASS_NAME + ' - execute - flowParams : ' + JSON.serialize(flowParams) );

            Flow.Interview.IT_COA_SortingFlowsPart0 flow = new Flow.Interview.IT_COA_SortingFlowsPart0(flowParams);
            flow.start();
        }
        System.debug('IT_QueueAssignOwnerStartFlow START');
        IT_OpportunityTriggerHandler.triggerCallBack = true;
		
		String Mrgncy1 = '1';
        String Mrgncy2 = '2';
        String Mrgncy3 = '3';
        String Mrgncy4 = '4';
        String Mrgncy5 = '5';
        String Mrgncy6 = '6';
        String Mrgncy7 = '7';
        String Mrgncy8 = '8';
        String Mrgncy9 = '9';
        String Mrgncy10 = '10';
        String Mrgncy11 = '11';
        String Mrgncy12 = '12';
        String Mrgncy13 = '13';
        String Mrgncy14 = '14';
        String Mrgncy15 = '15';
        String Mrgncy16 = '16';
        String Mrgncy17 = '17';
        String Mrgncy18 = '18';
        String Mrgncy19 = '19';
        String Mrgncy20 = '20';
        String Mrgncy21 = '21';
        String Mrgncy22 = '22';
        String Mrgncy23 = '23';
        String Mrgncy24 = '24';
        String Mrgncy25 = '25';
        String Mrgncy26 = '26';
        String Mrgncy27 = '27';
        String Mrgncy28 = '28';
        String Mrgncy29 = '29';
        String Mrgncy30 = '30';
        String Mrgncy31 = '31';
        String Mrgncy32 = '32';
        String Mrgncy33 = '33';
        String Mrgncy34 = '34';
        String Mrgncy35 = '35';
        String Mrgncy36 = '36';
        String Mrgncy37 = '37';
        String Mrgncy38 = '38';
        String Mrgncy39 = '39';
        String Mrgncy40 = '40';
        String Mrgncy41 = '41';
        String Mrgncy42 = '42';
        String Mrgncy43 = '43';
        String Mrgncy44 = '44';
        String Mrgncy45 = '45';
        String Mrgncy46 = '46';
        String Mrgncy47 = '47';
        String Mrgncy48 = '48';
        String Mrgncy49 = '49';
        String Mrgncy50 = '50';
        String Mrgncy51 = '51';
        String Mrgncy52 = '52';
        String Mrgncy53 = '53';
        String Mrgncy54 = '54';
        String Mrgncy55 = '55';
        String Mrgncy56 = '56';
        String Mrgncy57 = '57';
        String Mrgncy58 = '58';
        String Mrgncy59 = '59';
        String Mrgncy60 = '60';
        String Mrgncy61 = '61';
        String Mrgncy62 = '62';
        String Mrgncy63 = '63';
        String Mrgncy64 = '64';
        String Mrgncy65 = '65';
        String Mrgncy66 = '66';
        String Mrgncy67 = '67';
        String Mrgncy68 = '68';
        String Mrgncy69 = '69';
        String Mrgncy70 = '70';
        String Mrgncy71 = '71';
        String Mrgncy72 = '72';
        String Mrgncy73 = '73';
        String Mrgncy74 = '74';
        String Mrgncy75 = '75';
        String Mrgncy76 = '76';
        String Mrgncy77 = '77';
        String Mrgncy78 = '78';
        String Mrgncy79 = '79';
        String Mrgncy80 = '80';
        String Mrgncy81 = '81';
        String Mrgncy82 = '82';
        String Mrgncy83 = '83';
        String Mrgncy84 = '84';
        String Mrgncy85 = '85';
        String Mrgncy86 = '86';
        String Mrgncy87 = '87';
        String Mrgncy88 = '88';
        String Mrgncy89 = '89';
        String Mrgncy90 = '90';
        String Mrgncy91 = '91';
        String Mrgncy92 = '92';
        String Mrgncy93 = '93';
        String Mrgncy94 = '94';
        String Mrgncy95 = '95';
        String Mrgncy96 = '96';
        String Mrgncy97 = '97';
        String Mrgncy98 = '98';
        String Mrgncy99 = '99';
        String Mrgncy100 = '100';
        String Mrgncy101 = '101';
        String Mrgncy102 = '102';
        String Mrgncy103 = '103';
        String Mrgncy104 = '104';
        String Mrgncy105 = '105';
        String Mrgncy106 = '106';
        String Mrgncy107 = '107';
        String Mrgncy108 = '108';
        String Mrgncy109 = '109';
        String Mrgncy110 = '110';
        String Mrgncy111 = '111';
        String Mrgncy112 = '112';
        String Mrgncy113 = '113';
        String Mrgncy114 = '114';
        String Mrgncy115 = '115';
        String Mrgncy116 = '116';
        String Mrgncy117 = '117';
        String Mrgncy118 = '118';
        String Mrgncy119 = '119';
        String Mrgncy120 = '120';
        String Mrgncy121 = '121';
        String Mrgncy122 = '122';
        String Mrgncy123 = '123';
        String Mrgncy124 = '124';
        String Mrgncy125 = '125';
        String Mrgncy126 = '126';
        String Mrgncy127 = '127';
        String Mrgncy128 = '128';
        String Mrgncy129 = '129';
        String Mrgncy130 = '130';
        String Mrgncy131 = '131';
        String Mrgncy132 = '132';
        String Mrgncy133 = '133';
        String Mrgncy134 = '134';
        String Mrgncy135 = '135';
        String Mrgncy136 = '136';
        String Mrgncy137 = '137';
        String Mrgncy138 = '138';
        String Mrgncy139 = '139';
        String Mrgncy140 = '140';
        String Mrgncy141 = '141';
        String Mrgncy142 = '142';
        String Mrgncy143 = '143';
        String Mrgncy144 = '144';
        String Mrgncy145 = '145';
        String Mrgncy146 = '146';
        String Mrgncy147 = '147';
        String Mrgncy148 = '148';
        String Mrgncy149 = '149';
        String Mrgncy150 = '150';
        String Mrgncy151 = '151';
        String Mrgncy152 = '152';
        String Mrgncy153 = '153';
        String Mrgncy154 = '154';
        String Mrgncy155 = '155';
        String Mrgncy156 = '156';
        String Mrgncy157 = '157';
        String Mrgncy158 = '158';
        String Mrgncy159 = '159';
        String Mrgncy160 = '160';
        String Mrgncy161 = '161';
        String Mrgncy162 = '162';
        String Mrgncy163 = '163';
        String Mrgncy164 = '164';
        String Mrgncy165 = '165';
        String Mrgncy166 = '166';
        String Mrgncy167 = '167';
        String Mrgncy168 = '168';
        String Mrgncy169 = '169';
        String Mrgncy170 = '170';
        String Mrgncy171 = '171';
        String Mrgncy172 = '172';
        String Mrgncy173 = '173';
        String Mrgncy174 = '174';
        String Mrgncy175 = '175';
        String Mrgncy176 = '176';
        String Mrgncy177 = '177';
        String Mrgncy178 = '178';
        String Mrgncy179 = '179';
        String Mrgncy180 = '180';
        String Mrgncy181 = '181';
        String Mrgncy182 = '182';
        String Mrgncy183 = '183';
        String Mrgncy184 = '184';
        String Mrgncy185 = '185';
        String Mrgncy186 = '186';
        String Mrgncy187 = '187';
        String Mrgncy188 = '188';
        String Mrgncy189 = '189';
        String Mrgncy190 = '190';
        String Mrgncy191 = '191';
        String Mrgncy192 = '192';
        String Mrgncy193 = '193';
        String Mrgncy194 = '194';
        String Mrgncy195 = '195';
        String Mrgncy196 = '196';
        String Mrgncy197 = '197';
        String Mrgncy198 = '198';
        String Mrgncy199 = '199';
        String Mrgncy200 = '200';
        String Mrgncy201 = '201';
        String Mrgncy202 = '202';
        String Mrgncy203 = '203';
        String Mrgncy204 = '204';
        String Mrgncy205 = '205';
        String Mrgncy206 = '206';
        String Mrgncy207 = '207';
        String Mrgncy208 = '208';
        String Mrgncy209 = '209';

    }

    public String getMatrix(String typeMatrix, IT_OpportunityAssignmentRule__c OAR){
        String matrix = '';
        //EScudo, 12/05/22, changed IT_MainCountry__c with IT_Country__c
        Map<String,String> mainCountryPicklistMap = IT_Utility.getPicklistLabel('IT_OpportunityAssignmentRule__c','IT_Country__c');

        switch on typeMatrix {
            when 'product' {
                // matrix = verifyNull(OAR.IT_BusinessUnit__r.CORE_Business_Unit_ExternalId__c)
                //         +'%'+verifyNull(OAR.IT_Country__c)+'%'+verifyNull(OAR.IT_CaptureChannel__c)
                //         +'%'+verifyNull(OAR.IT_NavigationLanguage__c)
                //         +'%'+verifyNull(OAR.IT_Level__c);
                matrix = verifyNull(OAR.IT_BusinessUnit__r.CORE_Business_Unit_ExternalId__c)
                        +'%'+verifyNull(OAR.IT_Country__c)+'%'+verifyNull(OAR.IT_CaptureChannel__c)
                        +'%'+verifyNull(OAR.IT_Level__c);
            }
            when 'productCL' {
                matrix = verifyNull(OAR.IT_BusinessUnit__r.CORE_Business_Unit_ExternalId__c)
                        +'%'+verifyNull(mainCountryPicklistMap.get(OAR.IT_Country__c))
                        +'%'+verifyNull(OAR.IT_CaptureChannel__c)+
                        +verifyNull(OAR.IT_Level__c);
            }
            when 'noProduct' {
                matrix =verifyNull(OAR.IT_BusinessUnit__r.CORE_Business_Unit_ExternalId__c)
                        +'%'+verifyNull(OAR.IT_Country__c)+'%'
                        +verifyNull(OAR.IT_CaptureChannel__c);
            }
            when 'noProductCL' {
                matrix = verifyNull(OAR.IT_BusinessUnit__r.CORE_Business_Unit_ExternalId__c)+'%'
                        +verifyNull(mainCountryPicklistMap.get(OAR.IT_Country__c))+'%'+verifyNull(OAR.IT_CaptureChannel__c);
            }
        }
        return matrix;
    }

    public static String verifyNull(String inputString){
        return inputString!=null?inputString:'';
    }

    public static Map<String, List<Opportunity>> populateUserQueue(Map<Opportunity,IT_OpportunityAssignmentRule__c> optyOARMap, List<Opportunity> optyList,Map<String,String> captureChannel ){

        Map<String, List<Opportunity>> toReturn = new Map<String, List<Opportunity>>(); // 28/01/22 - Changed return method. If coa not applied true, exclude them after update of certains fields )
        List<Opportunity> opportunityToExclude = new List<Opportunity>();

        for(Opportunity optyRecord : optyList){
            IT_OpportunityAssignmentRule__c OARfound = optyOARMap.get(optyRecord);
            if(OARfound!=null){
                optyRecord.IT_InformationOfficer__c = OARfound.IT_InformationOfficer__c;
                optyRecord.IT_InformationQueue__c = OARfound.IT_InformationQueue__c;
                optyRecord.IT_AdmissionOfficer__c = OARfound.IT_AdmissionOfficer__c;
                optyRecord.IT_AdmissionQueue__c = OARfound.IT_AdmissionQueue__c;
                optyRecord.IT_OrientationOfficer__c = OARfound.IT_OrientationOfficer__c;
                optyRecord.IT_OrientationQueue__c = OARfound.IT_OrientationQueue__c;
                optyRecord.CORE_OPP_Retail_Agency__c = OARfound.IT_RetailAgency__c;
                //if(OARfound.IT_InformationOfficer__c!=null && String.isNotBlank(optyRecord.CORE_Capture_Channel__c) && captureChannel!=null && captureChannel.containsKey(optyRecord.CORE_Capture_Channel__c)){
                if (OARfound.IT_InformationOfficer__c != null) {
                    optyRecord.OwnerId = OARfound.IT_InformationOfficer__c;
                }
                if (OARfound.IT_InformationOfficer__c != null || optyRecord.CORE_OPP_Retail_Agency__c != null) {
                    System.debug('IT_QueueAssignOwnerStartFlow populateUserQueue, set owner ');
                    optyRecord.IT_OpportunityAssigned__c = true;
                }
                //EScudo, 26/11/2021, added deafault on office in charge = information
                if (optyRecord.CORE_OPP_Retail_Agency__c != null) {
                    optyRecord.IT_OfficeInCharge__c = null;
                } else {
                    optyRecord.IT_OfficeInCharge__c = 'Information';
                }
				
                if (optyRecord.CORE_Capture_Channel__c == 'CORE_PKL_Web_form' && String.isNotBLank(optyRecord.CORE_OPP_Retail_Agency__c)) {
                    optyRecord.CORE_Enrollment_Channel__c = 'CORE_PKL_Retail';
                } else if(optyRecord.CORE_Capture_Channel__c == 'CORE_PKL_Web_form' && String.isBlank(optyRecord.CORE_OPP_Retail_Agency__c) ) {
                    optyRecord.CORE_Enrollment_Channel__c = 'CORE_PKL_Direct';
                }
                
                // 28/01/22 - Changed return method. If coa not applied true, exclude them after update of certains fields ) START
                if (OARfound.IT_COANotApplied__c == true) {
                    opportunityToExclude.add(optyRecord);
                }
                // 28/01/22 - Changed return method. If coa not applied true, exclude them after update of certains fields ) END
            }
        }
        if (optyList.size() > 0) {
            toReturn.put(TO_PROCESS_KEY, optyList);
        }
        if (opportunityToExclude.size() > 0) {
            toReturn.put(NOT_TO_PROCESS_KEY, opportunityToExclude);
        }
        return toReturn;
    }


    public static List<Opportunity> verifyOtherOpty(List<Opportunity> optyList,Map<Id,Id> optyIdAccountIdMap,Map<Opportunity,IT_OpportunityAssignmentRule__c> optyOARMap, Map<String,String> captureChannel){

        Map<Id,Account> accountMap = new Map<Id,Account>([SELECT Id, (SELECT Id, CORE_OPP_Division__c, CORE_OPP_Business_Unit__r.CORE_Brand__c, CORE_Capture_Channel__c FROM Opportunities WHERE Id!= :optyIdAccountIdMap.keySet() ORDER BY CreatedDate DESC), (SELECT Id, IT_Account__c, IT_LastContactAdmissionDate__c, IT_AdmissionOwner__c, IT_LastContactInformationDate__c, IT_InformationOfficer__c, IT_Brand__c FROM Extended_Informations__r) FROM Account WHERE Id IN :optyIdAccountIdMap.values()]);


        for(Opportunity recordOpty : optyList){
            Account recordAccount = accountMap.get(recordOpty.AccountId);
            List<Opportunity> otherOptyList = recordAccount.Opportunities;
            Boolean alreadyExistOptyBrandBU = false;
            Boolean alreadyExistOptyBrand = false;
            for(Opportunity otherOpty : otherOptyList){
                if(recordOpty.CORE_OPP_Business_Unit__c== otherOpty.CORE_OPP_Business_Unit__c && recordOpty.CORE_OPP_Business_Unit__r.CORE_Brand__c == otherOpty.CORE_OPP_Business_Unit__r.CORE_Brand__c){
                    alreadyExistOptyBrandBU = true;

                }

                System.debug('recordOpty.CORE_OPP_Business_Unit__r.CORE_Brand__c:: ' + recordOpty.CORE_OPP_Business_Unit__r.CORE_Brand__c);
                System.debug('otherOpty.CORE_OPP_Business_Unit__r.CORE_Brand__c:: ' + otherOpty.CORE_OPP_Business_Unit__r.CORE_Brand__c);
                System.debug('alreadyExistOptyBrand:: ' + alreadyExistOptyBrand);
                if(recordOpty.CORE_OPP_Business_Unit__r.CORE_Brand__c == otherOpty.CORE_OPP_Business_Unit__r.CORE_Brand__c){
                    alreadyExistOptyBrand = true;
                }
            }
            //System.debug('IT_QueueAssignOwnerStartFlow alreadyExistOpty ' + alreadyExistOpty);
            List<IT_ExtendedInfo__c> extInfoList = recordAccount.Extended_Informations__r;
            if(alreadyExistOptyBrand){
                for(IT_ExtendedInfo__c extendedInfo : extInfoList){
                    System.debug('test');
                    if(recordOpty.CORE_OPP_Business_Unit__r.CORE_Brand__c == extendedInfo.IT_Brand__c){
                        DateTime informationDate = extendedInfo.IT_LastContactInformationDate__c;
                        DateTime admissionDate = extendedInfo.IT_LastContactAdmissionDate__c;
                        Boolean isInformationDate = admissionDate!=null? informationDate>admissionDate : true;
                        if(String.isNotBlank(recordOpty.CORE_Capture_Channel__c) && captureChannel!=null && captureChannel.containsKey(recordOpty.CORE_Capture_Channel__c)){
                            if(isInformationDate && extendedInfo.IT_InformationOfficer__c!=null){
                                System.debug('IT_QueueAssignOwnerStartFlow verifyOtherOpty, isInformationDate set owner ');
                                recordOpty.OwnerId = extendedInfo.IT_InformationOfficer__c;
                                recordOpty.IT_OpportunityAssigned__c = true;
                            } else if(!isInformationDate && extendedInfo.IT_AdmissionOwner__c!= null){
                                System.debug('IT_QueueAssignOwnerStartFlow verifyOtherOpty, !isInformationDate set owner ');
                                recordOpty.OwnerId = extendedInfo.IT_AdmissionOwner__c;
                                recordOpty.IT_OpportunityAssigned__c = true;
                            }

                        }

                        if(!alreadyExistOptyBrandBU && !isInformationDate){
                            IT_OpportunityAssignmentRule__c OARfound = optyOARMap.get(recordOpty);
                            if(OARfound!=null && OARfound.IT_AdmissionQueue__c!=null){
                                recordOpty.IT_OfficeInCharge__c = 'Admission';
                                recordOpty.IT_AdmissionQueue__c = OARfound.IT_AdmissionQueue__c;
                            }
                        }
                    }
                }
            } else {
                IT_OpportunityAssignmentRule__c OARfound = optyOARMap.get(recordOpty);
                System.debug('OARfound:: ' + OARfound);
                if(OARfound!=null && OARfound.IT_InformationOfficer__c!=null){
                    System.debug('IT_QueueAssignOwnerStartFlow IT_InformationOfficer__c found');
                    if(OARfound.IT_InformationOfficer__c!=null && String.isNotBlank(recordOpty.CORE_Capture_Channel__c) && captureChannel!=null && captureChannel.containsKey(recordOpty.CORE_Capture_Channel__c)){
                        System.debug('IT_QueueAssignOwnerStartFlow verifyOtherOpty, set owner ');
                        recordOpty.OwnerId = OARfound.IT_InformationOfficer__c;
                        recordOpty.IT_OpportunityAssigned__c = true;
                    }

                }
            }
        }

        return optyList;

    }
}