public class IT_MarketingCloudHandler {
    
    public static String generateRandomUid(Integer length) {
        final String chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789abcdefghijklmnopqrstuvwxyz';
        String guid = '';
    
        while (guid.length() < length) {
            Integer idx = Math.mod(Math.abs(Crypto.getRandomInteger()), chars.length());
            guid += chars.substring(idx, idx + 1);
        }
    
        return guid;
    }
    
}