public without sharing class CORE_AccountTriggerHandler {
    @TestVisible
    private static final string DELETED_OBJECT_TYPE = 'CORE_PKL_Account';

    @TestVisible
    private static final string DELETED_OBJECT_TYPE_OPPORTUNITY = 'CORE_PKL_Opportunity';

    public static void handleTrigger(List<Account> oldRecords, List<Account> newRecords, System.TriggerOperation triggerEvent) {
        switch on triggerEvent {
            when BEFORE_INSERT {
                System.debug('CORE_AccountTriggerHandler.beforeInsert : START');
                beforeInsert(newRecords);
                System.debug('CORE_AccountTriggerHandler.beforeInsert : END');
            }
            when BEFORE_UPDATE {
                System.debug('CORE_AccountTriggerHandler.beforeUpdate : START');
                beforeUpdate(oldRecords,newRecords);
                System.debug('CORE_AccountTriggerHandler.beforeUpdate : END');
            }
            when AFTER_DELETE {
                System.debug('CORE_AccountTriggerHandler.afterDelete : START');
                afterDelete(oldRecords);
                System.debug('CORE_AccountTriggerHandler.afterDelete : END');
            }
        }
        
    }

    @TestVisible
    private static void beforeInsert(List<Account> newRecords) {
        List<CORE_CountryPhoneRelationship__c> listCountryPhoneIndicatif = [SELECT Id,CORE_Country_Code_Iso_2__c,CORE_Country_Phone_Code__c FROM CORE_CountryPhoneRelationship__c];
        Map<String,String> mapCountryPhoneIndicatif = new Map<String,String>();
        for(CORE_CountryPhoneRelationship__c countryPhoneRelationship :listCountryPhoneIndicatif){
            mapCountryPhoneIndicatif.put(countryPhoneRelationship.CORE_Country_Phone_Code__c,countryPhoneRelationship.CORE_Country_Code_Iso_2__c);
        }
        for(Account newAccount : newRecords){
            if(newAccount.IsPersonAccount){
                String allPhones = getAllPhones(newAccount);
                newAccount.CORE_TECH_All_Phones__c = String.isEmpty(allPhones) ? null : allPhones;
                if(!String.isBlank(newAccount.PersonMobilePhone)){
                    Integer j=5;
                    for(Integer i=0;i<4;i++){
                        if(newAccount.PersonMobilePhone.length() < j){
                            j--;
                            continue;
                        }
                        newAccount.et4ae5__Mobile_Country_Code__pc = mapCountryPhoneIndicatif.get(newAccount.PersonMobilePhone.left(j));
                        j--;
                        if(!String.isBlank(newAccount.et4ae5__Mobile_Country_Code__pc)){
                            break;
                        }
                    }
                }
            }
        }
    }

    @TestVisible
    private static void beforeUpdate(List<Account> oldRecords, List<Account> newRecords) {
        Map<Id,Account> oldAccounts = new Map<Id,Account>(oldRecords);
        List<CORE_CountryPhoneRelationship__c> listCountryPhoneIndicatif = [SELECT Id,CORE_Country_Code_Iso_2__c,CORE_Country_Phone_Code__c FROM CORE_CountryPhoneRelationship__c];
        Map<String,String> mapCountryPhoneIndicatif = new Map<String,String>();
        for(CORE_CountryPhoneRelationship__c countryPhoneRelationship :listCountryPhoneIndicatif){
            mapCountryPhoneIndicatif.put(countryPhoneRelationship.CORE_Country_Phone_Code__c,countryPhoneRelationship.CORE_Country_Code_Iso_2__c);
        }
        for(Account newAccount : newRecords){
            if(newAccount.IsPersonAccount){
                Account oldAccount = oldAccounts.get(newAccount.Id);
                String allPhones = getAllPhones(newAccount);

                if(isPhoneChanged(oldAccount,newAccount) || (!String.IsEmpty(allPhones) && String.IsEmpty(newAccount.CORE_TECH_All_Phones__c))){
                    newAccount.CORE_TECH_All_Phones__c = String.isEmpty(allPhones) ? null : allPhones;
                    if(!String.isBlank(newAccount.PersonMobilePhone)){
                        Integer j=5;
                        for(Integer i=0;i<4;i++){
                            if(newAccount.PersonMobilePhone.length() < j){
                                j--;
                                continue;
                            }

                            newAccount.et4ae5__Mobile_Country_Code__pc = mapCountryPhoneIndicatif.get(newAccount.PersonMobilePhone.left(j));

                            j--;
                            if(!String.isBlank(newAccount.et4ae5__Mobile_Country_Code__pc)){
                                break;
                            }
                        }
                    }
                }
            }
        }
    }

    private static Boolean isPhoneChanged(Account oldAccount, Account newAccount){
        if(oldAccount.Phone != newAccount.Phone){
            return true;
        }
        if(oldAccount.CORE_Phone2__pc != newAccount.CORE_Phone2__pc){
            return true;
        }
        if(oldAccount.PersonMobilePhone != newAccount.PersonMobilePhone){
            return true;
        }
        if(oldAccount.CORE_Mobile_2__pc != newAccount.CORE_Mobile_2__pc){
            return true;
        }
        if(oldAccount.CORE_Mobile_3__pc != newAccount.CORE_Mobile_3__pc){
            return true;
        }
        
        return false;
    }

    private static String getAllPhones(Account account){
        String allPhones = '';
        if(!String.isEmpty(account.Phone)){
            allPhones += account.Phone;
        }
        if(!String.isEmpty(account.CORE_Phone2__pc)){
            allPhones += account.CORE_Phone2__pc;
        }
        if(!String.isEmpty(account.PersonMobilePhone)){
            allPhones += account.PersonMobilePhone;
        }
        if(!String.isEmpty(account.CORE_Mobile_2__pc)){
            allPhones += account.CORE_Mobile_2__pc;
        }
        if(!String.isEmpty(account.CORE_Mobile_3__pc)){
            allPhones += account.CORE_Mobile_3__pc;
        }
        
        return allPhones;
    }

    @TestVisible
    private static void afterDelete(List<Account> oldRecords) {
        List<CORE_Deleted_record__c> deletedRecordsToUpsert = new List<CORE_Deleted_record__c>();

        Set<Id> recordTypeIds = new Set<Id>();
        Set<Id> masterAccountsIds = new Set<Id>();

        for(Account currentRecord : oldRecords){
            recordTypeIds.add(currentRecord.RecordTypeId);

            if(currentRecord.MasterRecordId != null){
                masterAccountsIds.add(currentRecord.MasterRecordId);
            }
        }

        Set<Id> oldRecordIds = (new Map<Id, Account>(oldRecords)).keySet();
        List<Opportunity> opportunitiesToDeleted = [Select Id, StageName, CORE_IsFromDeferal__c, CORE_OPP_Sub_Status__c,IsClosed, RecordTypeId, LastModifiedDate FROM Opportunity WHERE AccountId IN :oldRecordIds ALL ROWS];
        deleteOpportunities(opportunitiesToDeleted, deletedRecordsToUpsert);

        Map<Id, String> recordTypeDevNames = CORE_RecordTypeUtils.getDeveloperNameByIds('Account', recordTypeIds);

        Map<Id, Account> masterAccountsByIds = new Map<Id, Account>();

        if(!masterAccountsIds.isEmpty()){
            List<Account> masterAccounts = [Select Id, CORE_LegacyCRMID__c, CORE_SISStudentID__c FROM Account where Id in :masterAccountsIds];
            masterAccountsByIds = new Map<Id, Account>(masterAccounts);
        }
        
        for(Account currentRecord : oldRecords){
            Id currentId = currentRecord.Id;
            Id maseterId = currentRecord.MasterRecordId;
            CORE_Deleted_record__c deletedRecord = new CORE_Deleted_record__c(
                CORE_Deletion_Date__c = System.now(),
                CORE_Object_Name__c = DELETED_OBJECT_TYPE,
                CORE_Record_Id__c = currentId,
                CORE_ContactId__c = currentRecord.PersonContactId,
                CORE_Record_Type__c = recordTypeDevNames.get(currentRecord.RecordTypeId),
                CORE_LegacyCRMID__c = currentRecord.CORE_LegacyCRMID__c,
                CORE_SISStudentID__c = currentRecord.CORE_SISStudentID__c,
                CORE_MasterId__c = maseterId,
                CORE_MasterLegacyCRMID__c = maseterId == null ? null : masterAccountsByIds.get(maseterId).CORE_LegacyCRMID__c,
                CORE_MasterSISStudentID__c = maseterId == null ? null : masterAccountsByIds.get(maseterId).CORE_SISStudentID__c
            );
            

            deletedRecordsToUpsert.add(deletedRecord);
        }

        if(!deletedRecordsToUpsert.isEmpty()){
            Upsert deletedRecordsToUpsert CORE_Deleted_record__c.fields.CORE_Record_Id__c;
        }
    }

    private static void deleteOpportunities(List<Opportunity> oldRecords, List<CORE_Deleted_record__c> deletedRecordsToUpsert) {
        Set<Id> recordTypeIds = new Set<Id>();

        for(Opportunity currentRecord : oldRecords){
            recordTypeIds.add(currentRecord.RecordTypeId);
        }

        Map<Id, String> recordTypeDevNames = CORE_RecordTypeUtils.getDeveloperNameByIds('Opportunity', recordTypeIds);

        for(Opportunity currentRecord : oldRecords){
            CORE_Deleted_record__c deletedRecord = new CORE_Deleted_record__c(
                CORE_Deletion_Date__c = System.now(),
                CORE_Object_Name__c = DELETED_OBJECT_TYPE_OPPORTUNITY,
                CORE_Record_Id__c = currentRecord.Id,
                CORE_Record_Type__c = recordTypeDevNames.get(currentRecord.RecordTypeId)
            );
            
            deletedRecordsToUpsert.add(deletedRecord);
        }
    }
}