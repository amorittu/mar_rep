/**
 * Created by ftrapani on 18/11/2021.
 */
@IsTest
public with sharing class IT_UtilityTest {
    @IsTest
    static void allMethods(){
        CORE_DataFaker_CatalogHierarchy catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test1');
        catalogHierarchy.intake.CORE_Session__c = 'June';

        Test.startTest();
        Test.setMock(
                HttpCalloutMock.class, new IT_HTTPMockResponse(
                        200, 'OK', '\n' +
                                '{\n' +
                                '  "controllerValues" : {\n' +
                                '    "Prospect" : 0,\n' +
                                '    "Lead" : 1,\n' +
                                '    "Applicant" : 2,\n' +
                                '    "Admitted" : 3,\n' +
                                '    "Registered" : 4,\n' +
                                '    "Enrolled" : 5,\n' +
                                '    "Withdraw" : 6,\n' +
                                '    "Closed Won" : 7,\n' +
                                '    "Closed Lost" : 8\n' +
                                '  },\n' +
                                '  "defaultValue" : null,\n' +
                                '  "eTag" : "cab3b08974eb8f78f75143e3091b7494",\n' +
                                '  "url" : "/services/data/v53.0/ui-api/object-info/Opportunity/picklist-values/01209000000EuTIAA0/CORE_OPP_Sub_Status__c",\n' +
                                '  "values" : [ {\n' +
                                '    "attributes" : null,\n' +
                                '    "label" : "Prospect - New",\n' +
                                '    "validFor" : [ 0, 7 ],\n' +
                                '    "value" : "Prospect - New"\n' +
                                '  }, {\n' +
                                '    "attributes" : null,\n' +
                                '    "label" : "Prospect - Progressing",\n' +
                                '    "validFor" : [ 0, 7 ],\n' +
                                '    "value" : "Prospect - Progressing"\n' +
                                '  }, {\n' +
                                '    "attributes" : null,\n' +
                                '    "label" : "Prospect - Booking",\n' +
                                '    "validFor" : [ 0, 7 ],\n' +
                                '    "value" : "Prospect - Booking"\n' +
                                '  }, {\n' +
                                '    "attributes" : null,\n' +
                                '    "label" : "Prospect - Show",\n' +
                                '    "validFor" : [ 0, 7 ],\n' +
                                '    "value" : "Prospect - Show"\n' +
                                '  }, {\n' +
                                '    "attributes" : null,\n' +
                                '    "label" : "Prospect - Qualified For Intake / Waiting For Application",\n' +
                                '    "validFor" : [ 0, 7 ],\n' +
                                '    "value" : "Prospect - Qualified For Intake / Waiting For Application"\n' +
                                '  }, {\n' +
                                '    "attributes" : null,\n' +
                                '    "label" : "Prospect - Exit - Abandoned",\n' +
                                '    "validFor" : [ 0, 7 ],\n' +
                                '    "value" : "Prospect - Exit - Abandoned"\n' +
                                '  }, {\n' +
                                '    "attributes" : null,\n' +
                                '    "label" : "Prospect - Exit - Term Deferral",\n' +
                                '    "validFor" : [ 0, 7 ],\n' +
                                '    "value" : "Prospect - Exit - Term Deferral"\n' +
                                '  }, {\n' +
                                '    "attributes" : null,\n' +
                                '    "label" : "Prospect - Exit - Reoriented",\n' +
                                '    "validFor" : [ 0, 7 ],\n' +
                                '    "value" : "Prospect - Exit - Reoriented"\n' +
                                '  }, {\n' +
                                '    "attributes" : null,\n' +
                                '    "label" : "Lead - New",\n' +
                                '    "validFor" : [ 1, 7 ],\n' +
                                '    "value" : "Lead - New"\n' +
                                '  }, {\n' +
                                '    "attributes" : null,\n' +
                                '    "label" : "Lead - Booking",\n' +
                                '    "validFor" : [ 1, 7 ],\n' +
                                '    "value" : "Lead - Booking"\n' +
                                '  }, {\n' +
                                '    "attributes" : null,\n' +
                                '    "label" : "Lead - Show",\n' +
                                '    "validFor" : [ 1, 7 ],\n' +
                                '    "value" : "Lead - Show"\n' +
                                '  }, {\n' +
                                '    "attributes" : null,\n' +
                                '    "label" : "Lead - Qualified For Intake / Waiting For Application",\n' +
                                '    "validFor" : [ 1, 7 ],\n' +
                                '    "value" : "Lead - Qualified For Intake / Waiting For Application"\n' +
                                '  }, {\n' +
                                '    "attributes" : null,\n' +
                                '    "label" : "Lead - Exit - Abandoned",\n' +
                                '    "validFor" : [ 1, 7 ],\n' +
                                '    "value" : "Lead - Exit - Abandoned"\n' +
                                '  }, {\n' +
                                '    "attributes" : null,\n' +
                                '    "label" : "Lead - Exit - Term Deferral",\n' +
                                '    "validFor" : [ 1, 7 ],\n' +
                                '    "value" : "Lead - Exit - Term Deferral"\n' +
                                '  }, {\n' +
                                '    "attributes" : null,\n' +
                                '    "label" : "Lead - Exit - Reoriented",\n' +
                                '    "validFor" : [ 1, 7 ],\n' +
                                '    "value" : "Lead - Exit - Reoriented"\n' +
                                '  }, {\n' +
                                '    "attributes" : null,\n' +
                                '    "label" : "Applicant - Application Submitted / New",\n' +
                                '    "validFor" : [ 2, 7 ],\n' +
                                '    "value" : "Applicant - Application Submitted / New"\n' +
                                '  }, {\n' +
                                '    "attributes" : null,\n' +
                                '    "label" : "Applicant - Application Incomplete / Progressing",\n' +
                                '    "validFor" : [ 2, 7 ],\n' +
                                '    "value" : "Applicant - Application Incomplete / Progressing"\n' +
                                '  }, {\n' +
                                '    "attributes" : null,\n' +
                                '    "label" : "Applicant - Application Completed / Eligible",\n' +
                                '    "validFor" : [ 2, 7 ],\n' +
                                '    "value" : "Applicant - Application Completed / Eligible"\n' +
                                '  }, {\n' +
                                '    "attributes" : null,\n' +
                                '    "label" : "Applicant - Exit - Abandoned",\n' +
                                '    "validFor" : [ 2, 7 ],\n' +
                                '    "value" : "Applicant - Exit - Abandoned"\n' +
                                '  }, {\n' +
                                '    "attributes" : null,\n' +
                                '    "label" : "Applicant - Exit - Not Eligible",\n' +
                                '    "validFor" : [ 2, 7 ],\n' +
                                '    "value" : "Applicant - Exit - Not Eligible"\n' +
                                '  }, {\n' +
                                '    "attributes" : null,\n' +
                                '    "label" : "Applicant - Exit - Not Admitted",\n' +
                                '    "validFor" : [ 2, 7 ],\n' +
                                '    "value" : "Applicant - Exit - Not Admitted"\n' +
                                '  }, {\n' +
                                '    "attributes" : null,\n' +
                                '    "label" : "Applicant - Exit - Term Deferral",\n' +
                                '    "validFor" : [ 2, 7 ],\n' +
                                '    "value" : "Applicant - Exit - Term Deferral"\n' +
                                '  }, {\n' +
                                '    "attributes" : null,\n' +
                                '    "label" : "Applicant - Exit - Reoriented",\n' +
                                '    "validFor" : [ 2, 7 ],\n' +
                                '    "value" : "Applicant - Exit - Reoriented"\n' +
                                '  }, {\n' +
                                '    "attributes" : null,\n' +
                                '    "label" : "Admitted - Admit",\n' +
                                '    "validFor" : [ 3, 7 ],\n' +
                                '    "value" : "Admitted - Admit"\n' +
                                '  }, {\n' +
                                '    "attributes" : null,\n' +
                                '    "label" : "Admitted - Exit - Abandoned",\n' +
                                '    "validFor" : [ 3, 7 ],\n' +
                                '    "value" : "Admitted - Exit - Abandoned"\n' +
                                '  }, {\n' +
                                '    "attributes" : null,\n' +
                                '    "label" : "Admitted - Exit - Term Deferral",\n' +
                                '    "validFor" : [ 3, 7 ],\n' +
                                '    "value" : "Admitted - Exit - Term Deferral"\n' +
                                '  }, {\n' +
                                '    "attributes" : null,\n' +
                                '    "label" : "Admitted - Exit - Reoriented",\n' +
                                '    "validFor" : [ 3, 7 ],\n' +
                                '    "value" : "Admitted - Exit - Reoriented"\n' +
                                '  }, {\n' +
                                '    "attributes" : null,\n' +
                                '    "label" : "Registered - Classical",\n' +
                                '    "validFor" : [ 4, 7 ],\n' +
                                '    "value" : "Registered - Classical"\n' +
                                '  }, {\n' +
                                '    "attributes" : null,\n' +
                                '    "label" : "Registered - Tuition Fees payments",\n' +
                                '    "validFor" : [ 4, 7 ],\n' +
                                '    "value" : "Registered - Tuition Fees payments"\n' +
                                '  }, {\n' +
                                '    "attributes" : null,\n' +
                                '    "label" : "Registered - Fees Fully Paid",\n' +
                                '    "validFor" : [ 4, 7 ],\n' +
                                '    "value" : "Registered - Fees Fully Paid"\n' +
                                '  }, {\n' +
                                '    "attributes" : null,\n' +
                                '    "label" : "Registered - Exit - Reoriented",\n' +
                                '    "validFor" : [ 4, 7 ],\n' +
                                '    "value" : "Registered - Exit - Reoriented"\n' +
                                '  }, {\n' +
                                '    "attributes" : null,\n' +
                                '    "label" : "Registered - Exit - Abandoned",\n' +
                                '    "validFor" : [ 4, 7 ],\n' +
                                '    "value" : "Registered - Exit - Abandoned"\n' +
                                '  }, {\n' +
                                '    "attributes" : null,\n' +
                                '    "label" : "Registered - Exit - Cancelled reimbursement",\n' +
                                '    "validFor" : [ 4, 7 ],\n' +
                                '    "value" : "Registered - Exit - Cancelled reimbursement"\n' +
                                '  }, {\n' +
                                '    "attributes" : null,\n' +
                                '    "label" : "Enrolled - Classical",\n' +
                                '    "validFor" : [ 5, 7 ],\n' +
                                '    "value" : "Enrolled - Classical"\n' +
                                '  }, {\n' +
                                '    "attributes" : null,\n' +
                                '    "label" : "Closed Won",\n' +
                                '    "validFor" : [ 7 ],\n' +
                                '    "value" : "Closed Won"\n' +
                                '  }, {\n' +
                                '    "attributes" : null,\n' +
                                '    "label" : "Closed Lost",\n' +
                                '    "validFor" : [ 8 ],\n' +
                                '    "value" : "Closed Lost"\n' +
                                '  } ]\n' +
                                '}', null
                )
        );
        Test.stopTest();

        Id enrollmentRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('CORE_Enrollment').getRecordTypeId();

        Map<String,String> getPicklistLabel = IT_Utility.getPicklistLabel('Opportunity','Stagename');
        Map<String,IT_Role_Type__mdt> getRoleTypeMDT = IT_Utility.getRoleTypeMDT2();
        Map<String,String> getCapturechannel4AssignOwner = IT_Utility.getCapturechannel4AssignOwner();
        Map<String,String> getSenderEmail = IT_Utility.getSenderEmail();
        Map<String,Decimal> getOptyOrderStatus = IT_Utility.getOptyOrderStatus();
        Map<String,Decimal> getOptyOrderSubStatus = IT_Utility.getOptyOrderSubStatus();
        IT_Utility.getFieldDependencies('Opportunity','StageName','CORE_OPP_Sub_Status__c');
        IT_Utility.generateNumberByMonth();
        IT_Utility.isIntakeStarted(catalogHierarchy.intake, '8');
        IT_Utility.courseStartDate(catalogHierarchy.intake, '8');
        IT_Utility.getPicklistOptions('Opportunity', new String[] {'StageName'});
        IT_Utility.getPicklistValuesByRecordType(enrollmentRecordTypeId, 'Opportunity', 'CORE_OPP_Sub_Status__c');
    }
}