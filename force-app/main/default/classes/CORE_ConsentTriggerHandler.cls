public with sharing class CORE_ConsentTriggerHandler {
    @TestVisible
    private static String testScope = 'CORE_PKL_BusinessUnit';

    public static void handleTrigger(List<CORE_Consent__c> oldRecords, List<CORE_Consent__c> newRecords, System.TriggerOperation triggerEvent) {
        CORE_Consent_Setting__mdt consentMdt;
        String consentScope = 'CORE_PKL_BusinessUnit';
        if(Test.isRunningTest()){
            consentScope = testScope;
        } else {
            consentMdt = [SELECT CORE_Scope__c
                FROM CORE_Consent_Setting__mdt LIMIT 1];
            if(consentMdt != null && !String.isEmpty(consentMdt.CORE_Scope__c)){
                consentScope = consentMdt.CORE_Scope__c;
            }
        }

        switch on triggerEvent {
            when BEFORE_INSERT {
                beforeInsert(newRecords, consentScope);
            }
           /* when AFTER_INSERT {
                afterInsert(newRecords);
            }*/
            when BEFORE_UPDATE {
                beforeUpdate(oldRecords, newRecords, consentScope);
            }
           /* when AFTER_UPDATE {
                afterUpdate(oldRecords, newRecords);
            }*/
        }
    }

    private static void beforeInsert(List<CORE_Consent__c> newRecords, String consentScope) {
        updateConsentsScope(newRecords, consentScope);
        updateInternalReference(newRecords);
    }

    private static void beforeUpdate(List<CORE_Consent__c> oldRecords, List<CORE_Consent__c> newRecords, String consentScope) {
        Map<Id, CORE_Consent__c> oldRecordsMap = new Map<Id, CORE_Consent__c> (oldRecords);
        List<CORE_Consent__c> consentsToUpdateRef = new List<CORE_Consent__c>();
        List<CORE_Consent__c> consentsToUpdateScope = new List<CORE_Consent__c>();
        for(CORE_Consent__c newConsent : newRecords){
            CORE_Consent__c oldConsent = oldRecordsMap.get(newConsent.Id);
            if(mustUpdateInternalReference(oldConsent, newConsent)){
                consentsToUpdateRef.add(newConsent);
            }
            if(newConsent.CORE_Scope__c != consentScope){
                consentsToUpdateScope.add(newConsent);
            }
        }

        updateInternalReference(consentsToUpdateRef);
        updateConsentsScope(consentsToUpdateScope,consentScope);
    }

    /*private static void afterInsert(List<CORE_Consent__c> newRecords) {
        System.debug('Start Consent after insert trigger');
        List<String> paIds = new List<String>();

        for(CORE_Consent__c newConsent : newRecords){
            paIds.Add(newConsent.CORE_Person_Account__c);
        }

        CompleteOpp(newRecords, paIds);
    }

    private static void afterUpdate(List<CORE_Consent__c> oldRecords, List<CORE_Consent__c> newRecords) {
        System.debug('Start Consent after update trigger');
        List<String> paIds = new List<String>();

        for(CORE_Consent__c newConsent : newRecords){
            CORE_Consent__c oldConsent = oldRecords.Get(newRecords.IndexOf(newConsent));

            if(newConsent.CORE_Opt_out__c == true &&  oldConsent.CORE_Opt_out__c == false || // If Opt-out became true
            newConsent.CORE_Opt_in__c == true &&  oldConsent.CORE_Opt_in__c == false){       // If Opt-in became true
                paIds.Add(newConsent.CORE_Person_Account__c);
            }
        }

        CompleteOpp(newRecords, paIds);
    }*/

    /*private static void CompleteOpp(List<CORE_Consent__c> newRecords, List<String> paIds){
        List<Opportunity> opportunities = [SELECT Id, CORE_OPP_Division__c, CORE_OPP_Business_Unit__c, AccountId, CORE_Opt_out_channels__c, CORE_OPP_Academic_Year__c, CORE_Opt_out_channels_Marketing__c, CORE_BU_Brand__c FROM Opportunity WHERE AccountId IN :paIds];

        for(CORE_Consent__c consent : newRecords){
            for(Opportunity opp : opportunities){
                if (opp.CORE_OPP_Business_Unit__c == consent.CORE_Business_Unit__c && consent.CORE_Business_Unit__c != null &&
                opp.AccountId == consent.CORE_Person_Account__c && consent.CORE_Person_Account__c != null &&
                (consent.CORE_Scope__c == 'CORE_PKL_BusinessUnit' || consent.CORE_Scope__c == null)){
                    if (consent.CORE_Opt_in__c == true){
                        updateOptInStatus(opp, consent);
                    }
                    else {
                        updateOptOutStatus(opp, consent, opportunities);
                    }
                }
                else if (consent.CORE_Scope__c == 'CORE_PKL_Brand' && consent.CORE_Brand__c == opp.CORE_BU_Brand__c &&
                opp.AccountId == consent.CORE_Person_Account__c && consent.CORE_Person_Account__c != null){
                    if (consent.CORE_Opt_in__c == true){
                        updateOptInStatus(opp, consent);
                    }
                    else {
                        updateOptOutStatus(opp, consent, opportunities);
                    }
                }
                else if (opp.CORE_OPP_Division__c == consent.CORE_Division__c && consent.CORE_Division__c != null &&
                opp.AccountId == consent.CORE_Person_Account__c && consent.CORE_Person_Account__c != null &&
                consent.CORE_Scope__c == 'CORE_PKL_Division'){
                    if (consent.CORE_Opt_in__c == true){
                        updateOptInStatus(opp, consent);
                    }
                    else {
                        updateOptOutStatus(opp, consent, opportunities);
                    }
                }
            }
        }
        CORE_TriggerUtils.setBypassTrigger();
        update opportunities;
        CORE_TriggerUtils.setBypassTrigger();
    }

    private static void updateOptOutStatus(Opportunity oppToUpdate, CORE_Consent__c newConsent, List<Opportunity> opportunities){
        if (newConsent.CORE_Consent_type__c == 'CORE_PKL_Authorisation_to_be_contacted_by_channel' ||
        newConsent.CORE_Consent_type__c == 'CORE_PKL_Global_consent'){
            if (oppToUpdate.CORE_Opt_out_channels__c == null){
                oppToUpdate.CORE_Opt_out_channels__c = newConsent.CORE_Consent_channel__c;
            }
            else if (!oppToUpdate.CORE_Opt_out_channels__c.Contains(newConsent.CORE_Consent_channel__c)){
                oppToUpdate.CORE_Opt_out_channels__c = oppToUpdate.CORE_Opt_out_channels__c + ';' + newConsent.CORE_Consent_channel__c;
            }
        }
        if (newConsent.CORE_Consent_type__c == 'CORE_PKL_Authorisation_to_be_contacted_by_channel_marketing' ||
        newConsent.CORE_Consent_type__c == 'CORE_PKL_Global_consent'){
            if (oppToUpdate.CORE_Opt_out_channels_Marketing__c == null){
                oppToUpdate.CORE_Opt_out_channels_Marketing__c = newConsent.CORE_Consent_channel__c;
            }
            else if (!oppToUpdate.CORE_Opt_out_channels_Marketing__c.Contains(newConsent.CORE_Consent_channel__c)){
                oppToUpdate.CORE_Opt_out_channels_Marketing__c = oppToUpdate.CORE_Opt_out_channels_Marketing__c + ';' + newConsent.CORE_Consent_channel__c;
            }
        }
    }

    private static void updateOptInStatus(Opportunity oppToUpdate, CORE_Consent__c newConsent){
        if (newConsent.CORE_Consent_type__c == 'CORE_PKL_Authorisation_to_be_contacted_by_channel' ||
        newConsent.CORE_Consent_type__c == 'CORE_PKL_Global_consent'){
            if (oppToUpdate.CORE_Opt_out_channels__c != null &&
            oppToUpdate.CORE_Opt_out_channels__c.Contains(newConsent.CORE_Consent_channel__c)){
                oppToUpdate.CORE_Opt_out_channels__c = oppToUpdate.CORE_Opt_out_channels__c.Replace(newConsent.CORE_Consent_channel__c, '');
            }
        }
        if (newConsent.CORE_Consent_type__c == 'CORE_PKL_Authorisation_to_be_contacted_by_channel_marketing' ||
        newConsent.CORE_Consent_type__c == 'CORE_PKL_Global_consent'){
            if (oppToUpdate.CORE_Opt_out_channels_Marketing__c != null &&
            oppToUpdate.CORE_Opt_out_channels_Marketing__c.Contains(newConsent.CORE_Consent_channel__c)){
                oppToUpdate.CORE_Opt_out_channels_Marketing__c = oppToUpdate.CORE_Opt_out_channels_Marketing__c.Replace(newConsent.CORE_Consent_channel__c, '');
            }
        }
    }*/

    private static Boolean mustUpdateInternalReference(CORE_Consent__c oldConsent, CORE_Consent__c newConsent){
        if(String.isEmpty(oldConsent.CORE_TECH_InternalReference__c) ||
            oldConsent.CORE_Person_Account__c != newConsent.CORE_Person_Account__c ||
            oldConsent.CORE_Business_Unit__c != newConsent.CORE_Business_Unit__c ||
            oldConsent.CORE_Division__c != newConsent.CORE_Division__c ||
            oldConsent.CORE_Brand__c != newConsent.CORE_Brand__c ||
            oldConsent.CORE_Consent_type__c != newConsent.CORE_Consent_type__c ||
            oldConsent.CORE_Consent_channel__c != newConsent.CORE_Consent_channel__c
        ) {
            return true;
        } else {
            return false;
        }
    }

    private static void updateConsentsScope(List<CORE_Consent__c> consents, String consentScope){
        for(CORE_Consent__c consent : consents){
            consent.CORE_Scope__c = consentScope;
        }
    }

    public static void updateInternalReference(List<CORE_Consent__c> consents){
        Set<Id> businessUnitIds = new Set<Id>();
        for(CORE_Consent__c consent : consents){
            businessUnitIds.add(consent.CORE_Business_Unit__c);
        }

        List<CORE_Business_Unit__c> businessUnits = [Select Id, CORE_Brand__c FROM CORE_Business_Unit__c where id IN :businessUnitIds];
        Map<Id, CORE_Business_Unit__c> buByIds = new Map<Id,CORE_Business_Unit__c>(businessUnits);

        for(CORE_Consent__c consent : consents){
            String brand = consent.CORE_Business_Unit__c == null || buByIds.get(consent.CORE_Business_Unit__c) == null ? null : buByIds.get(consent.CORE_Business_Unit__c).CORE_Brand__c;
            consent.CORE_TECH_InternalReference__c = getInternalReference(consent, brand);
        }
    }

    public static String getInternalReference(CORE_Consent__c consent, String brand){
        String scopeValue = '';
        switch on consent.CORE_Scope__c {
            when 'CORE_PKL_Division' {
                scopeValue = consent.CORE_Division__c;
            }
            when 'CORE_PKL_Brand' {
                scopeValue = brand;
            }
            when else {
                scopeValue = consent.CORE_Business_Unit__c;
            }
        }

        return consent.CORE_Person_Account__c 
            + '_' +  scopeValue 
            + consent.CORE_Consent_type__c 
            + consent.CORE_Consent_channel__c;
    }
}