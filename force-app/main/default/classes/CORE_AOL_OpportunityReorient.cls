@RestResource(urlMapping='/aol/opportunity/re-orient')
global without sharing class CORE_AOL_OpportunityReorient {
    public static CORE_AOL_Wrapper.ResultWrapper result { get; set; }
    private static final String SUCCESS_MESSAGE = 'AOL reorient & progression fiedls upserted with success';
    
    @TestVisible
    private CORE_OpportunityDataMaker dataMaker;
    @TestVisible
    private CORE_AOL_GlobalWorker aolWorker;
    @TestVisible
    private String formerAolExternalId;
    @TestVisible
    private String newAolExternalId;
    @TestVisible
    private CORE_AOL_Wrapper.InterestWrapper interest;

    public CORE_AOL_OpportunityReorient(CORE_AOL_GlobalWorker aolWorker, String formerAolExternalId, String newAolExternalId, CORE_AOL_Wrapper.InterestWrapper interest){
        this.dataMaker = new CORE_OpportunityDataMaker();
        this.aolWorker = aolWorker;
        this.formerAolExternalId = formerAolExternalId; 
        this.newAolExternalId = newAolExternalId;
        this.interest = interest;
    }

    @HttpPut
    global static CORE_AOL_Wrapper.ResultWrapper execute(String formerAolExternalId, 
                                    String newAolExternalId, 
                                    CORE_AOL_Wrapper.InterestWrapper interest,
                                    CORE_AOL_Wrapper.AOLProgressionFields aolProgressionFields) {
                               
        Savepoint sp = Database.setSavepoint();

        // Initiating logs
        Long dt1 = DateTime.now().getTime();
        Long dt2;
        CORE_LogManager logSettings;
        CORE_LOG__c log = new CORE_LOG__c(CORE_API_Name__c = 'AOL');
        Boolean isErrorLog = false;
        logSettings = new CORE_LogManager('AOL');

        CORE_AOL_Wrapper.ResultWrapper result = checkMandatoryParameters(formerAolExternalId, newAolExternalId, interest);

        try{
            if(result != null){
                //missing parameter(s)
                return result;
            }
            CORE_AOL_GlobalWorker aolWorker = new CORE_AOL_GlobalWorker(aolProgressionFields,formerAolExternalId);
            Opportunity opportunity = aolWorker.aolOpportunity;

            if(opportunity == null){
                result = new CORE_AOL_Wrapper.ResultWrapper(
                    CORE_AOL_Constants.ERROR_STATUS, 
                    String.format(CORE_AOL_Constants.MSG_OPPORTUNITY_NOT_FOUND, new List<String>{formerAolExternalId}),
                    null
                );
            } else {
                CORE_AOL_OpportunityReorient process = new CORE_AOL_OpportunityReorient(aolWorker, formerAolExternalId, newAolExternalId, interest);
                Opportunity newOpportunity = process.reorient(true);

                CORE_AOL_Wrapper.OpportunityWrapper oppWrapper = new CORE_AOL_Wrapper.OpportunityWrapper(newOpportunity);

                result = new CORE_AOL_Wrapper.ResultWrapper(CORE_AOL_Constants.SUCCESS_STATUS, SUCCESS_MESSAGE, oppWrapper);
            }
        } catch(Exception e){
            result = new CORE_AOL_Wrapper.ResultWrapper(CORE_AOL_Constants.ERROR_STATUS, e.getMessage() + ' at ' + e.getStackTraceString(),null);
            Database.rollback(sp);
        } finally{
            dt2 = DateTime.now().getTime();
            System.debug('CORE_AOL_AcademicDiplomaHistory(urlMapping=\'/aol/academic-diploma-history\') : T0TAL TIME OF EXECUTION = ' + (dt2-dt1) + ' ms');
            // creating log
            log.CORE_Processing_Duration__c =(dt2-dt1);
            log.CORE_HTTP_Status_Code__c = result.statut == 'KO' ? 400 : result.statut == 'OK' ? 200 : 500;
            log.CORE_Sent_Body__c = JSON.serialize(result);
            String responseJSON = '{"formerAolExternalId" : ' + formerAolExternalId + '","newAolExternalId" : "' + newAolExternalId + '",';
            responseJSON += '"interest" : ' + JSON.serialize(interest) + ',"aolProgressionFields" : ' + JSON.serialize(aolProgressionFields) + '}';
            log.CORE_Received_Body__c =  responseJSON;
            
            if(!logSettings.isNone()){
                if(logSettings.isAllLogged()){ // is All logged
                    CORE_LogManager.createLog(log);
                }else if(logSettings.isErrorLogged() && isErrorLog){ // is Error
                    CORE_LogManager.createLog(log);
                }
            }
        }

        return result;
    }

    public Opportunity reorient(boolean updateProgressionFields){

        CORE_Intake__c intake = CORE_AOL_GlobalWorker.getIntakeFromExternalId(this.interest.intake);
        if(intake == null){
            throw new IllegalArgumentException('Intake not found.');
        }
        String targetAcademicYear = intake.CORE_Academic_Year__c;
        System.debug('@reorient > targetAcademicYear = ' + targetAcademicYear);

        Opportunity mainOpportunity = CORE_AOL_GlobalWorker.getExistingMainOpp(this.aolWorker.aolOpportunity.AccountId, this.interest.businessUnit, targetAcademicYear);
        System.debug('@reorient > mainOpportunity = ' + mainOpportunity);
        Boolean isExisingOpenMainOpp = mainOpportunity == null ? false : true;

        opportunity newOpportunity;
        Boolean updateFormerAolOpp = true;
        Boolean insertOppLineItem = true;
        boolean duplicateRelatedRecords = true;
        boolean upsertOpportunity = true;

        if(!isExisingOpenMainOpp){
            System.debug('@reorient > reorientInternally');
            newOpportunity = reorientInternally();
        } else {
            if(mainOpportunity.CORE_Main_Product_Interest__r.CORE_Product_Type__c != CORE_AOL_Constants.PKL_PRODUCT_TYPE){
                System.debug('@reorient > reorientByUpdate');

                newOpportunity = reorientByUpdate(mainOpportunity);
                duplicateRelatedRecords = false;
            } else {
                System.debug('@reorient > createOpportunityProcess');
                this.interest.isMain = true;
                this.interest.personAccountId = this.aolWorker.aolOpportunity.AccountId;
                newOpportunity = CORE_AOL_Opportunity.createOpportunityProcess(this.newAolExternalId, this.interest, this.aolWorker.aolProgressionFields);
                newOpportunity.OwnerId = mainOpportunity.OwnerId;
                updateFormerAolOpp = false;
                insertOppLineItem = false;
                duplicateRelatedRecords = false;
                //upsertOpportunity = false;
                updateProgressionFields = false;
            }
        }

        if(updateProgressionFields){
            newOpportunity = CORE_AOL_GlobalWorker.getAolProgressionFields(newOpportunity, this.aolWorker.aolProgressionFields);
        }

        System.debug('@reorient > newOpportunity = ' + newOpportunity);

        if(upsertOpportunity){
            upsert newOpportunity;
        }

        if(insertOppLineItem){
            CORE_AOL_ChangeInterest.getOppLineItem(this.datamaker, newOpportunity, true);
        }
        
        if(duplicateRelatedRecords){
            duplicateRelatedDatas(this.aolWorker.aolOpportunity.Id, newOpportunity.Id);
        }

        if(updateFormerAolOpp){
            this.aolWorker.aolOpportunity.CORE_OPP_Parent_Opportunity__c = newOpportunity.Id;
            update this.aolWorker.aolOpportunity;
        }

        return newOpportunity;
    }

    @TestVisible
    private static Opportunity moveOpportunityToExitReoriented(Opportunity opportunity){
        opportunity.CORE_OPP_Sub_Status__c = String.format(CORE_AOL_Constants.PKL_OPP_SUBSTATUS_REORIENTED, new List<String> {opportunity.StageName});
        
        return opportunity;
    }
    public Opportunity reorientInternally(){
        Opportunity oldOpportunity = this.aolWorker.aolOpportunity;
        Opportunity newOpportunity = oldOpportunity.clone(false, true, false, false);
        newOpportunity = CORE_AOL_GlobalWorker.removeUniqueFields(newOpportunity);

        oldOpportunity = moveOpportunityToExitReoriented(oldOpportunity);
        newOpportunity.CORE_Business_Unit_Before_Re_Orientation__c = oldOpportunity.CORE_OPP_Business_Unit__c;
        newOpportunity.CORE_OPP_Application_Online__c = true;
        newOpportunity.CORE_OPP_Application_Online_ID__c = this.newAolExternalId;

        newOpportunity.CORE_Is_AOL_Register__c = true;
        newOpportunity.CORE_AOL_Re_Orientation__c = true;
        newOpportunity.CloseDate = oldOpportunity.CloseDate;
        CORE_CatalogHierarchyModel catalogHierarchy = CORE_AOL_ChangeInterest.getCatalogHierarchy(this.interest);

        newOpportunity = this.dataMaker.initOpportunityCatalog(newOpportunity, catalogHierarchy);
        newOpportunity.OwnerId = catalogHierarchy.businessUnit.CORE_BU_Technical_User__c;
        newOpportunity.CORE_OPP_Academic_Year__c = catalogHierarchy.intake.CORE_Academic_Year__c;

        //TODO finish

        return newOpportunity;
    }

    @TestVisible
    private Opportunity reorientByUpdate(Opportunity newOpportunity){
        Opportunity oldOpportunity = this.aolWorker.aolOpportunity;

        oldOpportunity = moveOpportunityToExitReoriented(oldOpportunity);
        newOpportunity.CORE_AOL_Re_Orientation__c = true;
        newOpportunity.CORE_Business_Unit_Before_Re_Orientation__c = oldOpportunity.CORE_OPP_Business_Unit__c;

        CORE_CatalogHierarchyModel catalogHierarchy = CORE_AOL_ChangeInterest.getCatalogHierarchy(this.interest);

        newOpportunity = this.dataMaker.initOpportunityCatalog(newOpportunity, catalogHierarchy);
        newOpportunity.CORE_OPP_Application_Online__c = true;
        newOpportunity.CORE_Is_AOL_Register__c = true;
        newOpportunity.CORE_Is_AOL_Retail_Register__c = this.interest.isAolRetailRegistered == null ? false : this.interest.isAolRetailRegistered;
        newOpportunity.CORE_OPP_Application_Online_ID__c = this.newAolExternalId;
        newOpportunity.CORE_OPP_Application_started__c = System.now();

        return newOpportunity;
    }
    
    @TestVisible
    private static CORE_AOL_Wrapper.ResultWrapper checkMandatoryParameters(String formerAolExternalId,String newAolExternalId, CORE_AOL_Wrapper.InterestWrapper interest){
        List<String> missingParameters = new List<String>();
        Boolean isMissingFormerAolId = CORE_AOL_GlobalWorker.isAolExternalIdParameterMissing(formerAolExternalId);
        Boolean isMissingNewAolId = CORE_AOL_GlobalWorker.isAolExternalIdParameterMissing(newAolExternalId);

        if(isMissingFormerAolId){
            missingParameters.add('formerAolExternalId');
        }
        if(isMissingNewAolId){
            missingParameters.add('newAolExternalId');
        }
        
        missingParameters.addAll(CORE_AOL_ChangeInterest.getInterestMissingParameters(interest));

        CORE_AOL_Wrapper.ResultWrapperGlobal resultTmp = CORE_AOL_GlobalWorker.getMissingParameters(missingParameters);

        if(resultTmp == null){
            return null;
        } else {
            CORE_AOL_Wrapper.ResultWrapper result = new CORE_AOL_Wrapper.ResultWrapper(resultTmp.status, resultTmp.message, null);
            return result;
        }
    }

    @TestVisible
    private static void duplicateRelatedDatas(Id formerOpp, Id newOpp){
        List<CORE_Sub_Status_Timestamp_History__c> subStatus = CORE_AOL_QueryProvider.getSubStatus(formerOpp);

        CORE_OpportunityUtils.getDuplicatedSubStatusTimeStamps(subStatus, newOpp);

        asyncDuplication(formerOpp, newOpp);
    }

    @TestVisible
    @future
    private static void asyncDuplication(Id formerOpp, Id newOpp){
        List<CORE_Status_Timestamp_History__c> status = CORE_AOL_QueryProvider.getStatus(formerOpp);
        List<ContentDocumentLink> attachments = CORE_AOL_QueryProvider.getAttachments(formerOpp);
        List<OpportunityContactRole> contactRoles = CORE_AOL_QueryProvider.getOpportunityContactRoles(formerOpp);
        List<CORE_Point_of_Contact__c> pointOfContacts = CORE_AOL_QueryProvider.getPointOfContacts(formerOpp);

        CORE_OpportunityUtils.getDuplicatedAttachments(attachments, newOpp);
        CORE_OpportunityUtils.getDuplicatedContactRoles(contactRoles, newOpp);
        CORE_OpportunityUtils.getDuplicatedStatusTimeStamps(status, newOpp);
        CORE_OpportunityUtils.getDuplicatedPointOfContacts(pointOfContacts, newOpp);
    }
}