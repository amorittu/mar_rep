global class CORE_Flow_DuplicateOppTimestamps {

    @InvocableMethod
    global static List<String> duplicateSubStatusTimestamps(List<OppTimestampParameters> params) {
        //system.debug('@invocable updateOpportunity: ' + params);

        if (params != null) {
            Savepoint sp = Database.setSavepoint();

            List<Database.SaveResult> results = null;

            try {
                //Get old & new Opportunity
                Opportunity oldOpp = null;
                Opportunity newOpp = null;

                for(Opportunity opp : [SELECT Id, StageName, CORE_OPP_Sub_Status__c
                                        FROM Opportunity
                                        WHERE Id = :params[0].oldOppId
                                        OR Id = :params[0].newOppId]){

                    if(opp.Id == params[0].oldOppId){
                        oldOpp = opp;
                    }
                    else{
                        newOpp = opp;
                    }
                }

                cloneOppSubStatus(oldOpp, newOpp);

            } catch (Exception e) {
                system.debug('Error: ' + e.getMessage() + ' => ' + e.getStackTraceString());
                Database.rollback(sp);

                throw new OppTimestampsException('Error: ' + e.getMessage() + ' => ' + e.getStackTraceString());
            }
        }

        return null;
    }

    public static Map<String, Decimal> getOppStatusLevels(){
        Map<String, Decimal> statusLevelMap = new Map<String, Decimal>();

        for(OpportunityStage oppStatus : [SELECT Id, DefaultProbability, ApiName, MasterLabel FROM OpportunityStage WHERE IsActive = TRUE]){
            statusLevelMap.put(oppStatus.ApiName, oppStatus.DefaultProbability);
        }

        //system.debug('@getOppStatusLevels > statusLevelMap: ' + JSON.serializePretty(statusLevelMap));

        return statusLevelMap;
    }

    public static void cloneOppSubStatus(Opportunity oldOpp, Opportunity newOpp){
        //system.debug('@cloneOppSubStatus > newOpp.StageName: ' + newOpp.StageName);
        Map<String, Decimal> statusLevelMap = getOppStatusLevels();
        Decimal oldStatusLevel = 0;
        Decimal newStatusLevel = statusLevelMap.get(newOpp.StageName);
        //system.debug('@cloneOppSubStatus > newStatusLevel: ' + newStatusLevel);

        List<CORE_Sub_Status_Timestamp_History__c> subStatusTimestampsList = new List<CORE_Sub_Status_Timestamp_History__c>();

        //Get old Opportunity Sub-status Timestamps
        for(CORE_Sub_Status_Timestamp_History__c sst : [SELECT Id, CORE_Sub_status__c, CORE_Status__c, CORE_Modification_Sub_status_Date_time__c
                                                        FROM CORE_Sub_Status_Timestamp_History__c
                                                        WHERE CORE_Opportunity__c = :oldOpp.Id]){

            system.debug('@cloneOppSubStatus > sst.CORE_Status__c: ' + sst.CORE_Status__c);
            system.debug('@cloneOppSubStatus > sst.CORE_Sub_status__c: ' + sst.CORE_Sub_status__c);

            oldStatusLevel = statusLevelMap.get(sst.CORE_Status__c);
            system.debug('@cloneOppSubStatus > oldStatusLevel: ' + oldStatusLevel);
            system.debug('@cloneOppSubStatus > oldStatusLevel <= newStatusLevel: ' + (oldStatusLevel <= newStatusLevel));

            if(oldStatusLevel <= newStatusLevel){
                subStatusTimestampsList.add(sst);
            }
        }

        //system.debug('@cloneOppSubStatus > subStatusTimestampsList: ' + JSON.serializePretty(subStatusTimestampsList));

        if(subStatusTimestampsList.size() > 0){
            duplicateSubStatusTimestamps(subStatusTimestampsList, newOpp.Id);
        }
    }

    private static void duplicateSubStatusTimestamps(List<CORE_Sub_Status_Timestamp_History__c> subStatusTimestampsList, Id oppId){
        List<CORE_Sub_Status_Timestamp_History__c> duplicates = new List<CORE_Sub_Status_Timestamp_History__c>();

        for(CORE_Sub_Status_Timestamp_History__c subStatus : subStatusTimestampsList){
            CORE_Sub_Status_Timestamp_History__c subStatusTmp = subStatus.clone(false, true, true, false);
            subStatusTmp.Id = null;
            subStatusTmp.CORE_External_Id__c = null;
            subStatusTmp.CORE_Opportunity__c = oppId;
            duplicates.add(subStatusTmp);
        }

        database.insert(duplicates, true);
    }

    global class OppTimestampParameters {

        @InvocableVariable(required=true)
        global Id oldOppId;

        @InvocableVariable(required=true)
        global Id newOppId;

    }

    class OppTimestampsException extends Exception {}
}