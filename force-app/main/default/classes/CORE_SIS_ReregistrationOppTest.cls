@isTest
private class CORE_SIS_ReregistrationOppTest {

    @testSetup 
    static void createData(){
        Id profileId = [SELECT Id FROM Profile WHERE Name = 'CORE_API_Profile' LIMIT 1].Id;

        User usr = new User(
            Username = 'usertest@sistest.com',
            LastName = 'TEST_USER',
            Email = 'testemail@yopmail.com',
            Alias = 'T', 
            CommunityNickname = 'T',
            TimeZoneSidKey = 'Europe/Paris',
            LocaleSidKey = 'fr_FR_EURO',
            EmailEncodingKey = 'ISO-8859-1', 
            ProfileId = profileId,
            LanguageLocaleKey = 'fr'      
        );
        database.insert(usr, true);

        Account account = CORE_DataFaker_Account.getStudentAccount('test1');
        CORE_CatalogHierarchyModel catalogHierarchy= new CORE_DataFaker_CatalogHierarchy('1234').catalogHierarchy;
        Opportunity opp1 = new Opportunity(
            Name ='test',
            StageName = CORE_SIS_Constants.PKL_OPP_STAGENAME_PROSPECT,
            CORE_OPP_Sub_Status__c = CORE_SIS_Constants.PKL_OPP_SUBSTATUS_PROSPECTAPPSTARTED,
            CORE_OPP_Business_Unit__c = catalogHierarchy.businessUnit.Id,
            CORE_OPP_Division__c = catalogHierarchy.division.Id,
            AccountId = account.Id,
            CloseDate = Date.Today().addDays(2)
            
        );
        database.insert(opp1, true);
        Id pricebookId = CORE_DataFaker_PriceBook.getStandardPricebookId();
        PricebookEntry pbe1 = CORE_DataFaker_PriceBook.getPriceBookEntry(catalogHierarchy.product.Id, pricebookId, 100.0);
        //CORE_PriceBook_BU__c pbu1 = CORE_DataFaker_PriceBook.getPriceBookBu(pricebookId, catalog.businessUnit.Id, '1234');

        CORE_PriceBook_BU__c pricebookBu = CORE_DataFaker_PriceBook.getPriceBookBuWithoutInsert(pricebookId, catalogHierarchy.businessUnit.Id, '1234');
        pricebookBu.CORE_Academic_Year__c = 'CORE_PKL_2023-2024';

        insert pricebookBu;


    }
    @IsTest
    private static void testPostPartial(){
        User usr = [SELECT Id FROM User WHERE Username = 'usertest@sistest.com' LIMIT 1];

        List<PermissionSetAssignment> psaList = new List<PermissionSetAssignment>();

        for(PermissionSet ps : [SELECT Id FROM PermissionSet WHERE Name = 'CORE_API_SIS' OR Name = 'CORE_LeadFormAcquisition']){
            psaList.add(
                new PermissionSetAssignment(AssigneeId = usr.id, PermissionSetId = ps.Id)
            );
        }
        
        database.insert(psaList, true);

        System.runAs(usr){
            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
            req.requestURI = '/services/apexrest/sis/opportunity/re-registration-opportunity';
            req.httpMethod = 'POST';

            RestContext.request = req;
            RestContext.response= res;

            Account account = [SELECT Id FROM Account LIMIT 1];
            CORE_Business_Unit__c bu = [SELECT Id,CORE_Business_Unit_ExternalId__c FROM CORE_Business_Unit__c  LIMIT 1];
            Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];
            CORE_SIS_Wrapper.OpportunityWrapper sisrereg= new CORE_SIS_Wrapper.OpportunityWrapper(opp.Id,'Test12345',account.Id,'CORE_PKL_2021-2022',bu.CORE_Business_Unit_ExternalId__c,'', '', '','','LEAD','LEAD - New');
            //CORE_SIS_Wrapper.ResultWrapperGlobal result = CORE_SIS_ReregistrationOpp.opportunity(sisrereg);
           // System.assertEquals('OK', result.status);
            test.startTest();
            CORE_SIS_ReregistrationOpp.opportunity(sisrereg);
            test.stopTest();
        }
    }
    @isTest
    private static void testPostFull(){
        User usr = [SELECT Id FROM User WHERE Username = 'usertest@sistest.com' LIMIT 1];

        List<PermissionSetAssignment> psaList = new List<PermissionSetAssignment>();

        for(PermissionSet ps : [SELECT Id FROM PermissionSet WHERE Name = 'CORE_API_SIS' OR Name = 'CORE_LeadFormAcquisition']){
            psaList.add(
                new PermissionSetAssignment(AssigneeId = usr.id, PermissionSetId = ps.Id)
            );
        }
        
        database.insert(psaList, true);

        System.runAs(usr){
            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
            req.requestURI = '/services/apexrest/sis/opportunity/re-registration-opportunity';
            req.httpMethod = 'POST';

            RestContext.request = req;
            RestContext.response= res;

            Account account = [SELECT Id FROM Account LIMIT 1];
            CORE_Business_Unit__c bu = [SELECT Id,CORE_Business_Unit_ExternalId__c FROM CORE_Business_Unit__c  LIMIT 1];
            Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];
            CORE_Level__c level = [SELECT Id,CORE_Level_ExternalId__c FROM CORE_Level__c  LIMIT 1];
            CORE_Curriculum__c curriculum = [SELECT Id,CORE_Curriculum_ExternalId__c FROM CORE_Curriculum__c  LIMIT 1];
            CORE_Intake__c intake = [SELECT Id,CORE_Intake_ExternalId__c FROM CORE_Intake__c  LIMIT 1];
            Product2 product = [SELECT Id,ExternalId FROM Product2  LIMIT 1];

            CORE_SIS_Wrapper.OpportunityWrapper sisrereg= new CORE_SIS_Wrapper.OpportunityWrapper(opp.Id,'Test12345',account.Id,'CORE_PKL_2021-2022',bu.CORE_Business_Unit_ExternalId__c,curriculum.CORE_Curriculum_ExternalId__c, level.CORE_Level_ExternalId__c,intake.CORE_Intake_ExternalId__c,product.ExternalId,'LEAD','LEAD - New');
            //CORE_SIS_ReregistrationOpp opp = new CORE_SIS_ReregistrationOpp(sisrereg);
           // System.assertEquals('OK', result.status);
            test.startTest();
            CORE_SIS_Wrapper.ResultWrapperGlobal result = CORE_SIS_ReregistrationOpp.opportunity(sisrereg);
            test.stopTest();
            System.assertEquals(CORE_SIS_Constants.SUCCESS_STATUS, result.status);
            //System.assertEquals(result.status,200);
        }
    }
    @isTest
    static void test_MissingParams(){
        User usr = [SELECT Id FROM User WHERE Username = 'usertest@sistest.com' LIMIT 1];

        List<PermissionSetAssignment> psaList = new List<PermissionSetAssignment>();

        for(PermissionSet ps : [SELECT Id FROM PermissionSet WHERE Name = 'CORE_API_SIS' OR Name = 'CORE_LeadFormAcquisition']){
            psaList.add(
                new PermissionSetAssignment(AssigneeId = usr.id, PermissionSetId = ps.Id)
            );
        }
        
        database.insert(psaList, true);

        System.runAs(usr){
            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
            req.requestURI = '/services/apexrest/sis/opportunity/re-registration-opportunity';
            req.httpMethod = 'POST';

            RestContext.request = req;
            RestContext.response= res;

            Account account = [SELECT Id FROM Account LIMIT 1];
            CORE_Business_Unit__c bu = [SELECT Id,CORE_Business_Unit_ExternalId__c FROM CORE_Business_Unit__c  LIMIT 1];
            Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];
            CORE_Level__c level = [SELECT Id,CORE_Level_ExternalId__c FROM CORE_Level__c  LIMIT 1];
            CORE_Curriculum__c curriculum = [SELECT Id,CORE_Curriculum_ExternalId__c FROM CORE_Curriculum__c  LIMIT 1];
            CORE_Intake__c intake = [SELECT Id,CORE_Intake_ExternalId__c FROM CORE_Intake__c  LIMIT 1];
            Product2 product = [SELECT Id,ExternalId FROM Product2  LIMIT 1];

            CORE_SIS_Wrapper.OpportunityWrapper sisrereg= new CORE_SIS_Wrapper.OpportunityWrapper(opp.Id,'Test12345','','CORE_PKL_2021-2022',bu.CORE_Business_Unit_ExternalId__c,curriculum.CORE_Curriculum_ExternalId__c, level.CORE_Level_ExternalId__c,intake.CORE_Intake_ExternalId__c,product.ExternalId,'LEAD','LEAD - New');
            //CORE_SIS_Wrapper.ResultWrapperGlobal result = CORE_SIS_ReregistrationOpp.opportunity(sisrereg);
           // System.assertEquals('OK', result.status);
            test.startTest();
            CORE_SIS_Wrapper.ResultWrapperGlobal result = CORE_SIS_ReregistrationOpp.opportunity(sisrereg);
            test.stopTest();
            System.assertEquals(CORE_SIS_Constants.ERROR_STATUS, result.status);
        }
    }
}