@isTest
private class CORE_FLEX_HistoConversationTest {

    static Task createTask() {
        Account account = CORE_DataFaker_Account.getStudentAccount('student1');
        
        CORE_FLEX_HistoConversation__c conv = new CORE_FLEX_HistoConversation__c();
        insert conv;

        string customerPhone='+33602030405';
        string twilioPhone='+33102030405';        
        Task task = new Task(WhatId=account.Id,CORE_Customer_phone_used__c=customerPhone,CORE_Galileo_phone_used__c=twilioPhone,CORE_FLEX_HistoConversation__c=conv.Id);
        insert task;  
        return task;
    }
    
    @isTest
    static void testFind() {
        CORE_FLEX_HistoConversation.find(null,null,null);

		Task task = createTask();        
        CORE_FLEX_HistoConversation.find(task.WhatId,task.CORE_Customer_phone_used__c,task.CORE_Galileo_phone_used__c);
    }
    
    @isTest
    static void testLoadContent() {
        
        CORE_FLEX_HistoConversation.loadContent(null,null, null,0,10);
        
        Task task = createTask();
        CORE_FLEX_HistoConversation.loadContent(task.CORE_FLEX_HistoConversation__c,null, null,0,10);
        
        
        CORE_FLEX_HistoConversation__c conv = new CORE_FLEX_HistoConversation__c();
        insert conv;

        try {
            CORE_DataFaker_CatalogHierarchy catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test1');
            Account account = CORE_DataFaker_Account.getStudentAccount('student1');
            Opportunity opportunity = CORE_DataFaker_Opportunity.getOpportunity(catalogHierarchy.division.Id, catalogHierarchy.businessUnit.Id, account.Id, 'Lead', 'Lead - New');
            task = new Task(CORE_FLEX_HistoConversation__c=conv.Id, WhatId=opportunity.Id);
            insert task;        
        } catch(DMLException ex) {
            task = new Task(CORE_FLEX_HistoConversation__c=conv.Id);
            insert task;    
        }
        CORE_FLEX_HistoConversation.loadContent(conv.Id,null, null,0,10);
    }
}