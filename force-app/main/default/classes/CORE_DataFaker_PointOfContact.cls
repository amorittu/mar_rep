@IsTest
public class CORE_DataFaker_PointOfContact {
    private static final String PKL_CAMPAIGN_MEDIUM = 'CORE_PKL_Site';
    private static final String PKL_CAPTURE_CHANNEL = 'CORE_PKL_Phone_call';
    private static final String PKL_DEVICE = 'CORE_PKL_Computer';
    private static final String PKL_ENROLLMENT_CHANNEL = 'CORE_PKL_Direct';
    private static final String PKL_INSERTION_MODE = 'CORE_PKL_Automatic';
    private static final String PKL_NATURE = 'CORE_PKL_Online';
    private static final String PKL_FORM = 'CORE_PKL_Brochure';
    private static final String PKL_FORM_AREA = 'CORE_PKL_Bottom_of_page';

    public static CORE_Point_of_Contact__c getPointOfContact(Id opportunityId, Id accountId, string externalId){
        String testString = 'testString';
        CORE_Point_of_Contact__c pointOfContact = new CORE_Point_of_Contact__c (
            CORE_External_Id__c = externalId,
            CORE_Account__c = accountId,
            CORE_Opportunity__c = opportunityId,
            CORE_Campaign_Content__c = testString,
            Campaign_Medium__c = PKL_CAMPAIGN_MEDIUM, 
            CORE_Campaign_Name__c = testString,
            CORE_Campaign_Source__c = testString,
            CORE_Campaign_Term__c = testString,
            CORE_Capture_Channel__c = PKL_CAPTURE_CHANNEL ,
            CORE_Chat_With__c = testString,
            CORE_Device__c = PKL_DEVICE, 
            CORE_Enrollment_Channel__c = PKL_ENROLLMENT_CHANNEL, 
            CORE_First_page__c = testString,
            CORE_First_visit_datetime__c = System.Now(),
            CORE_Insertion_Date__c = System.Now(),
            CORE_Insertion_Mode__c = PKL_INSERTION_MODE,
            CORE_IP_address__c = testString,
            CORE_IsDuplicated__c = false,
            CORE_Latitude__c = testString,
            CORE_Longitude__c = testString,
            CORE_Nature__c = PKL_NATURE, 
            CORE_Origin__c = testString,
            CORE_Origin_Sales__c = testString,
            CORE_Proactive_Engaged__c = testString,
            CORE_Proactive_Prompt__c = testString,
            CORE_Source__c = testString,
            CORE_GCLID__c = testString,
            Core_Form__c = PKL_FORM,
            Core_Form_area__c = PKL_FORM_AREA
        );
       
        insert pointOfContact;
        
        return pointOfContact;
    }
    public static CORE_Point_of_Contact__c getUniquePointOfContact(Id opportunityId, Id accountId, string externalId){
        String testString = 'testString';
        CORE_Point_of_Contact__c pointOfContact = new CORE_Point_of_Contact__c (
            CORE_External_Id__c = externalId,
            CORE_Account__c = accountId,
            CORE_Opportunity__c = opportunityId,
            CORE_Campaign_Content__c = testString + '1',
            Campaign_Medium__c = PKL_CAMPAIGN_MEDIUM, 
            CORE_Campaign_Name__c = testString + '2',
            CORE_Campaign_Source__c = testString + '3',
            CORE_Campaign_Term__c = testString + '4',
            CORE_Capture_Channel__c = PKL_CAPTURE_CHANNEL ,
            CORE_Chat_With__c = testString + '5',
            CORE_Device__c = PKL_DEVICE, 
            CORE_Enrollment_Channel__c = PKL_ENROLLMENT_CHANNEL, 
            CORE_First_page__c = testString + '6',
            CORE_First_visit_datetime__c = System.Now(),
            CORE_Insertion_Date__c = System.Now(),
            CORE_Insertion_Mode__c = PKL_INSERTION_MODE,
            CORE_IP_address__c = testString + '7',
            CORE_IsDuplicated__c = false,
            CORE_Latitude__c = testString + '8',
            CORE_Longitude__c = testString + '9',
            CORE_Nature__c = PKL_NATURE, 
            CORE_Origin__c = testString + '10',
            CORE_Origin_Sales__c = testString + '11',
            CORE_Proactive_Engaged__c = testString + '12',
            CORE_Proactive_Prompt__c = testString + '13',
            CORE_Source__c = testString + '14',
            CORE_GCLID__c = testString + '15',
            Core_Form__c = PKL_FORM,
            Core_Form_area__c = PKL_FORM_AREA
        );
       
        insert pointOfContact;
        
        return pointOfContact;
    }
}