/**
 * @author Valentin Pitel
 * @create date 2022-05-09 16:40:22
 * @modify date 2022-05-09 16:40:22
 * @desc Invocable action to upsert records.
 */
public inherited sharing  class CORE_Flow_UpsertRecords {
    public class CustomException extends Exception {}
    
    @InvocableMethod(label='Upsert Record')
    public static ActionOutput[] initUpsert(ActionInput[] inputs) {

        if(inputs.isEmpty()) {
            return null;
        }

        Map<String,Database.UpsertResult> externalKeyToUpsertResultMap = upsertRecords(
            inputs[0].records, 
            inputs[0].objectApiName, 
            inputs[0].externalIdField
        );
        System.debug('inputs[0] = ' + inputs[0]);
        return getActionOutputs(inputs[0], externalKeyToUpsertResultMap);
    }

    private static Map<String,Database.UpsertResult> upsertRecords(SObject[] recordsList, String objectApiName, String externalIdField) {

        Database.UpsertResult[] results = Database.upsert(
            recordsList, 
            CORE_SobjectUtils.getSObjectField(objectApiName, externalIdField),
            false
        );

        Map<Id,SObject> recordsMap = new Map<Id,SObject>(recordsList);

        Map<String,Database.UpsertResult> externalKeyToUpsertResultMap = new Map<String,Database.UpsertResult>();
        for(Database.UpsertResult result : results) {
            externalKeyToUpsertResultMap.put((String)recordsMap.get(result.getId()).get(externalIdField), result);
        }
        return externalKeyToUpsertResultMap;
    }

    private static ActionOutput[] getActionOutputs(ActionInput input, Map<String,Database.UpsertResult> externalKeyToUpsertResultMap) {
        ActionOutput[] outputs = new ActionOutput[]{};
        Boolean isSuccess = true;
        String errorMsg = '';
        ActionOutput output = new ActionOutput();
        output.records = new List<SObject>();
        for(SObject record : input.records) {
            Database.UpsertResult result = externalKeyToUpsertResultMap.get((String)record.get(input.externalIdField));

            output.records.add(record);
            if(!result.isSuccess()){
                isSuccess = false;
                errorMsg += '\\n' + JSON.serialize(result.getErrors());
            }
        }
        output.isSuccess = isSuccess;
        output.errors = errorMsg;

        outputs.add(output);
        return outputs;
    }
    
    /* Input parameters for the Apex action */
    public class ActionInput {

        @InvocableVariable(label='Records' required=true)
        public List<SObject> records;

        @InvocableVariable(label='Object API Name' required=true)
        public String objectApiName;

        @InvocableVariable(label='External Id Field(API name)' required=true)
        public String externalIdField;
    }
    
    /* Output parameters of the Apex action */
    public class ActionOutput {

        @InvocableVariable(label='Upsert Success?')
        public Boolean isSuccess;

        @InvocableVariable(label='Error(s)')
        public String errors;
        /*
        @InvocableVariable(label='Is Record Updated?')
        public Boolean isUpdated;

        @InvocableVariable(label='Is Record Created?')
        public Boolean isCreated;
        */
        @InvocableVariable(label='Records with ids')
        public List<SObject> records;
    }
}