@IsTest
public class CORE_DataConvertionTest {
    @IsTest
    public static void convertTimeStampToDateTime_Should_Return_local_datetime_when_parameter_is_set_to_true() {
        String strTimeStamp = '1623844967000';

        Test.startTest();
        DateTime dtResult = CORE_DataConvertion.convertTimeStampToDateTime(strTimeStamp,true);

        Test.stopTest();
        Long timestamp = Long.valueOf(strTimestamp);
        DateTime gmtDatetime = DateTime.newInstance(timestamp);
        
        System.TimeZone myTz = UserInfo.getTimeZone();
        
        Integer millisecondOffsetGmt = myTz.getOffset(gmtDateTime);
        DateTime localDatetime = DateTime.newInstance(timeStamp - millisecondOffsetGmt);

        System.AssertEquals(localDatetime,dtResult, 'convertTimeStampToDateTime should return local datetime');
    }

    @IsTest
    public static void convertTimeStampToDateTime_Should_Return_gmt_datetime_when_parameter_is_set_to_false() {
        String strTimeStamp = '1626798538000';

        Test.startTest();
        DateTime dtResult = CORE_DataConvertion.convertTimeStampToDateTime(strTimeStamp,false);

        Test.stopTest();
        Long timestamp = Long.valueOf(strTimestamp);
        DateTime gmtDatetime = DateTime.newInstance(timestamp);
      
        if(gmtDatetime <= datetime.now()){
            System.AssertEquals(gmtDatetime,dtResult, 'convertTimeStampToDateTime should return gmt datetime');
        }
    }

    @IsTest
    public  static void convertTimeStampToDateTime_Should_Return_null_if_string_parameter_is_blank() {
        Test.startTest();
        DateTime dtResult = CORE_DataConvertion.convertTimeStampToDateTime('',true);
        System.AssertEquals(null,dtResult, 'convertTimeStampToDateTime should return null');
        dtResult = CORE_DataConvertion.convertTimeStampToDateTime(null,true);
        System.AssertEquals(null,dtResult, 'convertTimeStampToDateTime should return null');
        Test.stopTest();
    }

    @IsTest
    public  static void convertSpecialStringFormatToDateTime_Should_Return_datetime(){
        List<String> months = new List<String> {'Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'};
        Integer monthNumber = 1;
        for(String month: months){
            String strDate = 'Sun, 25 ' + month + ' 2022 13:53:00 +0000';
            Datetime dt = CORE_DataConvertion.convertSpecialStringFormatToDateTime(strDate);
            System.assertNotEquals(null, dt);
            System.assertEquals(monthNumber, dt.month());
            monthNumber++;
        }
    }
}