/**
 * @author Valentin Pitel
 * @create date 2022-05-09 17:59:52
 * @modify date 2022-05-09 18:15:23
 * @desc Test class for CORE_Flow_Consent_internalReference class.
 */
@IsTest
private class  CORE_Flow_Consent_internalReferenceTest {
    @TestSetup
    static void makeData(){
        CORE_DataFaker_CatalogHierarchy catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test');

        Account account = CORE_DataFaker_Account.getStudentAccount('test');
        Opportunity opportunity = CORE_DataFaker_Opportunity.getOpportunity(catalogHierarchy.division.Id, 
            catalogHierarchy.businessUnit.Id, 
            catalogHierarchy.studyArea.Id,
            catalogHierarchy.curriculum.Id,
            catalogHierarchy.level.Id,
            catalogHierarchy.intake.Id, 
            account.Id, 
            'Lead', 
            'Lead - New'
        );

    }
    
    @IsTest
    static void getInternalReferences_Success() {
        Opportunity opp = [SELECT Id, AccountId,CORE_OPP_Business_Unit__c, CORE_OPP_Division__c, CORE_BU_Brand__c FROM Opportunity];
        
        CORE_Flow_Consent_internalReference.Parameters input = new CORE_Flow_Consent_internalReference.Parameters();

        CORE_Consent__c consent1 = new CORE_Consent__c();
        consent1.CORE_Person_Account__c = opp.AccountId;
        consent1.CORE_Business_Unit__c = opp.CORE_OPP_Business_Unit__c;
        consent1.CORE_Division__c = opp.CORE_OPP_Division__c;
        consent1.CORE_Consent_type__c = 'CORE_PKL_Processing_Collection_Storage_of_data_consent';
        consent1.CORE_Consent_channel__c = 'CORE_PKL_Email';
        consent1.CORE_Opt_in__c = true;
        consent1.CORE_Scope__c = 'CORE_PKL_BusinessUnit';

        input.consents = new List<CORE_Consent__c>{consent1};

        List<CORE_Flow_Consent_internalReference.Parameters> inputs = new List<CORE_Flow_Consent_internalReference.Parameters>{input};

        Test.startTest();
        CORE_Flow_Consent_internalReference.Results[] outputs = CORE_Flow_Consent_internalReference.getInternalReferences(inputs);
        Test.stopTest();

        System.assertEquals(
            false, 
            outputs[0].consentsWithInternalRefecences.isEmpty(),
            'Consents should be returned'
        );

        System.assertNotEquals(
            null, 
            outputs[0].consentsWithInternalRefecences[0].CORE_TECH_InternalReference__c,
            'Consents should be returned with internal reference field filled'
        );
    }
}