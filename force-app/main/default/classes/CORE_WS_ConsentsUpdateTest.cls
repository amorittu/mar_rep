@isTest
    private class CORE_WS_ConsentsUpdateTest {
    
        private static final String INTEREST_EXTERNALID = 'setup';
    
        @testSetup
        static void createData() {
            Id profileId = [SELECT Id FROM Profile WHERE Name = 'CORE_API_Profile' LIMIT 1].Id;
    
            User usr = new User(
                Username = 'usertest@aoltest.com',
                LastName = 'TEST_USER',
                Email = 'testemail@yopmail.com',
                Alias = 'T',
                CommunityNickname = 'T',
                TimeZoneSidKey = 'Europe/Paris',
                LocaleSidKey = 'fr_FR_EURO',
                EmailEncodingKey = 'ISO-8859-1',
                ProfileId = profileId,
                LanguageLocaleKey = 'fr'
            );
            database.insert(usr, true);
    
            CORE_CatalogHierarchyModel catalogHierarchy = new CORE_DataFaker_CatalogHierarchy(INTEREST_EXTERNALID).catalogHierarchy;
            Account account = CORE_DataFaker_Account.getStudentAccount(INTEREST_EXTERNALID);
    
            CORE_Consent__c consent1 = new CORE_Consent__c();
            consent1.CORE_Person_Account__c = account.Id;
            consent1.CORE_Business_Unit__c = catalogHierarchy.businessUnit.Id;
            consent1.CORE_Consent_type__c = 'CORE_Optin_Optout';
            consent1.CORE_Consent_channel__c = 'CORE_PKL_Email';
            consent1.CORE_Scope__c = 'CORE_PKL_BusinessUnit';
            consent1.CORE_Opt_in__c = true;
            consent1.CORE_Opt_in_date__c = datetime.now();
            database.insert(consent1, true);
        }
    
        @isTest
        static void test_updatePAConsents(){
            User usr = [SELECT Id FROM User WHERE Username = 'usertest@aoltest.com' LIMIT 1];
    
            List<PermissionSetAssignment> psaList = new List<PermissionSetAssignment>();
    
            for(PermissionSet ps : [SELECT Id FROM PermissionSet WHERE Name = 'CORE_API_SIS' OR Name = 'CORE_LeadFormAcquisition']){
                psaList.add(
                        new PermissionSetAssignment(AssigneeId = usr.id, PermissionSetId = ps.Id)
                );
            }
    
            database.insert(psaList, true);
    
            System.runAs(usr){
    
                CORE_Consent__c consent = [SELECT Id, CORE_Opt_in_date__c, CORE_Double_Opt_in_date__c, CORE_Consent_description__c FROM CORE_Consent__c LIMIT 1];
                System.debug('old consent'+consent);
                String reqBody = '{'
                + '"consents": [{'
                + '"SalesforceConsentID": "'+ consent.Id + '",'
                + '"CORE_Consent_description__c": "test",'
                + '"CORE_Opt_in__c": true,'
                + '"CORE_Opt_in_date__c": "2022-04-25T05:20:39.689Z",'
                + '"CORE_Double_Opt_in_date__c":"2022-04-26T05:20:39.689Z"'
                + '}]'
                + '}';
    
                RestRequest req = new RestRequest();
                RestResponse res = new RestResponse();
                req.requestURI = '/services/apexrest/core/consent';
                req.httpMethod = 'PUT';
                req.requestBody = Blob.valueOf(reqBody);
    
                RestContext.request = req;
                RestContext.response= res;
    
                test.startTest();
                CORE_WS_Wrapper.ResultWrapperConsents results = CORE_WS_PersonAccountConsents_Update.updateConsents();
                test.stopTest();
                System.debug('consents list' + results.consentsList);
                System.assertEquals(1, results.consentsList.size());
                CORE_Consent__c consent1 = [SELECT Id, CORE_Opt_in_date__c, CORE_Double_Opt_in_date__c, CORE_Consent_description__c FROM CORE_Consent__c LIMIT 1];
                System.debug('new consent'+consent1);
            }
        }
        
}