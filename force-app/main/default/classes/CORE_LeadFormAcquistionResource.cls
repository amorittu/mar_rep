@RestResource(urlMapping='/LeadFormAcquisition/*')
global class CORE_LeadFormAcquistionResource {
 
    /*@HttpPost
    global static CORE_LeadFormAcquisitionWrapper.GlobalWrapper postLeadForm(CORE_LeadFormAcquisitionWrapper.GlobalWrapper datas) {
        //CORE_LeadFormAcquisitionWrapper.GlobalWrapper arg = (CORE_LeadFormAcquisitionWrapper.GlobalWrapper)JSON.deserialize(datas, CORE_LeadFormAcquisitionWrapper.GlobalWrapper.class);
        return datas;
    }*/
    @HttpPost
    global static void postLeadForm(
        CORE_LeadFormAcquisitionWrapper.PersonAccountWrapper personAccount,
        List<CORE_LeadFormAcquisitionWrapper.ConsentWrapper> consents,
        CORE_LeadFormAcquisitionWrapper.OpportunityWrapper mainInterest,
        string prospectiveStudentComment,
        CORE_LeadFormAcquisitionWrapper.OpportunityWrapper secondInterest,
        CORE_LeadFormAcquisitionWrapper.OpportunityWrapper thirdInterest,
        CORE_LeadFormAcquisitionWrapper.PointOfContactWrapper pointOfContact,
        CORE_LeadFormAcquisitionWrapper.CampaignSubscriptionWrapper campaignSubscription,
        CORE_LeadFormAcquisitionWrapper.AcademicDiplomaHistoryWrapper academicDiplomaHistory) {
            Long dt1 = DateTime.now().getTime();
            Long dt2;

            RestResponse res = RestContext.response;
            res.addHeader('Content-Type', 'application/json');
            DateTime apiCallDate = Datetime.now();
            CORE_LeadFormAcquisitionWrapper.GlobalWrapper globalWrapper= new CORE_LeadFormAcquisitionWrapper.GlobalWrapper();
            // Initiating logs
            CORE_LogManager logSettings;
            CORE_LOG__c log = new CORE_LOG__c(CORE_API_Name__c = 'LeadForm');
            Boolean isErrorLog = false;
            try{
                logSettings = new CORE_LogManager('LeadForm');
                globalWrapper.personAccount = personAccount;
                globalWrapper.consents = consents;
                globalWrapper.mainInterest = mainInterest;
                globalWrapper.prospectiveStudentComment = prospectiveStudentComment;
                globalWrapper.secondInterest = secondInterest;
                globalWrapper.thirdInterest = thirdInterest;
                globalWrapper.pointOfContact = pointOfContact;
                globalWrapper.campaignSubscription = campaignSubscription;
                globalWrapper.academicDiplomaHistory = academicDiplomaHistory;

                CORE_LeadFormAcquisitionProcess acquisitionProcess;
                if(globalWrapper.mainInterest != null && !String.isBlank(globalWrapper.mainInterest.onlineShopId)){
                    acquisitionProcess = new CORE_LFAOnlineShopProcess(apiCallDate,globalWrapper);
                } else {
                    acquisitionProcess = new CORE_LeadFormAcquisitionProcess(apiCallDate,globalWrapper);
                }

                acquisitionProcess.execute();
                System.debug('acquisitionProcess.errors.size() = ' + acquisitionProcess.errors.size());

                if(acquisitionProcess.errors.size() > 0){
                    res.responseBody = Blob.valueOf(JSON.serialize(acquisitionProcess.errors, false));

                    res.statusCode = 400;

                    // log
                    isErrorLog =true;

                } else {
                    res.responseBody = Blob.valueOf(JSON.serialize(acquisitionProcess.result, false));
                    
                    res.statusCode = 200;
                }
                System.debug('res.statusCode = ' + res.statusCode);
            } catch(Exception e){
                res.statusCode = 500;
                System.debug('e.getCause() = '+ e.getCause());
                System.debug('e.getMessage() = '+ e.getMessage());
                System.debug('e.getTypeName() = '+ e.getTypeName());
                
                CORE_LeadFormAcquisitionWrapper.ErrorWrapper unexpectedError = new CORE_LeadFormAcquisitionWrapper.ErrorWrapper(
                    CORE_LeadFormAcquistionInputValidator.UNEXPECTED_MSG + ': ' + e.getMessage(),
                    CORE_LeadFormAcquistionInputValidator.UNEXPECTED_CODE, 
                    apiCallDate
                    );
                    
                    res.responseBody = Blob.valueOf(JSON.serialize(unexpectedError, false));
                    
                    
                    // log
                    isErrorLog =true;
                }
                finally{
                    dt2 = DateTime.now().getTime();
                    System.debug('CORE_LeadFormAcquistionResource.postLeadForm: T0TAL TIME OF EXECUTION = ' + (dt2-dt1) + ' ms');
                    // creating log
                    log.CORE_Processing_Duration__c =(dt2-dt1);
                    log.CORE_HTTP_Status_Code__c = res.statusCode;
                    log.CORE_Sent_Body__c = res.responseBody.toString();
                    log.CORE_Received_Body__c = JSON.serialize(globalWrapper);
                    
                    if(!logSettings.isNone()){
                        if(logSettings.isAllLogged()){ // is All logged
                            CORE_LogManager.createLog(log);
                        }else if(logSettings.isErrorLogged() && isErrorLog){ // is Error
                            CORE_LogManager.createLog(log);
                        }
                    }
                }
                
                

    }
}