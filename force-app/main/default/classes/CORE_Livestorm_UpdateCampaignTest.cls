@IsTest
global class CORE_Livestorm_UpdateCampaignTest {
    @TestSetup
    public static void makeData(){
        CORE_Livestorm_DataFaker_InitiateTest.CreateMetadata();
    }

    @IsTest
    public static void fullSuccessExecuteTest() {  
        Test.setMock(HttpCalloutMock.class, new FullSuccessUpdateCampaignMock());
        List<CORE_Business_Unit__c> bus = [Select Id FROM CORE_Business_Unit__c];
        Campaign campaignCreated = CORE_DataFaker_Campaign.getCampaign('', true, 'Planned', bus.get(0).Id, null, date.today().addDays(1), date.today().addDays(2));
  
        Campaign campaignInit2 = [SELECT Id, CORE_Campaign_Full_Name__c, status, Description, CORE_External_Id__c, CORE_Is_Up_to_Date_With_Livestorm__c FROM Campaign LIMIT 1];
        campaignInit2.CORE_Campaign_Full_Name__c = campaign.CORE_Campaign_Full_Name__c + ' Update';
        campaignInit2.status = 'In Progress';
        campaignInit2.Description = 'Started';
        campaignInit2.CORE_Timezone__c = '(GMT+02:00) Central European Summer Time (Europe/Paris)';

        Test.startTest();
        update campaignInit2;
        CORE_Livestorm_UpdateCampaign campaignBatchableUpdated = new CORE_Livestorm_UpdateCampaign(new Set<Id> {campaignInit2.Id});
        database.executebatch(campaignBatchableUpdated, 25);
        Test.stopTest();

        List<Campaign> campaignsUpdated = [SELECT Id, CORE_Is_Up_to_Date_With_Livestorm__c FROM Campaign];
        System.assertEquals(1, campaignsUpdated.size());

        Campaign campaign = campaignsUpdated.get(0);
        System.assertEquals(true, campaign.CORE_Is_Up_to_Date_With_Livestorm__c);

        
        List<AsyncApexJob> jobsApexBatch = [select Id, ApexClassID, ApexClass.Name, Status, JobType from AsyncApexJob where JobType = 'BatchApex' AND ApexClass.Name = 'CORE_Livestorm_UpdateCampaign'];
        System.assertEquals(1, jobsApexBatch.size(), 'expecting one apex batch job');
    }

    global class FullSuccessUpdateCampaignMock implements HttpCalloutMock {
        global HttpResponse respond(HttpRequest req) {
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');

            if (req.getEndpoint().endsWith('events')){
                res.setBody('{"data": {"id": "2b7de38f-d2d9-4a73-beac-a6ed54d8a38b","attributes": {"registration_link": "https://app.livestorm.co/p/2b7de38f-d2d9-4a73-beac-a6ed54d8a38b"}}}');
                res.setStatusCode(201);
            }
            else if (req.getEndpoint().endsWith('sessions')){
                res.setBody('{"data": {"id": "6b3e4bde-2a75-4ed8-a4af-83a08a68a057","attributes": {"room_link": "https://app.livestorm.co/p/2b7de38f-d2d9-4a73-beac-a6ed54d8a38b/live?s=6b3e4bde-2a75-4ed8-a4af-83a08a68a057"}}}');
                res.setStatusCode(201);
            }
            else if(req.getEndpoint().contains('events')){
                res.setStatusCode(200);
            }
            else if(req.getEndpoint().contains('sessions')){
                res.setStatusCode(200);
            }

            return res;
        }
    }

    @IsTest
    public static void partialSuccessExecuteTest() {  
        Test.setMock(HttpCalloutMock.class, new PartialSuccessUpdateCampaignMock());
        List<CORE_Business_Unit__c> bus = [Select Id FROM CORE_Business_Unit__c];
        Campaign campaignCreated = CORE_DataFaker_Campaign.getCampaign('TestBen', true, 'Planned', bus.get(0).Id, null, date.today().addDays(1), date.today().addDays(2));
  
        Campaign campaign = [SELECT Id, CORE_Campaign_Full_Name__c, status, Description FROM Campaign LIMIT 1];
        campaign.CORE_Campaign_Full_Name__c = campaign.CORE_Campaign_Full_Name__c + ' Update';
        campaign.Description = 'Started';

        Test.startTest();
        update campaign;
        CORE_Livestorm_UpdateCampaign campaignBatchableUpdated = new CORE_Livestorm_UpdateCampaign(new Set<Id> {campaign.Id});
        database.executebatch(campaignBatchableUpdated, 25);
        Test.stopTest();

        List<Campaign> campaignsUpdated = [SELECT Id, CORE_Is_Up_to_Date_With_Livestorm__c FROM Campaign];
        System.assertEquals(1, campaignsUpdated.size());
        
        Campaign campaignToTest = campaignsUpdated.get(0);
        System.assertEquals(false, campaignToTest.CORE_Is_Up_to_Date_With_Livestorm__c);

        List<AsyncApexJob> jobsApexBatch = [select Id, ApexClassID, ApexClass.Name, Status, JobType from AsyncApexJob where JobType = 'BatchApex'];
        //System.assertEquals(1, jobsApexBatch.size(), 'expecting one apex batch job');
    }

    
    global class PartialSuccessUpdateCampaignMock implements HttpCalloutMock {
        global HttpResponse respond(HttpRequest req) {
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');

            if (req.getEndpoint().endsWith('events')){
                res.setBody('{"data": {"id": "2b7de38f-d2d9-4a73-beac-a6ed54d8a38b","attributes": {"registration_link": "https://app.livestorm.co/p/2b7de38f-d2d9-4a73-beac-a6ed54d8a38b"}}}');
                res.setStatusCode(201);
            }
            else if (req.getEndpoint().endsWith('sessions')){
                res.setBody('{"data": {"id": "6b3e4bde-2a75-4ed8-a4af-83a08a68a057","attributes": {"room_link": "https://app.livestorm.co/p/2b7de38f-d2d9-4a73-beac-a6ed54d8a38b/live?s=6b3e4bde-2a75-4ed8-a4af-83a08a68a057"}}}');
                res.setStatusCode(201);
            }
            else if(req.getEndpoint().contains('events')){
                res.setStatusCode(200);
            }
            else if(req.getEndpoint().contains('sessions')){
                res.setStatusCode(202);
            }

            return res;
        }
    }
    
    @IsTest
    public static void fullFailExecuteTest() {  
        Test.setMock(HttpCalloutMock.class, new FullFailUpdateCampaignMock());
        List<CORE_Business_Unit__c> bus = [Select Id FROM CORE_Business_Unit__c];
        Campaign campaignCreated = CORE_DataFaker_Campaign.getCampaign('TestBen', true, 'Planned', bus.get(0).Id, null, date.today().addDays(1), date.today().addDays(2));
  
        Campaign campaign = [SELECT Id, CORE_Campaign_Full_Name__c, status, Description FROM Campaign LIMIT 1];
        campaign.CORE_Campaign_Full_Name__c = campaign.CORE_Campaign_Full_Name__c + ' Update';
        campaign.Description = 'Started';

        Test.startTest();
        update campaign;
        CORE_Livestorm_UpdateCampaign campaignBatchableUpdated = new CORE_Livestorm_UpdateCampaign(new Set<Id> {campaign.Id});
        database.executebatch(campaignBatchableUpdated, 25);
        Test.stopTest();

        List<Campaign> campaignsUpdated = [SELECT Id, CORE_Is_Up_to_Date_With_Livestorm__c FROM Campaign];
        System.assertEquals(1, campaignsUpdated.size());
                
        Campaign campaignToTest = campaignsUpdated.get(0);
        System.assertEquals(false, campaignToTest.CORE_Is_Up_to_Date_With_Livestorm__c);
        
        List<AsyncApexJob> jobsApexBatch = [select Id, ApexClassID, ApexClass.Name, Status, JobType from AsyncApexJob where JobType = 'BatchApex'];
        //System.assertEquals(1, jobsApexBatch.size(), 'expecting one apex batch job');
    }

    
    global class FullFailUpdateCampaignMock implements HttpCalloutMock {
        global HttpResponse respond(HttpRequest req) {
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');

            if (req.getEndpoint().endsWith('events')){
                res.setBody('{"data": {"id": "2b7de38f-d2d9-4a73-beac-a6ed54d8a38b","attributes": {"registration_link": "https://app.livestorm.co/p/2b7de38f-d2d9-4a73-beac-a6ed54d8a38b"}}}');
                res.setStatusCode(201);
            }
            else if (req.getEndpoint().endsWith('sessions')){
                res.setBody('{"data": {"id": "6b3e4bde-2a75-4ed8-a4af-83a08a68a057","attributes": {"room_link": "https://app.livestorm.co/p/2b7de38f-d2d9-4a73-beac-a6ed54d8a38b/live?s=6b3e4bde-2a75-4ed8-a4af-83a08a68a057"}}}');
                res.setStatusCode(201);
            }
            else if(req.getEndpoint().contains('events')){
                res.setStatusCode(202);
            }
            else if(req.getEndpoint().contains('sessions')){
                res.setStatusCode(202);
            }

            return res;
        }
    }
}