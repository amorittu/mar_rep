@RestResource(urlMapping='/aol/opportunity/exist')
global class CORE_AOL_OpportunityExist {
    @HttpPost
    global static CORE_AOL_Wrapper.ResultWrapperOpportuntiyExist opportunityExist(String aolExternalId) {

        // Initiating logs
        Long dt1 = DateTime.now().getTime();
        Long dt2;
        CORE_LogManager logSettings;
        CORE_LOG__c log = new CORE_LOG__c(CORE_API_Name__c = 'AOL');
        Boolean isErrorLog = false;
        logSettings = new CORE_LogManager('AOL');

        CORE_AOL_Wrapper.ResultWrapperOpportuntiyExist result = checkMandatoryParameters(aolExternalId);
        
        try{
            if(result != null){
                return result;
            }
            List<Opportunity> opportunityList = CORE_AOL_QueryProvider.getOpportunitiesIfExist(aolExternalId);
            Opportunity opportunity;
            if(opportunityList.size() == 1){
                opportunity = opportunityList.get(0);
            }
            if(opportunity == null){
                List<CORE_LeadAcquisitionBuffer__c> leadAcquisitionBufferList = CORE_AOL_QueryProvider.getAolLeadAcquisitionBuffer(aolExternalId);
                if(leadAcquisitionBufferList.size() == 1){
                    result = new CORE_AOL_Wrapper.ResultWrapperOpportuntiyExist(
                        CORE_AOL_Constants.OPPORTUNITY_PENDING, 
                        String.format(CORE_AOL_Constants.MSG_OPPORTUNITY_PENDING, new List<String>{aolExternalId}));
                }else{
                    result = new CORE_AOL_Wrapper.ResultWrapperOpportuntiyExist(
                        CORE_AOL_Constants.OPPORTUNTIY_NOT_EXIST, 
                        String.format(CORE_AOL_Constants.MSG_OPPORTUNITY_NOT_FOUND, new List<String>{aolExternalId}));
                }
                
            } else {
                result = new CORE_AOL_Wrapper.ResultWrapperOpportuntiyExist(CORE_AOL_Constants.OPPORTUNITY_EXIST, opportunity);
            }

        } catch(Exception e){
            result = new CORE_AOL_Wrapper.ResultWrapperOpportuntiyExist(CORE_AOL_Constants.ERROR_STATUS, e.getMessage() + ' at ' + e.getStackTraceString());
        } finally{
            dt2 = DateTime.now().getTime();
            System.debug('CORE_AOL_AcademicDiplomaHistory(urlMapping=\'/aol/academic-diploma-history\') : T0TAL TIME OF EXECUTION = ' + (dt2-dt1) + ' ms');
            // creating log
            log.CORE_Processing_Duration__c =(dt2-dt1);
            log.CORE_HTTP_Status_Code__c = result.status == 'KO' ? 400 : result.status == 'OK' ? 200 : 500;
            log.CORE_Sent_Body__c = JSON.serialize(result);
            log.CORE_Received_Body__c =  '{"aolExternalId" : "' + aolExternalId + '"}';
            
            if(!logSettings.isNone()){
                if(logSettings.isAllLogged()){ // is All logged
                    CORE_LogManager.createLog(log);
                }else if(logSettings.isErrorLogged() && isErrorLog){ // is Error
                    CORE_LogManager.createLog(log);
                }
            }
        }

        return result;
    }

    private static CORE_AOL_Wrapper.ResultWrapperOpportuntiyExist checkMandatoryParameters(String aolExternalId){
        List<String> missingParameters = new List<String>();
        Boolean isMissingAolId = CORE_AOL_GlobalWorker.isAolExternalIdParameterMissing(aolExternalId);

        if(isMissingAolId){
            missingParameters.add('aolExternalId');
        }
        
        return getMissingParameters(missingParameters);
    }

    public static CORE_AOL_Wrapper.ResultWrapperOpportuntiyExist getMissingParameters(List<String> missingParameters){
        String errorMsg = CORE_AOL_Constants.MSG_MISSING_PARAMETERS;

        if(missingParameters.size() > 0){
            errorMsg += missingParameters;

            return new CORE_AOL_Wrapper.ResultWrapperOpportuntiyExist(CORE_AOL_Constants.ERROR_STATUS, errorMsg);
        }
        
        return null;
    }
}