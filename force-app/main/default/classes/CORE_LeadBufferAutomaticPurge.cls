public without sharing class CORE_LeadBufferAutomaticPurge implements Database.batchable<sObject>,Database.Stateful {
    private static final Integer DEFAULT_RETENTION_DELAY= 365;
    private static final String OK_STATUS = 'OK';

    public CORE_CountrySpecificSettings__c countrySetting { get; set; } 
    public Integer retentionDelay { get; set; } 
    public Integer nbrecords=0;
    
    public Database.QueryLocator start(Database.BatchableContext bc) {
        Integer defaultRetentionDelay = DEFAULT_RETENTION_DELAY;
        init();

        System.debug('retentionDelay = '+ retentionDelay);
        String query = 'SELECT Id From CORE_LeadAcquisitionBuffer__c WHERE CORE_Status__c = \'' + OK_STATUS + '\' AND CreatedDate < LAST_N_DAYS:' + retentionDelay;
        return Database.getQueryLocator(query);
    }
    private void init(){
        if(countrySetting == null){
            countrySetting = CORE_CountrySpecificSettings__c.getOrgDefaults();
        }

        if(countrySetting != null && countrySetting.CORE_Retention_Delay__c != null){
            retentionDelay = Integer.valueof(countrySetting.CORE_Retention_Delay__c);
        } else {
            retentionDelay = DEFAULT_RETENTION_DELAY;
        }
    }
    public void execute(Database.BatchableContext bc, List<CORE_LeadAcquisitionBuffer__c> records){
        Integer recsize = records.size();
        system.debug('recsize ='+ recsize);
        this.nbrecords += recsize;
        
        if(records.isEmpty()){
            return ;
        }

        delete records;
        //String.format('SELECT {0} FROM Opportunity WHERE {1}', queryParams);
        //custom label {0} , string.format
    }
            
    

    public void finish(Database.BatchableContext bc){
        String[] toAddresses = new String[] {};
        System.debug('finish = countrySetting = '+ countrySetting);
        if(nbrecords == 0 || String.isBlank(countrySetting?.CORE_System_admins_email_adresses__c)){
            return;
        } else {
            toAddresses = countrySetting.CORE_System_admins_email_adresses__c.split(';');
        }

        List<Integer> parameters = new List<Integer> {nbrecords, retentionDelay };
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String body = String.format(Label.CORE_Lead_buffer_automatic_purge_mail,parameters);
        mail.setToAddresses(toAddresses);
        mail.setSubject('Salesforce : Lead Buffer Automatic Purge');
        mail.setPlainTextBody(body);

        Messaging.SendEmailResult[] emailResults = Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    }
}