global class CORE_Flow_GetActivePricebookForProduct {
    @InvocableMethod
    global static List<Result> getPricebooks(List<Parameter> params) {
        List<Result> results = new List<Result>();
        Result result = new Result();
        result.pricebookIds = new List<String>();

        if (params != null) {
            Parameter param = params.get(0);

            List<CORE_PriceBook_BU__c> pbBus = [SELECT CORE_Price_Book__c FROM CORE_PriceBook_BU__c WHERE CORE_Academic_Year__c = :param.relatedAcademicYear AND CORE_TECH_IsActivePricebook__c = true];
            
            if(!pbBus.isEmpty()){
                Set<Id> pbIds = new Set<Id>();
                for(CORE_PriceBook_BU__c pbBu : pbBus){
                    pbIds.add(pbBu.CORE_Price_Book__c);
                }

                List<PricebookEntry> pbEntries = [SELECT Pricebook2Id FROM PricebookEntry WHERE Product2Id = :param.relatedProductId AND Pricebook2Id IN :pbIds];
                for(PricebookEntry pbEntry : pbEntries){
                    Id pbId = pbEntry.Pricebook2Id;
                    if(!result.pricebookIds.contains(pbId)){
                        result.pricebookIds.add(pbId);
                    }
                }
            }
        }
        if(!result.pricebookIds.isEmpty()){
            result.pricebookIdsAsString = '(\'' + String.join(result.pricebookIds, '\',\'') + '\')';
        }
        results.add(result);
        return results;
    }

    global class Parameter {
        @InvocableVariable(required=true)
        global Id relatedProductId;

        @InvocableVariable(required=true)
        global String relatedAcademicYear;
    }

    global class Result {
        @InvocableVariable(label='List of active pricebook ids related to the product and academic year' required=true)
        global List<String> pricebookIds;
        @InvocableVariable(label='String of active pricebook ids related to the product and academic year (ready for filters)' required=true)
        global String pricebookIdsAsString;
    }
}