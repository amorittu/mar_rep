@IsTest
public class CORE_OpportunityProcessTest {

	@TestSetup
	public static void makeData(){
		String uniqueKey = 'test1';
		CORE_DataFaker_CatalogHierarchy catalogHierarchy = new CORE_DataFaker_CatalogHierarchy(uniqueKey);
		Account account = CORE_DataFaker_Account.getStudentAccount(uniqueKey);
		Id pricebookId = CORE_DataFaker_PriceBook.getStandardPricebookId();
		CORE_PriceBook_BU__c pricebookBu = CORE_DataFaker_PriceBook.getPriceBookBu(pricebookId, catalogHierarchy.businessUnit.Id, 'testKey');
	}

	@IsTest
	public static void createOpportunity_Should_create_and_return_an_opportunity_and_create_pointOfContact() {
		Account account = [Select Id from Account];
		CORE_Division__c division = [Select Id from CORE_Division__c];
		CORE_Business_Unit__c businessUnit = [Select Id from CORE_Business_Unit__c];
		CORE_Study_Area__c studyArea = [Select Id from CORE_Study_Area__c];
		CORE_Curriculum__c curriculum = [Select Id from CORE_Curriculum__c];
		CORE_Level__c level = [Select Id from CORE_Level__c];
		Id pricebookId = CORE_DataFaker_PriceBook.getStandardPricebookId();

        List<CORE_OpportunityProcess.OpportunityProcessParameters> params = new List<CORE_OpportunityProcess.OpportunityProcessParameters>();
        CORE_OpportunityProcess.OpportunityProcessParameters param = new CORE_OpportunityProcess.OpportunityProcessParameters();

		List<String> academicYears = CORE_PicklistUtils.getPicklistValues('Opportunity', 'CORE_OPP_Academic_Year__c');

        param.academicYear = 'CORE_PKL_2021-2022';
		param.accountId = account.Id;
		param.businessUnitId = businessUnit.Id;
		param.studyAreaId = studyArea.Id;
		param.curriculumId = curriculum.Id;
		param.priceBookId = pricebookId;
		param.origin = 'originTest';
		param.captureChannel = 'CORE_PKL_Phone_call';
		param.nature = 'CORE_PKL_Online';
		params.add(param);

        Test.startTest();
        List<Opportunity> result = CORE_OpportunityProcess.createOpportunity(params);
        Test.stopTest();

		List<Opportunity> opportunities = [Select id, AccountId, CORE_OPP_Academic_Year__c,CORE_OPP_Business_Unit__c, CORE_OPP_Study_Area__c, CORE_OPP_Curriculum__c, Pricebook2Id  from opportunity];
		List<CORE_Point_of_Contact__c> pocs = [Select Id from CORE_Point_of_Contact__c];
		System.assertEquals(1,opportunities.size(),'1 opportunity should be created');
		System.assertEquals(1,pocs.size(),'1 point of contact should be created');

		System.assertEquals(account.Id, result.get(0).AccountId);
		System.assertEquals(businessUnit.Id, result.get(0).CORE_OPP_Business_Unit__c);
		System.assertEquals(studyArea.Id, result.get(0).CORE_OPP_Study_Area__c);
		System.assertEquals(curriculum.Id, result.get(0).CORE_OPP_Curriculum__c);
		System.assertEquals(pricebookId, result.get(0).Pricebook2Id);

	}

	@IsTest
	public static void createOpportunity_Should_create_and_return_an_opportunity_and_create_pointOfContact_and_opportunity_line_item() {
		Account account = [Select Id from Account];
		CORE_Division__c division = [Select Id from CORE_Division__c];
		CORE_Business_Unit__c businessUnit = [Select Id from CORE_Business_Unit__c];
		CORE_Study_Area__c studyArea = [Select Id from CORE_Study_Area__c];
		CORE_Curriculum__c curriculum = [Select Id from CORE_Curriculum__c];
		CORE_Level__c level = [Select Id from CORE_Level__c];
		CORE_Intake__c intake = [Select Id from CORE_Intake__c];
		Product2 product = [Select Id from Product2];
		Id pricebookId = CORE_DataFaker_PriceBook.getStandardPricebookId();
		PricebookEntry pricebookEntry = CORE_DataFaker_PriceBook.getPriceBookEntry(product.Id, pricebookId, 100.5);


        List<CORE_OpportunityProcess.OpportunityProcessParameters> params = new List<CORE_OpportunityProcess.OpportunityProcessParameters>();
        CORE_OpportunityProcess.OpportunityProcessParameters param = new CORE_OpportunityProcess.OpportunityProcessParameters();

		List<String> academicYears = CORE_PicklistUtils.getPicklistValues('Opportunity', 'CORE_OPP_Academic_Year__c');

        param.academicYear = 'CORE_PKL_2021-2022';
		param.accountId = account.Id;
		param.businessUnitId = businessUnit.Id;
		param.studyAreaId = studyArea.Id;
		param.curriculumId = curriculum.Id;
		param.priceBookId = pricebookId;
		param.intakeId = intake.Id;
		param.origin = 'origin';
		param.captureChannel = 'CORE_PKL_Phone_call';
		param.nature = 'CORE_PKL_Online';
		params.add(param);

        Test.startTest();
        List<Opportunity> result = CORE_OpportunityProcess.createOpportunity(params);
        Test.stopTest();

		List<Opportunity> opportunities = [Select id, AccountId, CORE_OPP_Academic_Year__c,CORE_OPP_Business_Unit__c, CORE_OPP_Study_Area__c, CORE_OPP_Curriculum__c, Pricebook2Id  from opportunity];
		List<CORE_Point_of_Contact__c> pocs = [Select Id from CORE_Point_of_Contact__c];
		List<OpportunityLineItem> oppLineItems = [Select Id from OpportunityLineItem];
		System.assertEquals(1,opportunities.size(),'1 opportunity should be created');
		System.assertEquals(1,pocs.size(),'1 point of contact should be created');
		System.assertEquals(1,oppLineItems.size(),'1 opportunityLineItem should be created');

		System.assertEquals(account.Id, result.get(0).AccountId);
		System.assertEquals(businessUnit.Id, result.get(0).CORE_OPP_Business_Unit__c);
		System.assertEquals(studyArea.Id, result.get(0).CORE_OPP_Study_Area__c);
		System.assertEquals(curriculum.Id, result.get(0).CORE_OPP_Curriculum__c);
		System.assertEquals(pricebookId, result.get(0).Pricebook2Id);
	}

	@IsTest
	public static void createOpportunity_Should_create_and_return_an_opportunity_and_create_pointOfContact_and_not_create_opportunity_line_item() {
		Account account = [Select Id from Account];
		CORE_Division__c division = [Select Id from CORE_Division__c];
		CORE_Business_Unit__c businessUnit = [Select Id from CORE_Business_Unit__c];
		CORE_Study_Area__c studyArea = [Select Id from CORE_Study_Area__c];
		CORE_Curriculum__c curriculum = [Select Id from CORE_Curriculum__c];
		CORE_Level__c level = [Select Id from CORE_Level__c];
		Product2 product = [Select Id from Product2];
		Id pricebookId = CORE_DataFaker_PriceBook.getStandardPricebookId();

        List<CORE_OpportunityProcess.OpportunityProcessParameters> params = new List<CORE_OpportunityProcess.OpportunityProcessParameters>();
        CORE_OpportunityProcess.OpportunityProcessParameters param = new CORE_OpportunityProcess.OpportunityProcessParameters();

		List<String> academicYears = CORE_PicklistUtils.getPicklistValues('Opportunity', 'CORE_OPP_Academic_Year__c');

        param.academicYear = 'CORE_PKL_2021-2022';
		param.accountId = account.Id;
		param.businessUnitId = businessUnit.Id;
		param.studyAreaId = studyArea.Id;
		param.curriculumId = curriculum.Id;
		param.priceBookId = pricebookId;

		params.add(param);

        Test.startTest();
        List<Opportunity> result = CORE_OpportunityProcess.createOpportunity(params);
        Test.stopTest();

		List<Opportunity> opportunities = [Select id, AccountId, CORE_OPP_Academic_Year__c,CORE_OPP_Business_Unit__c, CORE_OPP_Study_Area__c, CORE_OPP_Curriculum__c, Pricebook2Id  from opportunity];
		List<CORE_Point_of_Contact__c> pocs = [Select Id from CORE_Point_of_Contact__c];

		//should be empty because there is no PriceBook_bu related to this bu and pricebook
		List<OpportunityLineItem> oppLineItems = [Select Id from OpportunityLineItem];
		System.assertEquals(1,opportunities.size(),'1 opportunity should be created');
		System.assertEquals(1,pocs.size(),'1 point of contact should be created');
		System.assertEquals(0,oppLineItems.size(),'0 opportunityLineItem should be created => no existing PricebookBu');

		System.assertEquals(account.Id, result.get(0).AccountId);
		System.assertEquals(businessUnit.Id, result.get(0).CORE_OPP_Business_Unit__c);
		System.assertEquals(studyArea.Id, result.get(0).CORE_OPP_Study_Area__c);
		System.assertEquals(curriculum.Id, result.get(0).CORE_OPP_Curriculum__c);
		System.assertEquals(pricebookId, result.get(0).Pricebook2Id);
	}

}