/**
* @author Fodil BOUDJEDIEN - Almavia
* @date 2022-03-30
* @group LogManager
* @description Manager class for Logging using the custom metadataType CORE_Log_Settings__mdt and the Object CORE_LOG__c
* @test class CORE_LogManagerTest
*/
public  class CORE_LogManager {

    public class CORE_LogManagerQueueable implements Queueable{
        CORE_LOG__c log = new CORE_LOG__c();
        public CORE_LogManagerQueueable(CORE_LOG__c log){
            this.log = log;
        }
        public void execute(QueueableContext context) {
            insert log;
        }
    }

    public CORE_Log_Settings__mdt customMetadataType;

    public CORE_LogManager() {}

    public CORE_LogManager(String logSettingName) {
        // Custom Metadata Type
         this.customMetadataType = CORE_Log_Settings__mdt.getInstance(logSettingName);
    }

    public CORE_Log_Settings__mdt getCustomMetadataType(){
        return this.customMetadataType;
    }
    
    public Boolean isErrorLogged (){
        return (this.customMetadataType.CORE_Log_Type__c == 'ERROR');
    }
    
    public Boolean isAllLogged(){
        return (this.customMetadataType.CORE_Log_Type__c == 'ALL');
    }

    public Boolean isNone(){
        return (this.customMetadataType.CORE_Log_Type__c == 'NONE');
    }
    
    // create the log for our organization
    public static void createLog(CORE_LOG__c log){
        CORE_LogManagerQueueable logJob = new CORE_LogManagerQueueable(log);
        System.enqueueJob(logJob);
    } 

}