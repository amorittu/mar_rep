@isTest
public with sharing class IT_TemporaryDataCleanerClassTest {
    
    @isTest
    public static void testClass() {
        CORE_DataFaker_CatalogHierarchy catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test1');
		Account account = CORE_DataFaker_Account.getStudentAccount('test1');
		Opportunity opportunity = CORE_DataFaker_Opportunity.getOpportunity(
				catalogHierarchy.division.Id,
				catalogHierarchy.businessUnit.Id,
				account.Id,
				'Applicant',
				'Applicant - Application Submitted / New'
		);

		opportunity.CORE_OPP_Intake__c = catalogHierarchy.intake.Id;
		opportunity.CORE_OPP_Curriculum__c = catalogHierarchy.curriculum.Id;
		opportunity.CORE_OPP_Study_Area__c = catalogHierarchy.studyArea.Id;
		opportunity.CORE_OPP_Level__c = catalogHierarchy.level.Id;
		opportunity.IT_AdmissionOfficer__c = UserInfo.getUserId();
        opportunity.CORE_OPP_Business_Hours__c = [SELECT Id FROM BusinessHours WHERE isDefault = true].Id;
		update opportunity;

		Task t = new Task();
		t.WhatId = opportunity.Id;
        t.CORE_Start_Date__c = System.today();
        t.Status = 'CORE_PKL_Open';
		insert t;
        
        t.Status = 'CORE_PKL_Completed';
        update t;
        
        System.debug('TASKKKKKK IS ::::: ' + t);
        
        Test.startTest();
        Database.executeBatch(new IT_TemporaryDataCleanerClass(new String [] {t.Id} ) );
	Test.stopTest();
    }
}