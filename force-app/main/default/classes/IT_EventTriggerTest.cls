@isTest
public class IT_EventTriggerTest {
	@TestSetup
	static void makeData() {
		CORE_DataFaker_CatalogHierarchy catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test1');
		Account account = CORE_DataFaker_Account.getStudentAccount('test1');
		Opportunity opportunity = CORE_DataFaker_Opportunity.getOpportunity(
				catalogHierarchy.division.Id,
				catalogHierarchy.businessUnit.Id,
				account.Id,
				'Applicant',
				'Applicant - Application Submitted / New'
		);

		CORE_Point_of_Contact__c pc = new CORE_Point_of_Contact__c();
		pc.CORE_Opportunity__c = opportunity.Id;
		pc.CORE_Source__c = 'source test';
		pc.CORE_Insertion_Mode__c = 'CORE_PKL_Manual';
		pc.CORE_Nature__c = 'CORE_PKL_Online';
		pc.CORE_Enrollment_Channel__c = 'CORE_PKL_Direct';
		pc.CORE_Capture_Channel__c = 'CORE_PKL_CIO';
		pc.CORE_Origin__c = 'BookingOR';
		pc.Campaign_Medium__c = 'CORE_PKL_Directory';
		pc.CORE_Campaign_Source__c = 'source campaign test';
		pc.CORE_Campaign_Term__c = 'term campaign test';
		pc.CORE_Campaign_Content__c = 'content campaign test';
		pc.CORE_Campaign_Name__c = 'name campaign test';
		pc.CORE_Device__c = 'CORE_PKL_Computer';
		pc.CORE_First_visit_datetime__c = DateTime.newInstance(2021, 07, 01, 12, 0, 0);
		pc.CORE_IP_address__c = 'IP adresse';
		pc.CORE_First_page__c = 'first page test';
		pc.CORE_Latitude__c = 'latitude test';
		pc.CORE_Longitude__c = 'longitude test';
		pc.CreatedDate = DateTime.newInstance(2021, 07, 01, 12, 0, 0);
		pc.LastModifiedDate = DateTime.newInstance(2021, 07, 07, 12, 0, 0);
		pc.CORE_Proactive_Prompt__c = 'CORE_Proactive_Prompt__cTest';
		pc.CORE_Proactive_Engaged__c = 'CORE_Proactive_Engaged__cTest';
		pc.CORE_Chat_With__c = 'CORE_Chat_With__cTest';
		insert pc;


		opportunity.CORE_OPP_Intake__c = catalogHierarchy.intake.Id;
		opportunity.CORE_OPP_Curriculum__c = catalogHierarchy.curriculum.Id;
		opportunity.CORE_OPP_Study_Area__c = catalogHierarchy.studyArea.Id;
		opportunity.CORE_OPP_Level__c = catalogHierarchy.level.Id;
		opportunity.IT_AdmissionOfficer__c = UserInfo.getUserId();
		update opportunity;

		IT_DataFaker_Objects.createExtendedInfo(account.Id, opportunity.CORE_OPP_Business_Unit__r.CORE_Brand__c, true);

		Id rtId = Schema.SObjectType.Task.getRecordTypeInfosByDeveloperName().get('CORE_Continuum_of_action').getRecordTypeId();

		Task t = new Task();
		t.Subject = 'Request from website';
		t.CORE_Point_of_contact__c = pc.Id ;
		t.WhatId = opportunity.Id;
		t.RecordTypeId = rtId;
		t.Status = 'CORE_PKL_Open';
		insert t;

		ServiceAppointment sa = new ServiceAppointment();
		sa.ParentRecordId = account.Id;
		sa.DueDate = DateTime.Now().addDays(1);
		sa.EarliestStartTime = DateTime.Now();
		sa.SchedEndTime = DateTime.Now().addHours(1);
		sa.SchedStartTime = DateTime.Now();
		sa.Status = 'Scheduled';
		sa.AppointmentType = 'Call';
		sa.CORE_TECH_Relatd_Opportunity__c = opportunity.Id;
		insert sa;
	}

	@isTest
	public static void testAfterInsertBehavior() {
		Opportunity opportunity = [SELECT Id FROM Opportunity];
		ServiceAppointment sa = [SELECT Id FROM ServiceAppointment];

		ServiceResource serviceResource = new ServiceResource(
				Name = 'TEST',
				Description = 'test sr',
				IsActive = true,
				RelatedRecordId = UserInfo.getUserId(),
				ResourceType = 'T'
		);


		Test.startTest();
		Event evt = new Event();
		evt.WhatId = opportunity.Id;
		evt.ShowAs = 'Busy';
		evt.Subject = 'ImJustAtest';
		evt.DurationInMinutes = 1;
		evt.ActivityDateTime = system.now();
		insert evt;
		insert serviceResource;
		Test.stopTest();
	}
}