public without sharing class CORE_CampaignUpdateStatusBatchable implements Database.batchable<SObject> {
    
    private static final String PKL_STATUS_INPROGRESS = 'In Progress';
    private static final String PKL_STATUS_COMPLETED = 'Completed';
    
    public Database.QueryLocator start(Database.BatchableContext info){
        System.debug('CORE_CampaignUpdateStatusBatchable.start() : START');
        return Database.getQueryLocator([Select Id, Status, StartDate, EndDate FROM Campaign]); 
    }

    public void execute(Database.BatchableContext info, List<Campaign> campaigns){
        System.debug('CORE_CampaignUpdateStatusBatchable.execute() : START');
        for (Campaign camp : campaigns){
            if (System.today() == camp.StartDate){
                camp.Status = PKL_STATUS_INPROGRESS;
            } else if (System.today() > camp.EndDate){
                camp.Status = PKL_STATUS_COMPLETED;
            }
        }
        
        update campaigns;
        System.debug('CORE_CampaignUpdateStatusBatchable.execute() : END');
    }

    public void finish(Database.BatchableContext info){
        System.debug('CORE_CampaignUpdateStatusBatchable.finish() : FINISHED');
    } 
}