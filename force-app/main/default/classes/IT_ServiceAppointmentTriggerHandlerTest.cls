@isTest
public with sharing class IT_ServiceAppointmentTriggerHandlerTest {
    
    @TestSetup
    public static void makeData(){
        CORE_DataFaker_CatalogHierarchy catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test1');

        Account account = CORE_DataFaker_Account.getStudentAccount('test1');
        Opportunity opportunity = CORE_DataFaker_Opportunity.getOpportunity(
                catalogHierarchy.division.Id,
                catalogHierarchy.businessUnit.Id,
                account.Id,
                'Applicant',
                'Applicant - Application Submitted / New'
        );

        opportunity.Pricebook2Id = Test.getStandardPricebookId();
        opportunity.CORE_OPP_Intake__c = catalogHierarchy.intake.Id;
        opportunity.CORE_OPP_Level__c = catalogHierarchy.level.Id;
        opportunity.CORE_OPP_Curriculum__c = catalogHierarchy.curriculum.Id;
        opportunity.CORE_OPP_Study_Area__c = catalogHierarchy.studyArea.Id;
        opportunity.CORE_Capture_Channel__c = 'CORE_PKL_Web_form';

        update opportunity;

        Opportunity opportunity2 = CORE_DataFaker_Opportunity.getOpportunity(
                catalogHierarchy.division.Id,
                catalogHierarchy.businessUnit.Id,
                account.Id,
                'Applicant',
                'Applicant - Application Submitted / New'
        );

        IT_ExtendedInfo__c extendedInfo = IT_DataFaker_Objects.createExtendedInfo(account.id, opportunity.IT_Brand__c, false);
        extendedInfo.IT_LastContactInformationDate__c = Datetime.newInstance(2022, 1, 2, 0, 0 , 0);
        extendedInfo.IT_LastContactAdmissionDate__c = Datetime.newInstance(2022, 1, 1, 0, 0 , 0);
        System.debug('extendedInfo.IT_LastContactInformationDate__c:: ' + extendedInfo.IT_LastContactInformationDate__c);
        System.debug('extendedInfo.IT_LastContactAdmissionDate__c:: ' + extendedInfo.IT_LastContactAdmissionDate__c);
        insert extendedInfo;

        System.debug('extendedInfo inserted:: ' + [SELECT Id, IT_LastContactInformationDate__c, IT_LastContactAdmissionDate__c FROM IT_ExtendedInfo__c]);

        IT_OpportunityAssignmentRule__c oar = new IT_OpportunityAssignmentRule__c();
        oar.IT_BusinessUnit__c = opportunity.CORE_OPP_Business_Unit__c;
        oar.IT_MainCountry__c = opportunity.Account.PersonMailingCountry;
        oar.IT_CaptureChannel__c = opportunity.CORE_Capture_Channel__c;
        insert oar;

        IT_CountryRelevance__c cr = new IT_CountryRelevance__c();
        cr.IT_BusinessUnit__c = opportunity.CORE_OPP_Business_Unit__c;
        cr.IT_CountryISO__c = opportunity.Account.PersonMailingCountry;
        insert cr;
        
        ServiceAppointment sa = new ServiceAppointment();
        sa.Status = 'None';
        sa.ParentRecordId = opportunity.Id;
        sa.CORE_TECH_Relatd_Opportunity__c = opportunity.Id; 
        sa.DueDate = System.now().addDays(1);
        sa.EarliestStartTime = System.now();
        sa.SchedStartTime = System.now();
        insert sa;
    }

    @isTest
    public static void afterInsertTest() {
        /* Opportunity opportunity     = [SELECT Id FROM Opportunity];
        
 		 ServiceAppointment sa_cancelled = new ServiceAppointment();
         sa_cancelled.Status = 'Canceled';
         sa_cancelled.Status = 'None';
         sa_cancelled.ParentRecordId = opportunity.Id,
         sa_cancelled.DueDate = System.now().addDays(1);
         sa_cancelled.EarliestStartTime = System.now();
         insert sa_cancelled;
       */ 
        Test.startTest();

        Test.stopTest();
    }
    
    @isTest
    public static void afterUpdateTest() {
        Opportunity opportunity     = [SELECT Id FROM Opportunity LIMIT 1];
        ServiceAppointment sa       = [SELECT Id FROM ServiceAppointment LIMIT 1];
        
        Test.startTest();
        sa.Comments = '1234';
            update sa;
        Test.stopTest();
    }
    
    @isTest
    public static void afterUpdateTaskCancelled() {
        ServiceAppointment sa       = [SELECT Id, CORE_TECH_Relatd_Opportunity__c FROM ServiceAppointment LIMIT 1];
		
        Id rtId = Schema.SObjectType.Task.getRecordTypeInfosByDeveloperName().get('CORE_Continuum_of_action').getRecordTypeId();
        Task task = new Task();
        task.WhatId = sa.CORE_TECH_Relatd_Opportunity__c;
        task.Subject    = 'Fake Task';
        task.Priority   = 'CORE_PKL_Normal';
        task.Status     = 'CORE_PKL_Open';
        task.IT_COAProcess__c = 'Booking';
        task.RecordTypeId = rtId;
        insert task;
            Test.startTest();
        		sa.Status = 'Canceled';
            	sa.Comments = '1234';
                update sa;
            Test.stopTest();
    }
}