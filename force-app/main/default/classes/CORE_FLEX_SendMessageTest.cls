@isTest
private class CORE_FLEX_SendMessageTest {
	private class RestMock implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            String fullJson = '[]';

            HTTPResponse res = new HTTPResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setBody(fullJson);
            res.setStatusCode(200);
            return res;
        }
    }
    
    static {
        CORE_FLEX_Settings__c cs = new CORE_FLEX_Settings__c();
		Blob cryptoKey = Crypto.generateAesKey(256);
		Blob data = Blob.valueOf('password');
		Blob encryptedData = Crypto.encryptWithManagedIV('AES256', cryptoKey, data);

		cs.UrlFlex__c='https://project.twil.io';
		cs.TokenFlex__c=EncodingUtil.base64Encode(encryptedData);
		cs.KeySecret__c= EncodingUtil.base64Encode(cryptoKey);
        
		insert cs;
    }

    @isTest
    static void testLoadPhones() {        
        CORE_FLEX_SendMessage.Phones phones = CORE_FLEX_SendMessage.loadPhones(null);
        system.assert(phones.CustomerPhones.size()==0);
        system.assert(phones.TwilioPhones.size()==0);
        Account account = CORE_DataFaker_Account.getStudentAccount('student1');
        account.Phone = '+33602030405';
        update account;

        Test.setMock(HttpCalloutMock.class, new RestMock());
        Test.startTest();
        phones = CORE_FLEX_SendMessage.loadPhones(account.Id);
        system.assert(phones.CustomerPhones.size()==1);
        system.assert(phones.TwilioPhones.size()==0);     
        
        Test.stopTest();
    }
    
    @isTest
    static void testSendFlexSms() {        
        string result = CORE_FLEX_SendMessage.sendFlexSms(null,null,null);
        system.assert(result=='KO');
        
        string customerPhone='+33602030405';
        string twilioPhone='+33102030405';
        
        Account account = CORE_DataFaker_Account.getStudentAccount('student1');
        
        Test.setMock(HttpCalloutMock.class, new RestMock());
        Test.startTest();
        result = CORE_FLEX_SendMessage.sendFlexSms(customerPhone,twilioPhone,account.Id);
        //system.assert(result=='OK');
        Test.stopTest();        
    }

    @isTest
    static void testSendFlexWhatsapp() {        
        string result = CORE_FLEX_SendMessage.sendFlexWhatsapp(null,null,null);
        system.assert(result=='KO');
        
        string customerPhone='+33602030405';
        string twilioPhone='+33102030405';
        
        Account account = CORE_DataFaker_Account.getStudentAccount('student1');
        
        Test.setMock(HttpCalloutMock.class, new RestMock());
        Test.startTest();
        result = CORE_FLEX_SendMessage.sendFlexWhatsapp(customerPhone,twilioPhone,account.Id);
        //system.assert(result=='OK');
        Test.stopTest();        
    }
    
    
}