/*******************************************************/
/**     @Author: Valentin Pitel                       **/
/**     @OriginClass: CORE_AccountDataMaker           **/
/*******************************************************/
@IsTest
public class CORE_AccountDataMakerTest {

    private static CORE_LeadFormAcquisitionWrapper.PersonAccountWrapper getPaw(){
        CORE_LeadFormAcquisitionWrapper.PersonAccountWrapper paw = new CORE_LeadFormAcquisitionWrapper.PersonAccountWrapper();
        paw.creationDate = '1622798507000';
		paw.salutation = 'Mr';
		paw.lastName = 'TestLastName';
		paw.firstName = 'TestFirstName';
		paw.phone = '+33200000000';
		paw.mobilePhone = '+33600000000';
		paw.emailAddress = 'test@email.com';
		paw.street = '2 rue de paris';
		paw.postalCode = '75000';
		paw.city = 'Paris';
		paw.stateCode = 'state';
		paw.countryCode = 'FRA';
		paw.gender = 'CORE_PKL_Male';
		paw.languageWebsite = 'CORE_PKL_University_Student';
		paw.countryCode = 'FRA';
		paw.birthDate = Date.newInstance(1980, 2, 7);
		paw.birthYear = 1980;

        return paw;
    }

    @IsTest
    public static void initPersonAccount_Should_Return_an_account_without_type() {
        CORE_LeadFormAcquisitionWrapper.PersonAccountWrapper paw = getPaw();
        Boolean countryPicklistIsActivated = CORE_SobjectUtils.doesFieldExist('User', 'Countrycode');

        Test.startTest();
        Account account = CORE_AccountDataMaker.initPersonAccount(paw,countryPicklistIsActivated);
        Test.stopTest();

        System.assertNotEquals(null, account);

        Id expectedRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(CORE_AccountDataMaker.STUDENT_RECORDTYPE_DEVNAME).getRecordTypeId();

        System.assertEquals(expectedRecordTypeId, account.RecordTypeId);
        System.assertEquals(null, account.Type);

        System.assertEquals(CORE_DataConvertion.convertTimeStampToDateTime(paw.creationDate, false), account.CreatedDate);
        System.assertEquals(paw.salutation, account.Salutation);
        System.assertEquals(paw.lastName, account.LastName);
        System.assertEquals(paw.firstName, account.FirstName);
        System.assertEquals(paw.phone, account.Phone);
        System.assertEquals(paw.mobilePhone, account.PersonMobilePhone);
        System.assertEquals(paw.emailAddress, account.PersonEmail);
        System.assertEquals(paw.street, account.PersonMailingStreet);
        System.assertEquals(paw.postalcode, account.PersonMailingPostalCode);
        System.assertEquals(paw.city, account.PersonMailingCity);
        System.assertEquals(paw.languageWebsite, account.CORE_Language_website__pc);
    }

    
    @IsTest
    public static void initPersonAccount_Should_Return_an_account() {
        CORE_LeadFormAcquisitionWrapper.PersonAccountWrapper paw = getPaw();
        Boolean countryPicklistIsActivated = CORE_SobjectUtils.doesFieldExist('User', 'Countrycode');

        Test.startTest();
        Account account = CORE_AccountDataMaker.initPersonAccount(paw, countryPicklistIsActivated);
        Test.stopTest();

        System.assertNotEquals(null, account);

        Id expectedRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(CORE_AccountDataMaker.STUDENT_RECORDTYPE_DEVNAME).getRecordTypeId();

        System.assertEquals(expectedRecordTypeId, account.RecordTypeId);

        System.assertEquals(CORE_DataConvertion.convertTimeStampToDateTime(paw.creationDate, false), account.CreatedDate);
        System.assertEquals(paw.salutation, account.Salutation);
        System.assertEquals(paw.lastName, account.LastName);
        System.assertEquals(paw.firstName, account.FirstName);
        System.assertEquals(paw.phone, account.Phone);
        System.assertEquals(paw.mobilePhone, account.PersonMobilePhone);
        System.assertEquals(paw.emailAddress, account.PersonEmail);
        System.assertEquals(paw.street, account.PersonMailingStreet);
        System.assertEquals(paw.postalcode, account.PersonMailingPostalCode);
        System.assertEquals(paw.city, account.PersonMailingCity);
        System.assertEquals(paw.languageWebsite, account.CORE_Language_website__pc);
    }

    @IsTest
    public static void getUpdatedPersonAccount_Should_Return_an_account_with_new_values_if_old_values_are_empty() {
        CORE_LeadFormAcquisitionWrapper.PersonAccountWrapper paw = getPaw();
        Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(CORE_AccountDataMaker.STUDENT_RECORDTYPE_DEVNAME).getRecordTypeId();

        Account account = new Account(
            CreatedDate = CORE_DataConvertion.convertTimeStampToDateTime(paw.creationDate, false),
            Salutation = paw.salutation,
            LastName = paw.lastName,
            FirstName = null,
            Phone = paw.phone,
            PersonMobilePhone = null,
            PersonEmail = null,
            PersonMailingStreet = null,
            PersonMailingPostalCode = paw.postalcode,
            PersonMailingCity = 'France',
            PersonMailingState = 'ee',
            CORE_Language_website__pc = null,
            RecordTypeId = recordTypeId
        );
        Boolean countryPicklistIsActivated = CORE_SobjectUtils.doesFieldExist('User', 'Countrycode');

        Test.startTest();
        Account accountResult = CORE_AccountDataMaker.getUpdatedPersonAccount(account,paw, countryPicklistIsActivated);
        

        System.assertNotEquals(null, accountResult);
        System.assertEquals(account.Salutation, accountResult.Salutation);
        System.assertEquals(account.LastName, accountResult.LastName);
        System.assertEquals(paw.firstName, accountResult.FirstName);
        System.assertEquals(paw.street, accountResult.PersonMailingStreet);
        System.assertEquals(account.PersonMailingPostalCode, accountResult.PersonMailingPostalCode);
        System.assertEquals(account.PersonMailingCity, accountResult.PersonMailingCity);
        System.assertEquals(paw.languageWebsite, accountResult.CORE_Language_website__pc);
        System.assertEquals(account.PersonMailingState, accountResult.PersonMailingState);
        
        account.Salutation = null;
        account.LastName = null;
        account.PersonMailingPostalCode = null;
        account.PersonMailingCity = null;
        account.PersonMailingState = null;
        accountResult = CORE_AccountDataMaker.getUpdatedPersonAccount(account,paw, countryPicklistIsActivated);
        System.assertEquals(paw.salutation, accountResult.Salutation);
        System.assertEquals(paw.lastName, accountResult.LastName);
        System.assertEquals(paw.postalcode, accountResult.PersonMailingPostalCode);
        System.assertEquals(paw.city, accountResult.PersonMailingCity);
        System.assertEquals(paw.birthYear, accountResult.CORE_Year_of_birth__pc);
        System.assertEquals(paw.birthDate, accountResult.PersonBirthdate);

        Test.stopTest();
    }

    @IsTest
    public static void getUpdatedPersonAccount_Should_Return_an_account_with_new_values_if_updateIfNotNull_is_true() {
        CORE_LeadFormAcquisitionWrapper.PersonAccountWrapper paw = getPaw();
        paw.updateIfNotNull = true;
        Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(CORE_AccountDataMaker.STUDENT_RECORDTYPE_DEVNAME).getRecordTypeId();

        Account account = new Account(
            CreatedDate = CORE_DataConvertion.convertTimeStampToDateTime(paw.creationDate, false),
            Salutation = paw.salutation,
            LastName = paw.lastName,
            FirstName = null,
            Phone = paw.phone,
            PersonMobilePhone = null,
            PersonEmail = null,
            PersonMailingStreet = null,
            PersonMailingPostalCode = paw.postalcode,
            PersonMailingCity = 'France',
            PersonMailingState = 'ee',
            CORE_Language_website__pc = null,
            PersonBirthdate = Date.newInstance(1900, 2, 17),
            CORE_Year_of_birth__pc = 1980,
            RecordTypeId = recordTypeId
        );
        Boolean countryPicklistIsActivated = CORE_SobjectUtils.doesFieldExist('User', 'Countrycode');

        Test.startTest();
        Account accountResult = CORE_AccountDataMaker.getUpdatedPersonAccount(account,paw, countryPicklistIsActivated);
        

        System.assertNotEquals(null, accountResult);
        System.assertEquals(paw.Salutation, accountResult.Salutation);
        System.assertEquals(paw.LastName, accountResult.LastName);
        System.assertEquals(paw.firstName, accountResult.FirstName);
        System.assertEquals(paw.street, accountResult.PersonMailingStreet);
        System.assertEquals(paw.postalcode, accountResult.PersonMailingPostalCode);
        System.assertEquals(paw.city, accountResult.PersonMailingCity);
        System.assertEquals(paw.languageWebsite, accountResult.CORE_Language_website__pc);
        System.assertEquals(paw.birthDate, accountResult.PersonBirthdate);
        System.assertEquals(paw.birthYear, accountResult.CORE_Year_of_birth__pc);

        Test.stopTest();
    }
    
    @IsTest
    public static void getAccountWithPhoneFields_should_add_phone_in_phone_fields_if_not_exist() {
        CORE_LeadFormAcquisitionWrapper.PersonAccountWrapper paw = getPaw();
        Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(CORE_AccountDataMaker.STUDENT_RECORDTYPE_DEVNAME).getRecordTypeId();
        String phone1 = '+33200000001';
        String phone2 = '+33200000002';
        String phone3 = '+33200000003';
        String mobile1 = '+33600000001';
        String mobile2 = '+33600000002';
        String mobile3 = '+33600000003';
        String mobile4 = '+33600000004';

        Account account = new Account(
            CreatedDate = CORE_DataConvertion.convertTimeStampToDateTime(paw.creationDate, false),
            Salutation = paw.salutation,
            LastName = paw.lastName,
            FirstName = null,
            Phone = null,
            PersonMobilePhone = null,
            PersonEmail = null,
            PersonMailingStreet = null,
            PersonMailingPostalCode = paw.postalcode,
            PersonMailingCity = 'France',
            CORE_Language_website__pc = null,
            RecordTypeId = recordTypeId
        );

        Test.startTest();
        Account accountResult = CORE_AccountDataMaker.getAccountWithPhoneFields(account,phone1,false, false);
        
        
        System.assertNotEquals(null, accountResult);
        System.assertEquals(phone1, accountResult.Phone);
        System.assertEquals(null, accountResult.CORE_Phone2__pc);
        System.assertEquals(null, accountResult.PersonMobilePhone);
        System.assertEquals(null, accountResult.CORE_Mobile_2__pc);
        System.assertEquals(null, accountResult.CORE_Mobile_3__pc);

        //not updated because phone already exist
        accountResult = CORE_AccountDataMaker.getAccountWithPhoneFields(account,phone1,false, false);
        System.assertNotEquals(null, accountResult);
        System.assertEquals(phone1, accountResult.Phone);
        System.assertEquals(null, accountResult.CORE_Phone2__pc);
        System.assertEquals(null, accountResult.PersonMobilePhone);
        System.assertEquals(null, accountResult.CORE_Mobile_2__pc);
        System.assertEquals(null, accountResult.CORE_Mobile_3__pc);

        accountResult = CORE_AccountDataMaker.getAccountWithPhoneFields(account,phone2,false, false);
        System.assertNotEquals(null, accountResult);
        System.assertEquals(phone2, accountResult.Phone);
        System.assertEquals(phone1, accountResult.CORE_Phone2__pc);
        System.assertEquals(null, accountResult.PersonMobilePhone);
        System.assertEquals(null, accountResult.CORE_Mobile_2__pc);
        System.assertEquals(null, accountResult.CORE_Mobile_3__pc);

        accountResult = CORE_AccountDataMaker.getAccountWithPhoneFields(account,phone2,false, false);
        System.assertNotEquals(null, accountResult);
        System.assertEquals(phone2, accountResult.Phone);
        System.assertEquals(phone1, accountResult.CORE_Phone2__pc);
        System.assertEquals(null, accountResult.PersonMobilePhone);
        System.assertEquals(null, accountResult.CORE_Mobile_2__pc);
        System.assertEquals(null, accountResult.CORE_Mobile_3__pc);

        accountResult = CORE_AccountDataMaker.getAccountWithPhoneFields(account,phone3,false, false);
        System.assertNotEquals(null, accountResult);
        System.assertEquals(phone2, accountResult.Phone);
        System.assertEquals(phone1, accountResult.CORE_Phone2__pc);
        System.assertEquals(null, accountResult.PersonMobilePhone);
        System.assertEquals(null, accountResult.CORE_Mobile_2__pc);
        System.assertEquals(null, accountResult.CORE_Mobile_3__pc);

        accountResult = CORE_AccountDataMaker.getAccountWithPhoneFields(account,phone3,false, true);
        System.assertNotEquals(null, accountResult);
        System.assertEquals(phone3, accountResult.Phone);
        System.assertEquals(phone2, accountResult.CORE_Phone2__pc);
        System.assertEquals(null, accountResult.PersonMobilePhone);
        System.assertEquals(null, accountResult.CORE_Mobile_2__pc);
        System.assertEquals(null, accountResult.CORE_Mobile_3__pc);

        accountResult = CORE_AccountDataMaker.getAccountWithPhoneFields(account,mobile1,true, false);
        System.assertNotEquals(null, accountResult);
        System.assertEquals(phone3, accountResult.Phone);
        System.assertEquals(phone2, accountResult.CORE_Phone2__pc);
        System.assertEquals(mobile1, accountResult.PersonMobilePhone);
        System.assertEquals(null, accountResult.CORE_Mobile_2__pc);
        System.assertEquals(null, accountResult.CORE_Mobile_3__pc);

        accountResult = CORE_AccountDataMaker.getAccountWithPhoneFields(account,mobile1,true, false);
        System.assertNotEquals(null, accountResult);
        System.assertEquals(phone3, accountResult.Phone);
        System.assertEquals(phone2, accountResult.CORE_Phone2__pc);
        System.assertEquals(mobile1, accountResult.PersonMobilePhone);
        System.assertEquals(null, accountResult.CORE_Mobile_2__pc);
        System.assertEquals(null, accountResult.CORE_Mobile_3__pc);

        accountResult = CORE_AccountDataMaker.getAccountWithPhoneFields(account,mobile2,true, false);
        System.assertNotEquals(null, accountResult);
        System.assertEquals(phone3, accountResult.Phone);
        System.assertEquals(phone2, accountResult.CORE_Phone2__pc);
        System.assertEquals(mobile2, accountResult.PersonMobilePhone);
        System.assertEquals(mobile1, accountResult.CORE_Mobile_2__pc);
        System.assertEquals(null, accountResult.CORE_Mobile_3__pc);

        accountResult = CORE_AccountDataMaker.getAccountWithPhoneFields(account,mobile2,true, false);
        System.assertNotEquals(null, accountResult);
        System.assertEquals(phone3, accountResult.Phone);
        System.assertEquals(phone2, accountResult.CORE_Phone2__pc);
        System.assertEquals(mobile2, accountResult.PersonMobilePhone);
        System.assertEquals(mobile1, accountResult.CORE_Mobile_2__pc);
        System.assertEquals(null, accountResult.CORE_Mobile_3__pc);

        accountResult = CORE_AccountDataMaker.getAccountWithPhoneFields(account,mobile3,true, false);
        System.assertNotEquals(null, accountResult);
        System.assertEquals(phone3, accountResult.Phone);
        System.assertEquals(phone2, accountResult.CORE_Phone2__pc);
        System.assertEquals(mobile3, accountResult.PersonMobilePhone);
        System.assertEquals(mobile2, accountResult.CORE_Mobile_2__pc);
        System.assertEquals(mobile1, accountResult.CORE_Mobile_3__pc);

        accountResult = CORE_AccountDataMaker.getAccountWithPhoneFields(account,mobile3,true, false);
        System.assertNotEquals(null, accountResult);
        System.assertEquals(phone3, accountResult.Phone);
        System.assertEquals(phone2, accountResult.CORE_Phone2__pc);
        System.assertEquals(mobile3, accountResult.PersonMobilePhone);
        System.assertEquals(mobile2, accountResult.CORE_Mobile_2__pc);
        System.assertEquals(mobile1, accountResult.CORE_Mobile_3__pc);

        accountResult = CORE_AccountDataMaker.getAccountWithPhoneFields(account,mobile4,true, false);
        System.assertNotEquals(null, accountResult);
        System.assertEquals(phone3, accountResult.Phone);
        System.assertEquals(phone2, accountResult.CORE_Phone2__pc);
        System.assertEquals(mobile3, accountResult.PersonMobilePhone);
        System.assertEquals(mobile2, accountResult.CORE_Mobile_2__pc);
        System.assertEquals(mobile1, accountResult.CORE_Mobile_3__pc);

        accountResult = CORE_AccountDataMaker.getAccountWithPhoneFields(account,mobile4,true, true);
        System.assertNotEquals(null, accountResult);
        System.assertEquals(phone3, accountResult.Phone);
        System.assertEquals(phone2, accountResult.CORE_Phone2__pc);
        System.assertEquals(mobile4, accountResult.PersonMobilePhone);
        System.assertEquals(mobile3, accountResult.CORE_Mobile_2__pc);
        System.assertEquals(mobile2, accountResult.CORE_Mobile_3__pc);

        accountResult = CORE_AccountDataMaker.getAccountWithPhoneFields(account,mobile4,true, true);
        System.assertNotEquals(null, accountResult);
        System.assertEquals(phone3, accountResult.Phone);
        System.assertEquals(phone2, accountResult.CORE_Phone2__pc);
        System.assertEquals(mobile4, accountResult.PersonMobilePhone);
        System.assertEquals(mobile3, accountResult.CORE_Mobile_2__pc);
        System.assertEquals(mobile2, accountResult.CORE_Mobile_3__pc);

        accountResult = CORE_AccountDataMaker.getAccountWithPhoneFields(account,mobile1,true, true);
        System.assertNotEquals(null, accountResult);
        System.assertEquals(phone3, accountResult.Phone);
        System.assertEquals(phone2, accountResult.CORE_Phone2__pc);
        System.assertEquals(mobile1, accountResult.PersonMobilePhone);
        System.assertEquals(mobile4, accountResult.CORE_Mobile_2__pc);
        System.assertEquals(mobile3, accountResult.CORE_Mobile_3__pc);
        Test.stopTest();
    }

    @IsTest
    public static void getAccountWithEmailFields_should_add_email_in_email_fields_if_not_exist() {
        CORE_LeadFormAcquisitionWrapper.PersonAccountWrapper paw = getPaw();
        Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(CORE_AccountDataMaker.STUDENT_RECORDTYPE_DEVNAME).getRecordTypeId();
        String email1 = 'test1@test.fr';
        String email2 = 'test2@test.fr';
        String email3 = 'test3@test.fr';
        String email4 = 'test4@test.fr';
        Account account = new Account(
            CreatedDate = CORE_DataConvertion.convertTimeStampToDateTime(paw.creationDate, false),
            Salutation = paw.salutation,
            LastName = paw.lastName,
            FirstName = null,
            Phone = null,
            PersonMobilePhone = null,
            PersonEmail = null,
            PersonMailingStreet = null,
            PersonMailingPostalCode = paw.postalcode,
            PersonMailingCity = 'France',
            CORE_Language_website__pc = null,
            RecordTypeId = recordTypeId
        );

        Test.startTest();

        Account accountResult = CORE_AccountDataMaker.getAccountWithEmailFields(account,email1, false);
        System.assertNotEquals(null, accountResult);
        System.assertEquals(email1, accountResult.PersonEmail);
        System.assertEquals(null, accountResult.CORE_Email_2__pc);
        System.assertEquals(null, accountResult.CORE_Email_3__pc);

        //not updated because phone already exist
        accountResult = CORE_AccountDataMaker.getAccountWithEmailFields(account,email1, false);
        System.assertNotEquals(null, accountResult);
        System.assertEquals(email1, accountResult.PersonEmail);
        System.assertEquals(null, accountResult.CORE_Email_2__pc);
        System.assertEquals(null, accountResult.CORE_Email_3__pc);

        accountResult = CORE_AccountDataMaker.getAccountWithEmailFields(account,email2, false);
        System.assertNotEquals(null, accountResult);
        System.assertEquals(email2, accountResult.PersonEmail);
        System.assertEquals(email1, accountResult.CORE_Email_2__pc);
        System.assertEquals(null, accountResult.CORE_Email_3__pc);

        accountResult = CORE_AccountDataMaker.getAccountWithEmailFields(account,email2, false);
        System.assertNotEquals(null, accountResult);
        System.assertEquals(email2, accountResult.PersonEmail);
        System.assertEquals(email1, accountResult.CORE_Email_2__pc);
        System.assertEquals(null, accountResult.CORE_Email_3__pc);

        accountResult = CORE_AccountDataMaker.getAccountWithEmailFields(account,email3, false);
        System.assertNotEquals(null, accountResult);
        System.assertEquals(email3, accountResult.PersonEmail);
        System.assertEquals(email2, accountResult.CORE_Email_2__pc);
        System.assertEquals(email1, accountResult.CORE_Email_3__pc);

        accountResult = CORE_AccountDataMaker.getAccountWithEmailFields(account,email3, false);
        System.assertNotEquals(null, accountResult);
        System.assertEquals(email3, accountResult.PersonEmail);
        System.assertEquals(email2, accountResult.CORE_Email_2__pc);
        System.assertEquals(email1, accountResult.CORE_Email_3__pc);

        accountResult = CORE_AccountDataMaker.getAccountWithEmailFields(account,email4, false);
        System.assertNotEquals(null, accountResult);
        System.assertEquals(email3, accountResult.PersonEmail);
        System.assertEquals(email2, accountResult.CORE_Email_2__pc);
        System.assertEquals(email1, accountResult.CORE_Email_3__pc);

        accountResult = CORE_AccountDataMaker.getAccountWithEmailFields(account,email4, true);
        System.assertNotEquals(null, accountResult);
        System.assertEquals(email4, accountResult.PersonEmail);
        System.assertEquals(email3, accountResult.CORE_Email_2__pc);
        System.assertEquals(email2, accountResult.CORE_Email_3__pc);
        Test.stopTest();
    }

}