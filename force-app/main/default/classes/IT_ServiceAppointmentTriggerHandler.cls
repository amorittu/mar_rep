/**
 * Created by aMorittu on 01/02/2022
 * Test class: IT_ServiceAppointmentTriggerHandlerTest
 */
public class IT_ServiceAppointmentTriggerHandler extends TriggerHandler  {
    public static final String CLASS_NAME = IT_ServiceAppointmentTriggerHandler.class.getName();


    // public void beforeInsert(ServiceAppointment[] newObjects){
    //     System.debug(CLASS_NAME + ' - afterInsert START ------------------------------------------------------------ ');
    //     setDefaultAttempts(newObjects);
    //     System.debug(CLASS_NAME + ' - afterInsert END ------------------------------------------------------------ ');
    // }

    // public static void setDefaultAttempts(ServiceAppointment[] newObjects) {
    //     System.debug(CLASS_NAME + ' - setDefaultAttempts START ------------------------------------------------------------ ');
    //     for(ServiceAppointment record : newObjects) {
    //         if (String.isBlank(String.valueOf(record.IT_ProspectionAttempts__c) )  ) {
    //             record.IT_ProspectionAttempts__c = 0;
    //         }
    //         if (String.isBlank(String.valueOf(record.IT_ReminderAttempts__c) )  ) {
    //             record.IT_ReminderAttempts__c = 0;
    //         }
    //     }
    //     System.debug(CLASS_NAME + ' - setDefaultAttempts END ------------------------------------------------------------ ');
    // }


    public void afterInsert(ServiceAppointment[] newObjects){
        IT_OpportunityStatusMgm.checkAppointmentAdding(newObjects, null);
        ServiceAppointment[] toProcess = new ServiceAppointment[]{};

        Set<Id> serviceAppointmentIDSCoaBooking = new Set<Id>();
        for (ServiceAppointment SA_record : newObjects ) {
            if (SA_record.Status != 'Canceled' && String.isNotBlank(SA_record.CORE_TECH_Relatd_Opportunity__c) ) { // 09/06 fix on related opportunity
                serviceAppointmentIDSCoaBooking.add(SA_record.Id);
            }
        }

        if (serviceAppointmentIDSCoaBooking.size() > 0 ) {
            System.enqueueJob(new IT_SPcoaBookingQueueable(serviceAppointmentIDSCoaBooking));
        }
    }
 
    public void afterUpdate(ServiceAppointment[] newObjects, Map<Id, ServiceAppointment> oldRecordsMap ){
        IT_OpportunityStatusMgm.checkAppointmentAdding(newObjects, oldRecordsMap);
        Set<Id> serviceAppointmentIDSCoaBooking = new Set<Id>();
        Map<String, String> serviceAppointmentIDSCancelled = new Map<String, String>();

        for (ServiceAppointment SA_record : newObjects ) {

            if (SA_record.Status != 'Canceled') { // CCI-137
                //EScudo, va in loop perchè gira ad ogni update, commento ipotizzando che il CoA non parta mai alla modifica del SA
                //serviceAppointmentIDSCoaBooking.add(SA_record.Id);
            } else {
                if( String.isNotBlank(SA_record.CORE_TECH_Relatd_Opportunity__c) ) { // 09/06 fix on related opportunity
                    serviceAppointmentIDSCancelled.put(SA_record.CORE_TECH_Relatd_Opportunity__c, SA_record.Id );
                }
            }
        }

        

        Task[] taskToUpdate = new Task[]{};
        if (serviceAppointmentIDSCancelled.size() >  0 ) {
            Task[] tasksToCancel = [SELECT Id FROM Task WHERE WhatId =: serviceAppointmentIDSCancelled.keySet() AND RecordType.DeveloperName = 'CORE_Continuum_of_action' AND IT_COAProcess__c  LIKE '%Booking%' ];
            for (Task task : tasksToCancel) {
                task.status = 'CORE_PKL_Cancelled';
                taskToUpdate.add(task);
            }
        }
        if (taskToUpdate.size() > 0 ) {
            update taskToUpdate;
        }
        
        if (serviceAppointmentIDSCoaBooking.size() > 0 && !Test.isRunningTest() ) {
            System.enqueueJob(new IT_SPcoaBookingQueueable(serviceAppointmentIDSCoaBooking));
        }
    }
}