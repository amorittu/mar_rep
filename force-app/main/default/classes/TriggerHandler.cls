/**
*	Author:			CRMoving Build Team
*	Company:		Atlantic Technologies S.p.A.
*	Created date:	28/04/2019
*	Description:	Class used to extend Apex trigger handlers related to sObjects
*	Test Classes:	Test_TriggerHandler
**/
public virtual class TriggerHandler {
    
	static	AT_TriggerManager__c	atCS	=	AT_TriggerManager__c.getInstance();

	@testVisible
	final	String	fieldNameBefInsMDT		=	AT_SObjTriggerMgr__mdt.BypassBeforeInsert__c.getDescribe().getName();
	
	@testVisible
	final	String	fieldNameBefUpdMDT		=	AT_SObjTriggerMgr__mdt.BypassBeforeUpdate__c.getDescribe().getName();
	
	@testVisible
	final	String	fieldNameBefDelMDT		=	AT_SObjTriggerMgr__mdt.BypassBeforeDelete__c.getDescribe().getName();
	
	@testVisible
	final	String	fieldNameAftInsMDT		=	AT_SObjTriggerMgr__mdt.BypassAfterInsert__c.getDescribe().getName();
	
	@testVisible
	final	String	fieldNameAftUpdMDT		=	AT_SObjTriggerMgr__mdt.BypassAfterUpdate__c.getDescribe().getName();
	
	@testVisible
	final	String	fieldNameAftDelMDT		=	AT_SObjTriggerMgr__mdt.BypassAfterDelete__c.getDescribe().getName();
	
	@testVisible
	final	String	fieldNameAftUnDelMDT	=	AT_SObjTriggerMgr__mdt.BypassAfterUndelete__c.getDescribe().getName();
	
	//******************************

	@TestVisible
	final	String	fieldNameDisableAllCS	=	AT_TriggerManager__c.disableAll__c.getDescribe().getName();

	@testVisible
	final	String	fieldNameBefInsCS		=	AT_TriggerManager__c.DisableBeforeInsert__c.getDescribe().getName();
	
	@testVisible
	final	String	fieldNameBefUpdCS		=	AT_TriggerManager__c.DisableBeforeUpdate__c.getDescribe().getName();
	
	@testVisible
	final	String	fieldNameBefDelCS		=	AT_TriggerManager__c.DisableBeforeDelete__c.getDescribe().getName();
	
	@testVisible
	final	String	fieldNameAftInsCS		=	AT_TriggerManager__c.DisableAfterInsert__c.getDescribe().getName();
	
	@testVisible
	final	String	fieldNameAftUpdCS		=	AT_TriggerManager__c.DisableAfterUpdate__c.getDescribe().getName();
	
	@testVisible
	final	String	fieldNameAftDelCS		=	AT_TriggerManager__c.DisableAfterDelete__c.getDescribe().getName();
	
	@testVisible
	final	String	fieldNameAftUnDelCS		=	AT_TriggerManager__c.DisableAfterUndelete__c.getDescribe().getName();

	//******************************
	
	@testVisible
    //Retrieves sObject API Name
	public	String	getSObjectName(sObject sObj){
		
		return sObj.getsObjectType().getDescribe().getName();
	}
	
	@testVisible
    //Query custom metadata type for specific sObject
	virtual	List<AT_SObjTriggerMgr__mdt>	queryMetadata(String sObjName){
		
		return	[select BypassAfterDelete__c, BypassAfterInsert__c, BypassAfterUndelete__c,
						BypassAfterUpdate__c, BypassBeforeDelete__c, BypassBeforeInsert__c,
						BypassBeforeUpdate__c		
				from	AT_SObjTriggerMgr__mdt
				where	sObjectType__c=:sObjName];
	}
	
	@testVisible
    //Retrieves custom metadata type settings for specific sObject
	AT_SObjTriggerMgr__mdt	checkSObjMdt(String sObjName){
		
		List<AT_SObjTriggerMgr__mdt> listSobjTrMgr	=	queryMetadata(sObjName);
		
		return !listSobjTrMgr.isEmpty()? listSobjTrMgr[0]: new AT_SObjTriggerMgr__mdt();
	}
	
    //Generic method to detect if the event is bypassed for specific sObject and running user
    boolean	bypassEvent(List<sObject> listSObj, String fieldNameMDT, String fieldNameCS){
    	
    	boolean retVal	=	false;

		String sObjName;
		Object	tmpObjDisableAll = atCS.get(fieldNameDisableAllCS),
				tmpObjDisableEvt;

		//Check if all events are bypassed
		if(tmpObjDisableAll!=null && ((boolean) tmpObjDisableAll)){
    		
    		retVal	=	true;
    	}else{

			//General switch is not enabled, so checking if to bypass the interested event

    		if(listSObj!=null && !listSObj.isEmpty()){
    			
    			//Retrieving sObjectName
    			sObject sObj	=	listSObj[0];
    			sObjName		=	getSObjectName(sObj);
    			//

				tmpObjDisableEvt =	atCS.get(fieldNameCS);

	    		if(tmpObjDisableEvt!=null && ((boolean) tmpObjDisableEvt)){
	    		
	    			//Check if custom metadata has been provided for current sObject type
					AT_SObjTriggerMgr__mdt	sobjTrMgr	=	checkSObjMdt(sObjName);
					
					retVal	=	(boolean)	sobjTrMgr.get(fieldNameMDT);
	    			//
	    		}
    		}
    	}

		system.debug(System.LoggingLevel.FINE,
					'### TriggerHandler.bypassEvent '+
					(String.isNotBlank(sObjName)? 'for object ['+sObjName+'] ':'')+
					'final result:'+String.valueOf(retVal)+' - '+
					fieldNameDisableAllCS+':'+((tmpObjDisableAll==null)? String.valueOf(false): tmpObjDisableAll) +' - '+
					fieldNameMDT+':'+((tmpObjDisableEvt==null)? String.valueOf(false): tmpObjDisableEvt));
    	return	retVal;
    }
    
    //Checks if "Before Insert" event is bypassed for specific sObject and running user
    public	boolean	isBypassBeforeInsert(List<sObject> listSObj){
    	
    	return bypassEvent(listSObj, fieldNameBefInsMDT,  fieldNameBefInsCS);
    }
    
    //Checks if "Before Update" event is bypassed for specific sObject and running user
	public	boolean	isBypassBeforeUpdate(List<sObject> listSObj){
	
		return bypassEvent(listSObj, fieldNameBefUpdMDT, fieldNameBefUpdCS);
	}
	
    //Checks if "Before Delete" event is bypassed for specific sObject and running user
	public	boolean	isBypassBeforeDelete(List<sObject> listSObj){
		
		return bypassEvent(listSObj, fieldNameBefDelMDT, fieldNameBefDelCS);
	}
	
    //Checks if "After Insert" event is bypassed for specific sObject and running user
	public	boolean	isBypassAfterInsert(List<sObject> listSObj){
			
		return bypassEvent(listSObj, fieldNameAftInsMDT, fieldNameAftInsCS);
	}
	
    //Checks if "After Update" event is bypassed for specific sObject and running user
	public	boolean	isBypassAfterUpdate(List<sObject> listSObj){
		
		return bypassEvent(listSObj, fieldNameAftUpdMDT, fieldNameAftUpdCS);
	}
	
    //Checks if "After Delete" event is bypassed for specific sObject and running user
	public	boolean	isBypassAfterDelete(List<sObject> listSObj){
		
		return bypassEvent(listSObj, fieldNameAftDelMDT, fieldNameAftDelCS);
	}
	
    //Checks if "After Undelete" event is bypassed for specific sObject and running user
	public	boolean	isBypassAfterUndelete(List<sObject> listSObj){
		
		return bypassEvent(listSObj, fieldNameAftUndelMDT, fieldNameAftUndelCS);
	}
    
}