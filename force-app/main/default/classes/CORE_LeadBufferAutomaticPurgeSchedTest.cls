@IsTest
public class CORE_LeadBufferAutomaticPurgeSchedTest {
    @TestSetup
    public static void setup(){
        CORE_CountrySpecificSettings__c countrySetting = new CORE_CountrySpecificSettings__c(
                CORE_Retention_Delay__c= 3,
            	CORE_System_admins_email_adresses__c = 'yasminetahi@gmail.com;'
        );
        database.insert(countrySetting, true);
    }
    @IsTest
    public static void LeadBufferAutomaticPurge_update_Should_Be_Launch() {
        test.starttest();
        CORE_LeadBufferAutomaticPurgeSched myClass = new CORE_LeadBufferAutomaticPurgeSched();   
        String chron = '0 0 23 * * ?';        
        system.schedule('Test Sched', chron, myClass);
        test.stopTest();

        List<AsyncApexJob> jobsScheduled = [select Id, ApexClassID, ApexClass.Name, Status, JobType from AsyncApexJob where JobType = 'ScheduledApex'];
        System.assertEquals(1, jobsScheduled.size(), 'expecting one scheduled job');
        System.assertEquals('CORE_LeadBufferAutomaticPurgeSched', jobsScheduled[0].ApexClass.Name, 'expecting specific scheduled job');

        List<AsyncApexJob> jobsApexBatch = [select Id, ApexClassID, ApexClass.Name, Status, JobType from AsyncApexJob where JobType = 'BatchApex'];
        System.assertEquals(1, jobsApexBatch.size(), 'expecting one apex batch job');
        System.assertEquals('CORE_LeadBufferAutomaticPurge', jobsApexBatch[0].ApexClass.Name, 'expecting specific batch job');
    }
}