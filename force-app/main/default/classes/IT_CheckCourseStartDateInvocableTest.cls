@isTest
public with sharing class IT_CheckCourseStartDateInvocableTest {
    
    @IsTest
    public static void testBehavior(){

        CORE_DataFaker_CatalogHierarchy testCatalog = new CORE_DataFaker_CatalogHierarchy('key1');

        CORE_Business_Unit__c businessUnit = testCatalog.businessUnit;
        businessUnit.IT_AcademicYearStartMonth__c = '8';
        update businessUnit;

        CORE_Intake__c intake = testCatalog.intake;
        intake.CORE_Academic_Year__c = 'CORE_PKL_2021-2022';
        intake.CORE_Session__c = 'October';
        update intake; 

        IT_CheckCourseStartDateInvocable.FlowInputs flowInput = new IT_CheckCourseStartDateInvocable.FlowInputs();
        flowInput.intake = intake;
        flowInput.businessUnitId = businessUnit.Id;
        
        List<IT_CheckCourseStartDateInvocable.FlowInputs> flowInputs = new List<IT_CheckCourseStartDateInvocable.FlowInputs>{flowInput};
        IT_CheckCourseStartDateInvocable.getNextBH(flowInputs);
    }
}