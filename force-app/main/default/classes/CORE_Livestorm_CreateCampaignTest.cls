@IsTest
global class CORE_Livestorm_CreateCampaignTest {
    @TestSetup
    public static void makeData(){
        CORE_Livestorm_DataFaker_InitiateTest.CreateMetadata();
    }

    @isTest
    public static void fullSuccessExecuteTest() {        
        Test.setMock(HttpCalloutMock.class, new FullSuccessCreateCampaignMock());
        List<CORE_Business_Unit__c> bus = [Select Id FROM CORE_Business_Unit__c];

        Test.startTest();
        Campaign campaignInit = CORE_DataFaker_Campaign.getCampaign('', true, 'Planned', bus.get(0).Id, '(GMT+03:00) Eastern European Summer Time (Europe/Bucharest)', date.today().addDays(1), date.today().addDays(2));
        Test.stopTest();

        List<Campaign> campaigns = [SELECT Id,CORE_Online_event_url__c, CORE_Livestorm_Event_Id__c, CORE_Livestorm_Session_Id__c, CORE_Livestorm_Session_Link__c FROM Campaign];
        System.assertEquals(1, campaigns.size());

        Campaign campaign = campaigns.get(0);
        System.assertEquals('https://app.livestorm.co/p/2b7de38f-d2d9-4a73-beac-a6ed54d8a38b', campaign.CORE_Online_event_url__c);
        System.assertEquals('2b7de38f-d2d9-4a73-beac-a6ed54d8a38b', campaign.CORE_Livestorm_Event_Id__c);
        System.assertEquals('https://app.livestorm.co/p/2b7de38f-d2d9-4a73-beac-a6ed54d8a38b/live?s=6b3e4bde-2a75-4ed8-a4af-83a08a68a057', campaign.CORE_Livestorm_Session_Link__c);
        System.assertEquals('6b3e4bde-2a75-4ed8-a4af-83a08a68a057', campaign.CORE_Livestorm_Session_Id__c);
    }

    @isTest
    public static void fullSuccessWrongMetadataExecuteTest() {        
        Test.setMock(HttpCalloutMock.class, new FullSuccessCreateCampaignMock());
        List<CORE_Business_Unit__c> bus = [Select Id FROM CORE_Business_Unit__c];

        Test.startTest();
        Campaign campaignInit = CORE_DataFaker_Campaign.getCampaign('', true, 'Planned', bus.get(0).Id, null, date.today().addDays(1), date.today().addDays(2));
        Test.stopTest();

        List<Campaign> campaigns = [SELECT Id,CORE_Online_event_url__c, CORE_Livestorm_Event_Id__c, CORE_Livestorm_Session_Id__c, CORE_Livestorm_Session_Link__c FROM Campaign];
        System.assertEquals(1, campaigns.size());

        Campaign campaign = campaigns.get(0);
        System.assertEquals('https://app.livestorm.co/p/2b7de38f-d2d9-4a73-beac-a6ed54d8a38b', campaign.CORE_Online_event_url__c);
        System.assertEquals('2b7de38f-d2d9-4a73-beac-a6ed54d8a38b', campaign.CORE_Livestorm_Event_Id__c);
        System.assertEquals('https://app.livestorm.co/p/2b7de38f-d2d9-4a73-beac-a6ed54d8a38b/live?s=6b3e4bde-2a75-4ed8-a4af-83a08a68a057', campaign.CORE_Livestorm_Session_Link__c);
        System.assertEquals('6b3e4bde-2a75-4ed8-a4af-83a08a68a057', campaign.CORE_Livestorm_Session_Id__c);
    }

    
    global class FullSuccessCreateCampaignMock implements HttpCalloutMock {
        global HttpResponse respond(HttpRequest req) {
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');

            if (req.getEndpoint().endsWith('events')){
                res.setBody('{"data": {"id": "2b7de38f-d2d9-4a73-beac-a6ed54d8a38b","attributes": {"registration_link": "https://app.livestorm.co/p/2b7de38f-d2d9-4a73-beac-a6ed54d8a38b"}}}');
                res.setStatusCode(201);
            }
            else if (req.getEndpoint().endsWith('sessions')){
                res.setBody('{"data": {"id": "6b3e4bde-2a75-4ed8-a4af-83a08a68a057","attributes": {"room_link": "https://app.livestorm.co/p/2b7de38f-d2d9-4a73-beac-a6ed54d8a38b/live?s=6b3e4bde-2a75-4ed8-a4af-83a08a68a057"}}}');
                res.setStatusCode(201);
            }

            return res;
        }
    }
    
    @isTest
    public static void exceptionOccurDuringEventCreationExecuteTest() {        
        Test.setMock(HttpCalloutMock.class, new ExceptionOccurEventCreationCreateCampaignMock());
        List<CORE_Business_Unit__c> bus = [Select Id FROM CORE_Business_Unit__c];

        Test.startTest();
        Campaign campaignInit = CORE_DataFaker_Campaign.getCampaign('', true, 'Planned', bus.get(0).Id, null, date.today().addDays(1), date.today().addDays(2));
        Test.stopTest();

        List<Campaign> campaigns = [SELECT Id,CORE_Online_event_url__c, CORE_Livestorm_Event_Id__c, CORE_Livestorm_Session_Id__c, CORE_Livestorm_Session_Link__c FROM Campaign];
        System.assertEquals(1, campaigns.size());
        Campaign campaign = campaigns.get(0);
        System.assertEquals(null, campaign.CORE_Online_event_url__c);
        System.assertEquals(null, campaign.CORE_Livestorm_Event_Id__c);
        System.assertEquals(null, campaign.CORE_Livestorm_Session_Link__c);
        System.assertEquals(null, campaign.CORE_Livestorm_Session_Id__c);
    }

    
    global class ExceptionOccurEventCreationCreateCampaignMock implements HttpCalloutMock {
        global HttpResponse respond(HttpRequest req) {
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');

            if (req.getEndpoint().endsWith('events')){
                res.setBody('{"data2": {"id": "2b7de38f-d2d9-4a73-beac-a6ed54d8a38b","attributes": {"registration_link": "https://app.livestorm.co/p/2b7de38f-d2d9-4a73-beac-a6ed54d8a38b"}}}');
                res.setStatusCode(201);
            }
            else if (req.getEndpoint().endsWith('sessions')){
                res.setBody('{"data2": {"id": "6b3e4bde-2a75-4ed8-a4af-83a08a68a057","attributes": {"room_link": "https://app.livestorm.co/p/2b7de38f-d2d9-4a73-beac-a6ed54d8a38b/live?s=6b3e4bde-2a75-4ed8-a4af-83a08a68a057"}}}');
                res.setStatusCode(201);
            }

            return res;
        }
    }

    @isTest
    public static void exceptionOccurDuringSessionCreationExecuteTest() {        
        Test.setMock(HttpCalloutMock.class, new ExceptionOccurSessionCreationCreateCampaignMock());
        List<CORE_Business_Unit__c> bus = [Select Id FROM CORE_Business_Unit__c];

        Test.startTest();
        Campaign campaignInit = CORE_DataFaker_Campaign.getCampaign('', true, 'Planned', bus.get(0).Id, null, date.today().addDays(1), date.today().addDays(2));
        Test.stopTest();
        List<Campaign> campaigns = [SELECT Id,CORE_Online_event_url__c, CORE_Livestorm_Event_Id__c, CORE_Livestorm_Session_Id__c, CORE_Livestorm_Session_Link__c FROM Campaign];
        System.assertEquals(1, campaigns.size());
        Campaign campaign = campaigns.get(0);
        System.assertEquals('https://app.livestorm.co/p/2b7de38f-d2d9-4a73-beac-a6ed54d8a38b', campaign.CORE_Online_event_url__c);
        System.assertEquals('2b7de38f-d2d9-4a73-beac-a6ed54d8a38b', campaign.CORE_Livestorm_Event_Id__c);
        System.assertEquals(null, campaign.CORE_Livestorm_Session_Link__c);
        System.assertEquals(null, campaign.CORE_Livestorm_Session_Id__c);
    }

    
    global class ExceptionOccurSessionCreationCreateCampaignMock implements HttpCalloutMock {
        global HttpResponse respond(HttpRequest req) {
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');

            if (req.getEndpoint().endsWith('events')){
                res.setBody('{"data": {"id": "2b7de38f-d2d9-4a73-beac-a6ed54d8a38b","attributes": {"registration_link": "https://app.livestorm.co/p/2b7de38f-d2d9-4a73-beac-a6ed54d8a38b"}}}');
                res.setStatusCode(201);
            }
            else if (req.getEndpoint().endsWith('sessions')){
                res.setBody('{"data2": {"id": "6b3e4bde-2a75-4ed8-a4af-83a08a68a057","attributes": {"room_link": "https://app.livestorm.co/p/2b7de38f-d2d9-4a73-beac-a6ed54d8a38b/live?s=6b3e4bde-2a75-4ed8-a4af-83a08a68a057"}}}');
                res.setStatusCode(201);
            }

            return res;
        }
    }

    @isTest
    public static void partialSuccessExecuteTest() {        
        Test.setMock(HttpCalloutMock.class, new PartialSuccessCreateCampaignMock());
        List<CORE_Business_Unit__c> bus = [Select Id FROM CORE_Business_Unit__c];

        Test.startTest();
        Campaign campaignInit = CORE_DataFaker_Campaign.getCampaign('', true, 'Planned', bus.get(0).Id, null, date.today().addDays(1), date.today().addDays(2));
        Test.stopTest();
        List<Campaign> campaigns = [SELECT Id,CORE_Online_event_url__c, CORE_Livestorm_Event_Id__c, CORE_Livestorm_Session_Id__c, CORE_Livestorm_Session_Link__c FROM Campaign];
        System.assertEquals(1, campaigns.size());
        Campaign campaign = campaigns.get(0);
        System.assertEquals('https://app.livestorm.co/p/2b7de38f-d2d9-4a73-beac-a6ed54d8a38b', campaign.CORE_Online_event_url__c);
        System.assertEquals('2b7de38f-d2d9-4a73-beac-a6ed54d8a38b', campaign.CORE_Livestorm_Event_Id__c);
        System.assertEquals(null, campaign.CORE_Livestorm_Session_Link__c);
        System.assertEquals(null, campaign.CORE_Livestorm_Session_Id__c);
    }

    
    global class PartialSuccessCreateCampaignMock implements HttpCalloutMock {
        global HttpResponse respond(HttpRequest req) {
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');

            if (req.getEndpoint().endsWith('events')){
                res.setBody('{"data": {"id": "2b7de38f-d2d9-4a73-beac-a6ed54d8a38b","attributes": {"registration_link": "https://app.livestorm.co/p/2b7de38f-d2d9-4a73-beac-a6ed54d8a38b"}}}');
                res.setStatusCode(201);
            }
            else if (req.getEndpoint().endsWith('sessions')){
                res.setBody('{"data": {"id": "6b3e4bde-2a75-4ed8-a4af-83a08a68a057","attributes": {"room_link": "https://app.livestorm.co/p/2b7de38f-d2d9-4a73-beac-a6ed54d8a38b/live?s=6b3e4bde-2a75-4ed8-a4af-83a08a68a057"}}}');
                res.setStatusCode(202);
            }

            return res;
        }
    }

    @isTest
    public static void fullFailExecuteTest() {        
        Test.setMock(HttpCalloutMock.class, new FullFailCreateCampaignMock());
        List<CORE_Business_Unit__c> bus = [Select Id FROM CORE_Business_Unit__c];
        
        Test.startTest();
        Campaign campaignInit = CORE_DataFaker_Campaign.getCampaign('', true, 'In Progress', bus.get(0).Id, null, date.today().addDays(1), date.today().addDays(2));
        Test.stopTest();
        List<Campaign> campaigns = [SELECT Id,CORE_Online_event_url__c, CORE_Livestorm_Event_Id__c, CORE_Livestorm_Session_Id__c, CORE_Livestorm_Session_Link__c FROM Campaign];
        System.assertEquals(1, campaigns.size());
        Campaign campaign = campaigns.get(0);
        System.assertEquals(null, campaign.CORE_Online_event_url__c);
        System.assertEquals(null, campaign.CORE_Livestorm_Event_Id__c);
        System.assertEquals(null, campaign.CORE_Livestorm_Session_Link__c);
        System.assertEquals(null, campaign.CORE_Livestorm_Session_Id__c);
    }

    
    global class FullFailCreateCampaignMock implements HttpCalloutMock {
        global HttpResponse respond(HttpRequest req) {
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');

            if (req.getEndpoint().endsWith('events')){
                res.setBody('{"data": {"id": "2b7de38f-d2d9-4a73-beac-a6ed54d8a38b","attributes": {"registration_link": "https://app.livestorm.co/p/2b7de38f-d2d9-4a73-beac-a6ed54d8a38b"}}}');
                res.setStatusCode(202);
            }
            else if (req.getEndpoint().endsWith('sessions')){
                res.setBody('{"data": {"id": "6b3e4bde-2a75-4ed8-a4af-83a08a68a057","attributes": {"room_link": "https://app.livestorm.co/p/2b7de38f-d2d9-4a73-beac-a6ed54d8a38b/live?s=6b3e4bde-2a75-4ed8-a4af-83a08a68a057"}}}');
                res.setStatusCode(202);
            }

            return res;
        }
    } 
}