public without sharing class CORE_AsyncLFAWorkerOnlineShop extends CORE_AsyncLFAWorker {
    public CORE_AsyncLFAWorkerOnlineShop(List<CORE_LeadAcquisitionBuffer__c> bufferDatas){
        super(bufferDatas);
    }

    public override void executeOpportunityProcess(Savepoint sp){
        System.debug('@CORE_AsyncLFAWorkerOnlineShop > executeOpportunityProcess overrided');
        List<Opportunity> opportunitiesToUpsert = new List<Opportunity>();
        Map<Integer, Map<Id,Set<CORE_LFA_OpportunityManagement.OpportunityType>>> bufferRecordsByItOpp = new Map<Integer, Map<Id,Set<CORE_LFA_OpportunityManagement.OpportunityType>>>();
        Set<Id> existingOpportunities = new Set<Id>();
        Set<String> existingOpportunitiesExternal = new Set<String>();

        Map<Id, Account> accountUpserted = new Map<Id, Account> (this.accountUpserted);
        List<CORE_LeadAcquisitionBuffer__c> availableBufferRecord = getBufferRecordsWithoutErrors();

        Set<Id> accountIds = accountUpserted.keySet();
        Set<Id> businessUnitIds = new Set<Id>();
        Set<String> onlineShopIds = new Set<String>();

        Map<Id, List<Opportunity>> mainOpportunitiesByAccountIds = new Map<Id, List<Opportunity>>();

        //initialize map mainOpportunitiesByAccountIds
        for(Id accountId : accountIds){
            mainOpportunitiesByAccountIds.put(accountId, new List<Opportunity>());
        }

        for(CORE_LeadAcquisitionBuffer__c currentRecord : availableBufferRecord){
            Id accountId = this.bufferRecordByIds.get(currentRecord.Id).CORE_AccountId__c;
            Set<String> onlineShopIdsTmp = getOnlineShopIdsFromBuffer(currentRecord);

            if(!onlineShopIdsTmp.isEmpty()){
                onlineShopIds.addAll(onlineShopIdsTmp);
            }

            if(!String.isEmpty(currentRecord.CORE_MI_OPPTBusiness_Unit__c)){
                businessUnitIds.add(currentRecord.CORE_MI_OPPTBusiness_Unit__c);
            }
        }
        
        List<Opportunity> onlineShopOpportunities = CORE_OpportunityUtils.getExistingOnlineShopOpportunities(onlineShopIds);
        List<Opportunity> mainOpportunities = CORE_OpportunityUtils.getExistingMainOpportunitiesByBuIds(businessUnitIds, accountIds);

        Map<String,Opportunity> opportunnitiesByOnlineShopIds = new Map<String, Opportunity>();
        if(!onlineShopOpportunities.isEmpty()){
            for(Opportunity opportunity : onlineShopOpportunities){
                opportunnitiesByOnlineShopIds.put(opportunity.CORE_OPP_OnlineShop_Id__c, opportunity);
            }
        }

        Set<Id> mainOpportunitiesIds = new Set<Id>();

        for(Opportunity opportunity : mainOpportunities){
            Id opportunityId = opportunity.Id;

            if(!mainOpportunitiesIds.contains(opportunityId)){
                mainOpportunitiesByAccountIds.get(opportunity.AccountId).add(opportunity);
                //mainOpportunitiesIdsByAccountIds.get(opportunity.AccountId).add(opportunityId);
                mainOpportunitiesIds.add(opportunityId);
            }
        }

        Integer opportunityNumber = -1;
        Map<Id,Integer> oppItNumberById = new Map<Id,Integer>();
        Map<String, Integer> oppItNumberByExternalId = new Map<String, Integer> ();
        Set<String> existingOppKey = new Set<String>();
        Map<String,Integer> oppItNumberByUniqueKey = new Map<String,Integer>();

        Map<Id, CORE_LFA_OpportunityManagement.CatalogBuffer> catalogBuffers = CORE_LFA_OpportunityManagement.getCatalogHierarchiesFromBufferRecords(availableBufferRecord);
       
        for(CORE_LeadAcquisitionBuffer__c currentRecord : availableBufferRecord){
            Id accountId = this.bufferRecordByIds.get(currentRecord.Id).CORE_AccountId__c;
            Account currentAccount = accountUpserted.get(accountId);

            CORE_LeadFormAcquisitionWrapper.OpportunityWrapper currentInterest;
            //CORE_LeadFormAcquisitionWrapper.OpportunityWrapper secondInterest;
            //CORE_LeadFormAcquisitionWrapper.OpportunityWrapper thirdInterest;

            if(!String.isEmpty(currentRecord.CORE_MI_OPPTBusiness_Unit__c)){
                currentInterest = CORE_LFA_OpportunityManagement.getOpportunityWrapperFromBufferRecord(currentRecord,CORE_LFA_OpportunityManagement.OpportunityType.MAIN_INTEREST);
            } else {
                continue;
            }

            List<Opportunity> mainOpportunitiesIdsForThisRecord = mainOpportunitiesByAccountIds.get(currentAccount.Id);
            Campaign campaign;
            //mainInterst
            Integer interestNumber = 0;

            Opportunity opportunityToUpsert;
            CORE_LFA_OpportunityManagement.OpportunityType oppType;
            CORE_CatalogHierarchyModel catalogHierarchy = new CORE_CatalogHierarchyModel();

            oppType = CORE_LFA_OpportunityManagement.OpportunityType.MAIN_INTEREST;
            catalogHierarchy = catalogBuffers.get(currentRecord.Id).mainInterest;
            
            catalogHierarchy.isSuspect = currentInterest.isSuspect;
            //System.debug('catalogHierarchy init , currentRecord.Id =  ' + currentRecord.Id);
            //System.debug('catalogHierarchy init , currentInterest.businessUnit =  ' + currentInterest.businessUnit);
            //System.debug('catalogHierarchy init , catalogBuffers.get(currentRecord.Id) =  ' + catalogBuffers.get(currentRecord.Id));
            //System.debug('catalogHierarchy init , interestNumber =  ' + interestNumber);

            //catalogHierarchy = CORE_LFA_OpportunityManagement.getCatalogHierarchyFromBufferRecord(currentRecord,oppType);


            String finalAcademicYear = CORE_OpportunityUtils.getAcademicYear(
                dataMaker.getDefaultAcademicYear(),
                currentInterest.academicYear, 
                catalogHierarchy, 
                currentAccount.CreatedDate,
                this.availableAcademicYears
            );

            Opportunity mainOpportunity;
            catalogHierarchy.interest = currentInterest;
            if(!String.isBlank(currentInterest.onlineShopId)){
                mainOpportunity = opportunnitiesByOnlineShopIds.get(currentInterest.onlineShopId);
                opportunityToUpsert = this.dataMaker.initOnlineShopOpportunity(currentAccount, finalAcademicYear, catalogHierarchy, mainOpportunity);
                System.debug('opportunityToUpsert' + JSON.serialize(opportunityToUpsert));
            } 
            System.debug('d1');
            if(mainOpportunity == null){
                List<Opportunity> mainOppsForThisBu = CORE_LFA_OpportunityManagement.getMainOpportunityFromBusinessUnit(currentInterest.businessUnit, mainOpportunitiesIdsForThisRecord);
                System.debug('finalAcademicYear = '+ finalAcademicYear);

                if(!mainOppsForThisBu.isEmpty()){
                    System.debug('!mainOppsForThisBu.isEmpty()');
                    System.debug('mainOppsForThisBu = '+ mainOppsForThisBu);

                    mainOpportunity = CORE_LFA_OpportunityManagement.getMainOpportunityFromAcademicYear(finalAcademicYear, mainOppsForThisBu);
                }
                if(mainOpportunity == null && !mainOppsForThisBu.isEmpty()){
                    mainOpportunity = mainOppsForThisBu.get(0);
                }
                System.debug('d2');
                opportunityToUpsert = this.dataMaker.initOnlineShopOpportunity(currentAccount, finalAcademicYear, catalogHierarchy, mainOpportunity);
            }

            if(opportunityToUpsert.Id != null){
                Task task = CORE_OpportunityUtils.getReturningTask(catalogHierarchy, currentRecord.CORE_Comments__c, opportunityToUpsert, currentAccount);
                TaskBuffer taskBuffer = new taskBuffer();
                taskBuffer.catalogHierarchy = catalogHierarchy;
                taskBuffer.oppType = oppType;
                taskBuffer.bufferId = currentRecord.Id;
                taskBuffer.task = task;
                system.debug('taskBuffer.catalogHierarchy = ' + taskBuffer.catalogHierarchy);
                system.debug('taskBuffer.oppType = ' + taskBuffer.oppType);
                system.debug('taskBuffer.task = ' + taskBuffer.task);
                system.debug('taskBuffer.bufferId = ' + currentRecord.Id);
                this.taskToCreateByBuffer.add(taskBuffer);
            } else {
                opportunityToUpsert.CORE_Lead_Source_External_Id__c = currentInterest.leadSourceExternalId;
                opportunityToUpsert.CORE_Brochure_shipped_by_mail__c = currentInterest.brochureShippedByMail;
                opportunityToUpsert.CORE_Application_form_shipped_by_mail__c = currentInterest.applicationFormShippedByMail;
                opportunityToUpsert.CORE_Questions__c = CORE_OpportunityUtils.getShortQuestionForOpportunity(currentRecord.CORE_Comments__c);
                opportunityToUpsert.CORE_OPP_Learning_Material__c = currentInterest.learningMaterial;
                opportunityToUpsert.CORE_OPP_Application_Online_ID__c = currentInterest.applicationOnlineId;
                opportunityToUpsert.CORE_OPP_Funding__c = currentInterest.funding;
            }
            
            String oppUniqueKey = getOppUniqueKey(currentAccount.Id,catalogHierarchy.businessUnit.Id, finalAcademicYear);

            String externalIdTmp = opportunityToUpsert.CORE_Lead_Source_External_Id__c;
            System.debug('1-opportunityToUpsert.Id = '+ opportunityToUpsert.Id);
            System.debug('1-opportunityToUpsert.externalIdTmp = '+ externalIdTmp);
            System.debug('1-existingOpportunitiesExternal = '+ existingOpportunitiesExternal);
            System.debug('1-existingOpportunities = '+ existingOpportunities);
            System.debug('1-existingOppKey = '+ existingOppKey);

            if(opportunitiesToUpsert.isEmpty()){
                opportunitiesToUpsert.add(opportunityToUpsert);
                System.debug('addOpp0');
                opportunityNumber++;
            } else if(String.IsEmpty(opportunityToUpsert.Id) || !existingOpportunities.contains(opportunityToUpsert.Id)){
                boolean addOpp = false;
                if(!existingOppKey.contains(oppUniqueKey)){
                    System.debug('addOpp1');
                    addOpp = true;
                } else if(externalIdTmp != null){
                    if(!existingOppKey.contains(oppUniqueKey) && !existingOpportunitiesExternal.contains(externalIdTmp)){
                        addOpp = true;
                        System.debug('addOpp2');
                    }
                }
                //System.debug('opp wil be upsert : oppUniqueKey = ' + oppUniqueKey);
                if(addOpp){
                    opportunitiesToUpsert.add(opportunityToUpsert);
                    opportunityNumber++;
                }
            }

            existingOppKey.add(oppUniqueKey);
            
            if(externalIdTmp != null){
                existingOpportunitiesExternal.add(externalIdTmp);
            }
            
            
            Integer oppNumberTmp = opportunityNumber;
            System.debug( 'oppNumberTmp = '+ oppNumberTmp);
            Map<Id,Set<CORE_LFA_OpportunityManagement.OpportunityType>> oppTypeByBufferId = new Map<Id,Set<CORE_LFA_OpportunityManagement.OpportunityType>>();

            //existingOpportunitiesExternal
            if(opportunityToUpsert.Id != null){
                existingOpportunities.add(opportunityToUpsert.Id);

                if(!oppItNumberById.containsKey(opportunityToUpsert.Id)){
                    oppItNumberById.put(opportunityToUpsert.Id, oppNumberTmp);
                } else {
                    oppNumberTmp = oppItNumberById.get(opportunityToUpsert.Id);

                    oppTypeByBufferId = bufferRecordsByItOpp.get(oppNumberTmp);
                }
                if(!oppItNumberByUniqueKey.containsKey(oppUniqueKey)){
                    oppItNumberByUniqueKey.put(oppUniqueKey, oppNumberTmp);
                }
            } else if (existingOppKey.contains(oppUniqueKey) || opportunityToUpsert.CORE_Lead_Source_External_Id__c != null){
                System.debug( 'byExternalId or uniqueKey');

                if(!this.oppotunitiesCreatedAsReturning.containsKey(oppUniqueKey)){
                    this.oppotunitiesCreatedAsReturning.put(oppUniqueKey,0);
                }

                if(!oppItNumberByUniqueKey.containsKey(oppUniqueKey)){
                    oppItNumberByUniqueKey.put(oppUniqueKey, oppNumberTmp);
                    System.debug( 'Ajout de la uniqueKey : ' + oppUniqueKey);

                } else {
                    System.debug( 'uniqueKey existante : ' + oppUniqueKey);

                    oppNumberTmp = oppItNumberByUniqueKey.get(oppUniqueKey);
                    this.dataMaker.setReturningLead(opportunitiesToUpsert.get(oppNumberTmp));

                    this.oppotunitiesCreatedAsReturning.put(oppUniqueKey,oppotunitiesCreatedAsReturning.get(oppUniqueKey)+1);
                    if(!this.catalogForOppotunitiesCreatedAsReturning.containsKey(oppUniqueKey)){
                        this.catalogForOppotunitiesCreatedAsReturning.put(oppUniqueKey,new Map<Id,CORE_CatalogHierarchyModel>());
                    }
                    this.catalogForOppotunitiesCreatedAsReturning.get(oppUniqueKey).put(currentRecord.Id,catalogHierarchy);
                    System.debug( 'this.catalogForOppotunitiesCreatedAsReturning.get(oppUniqueKey) : ' + this.catalogForOppotunitiesCreatedAsReturning.get(oppUniqueKey));

                    oppTypeByBufferId = bufferRecordsByItOpp.get(oppNumberTmp);
                }

                if(opportunityToUpsert.CORE_Lead_Source_External_Id__c != null){
                    System.debug( 'opportunityToUpsert.CORE_Lead_Source_External_Id__c != null');

                    if(!oppItNumberByExternalId.containsKey(opportunityToUpsert.CORE_Lead_Source_External_Id__c)){
                        oppItNumberByExternalId.put(opportunityToUpsert.CORE_Lead_Source_External_Id__c, oppNumberTmp);
                    } else {
                        oppNumberTmp = oppItNumberByExternalId.get(opportunityToUpsert.CORE_Lead_Source_External_Id__c);
                        this.dataMaker.setReturningLead(opportunitiesToUpsert.get(oppNumberTmp));
                        oppTypeByBufferId = bufferRecordsByItOpp.get(oppNumberTmp);
                    }
                }
            }
            System.debug('oppNumberTmp afterproceess = ' + oppNumberTmp);
            System.debug('oppTypeByBufferId before = ' + oppTypeByBufferId);

            //used to match opp ids with buffer records
            if(!oppTypeByBufferId.containsKey(currentRecord.Id)){
                oppTypeByBufferId.put(currentRecord.Id,new Set<CORE_LFA_OpportunityManagement.OpportunityType>{oppType});
            } else {
                oppTypeByBufferId.get(currentRecord.Id).add(oppType);
                
            }
            
            System.debug('currentInterest.isFullHierarchy = ' + catalogHierarchy.isFullHierarchy);
            System.debug('currentInterest.product?.Id = ' + catalogHierarchy.product?.Id);
            System.debug('opportunityToUpsert.stagename = ' + opportunityToUpsert.stagename);
            System.debug('opportunityToUpsert.CORE_OPP_Sub_Status__c = ' + opportunityToUpsert.CORE_OPP_Sub_Status__c);
            System.debug('opportunityToUpsert.AccountId = ' + opportunityToUpsert.AccountId);
            System.debug('opportunityToUpsert.Id = ' + opportunityToUpsert.Id);
            System.debug('oppNumberTmp = ' + oppNumberTmp);
            System.debug('oppTypeByBufferId = ' + oppTypeByBufferId);
            System.debug('oppcurrentRecordId = ' + currentRecord.Id);
            System.debug('oppUniqueKey = ' + oppUniqueKey);
            System.debug('bufferRecordsByItOpp = ' + bufferRecordsByItOpp);

            bufferRecordsByItOpp.put(oppNumberTmp, oppTypeByBufferId);
        }
        
        try{
            System.debug('CORE_AsyncLFAWorker.execute() : Upsert '+ opportunitiesToUpsert.size() + ' opportunities');
            System.debug('bufferRecordsByItOpp = ' + bufferRecordsByItOpp);

            if(!opportunitiesToUpsert.isEmpty()){
                List<Database.UpsertResult> opportunitiesUpsertResult = Database.upsert(opportunitiesToUpsert, Opportunity.Fields.Id, this.dmlOptions.OptAllOrNone);
                updateBufferAfterOpportunityProcess(opportunitiesUpsertResult, opportunitiesToUpsert, bufferRecordsByItOpp);
            }
        
        } catch (Exception ex){
            Database.rollback(sp);
            throw ex;
        }
    }

    public override void executeOpportunityLineItemProcess(List<Opportunity> oppsUpserted, Savepoint sp){
        Map<Id,CORE_LeadAcquisitionBuffer__c> availableBufferRecord = new Map<Id,CORE_LeadAcquisitionBuffer__c> (getBufferRecordsWithoutErrors());

        List<OpportunityLineItem> oppLineItems = new List<OpportunityLineItem>();
        Map<Id,Id> buByProducts = new Map<Id,Id>();
        Map<Id,Id> productsByOpportunities = new Map<Id,Id>();
        Map<Id,Opportunity> oppsMap = new Map<ID, Opportunity>(oppsUpserted);

        for(Integer i = 0 ; i < oppsUpserted.size(); i++){
            Opportunity opportunity = oppsUpserted.get(i);

            if(!String.IsBlank(opportunity.CORE_Main_Product_Interest__c) && !String.IsBlank(opportunity.CORE_OPP_Business_Unit__c)){
                buByProducts.put(opportunity.CORE_Main_Product_Interest__c,opportunity.CORE_OPP_Business_Unit__c);
                productsByOpportunities.put(opportunity.Id, opportunity.CORE_Main_Product_Interest__c);
            }
        }
        if(productsByOpportunities.keySet().size() == 0){
            return;
        }

        Map<Id,PricebookEntry> pricebookEntries = this.dataMaker.getPricebookEntryByProducts(buByProducts);
        System.debug('pricebookEntries = '+pricebookEntries);
        Set<String> oppAlreadyTreated = new Set<String>();

        for(Id opportunityId : productsByOpportunities.keySet()){
            Opportunity currentOpportunity = oppsMap.get(opportunityId);
            String oppUniqueKey = getOppUniqueKey(currentOpportunity.AccountId,currentOpportunity.CORE_OPP_Business_Unit__c, currentOpportunity.CORE_OPP_Academic_Year__c);

            if(currentOpportunity.CORE_ReturningLead__c == null || currentOpportunity.CORE_ReturningLead__c == true){
                if(!this.oppotunitiesCreatedAsReturning.containsKey(oppUniqueKey)){
                    continue;
                } else if(this.oppotunitiesCreatedAsReturning.containsKey(oppUniqueKey) 
                && this.oppotunitiesCreatedAsReturning.get(oppUniqueKey) > 0 
                && oppAlreadyTreated.contains(oppUniqueKey)){
                    continue;
                }
            }

            oppAlreadyTreated.add(oppUniqueKey);
            
            PricebookEntry pricebookEntry = pricebookEntries.get(productsByOpportunities.get(opportunityId));
            System.debug('pricebookEntry = ' + pricebookEntry);
            if(pricebookEntry != NULL){
                OpportunityLineItem oppLineItem = this.dataMaker.initOppLineItem(oppsMap.get(opportunityId), pricebookEntry);
                System.debug('this.bufferIdsByOpportunityIds.get(opportunityId) = ' + this.bufferIdsByOpportunityIds.get(opportunityId));

                for(Id bufferId : this.bufferIdsByOpportunityIds.get(opportunityId)){
                    CORE_LeadAcquisitionBuffer__c currentAvailableBuffer = availableBufferRecord.get(bufferId);
                    CORE_LeadAcquisitionBuffer__c currentBufferUpdated = this.bufferRecordByIds.get(bufferId);
                    System.debug('currentBuffer.CORE_MainInterestId__c = ' + currentBufferUpdated.CORE_MainInterestId__c);
                    if(currentBufferUpdated.CORE_MainInterestId__c == opportunityId){
                        oppLineItem.Quantity = currentAvailableBuffer.CORE_MI_Quantity__c != null ? currentAvailableBuffer.CORE_MI_Quantity__c : oppLineItem.Quantity;
                        oppLineItem.UnitPrice = currentAvailableBuffer.CORE_MI_Sales_Price__c != null ? currentAvailableBuffer.CORE_MI_Sales_Price__c : oppLineItem.UnitPrice;
                        break;
                    } else {
                        continue;
                    }
                }
                oppLineItems.add(oppLineItem);
            }
        }

        try{
            System.debug('CORE_AsyncLFAWorker.execute() : Upsert opportunities');
            if(!oppLineItems.isEmpty()){
                List<Database.SaveResult> opportunityLineItemsUpsertResult = Database.insert(oppLineItems, this.dmlOptions.OptAllOrNone);
            }
        
        } catch (Exception ex){
            Database.rollback(sp);
            throw ex;
        }
    }

    public override void executeCampaignProcess(List<Opportunity> oppsUpserted, Savepoint sp){
        //nothing to do for onlineShop
    }

    private Set<String> getOnlineShopIdsFromBuffer(CORE_LeadAcquisitionBuffer__c buffer){
        Set<String> onlineShopIds = new Set<String>();

        if(!String.isEmpty(buffer.CORE_MI_OnlineShop_ExternalId__c)){
            onlineShopIds.add(buffer.CORE_MI_OnlineShop_ExternalId__c);
        }
        if(!String.isEmpty(buffer.CORE_SI_OnlineShop_ExternalId__c)){
            onlineShopIds.add(buffer.CORE_SI_OnlineShop_ExternalId__c);
        }
        if(!String.isEmpty(buffer.CORE_TI_OnlineShop_ExternalId__c)){
            onlineShopIds.add(buffer.CORE_TI_OnlineShop_ExternalId__c);
        }

        return onlineShopIds;
    }

    public override void updateBufferAfterOpportunityProcess(List<Database.UpsertResult> opportunitiesUpsertResults, List<Opportunity> opportunitiesToUpsert, Map<Integer, Map<Id,Set<CORE_LFA_OpportunityManagement.OpportunityType>>> bufferRecordsByItOpp){   
        for(Integer i = 0 ; i < opportunitiesUpsertResults.size() ; i++){
            Database.UpsertResult sr = opportunitiesUpsertResults.get(i);
            String errorCode;
            String description;
            String statusCode;
            
            Set<Id> bufferIds = new Set<Id>();
            Map<Id,Set<CORE_LFA_OpportunityManagement.OpportunityType>> bufferByTypes = bufferRecordsByItOpp.get(i);
            System.debug('updateBufferAfterOpportunityProcess i = '+ i);
            bufferIds.addAll(bufferByTypes.keySet());
            
            //CORE_LeadAcquisitionBuffer__c bufferToUpdate = this.bufferRecordByIds.get(bufferId);
            Id opportunityId;
            if (sr.isSuccess()) {
                // Operation was successful, so get the ID of the record that was processed
                //System.debug('Successfully inserted Opportunity. Opportunity ID: ' + sr.getId());
                
                //bufferToUpdate.CORE_AccountId__c = sr.getId();
                opportunityId = sr.getId();

                
                Opportunity opp = opportunitiesToUpsert.get(i);
                if(opp.CORE_Main_Product_Interest__c == null){
                    this.buffersInError.addAll(bufferIds);
                    statusCode = ERROR_STATUS;
                    description = 'Opportunity created without full interest';
                }
                opp.Id = opportunityId;
                //store the upserted account
            }
            else {
                opportunityId = opportunitiesToUpsert.get(i).Id;
                errorCode = '';
                description = '';
                statusCode = ERROR_STATUS;
                // Operation failed, so get all errors                
                for(Database.Error err : sr.getErrors()) {
                    System.debug('The following error has occurred on accountID = ' + sr.getId());                    
                    System.debug(err.getStatusCode() + ': ' + err.getMessage());
                    System.debug('Account fields that affected this error: ' + err.getFields());
                    errorCode += (String.isBlank(errorCode) ? '' : ' - ') + err.getStatusCode();
                    description += (String.isBlank(description) ? '' : ' \n ') + err.getMessage() + '. Account fields that affected this error: ' + err.getFields();

                }

                this.buffersInError.addAll(bufferIds);
            }
            
            System.debug('@updateBufferAfterOpportunityProcess > bufferIds = '+ bufferIds);
            for(Id bufferId : bufferIds){
                CORE_LeadAcquisitionBuffer__c bufferToUpdate = this.bufferRecordByIds.get(bufferId);
                Set<CORE_LFA_OpportunityManagement.OpportunityType> types = bufferByTypes.get(bufferId);
            	System.debug('@updateBufferAfterOpportunityProcess > currentBufferId = '+ bufferId);
            	System.debug('@updateBufferAfterOpportunityProcess > currentBufferId = '+ types);

                for(CORE_LFA_OpportunityManagement.OpportunityType type : types){
                    switch on type {
                        when MAIN_INTEREST {
                            bufferToUpdate.CORE_MainInterestId__c = opportunityId;
                        }
                        when SECOND_INTEREST {
                            bufferToUpdate.CORE_SecondInterestId__c = opportunityId;
                        }
                        when THIRD_INTEREST {
                            bufferToUpdate.CORE_ThirdInterestId__c = opportunityId;
                        }
                    }
                }
                
                if(statusCode != null){
                    bufferToUpdate.CORE_Status__c = statusCode;
                }
                //bufferToUpdate.CORE_ErrorCode__c = errorCode;
                if(!String.IsEmpty(description)){
                    bufferToUpdate.CORE_ErrorDescription__c += description;
                }
                this.bufferRecordByIds.put(bufferId, bufferToUpdate);

                if(!String.IsEmpty(opportunityId)){
                    if(!this.bufferIdsByOpportunityIds.containsKey(opportunityId)){
                        this.bufferIdsByOpportunityIds.put(opportunityId, new Set<Id> {bufferId});
                    } else {
                        this.bufferIdsByOpportunityIds.get(opportunityId).add(bufferId);
                    }
                    this.opportunityUpserted.add(opportunityId);
                }
            }
        }
    }
}