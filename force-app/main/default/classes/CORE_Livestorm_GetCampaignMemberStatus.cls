public class CORE_Livestorm_GetCampaignMemberStatus  implements Database.Batchable<SObject>, Database.AllowsCallouts {
    Set<Id> campaignIds;

    public CORE_Livestorm_GetCampaignMemberStatus(Set<Id> c){
        System.debug('CORE_Livestorm_GetCampaignMemberStatus : START');
        campaignIds = c;
    }

    public Database.QueryLocator start(Database.BatchableContext bc){
        return Database.getQueryLocator([SELECT Id, CORE_Livestorm_Session_Id__c, CORE_Business_Unit__c
        FROM Campaign where Id in :this.campaignIds]);
    }

    public void execute(Database.BatchableContext bc, List<Campaign> campaigns){
        System.debug('CORE_Livestorm_GetCampaignMemberStatus.execute() : START');

        Set<String> membersId = new Set<String>();
        Set<String> membersIdAttended = new Set<String>();
        Set<String> membersIdNotAttended = new Set<String>();

        Map<Id,CORE_Business_Unit__c> orderBusinessUnit = CORE_Livestorm_Helper.getBuFromCampaign(campaigns);
        
        for(Campaign campaign : campaigns){
            System.debug('Retreive campaign member status for : ' + campaign.CORE_Livestorm_Session_Id__c);
            CORE_Livestorm__mdt livestormMetadata = CORE_Livestorm_Helper.GetMetadata(orderBusinessUnit, campaign);

            if (livestormMetadata.CORE_Is_Livestorm_Active__c){
                try{
                    HttpMemberStatusResult result = getCampaignMemberDetails(campaign.CORE_Livestorm_Session_Id__c, livestormMetadata);

                    if (!result.IsAnErrorOccured){
                        for(Data data : result.People.data){
                            membersId.add(data.id);
                            if(data.attributes.registrant_detail.attended){
                                membersIdAttended.add(data.id);
                            }
                            else{
                                membersIdNotAttended.add(data.id);
                            }
                        }
                    }
                    else {
                        System.debug('An exception has occurred during people get status');
                    }
                    
                }
                catch(Exception e){
                    System.debug('The following exception has occurred during people get status : ' + e.getMessage());
                }
            }
        }

        System.debug('Total member covered : ' + membersId.size());
        System.debug('Member attended : ' + membersIdAttended.size());
        System.debug('Member not attended : ' + membersIdNotAttended.size());

        if (membersId.size() > 0){
            List<CampaignMember> campaignMembers = [SELECT Id, CORE_Livestorm_People_Id__c, Status FROM CAMPAIGNMEMBER WHERE CORE_Livestorm_People_Id__c in :membersId];
            System.debug('Member retreive : ' + campaignMembers.size());
            List<CampaignMember> campaignMembersToUpdate = new List<CampaignMember>();

            for (CampaignMember campaignMember : campaignMembers){
                if(membersIdAttended.contains(campaignMember.CORE_Livestorm_People_Id__c)){
                    CampaignMember newcm = new CampaignMember();
                    newcm.Id = campaignMember.id;
                    newcm.Status = 'Attended';
                    newcm.CORE_Livestorm_People_Status_Get__c = true;

                    campaignMembersToUpdate.add(newcm);
                }
                else if (membersIdNotAttended.contains(campaignMember.CORE_Livestorm_People_Id__c)){
                    CampaignMember newcm = new CampaignMember();
                    newcm.Id = campaignMember.id;
                    newcm.Status = 'No Show';
                    newcm.CORE_Livestorm_People_Status_Get__c = true;

                    campaignMembersToUpdate.add(newcm);
                }
            }

            List<Database.saveResult> results = Database.update(campaignMembersToUpdate, false);
        
            for(Database.saveResult result : results){
                if(!result.isSuccess()){
                System.debug(JSON.serialize(result.getErrors()));
                }
            }
        }

        System.debug('CORE_Livestorm_CreateCampaign.execute() : END');
    }    

    public class People{
        public Data[] data {get;set;}
        public Meta meta{get;set;}
    }

    public class Meta{
        public Integer record_count {get;set;}
    }

    public class Data{
        public string id {get;set;}
        public Attributes attributes {get;set;}
    }

    public class Attributes{
        public Registrant_Detail registrant_detail {get;set;}
    }

    public class Registrant_Detail{
        public Boolean attended {get;set;}
    }

    public class HttpMemberStatusResult{
        Boolean IsAnErrorOccured{get;set;}
        People People {get;set;}
    }

    public HttpMemberStatusResult getCampaignMemberDetails(String sessionId, CORE_Livestorm__mdt livestormMetadata){
        Http http = new Http();
        HttpRequest request = new HttpRequest();

        string fullEndPoint = livestormMetadata.CORE_Default_End_Point__c + livestormMetadata.CORE_Get_People_Status_End_Point__c;
        fullEndPoint = String.format(fullEndPoint, new List<String>{sessionId});
        
        request.setMethod('GET');
        request.setEndpoint(fullEndPoint);
        request.setHeader('Accept', 'application/vnd.api+json');
        request.setHeader('Authorization', livestormMetadata.CORE_Authorization__c);
        
        Long dt1 = DateTime.now().getTime();

        HttpResponse response = http.send(request);

        long dt2 = DateTime.now().getTime(); 
        System.debug('http execution time = ' + (dt2-dt1) + ' ms');

        HttpMemberStatusResult result = new HttpMemberStatusResult();

        if(response.getStatusCode() != 200) {
            System.debug('Event :The status code returned was not expected: ' + response.getStatusCode() + ' ' + response.getStatus());
            result.IsAnErrorOccured = true;
        } else {
            System.debug(response.getBody());
            People peoples = (People) JSON.deserialize(response.getBody(), People.class);
            System.debug(peoples);

            result.IsAnErrorOccured = false;
            result.People = peoples;
        }

        return result;
    }

    public void finish(Database.BatchableContext bc){
        System.debug('CORE_Livestorm_GetCampaignMemberStatus END.');
    }
}