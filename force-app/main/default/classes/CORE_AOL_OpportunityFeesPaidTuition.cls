@RestResource(urlMapping='/aol/opportunity/fees-paid/tuition')
global  class CORE_AOL_OpportunityFeesPaidTuition {
    private static final String SUCCESS_MESSAGE = 'Opportunity update with new fees paid tuition';

    @HttpPut
    global static CORE_AOL_Wrapper.ResultWrapperGlobal execute() {
        RestRequest	request = RestContext.request;

        // Initiating logs
        Long dt1 = DateTime.now().getTime();
        Long dt2;
        CORE_LogManager logSettings;
        CORE_LOG__c log = new CORE_LOG__c(CORE_API_Name__c = 'AOL');
        Boolean isErrorLog = false;
        logSettings = new CORE_LogManager('AOL');

        Map<String, Object> paramsMap = (Map<String, Object>)JSON.deserializeUntyped(request.requestBody.toString());
        String aolExternalId = (String)paramsMap.get(CORE_AOL_Constants.PARAM_AOL_EXTERNALID);
        CORE_AOL_Wrapper.TuitionFeesPaid tuitionFeesPaid = (CORE_AOL_Wrapper.TuitionFeesPaid)JSON.deserialize((String)JSON.serialize(paramsMap.get(CORE_AOL_Constants.PARAM_AOL_TUITION_FEES_PAID)), CORE_AOL_Wrapper.TuitionFeesPaid.class);
        CORE_AOL_Wrapper.AOLProgressionFields aolProgressionFields = (CORE_AOL_Wrapper.AOLProgressionFields)JSON.deserialize((String)JSON.serialize(paramsMap.get(CORE_AOL_Constants.PARAM_AOL_PROG_FIELDS)), CORE_AOL_Wrapper.AOLProgressionFields.class);

        CORE_AOL_Wrapper.ResultWrapperGlobal result = checkMandatoryParameters(aolExternalId);

        try{
            if(result != null){
                return result;
            }
            CORE_AOL_GlobalWorker aolWorker = new CORE_AOL_GlobalWorker(aolProgressionFields,aolExternalId);
            Opportunity opportunity = aolWorker.aolOpportunity;
            if(opportunity == null){
                result = new CORE_AOL_Wrapper.ResultWrapperGlobal(
                    CORE_AOL_Constants.ERROR_STATUS, 
                    String.format(CORE_AOL_Constants.MSG_OPPORTUNITY_NOT_FOUND, new List<String>{aolExternalId})
                );
            } else {
                opportunity = opportunityFeesPaidTuitionProcess(aolWorker,tuitionFeesPaid);

                //set Additional fields to update
                CORE_AOL_GlobalWorker.setAdditionalFields(aolWorker.aolOpportunity, request.requestBody.toString());

                aolWorker.updateOppIsNeeded = true;
                aolWorker.updateAolProgressionFields();

                result = new CORE_AOL_Wrapper.ResultWrapperGlobal(CORE_AOL_Constants.SUCCESS_STATUS, SUCCESS_MESSAGE);
            }
            
        } catch(Exception e){
            result = new CORE_AOL_Wrapper.ResultWrapperGlobal(CORE_AOL_Constants.ERROR_STATUS, e.getMessage() + ' at ' + e.getStackTraceString());
        } finally{
            dt2 = DateTime.now().getTime();
            System.debug('CORE_AOL_AcademicDiplomaHistory(urlMapping=\'/aol/academic-diploma-history\') : T0TAL TIME OF EXECUTION = ' + (dt2-dt1) + ' ms');
            // creating log
            log.CORE_Processing_Duration__c =(dt2-dt1);
            log.CORE_HTTP_Status_Code__c = result.status == 'KO' ? 400 : result.status == 'OK' ? 200 : 500;
            log.CORE_Sent_Body__c = JSON.serialize(result);
            log.CORE_Received_Body__c =  request.requestBody.toString();
            
            if(!logSettings.isNone()){
                if(logSettings.isAllLogged()){ // is All logged
                    CORE_LogManager.createLog(log);
                }else if(logSettings.isErrorLogged() && isErrorLog){ // is Error
                    CORE_LogManager.createLog(log);
                }
            }
        }

        return result;
    }

    public static Opportunity opportunityFeesPaidTuitionProcess(CORE_AOL_GlobalWorker aolWorker,
        CORE_AOL_Wrapper.TuitionFeesPaid tuitionFeesPaid){

        if(tuitionFeesPaid.received!=null){
            aolWorker.aolOpportunity.CORE_OPP_Tuition_Fee_Received__c = tuitionFeesPaid.received;
        }
        if(!String.IsBlank(tuitionFeesPaid.paymentMethod)){
            aolWorker.aolOpportunity.CORE_OPP_Tuition_Fee_Payment_Method__c = tuitionFeesPaid.paymentMethod;
        }
        if(tuitionFeesPaid.receivedDate!=null){
            aolWorker.aolOpportunity.CORE_OPP_Tuition_Fee_Received_Date__c = tuitionFeesPaid.receivedDate;
        }
        if(tuitionFeesPaid.acceptanceDate!=null){
            aolWorker.aolOpportunity.CORE_Tuition_Fee_Acceptance_Date__c = tuitionFeesPaid.acceptanceDate;
        }
        if(!String.IsBlank(tuitionFeesPaid.transactionId)){
            aolWorker.aolOpportunity.CORE_Tuition_fee_Transaction_ID__c = tuitionFeesPaid.transactionId;
        }
        if(!String.IsBlank(tuitionFeesPaid.bank)){
            aolWorker.aolOpportunity.CORE_Tuition_Fee_Bank__c = tuitionFeesPaid.bank;
        }
        if(!String.IsBlank(tuitionFeesPaid.sender)){
            aolWorker.aolOpportunity.CORE_Tuition_Fee_Sender__c = tuitionFeesPaid.sender;
        }
        if(tuitionFeesPaid.amount!=null){
            aolWorker.aolOpportunity.CORE_Tuition_fee_amount__c = tuitionFeesPaid.amount;
        }
        if(tuitionFeesPaid.paymentType!=null){
            aolWorker.aolOpportunity.CORE_OPP_Tuition_Fee_Payment_Type__c = tuitionFeesPaid.paymentType;
        }

        return aolWorker.aolOpportunity;
    }



    private static CORE_AOL_Wrapper.ResultWrapperGlobal checkMandatoryParameters(String aolExternalId){
        List<String> missingParameters = new List<String>();
        Boolean isMissingAolId = CORE_AOL_GlobalWorker.isAolExternalIdParameterMissing(aolExternalId);

        if(isMissingAolId){
            missingParameters.add(CORE_AOL_Constants.PARAM_AOL_EXTERNALID);
        }
        
        return CORE_AOL_GlobalWorker.getMissingParameters(missingParameters);
    }
}