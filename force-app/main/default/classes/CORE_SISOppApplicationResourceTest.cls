/**
 * Created By Fodil
 * Test class for CORE_SISOpportunityApplicationResource
 */

@isTest
public with sharing class CORE_SISOppApplicationResourceTest {
    @TestSetup
    static void makeData(){
        CORE_DataFaker_CatalogHierarchy catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test');
        catalogHierarchy.product.SIS_External_Id__c ='SIS-GGEIT-IM-MIL-Fashion-FD-MA1-2022-09';
        update catalogHierarchy.product;       
        Account account = CORE_DataFaker_Account.getStudentAccount('test');
        account.PersonEmail='test@email.com';
        account.Phone = '+33668888888';
        update account;

    }
    @IsTest
    static void postOpportunityApplicantSuccess(){
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();             
        req.requestURI = '/services/apexrest/opportunity/applicant/';
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response= res;
        Map<String,Object> reqBody = (Map<String,Object>) JSON.deserializeUntyped('{ "personAccount":{ "CORE_SISStudentID__c": "123","CORE_GovernmentStudentID__c": "",'+
        '"CORE_Social_Security_Number_Fiscal_Code__pc": "string","CORE_Gender__pc": "CORE_PKL_Female","Salutation": "Mr.", "Firstname": "Fodil New Test","Middlename": "Mid",'+
        '"CORE_Nickname__pc": "string","Lastname": "Plato", "CORE_Birthname__pc": "string","PersonBirthdate": "2022-04-05","CORE_Year_of_birth__pc": "1995", "CORE_Main_Nationality__pc": "ITA",'+
        '"CORE_Main_Passport_Number_ID_Number__c":"999999","CORE_Main_Passport_ID_Date_of_Expiration__c":"2022-12-19", "PersonEmail": "fodil.test+2@ggeedu.com","Phone": "+33777777770",'+
        '"CORE_Language__pc":"FRA", "CORE_Language_website__pc":"ITA","CORE_Foreign_language_1__c":"FRA","CORE_Foreign_language_1_Level__c":"DEU"},"mainInterest":{ "SIS_OpportunityId" :"00000030" ,"productExternalID":"SIS-GGEIT-IM-MIL-Fashion-FD-MA1-2022-09",'+
        '"academicYear": "CORE_PKL_2021-2022","division":"IM","businessUnit": "MD","studyArea": "FSA", "curriculum": "MAMFL", "level": "TCMA1FA", "intake": "SFA1"},'+
        '"opportunity":{"description" : "test description", "CORE_OPP_Credit_Transfer_Number__c":"23333"},'+
        '"acquisitionPointOfContact":{ "createdDate": 1621951200000,"sourceId": "IDAPITEST", "nature": "CORE_PKL_Online","captureChannel": "CORE_PKL_Phone_call", "origin": "TEST API", "campaignMedium": "CORE_PKL_Directory",'+
        '"campaignSource": "source campaign","campaignTerm": "campaign term","device": "CORE_PKL_Computer", "firstVisitDate": "2021-05-19", "firstPage": "https://www.mba-esg.com/master-production-audiovisuelle.html?gclid=CjwKCAjwwbHWBRBWEiwAMIV7E2dd4Vyedrh5-jC1xDkw5dacsDrjtlOuAzYlyFpxZICKfW98nzHG7BoC9PkQAvD_BwE",'+
        '"ipAddress": "192.168.1.1","latitute": "0.132400","longitude": "34.750000", "salesOrigin" : "testOriginSales" }}');
        req.requestBody = Blob.valueOf(JSON.serialize(reqBody));
        Test.startTest();
        CORE_SISOpportunityApplicationResource.postOpportunityApplicant();
        Test.stopTest();
        System.assertEquals(200,res.statusCode );
    }

    @IsTest
    static void postOpportunityApplicantFailure(){
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();             
        req.requestURI = '/services/apexrest/opportunity/applicant/';
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response= res;
        Map<String,Object> reqBody = (Map<String,Object>) JSON.deserializeUntyped('{ "personAccount":{ "CORE_SISStudentID__c": "123","CORE_GovernmentStudentID__c": "",'+
        '"CORE_Social_Security_Number_Fiscal_Code__pc": "string","CORE_Gender__pc": "CORE_PKL_Female","Salutation": "Mr.", "Firstname": "Fodil New Test","Middlename": "Mid",'+
        '"CORE_Nickname__pc": "string","Lastname": "Plato", "CORE_Birthname__pc": "string","PersonBirthdate": "2022-04-05","CORE_Year_of_birth__pc": "1995", "CORE_Main_Nationality__pc": "ITA",'+
        '"CORE_Main_Passport_Number_ID_Number__c":"999999","CORE_Main_Passport_ID_Date_of_Expiration__c":"2022-12-19", "PersonEmail": "fodil.test+2@ggeedu.com","Phone": "+33777777770",'+
        '"CORE_Language__pc":"FRA", "CORE_Language_website__pc":"ITA","CORE_Foreign_language_1__c":"FRA","CORE_Foreign_language_1_Level__c":"DEU"},"mainInterest":{ "SIS_OpportunityId" :"00000030" ,"productExternalID":"SIS-GGEIT-IM-MIL-Fashion-FD-MA1-2022-09",'+
        '"academicYear": "CORE_PKL_2021-2022","division":"IM","businessUnit": "MD","studyArea": "FSA", "curriculum": "MAMFL", "level": "TCMA1FA", "intake": "SFA1"},'+
        '"consents" : [{"CORE_Consent_type__c": "CORE_PKL_Processing_Collection_Storage_of_data_consent", "CORE_Consent_channel__c": "CORE_PKL_Not_applicable",'+
        ' "CORE_Consent_description__c":"short description","CORE_Opt_in__c": false,"CORE_Opt_in_date__c": 1621961240000,"CORE_Opt_out__c": true,"CORE_Opt_out_date__c": 1621951200000,"CORE_Business_Unit__c": "MD",'+
        '"CORE_Division__c": "IM]"}],"opportunity":{"description" : "test description", "CORE_OPP_Credit_Transfer_Number__c":"23333"},'+
        '"acquisitionPointOfContact":{ "createdDate": 1621951200000,"sourceId": "IDAPITEST", "nature": "CORE_PKL_Online","captureChannel": "CORE_PKL_Phone_call", "origin": "TEST API", "campaignMedium": "CORE_PKL_Directory",'+
        '"campaignSource": "source campaign","campaignTerm": "campaign term","device": "CORE_PKL_Computer", "firstVisitDate": "2021-05-19", "firstPage": "https://www.mba-esg.com/master-production-audiovisuelle.html?gclid=CjwKCAjwwbHWBRBWEiwAMIV7E2dd4Vyedrh5-jC1xDkw5dacsDrjtlOuAzYlyFpxZICKfW98nzHG7BoC9PkQAvD_BwE",'+
        '"ipAddress": "192.168.1.1","latitute": "0.132400","longitude": "34.750000", "salesOrigin" : "testOriginSales" }}');
        req.requestBody = Blob.valueOf(JSON.serialize(reqBody));
        Test.startTest();
        CORE_SISOpportunityApplicationResource.postOpportunityApplicant();
        Test.stopTest();
        System.assertNotEquals(200,res.statusCode );

    }

    
}