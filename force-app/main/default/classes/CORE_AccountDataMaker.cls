public class CORE_AccountDataMaker {
    @TestVisible
    private static final String STUDENT_RECORDTYPE_DEVNAME = 'Core_Student';
    @TestVisible
    public static final String ACADEMIC_DIPLOMA_RECORDTYPE_DEVNAME = 'Core_Academic_Diploma_History';

    public static CORE_Academic_Diploma_History__c initAcademicDiplomaHistoryFromAPI(Account account, CORE_LeadFormAcquisitionWrapper.AcademicDiplomaHistoryWrapper academicDiplomaHistoryWrapper){
        Id recordTypeId = Schema.SObjectType.CORE_Academic_Diploma_History__c.getRecordTypeInfosByDeveloperName().get(ACADEMIC_DIPLOMA_RECORDTYPE_DEVNAME).getRecordTypeId();
        CORE_Academic_Diploma_History__c academicDiplomaHistory = new CORE_Academic_Diploma_History__c(
            Name = academicDiplomaHistoryWrapper.name,
            CORE_Diploma_level__c = academicDiplomaHistoryWrapper.level,
            CORE_Diploma_date__c = academicDiplomaHistoryWrapper.diplomaDate,
            CORE_Person_Account_Id__c = account.Id,
            RecordTypeId = recordTypeId
        );

        return academicDiplomaHistory;
    }

    public static Account initPersonAccount(CORE_LeadFormAcquisitionWrapper.PersonAccountWrapper personAccountWrapper, Boolean countryPicklistIsActivated){
        System.debug('CORE_AccountDataMaker.initPersonAccount() : START');
        Long dt1 = DateTime.now().getTime();
        Long dt2;

        Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(STUDENT_RECORDTYPE_DEVNAME).getRecordTypeId();
        
        Account account = new Account(
            CreatedDate = CORE_DataConvertion.convertTimeStampToDateTime(personAccountWrapper.creationDate, false),
            Salutation = personAccountWrapper.salutation,
            CORE_Gender__pc = personAccountWrapper.gender,
            CORE_Professional_situation__pc = personAccountWrapper.professionalSituation,
            LastName = personAccountWrapper.lastName,
            FirstName = personAccountWrapper.firstName,
            Phone = personAccountWrapper.phone,
            PersonMobilePhone = personAccountWrapper.mobilePhone,
            PersonEmail = personAccountWrapper.emailAddress,
            PersonMailingStreet = personAccountWrapper.street,
            PersonMailingPostalCode = personAccountWrapper.postalcode,
            PersonMailingCity = personAccountWrapper.city,
            CORE_Language_website__pc = personAccountWrapper.languageWebsite,
            RecordTypeId = recordTypeId,
            OwnerId = UserInfo.getUserId(),
            CORE_GA_User_ID__c = personAccountWrapper.gaUserId,
            CORE_Main_Nationality__pc = personAccountWrapper.mainNationality,
            //SGS-1741
            PersonBirthdate = personAccountWrapper.birthDate,
            CORE_Year_of_birth__pc = personAccountWrapper.birthYear
        );

        if(countryPicklistIsActivated){
            account.PersonMailingCountryCode = personAccountWrapper.countryCode;
            account.PersonMailingStateCode = personAccountWrapper.stateCode;
        } else {
            account.PersonMailingCountry = personAccountWrapper.countryCode;
            account.PersonMailingState = personAccountWrapper.stateCode;
        }

        dt2 = DateTime.now().getTime();
        System.debug('initPersonAccount time execution = ' + (dt2-dt1) + ' ms');
        System.debug('CORE_AccountDataMaker.initPersonAccount() : END');

        return account;
    }
    
    //ici
    public static Account getUpdatedPersonAccount(Account existingAccount, CORE_LeadFormAcquisitionWrapper.PersonAccountWrapper personAccountWrapper, Boolean countryPicklistIsActivated){
        Account account = existingAccount;
        Boolean updateIfNotNull = personAccountWrapper.updateIfNotNull == null ? false : personAccountWrapper.updateIfNotNull;
        if(!String.isBlank(personAccountWrapper.salutation) && (String.isBlank(account.Salutation) || updateIfNotNull)){
            account.Salutation = personAccountWrapper.salutation;
        }
        if(!String.isBlank(personAccountWrapper.gender) && (String.isBlank(account.CORE_Gender__pc) || updateIfNotNull)){
            account.CORE_Gender__pc = personAccountWrapper.gender;
        }
        if(!String.isBlank(personAccountWrapper.professionalSituation) && (String.isBlank(account.CORE_Professional_situation__pc) || updateIfNotNull)){
            account.CORE_Professional_situation__pc = personAccountWrapper.professionalSituation;
        }
        if(!String.isBlank(personAccountWrapper.lastName) && (String.isBlank(account.LastName) || updateIfNotNull)){
            account.LastName = personAccountWrapper.lastName;
        }

        if(!String.isBlank(personAccountWrapper.firstName) && (String.isBlank(account.FirstName) || updateIfNotNull)){
            account.FirstName = personAccountWrapper.firstName;
        }
    
        if(!String.isBlank(personAccountWrapper.street) && (String.isBlank(account.PersonMailingStreet) || updateIfNotNull)){
            account.PersonMailingStreet = personAccountWrapper.street;
        }

        if(!String.isBlank(personAccountWrapper.postalcode) && (String.isBlank(account.PersonMailingPostalCode) || updateIfNotNull)){
            account.PersonMailingPostalCode = personAccountWrapper.postalcode;
        }

        if(!String.isBlank(personAccountWrapper.city) && (String.isBlank(account.PersonMailingCity) || updateIfNotNull)){
            account.PersonMailingCity = personAccountWrapper.city;
        }

        if(!String.isBlank(personAccountWrapper.languageWebsite) && (String.isBlank(account.CORE_Language_website__pc) || updateIfNotNull)){
            account.CORE_Language_website__pc = personAccountWrapper.languageWebsite;
        }

        if(!String.isBlank(personAccountWrapper.countryCode) && (String.isBlank(account.PersonMailingCountry) || updateIfNotNull)){
            if(countryPicklistIsActivated){
                account.PersonMailingCountryCode = personAccountWrapper.countryCode;
            } else {
                account.PersonMailingCountry = personAccountWrapper.countryCode;
            }
        }

        if(!String.isBlank(personAccountWrapper.stateCode) && (String.isBlank(account.PersonMailingState) || updateIfNotNull)){
            if(countryPicklistIsActivated){
                account.PersonMailingStateCode = personAccountWrapper.stateCode;
            } else {
                account.PersonMailingState = personAccountWrapper.stateCode;
            }
        }

        if(!String.isBlank(personAccountWrapper.gaUserId) && (String.isBlank(account.PersonMailingState) || updateIfNotNull)){
            account.CORE_GA_User_ID__c = personAccountWrapper.gaUserId;
        }
        if(!String.isBlank(personAccountWrapper.mainNationality) && (String.isBlank(account.CORE_Main_Nationality__pc) || updateIfNotNull)){
            account.CORE_Main_Nationality__pc = personAccountWrapper.mainNationality;
        }
        //SGS-1741
        if(personAccountWrapper.birthDate != null && (account.PersonBirthdate == null || updateIfNotNull)){
            account.PersonBirthdate = personAccountWrapper.birthDate;
        }
        //SGS-1741
        if(personAccountWrapper.birthYear != null && (account.CORE_Year_of_birth__pc == null || updateIfNotNull)){
            account.CORE_Year_of_birth__pc = personAccountWrapper.birthYear;
        }

        account = getAccountWithPhoneFields(account, personAccountWrapper.phone, false, updateIfNotNull);
        account = getAccountWithPhoneFields(account, personAccountWrapper.mobilePhone, true, updateIfNotNull);
        account = getAccountWithEmailFields(account, personAccountWrapper.emailAddress, updateIfNotNull);

        return account;
    }

    @TestVisible
    public static Account getAccountWithPhoneFields(Account account, String phoneNumber, boolean mobilePhone, boolean updateIfNotNull){
        Account acc = account;
        List<String> accountsPhones = new List<String>();
        if(!String.isBlank(acc.Phone)){
            accountsPhones.add(acc.Phone);
        }
        if(!String.isBlank(acc.CORE_Phone2__pc)){
            accountsPhones.add(acc.CORE_Phone2__pc);
        }
        if(!String.isBlank(acc.PersonMobilePhone)){
            accountsPhones.add(acc.PersonMobilePhone);
        }
        if(!String.isBlank(acc.CORE_Mobile_2__pc)){
            accountsPhones.add(acc.CORE_Mobile_2__pc);
        }
        if(!String.isBlank(acc.CORE_Mobile_3__pc)){
            accountsPhones.add(acc.CORE_Mobile_3__pc);
        }

        if(accountsPhones.contains(phoneNumber)){
            return acc;
        }

        if(!String.isBlank(phoneNumber)){
            if(String.isBlank(acc.Phone) && !mobilePhone){
                acc.Phone = phoneNumber;
            } else if((String.isBlank(acc.CORE_Phone2__pc) || updateIfNotNull) && !mobilePhone){
                acc.CORE_Phone2__pc = acc.Phone;
                acc.Phone = phoneNumber;
            } else if(String.isBlank(acc.PersonMobilePhone) && mobilePhone){
                acc.PersonMobilePhone = phoneNumber;
            } else if(String.isBlank(acc.CORE_Mobile_2__pc) && mobilePhone){
                acc.CORE_Mobile_2__pc = acc.PersonMobilePhone;
                acc.PersonMobilePhone = phoneNumber;
            } else if((String.isBlank(acc.CORE_Mobile_3__pc) || updateIfNotNull) && mobilePhone){
                acc.CORE_Mobile_3__pc = acc.CORE_Mobile_2__pc;
                acc.CORE_Mobile_2__pc = acc.PersonMobilePhone;
                acc.PersonMobilePhone = phoneNumber;
            }
        }

        return acc;
    }

    @TestVisible
    public static Account getAccountWithEmailFields(Account account, String emailAddress, boolean updateIfNotNull){
        Account acc = account;
        List<String> accountsEmails = new List<String>();
        String lowerEmail = String.isBlank(emailAddress) ? null : emailAddress.toLowercase();
        
        if(!String.isBlank(acc.PersonEmail)){
            accountsEmails.add(String.valueOf(acc.PersonEmail));
        }
        if(!String.isBlank(acc.CORE_Student_Email__pc)){
            accountsEmails.add(String.valueOf(acc.CORE_Student_Email__pc));
        }
        if(!String.isBlank(acc.CORE_Email_2__pc)){
            accountsEmails.add(String.valueOf(acc.CORE_Email_2__pc));
        }
        if(!String.isBlank(acc.CORE_Email_3__pc)){
            accountsEmails.add(String.valueOf(acc.CORE_Email_3__pc));
        }

        if(accountsEmails.contains(lowerEmail)){
            return acc;
        }

        if(!String.isBlank(emailAddress)){
            if(String.isBlank(acc.PersonEmail)){
                acc.PersonEmail = emailAddress;
            } else if(String.isBlank(acc.CORE_Email_2__pc)){
                acc.CORE_Email_2__pc = acc.PersonEmail;
                acc.PersonEmail = emailAddress;
            } else if((String.isBlank(acc.CORE_Email_3__pc) || updateIfNotNull)){
                acc.CORE_Email_3__pc = acc.CORE_Email_2__pc;
                acc.CORE_Email_2__pc = acc.PersonEmail;
                acc.PersonEmail = emailAddress;
            }
        }

        return acc;
    }
}