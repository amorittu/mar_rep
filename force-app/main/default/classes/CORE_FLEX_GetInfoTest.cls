@IsTest
public with sharing class CORE_FLEX_GetInfoTest {
    private static final String USERNAME_TEST = 'usertest@Flextest.com';
    private static final String API_PROFILE = 'CORE_API_Profile';
    private static final String FLEX_PERMISSION_SET = 'CORE_API_Twilio';
    private static final String OPP_STAGENAME_LEAD = 'Lead';
    private static final String OPP_SUBSTATUS_LEADNEW = 'Lead - New';
    private static final String ACC_PHONE_NUMBER = '+33647000000';
    private static final String ACC_PHONE_NUMBER2 = '+33600000000';
    private static final String BU_PHONE_NUMBER = '+33647000001';
    private static final String MISSING_PARAM_EXCEPTION_NAME = 'System.IllegalArgumentException';

    @TestSetup
    static void makeData(){
        Id profileId = [SELECT Id FROM Profile WHERE Name = :API_PROFILE LIMIT 1].Id;

        User usr = new User(
            Username = USERNAME_TEST,
            LastName = 'TEST_USER',
            Email = 'testemail@yopmail.com',
            Alias = 'T', 
            CommunityNickname = 'T',
            TimeZoneSidKey = 'Europe/Paris',
            LocaleSidKey = 'fr_FR_EURO',
            EmailEncodingKey = 'ISO-8859-1', 
            ProfileId = profileId,
            LanguageLocaleKey = 'fr'      
        );
        database.insert(usr, true);

        CORE_CatalogHierarchyModel catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('setup').catalogHierarchy;
        catalogHierarchy.businessUnit.CORE_Phone_numbers__c = BU_PHONE_NUMBER;

        update catalogHierarchy.businessUnit;

        Account account1 = CORE_DataFaker_Account.getStudentAccountWithoutInsert('setup1');
        account1.PersonMobilePhone = ACC_PHONE_NUMBER;

        Account account2 = CORE_DataFaker_Account.getStudentAccountWithoutInsert('setup2');
        account2.PersonMobilePhone = ACC_PHONE_NUMBER2;

        insert new List<Account>{account1, account2};

        Opportunity  opportunity = CORE_DataFaker_Opportunity.getOpportunity(
            catalogHierarchy.division.Id, 
            catalogHierarchy.businessUnit.Id,
            catalogHierarchy.studyArea.Id, 
            catalogHierarchy.curriculum.Id, 
            catalogHierarchy.level.Id,
            catalogHierarchy.intake.Id,
            account1.Id, 
            OPP_STAGENAME_LEAD, 
            OPP_SUBSTATUS_LEADNEW
        );
    }

    @IsTest
    private static void execute_should_return_an_error_if_parameters_are_missing(){
        User usr = [SELECT Id FROM User WHERE Username = :USERNAME_TEST LIMIT 1];

        List<PermissionSetAssignment> psaList = new List<PermissionSetAssignment>();

        for(PermissionSet ps : [SELECT Id FROM PermissionSet WHERE Name = :FLEX_PERMISSION_SET]){
            psaList.add(
                new PermissionSetAssignment(AssigneeId = usr.id, PermissionSetId = ps.Id)
            );
        }
        
        database.insert(psaList, true);
        
        System.runAs(usr){
            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
            req.requestURI = '/services/apexrest/twilio/CORE_FLEX_GetInfo';
            req.httpMethod = 'POST';

            RestContext.request = req;
            RestContext.response= res;
            
            Test.startTest();
            try{
                CORE_FLEX_GetInfo.execute(null);
            }  catch (Exception e){
                System.assertEquals(e.getTypeName(), MISSING_PARAM_EXCEPTION_NAME);
            }

            try{
                CORE_FLEX_GetInfo.execute(new List<CORE_FLEX_GetInfo.InputParams>());
            }  catch (Exception e){
                System.assertEquals(e.getTypeName(), 'System.IllegalArgumentException');
            }

            List<CORE_FLEX_GetInfo.InputParams> params = new List<CORE_FLEX_GetInfo.InputParams>();
            CORE_FLEX_GetInfo.InputParams param = new CORE_FLEX_GetInfo.InputParams();
            param.CalledPhone = null;
            param.CallerPhone = ACC_PHONE_NUMBER;
            param.InteractionType = 'test';

            params.add(param);

            try{
                CORE_FLEX_GetInfo.execute(params);
            }  catch (Exception e){
                System.assertEquals(e.getTypeName(), MISSING_PARAM_EXCEPTION_NAME);
            }

            param.CalledPhone = BU_PHONE_NUMBER;
            param.CallerPhone = null;
            try{
                CORE_FLEX_GetInfo.execute(params);
            }  catch (Exception e){
                System.assertEquals(e.getTypeName(), MISSING_PARAM_EXCEPTION_NAME);
            }

            param.CallerPhone = ACC_PHONE_NUMBER;
            param.InteractionType = ' ';

            try{
                CORE_FLEX_GetInfo.execute(params);
            }  catch (Exception e){
                System.assertEquals(e.getTypeName(), MISSING_PARAM_EXCEPTION_NAME);
            }

            param.CallerPhone = ACC_PHONE_NUMBER;
            param.InteractionType = null;

            Boolean exceptionCatched = false;
            try{
                CORE_FLEX_GetInfo.execute(params);
            }  catch (Exception e){
                exceptionCatched = true;
            }
            System.assertEquals(exceptionCatched, false);

            Test.stopTest();
        }
    }

    @IsTest
    private static void execute_should_return_an_account_if_found(){
        Account account = [SELECT Id FROM Account WHERE PersonMobilePhone = :ACC_PHONE_NUMBER2];

        User usr = [SELECT Id FROM User WHERE Username = :USERNAME_TEST LIMIT 1];

        List<PermissionSetAssignment> psaList = new List<PermissionSetAssignment>();

        for(PermissionSet ps : [SELECT Id FROM PermissionSet WHERE Name = :FLEX_PERMISSION_SET]){
            psaList.add(
                new PermissionSetAssignment(AssigneeId = usr.id, PermissionSetId = ps.Id)
            );
        }
        
        database.insert(psaList, true);
        
        System.runAs(usr){
            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
            req.requestURI = '/services/apexrest/twilio/CORE_FLEX_GetInfo';
            req.httpMethod = 'POST';

            RestContext.request = req;
            RestContext.response= res;
            
            List<CORE_FLEX_GetInfo.InputParams> params = new List<CORE_FLEX_GetInfo.InputParams>();
            CORE_FLEX_GetInfo.InputParams param = new CORE_FLEX_GetInfo.InputParams();
            param.CalledPhone = ACC_PHONE_NUMBER2;
            param.CallerPhone = ACC_PHONE_NUMBER2;
            param.InteractionType = 'test';
            params.add(param);
            
            Test.startTest();
            List<CORE_FLEX_GetInfo.SuccessResult> results = CORE_FLEX_GetInfo.execute(params);
                
            System.assertEquals(false, results.isEmpty());
            CORE_FLEX_GetInfo.SuccessResult result = results.get(0);

            System.assertEquals('CORE_FLEX_GetInfo', result.actionName);
            System.assertEquals(true, result.isSuccess);
            System.assertNotEquals(null, result.outputValues.Account);
            System.assertEquals(account.Id, result.outputValues.Account.Id);
            System.assertEquals(null, result.outputValues.Username);
            System.assertEquals(null, result.outputValues.BusinessUnit);
            System.assertEquals(null, result.outputValues.UserId);
            System.assertEquals(null, result.outputValues.Opportunity);
            System.assertEquals(null, result.outputValues.UserEmail);
            Test.stopTest();
        }
    }

    @IsTest
    private static void execute_should_return_a_BusinessUnit_if_found(){
        
        CORE_Business_Unit__c bu = [SELECT Id FROM CORE_Business_Unit__c LIMIT 1];

        User usr = [SELECT Id FROM User WHERE Username = :USERNAME_TEST LIMIT 1];

        List<PermissionSetAssignment> psaList = new List<PermissionSetAssignment>();

        for(PermissionSet ps : [SELECT Id FROM PermissionSet WHERE Name = :FLEX_PERMISSION_SET]){
            psaList.add(
                new PermissionSetAssignment(AssigneeId = usr.id, PermissionSetId = ps.Id)
            );
        }
        
        database.insert(psaList, true);
        
        System.runAs(usr){
            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
            req.requestURI = '/services/apexrest/twilio/CORE_FLEX_GetInfo';
            req.httpMethod = 'POST';

            RestContext.request = req;
            RestContext.response= res;
            
            List<CORE_FLEX_GetInfo.InputParams> params = new List<CORE_FLEX_GetInfo.InputParams>();
            CORE_FLEX_GetInfo.InputParams param = new CORE_FLEX_GetInfo.InputParams();
            param.CalledPhone = BU_PHONE_NUMBER;
            param.CallerPhone = '+33699999999';
            param.InteractionType = 'test';
            params.add(param);

            Test.startTest();
            List<CORE_FLEX_GetInfo.SuccessResult> results = CORE_FLEX_GetInfo.execute(params);
                
            System.assertEquals(false, results.isEmpty());
            CORE_FLEX_GetInfo.SuccessResult result = results.get(0);

            System.assertEquals('CORE_FLEX_GetInfo', result.actionName);
            System.assertEquals(true, result.isSuccess);
            System.assertEquals(null, result.outputValues.Account);
            System.assertEquals(null, result.outputValues.Username);
            System.assertNotEquals(null, result.outputValues.BusinessUnit);
            System.assertEquals(bu.Id, result.outputValues.BusinessUnit.Id);
            System.assertEquals(null, result.outputValues.UserId);
            System.assertEquals(null, result.outputValues.Opportunity);
            System.assertEquals(null, result.outputValues.UserEmail);
            Test.stopTest();
        }
    }

    @IsTest
    private static void execute_should_return_an_opportunity_if_found(){
        CORE_Business_Unit__c bu = [SELECT Id FROM CORE_Business_Unit__c LIMIT 1];
        Opportunity opp = [SELECT Id, OwnerId, Owner.Username, Owner.Email FROM Opportunity LIMIT 1];
        Account account = [SELECT Id FROM Account WHERE PersonMobilePhone = :ACC_PHONE_NUMBER];
        User usr = [SELECT Id FROM User WHERE Username = :USERNAME_TEST LIMIT 1];

        List<PermissionSetAssignment> psaList = new List<PermissionSetAssignment>();

        for(PermissionSet ps : [SELECT Id FROM PermissionSet WHERE Name = :FLEX_PERMISSION_SET]){
            psaList.add(
                new PermissionSetAssignment(AssigneeId = usr.id, PermissionSetId = ps.Id)
            );
        }
        
        database.insert(psaList, true);
        
        System.runAs(usr){
            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
            req.requestURI = '/services/apexrest/twilio/CORE_FLEX_GetInfo';
            req.httpMethod = 'POST';

            RestContext.request = req;
            RestContext.response= res;
            
            List<CORE_FLEX_GetInfo.InputParams> params = new List<CORE_FLEX_GetInfo.InputParams>();
            CORE_FLEX_GetInfo.InputParams param = new CORE_FLEX_GetInfo.InputParams();
            param.CalledPhone = BU_PHONE_NUMBER;
            param.CallerPhone = ACC_PHONE_NUMBER;
            param.InteractionType = 'test';
            params.add(param);

            Test.startTest();
            List<CORE_FLEX_GetInfo.SuccessResult> results = CORE_FLEX_GetInfo.execute(params);
                
            System.assertEquals(false, results.isEmpty());
            CORE_FLEX_GetInfo.SuccessResult result = results.get(0);

            System.assertEquals('CORE_FLEX_GetInfo', result.actionName);
            System.assertEquals(true, result.isSuccess);
            System.assertNotEquals(null, result.outputValues.Account);
            System.assertEquals(account.Id, result.outputValues.Account.Id);
            System.assertNotEquals(null, result.outputValues.Username);
            System.assertEquals(opp.Owner.Username, result.outputValues.Username);
            System.assertNotEquals(null, result.outputValues.BusinessUnit);
            System.assertEquals(bu.Id, result.outputValues.BusinessUnit.Id);
            System.assertEquals((String) opp.OwnerId, result.outputValues.UserId);
            System.assertNotEquals(null, result.outputValues.Opportunity);
            System.assertEquals(opp.Id, result.outputValues.Opportunity.Id);
            System.assertEquals((String) opp.Owner.Email, result.outputValues.UserEmail);
            Test.stopTest();
        }
    }
}