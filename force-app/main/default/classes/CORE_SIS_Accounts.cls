@RestResource(urlMapping='/sis/accounts')
global without sharing class CORE_SIS_Accounts {
    @HttpGet
    global static CORE_SIS_Wrapper.ResultWrapperAccountList accountList() {
        String urlCallback='';
        RestRequest	request = RestContext.request;
        string lastDate = request.params.get(CORE_SIS_Constants.PARAM_LASTDATE);
        String opportunityStatus = request.params.get(CORE_SIS_Constants.PARAM_OPPSTATUS);

        List<String> opportunityStatusList = CORE_SIS_GlobalWorker.splitValues(opportunityStatus);
        
        
        string recordType = request.params.get(CORE_SIS_Constants.PARAM_RECTYPE);
        string lastId = request.params.get(CORE_SIS_Constants.PARAM_LAST_ID);
        String additionalFields = request.params.get(CORE_SIS_Constants.PARAM_ADD_FIELDS);
        List<String> additionalFieldsList = CORE_SIS_GlobalWorker.splitValues(additionalFields);
        
        CORE_SIS_Wrapper.ResultWrapperAccountList result = checkMandatoryParameters(lastDate,recordType,opportunityStatus);
    
        if(result != null){
            return result;
        }
        try{
            List<String> accountsId = CORE_SIS_QueryProvider.getAccountListById(opportunityStatusList,lastDate,recordType);
            List<Account> accountList = CORE_SIS_QueryProvider.getAccountList(accountsId, additionalFieldsList,lastId);

            if(accountsId.size()>=2000){
                urlCallback = URL.getOrgDomainUrl().toExternalForm()
                            + '/services/apexrest/sis/accounts?' 
                            + CORE_SIS_Constants.PARAM_LASTDATE + '=' + lastDate
                            + '&' + CORE_SIS_Constants.PARAM_OPPSTATUS + '=' + opportunityStatus
                            + '&' + CORE_SIS_Constants.PARAM_RECTYPE + '=' + recordType
                            + '&' + CORE_SIS_Constants.PARAM_LAST_ID + '=' + accountList.get(accountList.size()-1).Id;
            }

            result = new CORE_SIS_Wrapper.ResultWrapperAccountList(accountsId.size(), urlCallback, accountList, CORE_SIS_Constants.SUCCESS_STATUS);
        } catch(Exception e){
            result = new CORE_SIS_Wrapper.ResultWrapperAccountList(CORE_SIS_Constants.ERROR_STATUS, e.getMessage() + ' at ' + e.getStackTraceString());
        }

        return result;
    }

    private static CORE_SIS_Wrapper.ResultWrapperAccountList checkMandatoryParameters(String lastDate, String recordType, String opportunityStatus){
        List<String> missingParameters = new List<String>();
        Boolean isMissingLastDate= CORE_SIS_GlobalWorker.isParameterMissing(lastDate);
        Boolean isMissingRecordType= CORE_SIS_GlobalWorker.isParameterMissing(recordType);
        Boolean isMissingOpportunityStatus= CORE_SIS_GlobalWorker.isParameterMissing(opportunityStatus);

        if(isMissinglastDate){
            missingParameters.add(CORE_SIS_Constants.PARAM_LASTDATE);
        }

        if(isMissingRecordType){
            missingParameters.add(CORE_SIS_Constants.PARAM_RECTYPE);
        }

        if(isMissingOpportunityStatus){
            missingParameters.add(CORE_SIS_Constants.PARAM_OPPSTATUS);
        }
        
        return getMissingParameters(missingParameters);
    }

    public static CORE_SIS_Wrapper.ResultWrapperAccountList getMissingParameters(List<String> missingParameters){
        String errorMsg = CORE_SIS_Constants.MSG_MISSING_PARAMETERS;

        if(missingParameters.size() > 0){
            errorMsg += missingParameters;

            return new CORE_SIS_Wrapper.ResultWrapperAccountList(CORE_SIS_Constants.ERROR_STATUS, errorMsg);
        }
        
        return null;
    }
}