@isTest
public with sharing class CORE_SISOpportunityApplicationUtilsTest {
    @TestSetup
    public static void makeData(){
        CORE_DataFaker_CatalogHierarchy catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test');
        Account account = CORE_DataFaker_Account.getStudentAccount('test');
        account.PersonEmail='test@email.com';
        account.Phone = '+33668888888';
        account.CORE_SISStudentID__c = 'SIS-Student';
        update account;
        
        Contact contact = new Contact(LastName='Test',CORE_LegacyContactCRMID__c ='SIS-Contact');
        insert contact;
        

        Opportunity Opportunity = CORE_DataFaker_Opportunity.getOpportunity(catalogHierarchy.division.Id, 
            catalogHierarchy.businessUnit.Id, 
            catalogHierarchy.studyArea.Id,
            catalogHierarchy.curriculum.Id,
            catalogHierarchy.level.Id,
            catalogHierarchy.intake.Id,
            account.Id, 
            'Registered', 
            'Registered - Classical'
        );
        Opportunity opp = new Opportunity(id=Opportunity.Id,CORE_OPP_Academic_Year__c = 'CORE_PKL_2021-2022');
        update opp;

        CORE_OpportunityDataMaker dataMaker = new CORE_OpportunityDataMaker();
        String captureChannel = 'CORE_PKL_Emailings';
        String nature = 'CORE_PKL_Online';
        String origin = 'Website';
        CORE_Point_of_Contact__c result = dataMaker.initPocDefaultValues(opp,nature,captureChannel,origin);
        insert result;


        CORE_Consent__c consent1 = new CORE_Consent__c();
        consent1.CORE_Person_Account__c = account.Id;
        consent1.CORE_Business_Unit__c = catalogHierarchy.businessUnit.Id;
        consent1.CORE_Consent_type__c = 'CORE_Optin_Optout';
        consent1.CORE_Consent_channel__c = 'CORE_PKL_Email';
        consent1.CORE_Opt_in__c = true;

        CORE_ConsentTriggerHandler.updateInternalReference(new List<CORE_Consent__c>{consent1});
        insert consent1;

    }
    @isTest
    public static void testGetAccountFromId() {
        List<Account> Ids= [SELECT ID from Account];
        System.assertNotEquals(null, CORE_SISOpportunityApplicationUtils.getAccountFromId(Ids[0].Id));
    }

    
    @isTest
    public static void testGetExistingAccounts() {
        System.assertEquals(1, CORE_SISOpportunityApplicationUtils.getExistingAccounts('test@email.com','').size());
        System.assertEquals(0, CORE_SISOpportunityApplicationUtils.getExistingAccounts('test+2@email.com','').size());
        System.assertEquals(1, CORE_SISOpportunityApplicationUtils.getExistingAccounts('','+33668888888').size());
        System.assertEquals(1, CORE_SISOpportunityApplicationUtils.getExistingAccounts('test+2@email.com','+33668888888').size());
    }

    @isTest
    public static void testInitPersonAccountFromMap() {
        Account account = [SELECT PersonEmail,Phone FROM Account LIMIT 1]; 
        Map<String, Object> accountmap = (Map<String, Object>) JSON.deserializeUntyped(JSON.serialize(account));

        System.assertEquals('test@email.com',CORE_SISOpportunityApplicationUtils.initPersonAccountFromMap(accountmap,false).PersonEmail);

    }

    @isTest
    public static void testInitOpportunityFromMap() {
        Opportunity opportunity = [SELECT CORE_OPP_Academic_Year__c FROM Opportunity LIMIT 1]; 
        Map<String, Object> opportunityMap = (Map<String, Object>) JSON.deserializeUntyped(JSON.serialize(opportunity));
        opportunityMap.put('CORE_OPP_Academic_Year__c', 'CORE_PKL_2022-2023');
        opportunityMap.put('CORE_OPP_Retail_Agency__c','SIS-Student');
        opportunityMap.put('CORE_RetailAgencyBusinessContact__c','SIS-Contact');
        System.assertEquals('CORE_PKL_2022-2023',(String) CORE_SISOpportunityApplicationUtils.initOpportunityFromMap(opportunityMap,opportunity).CORE_OPP_Academic_Year__c);

    }
    @isTest
    public static void testgetPocFromWrapper() {
        CORE_Point_Of_Contact__c poc = new CORE_Point_Of_Contact__c();
        CORE_Point_Of_Contact__c newPoc = new CORE_Point_Of_Contact__c();
        poc.CORE_Latitude__c = '14.20';
        poc.CORE_Longitude__c = '15.20';
        CORE_SISOpportunityApplicationWrapper.AcquisitionPointOfContactWrapper wrapper = new CORE_SISOpportunityApplicationWrapper.AcquisitionPointOfContactWrapper();
        wrapper.ipAddress='193.192.33.11';
        newPoc = CORE_SISOpportunityApplicationUtils.getPocFromWrapper(poc,  wrapper);

        System.assertEquals('14.20',newPoc.CORE_Latitude__c );
        System.assertEquals('15.20',newPoc.CORE_Longitude__c );
        System.assertEquals('193.192.33.11',newPoc.CORE_IP_address__c );
    }
        
    @isTest
    public static void testInitConsentFromMap() {
        CORE_Consent__c consent = new CORE_Consent__c();
        consent = [SELECT CORE_Consent_type__c, CORE_Consent_channel__c  FROM CORE_Consent__c LIMIT 1]; 
        Map<String, Object> consentMap = new Map<String,Object>();
        consentMap.put('CORE_Consent_channel__c', 'CORE_PKL_Phone');
        System.assertEquals('CORE_PKL_Phone',(String) CORE_SISOpportunityApplicationUtils.initConsentFromMap(consentMap,consent).CORE_Consent_channel__c);
    }
}