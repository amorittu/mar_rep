@isTest
private class CORE_Flow_AnonymizeText_TEST {

    @isTest
    static void testAnonymize(){
        CORE_Flow_AnonymizeText.AnonymizeParameters param = new CORE_Flow_AnonymizeText.AnonymizeParameters();
        param.textLength = 20;

        test.startTest();
        
        List<String> textList = CORE_Flow_AnonymizeText.anonymize(
            new List<CORE_Flow_AnonymizeText.AnonymizeParameters> { param }
        );

        test.stopTest();

        system.assertEquals(20, textList[0].length());
    }
}