/**
 * Created by ftrapani on 29/10/2021.
 * Test class: IT_TaskTriggerHandlerTest
 */

public without sharing class IT_TaskTriggerHandler {
   public static final String CLASS_NAME = IT_TaskTriggerHandler.class.getName();
    public static Boolean runAssignment = true;
   public static Boolean runUpdateRelatedOpps = true;

    public static void beforeInsert(List<Task> newRecordsList) {
        manageReturningInterestQualificationTask(newRecordsList, null);
    }
    public static void beforeUpdate(List<Task> newRecordsList,Map<Id,Task> oldRecordsMap) {

    }
    public static void afterInsert(List<Task> newRecordsList) {
        upsertExtendedInfo(newRecordsList,null, false);
    }
    public static void afterUpdate(List<Task> newRecordsList,Map<Id,Task> oldRecordsMap) {
        upsertExtendedInfo(newRecordsList,oldRecordsMap, false);
        if(runAssignment){
            getTaskReassignmet(newRecordsList,oldRecordsMap);
        }

        // if (runUpdateRelatedOpps) {
            updateRelatedOpps(newRecordsList, oldRecordsMap);
        // }
    }

    public static void upsertExtendedInfo(List<Task> newRecordsList,Map<Id,Task> oldRecordsMap, Boolean batchRecalculation){
        List<IT_ExtendedInfo__c> extInfoList = new List<IT_ExtendedInfo__c>();
                Set<Id> optyIdSet = new Set<Id>();
        List<Task> processedTask = new List<Task>();
        Set<Id> userIds = new Set<Id>();    //SZOCCHI
        userIds.add(UserInfo.getUserId());  //SZOCCHI
        for(Task taskRecord : newRecordsList){
            if(taskRecord.WhatId!= null && ((String)taskRecord.WhatId).startsWith('006')){
                optyIdSet.add(taskRecord.WhatId);
                processedTask.add(taskRecord);
                userIds.add(taskRecord.OwnerId);  //SZOCCHI
            }
        }

        if(processedTask.size()>0){
            Map<Id,User> userMap = new Map<Id, User>([SELECT Id, UserRoleId, UserRole.DeveloperName FROM User WHERE Id IN :userIds]);  //SZOCCHI
            Map<Id,Opportunity> opportunityMap = new Map<Id,Opportunity>([SELECT Id, CORE_OPP_Business_Unit__r.CORE_Brand__c, CORE_Reached_Lead__c, AccountId FROM Opportunity WHERE Id IN :optyIdSet]);
            Set<Id> accountIdSet = new  Set<Id>();
            Map<Id,Id> opportunityIdAccountIdMap = new  Map<Id,Id>();

            for(Opportunity opportunityRecord : opportunityMap.values()){
                accountIdSet.add(opportunityRecord.AccountId);
                opportunityIdAccountIdMap.put(opportunityRecord.Id,opportunityRecord.AccountId);

            }

            Map<Id,Account> accountMap = new Map<Id,Account>([SELECT Id, (SELECT Id, IT_Brand__c, IT_FirstContactAdmissionDate__c, IT_FirstContactReachedDateInformation__c, 
                                                                          IT_FirstContactReachedDateAdmission__c, IT_First_Contact_with_IC__c,IT_FirstContactDate__c, IT_ExternalId__c,
                                                                          IT_LastInboundcontactDate__c   FROM Extended_Informations__r) FROM Account WHERE Id IN :accountIdSet]);
            Map<String,IT_Role_Type__mdt> typeRoleMap = IT_Utility.getRoleTypeMDT2();
            User cu = userMap.get(UserInfo.getUserId());
            String currentUserRoleType = typeRoleMap.get(cu?.UserRole.DeveloperName)!=null && String.isNotBlank( typeRoleMap.get(cu?.UserRole.DeveloperName).IT_Type__c) ? typeRoleMap.get(cu?.UserRole.DeveloperName).IT_Type__c : '';

            for(Task taskRecord : processedTask){
                User owner = userMap.get(taskRecord.OwnerId);
                String ownerRoleType = typeRoleMap.get(owner?.UserRole.DeveloperName)!=null && String.isNotBlank( typeRoleMap.get(owner?.UserRole.DeveloperName).IT_Type__c) ? typeRoleMap.get(owner?.UserRole.DeveloperName).IT_Type__c : '';

                String roleType = batchRecalculation ? ownerRoleType : currentUserRoleType;

                Task oldTaskRecord = oldRecordsMap!= null ? oldRecordsMap.get(taskRecord.Id) : null;
                Opportunity optyRecord = opportunityMap.get(taskRecord.WhatId);
                Account accountRecord = accountMap.get(opportunityIdAccountIdMap.get(taskRecord.WhatId));
                List<IT_ExtendedInfo__c> extendedInfoList = accountRecord.Extended_Informations__r!=null ? accountRecord.Extended_Informations__r : new List<IT_ExtendedInfo__c> ();
                System.debug(CLASS_NAME + ' upsertExtendedInfo - extendedInfoList is : ' + extendedInfoList);
                Map<String,IT_ExtendedInfo__c> brandExtInfoMap = new Map<String,IT_ExtendedInfo__c>();
                if(extendedInfoList.size()>0){
                    for(IT_ExtendedInfo__c extInfo : extendedInfoList){
                        brandExtInfoMap.put(extInfo.IT_Brand__c,extInfo);
                    }

                }
                IT_ExtendedInfo__c extInfo = brandExtInfoMap.get(optyRecord.CORE_OPP_Business_Unit__r.CORE_Brand__c)!= null ? brandExtInfoMap.get(optyRecord.CORE_OPP_Business_Unit__r.CORE_Brand__c) : null ;

                // SZOCCHI: System.Now() -> taskRecord.CompletedDateTime
                if(extInfo!=null){
                    if(oldTaskRecord!=null){
                        if((batchRecalculation || oldTaskRecord.CompletedDateTime!= taskRecord.CompletedDateTime) && !taskRecord.CORE_Is_Incoming__c){
                            if(String.isNotBlank(roleType)){

                                System.debug('roleType: ' + roleType);
                                if(roleType=='Information'){
                                    extInfo.IT_LastContactInformationDate__c = taskRecord.CompletedDateTime; //SZOCCHI

                                    if(taskRecord.Status=='CORE_PKL_Completed' && extInfo.IT_First_Contact_with_IC__c==null){
                                        extInfo.IT_First_Contact_with_IC__c = taskRecord.CompletedDateTime; //SZOCCHI
                                    }
                                    if(optyRecord.CORE_Reached_Lead__c){
                                        extInfo.IT_FirstContactReachedDateInformation__c = extInfo.IT_FirstContactReachedDateInformation__c==null ? taskRecord.CompletedDateTime : extInfo.IT_FirstContactReachedDateInformation__c; //SZOCCHI
                                        extInfo.IT_LastContactReachedDateInformation__c = taskRecord.CompletedDateTime; //SZOCCHI
                                    }

                                }
                                if(roleType=='Admission'){
                                    extInfo.IT_LastContactAdmissionDate__c = taskRecord.CompletedDateTime; //SZOCCHI
                                    if(taskRecord.Status=='CORE_PKL_Completed' && extInfo.IT_FirstContactAdmissionDate__c==null){
                                        extInfo.IT_FirstContactAdmissionDate__c = taskRecord.CompletedDateTime; //SZOCCHI
                                    }

                                    // if(optyRecord.CORE_Reached_Lead__c){
                                    //     extInfo.IT_FirstContactReachedDateAdmission__c = extInfo.IT_FirstContactReachedDateAdmission__c==null ? taskRecord.CompletedDateTime : extInfo.IT_FirstContactReachedDateInformation__c; //SZOCCHI
                                    //     extInfo.IT_LastContactReachedDateAdmission__c = taskRecord.CompletedDateTime; //SZOCCHI
                                    // }

                                    // Amorittu - 22/08/22 - Fix update on IT_FirstContactReachedDateAdmission__c, IT_LastContactReachedDateAdmission__c
                                    System.debug('taskRecord.Core_Task_Result__c ' + taskRecord.Core_Task_Result__c);
                                    System.debug('extInfo.IT_FirstContactReachedDateAdmission__c ' + extInfo.IT_FirstContactReachedDateAdmission__c);
                                    System.debug('taskRecord.CompletedDateTime ' + taskRecord.CompletedDateTime);
                                    if(String.isNotBlank(taskRecord.Core_Task_Result__c) && ( taskRecord.Core_Task_Result__c.containsIgnoreCase('IT_PKL_Reached')  || taskRecord.Core_Task_Result__c.containsIgnoreCase('CORE_PKL_Reached') ) ){
                                        extInfo.IT_FirstContactReachedDateAdmission__c = extInfo.IT_FirstContactReachedDateAdmission__c==null ? taskRecord.CompletedDateTime : extInfo.IT_FirstContactReachedDateInformation__c; //SZOCCHI
                                        extInfo.IT_LastContactReachedDateAdmission__c = taskRecord.CompletedDateTime; //SZOCCHI
                                    }
                                }

                                extInfoList.add(extInfo);



                            }
                        }
                     } else {
                         System.debug('§§§ task Id: '+ taskRecord.Id);
						//ESCUDO, 10/06/22: rework on date
						Datetime nowDate = System.Now();
                        if(batchRecalculation){
                            nowDate = taskRecord.CreatedDate;
                        }
                         system.debug('§§ taskRecord.CreatedDate: '+taskRecord.CreatedDate);
                       // Datetime nowDate = taskRecord.LastModifiedDate;     // SZOCCHI: System.Now() -> taskRecord.LastModifiedDate
                        if(taskRecord.CORE_Is_Incoming__c && ( extInfo.IT_LastInboundcontactDate__c == null || nowDate > extInfo.IT_LastInboundcontactDate__c )){
                            extInfo.IT_LastInboundcontactDate__c = nowDate;
                        }
						system.debug('§§ extInfo.IT_LastInboundcontactDate__c: '+ extInfo.IT_LastInboundcontactDate__c);
                         system.debug('§§ extInfo.IT_FirstContactDate__c: '+ extInfo.IT_FirstContactDate__c);
                        if(extInfo.IT_FirstContactDate__c==null && taskRecord.CORE_Is_Incoming__c){ //ESCUDO, added filter only incoming tasks
                            extInfo.IT_FirstContactDate__c = nowDate;
                        }
                         system.debug('§§ extInfo.IT_FirstContactDate__c after: '+ extInfo.IT_FirstContactDate__c);
                        extInfoList.add(extInfo);


                    }
                }

            }
            if(extInfoList.size()>0){
                // update extInfoList;
                Set<IT_ExtendedInfo__c> extInfoSet= new Set<IT_ExtendedInfo__c>();
                extInfoSet.addAll(extInfoList);
                extInfoList.clear();

                extInfoList.addAll(extInfoSet);
                List<Database.SaveResult> results = Database.update( extInfoList, false );

                for(Database.SaveResult resultRecord : results) {
                    if(!resultRecord.isSuccess()){
                        List<Database.Error> errors = resultRecord.getErrors();
                        System.debug('IT_TaskTriggerHandler update error: ' + resultRecord + ', errors ' + errors);
                    }
                }

            }
        }

    }

    public static void manageReturningInterestQualificationTask(List<Task> newRecordsList,Map<Id,Task> oldRecordsMap){
        System.debug('	IT_TaskTriggerHandler::manageReturningInterestQualificationTask::begin');
        Set<Id> intakeIds = new Set<Id>();
        Set<Id> optyIds = new Set<Id>();
        Set<Id> optyToUpdateIds = new Set<Id>();
        Set<Id> productIds = new Set<Id>();
        Set<Id> productIdsToFindPBE = new Set<Id>();
        Set<Id> optyPriceBook2Ids = new Set<Id>();
        Set<String> optyAndProductIds = new Set<String>();
        List<Task> processedTasks = new List<Task>();
        List<OpportunityLineItem> opportunityLineItemsToInsert = new List<OpportunityLineItem>();
        Boolean isWhatIdOpty = false;
        for (Task task : newRecordsList) {
            if(task.WhatId!=null){
                SObject sObjParent = task.WhatId.getSobjectType().newSObject();
                if(sObjParent instanceof Opportunity && task.CORE_Intake__c != null
                        && task.CORE_Product__c != null && task.Subject == 'Returning interest qualification'
                        && task.Type == 'CORE_PKL_Web_Form'){
                    intakeIds.add(task.CORE_Intake__c);
                    optyIds.add(task.WhatId);
                    productIdsToFindPBE.add(task.CORE_Product__c);
                    processedTasks.add(task);
                    isWhatIdOpty = true;
                }
            }
        }
	System.debug('§§ isWhatIdOpty: '+isWhatIdOpty);
        if(isWhatIdOpty) {
            Map<Id, Opportunity> opportunityMap = new Map<Id, Opportunity>([
                    SELECT Id, StageName, CORE_OPP_Intake__c, CORE_Main_Product_Interest__c, CORE_OPP_Level__c, CORE_OPP_Curriculum__c, CORE_OPP_Study_Area__c,
                            CORE_OPP_Business_Unit__r.IT_AcademicYearStartMonth__c, CORE_OPP_Confirmed_Interest__c, Pricebook2Id
                    FROM Opportunity
                    WHERE Id IN :optyIds AND StageName IN ('Lead', 'Prospect')
            ]);
            //EScudo, 19/05/22, remove unnecessary queries
            if(opportunityMap.isEmpty()){
                return;
            }
            System.debug('§§§ opportunityMap: '+opportunityMap);
            for (Task task : processedTasks) {
                if (opportunityMap.containsKey(task.WhatId)) {
                    Opportunity opty = opportunityMap.get(task.WhatId);
                    optyPriceBook2Ids.add(opty.Pricebook2Id);
                }
                
            }
			System.debug('§§§ opportunityMap 2 : '+opportunityMap);
            
            List<PricebookEntry> pricebookEntries = [
                    SELECT Id, Product2Id, Pricebook2Id
                    FROM PricebookEntry
                    WHERE Product2Id IN :productIdsToFindPBE AND Pricebook2Id IN :optyPriceBook2Ids
            ];
            Map<String, PricebookEntry> pricebookEntryMap = new Map<String, PricebookEntry>();

            for (PricebookEntry pbe : pricebookEntries) {
                String key = pbe.Product2Id + '_' + pbe.Pricebook2Id;
                pricebookEntryMap.put(key, pbe);
                system.debug('§§ pricebookEntryMap key: '+key );
            }

            Map<Id, CORE_Intake__c> coreIntakeMap = new Map<Id, CORE_Intake__c>([
                    SELECT Id, CORE_Academic_Year__c, CORE_Session__c
                    FROM CORE_Intake__c
                    WHERE Id IN :intakeIds
            ]);

            for (Task task : processedTasks) {
                Opportunity opty = opportunityMap.get(task.WhatId);
                CORE_Intake__c intake = coreIntakeMap.get(task.CORE_Intake__c);
                
                //EScudo, 19/05/22 fix bug
                PricebookEntry pbe = new PricebookEntry();
                if(opty != null && opty.Pricebook2Id != null){
                	System.debug('§§§§opty.Pricebook2Id: '+opty.Pricebook2Id);
                	pbe = pricebookEntryMap.get(task.CORE_Product__c + '_' + opty.Pricebook2Id);
                	System.debug('pbe:: ' + pbe + ' opty.CORE_OPP_Confirmed_Interest__c:: ' + opty.CORE_OPP_Confirmed_Interest__c);
                }
                if (pbe != null && opty != null) {
                    System.debug('opty.CORE_OPP_Business_Unit__r.IT_AcademicYearStartMonth__c: '+opty.CORE_OPP_Business_Unit__r.IT_AcademicYearStartMonth__c);
                    System.debug('§§intake: '+intake);
                    if (!opty.CORE_OPP_Confirmed_Interest__c || (intake != null && isIntakeStarted(intake, opty.CORE_OPP_Business_Unit__r.IT_AcademicYearStartMonth__c))) {
                        productIds.add(opty.CORE_Main_Product_Interest__c);
                        optyAndProductIds.add(opty.Id + '_' + opty.CORE_Main_Product_Interest__c);
                        populateProductInfoAndIntake(task, opty);
                        optyToUpdateIds.add(opty.Id);

                        opportunityLineItemsToInsert.add(new OpportunityLineItem(
                                Product2Id = task.CORE_Product__c,
                                OpportunityId = opty.Id,
                                Quantity = 1,
                                UnitPrice = 0,
                                PricebookEntryId = pbe.Id
                        ));
                    }
                }
            }

            //update opportunityMap.values();

            List<Database.SaveResult> results = Database.update(opportunityMap.values(), false);

            for (Database.SaveResult resultRecord : results) {
                if (!resultRecord.isSuccess()) {
                    List<Database.Error> errors = resultRecord.getErrors();
                    System.debug('IT_TaskTriggerHandler update error: ' + resultRecord + ', errors ' + errors);
                }
            }

            List<OpportunityLineItem> opportunityLineItems = [SELECT Id, OpportunityId, Product2Id FROM OpportunityLineItem WHERE OpportunityId IN :optyToUpdateIds AND Product2Id IN :productIds];
            List<OpportunityLineItem> opportunityLineItemsToDelete = new List<OpportunityLineItem>();
            for (OpportunityLineItem optyLineItem : opportunityLineItems) {
                if (optyAndProductIds.contains(optyLineItem.OpportunityId + '_' + optyLineItem.Product2Id)) {
                    opportunityLineItemsToDelete.add(optyLineItem);
                }
            }
            delete opportunityLineItemsToDelete;

            //insert opportunityLineItemsToInsert;

            List<Database.SaveResult> results2 = Database.insert(opportunityLineItemsToInsert, false);

            for (Database.SaveResult resultRecord : results2) {
                if (!resultRecord.isSuccess()) {
                    List<Database.Error> errors = resultRecord.getErrors();
                    System.debug('IT_TaskTriggerHandler update error: ' + resultRecord + ', errors ' + errors);
                }
            }
        }

    }

    private static void populateProductInfoAndIntake(Task task, Opportunity opty){
        System.debug('populateProductInfoAndIntake has been fired');
        task.Status = 'CORE_PKL_Completed';
        //ESCUDO, 17/06/22, added level, curriculum, study area
        opty.CORE_OPP_Study_Area__c = task.CORE_Study_Area__c != null ? task.CORE_Study_Area__c : opty.CORE_OPP_Study_Area__c;
        opty.CORE_OPP_Curriculum__c = task.CORE_Curriculum__c != null ? task.CORE_Curriculum__c : opty.CORE_OPP_Curriculum__c;
        opty.CORE_OPP_Level__c = task.CORE_Level__c != null ? task.CORE_Level__c : opty.CORE_OPP_Level__c;
        opty.CORE_OPP_Intake__c = task.CORE_Intake__c != null ? task.CORE_Intake__c : opty.CORE_OPP_Intake__c;
        opty.CORE_Main_Product_Interest__c = task.CORE_Product__c != null ? task.CORE_Product__c : opty.CORE_Main_Product_Interest__c;
    }

    private static Boolean isIntakeStarted(CORE_Intake__c intake, String academicYearStartMonth){
        return IT_Utility.isIntakeStarted(intake, academicYearStartMonth);
    }

    public static void getTaskReassignmet(List<Task> newRecordsList, Map<Id,Task> oldRecordsMap){
        List<Task> taskList = new List<Task>();
        List<Id> opportunityIdList = new List<Id>();
        Map<Id,Id> opportunityOwnerIdMap = new  Map<Id,Id>();
        for(Task taskRecord : newRecordsList) {
            Task oldTask = oldRecordsMap.get(taskRecord.Id);
            if(taskRecord.CORE_RecordTypeDevName__c=='CORE_Continuum_of_action' && taskRecord.Status == 'CORE_PKL_Open' && 
               taskRecord.WhatId!= null && taskRecord.WhatId.getSobjectType().newSObject() instanceof Opportunity && taskRecord.OwnerId!=oldTask.OwnerId && 
               ((String)oldTask.OwnerId).startsWith('00G') && ((String)taskRecord.OwnerId).startsWith('005')){
                opportunityIdList.add(taskRecord.WhatId);
                taskList.add(taskRecord);
                opportunityOwnerIdMap.put(taskRecord.WhatId,taskRecord.OwnerId);
            }
        }

        List<Opportunity> optyList = new List<Opportunity>();
        List<Task> totalTaskList = new List<Task>();
        if(opportunityIdList.size()>0){
            List<Opportunity> opportunityList = [SELECT Id, OwnerId, (SELECT Id, OwnerId FROM Tasks WHERE Status = 'CORE_PKL_Open' AND CORE_RecordTypeDevName__c='CORE_Continuum_of_action' AND Id NOT IN :taskList) FROM Opportunity WHERE Id IN :opportunityIdList ];
            for(Opportunity optyRecord : opportunityList){
                List<Task> optyTaskList = optyRecord.Tasks;
                optyRecord.OwnerId = opportunityOwnerIdMap.get(optyRecord.Id);
                if(optyTaskList.size()>0){
                    for(Task tRecord : optyTaskList){
                        tRecord.OwnerId = optyRecord.OwnerId;
                    }
                    totalTaskList.addAll(optyTaskList);
                }
                optyList.add(optyRecord);
            }

            if(optyList.size()>0){
                update optyList;
            }
            if(totalTaskList.size()>0){
                update totalTaskList;
            }
        }

    }

    static void updateRelatedOpps(List<Task> newTasks, Map<Id, Task> oldMap) {
        System.debug('---newTasks' + newTasks);
        Map<Id, Id> taskOppMap = new Map<Id, Id>();
        Map<Id, String> taskEmailMap = new Map<Id, String>();
        for (Task t: newTasks) {
            if (t.CORE_Recipient_Incoming_Email_Address__c != oldMap.get(t.id).CORE_Recipient_Incoming_Email_Address__c) {
                if (t.CORE_Is_Link_To_Opportunity__c && t.CORE_Recipient_Incoming_Email_Address__c != null) {
                    taskOppMap.put(t.Id, t.WhatId);
                    taskEmailMap.put(t.Id, t.CORE_Recipient_Incoming_Email_Address__c);
                }    
            }
        }
        System.debug('---taskEmailMap ' + taskEmailMap);
        Map<Id,Opportunity> relatedOppsMap = new Map<Id,Opportunity>([
            SELECT IT_OrientationQueue__c, IT_OfficeInCharge__c, CORE_OPP_Business_Unit__c
            FROM Opportunity
            WHERE Id IN: taskOppMap.values()
            AND CORE_Capture_Channel__c = 'CORE_PKL_Email' AND IT_OpportunityAssigned__c = false
        ]);
        System.debug('---relatedOppsMap ' + relatedOppsMap );
        List<IT_Office__c> relatedOffices = [
            SELECT IT_Type__c, IT_Email__c, IT_BusinessUnit__c, IT_BusinessHours__c
            FROM IT_Office__c
            WHERE IT_Email__c != null
        ];

        Map<String, IT_Office__c> emailOfficesWithBUMap = new Map<String, IT_Office__c>();
        Map<String, IT_Office__c> emailOfficesNoBUMap = new Map<String, IT_Office__c>();

        for (IT_Office__c office : relatedOffices) {
            List<String> emailsList = office.IT_Email__c.split(';');
            for (String email : emailsList) {
                if (office.IT_BusinessUnit__c != null) {
                    emailOfficesWithBUMap.put(email, office);
                } else {
                    emailOfficesNoBUMap.put(email, office);
                }
            }
        }
        System.debug('---emailOfficesWithBUMap ' + emailOfficesWithBUMap);
        System.debug('---emailOfficesNoBUMap ' + emailOfficesNoBUMap);
        List<Opportunity> oppForUpdate = new List<Opportunity>();
        for (Id taskId: taskOppMap.keySet()) {
            Opportunity opp = relatedOppsMap.get(taskOppMap.get(taskId));
            System.debug('---opp ' + opp);
            if (opp != null && !taskEmailMap.isEmpty() && !emailOfficesWithBUMap.isEmpty() ) {
                System.debug('---taskEmailMap ' + JSON.serialize(taskEmailMap) );
                System.debug('---emailOfficesWithBUMap ' + JSON.serialize(emailOfficesWithBUMap) );
                if (taskEmailMap.containsKey(taskId) ) {
                    IT_Office__c office = new IT_Office__c ();
                    if (emailOfficesWithBUMap.containsKey(taskEmailMap.get(taskId) )) {
                        office = emailOfficesWithBUMap.get(taskEmailMap.get(taskId));
                    } else if (taskEmailMap.containsKey(taskId) && emailOfficesNoBUMap.containsKey(taskEmailMap.get(taskId))) {
                        office = emailOfficesNoBUMap.get(taskEmailMap.get(taskId));
                    }

                    System.debug('---office ' + office);
                    
                    if (opp != null && office != null) {
                        if (office.IT_Type__c == 'Admission') {
                            opp.IT_AdmissionQueue__c = office.Id;
                            opp.IT_OfficeInCharge__c = 'Admission';
                            opp.CORE_OPP_Business_Hours__c = office.IT_BusinessHours__c;
                        } else if (office.IT_Type__c == 'Information') {
                            opp.IT_InformationQueue__c = office.Id;
                            opp.IT_OfficeInCharge__c = 'Information';
                            opp.CORE_OPP_Business_Hours__c = office.IT_BusinessHours__c;
                        } else if (office.IT_Type__c == 'Orientation') {
                            opp.IT_OrientationQueue__c = office.Id;
                            opp.IT_OfficeInCharge__c = 'Orientation';
                            opp.CORE_OPP_Business_Hours__c = office.IT_BusinessHours__c;
                        }
                        oppForUpdate.add(opp);
                    }
                }
            }
        }
        System.debug('---oppForUpdate ' + JSON.serialize(oppForUpdate) );
        if(! oppForUpdate.isEmpty() ) {
            update oppForUpdate;
            runUpdateRelatedOpps = false;
        }
    }
}