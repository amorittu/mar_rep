public without sharing class CORE_LFA_AccountManagement {

    @TestVisible
    private static Set<Account> searchAccountsByEmails(Set<String> emails){
        System.debug('CORE_LFA_AccountManagement.searchAccountsByEmails() : START');
        Long dt1 = DateTime.now().getTime();
        Long dt2;

        Set<Account> accounts = new Set<Account> ([SELECT Id, PersonContactId, CreatedDate, Salutation, LastName, FirstName, Phone, CORE_Phone2__pc,
            PersonMobilePhone, CORE_Mobile_2__pc, CORE_Mobile_3__pc, PersonEmail,CORE_Email_2__pc,CORE_Email_3__pc, CORE_Student_Email__pc,
            PersonMailingStreet, PersonMailingPostalCode, PersonMailingCity, CORE_Language_website__pc, PersonMailingCountry, PersonMailingState, OwnerId,CORE_GA_User_ID__c,
            CORE_Gender__pc , CORE_Professional_situation__pc, CORE_Main_Nationality__pc,CORE_Year_of_birth__pc,PersonBirthdate
            FROM Account
            WHERE PersonEmail in :emails OR CORE_Email_2__pc in :emails OR CORE_Email_3__pc in :emails OR CORE_Student_Email__pc in :emails
            ORDER BY CORE_Last_inbound_contact_date__c DESC NULLS LAST, LastModifiedDate Desc]);

        dt2 = DateTime.now().getTime();
        System.debug('CORE_LFA_AccountManagement.searchAccountsByEmails() : END. [EXECUTION TIME : ' + (dt2-dt1) + ' ms]');

        return accounts;
    }

    @TestVisible
    private static Set<Account> searchAccountsByPhones(Set<String> phones){
        System.debug('CORE_LFA_AccountManagement.searchAccountsByPhones() : START');
        Long dt1 = DateTime.now().getTime();
        Long dt2;

        Set<Account> accounts = new Set<Account> ([SELECT Id, PersonContactId, CreatedDate, Salutation, LastName, FirstName, Phone, CORE_Phone2__pc, 
            PersonMobilePhone, CORE_Mobile_2__pc, CORE_Mobile_3__pc, PersonEmail,CORE_Email_2__pc,CORE_Email_3__pc, CORE_Student_Email__pc,
            PersonMailingStreet, PersonMailingPostalCode, PersonMailingCity, CORE_Language_website__pc, PersonMailingCountry, PersonMailingState, OwnerId,CORE_GA_User_ID__c,
            CORE_Gender__pc , CORE_Professional_situation__pc,CORE_Main_Nationality__pc,CORE_Year_of_birth__pc,PersonBirthdate
            FROM Account
            WHERE Phone in :phones OR CORE_Phone2__pc in :phones OR PersonMobilePhone in :phones OR CORE_Mobile_2__pc in :phones OR CORE_Mobile_3__pc in :phones
            ORDER BY CORE_Last_inbound_contact_date__c DESC NULLS LAST, LastModifiedDate Desc]);

        dt2 = DateTime.now().getTime();
        System.debug('CORE_LFA_AccountManagement.searchAccountsByPhones() : END. [EXECUTION TIME : ' + (dt2-dt1) + ' ms]');

        return accounts;
    }

    @TestVisible
    private static Map<String,Set<Account>> sortAccountsByEmails(Set<Account> accounts){
        Map<String, Set<Account>> accountsByEmail = new Map<String,Set<Account>>();

        for(Account currentAccount : accounts){
            if(!String.isEmpty(currentAccount.PersonEmail)){
                if(!accountsByEmail.containsKey(currentAccount.PersonEmail)){
                    accountsByEmail.put(currentAccount.PersonEmail, new Set<Account> {currentAccount});
                } else {
                    accountsByEmail.get(currentAccount.PersonEmail).add(currentAccount);
                }
            }
            if(!String.isEmpty(currentAccount.CORE_Email_2__pc)){
                if(!accountsByEmail.containsKey(currentAccount.CORE_Email_2__pc)){
                    accountsByEmail.put(currentAccount.CORE_Email_2__pc, new Set<Account> {currentAccount});
                } else {
                    accountsByEmail.get(currentAccount.CORE_Email_2__pc).add(currentAccount);
                }
            }
            if(!String.isEmpty(currentAccount.CORE_Email_3__pc)){
                if(!accountsByEmail.containsKey(currentAccount.CORE_Email_3__pc)){
                    accountsByEmail.put(currentAccount.CORE_Email_3__pc, new Set<Account> {currentAccount});
                } else {
                    accountsByEmail.get(currentAccount.CORE_Email_3__pc).add(currentAccount);
                }
            }
            if(!String.isEmpty(currentAccount.CORE_Student_Email__pc)){
                if(!accountsByEmail.containsKey(currentAccount.CORE_Student_Email__pc)){
                    accountsByEmail.put(currentAccount.CORE_Student_Email__pc, new Set<Account> {currentAccount});
                } else {
                    accountsByEmail.get(currentAccount.CORE_Student_Email__pc).add(currentAccount);
                }
            }
        }

        return accountsByEmail;
    }

    @TestVisible
    private static Map<String,Set<Account>> sortAccountsByPhones(Set<Account> accounts){
        Map<String, Set<Account>> accountsByPhone = new Map<String,Set<Account>>();
        for(Account currentAccount : accounts){
            if(!String.isEmpty(currentAccount.Phone)){
                if(!accountsByPhone.containsKey(currentAccount.Phone)){
                    accountsByPhone.put(currentAccount.Phone, new Set<Account> {currentAccount});
                } else {
                    accountsByPhone.get(currentAccount.Phone).add(currentAccount);
                }
            }
            if(!String.isEmpty(currentAccount.PersonMobilePhone)){
                if(!accountsByPhone.containsKey(currentAccount.PersonMobilePhone)){
                    accountsByPhone.put(currentAccount.PersonMobilePhone, new Set<Account> {currentAccount});
                } else {
                    accountsByPhone.get(currentAccount.PersonMobilePhone).add(currentAccount);
                }
            }
            if(!String.isEmpty(currentAccount.CORE_Mobile_2__pc)){
                if(!accountsByPhone.containsKey(currentAccount.CORE_Mobile_2__pc)){
                    accountsByPhone.put(currentAccount.CORE_Mobile_2__pc, new Set<Account> {currentAccount});
                } else {
                    accountsByPhone.get(currentAccount.CORE_Mobile_2__pc).add(currentAccount);
                }
            }
            if(!String.isEmpty(currentAccount.CORE_Mobile_3__pc)){
                if(!accountsByPhone.containsKey(currentAccount.CORE_Mobile_3__pc)){
                    accountsByPhone.put(currentAccount.CORE_Mobile_3__pc, new Set<Account> {currentAccount});
                } else {
                    accountsByPhone.get(currentAccount.CORE_Mobile_3__pc).add(currentAccount);
                }
            }
        }

        
        return accountsByPhone;
    }

    @TestVisible
    public static Map<Id,Set<Account>> getExestingAccounts(List<CORE_LeadAcquisitionBuffer__c> bufferRecords){
        System.debug('CORE_LFA_AccountManagement.getExestingAccounts() : START');
        Long dt1 = DateTime.now().getTime();
        Long dt2;
        
        Map<Id,Set<Account>> results = new Map<Id,Set<Account>>();


        Map<Id,Boolean> searchUsingEmailByRecords = new Map<Id,Boolean>();
        Set<String> emails = new Set<String>();
        Set<String> phones = new Set<String>();

        for(CORE_LeadAcquisitionBuffer__c currentRecord : bufferRecords){
            //init map result
            results.put(currentRecord.Id,new Set<Account>());

            //Init list for search conditions
            if(!String.isEmpty(currentRecord.CORE_PA_Email__c)){
                emails.add(currentRecord.CORE_PA_Email__c);
            } 
            if(!String.isEmpty(currentRecord.CORE_PA_Phone__c)){
                phones.add(currentRecord.CORE_PA_Phone__c);
            }
            if(!String.isEmpty(currentRecord.CORE_PA_Mobile__c)){
                phones.add(currentRecord.CORE_PA_Mobile__c);
            }
        }

        Set<Account> accountFounds = new Set<Account>();

        Map<String, Set<Account>> accountsByEmail = new Map<String,Set<Account>>();
        Map<String, Set<Account>> accountsByPhone = new Map<String,Set<Account>>();

        if(!emails.isEmpty()){
            accountFounds = searchAccountsByEmails(emails);
            accountsByEmail = sortAccountsByEmails(accountFounds);
        }
        if(!phones.isEmpty()){
            accountFounds = searchAccountsByPhones(phones);
            accountsByPhone = sortAccountsByPhones(accountFounds);
        }

        for(CORE_LeadAcquisitionBuffer__c currentRecord : bufferRecords){
            if(!String.isEmpty(currentRecord.CORE_PA_Email__c) && accountsByEmail.containsKey(currentRecord.CORE_PA_Email__c) && !accountsByEmail.get(currentRecord.CORE_PA_Email__c).isEmpty()){
                results.get(currentRecord.Id).addAll(accountsByEmail.get(currentRecord.CORE_PA_Email__c));
            } else {
                if(!String.isEmpty(currentRecord.CORE_PA_Phone__c) && accountsByPhone.containsKey(currentRecord.CORE_PA_Phone__c)){
                    results.get(currentRecord.Id).addAll(accountsByPhone.get(currentRecord.CORE_PA_Phone__c));
                }
                if(!String.isEmpty(currentRecord.CORE_PA_Mobile__c) && accountsByPhone.containsKey(currentRecord.CORE_PA_Mobile__c)){
                    results.get(currentRecord.Id).addAll(accountsByPhone.get(currentRecord.CORE_PA_Mobile__c));
                }
            }
        }
        
        dt2 = DateTime.now().getTime();
        System.debug('CORE_LFA_AccountManagement.getExestingAccounts() : END. [EXECUTION TIME : ' + (dt2-dt1) + ' ms]');

        return results;
    }

    public static CORE_LeadFormAcquisitionWrapper.PersonAccountWrapper getAccountWrapperFromBufferRecord(CORE_LeadAcquisitionBuffer__c bufferRecord){
        CORE_LeadFormAcquisitionWrapper.PersonAccountWrapper personAccountWrapper = new CORE_LeadFormAcquisitionWrapper.PersonAccountWrapper();
        personAccountWrapper.salutation = bufferRecord.CORE_PA_Salutation__c;
        personAccountWrapper.gender = bufferRecord.CORE_PA_Gender__c;
        personAccountWrapper.professionalSituation = bufferRecord.CORE_PA_Professional_situation__c;
        personAccountWrapper.creationDate = bufferRecord.CORE_PA_CreatedDate__c != null ? String.valueOf(bufferRecord.CORE_PA_CreatedDate__c.getTime()) : null;
		personAccountWrapper.lastName = bufferRecord.CORE_PA_LastName__c;
		personAccountWrapper.firstName = bufferRecord.CORE_PA_FirstName__c;
		personAccountWrapper.phone = bufferRecord.CORE_PA_Phone__c;
		personAccountWrapper.mobilePhone = bufferRecord.CORE_PA_Mobile__c;
		personAccountWrapper.emailAddress = bufferRecord.CORE_PA_Email__c;
		personAccountWrapper.street = bufferRecord.CORE_PA_MailingStreet__c;
		personAccountWrapper.postalCode = bufferRecord.CORE_PA_MailingPostalCode__c;
		personAccountWrapper.city = bufferRecord.CORE_PA_MailingCity__c;
		personAccountWrapper.stateCode = bufferRecord.CORE_PA_MailingState__c;
		personAccountWrapper.countryCode = bufferRecord.CORE_PA_MailingCountryCode__c;
		personAccountWrapper.languageWebsite = bufferRecord.CORE_PA_LanguageWebsite__c;
		personAccountWrapper.gaUserId = bufferRecord.CORE_PA_GAUserId__c;
		personAccountWrapper.updateIfNotNull = bufferRecord.CORE_PA_updateIfNotNull__c;
        personAccountWrapper.mainNationality = bufferRecord.CORE_PA_MainNationality__c;
        personAccountWrapper.birthDate = bufferRecord.CORE_PA_birthDate__c;
        personAccountWrapper.birthYear = bufferRecord.CORE_PA_Year_of_Birth__c == null ? null : Integer.valueOf(bufferRecord.CORE_PA_Year_of_Birth__c);
        
        return personAccountWrapper;
    }

    public static CORE_LeadFormAcquisitionWrapper.ConsentWrapper getConsentWrapperFromBufferRecord(CORE_LeadAcquisitionBuffer__c bufferRecord, Integer consentNumber){
        CORE_LeadFormAcquisitionWrapper.ConsentWrapper consentWrapper = new CORE_LeadFormAcquisitionWrapper.ConsentWrapper();

        switch on consentNumber {
            when 2 {
                consentWrapper.consentType = bufferRecord.CORE_CO2_ConsentType__c;
                consentWrapper.channel = bufferRecord.CORE_CO2_ConsentChannel__c;
                consentWrapper.description = bufferRecord.CORE_CO2_ConsentDescription__c;
                consentWrapper.optIn = bufferRecord.CORE_CO2_OptIn__c;
                consentWrapper.optInDate = bufferRecord.CORE_CO2_OptInDate__c != null ? String.valueOf(bufferRecord.CORE_CO2_OptInDate__c.getTime()) : null;
                consentWrapper.doubleOptInDate = bufferRecord.CORE_CO2_DoubleOptInDate__c != null ? String.valueOf(bufferRecord.CORE_CO2_DoubleOptInDate__c.getTime()) : null;
                consentWrapper.optOut = bufferRecord.CORE_CO2_OptOut__c;
                consentWrapper.optOutDate = bufferRecord.CORE_CO2_OptOutDate__c != null ? String.valueOf(bufferRecord.CORE_CO2_OptOutDate__c.getTime()) : null;
                consentWrapper.division = bufferRecord.CORE_CO2_Division__c;
                consentWrapper.businessUnit = bufferRecord.CORE_CO2_BusinessUnit__c;
                consentWrapper.brand = bufferRecord.CORE_CO2_BusinessUnit__r.CORE_Brand__c;
            }
            when 3 {
                consentWrapper.consentType = bufferRecord.CORE_CO3_ConsentType__c;
                consentWrapper.channel = bufferRecord.CORE_CO3_ConsentChannel__c;
                consentWrapper.description = bufferRecord.CORE_CO3_ConsentDescription__c;
                consentWrapper.optIn = bufferRecord.CORE_CO3_OptIn__c;
                consentWrapper.optInDate = bufferRecord.CORE_CO3_OptInDate__c != null ? String.valueOf(bufferRecord.CORE_CO3_OptInDate__c.getTime()) : null;
                consentWrapper.doubleOptInDate = bufferRecord.CORE_CO3_DoubleOptInDate__c != null ? String.valueOf(bufferRecord.CORE_CO3_DoubleOptInDate__c.getTime()) : null;
                consentWrapper.optOut = bufferRecord.CORE_CO3_OptOut__c;
                consentWrapper.optOutDate = bufferRecord.CORE_CO3_OptOutDate__c != null ? String.valueOf(bufferRecord.CORE_CO3_OptOutDate__c.getTime()) : null;
                consentWrapper.division = bufferRecord.CORE_CO3_Division__c;
                consentWrapper.businessUnit = bufferRecord.CORE_CO3_BusinessUnit__c;
                consentWrapper.brand = bufferRecord.CORE_CO3_BusinessUnit__r.CORE_Brand__c;
            }
            when 4 {
                consentWrapper.consentType = bufferRecord.CORE_CO4_ConsentType__c;
                consentWrapper.channel = bufferRecord.CORE_CO4_ConsentChannel__c;
                consentWrapper.description = bufferRecord.CORE_CO4_ConsentDescription__c;
                consentWrapper.optIn = bufferRecord.CORE_CO4_OptIn__c;
                consentWrapper.optInDate = bufferRecord.CORE_CO4_OptInDate__c != null ? String.valueOf(bufferRecord.CORE_CO4_OptInDate__c.getTime()) : null;
                consentWrapper.doubleOptInDate = bufferRecord.CORE_CO4_DoubleOptInDate__c != null ? String.valueOf(bufferRecord.CORE_CO4_DoubleOptInDate__c.getTime()) : null;
                consentWrapper.optOut = bufferRecord.CORE_CO4_OptOut__c;
                consentWrapper.optOutDate = bufferRecord.CORE_CO4_OptOutDate__c != null ? String.valueOf(bufferRecord.CORE_CO4_OptOutDate__c.getTime()) : null;
                consentWrapper.division = bufferRecord.CORE_CO4_Division__c;
                consentWrapper.businessUnit = bufferRecord.CORE_CO4_BusinessUnit__c;
                consentWrapper.brand = bufferRecord.CORE_CO4_BusinessUnit__r.CORE_Brand__c;
            }
            when 5 {
                consentWrapper.consentType = bufferRecord.CORE_CO5_ConsentType__c;
                consentWrapper.channel = bufferRecord.CORE_CO5_ConsentChannel__c;
                consentWrapper.description = bufferRecord.CORE_CO5_ConsentDescription__c;
                consentWrapper.optIn = bufferRecord.CORE_CO5_OptIn__c;
                consentWrapper.optInDate = bufferRecord.CORE_CO5_OptInDate__c != null ? String.valueOf(bufferRecord.CORE_CO5_OptInDate__c.getTime()) : null;
                consentWrapper.doubleOptInDate = bufferRecord.CORE_CO5_DoubleOptInDate__c != null ? String.valueOf(bufferRecord.CORE_CO5_DoubleOptInDate__c.getTime()) : null;
                consentWrapper.optOut = bufferRecord.CORE_CO5_OptOut__c;
                consentWrapper.optOutDate = bufferRecord.CORE_CO5_OptOutDate__c != null ? String.valueOf(bufferRecord.CORE_CO5_OptOutDate__c.getTime()) : null;
                consentWrapper.division = bufferRecord.CORE_CO5_Division__c;
                consentWrapper.businessUnit = bufferRecord.CORE_CO5_BusinessUnit__c;
                consentWrapper.brand = bufferRecord.CORE_CO5_BusinessUnit__r.CORE_Brand__c;
            }
            when else {
                consentWrapper.consentType = bufferRecord.CORE_CO1_ConsentType__c;
                consentWrapper.channel = bufferRecord.CORE_CO1_ConsentChannel__c;
                consentWrapper.description = bufferRecord.CORE_CO1_ConsentDescription__c;
                consentWrapper.optIn = bufferRecord.CORE_CO1_OptIn__c;
                consentWrapper.optInDate = bufferRecord.CORE_CO1_OptInDate__c != null ? String.valueOf(bufferRecord.CORE_CO1_OptInDate__c.getTime()) : null;
                consentWrapper.doubleOptInDate = bufferRecord.CORE_CO1_DoubleOptInDate__c != null ? String.valueOf(bufferRecord.CORE_CO1_DoubleOptInDate__c.getTime()) : null;
                consentWrapper.optOut = bufferRecord.CORE_CO1_OptOut__c;
                consentWrapper.optOutDate = bufferRecord.CORE_CO1_OptOutDate__c != null ? String.valueOf(bufferRecord.CORE_CO1_OptOutDate__c.getTime()) : null;
                consentWrapper.division = bufferRecord.CORE_CO1_Division__c;
                consentWrapper.businessUnit = bufferRecord.CORE_CO1_BusinessUnit__c;
                consentWrapper.brand = bufferRecord.CORE_CO1_BusinessUnit__r.CORE_Brand__c;
            }
        }
        
        return consentWrapper;
    }

    public static Boolean isExistingConsentValue(CORE_LeadAcquisitionBuffer__c bufferRecord,Integer consentNumber){
        Boolean isExistingValue = false;
        switch on consentNumber {
            when 2 {
                if(!String.IsEmpty(bufferRecord.CORE_CO2_ConsentType__c) ||
                    !String.IsEmpty(bufferRecord.CORE_CO2_ConsentChannel__c) ||
                    !String.IsEmpty(bufferRecord.CORE_CO2_ConsentDescription__c) ||
                    !String.IsEmpty(bufferRecord.CORE_CO2_Division__c) ||
                    !String.IsEmpty(bufferRecord.CORE_CO2_BusinessUnit__c)){
                        isExistingValue = true;
                }
            }
            when 3 {
                if(!String.IsEmpty(bufferRecord.CORE_CO3_ConsentType__c) ||
                    !String.IsEmpty(bufferRecord.CORE_CO3_ConsentChannel__c) ||
                    !String.IsEmpty(bufferRecord.CORE_CO3_ConsentDescription__c) ||
                    !String.IsEmpty(bufferRecord.CORE_CO3_Division__c) ||
                    !String.IsEmpty(bufferRecord.CORE_CO3_BusinessUnit__c)){
                        isExistingValue = true;
                }
            }
            when 4 {
                if(!String.IsEmpty(bufferRecord.CORE_CO4_ConsentType__c) ||
                    !String.IsEmpty(bufferRecord.CORE_CO4_ConsentChannel__c) ||
                    !String.IsEmpty(bufferRecord.CORE_CO4_ConsentDescription__c) ||
                    !String.IsEmpty(bufferRecord.CORE_CO4_Division__c) ||
                    !String.IsEmpty(bufferRecord.CORE_CO4_BusinessUnit__c)){
                        isExistingValue = true;
                }
            }
            when 5 {
                if(!String.IsEmpty(bufferRecord.CORE_CO5_ConsentType__c) ||
                    !String.IsEmpty(bufferRecord.CORE_CO5_ConsentChannel__c) ||
                    !String.IsEmpty(bufferRecord.CORE_CO5_ConsentDescription__c) ||
                    !String.IsEmpty(bufferRecord.CORE_CO5_Division__c) ||
                    !String.IsEmpty(bufferRecord.CORE_CO5_BusinessUnit__c)){
                        isExistingValue = true;
                }
            }
            when else {
                if(!String.IsEmpty(bufferRecord.CORE_CO1_ConsentType__c) ||
                    !String.IsEmpty(bufferRecord.CORE_CO1_ConsentChannel__c) ||
                    !String.IsEmpty(bufferRecord.CORE_CO1_ConsentDescription__c) ||
                    !String.IsEmpty(bufferRecord.CORE_CO1_Division__c) ||
                    !String.IsEmpty(bufferRecord.CORE_CO1_BusinessUnit__c)){
                        isExistingValue = true;
                }
            }
        }

        return isExistingValue;
    }

    public static Map<Boolean, Account> prepareAccountToUpsert(List<Account> existingAccounts, CORE_LeadFormAcquisitionWrapper.PersonAccountWrapper personAccountWrapper, Boolean countryPicklistIsActivated){
        System.debug('CORE_LFA_AccountManagement.prepareAccountToUpsert() : START');
        Map<Boolean, Account> accountToUpsert = new Map<Boolean, Account>();
        Boolean bIsInsert = true;
        Account account;
        switch on existingAccounts.size() {
            when 0 {
                //New account must be created;
                account = CORE_AccountDataMaker.initPersonAccount(personAccountWrapper,countryPicklistIsActivated);
                bIsInsert = true;
            }
            when else {
                account = CORE_AccountDataMaker.getUpdatedPersonAccount(existingAccounts.get(0), personAccountWrapper, countryPicklistIsActivated);
                bIsInsert = false;
            }
            /*when else {
                account = CORE_AccountDataMaker.getUpdatedPersonAccount(existingAccounts.get(0), personAccountWrapper);
                bIsInsert = false;
            }*/
        }
        accountToUpsert.put(bIsInsert,account);
        System.debug('CORE_LFA_AccountManagement.prepareAccountToUpsert() : END');
        return accountToUpsert;
    }
}