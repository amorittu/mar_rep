/**
 * Created By Fodil
 * Test class for CORE_SISOpportunityApplicationProcess
 */
@IsTest
public with sharing class CORE_SISOppApplicationProcessTest {
    static String requestBodyWithoutConsents = '{ "personAccount":{ "CORE_SISStudentID__c": "123","CORE_GovernmentStudentID__c": "",'+
    '"CORE_Social_Security_Number_Fiscal_Code__pc": "string","CORE_Gender__pc": "CORE_PKL_Female","Salutation": "Mr.", "Firstname": "Fodil New Test","Middlename": "Mid",'+
    '"CORE_Nickname__pc": "string","Lastname": "Plato", "CORE_Birthname__pc": "string","PersonBirthdate": "2022-04-05","CORE_Year_of_birth__pc": "1995", "CORE_Main_Nationality__pc": "ITA",'+
    '"CORE_Main_Passport_Number_ID_Number__c":"999999","CORE_Main_Passport_ID_Date_of_Expiration__c":"2022-12-19", "PersonEmail": "fodil.test+2@ggeedu.com","Phone": "+33668888888","PersonMobilePhone":  "+33777777791",'+
    '"CORE_Language__pc":"FRA", "CORE_Language_website__pc":"ITA","CORE_Foreign_language_1__c":"FRA","CORE_Foreign_language_1_Level__c":"DEU"},"mainInterest":{ "SIS_OpportunityId" :"00000030" ,'+
    '"academicYear": "CORE_PKL_2021-2022","division":"IM","businessUnit": "MD","studyArea": "FSA", "curriculum": "MAMFL", "level": "TCMA1FA", "intake": "SFA1"},'+
    '"opportunity":{"description" : "test description", "CORE_OPP_Credit_Transfer_Number__c":"23333"},'+
    '"acquisitionPointOfContact":{ "createdDate": 1621951200000,"sourceId": "IDAPITEST", "nature": "CORE_PKL_Online","captureChannel": "CORE_PKL_Phone_call", "origin": "TEST API", "campaignMedium": "CORE_PKL_Directory",'+
    '"campaignSource": "source campaign","campaignTerm": "campaign term","device": "CORE_PKL_Computer", "firstVisitDate": "2021-05-19", "firstPage": "https://www.mba-esg.com/master-production-audiovisuelle.html?gclid=CjwKCAjwwbHWBRBWEiwAMIV7E2dd4Vyedrh5-jC1xDkw5dacsDrjtlOuAzYlyFpxZICKfW98nzHG7BoC9PkQAvD_BwE",'+
    '"ipAddress": "192.168.1.1","latitute": "0.132400","longitude": "34.750000", "salesOrigin" : "testOriginSales" }}';
    
    static String requestBodyWithoutConsentsAndNotExistingPA = '{ "personAccount":{ "CORE_SISStudentID__c": "1234","CORE_GovernmentStudentID__c": "",'+
    '"CORE_Social_Security_Number_Fiscal_Code__pc": "string","CORE_Gender__pc": "CORE_PKL_Female","Salutation": "Mr.", "Firstname": "Fodil New Test","Middlename": "Mid",'+
    '"CORE_Nickname__pc": "string","Lastname": "Plato", "CORE_Birthname__pc": "string","PersonBirthdate": "2022-04-05","CORE_Year_of_birth__pc": "1995", "CORE_Main_Nationality__pc": "ITA",'+
    '"CORE_Main_Passport_Number_ID_Number__c":"999999","CORE_Main_Passport_ID_Date_of_Expiration__c":"2022-12-19", "PersonEmail": "fodil.test+3@ggeedu.com","Phone": "+33777777771",'+
    '"CORE_Language__pc":"FRA", "CORE_Language_website__pc":"ITA","CORE_Foreign_language_1__c":"FRA","CORE_Foreign_language_1_Level__c":"DEU"},"mainInterest":{ "SIS_OpportunityId" :"00000030" ,'+
    '"academicYear": "CORE_PKL_2021-2022","division":"IM","businessUnit": "MD","studyArea": "FSA", "curriculum": "MAMFL", "level": "TCMA1FA", "intake": "SFA1"},'+
    '"opportunity":{"description" : "test description", "CORE_OPP_Credit_Transfer_Number__c":"23333"},'+
    '"acquisitionPointOfContact":{ "createdDate": 1621951200000,"sourceId": "IDAPITEST", "nature": "CORE_PKL_Online","captureChannel": "CORE_PKL_Phone_call", "origin": "TEST API", "campaignMedium": "CORE_PKL_Directory",'+
    '"campaignSource": "source campaign","campaignTerm": "campaign term","device": "CORE_PKL_Computer", "firstVisitDate": "2021-05-19", "firstPage": "https://www.mba-esg.com/master-production-audiovisuelle.html?gclid=CjwKCAjwwbHWBRBWEiwAMIV7E2dd4Vyedrh5-jC1xDkw5dacsDrjtlOuAzYlyFpxZICKfW98nzHG7BoC9PkQAvD_BwE",'+
    '"ipAddress": "192.168.1.1","latitute": "0.132400","longitude": "34.750000", "salesOrigin" : "testOriginSales" }}';
    
    static String requestBodyWithConsents = '{ "personAccount":{ "CORE_SISStudentID__c": "123","CORE_GovernmentStudentID__c": "",'+
    '"CORE_Social_Security_Number_Fiscal_Code__pc": "string","CORE_Gender__pc": "CORE_PKL_Female","Salutation": "Mr.", "Firstname": "Fodil New Test","Middlename": "Mid",'+
    '"CORE_Nickname__pc": "string","Lastname": "Plato", "CORE_Birthname__pc": "string","PersonBirthdate": "2022-04-05","CORE_Year_of_birth__pc": "1995", "CORE_Main_Nationality__pc": "ITA",'+
    '"CORE_Main_Passport_Number_ID_Number__c":"999999","CORE_Main_Passport_ID_Date_of_Expiration__c":"2022-12-19", "PersonEmail": "fodil.test+2@ggeedu.com","Phone": "+33668888888",'+
    '"CORE_Language__pc":"FRA", "CORE_Language_website__pc":"ITA","CORE_Foreign_language_1__c":"FRA","CORE_Foreign_language_1_Level__c":"DEU"},"mainInterest":{ "SIS_OpportunityId" :"00000030" ,"productExternalID":"SIS-GGEIT-IM-MIL-Fashion-FD-MA1-2022-09",'+
    '"academicYear": "CORE_PKL_2021-2022","division":"IM","businessUnit": "MD","studyArea": "FSA", "curriculum": "MAMFL", "level": "TCMA1FA", "intake": "SFA1"},'+
    '"consents" : [{"CORE_Consent_type__c": "CORE_PKL_Processing_Collection_Storage_of_data_consent", "CORE_Consent_channel__c": "CORE_PKL_Not_applicable",'+
    ' "CORE_Consent_description__c":"short description","CORE_Opt_in__c": false,"CORE_Opt_in_date__c": 1621961240000,"CORE_Opt_out__c": true,"CORE_Opt_out_date__c": 1621951200000,"CORE_Business_Unit__c": "MD",'+
    '"CORE_Division__c": "IM"}],"opportunity":{"description" : "test description", "CORE_OPP_Credit_Transfer_Number__c":"23333"},'+
    '"acquisitionPointOfContact":{ "createdDate": 1621951200000,"sourceId": "IDAPITEST", "nature": "CORE_PKL_Online","captureChannel": "CORE_PKL_Phone_call", "origin": "TEST API", "campaignMedium": "CORE_PKL_Directory",'+
    '"campaignSource": "source campaign","campaignTerm": "campaign term","device": "CORE_PKL_Computer", "firstVisitDate": "2021-05-19", "firstPage": "https://www.mba-esg.com/master-production-audiovisuelle.html?gclid=CjwKCAjwwbHWBRBWEiwAMIV7E2dd4Vyedrh5-jC1xDkw5dacsDrjtlOuAzYlyFpxZICKfW98nzHG7BoC9PkQAvD_BwE",'+
    '"ipAddress": "192.168.1.1","latitute": "0.132400","longitude": "34.750000", "salesOrigin" : "testOriginSales" }}';

    @TestSetup
    static void makeData(){
        CORE_DataFaker_CatalogHierarchy catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('SIS_Test');
        catalogHierarchy.product.SIS_External_Id__c ='SIS-GGEIT-IM-MIL-Fashion-FD-MA1-2022-09';
        Account account = CORE_DataFaker_Account.getStudentAccount('test');
        account.PersonEmail='test@email.com';
        account.Phone = '+33668888888';
        update account;
        catalogHierarchy.businessUnit.CORE_Business_Unit_ExternalId__c ='MD';
        catalogHierarchy.division.CORE_Division_ExternalId__c = 'IM';
        catalogHierarchy.curriculum.CORE_Curriculum_ExternalId__c = 'MAMFL';
        catalogHierarchy.intake.CORE_Intake_ExternalId__c = 'SFA1';
        catalogHierarchy.level.CORE_Level_ExternalId__c = 'TCMA1FA';
        catalogHierarchy.studyArea.CORE_Study_Area_ExternalId__c = 'FSA';
        
        
        update catalogHierarchy.division;
        update catalogHierarchy.businessUnit;
        update catalogHierarchy.studyArea;
        update catalogHierarchy.curriculum;
        update catalogHierarchy.level;
        update catalogHierarchy.intake;
        update catalogHierarchy.product;       
        

        Opportunity opportunity = CORE_DataFaker_Opportunity.getOpportunity(catalogHierarchy.division.Id, 
        catalogHierarchy.businessUnit.Id, 
        catalogHierarchy.studyArea.Id,
        catalogHierarchy.curriculum.Id,
        catalogHierarchy.level.Id,
        catalogHierarchy.intake.Id, 
        account.Id, 
        'Lead', 
        'Lead - New'
    );
        opportunity.CORE_OPP_Main_Opportunity__c = true;
        opportunity.CORE_Main_Product_Interest__c = catalogHierarchy.product.Id;
        update opportunity;

        Id pricebookId = CORE_DataFaker_PriceBook.getStandardPricebookId();
        CORE_PriceBook_BU__c pricebookBu =  CORE_DataFaker_PriceBook.getPriceBookBu(pricebookId, catalogHierarchy.businessUnit.Id, 'test');
        PricebookEntry pbEntry = CORE_DataFaker_PriceBook.getPriceBookEntry(catalogHierarchy.product.Id, pricebookId, 10);
        OpportunityLineItem oppLineItem = CORE_DataFaker_Opportunity.getOpportunityLineItem(opportunity.Id, pbEntry.Id, 'test');

    }

    @IsTest
    static void testExecute(){
        Map<String,Object> reqBody = (Map<String,Object>) JSON.deserializeUntyped(requestBodyWithoutConsents);
        CORE_SISOpportunityApplicationWrapper.GlobalWrapper reqWrapper = new CORE_SISOpportunityApplicationWrapper.GlobalWrapper();
        reqWrapper.acquisitionPointOfContact = (CORE_SISOpportunityApplicationWrapper.AcquisitionPointOfContactWrapper) JSON.deserialize(JSON.serialize(reqBody.get('acquisitionPointOfContact')), CORE_SISOpportunityApplicationWrapper.AcquisitionPointOfContactWrapper.class);
        reqWrapper.Consents = (List<Object>) reqBody.get('consents');
        reqWrapper.opportunity = (Map<String,Object>) reqBody.get('opportunity');
        reqWrapper.mainInterest = (CORE_SISOpportunityApplicationWrapper.MainInterestWrapper) JSON.deserialize(JSON.serialize(reqBody.get('mainInterest')), CORE_SISOpportunityApplicationWrapper.MainInterestWrapper.class);
        reqWrapper.personAccount = (Map<String,Object>) reqBody.get('personAccount');
        Test.startTest();
        CORE_SISOpportunityApplicationProcess process = new CORE_SISOpportunityApplicationProcess(DateTime.Now(),reqWrapper);
        process.execute();
        Test.stopTest();
        System.assertEquals(0, process.errors.size());
    }
    @IsTest
    static void testExecute_Withoutconsent(){
        Map<String,Object> reqBody = (Map<String,Object>) JSON.deserializeUntyped(requestBodyWithoutConsents);
        CORE_SISOpportunityApplicationWrapper.GlobalWrapper reqWrapper = new CORE_SISOpportunityApplicationWrapper.GlobalWrapper();
        reqWrapper.acquisitionPointOfContact = (CORE_SISOpportunityApplicationWrapper.AcquisitionPointOfContactWrapper) JSON.deserialize(JSON.serialize(reqBody.get('acquisitionPointOfContact')), CORE_SISOpportunityApplicationWrapper.AcquisitionPointOfContactWrapper.class);
        reqWrapper.Consents = (List<Object>) reqBody.get('consents');
        reqWrapper.opportunity = (Map<String,Object>) reqBody.get('opportunity');
        reqWrapper.mainInterest = (CORE_SISOpportunityApplicationWrapper.MainInterestWrapper) JSON.deserialize(JSON.serialize(reqBody.get('mainInterest')), CORE_SISOpportunityApplicationWrapper.MainInterestWrapper.class);
        reqWrapper.personAccount = (Map<String,Object>) reqBody.get('personAccount');
        Test.startTest();
        CORE_SISOpportunityApplicationProcess process = new CORE_SISOpportunityApplicationProcess(DateTime.Now(),reqWrapper);
        process.execute();
        Test.stopTest();
        System.assertEquals(0, process.errors.size());
    }
    @IsTest
    static void testExecute_WithoutconsentandNoExistingAccount(){
        Map<String,Object> reqBody = (Map<String,Object>) JSON.deserializeUntyped(requestBodyWithoutConsentsAndNotExistingPA);
        CORE_SISOpportunityApplicationWrapper.GlobalWrapper reqWrapper = new CORE_SISOpportunityApplicationWrapper.GlobalWrapper();
        reqWrapper.acquisitionPointOfContact = (CORE_SISOpportunityApplicationWrapper.AcquisitionPointOfContactWrapper) JSON.deserialize(JSON.serialize(reqBody.get('acquisitionPointOfContact')), CORE_SISOpportunityApplicationWrapper.AcquisitionPointOfContactWrapper.class);
        reqWrapper.Consents = (List<Object>) reqBody.get('consents');
        reqWrapper.opportunity = (Map<String,Object>) reqBody.get('opportunity');
        reqWrapper.mainInterest = (CORE_SISOpportunityApplicationWrapper.MainInterestWrapper) JSON.deserialize(JSON.serialize(reqBody.get('mainInterest')), CORE_SISOpportunityApplicationWrapper.MainInterestWrapper.class);
        reqWrapper.personAccount = (Map<String,Object>) reqBody.get('personAccount');
        Test.startTest();
        CORE_SISOpportunityApplicationProcess process = new CORE_SISOpportunityApplicationProcess(DateTime.Now(),reqWrapper);
        process.execute();
        Test.stopTest();
        System.assertEquals(0, process.errors.size());
    }
    @IsTest
    static void testGetAccount(){
        
        Map<String,Object> reqBody = (Map<String,Object>) JSON.deserializeUntyped(requestBodyWithoutConsents);
        CORE_SISOpportunityApplicationWrapper.GlobalWrapper reqWrapper = new CORE_SISOpportunityApplicationWrapper.GlobalWrapper();
        reqWrapper.acquisitionPointOfContact = (CORE_SISOpportunityApplicationWrapper.AcquisitionPointOfContactWrapper) JSON.deserialize(JSON.serialize(reqBody.get('acquisitionPointOfContact')), CORE_SISOpportunityApplicationWrapper.AcquisitionPointOfContactWrapper.class);
        reqWrapper.Consents = (List<Object>) reqBody.get('consents');
        reqWrapper.opportunity = (Map<String,Object>) reqBody.get('opportunity');
        reqWrapper.mainInterest = (CORE_SISOpportunityApplicationWrapper.MainInterestWrapper) JSON.deserialize(JSON.serialize(reqBody.get('mainInterest')), CORE_SISOpportunityApplicationWrapper.MainInterestWrapper.class);
        reqWrapper.personAccount = (Map<String,Object>) reqBody.get('personAccount');
        Test.startTest();
        CORE_SISOpportunityApplicationProcess process = new CORE_SISOpportunityApplicationProcess(DateTime.Now(),reqWrapper);
        Account accountNotExisting = process.getAccount(new List<Account>());
        Test.stopTest();
        System.assertEquals('Fodil New Test',accountNotExisting.Firstname);
        
    }
    @IsTest
    static void testGetAccountExisting(){
        
        Map<String,Object> reqBody = (Map<String,Object>) JSON.deserializeUntyped(requestBodyWithoutConsents);
        CORE_SISOpportunityApplicationWrapper.GlobalWrapper reqWrapper = new CORE_SISOpportunityApplicationWrapper.GlobalWrapper();
        reqWrapper.acquisitionPointOfContact = (CORE_SISOpportunityApplicationWrapper.AcquisitionPointOfContactWrapper) JSON.deserialize(JSON.serialize(reqBody.get('acquisitionPointOfContact')), CORE_SISOpportunityApplicationWrapper.AcquisitionPointOfContactWrapper.class);
        reqWrapper.Consents = (List<Object>) reqBody.get('consents');
        reqWrapper.opportunity = (Map<String,Object>) reqBody.get('opportunity');
        reqWrapper.mainInterest = (CORE_SISOpportunityApplicationWrapper.MainInterestWrapper) JSON.deserialize(JSON.serialize(reqBody.get('mainInterest')), CORE_SISOpportunityApplicationWrapper.MainInterestWrapper.class);
        reqWrapper.personAccount = (Map<String,Object>) reqBody.get('personAccount');
        Test.startTest();
        CORE_SISOpportunityApplicationProcess process = new CORE_SISOpportunityApplicationProcess(DateTime.Now(),reqWrapper);
        Account account = process.getAccount([SELECT Id, PersonContactId, CreatedDate, Salutation, LastName, FirstName, Phone, CORE_Phone2__pc, 
        PersonMobilePhone, CORE_Mobile_2__pc, CORE_Mobile_3__pc, PersonEmail,CORE_Email_2__pc,CORE_Email_3__pc, CORE_Student_Email__pc,
        PersonMailingStreet, PersonMailingPostalCode, PersonMailingCity, CORE_Language_website__pc, PersonMailingCountry, PersonMailingState, OwnerId,CORE_GA_User_ID__c,
        CORE_Gender__pc , CORE_Professional_situation__pc FROM Account]);
        Test.stopTest();
        System.assertEquals('Fodil New Test',account.Firstname);
        
    }
    @IsTest
    static void testGetOppLineItems(){
        
        Map<String,Object> reqBody = (Map<String,Object>) JSON.deserializeUntyped(requestBodyWithoutConsents);
        CORE_SISOpportunityApplicationWrapper.GlobalWrapper reqWrapper = new CORE_SISOpportunityApplicationWrapper.GlobalWrapper();
        reqWrapper.acquisitionPointOfContact = (CORE_SISOpportunityApplicationWrapper.AcquisitionPointOfContactWrapper) JSON.deserialize(JSON.serialize(reqBody.get('acquisitionPointOfContact')), CORE_SISOpportunityApplicationWrapper.AcquisitionPointOfContactWrapper.class);
        reqWrapper.Consents = (List<Object>) reqBody.get('consents');
        reqWrapper.opportunity = (Map<String,Object>) reqBody.get('opportunity');
        reqWrapper.mainInterest = (CORE_SISOpportunityApplicationWrapper.MainInterestWrapper) JSON.deserialize(JSON.serialize(reqBody.get('mainInterest')), CORE_SISOpportunityApplicationWrapper.MainInterestWrapper.class);
        reqWrapper.personAccount = (Map<String,Object>) reqBody.get('personAccount');
        Test.startTest();
        CORE_SISOpportunityApplicationProcess process = new CORE_SISOpportunityApplicationProcess(DateTime.Now(),reqWrapper);
        List<OpportunityLineItem> oppLineItems = process.getOppLineItems([SELECT ID, CORE_Main_Product_Interest__c,CORE_OPP_Business_Unit__c,CORE_OPP_Main_Opportunity__c FROM Opportunity  ORDER BY CreatedDate ASC LIMIT 1 ]);
        Test.stopTest();
        System.assertEquals(1,oppLineItems.size());
        
    }
    @IsTest
    static void testChangetoNotMain(){
        Map<String,Object> reqBody = (Map<String,Object>) JSON.deserializeUntyped(requestBodyWithoutConsents);
        CORE_SISOpportunityApplicationWrapper.GlobalWrapper reqWrapper = new CORE_SISOpportunityApplicationWrapper.GlobalWrapper();
        reqWrapper.acquisitionPointOfContact = (CORE_SISOpportunityApplicationWrapper.AcquisitionPointOfContactWrapper) JSON.deserialize(JSON.serialize(reqBody.get('acquisitionPointOfContact')), CORE_SISOpportunityApplicationWrapper.AcquisitionPointOfContactWrapper.class);
        reqWrapper.Consents = (List<Object>) reqBody.get('consents');
        reqWrapper.opportunity = (Map<String,Object>) reqBody.get('opportunity');
        reqWrapper.mainInterest = (CORE_SISOpportunityApplicationWrapper.MainInterestWrapper) JSON.deserialize(JSON.serialize(reqBody.get('mainInterest')), CORE_SISOpportunityApplicationWrapper.MainInterestWrapper.class);
        reqWrapper.personAccount = (Map<String,Object>) reqBody.get('personAccount');
        Test.startTest();
        CORE_SISOpportunityApplicationProcess process = new CORE_SISOpportunityApplicationProcess(DateTime.Now(),reqWrapper);
        List<Opportunity> opps = process.changeToNotMain([SELECT ID, CORE_OPP_Main_Opportunity__c FROM Opportunity  ORDER BY CreatedDate ASC LIMIT 1 ]);
        Test.stopTest();
        System.assertEquals(false,opps[0].CORE_OPP_Main_Opportunity__c);
    }
    @IsTest
    static void testCreateDefaultPointOfContact(){
        
        Map<String,Object> reqBody = (Map<String,Object>) JSON.deserializeUntyped(requestBodyWithoutConsents);
        CORE_SISOpportunityApplicationWrapper.GlobalWrapper reqWrapper = new CORE_SISOpportunityApplicationWrapper.GlobalWrapper();
        reqWrapper.acquisitionPointOfContact = (CORE_SISOpportunityApplicationWrapper.AcquisitionPointOfContactWrapper) JSON.deserialize(JSON.serialize(reqBody.get('acquisitionPointOfContact')), CORE_SISOpportunityApplicationWrapper.AcquisitionPointOfContactWrapper.class);
        reqWrapper.Consents = (List<Object>) reqBody.get('consents');
        reqWrapper.opportunity = (Map<String,Object>) reqBody.get('opportunity');
        reqWrapper.mainInterest = (CORE_SISOpportunityApplicationWrapper.MainInterestWrapper) JSON.deserialize(JSON.serialize(reqBody.get('mainInterest')), CORE_SISOpportunityApplicationWrapper.MainInterestWrapper.class);
        reqWrapper.personAccount = (Map<String,Object>) reqBody.get('personAccount');
        Test.startTest();
        CORE_SISOpportunityApplicationProcess process = new CORE_SISOpportunityApplicationProcess(DateTime.Now(),reqWrapper);
        Opportunity opp = [SELECT ID,AccountId, CORE_OPP_Main_Opportunity__c FROM Opportunity  ORDER BY CreatedDate ASC LIMIT 1 ];
        CORE_Point_Of_Contact__c poc = process.createDefaultPointOfContact(opp);
        Test.stopTest();
        System.assertEquals(opp.Id, poc.CORE_Opportunity__c);
        
    }
    @IsTest
    static void testCreateDefaultConsents(){
        
        Map<String,Object> reqBody = (Map<String,Object>) JSON.deserializeUntyped(requestBodyWithoutConsents);
        CORE_SISOpportunityApplicationWrapper.GlobalWrapper reqWrapper = new CORE_SISOpportunityApplicationWrapper.GlobalWrapper();
        reqWrapper.acquisitionPointOfContact = (CORE_SISOpportunityApplicationWrapper.AcquisitionPointOfContactWrapper) JSON.deserialize(JSON.serialize(reqBody.get('acquisitionPointOfContact')), CORE_SISOpportunityApplicationWrapper.AcquisitionPointOfContactWrapper.class);
        reqWrapper.Consents = (List<Object>) reqBody.get('consents');
        reqWrapper.opportunity = (Map<String,Object>) reqBody.get('opportunity');
        reqWrapper.mainInterest = (CORE_SISOpportunityApplicationWrapper.MainInterestWrapper) JSON.deserialize(JSON.serialize(reqBody.get('mainInterest')), CORE_SISOpportunityApplicationWrapper.MainInterestWrapper.class);
        reqWrapper.personAccount = (Map<String,Object>) reqBody.get('personAccount');
        Test.startTest();
        CORE_SISOpportunityApplicationProcess process = new CORE_SISOpportunityApplicationProcess(DateTime.Now(),reqWrapper);
        Opportunity opp = [SELECT ID,AccountId,CORE_OPP_Division__r.CORE_Division_ExternalId__c,CORE_OPP_Business_Unit__r.CORE_Business_Unit_ExternalId__c, CORE_OPP_Main_Opportunity__c FROM Opportunity  ORDER BY CreatedDate ASC LIMIT 1 ];
        Account acct = [SELECT ID FROM Account  ORDER BY CreatedDate ASC LIMIT 1 ];
        List<CORE_Consent__c> consents = process.createDefaultConsents(acct,opp);
        Test.stopTest();
        System.assertEquals(2, consents.size());
        
    }
    @IsTest
    static void testInitConsentsFromAPI(){
        Map<String,Object> reqBody = (Map<String,Object>) JSON.deserializeUntyped(requestBodyWithConsents);
        CORE_SISOpportunityApplicationWrapper.GlobalWrapper reqWrapper = new CORE_SISOpportunityApplicationWrapper.GlobalWrapper();
        reqWrapper.acquisitionPointOfContact = (CORE_SISOpportunityApplicationWrapper.AcquisitionPointOfContactWrapper) JSON.deserialize(JSON.serialize(reqBody.get('acquisitionPointOfContact')), CORE_SISOpportunityApplicationWrapper.AcquisitionPointOfContactWrapper.class);
        reqWrapper.Consents = (List<Object>) reqBody.get('consents');
        reqWrapper.opportunity = (Map<String,Object>) reqBody.get('opportunity');
        reqWrapper.mainInterest = (CORE_SISOpportunityApplicationWrapper.MainInterestWrapper) JSON.deserialize(JSON.serialize(reqBody.get('mainInterest')), CORE_SISOpportunityApplicationWrapper.MainInterestWrapper.class);
        reqWrapper.personAccount = (Map<String,Object>) reqBody.get('personAccount');
        Test.startTest();
        CORE_SISOpportunityApplicationProcess process = new CORE_SISOpportunityApplicationProcess(DateTime.Now(),reqWrapper);
        process.execute();
        Opportunity opp = [SELECT ID,AccountId,CORE_OPP_Division__r.CORE_Division_ExternalId__c,CORE_OPP_Business_Unit__r.CORE_Business_Unit_ExternalId__c, CORE_OPP_Main_Opportunity__c FROM Opportunity  ORDER BY CreatedDate ASC LIMIT 1 ];
        Account acct = [SELECT ID FROM Account  ORDER BY CreatedDate ASC LIMIT 1 ];
        List<CORE_Consent__c> consents = process.initConsentsFromAPI(acct,reqWrapper.Consents);
        Test.stopTest();
        System.assertEquals(1, consents.size());
    }
}