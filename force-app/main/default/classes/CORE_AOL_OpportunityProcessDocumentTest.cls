@isTest
private class CORE_AOL_OpportunityProcessDocumentTest {

    private static final String AOL_EXTERNALID = 'setupAol';

    private static final String REQ_BODY = '{'
    + '"aolExternalId": "' + AOL_EXTERNALID + '",'
    + '"documentSendingFields": {'
    + '"documentSendingChoice": "Email"'
    + '},'
    + '"additionalFields": {'
    + '"CORE_Campaign_Name__c": "test value"'
    + '},'
    + '"aolProgressionFields": {'
    + '"aolProgression": 50.09,'
    + '"documentProgression": "Started",'
    + '"firstDocumentUploadDate": "2022-03-29",'
    + '"lastAolEventDate": "2022-03-29T14:33:54.152Z",'
    + '"lastDeaAolLoginDate": "2022-03-29T14:33:54.152Z",'
    + '"isAol": true,'
    + '"isAolOnlineRegistered": true,'
    + '"isAolRetailRegistered": true'
    + '}'
    + '}';

    private static final String REQ_BODY_ERROR_NO_ID = '{'
    + '"aolExternalId": "",'
    + '"documentSendingFields": {'
    + '"documentSendingChoice": "Email"'
    + '},'
    + '"additionalFields": {'
    + '"CORE_Campaign_Name__c": "test value"'
    + '},'
    + '"aolProgressionFields": {'
    + '"aolProgression": 50.09,'
    + '"documentProgression": "Started",'
    + '"firstDocumentUploadDate": "2022-03-29",'
    + '"lastAolEventDate": "2022-03-29T14:33:54.152Z",'
    + '"lastDeaAolLoginDate": "2022-03-29T14:33:54.152Z",'
    + '"isAol": true,'
    + '"isAolOnlineRegistered": true,'
    + '"isAolRetailRegistered": true'
    + '}'
    + '}';

    private static final String REQ_BODY_ERROR_INVALID_ID = '{'
    + '"aolExternalId": "invalidID",'
    + '"documentSendingFields": {'
    + '"documentSendingChoice": "Email"'
    + '},'
    + '"additionalFields": {'
    + '"CORE_Campaign_Name__c": "test value"'
    + '},'
    + '"aolProgressionFields": {'
    + '"aolProgression": 50.09,'
    + '"documentProgression": "Started",'
    + '"firstDocumentUploadDate": "2022-03-29",'
    + '"lastAolEventDate": "2022-03-29T14:33:54.152Z",'
    + '"lastDeaAolLoginDate": "2022-03-29T14:33:54.152Z",'
    + '"isAol": true,'
    + '"isAolOnlineRegistered": true,'
    + '"isAolRetailRegistered": true'
    + '}'
    + '}';

    private static final String REQ_BODY_ERROR = '{'
    + '"aolExternalId": "' + AOL_EXTERNALID + '",'
    + '"documentSendingFields": {'
    + '"documentSendingChoice": "wrong_value"'
    + '},'
    + '"additionalFields": {'
    + '"CORE_Campaign_Name__c": "test value"'
    + '},'
    + '"aolProgressionFields": {'
    + '"aolProgression": 50.09,'
    + '"documentProgression": "Started",'
    + '"firstDocumentUploadDate": "2022-03-29",'
    + '"lastAolEventDate": "2022-03-29T14:33:54.152Z",'
    + '"lastDeaAolLoginDate": "2022-03-29T14:33:54.152Z",'
    + '"isAol": true,'
    + '"isAolOnlineRegistered": true,'
    + '"isAolRetailRegistered": true'
    + '}'
    + '}';

    @TestSetup
    static void makeData(){
        CORE_CountrySpecificSettings__c countrySetting = new CORE_CountrySpecificSettings__c(
                CORE_FieldPrefix__c = 'CORE_'
        );
        database.insert(countrySetting, true);

        CORE_CatalogHierarchyModel catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test').catalogHierarchy;
        Account account = CORE_DataFaker_Account.getStudentAccount('test');
        Opportunity  opportunity = CORE_DataFaker_Opportunity.getAOLOpportunity(catalogHierarchy.division.Id,
                catalogHierarchy.businessUnit.Id,
                account.Id,
                CORE_AOL_Constants.PKL_OPP_STAGENAME_PROSPECT,
                CORE_AOL_Constants.PKL_OPP_SUBSTATUS_PROSPECTAPPSTARTED,
                AOL_EXTERNALID
        );
    }

    @IsTest
    static void opportunityProcessDocumentSending_Should_Update_Opportunity_and_return_it() {
        CORE_AOL_Wrapper.AOLProgressionFields aolProgressionFields= CORE_DataFaker_AOL_Wrapper.getAolProgressionFieldsWrapper();
        CORE_AOL_Wrapper.DocumentSendingFields documentSendingFields = CORE_DataFaker_AOL_Wrapper.getProcessDocumentWrapper();

        CORE_AOL_GlobalWorker globalWorker = new CORE_AOL_GlobalWorker(aolProgressionFields,AOL_EXTERNALID);

        Opportunity opportunity = CORE_AOL_OpportunityProcessDocument.opportunityProcessDocumentProcess(globalWorker, documentSendingFields);
        database.update(opportunity, true);

        opportunity = [SELECT Id,CORE_Document_sending_process_choice__c
        FROM Opportunity];

        System.assertEquals(documentSendingFields.documentSendingChoice, opportunity.CORE_Document_sending_process_choice__c);
    }

    @IsTest
    static void execute_should_return_an_error_if_mandatory_missing(){

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/aol/opportunity/process/document-sending';

        req.httpMethod = 'PUT';
        req.requestBody = Blob.valueOf(REQ_BODY_ERROR_NO_ID);

        RestContext.request = req;
        RestContext.response= res;

        CORE_AOL_Wrapper.ResultWrapperGlobal result = CORE_AOL_OpportunityProcessDocument.execute();

        System.assertEquals('KO', result.status);

    }

    @IsTest
    static void execute_should_return_an_error_if_externalId_wrong(){

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/aol/opportunity/process/document-sending';

        req.httpMethod = 'PUT';
        req.requestBody = Blob.valueOf(REQ_BODY_ERROR_INVALID_ID);

        RestContext.request = req;
        RestContext.response= res;

        CORE_AOL_Wrapper.ResultWrapperGlobal result = CORE_AOL_OpportunityProcessDocument.execute();

        System.assertEquals('KO', result.status);

    }

    @IsTest
    static void execute_should_return_an_error_if_something_goes_wrong(){

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/aol/opportunity/process/document-sending';

        req.httpMethod = 'PUT';
        req.requestBody = Blob.valueOf(REQ_BODY_ERROR);

        RestContext.request = req;
        RestContext.response= res;

        CORE_AOL_Wrapper.ResultWrapperGlobal result = CORE_AOL_OpportunityProcessDocument.execute();

        System.assertEquals('KO', result.status);

    }

    @IsTest
    static void execute_should_return_a_success_if_all_updates_are_ok(){

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/aol/opportunity/process/document-sending';

        req.httpMethod = 'PUT';
        req.requestBody = Blob.valueOf(REQ_BODY);

        RestContext.request = req;
        RestContext.response= res;

        CORE_AOL_Wrapper.ResultWrapperGlobal result = CORE_AOL_OpportunityProcessDocument.execute();

        System.assertEquals('OK', result.status);
    }
}