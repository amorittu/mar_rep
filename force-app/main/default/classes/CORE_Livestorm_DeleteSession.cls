public class CORE_Livestorm_DeleteSession  implements Database.Batchable<SObject>, Database.AllowsCallouts {
    Set<Id> campaignIds;

    public CORE_Livestorm_DeleteSession(Set<Id> c){
        System.debug('CORE_Livestorm_DeleteSession : START');
        campaignIds = c;
    }

    public Database.QueryLocator start(Database.BatchableContext bc){
        return Database.getQueryLocator([SELECT Id, CORE_Livestorm_Event_Id__c, CORE_Livestorm_Session_Id__c, CORE_Business_Unit__c
        FROM Campaign where Id in :this.campaignIds]);
    }

    public void execute(Database.BatchableContext bc, List<Campaign> campaigns){
        System.debug('CORE_Livestorm_DeleteSession.execute() : START');

        Map<Id,CORE_Business_Unit__c> orderBusinessUnit = CORE_Livestorm_Helper.getBuFromCampaign(campaigns);

        for(Campaign campaign : campaigns){
            System.debug('Delete session in livestorm for : ' + campaign.CORE_Livestorm_Event_Id__c);

            CORE_Livestorm__mdt livestormMetadata = CORE_Livestorm_Helper.GetMetadata(orderBusinessUnit, campaign);

            if (livestormMetadata.CORE_Is_Livestorm_Active__c){
                try{
                    if (!deleteSession(campaign.CORE_Livestorm_Session_Id__c, livestormMetadata)){
                        System.debug('Session : ' + campaign.CORE_Livestorm_Session_Id__c + ' updated');
                        campaign.CORE_Is_Up_to_Date_With_Livestorm__c = true;
                    }
                    else{
                        campaign.CORE_Is_Up_to_Date_With_Livestorm__c = false;
                    }
                } catch (Exception e){
                    System.debug('The following exception has occurred during session deletion : ' + e.getMessage());
                }
            }
        }
        Database.update(campaigns, false);
        System.debug('CORE_Livestorm_DeleteSession.execute() : END');
    }

    public void finish(Database.BatchableContext bc){
        System.debug('CORE_Livestorm_DeleteSession END.');
    }


    public Boolean deleteSession(String sessionId, CORE_Livestorm__mdt livestormMetadata){
        System.debug('Start deleting Session for ' + sessionId);

        Http http = new Http();
        HttpRequest request = new HttpRequest();

                
        string fullEndPoint = livestormMetadata.CORE_Default_End_Point__c + livestormMetadata.CORE_Delete_Session_End_Point__c;
        fullEndPoint = String.format(fullEndPoint, new List<String>{sessionId});
        
        request.setMethod('DELETE');
        request.setEndpoint(fullEndPoint);
        request.setHeader('Accept', 'application/vnd.api+json');
        request.setHeader('Authorization', livestormMetadata.CORE_Authorization__c);
        request.setHeader('Content-Type', 'application/vnd.api+json');

        Long dt1 = DateTime.now().getTime();
        DateTime dt1Date = DateTime.now();

        HttpResponse response = http.send(request);

        long dt2 = DateTime.now().getTime(); 
        DateTime dt2Date = DateTime.now();
        System.debug('http delete session execution time = ' + (dt2-dt1) + ' ms');

        if(response.getStatusCode() != 204) {
            System.debug('Session : The status code returned was not expected: ' + response.getStatusCode() + ' ' + response.getStatus());
            return true;
        } else {
            return false;
        }
    }
}