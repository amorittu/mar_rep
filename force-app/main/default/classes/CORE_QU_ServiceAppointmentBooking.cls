/**
* @author Zameer Khodabocus - Almavia
* @date 2022-04-12
* @group Queueable
* @description Class used create a videa appointment on Twilio via the API in asynchronous
* @TestClass CORE_ServiceAppointmentTriggerTest
*/
public without sharing class CORE_QU_ServiceAppointmentBooking implements Queueable, Database.AllowsCallouts {

    private Id serviceAppId;

    public CORE_QU_ServiceAppointmentBooking(Id serviceAppId){
        this.serviceAppId = serviceAppId;
    }

    public void execute(QueueableContext context) {
        //Get service appointment
        ServiceAppointment serviceApp = [SELECT Id, ParentRecordId, SchedStartTime, SchedEndTime, CORE_VideoMeetingId__c, 
                                         CORE_VideoMeetingUrl__c, Status, WorkTypeId, WorkType.CORE_Business_Unit__c,
                                         WorkType.CORE_Business_Unit__r.CORE_Phone_numbers__c, ServiceNote
                                         FROM ServiceAppointment
                                         WHERE Id = :serviceAppId LIMIT 1];

        CORE_FLEX_VideoAppointmentBooking.videoAppointmentBookingProcess(serviceApp);
    }

}