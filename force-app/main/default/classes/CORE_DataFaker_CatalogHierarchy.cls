@IsTest
public class CORE_DataFaker_CatalogHierarchy {
    private static final String PKL_ACADEMIC_YEAR = 'CORE_PKL_2023-2024';

    public CORE_Division__c division;
    public CORE_Business_Unit__c businessUnit;
    public CORE_Study_Area__c studyArea;
    public CORE_Curriculum__c curriculum;
    public CORE_Level__c level;
    public CORE_Intake__c intake;
    public Product2 product;
    public CORE_CatalogHierarchyModel catalogHierarchy;

    public CORE_DataFaker_CatalogHierarchy(String uniqueKey){
        createHierarchy(uniqueKey);
    }

    public CORE_DataFaker_CatalogHierarchy(){
    }

    public static List<CORE_DataFaker_CatalogHierarchy> createMultipleCatalogs(String uniqueKey, Integer nbCatalogs){
        Group testGroup = new Group(Name='test group' + uniqueKey, Type='Queue');
        insert testGroup;
        QueuesObject testQueue;
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            testQueue = new QueueSObject(QueueID = testGroup.id, SObjectType = 'Task');
            insert testQueue;
        }

        List<CORE_DataFaker_CatalogHierarchy> catalogHierarchies = new List<CORE_DataFaker_CatalogHierarchy>();
        List<CORE_Division__c> divisions = new List<CORE_Division__c>();
        List<CORE_Business_Unit__c> businessUnits = new List<CORE_Business_Unit__c>();
        List<CORE_Study_Area__c> studyAreas = new List<CORE_Study_Area__c>();
        List<CORE_Curriculum__c> curriculums = new List<CORE_Curriculum__c>();
        List<CORE_Level__c> levels = new List<CORE_Level__c>();
        List<CORE_Intake__c> intakes = new List<CORE_Intake__c>();
        List<Product2> products = new List<Product2>();

        for(Integer i = 0 ; i < nbCatalogs ; i++){
            CORE_Division__c division = getDivision(uniqueKey + i);
            divisions.add(division);
        }
        
        insert divisions;

        for(Integer i = 0 ; i < divisions.size() ; i++){
            CORE_Business_Unit__c businessUnit = getBusinessUnit(divisions.get(i).Id, uniqueKey + i);
            businessUnit.CORE_Telesales_Queue__c = testGroup.Id;
            businessUnits.add(businessUnit);
        }

        insert businessUnits;

        for(Integer i = 0 ; i < businessUnits.size() ; i++){
            CORE_Study_Area__c studyArea = getStudyArea(businessUnits.get(i).Id, uniqueKey + i);
            studyAreas.add(studyArea);
        }

        insert studyAreas;

        for(Integer i = 0 ; i < studyAreas.size() ; i++){
            CORE_Curriculum__c curriculum = getCurriculum(studyAreas.get(i).Id, uniqueKey + i);
            curriculums.add(curriculum);
        }

        insert curriculums;

        for(Integer i = 0 ; i < curriculums.size() ; i++){
            CORE_Level__c level = getLevel(curriculums.get(i).Id, uniqueKey + i);
            levels.add(level);
        }

        insert levels;

        for(Integer i = 0 ; i < levels.size() ; i++){
            CORE_Intake__c intake = getIntake(levels.get(i).Id, uniqueKey + i);
            intakes.add(intake);
        }

        insert intakes;

        for(Integer i = 0 ; i < intakes.size() ; i++){
            Product2 product = getProduct(intakes.get(i).Id, uniqueKey + i);
            products.add(product);
        }

        insert products;

        for(Integer i = 0 ; i < nbCatalogs ; i++){
            CORE_DataFaker_CatalogHierarchy catalogHierarchyTmp = new CORE_DataFaker_CatalogHierarchy();
            catalogHierarchyTmp.division = divisions.get(i);
            catalogHierarchyTmp.businessUnit = businessUnits.get(i);
            catalogHierarchyTmp.studyArea = studyAreas.get(i);
            catalogHierarchyTmp.curriculum = curriculums.get(i);
            catalogHierarchyTmp.level = levels.get(i);
            catalogHierarchyTmp.intake = intakes.get(i);
            catalogHierarchyTmp.product = products.get(i);

            catalogHierarchyTmp.catalogHierarchy = new CORE_CatalogHierarchyModel();
            catalogHierarchyTmp.catalogHierarchy.division = catalogHierarchyTmp.division;
            catalogHierarchyTmp.catalogHierarchy.businessUnit = catalogHierarchyTmp.businessUnit;
            catalogHierarchyTmp.catalogHierarchy.studyArea = catalogHierarchyTmp.studyArea;
            catalogHierarchyTmp.catalogHierarchy.curriculum = catalogHierarchyTmp.curriculum;
            catalogHierarchyTmp.catalogHierarchy.level = catalogHierarchyTmp.level;
            catalogHierarchyTmp.catalogHierarchy.intake = catalogHierarchyTmp.intake;
            catalogHierarchyTmp.catalogHierarchy.product = catalogHierarchyTmp.product;
            
            catalogHierarchies.add(catalogHierarchyTmp);
        }

        return catalogHierarchies;
    }

    public CORE_DataFaker_CatalogHierarchy(String uniqueKey, DateTime createdDate, DateTime lastModifiedDate){
        createHierarchy(uniqueKey, createdDate, lastModifiedDate);
    }

    
    private void createHierarchy(String uniqueKey, DateTime createdDate, DateTime lastModifiedDate) {
        Group testGroup = new Group(Name='test group', Type='Queue');
        insert testGroup;
        QueuesObject testQueue;
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            testQueue = new QueueSObject(QueueID = testGroup.id, SObjectType = 'Task');
            insert testQueue;
        }

        CORE_Division__c division = getDivision(uniqueKey);
        division.createdDate = createdDate;
        division.LastModifiedDate = lastModifiedDate;
        insert division;
        this.division = division;

        CORE_Business_Unit__c businessUnit = getBusinessUnit(this.division.Id, uniqueKey);
        businessUnit.createdDate = createdDate;
        businessUnit.LastModifiedDate = lastModifiedDate;
        businessUnit.CORE_Telesales_Queue__c = testGroup.Id;
        Insert businessUnit;
        this.businessUnit = businessUnit;

        CORE_Study_Area__c studyArea = getStudyArea(this.businessUnit.Id, uniqueKey);
        studyArea.createdDate = createdDate;
        studyArea.LastModifiedDate = lastModifiedDate;
        Insert studyArea;
        this.studyArea = studyArea;

        CORE_Curriculum__c curriculum = getCurriculum(this.studyArea.Id, uniqueKey);
        curriculum.createdDate = createdDate;
        curriculum.LastModifiedDate = lastModifiedDate;
        Insert curriculum;
        this.curriculum = curriculum;

        CORE_Level__c level =  getLevel(this.curriculum.Id, uniqueKey);
        level.createdDate = createdDate;
        level.LastModifiedDate = lastModifiedDate;
        Insert level;
        this.level = level;
        
        CORE_Intake__c intake = getIntake(this.level.Id, uniqueKey);
        intake.createdDate = createdDate;
        intake.LastModifiedDate = lastModifiedDate;
        Insert intake;
        this.intake = intake;

        Product2 product = getProduct(this.intake.Id, uniqueKey);
        Insert product;
        this.product = product;

        this.catalogHierarchy = new CORE_CatalogHierarchyModel();
        this.catalogHierarchy.division = this.division;
        this.catalogHierarchy.businessUnit = this.businessUnit;
        this.catalogHierarchy.studyArea = this.studyArea;
        this.catalogHierarchy.curriculum = this.curriculum;
        this.catalogHierarchy.level = this.level;
        this.catalogHierarchy.intake = this.intake;
        this.catalogHierarchy.product = this.product;
    }

    
    private void createHierarchy(String uniqueKey) {
        Group testGroup = new Group(Name='test group' + uniqueKey, Type='Queue');
        insert testGroup;
        QueuesObject testQueue;
        System.runAs(new User(Id=UserInfo.getUserId()))
        {
            testQueue = new QueueSObject(QueueID = testGroup.id, SObjectType = 'Task');
            insert testQueue;
        }

        CORE_Division__c division = getDivision(uniqueKey);
        insert division;
        this.division = division;

        CORE_Business_Unit__c businessUnit = getBusinessUnit(this.division.Id, uniqueKey);
        businessUnit.CORE_Telesales_Queue__c = testGroup.Id;
        Insert businessUnit;
        this.businessUnit = businessUnit;

        CORE_Study_Area__c studyArea = getStudyArea(this.businessUnit.Id, uniqueKey);

        Insert studyArea;
        this.studyArea = studyArea;

        CORE_Curriculum__c curriculum = getCurriculum(this.studyArea.Id, uniqueKey);
        Insert curriculum;
        this.curriculum = curriculum;

        CORE_Level__c level =  getLevel(this.curriculum.Id, uniqueKey);
        Insert level;
        this.level = level;
        
        CORE_Intake__c intake = getIntake(this.level.Id, uniqueKey);
        Insert intake;
        this.intake = intake;

        Product2 product = getProduct(this.intake.Id, uniqueKey);
        Insert product;
        this.product = product;

        this.catalogHierarchy = new CORE_CatalogHierarchyModel();
        this.catalogHierarchy.division = this.division;
        this.catalogHierarchy.businessUnit = this.businessUnit;
        this.catalogHierarchy.studyArea = this.studyArea;
        this.catalogHierarchy.curriculum = this.curriculum;
        this.catalogHierarchy.level = this.level;
        this.catalogHierarchy.intake = this.intake;
        this.catalogHierarchy.product = this.product;
    }

    private static CORE_Division__c getDivision(String uniqueKey){
        CORE_Division__c division = new CORE_Division__c(
            Name = 'Division - test ' + uniqueKey,
            CORE_Division_code__c = uniqueKey,
            CORE_Division_ExternalId__c = uniqueKey,
            CORE_Default_Academic_Year__c = PKL_ACADEMIC_YEAR
        );

        return division;
    }

    private static CORE_Business_Unit__c getBusinessUnit(Id divisionId, String uniqueKey){
        CORE_Business_Unit__c businessUnit = new CORE_Business_Unit__c(
            Name = 'BusinessUnit - test ' + uniqueKey,
            CORE_Business_Unit_code__c = uniqueKey,
            CORE_Business_Unit_ExternalId__c = uniqueKey,
            CORE_Parent_Division__c = divisionId,
            CORE_BU_Technical_User__c = UserInfo.getUserId(),
            CORE_Brand__c = 'CORE_PKL_NABA',
            CORE_Generic_contact_email__c = 'test' + uniqueKey + '@test.fr',
            CORE_Phone_numbers__c = '+33757597171'
        );

        return businessUnit;
    }

    private static CORE_Study_Area__c getStudyArea(Id businessUnitId, String uniqueKey){
        CORE_Study_Area__c studyArea = new CORE_Study_Area__c(
            Name = 'StudyArea - test ' + uniqueKey,
            CORE_Study_Area_code__c = uniqueKey,
            CORE_Study_Area_ExternalId__c = uniqueKey,
            CORE_Parent_Business_Unit__c = businessUnitId
        );

        return studyArea;
    }

    private static CORE_Curriculum__c getCurriculum(Id studyAreaId, String uniqueKey){
        CORE_Curriculum__c curriculum = new CORE_Curriculum__c(
            Name = 'Curriculum - test ' + uniqueKey,
            CORE_Curriculum_Name__c = 'Curriculum - test ' + uniqueKey,
            CORE_IsActive__c = true,
            CORE_Study_Mode__c = 'CORE_PKL_Conventional',
            CORE_Curriculum_code__c = uniqueKey,
            CORE_Curriculum_ExternalId__c = uniqueKey,
            CORE_Parent_Study_Area__c = studyAreaId
        );

        return curriculum;
    }

    private static CORE_Level__c getLevel(Id curriculumId, String uniqueKey){
        CORE_Level__c level = new CORE_Level__c(
            Name = 'Level - test ' + uniqueKey,
            CORE_Year__c = 'CORE_PKL_BA_1',
            CORE_Study_Cycle__c = 'CORE_PKL_BTS',
            CORE_Level_code__c = uniqueKey,
            CORE_Level_ExternalId__c = uniqueKey,
            CORE_Parent_Curriculum__c = curriculumId
        );

        return level;
    }

    private static CORE_Intake__c getIntake(Id levelId, String uniqueKey){
        CORE_Intake__c intake = new CORE_Intake__c(
            Name = 'Intake - test ' + uniqueKey,
            CORE_Intake_code__c = uniqueKey,
            CORE_Intake_ExternalId__c = uniqueKey,
            CORE_Parent_Level__c = levelId,
            CORE_Academic_Year__c = PKL_ACADEMIC_YEAR,
            CORE_Session__c = 'October'

        );

        return intake;
    }

    private static Product2 getProduct(Id intakeId, String uniqueKey){
        Product2 product = new Product2(
            Name = 'Product - test ' + uniqueKey,
            CORE_IntakeID__c = intakeId,
            CORE_Product_Type__c = 'CORE_PKL_Main_Program',
            ProductCode = uniqueKey,
            IsActive = true
        );

        return product;
    }

}