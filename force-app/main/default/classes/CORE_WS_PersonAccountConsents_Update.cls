@RestResource(urlMapping='/core/consent')
global class CORE_WS_PersonAccountConsents_Update {

    public static Integer limitPerPage = 2000;
    

    @HttpPut
    global static CORE_WS_Wrapper.ResultWrapperConsents updateConsents(){

        RestRequest	request = RestContext.request;
        system.debug('@updateConsents > request: ' + request);
        String paId = request.params.get(CORE_WS_Constants.PARAM_PA_ID);
        Map<String, Object> paramMap = (Map<String, Object>)JSON.deserializeUntyped(request.requestBody.toString());
        List<Object> paramsList = (List<Object>)JSON.deserializeUntyped((String)JSON.serialize(paramMap.get('consents')));
        CORE_WS_Wrapper.ResultWrapperConsents result = checkMandatoryParameters('SalesforceConsentID',paramsList);
        system.debug('@updateConsents > result: ' + result);
        if(result != null){
            return result;
        }

        try{
            List<CORE_Consent__c> consentsUpdateList = new List<CORE_Consent__c>();
            for(Object paramObj : paramsList){
                Map<String, Object> paramsMap = (Map<String, Object>)JSON.deserializeUntyped((String)JSON.serialize(paramObj));
                String salesforceId = (String)paramsMap.get('SalesforceConsentID');
                
                CORE_Consent__c consent = new CORE_Consent__c();
                system.debug('@updatePAConsents > consent: ' + consent);

                consent.Id = salesforceId;
                consent = (paramsMap.containsKey('CORE_Consent_description__c')) ? (CORE_Consent__c)CORE_WS_GlobalWorker.assignValue(consent, 'CORE_Consent_description__c', paramsMap.get('CORE_Consent_description__c')) :consent;
                consent = (paramsMap.containsKey('CORE_Opt_in__c')) ? (CORE_Consent__c)CORE_WS_GlobalWorker.assignValue(consent, 'CORE_Opt_in__c', paramsMap.get('CORE_Opt_in__c')) :consent;
                consent = (paramsMap.containsKey('CORE_Opt_in_date__c')) ? (CORE_Consent__c)CORE_WS_GlobalWorker.assignValue(consent, 'CORE_Opt_in_date__c', paramsMap.get('CORE_Opt_in_date__c')) :consent;
                consent = (paramsMap.containsKey('CORE_Opt_out__c')) ? (CORE_Consent__c)CORE_WS_GlobalWorker.assignValue(consent, 'CORE_Opt_out__c', paramsMap.get('CORE_Opt_out__c')) :consent;
                consent = (paramsMap.containsKey('CORE_Opt_out_date__c')) ? (CORE_Consent__c)CORE_WS_GlobalWorker.assignValue(consent, 'CORE_Opt_out_date__c', paramsMap.get('CORE_Opt_out_date__c')) :consent;
                consent = (paramsMap.containsKey('CORE_Double_Opt_in_date__c')) ? (CORE_Consent__c)CORE_WS_GlobalWorker.assignValue(consent, 'CORE_Double_Opt_in_date__c', paramsMap.get('CORE_Double_Opt_in_date__c')) :consent;
                consentsupdateList.add(consent);
            }

            system.debug('@updatePAConsents > consentsupdateList: ' + JSON.serializePretty(consentsupdateList));

            database.update(consentsupdateList, true);

            result = new CORE_WS_Wrapper.ResultWrapperConsents(consentsupdateList.size(), null, consentsupdateList, CORE_WS_Constants.SUCCESS_STATUS);

        } catch(Exception e){
            system.debug('@updatePAConsents > Exception: ' + e.getMessage() + ' at ' + e.getStackTraceString());
            result = new CORE_WS_Wrapper.ResultWrapperConsents(CORE_WS_Constants.ERROR_STATUS, e.getMessage() + ' at ' + e.getStackTraceString());
        }

        return result;
    }

    private static CORE_WS_Wrapper.ResultWrapperConsents checkMandatoryParameters(String mandparam ,List<Object> paramsList){
        List<String> missingParameters = new List<String>();
        for(Object paramObj : paramsList){
            Map<String, Object> paramsMap = (Map<String, Object>)JSON.deserializeUntyped((String)JSON.serialize(paramObj));
            if(paramsMap.containsKey(mandparam)
                   && paramsMap.get(mandparam) != null
                   && paramsMap.get(mandparam) != ''){

                    continue;
                }
                else{
                    missingParameters.add(mandparam);
                }
            }
        return getMissingParameters(missingParameters);
    }

    public static CORE_WS_Wrapper.ResultWrapperConsents getMissingParameters(List<String> missingParameters){
        String errorMsg = CORE_WS_Constants.MSG_MISSING_PARAMETERS;

        if(missingParameters.size() > 0){
            errorMsg += missingParameters;

            return new CORE_WS_Wrapper.ResultWrapperConsents(CORE_WS_Constants.ERROR_STATUS, errorMsg);
        }

        return null;
    }
}