/*************************************************************************************************************
 * @name			IT_EmailMessageQueuable
 * @author			Andrea Morittu <amorittu@atlantic-technologies.com>
 * @created			22 / 04 / 2022
 * @description		class invoked by IT_EmailMessageTriggerHandler. It will call all the flow related to Further contact by email
 *
 * Changes (version)
 * -----------------------------------------------------------------------------------------------------------
 * 				No.		Date			Author					Description
 * 				----	------------	--------------------	----------------------------------------------
 * @version		1.0		2022-04-22		Andrea Morittu			Creation
 *
**************************************************************************************************************/
public class IT_EmailMessageQueuable implements Queueable  {
    
    List<Opportunity> opportunityList = new List<Opportunity>();
    Map<String, EmailMessage> relatedToId_EmailMessageMap = new Map<String, EmailMessage>();
    Map<Id, Account> relatedAccounts = new Map<Id, Account>();
    
    /*********************************************************************************************************
     * @name			IT_EmailMessageQueuable()
     * @author			Andrea Morittu <amorittu@atlantic-technologies.com>
     * @created			22 / 04 / 2022
     * @description		constructor of IT_EmailMessageQueuable
     * @param			it will populate all the variable needed to execute method
     * @return			NA
    **********************************************************************************************************/
    public IT_EmailMessageQueuable(List<Opportunity> opportunityList, Map<Id, Account> relatedAccounts, Map<String, EmailMessage> relatedToId_EmailMessageMap) {
        this.opportunityList = opportunityList;
        this.relatedAccounts = relatedAccounts;
        this.relatedToId_EmailMessageMap = relatedToId_EmailMessageMap;
    }

    /*********************************************************************************************************
     * @name			execute
     * @author			Andrea Morittu <amorittu@atlantic-technologies.com>
     * @created			22 / 04 / 2022
     * @description		It will call all the flow related to Further contact by email
     * @param			QueueableContext context : (NOT USED)
     * @return			Void
    **********************************************************************************************************/
    public void execute(QueueableContext context) {
        // Commented to reduce number of SOQL (Unused) - 21/06/2022
        // BusinessHours[] defaultBH = [SELECT id FROM BusinessHours WHERE isDefault = TRUE ];

        if (! opportunityList.isEmpty() ) {
            for (Opportunity opportunity : opportunityList) {
                Map<String,Object> flowParameters = new Map<String,Object>();
                flowParameters.put('incomingOpportunity', opportunity);
                flowParameters.put('incomingAccount', relatedAccounts.get(opportunity.AccountId));
                flowParameters.put('InboundEmailMessage', relatedToId_EmailMessageMap.get(opportunity.Id));
                
                System.debug('flowParameters are :  ' + JSON.serialize(flowParameters) );
                if (!Test.isRunningTest() && opportunity != null  ) {
                    Flow.Interview.IT_COAInboundEmailFurtherContactStep1 coaFlow = new Flow.Interview.IT_COAInboundEmailFurtherContactStep1(flowParameters);
                    coaFlow.start();
                }
                IT_EmailMessageTriggerHandler.fireFurtherContact = false; // static variable to block a potential recursivity on the trigger
            }
        }
    }
}