public without sharing class CORE_AOL_GlobalWorker {
    public CORE_AOL_Wrapper.AOLProgressionFields aolProgressionFields;
    @TestVisible
    private String aolExternalId;
    public Opportunity aolOpportunity {get;set;}
    public Boolean updateOppIsNeeded {get;set;}
    public Map<String, Object> paramsMap { get; set; }

    public static CORE_CountrySpecificSettings__c countrySetting { get; set; }

    public CORE_AOL_GlobalWorker(CORE_AOL_Wrapper.AOLProgressionFields aolProgressionFields, String aolExternalId) {
        this.aolProgressionFields = aolProgressionFields;
        this.aolExternalId = aolExternalId;
        this.aolOpportunity = getAolOpportunity();
        this.updateOppIsNeeded = false;
    }

    public CORE_AOL_GlobalWorker(CORE_AOL_Wrapper.AOLProgressionFields aolProgressionFields, String aolExternalId, Map<String, Object> paramsMap) {
        this.aolProgressionFields = aolProgressionFields;
        this.aolExternalId = aolExternalId;
        this.aolOpportunity = getAolOpportunity();
        this.paramsMap = paramsMap;
        this.updateOppIsNeeded = false;
    }

    @TestVisible
    private Opportunity getAolOpportunity(){
        List<Opportunity> opportunities = CORE_AOL_QueryProvider.getAolOpportunities(this.aolExternalId);

        Opportunity opportunity;
        if(opportunities.size() == 1){
            opportunity = opportunities.get(0);
        }
        return opportunity;
    }

    public void updateAolProgressionFields(){
        if(this.aolOpportunity == null){
            System.debug('@aolProgressionProcess > opportunity not found with externalId');
            return;
        }
        Boolean updateProgression = false;

        Opportunity oppToUpdate = getAolProgressionFields(this.aolOpportunity, this.aolProgressionFields);

        if(oppToUpdate != null){
            updateProgression = true;
        } else {
            oppToUpdate = this.aolOpportunity;
        }
        
        if(updateOppIsNeeded || updateProgression){
            update oppToUpdate;
        }
    }

    public static Opportunity getAolProgressionFields(Opportunity opportunity, CORE_AOL_Wrapper.AOLProgressionFields aolProgressionFields){
        Boolean updateProgression = false;

        if(aolProgressionFields != null){
            opportunity.CORE_OPP_Application_Online_Advancement__c = aolProgressionFields.aolProgression;
            opportunity.CORE_Document_progression__c = aolProgressionFields.documentProgression;
            opportunity.CORE_First_document_upload_date__c = aolProgressionFields.firstDocumentUploadDate;
            opportunity.CORE_Last_AOL_event_date__c = aolProgressionFields.lastAolEventDate;
            opportunity.CORE_Last_DEA_AOL_login_date__c = aolProgressionFields.lastDeaAolLoginDate;
            if(aolProgressionFields.isAol != null){
                opportunity.CORE_OPP_Application_Online__c = aolProgressionFields.isAol;
            }
    
            if(aolProgressionFields.isAolOnlineRegistered != null){
                opportunity.CORE_Is_AOL_Register__c = aolProgressionFields.isAolOnlineRegistered;
            }
    
            if(aolProgressionFields.isAolRetailRegistered != null){
                opportunity.CORE_Is_AOL_Retail_Register__c = aolProgressionFields.isAolRetailRegistered;
            }

            updateProgression = true;
        }

        if(updateProgression){
            return opportunity;
        } else {
            return null;
        }
    }

    public Account getRelatedAccount(){
        Account account;

        if(this.aolOpportunity != null){
            account = this.aolOpportunity.Account;
            /*account.Salutation = opportunity.Account.Salutation;
            account.FirstName = opportunity.Account.FirstName;
            account.LastName = opportunity.Account.LastName;
            account.PersonBirthdate = opportunity.Account.PersonBirthdate; 
            account.CORE_Main_Nationality__pc = opportunity.Account.CORE_Main_Nationality__pc;
            account.PersonMailingStreet = opportunity.Account.PersonMailingStreet;
            account.PersonMailingCity = opportunity.Account.PersonMailingCity;
            account.PersonMailingState = opportunity.Account.PersonMailingState;
            account.PersonMailingCountry = opportunity.Account.PersonMailingCountry;*/
        }

        return account;
    }

    public static Opportunity getExistingMainOpp(Id accountId,String businessUnit, String academicYear){
        Opportunity mainOpportunity;

        List<Opportunity> mainOpportunities = CORE_AOL_QueryProvider.getExistingMainOpps(accountId, businessUnit, academicYear);

        if(mainOpportunities.size() > 0){
            mainOpportunity = mainOpportunities.get(0);
        }

        return mainOpportunity;
    }

    public static CORE_Intake__c getIntakeFromExternalId(String intakeExternalId){
        CORE_Intake__c intake;

        if(String.isBlank(intakeExternalId)){
            return intake;
        }
        List<CORE_Intake__c> intakes = [SELECT Id, CORE_Intake_ExternalId__c, CORE_Academic_Year__c
            FROM CORE_Intake__c 
            WHERE CORE_Intake_ExternalId__c = :intakeExternalId];

        if(intakes.size() > 0){
            intake = intakes.get(0);
        }

        return intake;
    }

    public static CORE_AOL_Wrapper.ResultWrapperGlobal getMissingParameters(List<String> missingParameters){
        String errorMsg = CORE_AOL_Constants.MSG_MISSING_PARAMETERS;

        if(missingParameters != null && !missingParameters.isEmpty()){
            errorMsg += missingParameters;

            return new CORE_AOL_Wrapper.ResultWrapperGlobal(CORE_AOL_Constants.ERROR_STATUS, errorMsg);
        }
        
        return null;
    }

    public static Opportunity removeUniqueFields(Opportunity opportunity){
        opportunity.CORE_Application_Fee_Transaction_Id__c = null;
        opportunity.CORE_Lead_Source_External_Id__c = null;
        opportunity.CORE_OPP_AOL_Opportunity_Id__c = null;
        opportunity.CORE_OPP_Application_Online_ID__c = null;
        opportunity.CORE_OPP_Legacy_CRM_Id__c = null;
        opportunity.CORE_OPP_SIS_Opportunity_Id__c = null;
        opportunity.CORE_OPP_SIS_Student_Academic_Record_ID__c = null;
        opportunity.CORE_Tuition_fee_Transaction_ID__c = null;

        return opportunity;
    }

    public static Boolean isAolExternalIdParameterMissing(String aolExternalId){
        if(String.IsBlank(aolExternalId)){
            return true;
        } else {
            return false;
        }
    }

    public static sObject assignValue(sObject obj, String field, Object val){
        system.debug('@assignValue > field: ' + field);
        system.debug('@assignValue > val: ' + val);

        try{
            obj.put(field, String.valueOf(val));
        }
        catch(Exception e0){
            system.debug('@assignValue e0: ' + e0.getMessage() + ' at ' + e0.getStackTraceString());
            try{
                obj.put(field, Boolean.valueOf(val));
            }
            catch(Exception e1){
                system.debug('@assignValue e1: ' + e1.getMessage() + ' at ' + e1.getStackTraceString());
                try{
                    obj.put(field, Date.valueOf((String)val));
                }
                catch(Exception e2){
                    system.debug('@assignValue e2: ' + e2.getMessage() + ' at ' + e2.getStackTraceString());

                    try{
                        obj.put(field, Datetime.valueOf(((String)val).replace('T',' ')));
                    }
                    catch(Exception e3){
                        system.debug('@assignValue e3: ' + e3.getMessage() + ' at ' + e3.getStackTraceString());

                        try{
                            obj.put(field, Double.valueOf(val));
                        }
                        catch(Exception e4){
                            system.debug('@assignValue e4: ' + e4.getMessage() + ' at ' + e4.getStackTraceString());

                            TypeException excep = new TypeException();
                            excep.setMessage('INVALID TYPE FOR FIELD "'+ field + '"');
                            throw excep;
                        }
                    }
                }
            }
        }

        return obj;
    }

    public static sObject setAdditionalFields(sObject currentRecord, String requestBody){

        if(countrySetting == null){
            countrySetting = CORE_CountrySpecificSettings__c.getOrgDefaults();
        }

        Map<String, Object> requestBodyMap = (Map<String, Object>)JSON.deserializeUntyped(requestBody);
        system.debug('requestBodyMap: ' + JSON.serializePretty(requestBodyMap));

        if(requestBodyMap.containsKey(CORE_AOL_Constants.PARAM_ADDITIONAL_FIELDS)){
            Map<String, Object> fieldValueMap = (Map<String, Object>)JSON.deserializeUntyped(
                (String)JSON.serialize(requestBodyMap.get(CORE_AOL_Constants.PARAM_ADDITIONAL_FIELDS), true)
            );

            for(String param : fieldValueMap.keySet()){
                if(countrySetting != null
                    && countrySetting.CORE_FieldPrefix__c != null
                    && countrySetting.CORE_FieldPrefix__c != CORE_AOL_Constants.PREFIX_CORE
                    && param.startsWith(countrySetting.CORE_FieldPrefix__c)){

                    currentRecord = assignValue(currentRecord, param, fieldValueMap.get(param));
                }
            }
        }

        return currentRecord;
    }
}