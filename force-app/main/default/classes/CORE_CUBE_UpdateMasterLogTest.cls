@IsTest
public with sharing class CORE_CUBE_UpdateMasterLogTest {
    @isTest
    public static void updateMasterLogTest(){
        DateTime todaysDate = Date.today();
        DateTime todaysTime = DateTime.now();

        String todaysDateStr = todaysDate.format('_yyyy_MM_dd');
        DateTime yesterday = DateTime.Now().AddDays(-1);

        Test.startTest();
        CORE_CUBE_Master_Log__c myMasterLog = new CORE_CUBE_Master_Log__c();
        myMasterLog.ErrorOn__c='';
        myMasterLog.Last_Success__c=yesterday;
        myMasterLog.Last_Try__c=yesterday;
        myMasterLog.IsSuccess__c = true;
        insert myMasterLog;
        CORE_CUBE_Log__c myLog = new CORE_CUBE_Log__c();
        myLog.ObjectName__c ='Account';
        myLog.isPassed__c=true;
        insert myLog;
        CORE_CUBE_Master_Log__c myMasterLogGet=[SELECT Id,ErrorOn__c,Last_Success__c,Last_Try__c,IsSuccess__c FROM CORE_CUBE_Master_Log__c];
        CORE_CUBE_UpdateMasterLog.updateMasterLog(myMasterLogGet);
        CORE_CUBE_Master_Log__c myMasterLogTest = [SELECT Id,ErrorOn__c,Last_Success__c,Last_Try__c,IsSuccess__c FROM CORE_CUBE_Master_Log__c];
        Test.stopTest();

        Timezone tz = UserInfo.getTimeZone();//.Timezone.getTimeZone(city);
        Integer offset = tz.getOffset(todaysDate);
        DateTime localDate = todaysDate.addSeconds(-(offset/1000));

        System.assertEquals(myMasterLogTest.IsSuccess__c,true);
        System.assertEquals(myMasterLogTest.ErrorOn__c,null);
        System.assertEquals(myMasterLogTest.Last_Success__c.date(),localDate.date());
        System.assertEquals(myMasterLogTest.Last_Success__c.hour(),localDate.hour());
        System.assertEquals(myMasterLogTest.Last_Success__c.minute(),localDate.minute());
        System.assertEquals(myMasterLogTest.Last_Try__c.date(),todaysTime.date());
        System.assertEquals(myMasterLogTest.Last_Try__c.hour(),todaysTime.hour());
        System.assertEquals(myMasterLogTest.Last_Try__c.minute(),todaysTime.minute());

    }

    @isTest
    public static void updateMasterLogErrorTest(){
        DateTime todaysDate = Date.today();
        DateTime todaysTime = DateTime.now();

        String todaysDateStr = todaysDate.format('_yyyy_MM_dd');
        DateTime yesterday = DateTime.Now().AddDays(-1);

        Test.startTest();
        CORE_CUBE_Master_Log__c myMasterLog = new CORE_CUBE_Master_Log__c();
        myMasterLog.ErrorOn__c='';
        myMasterLog.Last_Success__c=yesterday;
        myMasterLog.Last_Try__c=yesterday;
        myMasterLog.IsSuccess__c = true;
        insert myMasterLog;
        CORE_CUBE_Log__c myLog = new CORE_CUBE_Log__c();
        myLog.ObjectName__c ='Account';
        myLog.isPassed__c=false;
        insert myLog;
        CORE_CUBE_Master_Log__c myMasterLogGet=[SELECT Id,ErrorOn__c,Last_Success__c,Last_Try__c,IsSuccess__c FROM CORE_CUBE_Master_Log__c];
        CORE_CUBE_UpdateMasterLog.updateMasterLog(myMasterLogGet);
        CORE_CUBE_Master_Log__c myMasterLogTest = [SELECT Id,ErrorOn__c,Last_Success__c,Last_Try__c,IsSuccess__c FROM CORE_CUBE_Master_Log__c];
        Test.stopTest();

        System.assertEquals(myMasterLogTest.IsSuccess__c,false);
        System.assertEquals(myMasterLogTest.ErrorOn__c,'Account,');
        System.assertEquals(myMasterLogTest.Last_Success__c.date(),yesterday.date());
        System.assertEquals(myMasterLogTest.Last_Success__c.hour(),yesterday.hour());
        System.assertEquals(myMasterLogTest.Last_Success__c.minute(),yesterday.minute());
        System.assertEquals(myMasterLogTest.Last_Try__c.date(),todaysTime.date());
        System.assertEquals(myMasterLogTest.Last_Try__c.hour(),todaysTime.hour());
        System.assertEquals(myMasterLogTest.Last_Try__c.minute(),todaysTime.minute());
    }
}