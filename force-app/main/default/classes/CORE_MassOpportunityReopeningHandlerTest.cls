/**
* @author Fodil BOUDJEDIEN - Almavia
* @date 2022-03-25
* @group Opportunity Mass re-registration (Single) et (Massive)
* @description Test class of CORE_MassOpportunityReopeningHandler ;
* @test class 
*/

@isTest
public with sharing class CORE_MassOpportunityReopeningHandlerTest {
    @TestSetup
    static void makeData(){
        CORE_DataFaker_CatalogHierarchy catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test');
        Account account = CORE_DataFaker_Account.getStudentAccount('test');
        Opportunity Opportunity = CORE_DataFaker_Opportunity.getOpportunity(catalogHierarchy.division.Id, 
            catalogHierarchy.businessUnit.Id, 
            catalogHierarchy.studyArea.Id,
            catalogHierarchy.curriculum.Id,
            catalogHierarchy.level.Id,
            catalogHierarchy.intake.Id,
            account.Id, 
            'Registered', 
            'Registered - Classical'
        );
        Opportunity opp = new Opportunity(id=Opportunity.Id,CORE_OPP_Academic_Year__c = 'CORE_PKL_2021-2022');
        update opp;

        CORE_OpportunityDataMaker dataMaker = new CORE_OpportunityDataMaker();
        String captureChannel = 'CORE_PKL_Emailings';
        String nature = 'CORE_PKL_Online';
        String origin = 'Website';
        CORE_Point_of_Contact__c result = dataMaker.initPocDefaultValues(opp,nature,captureChannel,origin);
        insert result;
    }

    @isTest
    public static void testGetNextAcademicYear(){
        System.assertEquals('CORE_PKL_2022-2023', CORE_MassOpportunityReopeningHandler.getNextAcademicYear('CORE_PKL_2021-2022'));
    }

    @isTest
    public static void testGetQuertAndHandleOpportunities(){
        CORE_Business_Unit__c bu = [SELECT Id FROM CORE_Business_Unit__c Limit 1];
        Opportunity opp = [SELECT Id, AccountId, CORE_OPP_Business_Unit__c, CORE_OPP_Academic_Year__c FROM Opportunity LIMIT 1 ];

        Opportunity retrievedOpp = Database.query(CORE_MassOpportunityReopeningHandler.getMassOpportunityReregistrationQuery(bu.Id, 'CORE_PKL_2021-2022', ''));

        List<Opportunity> reregisteredOpportunities = CORE_MassOpportunityReopeningHandler.handleOpportunitiesReregistration('CORE_PKL_2021-2022', new List<Opportunity>{retrievedOpp}, bu.Id);
        List<CORE_Point_of_Contact__c> pointsOfContact= [SELECT Id FROM CORE_Point_of_Contact__c];
        System.assertEquals(reregisteredOpportunities.size(), 1);
        System.assertEquals(pointsOfContact.size(), 2);

    }
    @isTest
    public static void testGetQuertAndHandleFailedYearOpportunities(){
        CORE_Business_Unit__c bu = [SELECT Id FROM CORE_Business_Unit__c Limit 1];
        Opportunity opp = [SELECT Id, AccountId, CORE_OPP_Business_Unit__c, CORE_OPP_Academic_Year__c FROM Opportunity LIMIT 1 ];
        opp.CORE_Failed_Year__c = true;
        update opp;
        Opportunity retrievedOpp = Database.query(CORE_MassOpportunityReopeningHandler.getMassOpportunityReregistrationQuery(bu.Id, 'CORE_PKL_2021-2022', ''));

        List<Opportunity> reregisteredOpportunities = CORE_MassOpportunityReopeningHandler.handleOpportunitiesReregistration('CORE_PKL_2021-2022', new List<Opportunity>{retrievedOpp}, bu.Id);
        List<CORE_Point_of_Contact__c> pointsOfContact= [SELECT Id FROM CORE_Point_of_Contact__c];
        System.assertEquals(reregisteredOpportunities.size(), 1);
        System.assertEquals(pointsOfContact.size(), 2);

    }
}