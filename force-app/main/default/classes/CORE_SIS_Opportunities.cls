@RestResource(urlMapping='/sis/opportunities')
global class CORE_SIS_Opportunities {

    public static Integer limitPerPage = 2000;

    @HttpGet
    global static CORE_SIS_Wrapper.ResultWrapperOpportunities getOpportunities(){

        // Initiating logs
        Long dt1 = DateTime.now().getTime();
        Long dt2;
        CORE_LogManager logSettings;
        CORE_LOG__c log = new CORE_LOG__c(CORE_API_Name__c = 'SIS');
        Boolean isErrorLog = false;
        logSettings = new CORE_LogManager('SIS');

        String urlCallback = '';
        RestRequest	request = RestContext.request;
        String lastDate = request.params.get(CORE_SIS_Constants.PARAM_LASTDATE);
        String oppStatus = request.params.get(CORE_SIS_Constants.PARAM_OPPSTATUS);

        List<String> oppStatusList = CORE_SIS_GlobalWorker.splitValues(oppStatus);

        String oppRecTypeName = request.params.get(CORE_SIS_Constants.PARAM_OPPRECTYPE);
        String additionalFields = request.params.get(CORE_SIS_Constants.PARAM_ADD_FIELDS);
        String lastId = request.params.get(CORE_SIS_Constants.PARAM_LAST_ID);

        CORE_SIS_Wrapper.ResultWrapperOpportunities result = checkMandatoryParameters(lastDate, oppRecTypeName, oppStatusList);

        try{
            if(result != null){
                return result;
            }
            List<Opportunity> oppsList = CORE_SIS_QueryProvider.getOpportunityList(lastDate, oppStatusList, oppRecTypeName, additionalFields, lastId, limitPerPage);
            system.debug('@updateOpportunity > oppsList: ' + JSON.serializePretty(oppsList));
            if(oppsList.size() == limitPerPage){
                urlCallback = URL.getOrgDomainUrl()+'/services/apexrest/sis/opportunities?'
                            + CORE_SIS_Constants.PARAM_LASTDATE + '=' + lastDate 
                            + '&' + CORE_SIS_Constants.PARAM_OPPSTATUS + '=' + oppStatus
                            + '&' + CORE_SIS_Constants.PARAM_OPPRECTYPE + '=' + oppRecTypeName 
                            + '&' + CORE_SIS_Constants.PARAM_LAST_ID + '=' + oppsList.get(oppsList.size() - 1).Id;
            }

            result = new CORE_SIS_Wrapper.ResultWrapperOpportunities(oppsList.size(), urlCallback, oppsList, CORE_SIS_Constants.SUCCESS_STATUS);

        } catch(Exception e){
            system.debug('@getOpportunities > exception error: ' + e.getMessage() + ' at ' + e.getStackTraceString());
            result = new CORE_SIS_Wrapper.ResultWrapperOpportunities(CORE_SIS_Constants.ERROR_STATUS, e.getMessage() + ' at ' + e.getStackTraceString());
        } finally{
            dt2 = DateTime.now().getTime();
            System.debug('CORE_AOL_AcademicDiplomaHistory(urlMapping=\'/aol/academic-diploma-history\') : T0TAL TIME OF EXECUTION = ' + (dt2-dt1) + ' ms');
            // creating log
            log.CORE_Processing_Duration__c =(dt2-dt1);
            log.CORE_HTTP_Status_Code__c = result.status == 'KO' ? 400 : result.status == 'OK' ? 200 : 500;
            log.CORE_Sent_Body__c = JSON.serialize(result);
            log.CORE_Received_Body__c =  request?.requestBody?.toString();
            
            if(!logSettings.isNone()){
                if(logSettings.isAllLogged()){ // is All logged
                    CORE_LogManager.createLog(log);
                }else if(logSettings.isErrorLogged() && isErrorLog){ // is Error
                    CORE_LogManager.createLog(log);
                }
            }
        }

        return result;
    }

    private static CORE_SIS_Wrapper.ResultWrapperOpportunities checkMandatoryParameters(String lastDate,String recordType, List<String> opportunityStatus){
        List<String> missingParameters = new List<String>();
        Boolean isMissingLastDate = CORE_SIS_GlobalWorker.isParameterMissing(lastDate);
        Boolean isMissingRecordType = CORE_SIS_GlobalWorker.isParameterMissing(recordType);
        Boolean isMissingOpportunityStatus = CORE_SIS_GlobalWorker.isParameterMissing(opportunityStatus);

        if(isMissinglastDate){
            missingParameters.add(CORE_SIS_Constants.PARAM_LASTDATE);
        }
        if(isMissingRecordType){
            missingParameters.add(CORE_SIS_Constants.PARAM_OPPRECTYPE);
        }

        if(isMissingOpportunityStatus){
            missingParameters.add(CORE_SIS_Constants.PARAM_OPPSTATUS);
        }
        
        return getMissingParameters(missingParameters);
    }

    public static CORE_SIS_Wrapper.ResultWrapperOpportunities getMissingParameters(List<String> missingParameters){
        String errorMsg = CORE_SIS_Constants.MSG_MISSING_PARAMETERS;

        if(missingParameters.size() > 0){
            errorMsg += missingParameters;

            return new CORE_SIS_Wrapper.ResultWrapperOpportunities(CORE_AOL_Constants.ERROR_STATUS, errorMsg);
        }
        
        return null;
    }
}