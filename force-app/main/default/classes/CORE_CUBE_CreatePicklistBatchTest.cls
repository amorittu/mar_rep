@IsTest
public with sharing class CORE_CUBE_CreatePicklistBatchTest {

    @isTest
    public static void executeTest() {
        CORE_CUBE_CreatePicklistBatch testCoreCube = new CORE_CUBE_CreatePicklistBatch();
        List<CORE_CUBE_Picklist__c> myPicklistBefore = [SELECT Id FROM CORE_CUBE_Picklist__c];
        SchedulableContext ctx;
        Test.startTest();
        testCoreCube.execute(ctx);
        Test.stopTest();
        List<CORE_CUBE_Picklist__c> myPicklist = [SELECT Id FROM CORE_CUBE_Picklist__c];
        System.assertNotEquals(0, myPicklist.size());
    }
}