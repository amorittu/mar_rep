/**
 * Created by dgagliardi on 27/05/2022.
 */

@IsTest
private class IT_ReportExternalAgenciesControllerTest {
	@IsTest (SeeAllData=true)
	static void testBehavior() {
		IT_ReportExternalAgenciesController obj = new IT_ReportExternalAgenciesController();
		obj.execute(null);
	}

	@IsTest
	static void testSchedule() {
		Test.startTest();
		IT_ReportExternalAgenciesController myClass = new IT_ReportExternalAgenciesController();
		String chron = '0 0 23 * * ?';
		System.schedule('Test Sched', chron, myClass);
		Test.stopTest();
	}


}