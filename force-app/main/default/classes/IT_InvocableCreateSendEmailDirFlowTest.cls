/**
 * Created by Gabriele Vitanza on 15/11/2021.
 */

@IsTest
private class IT_InvocableCreateSendEmailDirFlowTest {
    @TestSetup
    static void testSetup() {
        CORE_DataFaker_CatalogHierarchy catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test1');
        Account account = CORE_DataFaker_Account.getStudentAccount('test1');

        CORE_Business_Unit__c businessUnit = new CORE_Business_Unit__c();
        businessUnit.CORE_Business_Unit_code__c = '123';
        businessUnit.CORE_Brand__c = 'CORE_PKL_Istituto_Marangoni';
        businessUnit.CORE_BU_Technical_User__c = UserInfo.getUserId();
        businessUnit.CORE_Parent_Division__c = [SELECT Id FROM CORE_Division__c LIMIT 1].Id;
        insert businessUnit;

        Opportunity opportunity = CORE_DataFaker_Opportunity.getOpportunity(
                catalogHierarchy.division.Id,
                catalogHierarchy.businessUnit.Id,
                account.Id,
                'Applicant',
                'Applicant - Application Submitted / New'
        );

        CORE_Study_Area__c studyArea = new CORE_Study_Area__c(Id = catalogHierarchy.studyArea.Id);
        studyArea.CORE_Parent_Business_Unit__c = businessUnit.Id;
        update studyArea;

        opportunity.CORE_OPP_Intake__c = catalogHierarchy.intake.Id;
        opportunity.CORE_OPP_Level__c = catalogHierarchy.level.Id;
        opportunity.CORE_OPP_Curriculum__c = catalogHierarchy.curriculum.Id;
        opportunity.CORE_OPP_Study_Area__c = catalogHierarchy.studyArea.Id;
        opportunity.CORE_Capture_Channel__c = 'CORE_PKL_Web_form';
        opportunity.CORE_OPP_Business_Unit__c = businessUnit.Id;
        update opportunity;

        EmailTemplate e = new EmailTemplate(
                DeveloperName = 'test',
                FolderId = [SELECT Id, Name, DeveloperName FROM Folder WHERE DeveloperName = 'CORE_Email_Templates'].Id,
                TemplateType= 'Text',
                Name = 'test',
                IsActive = true
        );
        System.runAs(new User(Id = UserInfo.getUserId())) {
            insert e;
        }
    }

    @IsTest
    static void testBehavior(){
        IT_InvocableCreateSendEmailDirectFlow.FlowInputs flowInput = new IT_InvocableCreateSendEmailDirectFlow.FlowInputs();
        flowInput.opportunityId = [SELECT Id, CORE_Capture_Channel__c FROM Opportunity LIMIT 1].Id;
        flowInput.templateName = 'test';
        List<IT_InvocableCreateSendEmailDirectFlow.FlowInputs> flowInputs = new List<IT_InvocableCreateSendEmailDirectFlow.FlowInputs>{flowInput};
        IT_InvocableCreateSendEmailDirectFlow.getOutputs(flowInputs);
    }
}