@RestResource(urlMapping='/aol/account/languages')
global class CORE_AOL_AccountLanguages {
    private static final String SUCCESS_MESSAGE = 'Languages & progression fiedls updated with success';
    @HttpPut
    global static CORE_AOL_Wrapper.ResultWrapperGlobal execute() {

        RestRequest	request = RestContext.request;

        // Initiating logs
        Long dt1 = DateTime.now().getTime();
        Long dt2;
        CORE_LogManager logSettings;
        CORE_LOG__c log = new CORE_LOG__c(CORE_API_Name__c = 'AOL');
        Boolean isErrorLog = false;
        logSettings = new CORE_LogManager('AOL');

        Map<String, Object> paramsMap = (Map<String, Object>)JSON.deserializeUntyped(request.requestBody.toString());
        String aolExternalId = (String)paramsMap.get(CORE_AOL_Constants.PARAM_AOL_EXTERNALID);
        CORE_AOL_Wrapper.ForeignLanguages foreignLanguages = (CORE_AOL_Wrapper.ForeignLanguages)JSON.deserialize((String)JSON.serialize(paramsMap.get(CORE_AOL_Constants.PARAM_AOL_ACCOUNT_FOREIGN_LANG)), CORE_AOL_Wrapper.ForeignLanguages.class);
        CORE_AOL_Wrapper.AOLProgressionFields aolProgressionFields = (CORE_AOL_Wrapper.AOLProgressionFields)JSON.deserialize((String)JSON.serialize(paramsMap.get(CORE_AOL_Constants.PARAM_AOL_PROG_FIELDS)), CORE_AOL_Wrapper.AOLProgressionFields.class);

        CORE_AOL_Wrapper.ResultWrapperGlobal result = checkMandatoryParameters(aolExternalId);
        
        try{
            if(result != null){
                return result;
            }
            CORE_AOL_GlobalWorker aolWorker = new CORE_AOL_GlobalWorker(aolProgressionFields,aolExternalId);
            Opportunity opportunity = aolWorker.aolOpportunity;
            if(opportunity == null){
                result = new CORE_AOL_Wrapper.ResultWrapperGlobal(
                    CORE_AOL_Constants.ERROR_STATUS, 
                    String.format(CORE_AOL_Constants.MSG_OPPORTUNITY_NOT_FOUND, new List<String>{aolExternalId})
                );
            } else {
                Account acc = languagesProcess(aolWorker,foreignLanguages);

                //set Additional fields to update
                acc = (Account)CORE_AOL_GlobalWorker.setAdditionalFields(acc, request.requestBody.toString());
                database.update(acc, true);

                aolWorker.updateOppIsNeeded = true;
                aolWorker.updateAolProgressionFields();

                result = new CORE_AOL_Wrapper.ResultWrapperGlobal(CORE_AOL_Constants.SUCCESS_STATUS, SUCCESS_MESSAGE);
            }
            
        } catch(Exception e){
            result = new CORE_AOL_Wrapper.ResultWrapperGlobal(CORE_AOL_Constants.ERROR_STATUS, e.getMessage() + ' at ' + e.getStackTraceString());
        } finally{
            dt2 = DateTime.now().getTime();
            System.debug('CORE_AOL_AcademicDiplomaHistory(urlMapping=\'/aol/academic-diploma-history\') : T0TAL TIME OF EXECUTION = ' + (dt2-dt1) + ' ms');
            // creating log
            log.CORE_Processing_Duration__c =(dt2-dt1);
            log.CORE_HTTP_Status_Code__c = result.status == 'KO' ? 400 : result.status == 'OK' ? 200 : 500;
            log.CORE_Sent_Body__c = JSON.serialize(result);
            log.CORE_Received_Body__c =  request.requestBody.toString();
            
            if(!logSettings.isNone()){
                if(logSettings.isAllLogged()){ // is All logged
                    CORE_LogManager.createLog(log);
                }else if(logSettings.isErrorLogged() && isErrorLog){ // is Error
                    CORE_LogManager.createLog(log);
                }
            }
        }

        return result;
    }

    public static Account languagesProcess(CORE_AOL_GlobalWorker aolWorker,
        CORE_AOL_Wrapper.ForeignLanguages foreignLanguages){

        Account account = aolWorker.getRelatedAccount();

        if(!String.IsBlank(foreignLanguages.language1)){
            account.CORE_Foreign_language_1__c = foreignLanguages.language1;
        }

        if(!String.IsBlank(foreignLanguages.language1Level)){
            account.CORE_Foreign_language_1_Level__c = foreignLanguages.language1Level;
        }

        if(!String.IsBlank(foreignLanguages.language2)){
            account.CORE_Foreign_language_2__c = foreignLanguages.language2;
        }

        if(!String.IsBlank(foreignLanguages.language2Level)){
            account.CORE_Foreign_language_2_Level__c = foreignLanguages.language2Level;
        }
        return account;
    }


    private static CORE_AOL_Wrapper.ResultWrapperGlobal checkMandatoryParameters(String aolExternalId){
        List<String> missingParameters = new List<String>();
        Boolean isMissingAolId = CORE_AOL_GlobalWorker.isAolExternalIdParameterMissing(aolExternalId);

        if(isMissingAolId){
            missingParameters.add(CORE_AOL_Constants.PARAM_AOL_EXTERNALID);
        }
        
        return CORE_AOL_GlobalWorker.getMissingParameters(missingParameters);
    }
}