/* SIS Opportunity Applicant Utils
    Created by : Fodil Boudjedien
    Created Date : 12 april 2022
    Group SIS Opportunity Applicant Utils
*/

public class CORE_SISOpportunityApplicationUtils {
    public static final Set<String> consentsFieldsSet = new Set<String> {
        'CORE_Consent_type__c','CORE_Consent_channel__c','CORE_Consent_description__c','CORE_Opt_in__c','CORE_Opt_in_date__c','CORE_Opt_out__c',
        'CORE_Opt_out_date__c','CORE_Division__c','CORE_Business_Unit__c'
    };
         
    public static List<Account> getAccountsFromIds(Set<Id> ids){
        List<Account> accounts = [SELECT Id, PersonContactId, CreatedDate, Salutation, LastName, FirstName, Phone,CORE_Phone2__pc, 
            PersonMobilePhone, CORE_Mobile_2__pc, CORE_Mobile_3__pc, PersonEmail,CORE_Email_2__pc,CORE_Email_3__pc, CORE_Student_Email__pc,
            PersonMailingStreet, PersonMailingPostalCode, PersonMailingCity, CORE_Language_website__pc, PersonMailingCountry, PersonMailingState, OwnerId,
            CORE_GA_User_ID__c, CORE_Gender__pc , CORE_Professional_situation__pc
            FROM Account WHERE ID in :ids];

        return accounts;
    }

    public static Account getAccountFromId(Id id){
        List<Account> accounts = getAccountsFromIds(new Set<Id>{id});
        Account acc;
        if(!accounts.isEmpty()){
            acc = accounts.get(0);
        }

        return acc;
    }

    public static List<Account> getExistingAccounts(String email,String phoneNumber){
        System.debug('CORE_SISOpportunityApplicationUtils.getExestingAccounts() : START');
        Long dt1 = DateTime.now().getTime();
        Long dt2;
        
        List<Account> accounts = new List<Account>();
        String emailQuerry = email;
        String phone = phoneNumber;

        if(!String.isBlank(emailQuerry)){
            accounts = [SELECT Id, PersonContactId, CreatedDate, Salutation, LastName, FirstName, Phone, CORE_Phone2__pc,
                PersonMobilePhone, CORE_Mobile_2__pc, CORE_Mobile_3__pc, PersonEmail,CORE_Email_2__pc,CORE_Email_3__pc, CORE_Student_Email__pc,
                PersonMailingStreet, PersonMailingPostalCode, PersonMailingCity, CORE_Language_website__pc, PersonMailingCountry, PersonMailingState, OwnerId,CORE_GA_User_ID__c,
                CORE_Gender__pc , CORE_Professional_situation__pc
                FROM Account
                WHERE PersonEmail = :emailQuerry OR CORE_Email_2__pc = :emailQuerry OR CORE_Email_3__pc = :emailQuerry OR CORE_Student_Email__pc = :emailQuerry
                ORDER BY CORE_Last_inbound_contact_date__c DESC NULLS LAST, LastModifiedDate Desc];
        }
        
        if(accounts.isEmpty()){
            if(!String.isBlank(phone)){
                accounts = [SELECT Id, PersonContactId, CreatedDate, Salutation, LastName, FirstName, Phone, CORE_Phone2__pc, 
                        PersonMobilePhone, CORE_Mobile_2__pc, CORE_Mobile_3__pc, PersonEmail,CORE_Email_2__pc,CORE_Email_3__pc, CORE_Student_Email__pc,
                        PersonMailingStreet, PersonMailingPostalCode, PersonMailingCity, CORE_Language_website__pc, PersonMailingCountry, PersonMailingState, OwnerId,CORE_GA_User_ID__c,
                        CORE_Gender__pc , CORE_Professional_situation__pc
                        FROM Account
                        WHERE Phone = :phone OR PersonMobilePhone = :phone OR CORE_Mobile_2__pc = :phone OR CORE_Mobile_3__pc = :phone
                        ORDER BY CORE_Last_inbound_contact_date__c DESC NULLS LAST, LastModifiedDate Desc];
            } 
        }
        
        dt2 = DateTime.now().getTime();
        System.debug('getExestingAccounts time execution = ' + (dt2-dt1) + ' ms');
        System.debug('CORE_SISOpportunityApplicationUtils.getExestingAccounts() : END');
        System.debug('accounts : ' + accounts);

        return accounts;
    }
    public static Account initPersonAccountFromMap(Map<String,Object> mapPersonAccount, Boolean countryPicklistIsActivated){
        System.debug('CORE_SISOpportunityApplicationUtils.initPersonAccountFromMap() : START');
        Long dt1 = DateTime.now().getTime();
        Long dt2;
        Account acct = new Account();
       
        List<String> validFieldsSet = CORE_SIS_QueryProvider.ACC_QUERY_FIELDS.split(',');

        CORE_CountrySpecificSettings__c countrySetting = CORE_CountrySpecificSettings__c.getOrgDefaults();
        for(String param : mapPersonAccount.keySet()){
            if(validFieldsSet.contains(param)
               || (countrySetting != null
                   && countrySetting.CORE_FieldPrefix__c != null
                   && countrySetting.CORE_FieldPrefix__c != CORE_SIS_Constants.PREFIX_CORE
                   && param.startsWith(countrySetting.CORE_FieldPrefix__c))){

                acct = (Account)CORE_SIS_GlobalWorker.assignValue(acct, param, mapPersonAccount.get(param));
            }
        }

        dt2 = DateTime.now().getTime();
        System.debug('initPersonAccountFromMap time execution = ' + (dt2-dt1) + ' ms');
        System.debug('CORE_SISOpportunityApplicationUtils.initPersonAccountFromMap() : END');
        
        return acct;
    }

    public static Opportunity initOpportunityFromMap(Map<String,Object> opportunityMap, Opportunity oppBase){
        System.debug('CORE_SISOpportunityApplicationUtils.initOpportunityFromMapAndInterest() : START');
        
        Opportunity opp = oppBase;
        CORE_CountrySpecificSettings__c countrySetting = CORE_CountrySpecificSettings__c.getOrgDefaults();
        Set<String> specialFields = new Set<String>{'CORE_OPP_Retail_Agency__c','CORE_RetailAgencyBusinessContact__c'};
        Set<String> validFieldsSet = CORE_SIS_QueryProvider.oppsFieldsSet;
        if(opportunityMap.containsKey('CORE_OPP_Retail_Agency__c') && !String.isBlank((String)opportunityMap.get('CORE_OPP_Retail_Agency__c'))){
            List<Account> accounts = [SELECT Id FROM Account where CORE_SISStudentID__c = :String.valueOf(opportunityMap.get('CORE_OPP_Retail_Agency__c'))];
            if(!accounts.isEmpty()){
                opp.CORE_OPP_Retail_Agency__c = accounts[0].Id;
            } else {
                throw new IllegalArgumentException(' CORE_OPP_Retail_Agency__c has no correspondant value in Salesforce');
            }
        }
        
        if(opportunityMap.containsKey('CORE_RetailAgencyBusinessContact__c') && !String.isBlank((String)opportunityMap.get('CORE_RetailAgencyBusinessContact__c'))){
            List<Contact> contacts = [SELECT Id FROM Contact where CORE_LegacyContactCRMID__c = :String.valueOf(opportunityMap.get('CORE_RetailAgencyBusinessContact__c')) ];
            if(!contacts.isEmpty()){
                opp.CORE_RetailAgencyBusinessContact__c = contacts[0].Id;
            } else {
                throw new IllegalArgumentException(' CORE_RetailAgencyBusinessContact__c has no correspondant value in Salesforce');
            }
        }

        for(String param : opportunityMap.keySet()){
            if((validFieldsSet.contains(param) && !specialFields.contains(param))
            || (countrySetting != null
            && countrySetting.CORE_FieldPrefix__c != null
            && countrySetting.CORE_FieldPrefix__c != CORE_SIS_Constants.PREFIX_CORE
            && param.startsWith(countrySetting.CORE_FieldPrefix__c))){
                opp = (Opportunity)CORE_SIS_GlobalWorker.assignValue(opp, param, opportunityMap.get(param));
            }
        }
        System.debug('CORE_SISOpportunityApplicationUtils.initOpportunityFromMapAndInterest() : END');
        return opp;
    }

    public static CORE_Point_of_Contact__c getPocFromWrapper(CORE_Point_Of_Contact__c poc, CORE_SISOpportunityApplicationWrapper.AcquisitionPointOfContactWrapper wrapper){
         if(wrapper.insertionDate != null) poc.createdDate = DateTime.newInstance(long.valueOf(wrapper.insertionDate));
         poc.CORE_Source__c = wrapper.sourceId ;
         if(wrapper.nature != null) poc.CORE_Nature__c = wrapper.nature;
         if(wrapper.captureChannel != null)poc.CORE_Capture_Channel__c = wrapper.captureChannel ;
         if(wrapper.enrollementChannel != null)poc.CORE_Enrollment_Channel__c = wrapper.enrollementChannel ;
         if(wrapper.origin != null) poc.CORE_Origin__c = wrapper.origin ;
         if(wrapper.salesOrigin != null) poc.CORE_Origin_Sales__c = wrapper.salesOrigin ;
         if(wrapper.campaignMedium != null) poc.Campaign_Medium__c = wrapper.campaignMedium ;
         if(wrapper.campaignSource != null) poc.CORE_Campaign_Source__c = wrapper.campaignSource ;
         if(wrapper.campaignTerm != null) poc.CORE_Campaign_Term__c = wrapper.campaignTerm ;
         if(wrapper.campaignContent != null) poc.CORE_Campaign_Content__c = wrapper.campaignContent ;
         if(wrapper.campaignName != null) poc.CORE_Campaign_Name__c = wrapper.campaignName ;
         if(wrapper.device != null) poc.CORE_Device__c = wrapper.device ;
         if(wrapper.firstPage != null) poc.CORE_First_page__c = wrapper.firstPage ;
         if(wrapper.ipAddress != null) poc.CORE_IP_address__c = wrapper.ipAddress ;
         if(wrapper.latitute != null) poc.CORE_Latitude__c = wrapper.latitute ;
         if(wrapper.longitude != null) poc.CORE_Longitude__c = wrapper.longitude ;
         if(wrapper.proactivePrompt != null) poc.CORE_Proactive_Prompt__c = wrapper.proactivePrompt ;
         if(wrapper.proactiveEngaged != null) poc.CORE_Proactive_Engaged__c = wrapper.proactiveEngaged ;
         if(wrapper.chatWith != null) poc.CORE_Chat_With__c = wrapper.chatWith ;
         if(wrapper.insertionMode != null) poc.CORE_Insertion_Mode__c = wrapper.insertionMode ;

        return poc;
    }

    public static CORE_Consent__c initConsentFromMap(Map<String,Object> consentMap,CORE_Consent__c consent){
        System.debug('CORE_SISOpportunityApplicationUtils.initConsentFromMap() : START');
        Set<String> datefields = new Set<String>{
            'CORE_Opt_in_date__c','CORE_Opt_out_date__c'
        };
        CORE_Consent__c cons = Consent;
        CORE_CountrySpecificSettings__c countrySetting = CORE_CountrySpecificSettings__c.getOrgDefaults();

        Set<String> validFieldsSet = CORE_SISOpportunityApplicationUtils.consentsFieldsSet;
        for(String param : consentMap.keySet()){
            if(validFieldsSet.contains(param)
            || (countrySetting != null
            && countrySetting.CORE_FieldPrefix__c != null
            && countrySetting.CORE_FieldPrefix__c != CORE_SIS_Constants.PREFIX_CORE
            && param.startsWith(countrySetting.CORE_FieldPrefix__c))){
                if(param.equalsIgnoreCase('CORE_Opt_in_date__c') || param.equalsIgnoreCase('CORE_Opt_out_date__c') ){
                    cons.put(param,DateTime.newInstance((Long)(consentMap.get(param))));
                }
                else{
                    cons = (CORE_Consent__c)CORE_SIS_GlobalWorker.assignValue(cons, param, consentMap.get(param));
                }
            }
        }
        System.debug('CORE_SISOpportunityApplicationUtils.initConsentFromMap() : END');
        return cons;
    }
  
}