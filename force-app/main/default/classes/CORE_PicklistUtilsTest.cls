@IsTest
public class CORE_PicklistUtilsTest {
    @IsTest
    public static void getPicklistValues_Should_return_list_of_available_api_names_for_picklist_on_an_object() {
        List<String> picklistValuesApiName = new List<String>();

        Schema.DescribeFieldResult fieldResult = Opportunity.StageName.getDescribe();
		List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
		for( Schema.PicklistEntry pickListVal : ple){
			picklistValuesApiName.add(pickListVal.getValue());
		}    
        

        List<String> result = CORE_PicklistUtils.getPicklistValues('Opportunity','StageName');

        System.assertEquals(picklistValuesApiName.size(),result.size());
        for(Integer i = 0 ; i < result.size() ; i++){
            System.assertEquals(picklistValuesApiName.get(i), result.get(i));
        }
        
    }
}