@isTest
public class IT_CampaignMemberTriggerHandlerTest {
	public static final String COA_IDENTIFIER_TEST = 'test_coa';

	@TestSetup
	public static void makeData() {

		CORE_DataFaker_CatalogHierarchy catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test1');
		Account account = CORE_DataFaker_Account.getStudentAccount('test1');
		Opportunity opportunity = CORE_DataFaker_Opportunity.getOpportunity(
				catalogHierarchy.division.Id,
				catalogHierarchy.businessUnit.Id,
				account.Id,
				'Applicant',
				'Applicant - Application Submitted / New'
		);
		//opportunity.Name =COA_IDENTIFIER_TEST;
		opportunity.CORE_OPP_Intake__c = catalogHierarchy.intake.Id;
		opportunity.CORE_OPP_Curriculum__c = catalogHierarchy.curriculum.Id;
		opportunity.CORE_OPP_Study_Area__c = catalogHierarchy.studyArea.Id;
		opportunity.CORE_OPP_Level__c = catalogHierarchy.level.Id;
		opportunity.IT_AdmissionOfficer__c = UserInfo.getUserId();
		update opportunity;
		
		CORE_Point_of_Contact__c poc = CORE_DataFaker_PointOfContact.getPointOfContact(opportunity.Id, account.Id, 'test1');
		poc.CORE_Origin__c = 'ipad';
		poc.CORE_Capture_Channel__c = 'open day';
		update poc;

		Campaign campaign = new Campaign();
		campaign.Name = COA_IDENTIFIER_TEST;
		campaign.CORE_External_Id__c = COA_IDENTIFIER_TEST;
		campaign.Status = 'Planned';
		campaign.IsActive = true;
		campaign.Type = IT_CampaignMemberTriggerHandler.OPEN_DAY_PROCESS;
		insert campaign;

		CampaignMember campaignMember = new CampaignMember();
		campaignMember.CampaignId = campaign.Id;
		campaignMember.ContactId = [SELECT Id FROM Contact WHERE AccountId = :account.Id].Id;
		campaignMember.CORE_Opportunity__c = opportunity.Id;
		insert campaignMember;

	}

	@isTest
	public static void afterInsertTest() {
		testWalkInLogics(); // TK-000487
	}

	/*********************************************************************************************************
	 * @name			The name of your class or method
	 * @author			Andrea Morittu <amorittu@atlantic-technologies.com>
	 * @created			30 / 08 / 2022
	 * @description		Test logics on TK-000487
	**********************************************************************************************************/
	@isTest 
	public static void testWalkInLogics() {
		CORE_DataFaker_CatalogHierarchy catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('IM');
		Account account = CORE_DataFaker_Account.getStudentAccount('test2');
		Opportunity opportunity = CORE_DataFaker_Opportunity.getOpportunity(
				catalogHierarchy.division.Id,
				catalogHierarchy.businessUnit.Id,
				account.Id,
				'Applicant',
				'Applicant - Application Submitted / New'
		);
        System.debug('opportunity ai is : ' + opportunity);
		

		CORE_Point_of_Contact__c poc = CORE_DataFaker_PointOfContact.getPointOfContact(opportunity.Id, account.Id, 'IM');
		poc.CORE_Origin__c = 'ipad';
		poc.CORE_Capture_Channel__c = 'CORE_PKL_Open_Day_JPO_SPO';
		update poc;

		//opportunity.Name =COA_IDENTIFIER_TEST;
		opportunity.CORE_OPP_Intake__c = catalogHierarchy.intake.Id;
		opportunity.CORE_OPP_Curriculum__c = catalogHierarchy.curriculum.Id;
		opportunity.CORE_OPP_Study_Area__c = catalogHierarchy.studyArea.Id;
		opportunity.CORE_OPP_Level__c = catalogHierarchy.level.Id;
		opportunity.IT_AdmissionOfficer__c = UserInfo.getUserId();
		opportunity.CORE_Campaign_Medium__c = 'IT_PKL_internalEmail'; // TO CHANGE|!!!!!!!!
		update opportunity;
        System.debug('opportunity bu is : ' + opportunity);

		Campaign campaign = new Campaign();
		campaign.Name = COA_IDENTIFIER_TEST +  '123';
		campaign.CORE_External_Id__c = COA_IDENTIFIER_TEST + '123';
		campaign.Status = 'Planned';
		campaign.IsActive = true;
		campaign.Type = IT_CampaignMemberTriggerHandler.OPEN_DAY_PROCESS;
		insert campaign;

		CampaignMember campaignMember = new CampaignMember();
		campaignMember.CampaignId = campaign.Id;
		campaignMember.ContactId = [SELECT Id FROM Contact WHERE AccountId = :account.Id].Id;
		campaignMember.CORE_Opportunity__c = opportunity.Id;
		Test.startTest();
			insert campaignMember;
		Test.stopTest();
	}

	@isTest
	public static void afterUpdateTest() {

		Campaign currentTestCampaign = [SELECT id FROM Campaign WHERE Name = :COA_IDENTIFIER_TEST];
		List<CampaignMember> oldCampaignMembers = [SELECT id, CampaignId, CORE_Opportunity__c,CreatedById FROM CampaignMember WHERE CampaignId = :currentTestCampaign.Id LIMIT 2];
		List<CampaignMember> newCampaignMembers = new List<CampaignMember>();
		Map<Id, CampaignMember> mapCampaignMembers = new Map<Id, CampaignMember>();
        Opportunity opportunity = [SELECT Id FROM Opportunity WHERE Id = :oldCampaignMembers[0].CORE_Opportunity__c];
		Test.startTest();
		for (CampaignMember cm : oldCampaignMembers) {
			mapCampaignMembers.put(cm.Id, cm);
			cm.CORE_Opportunity__c = opportunity.Id;
			newCampaignMembers.add(cm);
		}
		update newCampaignMembers;
		IT_CampaignMemberTriggerHandler.performCoaLogics(newCampaignMembers, mapCampaignMembers);
		Test.stopTest();
	}
}