public class CORE_AsyncLFABatch implements Database.Batchable<SObject>{
    String queryFields;
    private Set<Id> recordIds;
    private BUFFER_TYPE bufferType;

    public enum BUFFER_TYPE {STANDARD, AOL, ONLINE_SHOP}

    public CORE_AsyncLFABatch(Set<Id> bufferIds, CORE_AsyncLFABatch.BUFFER_TYPE bufferType){
        this.recordIds = bufferIds;
        this.bufferType = bufferType;
    }

    public Database.QueryLocator start(Database.BatchableContext bc){
        return Database.getQueryLocator([SELECT Id, 
        CORE_PA_CreatedDate__c, CORE_PA_FirstName__c, CORE_PA_GAUserId__c, CORE_PA_LanguageWebsite__c, CORE_PA_LastName__c, CORE_PA_MailingCity__c, 
        CORE_PA_MailingPostalCode__c, CORE_PA_MailingStreet__c, CORE_PA_Phone__c, CORE_PA_Mobile__c, CORE_PA_Salutation__c,
        CORE_PA_MailingState__c, CORE_PA_MailingCountryCode__c, CORE_PA_Email__c, CORE_PA_updateIfNotNull__c,CORE_PA_MainNationality__c, 
        CORE_PA_Professional_situation__c, CORE_PA_Gender__c,CORE_PA_Year_of_Birth__c, CORE_PA_birthDate__c,

        CORE_CO1_BusinessUnit__c, CORE_CO1_BusinessUnit__r.CORE_Brand__c, CORE_CO1_ConsentChannel__c, CORE_CO1_ConsentDescription__c, CORE_CO1_ConsentType__c, CORE_CO1_Division__c, 
        CORE_CO1_OptInDate__c, CORE_CO1_DoubleOptInDate__c, CORE_CO1_OptIn__c, CORE_CO1_OptOutDate__c, CORE_CO1_OptOut__c, 

        CORE_CO2_BusinessUnit__c, CORE_CO2_BusinessUnit__r.CORE_Brand__c, CORE_CO2_ConsentChannel__c, CORE_CO2_ConsentDescription__c, CORE_CO2_ConsentType__c, CORE_CO2_Division__c, 
        CORE_CO2_OptInDate__c, CORE_CO2_DoubleOptInDate__c, CORE_CO2_OptIn__c, CORE_CO2_OptOutDate__c, CORE_CO2_OptOut__c, 

        CORE_CO3_BusinessUnit__c, CORE_CO3_BusinessUnit__r.CORE_Brand__c, CORE_CO3_ConsentChannel__c, CORE_CO3_ConsentDescription__c, CORE_CO3_ConsentType__c, CORE_CO3_Division__c, 
        CORE_CO3_OptInDate__c, CORE_CO3_DoubleOptInDate__c, CORE_CO3_OptIn__c, CORE_CO3_OptOutDate__c, CORE_CO3_OptOut__c, 

        CORE_CO4_BusinessUnit__c, CORE_CO4_BusinessUnit__r.CORE_Brand__c, CORE_CO4_ConsentChannel__c, CORE_CO4_ConsentDescription__c, CORE_CO4_ConsentType__c, CORE_CO4_Division__c, 
        CORE_CO4_OptInDate__c, CORE_CO4_DoubleOptInDate__c, CORE_CO4_OptIn__c, CORE_CO4_OptOutDate__c, CORE_CO4_OptOut__c, 

        CORE_CO5_BusinessUnit__c, CORE_CO5_BusinessUnit__r.CORE_Brand__c, CORE_CO5_ConsentChannel__c, CORE_CO5_ConsentDescription__c, CORE_CO5_ConsentType__c, CORE_CO5_Division__c, 
        CORE_CO5_OptInDate__c, CORE_CO5_DoubleOptInDate__c, CORE_CO5_OptIn__c, CORE_CO5_OptOutDate__c, CORE_CO5_OptOut__c, CORE_CS_CampaignId__c, CORE_CS_CampaignExternalId__c, 
        CORE_Comments__c, 
        
        CORE_MI_ApplicationShippedByMail__c, CORE_MI_BrochureShippedByMail__c, CORE_MI_OPPAcademicYear__c, CORE_MI_OPPTBusiness_Unit__c, 
        CORE_MI_OPPTCurriculum__c,  CORE_MI_OPPTDivision__c, CORE_MI_OPPTIntake__c, CORE_MI_OPPTLevel__c, CORE_MI_OPPTStudy_Area__c, 
        CORE_MI_LeadSourceExternalId__c, CORE_MI_IsSuspect__c, CORE_MI_LearningMaterial__c , CORE_MI_AOL_ExternalId__c, CORE_MI_AOL_IsRetailRegistered__c,
        CORE_MI_OPP_Funding__c,CORE_MI_OnlineShop_ExternalId__c,CORE_MI_Product__c,CORE_MI_Sales_Price__c,CORE_MI_Quantity__c,CORE_MI_Retail_Agency__c,

        CORE_SI_ApplicationShippedByMail__c, CORE_SI_BrochureShippedByMail__c, CORE_SI_OPPAcademicYear__c, CORE_SI_OPPTBusiness_Unit__c, 
        CORE_SI_OPPTCurriculum__c, CORE_SI_OPPTDivision__c, CORE_SI_OPPTIntake__c, CORE_SI_OPPTLevel__c, CORE_SI_OPPTStudy_Area__c, 
        CORE_SI_LeadSourceExternalId__c, CORE_SI_IsSuspect__c, CORE_SI_LearningMaterial__c , CORE_SI_AOL_ExternalId__c, CORE_SI_AOL_IsRetailRegistered__c,
        CORE_SI_OPP_Funding__c,CORE_SI_OnlineShop_ExternalId__c,

        CORE_TI_ApplicationShippedByMail__c, CORE_TI_BrochureShippedByMail__c, CORE_TI_OPPAcademicYear__c, CORE_TI_OPPTBusiness_Unit__c, 
        CORE_TI_OPPTCurriculum__c, CORE_TI_OPPTDivision__c, CORE_TI_OPPTIntake__c, CORE_TI_OPPTLevel__c, CORE_TI_OPPTStudy_Area__c, 
        CORE_TI_LeadSourceExternalId__c, CORE_TI_IsSuspect__c, CORE_TI_LearningMaterial__c , CORE_TI_AOL_ExternalId__c, CORE_TI_AOL_IsRetailRegistered__c,
        CORE_TI_OPP_Funding__c,CORE_TI_OnlineShop_ExternalId__c,

        CORE_POC_CampaignContent__c, CORE_POC_CampaignMedium__c, CORE_POC_CampaignName__c, CORE_POC_CampaignSource__c, CORE_POC_CampaignTerm__c, 
        CORE_POC_CaptureChannel__c, CORE_POC_CreatedDate__c, CORE_POC_FirstPage__c, CORE_POC_FirstVisit__c, CORE_POC_IPAddress__c, 
        CORE_POC_Latitude__c, CORE_POC_Longitude__c, CORE_POC_Nature__c, CORE_POC_Origin__c, CORE_POC_SourceId__c, CORE_POC_SalesOrigin__c, 
        CORE_POC_Device__c, CORE_POC_Proactive_Engaged__c , CORE_POC_Proactive_Prompt__c, CORE_POC_Chat_With__c,CORE_POC_Conversion_page__c,
        CORE_Form__c,CORE_Form_area__c,CORE_GCLID__c,CORE_POC_City_Geoloc__c, CORE_POC_Country_Geoloc__c, CORE_POC_State_Geoloc__c, 
        
        CORE_ADH_Name__c, CORE_ADH_Level__c, 
        
        CORE_MainInterestId__c,CORE_SecondInterestId__c,CORE_ThirdInterestId__c, 
        CORE_MI_PointOfContactId__c, CORE_SI_PointOfContactId__c, CORE_TI_PointOfContactId__c, 
        CORE_Consent1Id__c, CORE_Consent2Id__c, CORE_Consent3Id__c, CORE_Consent4Id__c, CORE_Consent5Id__c,
        CORE_CampaignMemberId__c, 
        CORE_BatchGroupId__c, 
        CORE_Status__c, CORE_ErrorCode__c, CORE_ErrorDescription__c, CORE_LastTime__c
        
        FROM CORE_LeadAcquisitionBuffer__c where Id in :this.recordIds]);
    }

    public void execute(Database.BatchableContext bc, List<CORE_LeadAcquisitionBuffer__c> buffers){
        System.debug('CORE_AsyncLFABatch.execute() : START');


        switch on this.bufferType {
            when AOL {
                System.debug('CORE_AsyncLFABatch.execute() : AOL Case');

                CORE_AsyncLFAWorker asyncWorker = new CORE_AsyncLFAWorker(buffers);
                asyncWorker.execute();
            }
            when ONLINE_SHOP {
                System.debug('CORE_AsyncLFABatch.execute() : ONLINE_SHOP Case');
                CORE_AsyncLFAWorkerOnlineShop asyncWorker = new CORE_AsyncLFAWorkerOnlineShop(buffers);
                asyncWorker.execute();
            }
            when else {
                System.debug('CORE_AsyncLFABatch.execute() : NORMAL Case');

                CORE_AsyncLFAWorker asyncWorker = new CORE_AsyncLFAWorker(buffers);
                asyncWorker.execute();
            }
        }
        System.debug('CORE_AsyncLFABatch.execute() : END');

    }

    public void finish(Database.BatchableContext bc){
        System.debug('CORE_AsyncLFABatch END.');
    }
}