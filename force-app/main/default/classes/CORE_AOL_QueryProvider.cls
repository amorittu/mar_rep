public without sharing class CORE_AOL_QueryProvider {
    private static final String OPP_QUERY_FIELDS = 'Id, IsClosed, AccountId, Account.Salutation, Account.FirstName, Account.LastName, Account.PersonBirthdate, ' +
            'Account.CORE_Main_Nationality__pc, Account.PersonMailingStreet, Account.PersonMailingPostalCode, CORE_OPP_Application_Online_ID__c, ' +
            'CORE_OPP_SIS_Opportunity_Id__c, CORE_OPP_SIS_Student_Academic_Record_ID__c, Account.PersonMailingCity, Account.PersonMailingState, Account.PersonMailingCountry,' +
            'Account.PersonMailingStateCode, Account.PersonMailingCountryCode,' +
            'Account.CORE_City_of_birth__pc, Account.CORE_Country_of_birth__pc, Account.CORE_Department_of_birth__pc, Account.CORE_Gender__pc, ' +
            'Account.CORE_BillingPermanentRegion__c, Account.BillingStreet, Account.BillingCity, Account.BillingPostalCode, Account.BillingState, Account.BillingCountry,' +
            'Account.BillingStateCode, Account.BillingCountryCode,' +
            'Account.CORE_PersonMailingCurrentRegion__c, Account.CORE_Social_Security_Number_Fiscal_Code__pc, ' +
            'StageName, CORE_OPP_Sub_Status__c, CORE_Main_Product_Interest__r.CORE_Product_Type__c, CORE_Alumni__c, ' +
            'CORE_AOL_Re_Orientation__c,CORE_Application_form_shipped_by_mail__c,CORE_Brochure_shipped_by_mail__c, CORE_Business_Unit_Before_Re_Orientation__c,' +
            'CORE_Campaign_Medium__c,CORE_Campaign_Source__c, CORE_Campaign_Term__c,CORE_Capture_Channel__c,CORE_Change_Status__c,CORE_Date_of_withdraw__c, ' +
            'CORE_Deleted_Reason__c,CORE_Document_progression__c,CORE_Down_payment_acceptance_date__c, CORE_Enrollment_Channel__c,CORE_First_owner__c,' +
            'CORE_First_Returning_Date__c,CORE_Insertion_Date__c, CORE_Insertion_Mode__c,CORE_Is_AOL_Register__c,CORE_Is_AOL_Retail_Register__c,CORE_Is_Delegated__c, ' +
            'CORE_Is_First_Owner_equals_to_Owner__c,CORE_Is_Suspect__c,CORE_Is_synchronise_MC_BU_1__c, CORE_Is_synchronise_MC_BU_2__c,CORE_IsFromDeferal__c,' +
            'CORE_Last_AOL_event_date__c, CORE_Last_inbound_contact_date__c,CORE_Last_outbound_contact_date__c,CORE_Last_returning_date__c, CORE_Main_Product_Interest__c,' +
            'CORE_Nature__c,CORE_New_Owner__c,CORE_Not_eligible_for_re_registration__c, CORE_Not_eligible_reason__c,CORE_Number_of_inbound_contact__c,' +
            'CORE_Number_of_outbound_contact__c, CORE_OPP_Academic_Year__c,CORE_OPP_Acceptance_Application_Fee_Date__c,CORE_OPP_Admission_Test_Fee_Needed__c, ' +
            'CORE_OPP_Admission_Test_Fee_Received__c,CORE_OPP_AdmissionTest_Fee_Received_Date__c, CORE_OPP_AdmissionTestFee_Payment_Method__c,CORE_OPP_Agent_Perception__c,' +
            'CORE_OPP_AOL_document_folder_Url__c, CORE_OPP_Application_completed__c,CORE_OPP_Application_fee_amount__c,CORE_OPP_Application_Fee_Bank__c, ' +
            'CORE_OPP_Application_Fee_Payment_Method__c,CORE_OPP_Application_Fee_Received__c,CORE_OPP_Application_Fee_Received_Date__c, CORE_OPP_Application_Fee_Sender__c,' +
            'CORE_OPP_Application_Final_Decision__c,CORE_OPP_Application_Online__c, CORE_OPP_Application_Online_Advancement__c,CORE_OPP_Application_Reviewer_1__c,' +
            'CORE_OPP_Application_Reviewer_2__c, CORE_OPP_Application_started__c,CORE_OPP_Business_Hours__c,CORE_OPP_Competitor__c, CORE_OPP_Conditional_Admitted_Deadl__c,' +
            'CORE_OPP_Conditional_Admitted_Reaso__c,CORE_OPP_Confirmed_Budget__c, CORE_OPP_Credit_Transfer_Needed__c,CORE_OPP_Credit_Transfer_Number__c, ' +
            'CORE_OPP_Down_Payment_Fee_Payment_Method__c,CORE_OPP_Down_Payment_Fee_Received__c,CORE_OPP_Down_Payment_Fee_Received_Date__c, ' +
            'CORE_OPP_Drop_Out_Date__c,CORE_OPP_Estimated_Budget__c,CORE_OPP_First_Outbound_Contact_Date__c,CORE_OPP_First_Response_Time__c, CORE_OPP_Funding__c,' +
            'CORE_OPP_Internship__c,CORE_OPP_Internship_dispence__c,CORE_OPP_Learning_Material__c, CORE_OPP_Legacy_Amount__c,CORE_OPP_Loss_Reason__c,' +
            'CORE_OPP_Main_Opportunity__c,CORE_OPP_Most_Advanced_Status__c, CORE_OPP_Parent_Opportunity__c,CORE_OPP_Prerequisite_Diploma_Check__c,' +
            'CORE_OPP_Prerequisite_Diploma_Check_Date__c,  CORE_OPP_Reached_Lead_Qualification__c,CORE_OPP_Registration_Date__c,CORE_OPP_Registration_Form__c,  ' +
            'CORE_OPP_Registration_Form_Date__c,CORE_OPP_Requirement_Risk__c,CORE_OPP_Retail_Agency__c,CORE_OPP_Scholarship_Amount__c, CORE_OPP_Scholarship_Assigned__c,' +
            'CORE_OPP_Scholarship_Holder__c,CORE_OPP_Scholarship_Requested__c,CORE_OPP_Scholarship_Type__c, CORE_OPP_Selection_Process_Needed__c,' +
            'CORE_OPP_Selection_Test_Date__c,CORE_OPP_Selection_Test_Grade__c,CORE_OPP_Selection_Test_Needed__c, CORE_OPP_Selection_Test_No_Show_Counter__c,' +
            'CORE_OPP_Selection_Test_Result__c,CORE_OPP_Selection_Test_Show_No_Show__c, CORE_OPP_Selection_Test_Teacher__c,CORE_OPP_Tuition_Fee_Payment_Method__c, ' +
            'CORE_OPP_Tuition_Fee_Payment_Type__c,CORE_OPP_Tuition_Fee_Received__c,CORE_OPP_Tuition_Fee_Received_Date__c,CORE_OPP_Visa_Risk__c, CORE_OPP_Visa_Status__c,' +
            'CORE_Opt_out_channels__c,CORE_Opt_out_channels_Marketing__c,CORE_Origin__c,CORE_Origin_Sales__c, CORE_Selection_process_status__c,CORE_ProfileOwner__c,' +
            'CORE_Questions__c,CORE_Re_Orientation__c,CORE_Re_Orientation_Internally__c, CORE_Re_registered_opportunity__c,CORE_Reach_Date__c,CORE_Reached_Lead__c,' +
            'CORE_ReturningLead__c,CORE_ReturningLeadDate__c, CORE_Scholarship_Level__c,CORE_Selection_process_end_date__c,CORE_Selection_process_program_leader__c,' +
            'CORE_Selection_process_start_date__c, CORE_Selection_status__c,CORE_Set_as_close_Lost__c,CORE_Set_as_close_Won__c,CORE_Showed_up_to_classes__c,' +
            'CORE_Skill_test_start_date__c, CORE_TECH_CreatedbyAPI__c, CORE_Tuition_Fee_Acceptance_Date__c, CORE_Tuition_fee_amount__c, CORE_Tuition_Fee_Bank__c,  ' +
            'CORE_Tuition_Fee_Sender__c,Description, ExpectedRevenue, First_Assignation_date__c, Fiscal, FiscalQuarter, FiscalYear,  ForecastCategory, ForecastCategoryName, ' +
            'LeadSource, Name,OwnerId, RecordTypeId, CORE_OPP_Application_Reviewer_1_Decision__c, CORE_OPP_Application_Reviewer_2_Decision__c, Study_mode__c, SyncedQuoteId, ' +
            'Type, CORE_OPP_Division__c, CORE_OPP_Business_Unit__c, CORE_OPP_Curriculum__c, CORE_OPP_Study_Area__c, CORE_OPP_Level__c, CORE_OPP_Intake__c, CloseDate, ' +
            'CORE_Document_sending_process_choice__c, CORE_AOL_Deferral__c, CORE_RetailAgencyBusinessContact__c';

    private static final String OPP_QUERY_FIELDS_EXIST = 'Id, IsClosed, CORE_OPP_Application_Online_ID__c, CORE_OPP_SIS_Opportunity_Id__c, CORE_OPP_SIS_Student_Academic_Record_ID__c';

    public static List<CORE_LeadAcquisitionBuffer__c> getLeadBuffer(String aolExternalId){
        List<CORE_LeadAcquisitionBuffer__c> leadBuffers = [SELECT Id
            FROM CORE_LeadAcquisitionBuffer__c
            WHERE CORE_MI_AOL_ExternalId__c = :aolExternalId OR CORE_SI_AOL_ExternalId__c = :aolExternalId OR CORE_TI_AOL_ExternalId__c = :aolExternalId ];

        return leadBuffers;
    }

    public static List<OpportunityLineItem> getOppLineItems(Id opportunityId, Id productId){
        List<OpportunityLineItem> oppLineItems = [SELECT Id
            FROM OpportunityLineItem
            WHERE OpportunityId = :opportunityId AND Product2Id = :productId 
            ORDER BY LastModifiedDate DESC];

        return oppLineItems;
    }

    public static List<ContentDocumentLink> getAttachments(Id opportunityId){
        List<ContentDocumentLink> attachments = [SELECT Id,ContentDocumentId, LinkedEntityId, ShareType, Visibility
            FROM ContentDocumentLink
            WHERE LinkedEntityId = :opportunityId ];

        return attachments;
    }

    public static List<OpportunityContactRole> getOpportunityContactRoles(Id opportunityId){
        List<OpportunityContactRole> contactRoles = [SELECT Id,ContactId, CORE_External_Id__c, CreatedById, CreatedDate, LastModifiedById, LastModifiedDate,
            OpportunityId,IsPrimary, Role
            FROM OpportunityContactRole
            WHERE OpportunityId = :opportunityId ];

        return contactRoles;
    }

    public static List<CORE_Sub_Status_Timestamp_History__c> getSubStatus(Id opportunityId){
        List<CORE_Sub_Status_Timestamp_History__c> subStatus = [SELECT Id, CreatedById, CreatedDate, LastModifiedById, LastModifiedDate, CORE_External_Id__c, 
            CORE_Modification_Sub_status_Date_time__c, CORE_Opportunity__c, Name, CORE_Sub_status__c
            FROM CORE_Sub_Status_Timestamp_History__c
            WHERE CORE_Opportunity__c = :opportunityId ];

        return subStatus;
    }

    public static List<CORE_Status_Timestamp_History__c> getStatus(Id opportunityId){
        List<CORE_Status_Timestamp_History__c> status = [SELECT Id, CreatedById, CreatedDate, LastModifiedById, LastModifiedDate, CORE_External_Id__c, 
            CORE_Modification_Status_Date_time__c, CORE_Opportunity__c, Name, CORE_Status__c
            FROM CORE_Status_Timestamp_History__c
            WHERE CORE_Opportunity__c = :opportunityId ];

        return status;
    }

    public static List<CORE_Point_of_Contact__c> getPointOfContacts(Id opportunityId){
        List<CORE_Point_of_Contact__c> pointOfContacts = [SELECT Id, CreatedById, CreatedDate, LastModifiedById, LastModifiedDate,
            CORE_Account__c, CORE_Campaign_Content__c, Campaign_Medium__c, CORE_Campaign_Name__c, CORE_Campaign_Source__c, CORE_Campaign_Term__c,
            CORE_Capture_Channel__c, CORE_Chat_With__c, CORE_CRM_Campaign__c, CORE_Device__c, CORE_Enrollment_Channel__c, CORE_External_Id__c,
            CORE_First_page__c, CORE_First_visit_datetime__c, CORE_Insertion_Date__c,CORE_Insertion_Mode__c,CORE_IP_address__c,CORE_IsDuplicated__c, 
            CORE_Latitude__c, CORE_Longitude__c, CORE_Nature__c, CORE_Opportunity__c, CORE_Origin__c, CORE_Origin_Sales__c, Name,
            CORE_Proactive_Engaged__c, CORE_Proactive_Prompt__c, CORE_Source__c
            FROM CORE_Point_of_Contact__c
            WHERE CORE_Opportunity__c = :opportunityId ];

        return pointOfContacts;
    }
    public static List<Opportunity> getExistingMainOpps(Id accountId,String businessUnit, String academicYear){
        String oppEnrollmentRecordType = CORE_AOL_Constants.OPP_RECORDTYPE_ENROLLMENT;

        String whereCondition = 'CORE_OPP_Business_Unit__r.CORE_Business_Unit_ExternalId__c = :businessUnit';
        whereCondition += ' AND CORE_OPP_Academic_Year__c = :academicYear';
        whereCondition += ' AND CORE_OPP_Main_Opportunity__c = true';
        whereCondition += ' AND CORE_OPP_Application_Online_ID__c = null';
        whereCondition += ' AND IsClosed = false';
        whereCondition += ' AND accountId = :accountId';
        whereCondition += ' AND RecordType.DeveloperName  = :oppEnrollmentRecordType';

        List<String> queryParams = new List<String> {OPP_QUERY_FIELDS, whereCondition};
        String queryString = String.format('SELECT {0} FROM Opportunity WHERE {1}', queryParams);

        List<Opportunity> mainOpportunities = Database.query(queryString);
        
        return mainOpportunities;
    }

    public static List<Opportunity> getAolOpportunities(String aolExternalId){
        String whereCondition = 'CORE_OPP_Application_Online_ID__c =:aolExternalId';
        List<String> queryParams = new List<String> {OPP_QUERY_FIELDS, whereCondition};
        String queryString = String.format('SELECT {0} FROM Opportunity WHERE {1}', queryParams);

        List<Opportunity> opportunities = Database.query(queryString);
        
        return opportunities;
    }

    public static List<Opportunity> getOpportunitiesIfExist(String aolExternalId){
        String whereCondition = 'CORE_OPP_Application_Online_ID__c =:aolExternalId';
        List<String> queryParams = new List<String> {OPP_QUERY_FIELDS_EXIST, whereCondition};
        String queryString = String.format('SELECT {0} FROM Opportunity WHERE {1}', queryParams);

        List<Opportunity> opportunities = Database.query(queryString);
        
        return opportunities;
    }

    public static List<CORE_LeadAcquisitionBuffer__c> getAolLeadAcquisitionBuffer(String aolExternalId){
        String whereCondition1 = 'CORE_MI_AOL_ExternalId__c =:aolExternalId';
        String whereCondition2 = 'CORE_SI_AOL_ExternalId__c =:aolExternalId';
        String whereCondition3 = 'CORE_TI_AOL_ExternalId__c =:aolExternalId';
        List<String> queryParams = new List<String> {whereCondition1, whereCondition2, whereCondition3};
        String queryString = String.format('SELECT Id FROM CORE_LeadAcquisitionBuffer__c WHERE {0} OR {1} OR {2}', queryParams);
        List<CORE_LeadAcquisitionBuffer__c> leadAcquisitionBuffer = Database.query(queryString);
        
        return leadAcquisitionBuffer;
    }

    public static sObject getRelatedObj(String objAPI, String fields, String externalField, String externalID){
        return database.query('SELECT ' + fields + ' FROM ' + objAPI + ' WHERE ' + externalField + ' = :externalID LIMIT 1');
    }
}