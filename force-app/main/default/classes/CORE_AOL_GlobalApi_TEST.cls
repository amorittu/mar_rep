@isTest
private class CORE_AOL_GlobalApi_TEST {
    private static final String AOL_EXTERNALID = 'setupAol';

    @TestSetup
    static void makeData(){
        Id profileId = [SELECT Id FROM Profile WHERE Name = 'CORE_API_Profile' LIMIT 1].Id;

        User usr = new User(
            Username = 'usertest@aoltest.com',
            LastName = 'TEST_USER',
            Email = 'testemail@yopmail.com',
            Alias = 'T',
            CommunityNickname = 'T',
            TimeZoneSidKey = 'Europe/Paris',
            LocaleSidKey = 'fr_FR_EURO',
            EmailEncodingKey = 'ISO-8859-1',
            ProfileId = profileId,
            LanguageLocaleKey = 'fr'
        );
        database.insert(usr, true);

        CORE_CatalogHierarchyModel catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test').catalogHierarchy;
        Account account = CORE_DataFaker_Account.getStudentAccount('test');
        Opportunity  opportunity = CORE_DataFaker_Opportunity.getAOLOpportunity(catalogHierarchy.division.Id,
                catalogHierarchy.businessUnit.Id,
                account.Id,
                CORE_AOL_Constants.PKL_OPP_STAGENAME_PROSPECT,
                CORE_AOL_Constants.PKL_OPP_SUBSTATUS_PROSPECTAPPSTARTED,
                AOL_EXTERNALID
        );
    }

    @IsTest
    static void test_MandatoryMissing(){
        User usr = [SELECT Id FROM User WHERE Username = 'usertest@aoltest.com' LIMIT 1];

        List<PermissionSetAssignment> psaList = new List<PermissionSetAssignment>();

        for(PermissionSet ps : [SELECT Id FROM PermissionSet WHERE Name = 'CORE_API_AOL' OR Name = 'CORE_LeadFormAcquisition']){
            psaList.add(
                    new PermissionSetAssignment(AssigneeId = usr.id, PermissionSetId = ps.Id)
            );
        }

        database.insert(psaList, true);

        System.runAs(usr){
            String reqBody = '{'
                    + '"account": {'
                    + '"salutation": "Mr.",'
                    + '"firstname": "Jean",'
                    + '"lastname": "Dupont",'
                    + '"birthdate": "2022-03-22",'
                    + '"mainNationality": "FRA",'
                    + '"personMailingStreet": "12 rue des messageries",'
                    + '"personMailingPostalCode": "75010",'
                    + '"personMailingCity": "Paris",'
                    + '"personMailingState": "",'
                    + '"personMailingCountry": "France",'
                    + '"updateIfNotNull": true'
                    + '},'
                    + '"foreignLanguages": {'
                    + '"language1": "ENG",'
                    + '"language1Level": "A1-Elementary|Low",'
                    + '"language2": "FRA",'
                    + '"language2Level": "C1-Superior|Good"'
                    + '}'
                    + '}';

            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
            req.requestURI = '/services/apexrest/aol/global-api';
            req.httpMethod = 'POST';
            req.requestBody = Blob.valueOf(reqBody);

            RestContext.request = req;
            RestContext.response= res;

            test.startTest();
            CORE_AOL_Wrapper.ResultWrapperGlobal  results = CORE_AOL_GlobalApi.execute();
            test.stopTest();
        }
    }

    @IsTest
    static void test_Account(){
        User usr = [SELECT Id FROM User WHERE Username = 'usertest@aoltest.com' LIMIT 1];

        List<PermissionSetAssignment> psaList = new List<PermissionSetAssignment>();

        for(PermissionSet ps : [SELECT Id FROM PermissionSet WHERE Name = 'CORE_API_AOL' OR Name = 'CORE_LeadFormAcquisition']){
            psaList.add(
                    new PermissionSetAssignment(AssigneeId = usr.id, PermissionSetId = ps.Id)
            );
        }

        database.insert(psaList, true);

        System.runAs(usr){
            String reqBody = '{'
            + '"aolExternalId": "' + AOL_EXTERNALID + '",'
            + '"account": {'
            + '"salutation": "Mr.",'
            + '"firstname": "Jean",'
            + '"lastname": "Dupont",'
            + '"birthdate": "2022-03-22",'
            + '"mainNationality": "FRA",'
            + '"personMailingStreet": "12 rue des messageries",'
            + '"personMailingPostalCode": "75010",'
            + '"personMailingCity": "Paris",'
            + '"personMailingState": "",'
            + '"personMailingCountry": "France",'
            + '"updateIfNotNull": true'
            + '},'
            + '"foreignLanguages": {'
            + '"language1": "ENG",'
            + '"language1Level": "A1-Elementary|Low",'
            + '"language2": "FRA",'
            + '"language2Level": "C1-Superior|Good"'
            + '}'
            + '}';

            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
            req.requestURI = '/services/apexrest/aol/global-api';
            req.httpMethod = 'POST';
            req.requestBody = Blob.valueOf(reqBody);

            RestContext.request = req;
            RestContext.response= res;

            test.startTest();
            CORE_AOL_Wrapper.ResultWrapperGlobal  results = CORE_AOL_GlobalApi.execute();
            test.stopTest();
        }
    }

    @IsTest
    static void test_Opportunity(){
        User usr = [SELECT Id FROM User WHERE Username = 'usertest@aoltest.com' LIMIT 1];

        List<PermissionSetAssignment> psaList = new List<PermissionSetAssignment>();

        for(PermissionSet ps : [SELECT Id FROM PermissionSet WHERE Name = 'CORE_API_AOL' OR Name = 'CORE_LeadFormAcquisition']){
            psaList.add(
                    new PermissionSetAssignment(AssigneeId = usr.id, PermissionSetId = ps.Id)
            );
        }

        database.insert(psaList, true);

        System.runAs(usr){
            String reqBody = '{'
                    + '"aolExternalId": "' + AOL_EXTERNALID + '",'
                    + '"account": {},'
                    + '"foreignLanguages": {},'
                    + '"scholarship": {'
                    + '"requested": true,'
                    + '"type": "CORE_PKL_Scholar_Type_1",'
                    + '"level": "test"'
                    + '},'
                    + '"applicationFeesPaid": {'
                    + '"received": true,'
                    + '"paymentMethod": "CORE_PKL_Cash",'
                    + '"receivedDate": "2022-03-22T06:31:38.655Z",'
                    + '"acceptanceDate": "2022-03-22T06:31:38.655Z",'
                    + '"transactionId": "myTransactionId",'
                    + '"amount": 15.64,'
                    + '"bank": "myBank",'
                    + '"sender": "mySender"'
                    + '},'
                    + '"documentSendingFields": {'
                    + '"documentSendingChoice": "Email"'
                    + '},'
                    + '"onlineApplicationDocumentUrl": "testUrl",'
                    + '"aolDocuments": [{}],'
                    + '"selectionTest": {'
                    + '"date": "2022-03-22",'
                    + '"endDate": "2022-03-22",'
                    + '"show": true,'
                    + '"noShowCounter": 0,'
                    + '"grade": 0.01,'
                    + '"result": "CORE_PKL_Failed",'
                    + '"teacher": "' + UserInfo.getUserId() + '",'
                    + '"status": "CORE_PKL_Booked"'
                    + '},'
                    + '"enrollmentFeesPaid": {'
                    + '"received": true,'
                    + '"paymentMethod": "CORE_PKL_Cash",'
                    + '"receivedDate": "2022-03-22T06:31:38.655Z",'
                    + '"acceptanceDate": "2022-03-22T06:31:38.655Z"'
                    + '},'
                    + '"tuitionFeesPaid": {'
                    + '"received": true,'
                    + '"paymentType": "CORE_PKL_Partially_Paid",'
                    + '"paymentMethod": "CORE_PKL_Cash",'
                    + '"receivedDate": "2022-03-22T06:31:38.655Z",'
                    + '"acceptanceDate": "2022-03-22T06:31:38.655Z",'
                    + '"amount": 0.01,'
                    + '"transactionId": "myTransactionId",'
                    + '"bank": "myBank",'
                    + '"sender": "mySender"'
                    + '},'
                    + '"aolProgressionFields": {'
                    + '"aolProgression": 50.09,'
                    + '"documentProgression": "Started",'
                    + '"firstDocumentUploadDate": "2022-03-22",'
                    + '"lastAolEventDate": "2022-03-22T06:31:38.655Z",'
                    + '"isAol": true,'
                    + '"isAolOnlineRegistered": true,'
                    + '"isAolRetailRegistered": true'
                    + '}'
                    + '}';

            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
            req.requestURI = '/services/apexrest/aol/global-api';
            req.httpMethod = 'POST';
            req.requestBody = Blob.valueOf(reqBody);

            RestContext.request = req;
            RestContext.response= res;

            test.startTest();
            CORE_AOL_Wrapper.ResultWrapperGlobal results = CORE_AOL_GlobalApi.execute();
            test.stopTest();
        }
    }

    @IsTest
    static void test_Documents(){
        User usr = [SELECT Id FROM User WHERE Username = 'usertest@aoltest.com' LIMIT 1];

        List<PermissionSetAssignment> psaList = new List<PermissionSetAssignment>();

        for(PermissionSet ps : [SELECT Id FROM PermissionSet WHERE Name = 'CORE_API_AOL' OR Name = 'CORE_LeadFormAcquisition']){
            psaList.add(
                    new PermissionSetAssignment(AssigneeId = usr.id, PermissionSetId = ps.Id)
            );
        }

        database.insert(psaList, true);

        System.runAs(usr){
            String reqBody = '{'
                    + '"aolExternalId": "' + AOL_EXTERNALID + '",'
                    + '"account": {},'
                    + '"foreignLanguages": {},'
                    + '"scholarship": {},'
                    + '"applicationFeesPaid": {},'
                    + '"documentSendingFields": {},'
                    + '"onlineApplicationDocumentUrl": null,'
                    + '"aolDocuments": [{'
                    + '"externalId": "testExtId",'
                    + '"name": "testName",'
                    + '"externalUrl": "testExtUrl",'
                    + '"type": "Visa",'
                    + '"createdDate": "2022-03-22",'
                    + '"approvedDate": "2022-03-22",'
                    + '"rejectedDate": "2022-03-22"'
                    + '}],'
                    + '"selectionTest": {},'
                    + '"enrollmentFeesPaid": {},'
                    + '"tuitionFeesPaid": {},'
                    + '"aolProgressionFields": {}'
                    + '}';

            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
            req.requestURI = '/services/apexrest/aol/global-api';
            req.httpMethod = 'POST';
            req.requestBody = Blob.valueOf(reqBody);

            RestContext.request = req;
            RestContext.response= res;

            test.startTest();
            CORE_AOL_Wrapper.ResultWrapperGlobal results = CORE_AOL_GlobalApi.execute();
            test.stopTest();
        }
    }

    @IsTest
    static void test_AcademicDiplomaHistory(){
        User usr = [SELECT Id FROM User WHERE Username = 'usertest@aoltest.com' LIMIT 1];

        List<PermissionSetAssignment> psaList = new List<PermissionSetAssignment>();

        for(PermissionSet ps : [SELECT Id FROM PermissionSet WHERE Name = 'CORE_API_AOL' OR Name = 'CORE_LeadFormAcquisition']){
            psaList.add(
                    new PermissionSetAssignment(AssigneeId = usr.id, PermissionSetId = ps.Id)
            );
        }

        database.insert(psaList, true);

        System.runAs(usr){
            String reqBody = '{'
                    + '"aolExternalId": "' + AOL_EXTERNALID + '",'
                    + '"account": {},'
                    + '"academicDiplomaHistory": [{'
                    + '"diplomaExternalId": "testExtId",'
                    + '"diplomaName": "testName",'
                    + '"diplomaLevel": "CORE_PKL_Apprenticeship",'
                    + '"diplomaSchool": "School",'
                    + '"diplomaCity": "Paris",'
                    + '"diplomaCountry": "FRA",'
                    + '"diplomaStatus": "CORE_PKL_In_Progress",'
                    + '"diplomaExpectedGraduationDate": "2022-03-22",'
                    + '"diplomaModificationDate": "2022-03-22"'
                    + '}'
                    + '],'
                    + '"foreignLanguages": {},'
                    + '"scholarship": {},'
                    + '"applicationFeesPaid": {},'
                    + '"documentSendingFields": {},'
                    + '"onlineApplicationDocumentUrl": null,'
                    + '"aolDocuments": [{}],'
                    + '"selectionTest": {},'
                    + '"enrollmentFeesPaid": {},'
                    + '"tuitionFeesPaid": {},'
                    + '"aolProgressionFields": {}'
                    + '}';

            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
            req.requestURI = '/services/apexrest/aol/global-api';
            req.httpMethod = 'POST';
            req.requestBody = Blob.valueOf(reqBody);

            RestContext.request = req;
            RestContext.response= res;

            test.startTest();
            CORE_AOL_Wrapper.ResultWrapperGlobal results = CORE_AOL_GlobalApi.execute();
            test.stopTest();
        }
    }

    @IsTest
    static void test_All(){
        User usr = [SELECT Id FROM User WHERE Username = 'usertest@aoltest.com' LIMIT 1];

        List<PermissionSetAssignment> psaList = new List<PermissionSetAssignment>();

        for(PermissionSet ps : [SELECT Id FROM PermissionSet WHERE Name = 'CORE_API_AOL' OR Name = 'CORE_LeadFormAcquisition']){
            psaList.add(
                    new PermissionSetAssignment(AssigneeId = usr.id, PermissionSetId = ps.Id)
            );
        }

        database.insert(psaList, true);

        System.runAs(usr){
            String reqBody = '{'
                    + '"aolExternalId": "' + AOL_EXTERNALID + '",'
                    + '"account": {'
                    + '"salutation": "Mr.",'
                    + '"firstname": "Jean",'
                    + '"lastname": "Dupont",'
                    + '"birthdate": "2022-03-22",'
                    + '"mainNationality": "FRA",'
                    + '"personMailingStreet": "12 rue des messageries",'
                    + '"personMailingPostalCode": "75010",'
                    + '"personMailingCity": "Paris",'
                    + '"personMailingState": "",'
                    + '"personMailingCountry": "France",'
                    + '"updateIfNotNull": true'
                    + '},'
                    + '"academicDiplomaHistory": [{'
                    + '"diplomaExternalId": "testExtId",'
                    + '"diplomaName": "testName",'
                    + '"diplomaLevel": "CORE_PKL_Apprenticeship",'
                    + '"diplomaSchool": "School",'
                    + '"diplomaCity": "Paris",'
                    + '"diplomaCountry": "FRA",'
                    + '"diplomaStatus": "CORE_PKL_In_Progress",'
                    + '"diplomaExpectedGraduationDate": "2022-03-22",'
                    + '"diplomaModificationDate": "2022-03-22"'
                    + '}'
                    + '],'
                    + '"foreignLanguages": {'
                    + '"language1": "ENG",'
                    + '"language1Level": "A1-Elementary|Low",'
                    + '"language2": "FRA",'
                    + '"language2Level": "C1-Superior|Good"'
                    + '},'
                    + '"scholarship": {'
                    + '"requested": true,'
                    + '"type": "CORE_PKL_Scholar_Type_1",'
                    + '"level": "test"'
                    + '},'
                    + '"applicationFeesPaid": {'
                    + '"received": true,'
                    + '"paymentMethod": "CORE_PKL_Cash",'
                    + '"receivedDate": "2022-03-22T06:31:38.655Z",'
                    + '"acceptanceDate": "2022-03-22T06:31:38.655Z",'
                    + '"transactionId": "myTransactionId",'
                    + '"amount": 15.64,'
                    + '"bank": "myBank",'
                    + '"sender": "mySender"'
                    + '},'
                    + '"documentSendingFields": {'
                    + '"documentSendingChoice": "Email"'
                    + '},'
                    + '"onlineApplicationDocumentUrl": "testUrl",'
                    + '"aolDocuments": [{'
                    + '"externalId": "testExtId",'
                    + '"name": "testName",'
                    + '"externalUrl": "testExtUrl",'
                    + '"type": "Visa",'
                    + '"createdDate": "2022-03-22",'
                    + '"approvedDate": "2022-03-22",'
                    + '"rejectedDate": "2022-03-22"'
                    + '}],'
                    + '"selectionTest": {'
                    + '"date": "2022-03-22",'
                    + '"endDate": "2022-03-22",'
                    + '"show": true,'
                    + '"noShowCounter": 0,'
                    + '"grade": 0.01,'
                    + '"result": "CORE_PKL_Failed",'
                    + '"teacher": "' + UserInfo.getUserId() + '",'
                    + '"status": "CORE_PKL_Booked"'
                    + '},'
                    + '"enrollmentFeesPaid": {'
                    + '"received": true,'
                    + '"paymentMethod": "CORE_PKL_Cash",'
                    + '"receivedDate": "2022-03-22T06:31:38.655Z",'
                    + '"acceptanceDate": "2022-03-22T06:31:38.655Z"'
                    + '},'
                    + '"tuitionFeesPaid": {'
                    + '"received": true,'
                    + '"paymentType": "CORE_PKL_Partially_Paid",'
                    + '"paymentMethod": "CORE_PKL_Cash",'
                    + '"receivedDate": "2022-03-22T06:31:38.655Z",'
                    + '"acceptanceDate": "2022-03-22T06:31:38.655Z",'
                    + '"amount": 0.01,'
                    + '"transactionId": "myTransactionId",'
                    + '"bank": "myBank",'
                    + '"sender": "mySender"'
                    + '},'
                    + '"aolProgressionFields": {'
                    + '"aolProgression": 50.09,'
                    + '"documentProgression": "Started",'
                    + '"firstDocumentUploadDate": "2022-03-22",'
                    + '"lastAolEventDate": "2022-03-22T06:31:38.655Z",'
                    + '"isAol": true,'
                    + '"isAolOnlineRegistered": true,'
                    + '"isAolRetailRegistered": true'
                    + '}'
                    + '}';

            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
            req.requestURI = '/services/apexrest/aol/global-api';
            req.httpMethod = 'POST';
            req.requestBody = Blob.valueOf(reqBody);

            RestContext.request = req;
            RestContext.response= res;

            test.startTest();
            CORE_AOL_Wrapper.ResultWrapperGlobal results = CORE_AOL_GlobalApi.execute();
            test.stopTest();
        }
    }
}