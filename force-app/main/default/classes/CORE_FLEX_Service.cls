public class CORE_FLEX_Service {

    public static HttpRequest createRequestTwilio(string pathname) {

        CORE_FLEX_Settings__c setting = CORE_FLEX_Settings__c.getOrgDefaults();
        string url = setting.UrlFlex__c;
        Blob cryptoKey = EncodingUtil.base64Decode(setting.KeySecret__c);
        Blob encryptedData = EncodingUtil.base64Decode(setting.TokenFlex__c);
        Blob decryptedData = Crypto.decryptWithManagedIV('AES256', cryptoKey, encryptedData);
        string accountToken = decryptedData.toString();
        
        string urlSign = url+pathname;
        
        Blob signatureBlob = Crypto.generateMac('hmacSHA1', Blob.valueOf(urlSign), Blob.valueOf(accountToken));
        string token = EncodingUtil.base64Encode(signatureBlob);
        
        HttpRequest req = new HttpRequest();
     	req.setEndpoint(url+pathname);
        req.setHeader('Content-Type','application/json');
     	req.setMethod('POST');
     
 	    req.setHeader('X-Twilio-Signature', token);

         return req;

    }

}