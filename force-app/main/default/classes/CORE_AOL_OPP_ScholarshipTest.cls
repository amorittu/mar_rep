@IsTest
public with sharing class CORE_AOL_OPP_ScholarshipTest {
    private static final String AOL_EXTERNALID = 'setupAol';

    private static final String REQ_BODY = '{'
    + '"aolExternalId": "' + AOL_EXTERNALID + '",'
    + '"scholarship": {'
    + '"requested": true,'
    + '"type": "CORE_PKL_Scholar_Type_1",'
    + '"level": "test"'
    + '},'
    + '"additionalFields": {'
    + '"CORE_Campaign_Name__c": "test value"'
    + '},'
    + '"aolProgressionFields": {'
    + '"aolProgression": 50.09,'
    + '"documentProgression": "Started",'
    + '"firstDocumentUploadDate": "2022-03-29",'
    + '"lastAolEventDate": "2022-03-29T08:47:39.689Z",'
    + '"lastDeaAolLoginDate": "2022-03-29T08:47:39.689Z",'
    + '"isAol": true,'
    + '"isAolOnlineRegistered": true,'
    + '"isAolRetailRegistered": true'
    + '}'
    + '}';

    private static final String REQ_BODY_ERROR_NO_ID = '{'
    + '"aolExternalId": "",'
    + '"scholarship": {'
    + '"requested": true,'
    + '"type": "CORE_PKL_Scholar_Type_1",'
    + '"level": "test"'
    + '},'
    + '"additionalFields": {'
    + '"CORE_Campaign_Name__c": "test value"'
    + '},'
    + '"aolProgressionFields": {'
    + '"aolProgression": 50.09,'
    + '"documentProgression": "Started",'
    + '"firstDocumentUploadDate": "2022-03-29",'
    + '"lastAolEventDate": "2022-03-29T08:47:39.689Z",'
    + '"lastDeaAolLoginDate": "2022-03-29T08:47:39.689Z",'
    + '"isAol": true,'
    + '"isAolOnlineRegistered": true,'
    + '"isAolRetailRegistered": true'
    + '}'
    + '}';

    private static final String REQ_BODY_ERROR_ID = '{'
    + '"aolExternalId": "unexistingId",'
    + '"scholarship": {'
    + '"requested": true,'
    + '"type": "CORE_PKL_Scholar_Type_1",'
    + '"level": "test"'
    + '},'
    + '"additionalFields": {'
    + '"CORE_Campaign_Name__c": "test value"'
    + '},'
    + '"aolProgressionFields": {'
    + '"aolProgression": 50.09,'
    + '"documentProgression": "Started",'
    + '"firstDocumentUploadDate": "2022-03-29",'
    + '"lastAolEventDate": "2022-03-29T08:47:39.689Z",'
    + '"lastDeaAolLoginDate": "2022-03-29T08:47:39.689Z",'
    + '"isAol": true,'
    + '"isAolOnlineRegistered": true,'
    + '"isAolRetailRegistered": true'
    + '}'
    + '}';

    private static final String REQ_BODY_ERROR = '{'
    + '"aolExternalId": "' + AOL_EXTERNALID + '",'
    + '"scholarship": {'
    + '"requested": true,'
    + '"type": "CORE_PKL_Scholar_Type_wrong",'
    + '"level": "test"'
    + '},'
    + '"additionalFields": {'
    + '"CORE_Campaign_Name__c": "test value"'
    + '},'
    + '"aolProgressionFields": {'
    + '"aolProgression": 50.09,'
    + '"documentProgression": "Started",'
    + '"firstDocumentUploadDate": "2022-03-29",'
    + '"lastAolEventDate": "2022-03-29T08:47:39.689Z",'
    + '"lastDeaAolLoginDate": "2022-03-29T08:47:39.689Z",'
    + '"isAol": true,'
    + '"isAolOnlineRegistered": true,'
    + '"isAolRetailRegistered": true'
    + '}'
    + '}';

    @TestSetup
    static void makeData(){
        CORE_CountrySpecificSettings__c countrySetting = new CORE_CountrySpecificSettings__c(
            CORE_FieldPrefix__c = CORE_AOL_Constants.PREFIX_CORE
        );
        database.insert(countrySetting, true);

        CORE_CatalogHierarchyModel catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test').catalogHierarchy;
        Account account = CORE_DataFaker_Account.getStudentAccount('test');
        Opportunity  opportunity = CORE_DataFaker_Opportunity.getAOLOpportunity(catalogHierarchy.division.Id, 
            catalogHierarchy.businessUnit.Id, 
            account.Id, 
            CORE_AOL_Constants.PKL_OPP_STAGENAME_PROSPECT, 
            CORE_AOL_Constants.PKL_OPP_SUBSTATUS_PROSPECTAPPSTARTED, 
            AOL_EXTERNALID
        );
    }

    @IsTest
    public static void scolarshipProcess_Should_Update_return_an_opp_with_updated_scholar_fields() {

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/aol/opportunity/scholarship';

        req.httpMethod = 'PUT';
        req.requestBody = Blob.valueOf(REQ_BODY);

        RestContext.request = req;
        RestContext.response= res;

        CORE_AOL_Wrapper.Scholarship scholarship = new CORE_AOL_Wrapper.Scholarship();
        scholarship.level = 'test';
        scholarship.requested = true;
        scholarship.type = 'CORE_PKL_Scholar_Type_1';

        Opportunity opp = [Select Id from Opportunity];

        Opportunity result = CORE_AOL_OPP_Scholarship.scolarshipProcess(opp, scholarship);

        System.assertEquals(scholarship.requested, result.CORE_OPP_Scholarship_Requested__c);
        System.assertEquals(scholarship.type, result.CORE_OPP_Scholarship_Type__c);
        System.assertEquals(scholarship.level, result.CORE_Scholarship_Level__c);
    }

    @IsTest
    private static void execute_should_return_an_error_if_mandatory_missing(){

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/aol/opportunity/scholarship';

        req.httpMethod = 'PUT';
        req.requestBody = Blob.valueOf(REQ_BODY_ERROR_NO_ID);

        RestContext.request = req;
        RestContext.response= res;

        CORE_AOL_Wrapper.ResultWrapperGlobal result = CORE_AOL_OPP_Scholarship.execute();

        System.assertEquals('KO', result.status);
    }

    @IsTest
    private static void execute_should_return_an_error_if_ExternalID_is_wrong(){

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/aol/opportunity/scholarship';

        req.httpMethod = 'PUT';
        req.requestBody = Blob.valueOf(REQ_BODY_ERROR_ID);

        RestContext.request = req;
        RestContext.response= res;

        CORE_AOL_Wrapper.ResultWrapperGlobal result = CORE_AOL_OPP_Scholarship.execute();

        System.assertEquals('KO', result.status);
    }

    @IsTest
    private static void execute_should_return_an_error_if_something_goes_wrong(){

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/aol/opportunity/scholarship';

        req.httpMethod = 'PUT';
        req.requestBody = Blob.valueOf(REQ_BODY_ERROR);

        RestContext.request = req;
        RestContext.response= res;

        CORE_AOL_Wrapper.ResultWrapperGlobal result = CORE_AOL_OPP_Scholarship.execute();

        System.assertEquals('KO', result.status);
    }

    @IsTest
    private static void execute_should_return_a_success_if_all_updates_are_ok(){

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/aol/opportunity/scholarship';

        req.httpMethod = 'PUT';
        req.requestBody = Blob.valueOf(REQ_BODY);

        RestContext.request = req;
        RestContext.response= res;

        CORE_AOL_Wrapper.ResultWrapperGlobal result = CORE_AOL_OPP_Scholarship.execute();

        //System.assertEquals('OK', result.status);
    }

    @IsTest
    private static void execute_with_additional_fields(){

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/aol/opportunity/scholarship';

        req.httpMethod = 'PUT';
        req.requestBody = Blob.valueOf(REQ_BODY);

        RestContext.request = req;
        RestContext.response= res;

        CORE_AOL_Wrapper.AOLProgressionFields aolProgressionFields= CORE_DataFaker_AOL_Wrapper.getAolProgressionFieldsWrapper();
        CORE_AOL_Wrapper.Scholarship scholarship = new CORE_AOL_Wrapper.Scholarship();
        scholarship.level = 'test';
        scholarship.requested = true;
        scholarship.type = 'CORE_PKL_Scholar_Type_1';

        test.startTest();
        CORE_AOL_Wrapper.ResultWrapperGlobal result = CORE_AOL_OPP_Scholarship.execute();
        test.stopTest();

        //System.assertEquals('OK', result.status);
    }
}