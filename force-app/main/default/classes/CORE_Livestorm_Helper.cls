public class CORE_Livestorm_Helper {
    public static Map<Id,CORE_Business_Unit__c> getBuFromCampaign(List<Campaign> campaigns){
        List<String> buIds = new List<String>();
        for(Campaign campaign : campaigns){
            buIds.add(campaign.CORE_Business_Unit__c);
        }
        
        List<CORE_Business_Unit__c> businessUnits = [SELECT Id, CORE_Livestorm_Metadata_Name__c FROM CORE_Business_Unit__c WHERE Id in :buIds];
        return new Map<Id,CORE_Business_Unit__c> (businessUnits);
    }

    public static CORE_Livestorm__mdt GetMetadata(Map<Id,CORE_Business_Unit__c> orderBusinessUnit, Campaign campaign){
        CORE_Business_Unit__c bu = orderBusinessUnit.get(campaign.CORE_Business_Unit__c);
        String metadataName = bu == null || bu.CORE_Livestorm_Metadata_Name__c == '' || bu.CORE_Livestorm_Metadata_Name__c == null ? 'Default_Value' : bu.CORE_Livestorm_Metadata_Name__c;
        System.debug('Using metadata : ' + metadataName);

        CORE_Livestorm__mdt livestormMetadata = CORE_Livestorm__mdt.getInstance(metadataName);
        if (livestormMetadata == null){
            livestormMetadata = CORE_Livestorm__mdt.getInstance('Default_Value');
        }

        return livestormMetadata;
    }

    public static String getName(Campaign campaign){
        return campaign.CORE_Campaign_Full_Name__c != null ? campaign.CORE_Campaign_Full_Name__c : campaign.Name;
    }
    
    public static String GetTimezone(Campaign campaign){
        if (campaign.CORE_Timezone__c == null){
            System.debug('Europe/Paris');
            return 'Europe/Paris';
        }
        else{
            System.debug(campaign.CORE_Timezone__c.mid(campaign.CORE_Timezone__c.lastIndexOf('(') + 1, campaign.CORE_Timezone__c.lastIndexOf(')') - (campaign.CORE_Timezone__c.lastIndexOf('(') + 1)));
            return campaign.CORE_Timezone__c.mid(campaign.CORE_Timezone__c.lastIndexOf('(') + 1, campaign.CORE_Timezone__c.lastIndexOf(')') - (campaign.CORE_Timezone__c.lastIndexOf('(') + 1));
        }
    }

    public static String GetSessionStartDate(Campaign campaign){
        DateTime dateToReturn = campaign.CORE_Start_Date_and_hour__c != null ? campaign.CORE_Start_Date_and_hour__c : Datetime.now();
        System.debug(dateToReturn.format('yyyy-MM-dd HH:mm:ss'));

        return dateToReturn.format('yyyy-MM-dd HH:mm:ss');
    }
}