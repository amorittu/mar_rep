@IsTest
global with sharing class CORE_CUBE_CreateCsvTest {
    static final List<String> status = new List<String>{'Suspect', 'Prospect', 'Closed Won', 'Closed Lost'};
    @TestSetup

    static void makeData(){
        dateTime createdDate = DateTime.newInstance(2021, 07, 01, 12, 0, 0);
        CORE_DataFaker_CatalogHierarchy catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test1', createdDate, createdDate);
        Account account = CORE_DataFaker_Account.getStudentAccount('test1', createdDate, createdDate);
        Opportunity opportunity = CORE_DataFaker_Opportunity.getOpportunity(catalogHierarchy.division.Id, 
            catalogHierarchy.businessUnit.Id, 
            catalogHierarchy.studyArea.Id,
            catalogHierarchy.curriculum.Id,
            catalogHierarchy.level.Id,
            catalogHierarchy.intake.Id, 
            account.Id, 
            'Lead', 
            'Lead - New'
        );
    }

    @IsTest
    public static void manualExecutionShouldExecuteAllCallout(){
        Test.setMock(HttpCalloutMock.class, new CubeCalloutMock());
        Test.startTest();
        CORE_CUBE_CreateCsv.manualExecution();
        Test.stopTest();
        List<CORE_CUBE_Log__c> myLog = [SELECT Id,Name,IsPassed__c,ErrorMessage__c,ObjectName__c,date_donne_extraite__c,dateOk__c FROM CORE_CUBE_Log__c];
        System.assertEquals(13, myLog.size());
    }

    @IsTest
    public static void getFullExort_should_create_logs_without_callout(){
        Test.startTest();    
        List<String> fileNames = new List<String> {'Person_Account','Opportunity','Status_Journey','SubStatus_Journey','Study_Area','Curriculum','Level','Intake','Picklist',
            'Deleted_Objects','Point_of_Contact','Division','Business_Unit'};

        for(String filename : fileNames){
            CORE_CUBE_CreateCsv.getFullExort(filename);
        }

        Test.stopTest();
        List<CORE_CUBE_Log__c> myLog = [SELECT Id,Name,IsPassed__c,ErrorMessage__c,ObjectName__c,date_donne_extraite__c,dateOk__c FROM CORE_CUBE_Log__c];
        System.assertEquals(13, myLog.size());
    }

    @IsTest
    public static void getLastSuccesDateTest(){
        CORE_CUBE_Master_Log__c myMasterLog = new CORE_CUBE_Master_Log__c();
        myMasterLog.ErrorOn__c='Account';
        myMasterLog.Last_Success__c=DateTime.newInstance(2021, 07, 01, 12, 0, 0);
        myMasterLog.Last_Try__c=DateTime.newInstance(2021, 07, 05, 12, 0, 0);
        myMasterLog.IsSuccess__c=true;
        Test.startTest();
        insert myMasterLog;
        DateTime myTime = CORE_CUBE_CreateCsv.getLastSuccesDate();
        Test.stopTest();
        System.assertNotEquals(null, myTime);
        System.assertEquals(DateTime.newInstance(2021, 07, 01, 12, 0, 0),myTime);
    }
    @IsTest
    public static void getLastSuccesDateNoDateTest(){
        CORE_CUBE_Master_Log__c myMasterLog = new CORE_CUBE_Master_Log__c();
        myMasterLog.ErrorOn__c='Account';
        myMasterLog.Last_Try__c=DateTime.newInstance(2021, 07, 05);
        myMasterLog.IsSuccess__c=true;
        Test.startTest();
        insert myMasterLog;
        DateTime myTime = CORE_CUBE_CreateCsv.getLastSuccesDate();
        Test.stopTest();
        System.assertNotEquals(null, myTime);
        System.assertEquals(System.today().AddDays(-1),myTime);
    }
    @IsTest
    public static void getLastSuccesDateNoMasterLogTest(){
        Test.startTest();
        DateTime myTime = CORE_CUBE_CreateCsv.getLastSuccesDate();
        Test.stopTest();
        System.assertNotEquals(null, myTime);
        System.assertEquals(System.today().AddDays(-1),myTime);
    }

    @isTest
    public static void getHeaderPicklistTest(){
        String testHeader = 'List,Language,Code,Label';
        String value = CORE_CUBE_CreateCsv.getHeaderPicklist();
        System.assertEquals(value,testHeader);
    }
    @isTest
    public static void getHeaderIntakeTest(){
        String testHeader = 'Id,Uuid,Code,Label,Parent_uuid,Session,Academic_year,CreatedOn,ModifiedOn';
        String value = CORE_CUBE_CreateCsv.getHeaderIntake();
        System.assertEquals(value,testHeader);
    }

    @isTest
    public static void getListIntakeTest(){
        CORE_Intake__c myIntake = [SELECT Id,CORE_Intake_ExternalId__c,CORE_Intake_code__c,Name, CORE_Parent_Level__r.CORE_Level_ExternalId__c,CORE_Session__c,CORE_Academic_Year__c,CreatedDate,LastModifiedDate FROM CORE_Intake__c];
        Test.setCreatedDate(myIntake.Id, DateTime.newInstance(2021,07,10));
        CORE_Intake__c myNewIntake = [SELECT Id,CORE_Intake_ExternalId__c,CORE_Intake_code__c,Name, CORE_Parent_Level__r.CORE_Level_ExternalId__c,CORE_Session__c,CORE_Academic_Year__c,CreatedDate,LastModifiedDate FROM CORE_Intake__c];
        Test.startTest();
        string value = CORE_CUBE_CreateCsv.getListIntake(DateTime.newInstance(2021, 07, 01, 12, 0, 0));
        string testString = CORE_CUBE_CreateCsv.getHeaderIntake();
        testString +='\n';
        testString = testString+'"'+myNewIntake.Id+'","'+myNewIntake.CORE_Intake_ExternalId__c+'","'+myNewIntake.CORE_Intake_code__c+'","'+myNewIntake.Name+'","'+myNewIntake.CORE_Parent_Level__r.CORE_Level_ExternalId__c+'","'+myNewIntake.CORE_Session__c+'","'+myNewIntake.CORE_Academic_Year__c+'","'+myNewIntake.CreatedDate+'","'+myNewIntake.LastModifiedDate+'"\n';
        testString = testString.replace('"null"','');
        testString = testString.replace('"true"','true');
        testString = testString.replace('"false"','false');
        System.debug('testString '+ testString);
        System.debug('value '+ value);
        Test.stopTest();
        System.assertEquals(value,testString);
    }
    
    @isTest
    public static void getHeaderLevelTest(){
        String testHeader = 'Id,Uuid,Code,Label,Parent_uuid,Study_cycle,Year,CreatedOn,ModifiedOn';
        String value = CORE_CUBE_CreateCsv.getHeaderLevel();
        System.assertEquals(value,testHeader);
    }

    @isTest
    public static void getListLevelTest(){
        CORE_Level__c myLevel =  [SELECT Id,CORE_Level_ExternalId__c,CORE_Level_code__c,Name, CORE_Parent_Curriculum__r.CORE_Curriculum_ExternalId__c,CORE_Study_Cycle__c,CORE_Year__c,CreatedDate,LastModifiedDate FROM CORE_Level__c];
        Test.setCreatedDate(myLevel.Id, DateTime.newInstance(2021,07,10));
        CORE_Level__c myNewLevel =  [SELECT Id,CORE_Level_ExternalId__c,CORE_Level_code__c,Name, CORE_Parent_Curriculum__r.CORE_Curriculum_ExternalId__c,CORE_Study_Cycle__c,CORE_Year__c,CreatedDate,LastModifiedDate FROM CORE_Level__c];
        Test.startTest();
        string value = CORE_CUBE_CreateCsv.getListLevel(DateTime.newInstance(2021, 07, 01, 12, 0, 0));
        string testString = CORE_CUBE_CreateCsv.getHeaderLevel();
        testString +='\n';
        testString = testString+'"'+myNewLevel.Id+'","'+myNewLevel.CORE_Level_ExternalId__c+'","'+myNewLevel.CORE_Level_code__c+'","'+myNewLevel.Name+'","'+myNewLevel.CORE_Parent_Curriculum__r.CORE_Curriculum_ExternalId__c+'","'+myNewLevel.CORE_Study_Cycle__c+'","'+myNewLevel.CORE_Year__c+'","'+myNewLevel.CreatedDate+'","'+myNewLevel.LastModifiedDate+'"\n';
        testString = testString.replace('"null"','');
        testString = testString.replace('"true"','true');
        testString = testString.replace('"false"','false');
        Test.stopTest();
        System.assertEquals(value,testString);
    }

    @isTest
    public static void getListCurriculumTest(){
        CORE_Curriculum__c myCurriculum =  [SELECT Id,CORE_Curriculum_ExternalId__c,CORE_Curriculum_code__c,Name, CORE_Parent_Study_Area__r.CORE_Study_Area_ExternalId__c,CreatedDate,LastModifiedDate FROM CORE_Curriculum__c];
        Test.setCreatedDate(myCurriculum.Id, DateTime.newInstance(2021,07,10));
        CORE_Curriculum__c myNewCurriculum =[SELECT Id,CORE_Curriculum_ExternalId__c,CORE_Curriculum_code__c,Name, CORE_Parent_Study_Area__r.CORE_Study_Area_ExternalId__c,CreatedDate,LastModifiedDate FROM CORE_Curriculum__c];
        Test.startTest();
        string value = CORE_CUBE_CreateCsv.getListCurriculum(DateTime.newInstance(2021, 07, 01, 12, 0, 0));
        string testString = CORE_CUBE_CreateCsv.getHeaderCurriculum();
        testString +='\n';
        testString = testString+'"'+myNewCurriculum.Id+'","'+myNewCurriculum.CORE_Curriculum_ExternalId__c+'","'+myNewCurriculum.CORE_Curriculum_code__c+'","'+myNewCurriculum.Name+'","'+myNewCurriculum.CORE_Parent_Study_Area__r.CORE_Study_Area_ExternalId__c+'","'+myNewCurriculum.CreatedDate+'","'+myNewCurriculum.LastModifiedDate+'"\n';
        testString = testString.replace('"null"','');
        testString = testString.replace('"true"','true');
        testString = testString.replace('"false"','false');
        Test.stopTest();
        System.assertEquals(value,testString);
    }

    @isTest
    public static void getHeaderCurriculumTest(){
        String testHeader = 'Id,Uuid,Code,Label,Parent_uuid,CreatedOn,ModifiedOn';
        String value = CORE_CUBE_CreateCsv.getHeaderCurriculum();
        System.assertEquals(value,testHeader);
    }
    @isTest
    public static void getListStudyAreaTest(){
        CORE_Study_Area__c myStudyArea = [SELECT Id,CORE_Study_Area_ExternalId__c,CORE_Study_Area_code__c,Name,CORE_Parent_Business_Unit__r.CORE_Business_Unit_ExternalId__c,CreatedDate,LastModifiedDate FROM CORE_Study_Area__c];
        Test.setCreatedDate(myStudyArea.Id, DateTime.newInstance(2021,07,10));
        CORE_Study_Area__c myNewStudyArea = [SELECT Id,CORE_Study_Area_ExternalId__c,CORE_Study_Area_code__c,Name,CORE_Parent_Business_Unit__r.CORE_Business_Unit_ExternalId__c,CreatedDate,LastModifiedDate FROM CORE_Study_Area__c];
        Test.startTest();
        string value = CORE_CUBE_CreateCsv.getListStudyArea(DateTime.newInstance(2021, 07, 01, 12, 0, 0));
        string testString = CORE_CUBE_CreateCsv.getHeaderStudyArea();
        testString +='\n';
        testString = testString+'"'+myNewStudyArea.Id+'","'+myNewStudyArea.CORE_Study_Area_ExternalId__c+'","'+myNewStudyArea.CORE_Study_Area_code__c+'","'+myNewStudyArea.Name+'","'+myNewStudyArea.CORE_Parent_Business_Unit__r.CORE_Business_Unit_ExternalId__c+'","'+myNewStudyArea.CreatedDate+'","'+myNewStudyArea.LastModifiedDate+'"\n';
        testString = testString.replace('"null"','');
        testString = testString.replace('"true"','true');
        testString = testString.replace('"false"','false');
        Test.stopTest();
        System.assertEquals(value,testString);
    }

    @isTest
    public static void getHeaderStudyAreaTest(){
        String testHeader = 'Id,Uuid,Code,Label,Parent_uuid,CreatedOn,ModifiedOn';
        String value = CORE_CUBE_CreateCsv.getHeaderStudyArea();
        System.assertEquals(value,testHeader);
    }
    @isTest
    public static void getListBusinessUnitTest(){
        CORE_Business_Unit__c myBusinessUnit = [SELECT Id,CORE_Business_Unit_ExternalId__c,CORE_Business_Unit_code__c,Name,CORE_Parent_Division__r.CORE_Division_ExternalId__c,CreatedDate,LastModifiedDate FROM CORE_Business_Unit__c];
        Test.setCreatedDate(myBusinessUnit.Id, DateTime.newInstance(2021,07,10));
        CORE_Business_Unit__c myNewBusinessUnit = [SELECT Id,CORE_Business_Unit_ExternalId__c,CORE_Business_Unit_code__c,Name,CORE_Parent_Division__r.CORE_Division_ExternalId__c,CreatedDate,LastModifiedDate FROM CORE_Business_Unit__c];
        Test.startTest();
        string value = CORE_CUBE_CreateCsv.getListBusinessUnit(DateTime.newInstance(2021, 07, 01, 12, 0, 0));
        string testString = CORE_CUBE_CreateCsv.getHeaderBusinessUnit();
        testString +='\n';
        testString = testString+'"'+myNewBusinessUnit.Id+'","'+myNewBusinessUnit.CORE_Business_Unit_ExternalId__c+'","'+myNewBusinessUnit.CORE_Business_Unit_code__c+'","'+myNewBusinessUnit.Name+'","'+myNewBusinessUnit.CORE_Parent_Division__r.CORE_Division_ExternalId__c+'","'+myNewBusinessUnit.CreatedDate+'","'+myNewBusinessUnit.LastModifiedDate+'"\n';
        testString = testString.replace('"null"','');
        testString = testString.replace('"true"','true');
        testString = testString.replace('"false"','false');
        Test.stopTest();
        System.assertEquals(value,testString);

    }
    @isTest
    public static void getHeaderBusinessUnitTest(){
        String testHeader = 'Id,Uuid,Code,Label,Parent_uuid,CreatedOn,ModifiedOn';
        String value = CORE_CUBE_CreateCsv.getHeaderBusinessUnit();
        System.assertEquals(value,testHeader);
    }

    @isTest
    public static void getListDivisionTest(){
        CORE_Division__c myDivision = [SELECT Id,CORE_Division_ExternalId__c,CORE_Division_code__c,Name,CreatedDate,LastModifiedDate FROM CORE_Division__c];
        Test.setCreatedDate(myDivision.Id, DateTime.newInstance(2021,07,10));
        CORE_Division__c myNewDivision = [SELECT Id,CORE_Division_ExternalId__c,CORE_Division_code__c,Name,CreatedDate,LastModifiedDate FROM CORE_Division__c];
        Test.startTest();
        string value = CORE_CUBE_CreateCsv.getListDivision(DateTime.newInstance(2021, 07, 01, 12, 0, 0));
        string testString = CORE_CUBE_CreateCsv.getHeaderDivision();
        testString +='\n';
        testString = testString+'"'+myNewDivision.Id+'","'+myNewDivision.CORE_Division_code__c+'","'+myNewDivision.CORE_Division_code__c+'","'+myNewDivision.Name+'","'+myNewDivision.CreatedDate+'","'+myNewDivision.LastModifiedDate+'"\n';
        testString = testString.replace('"null"','');
        testString = testString.replace('"true"','true');
        testString = testString.replace('"false"','false');
        Test.stopTest();
        System.assertEquals(value,testString);
    }

    @isTest
    public static void getHeaderDivisionTest(){
        String testHeader = 'Id,Uuid,Code,Label,CreatedOn,ModifiedOn';
        String value = CORE_CUBE_CreateCsv.getHeaderDivision();
        System.assertEquals(value,testHeader);
    }

    @isTest
    public static void getListPointOfContactTest(){
        CORE_Point_of_Contact__c pc =new CORE_Point_of_Contact__c();
        Opportunity myOpp = [SELECT Id FROM Opportunity];
        pc.CORE_Opportunity__c = myOpp.Id;
        pc.CORE_Source__c = 'source test';
        pc.CORE_Insertion_Mode__c = 'CORE_PKL_Manual';
        pc.CORE_Nature__c ='CORE_PKL_Online';
        pc.CORE_Enrollment_Channel__c= 'CORE_PKL_Direct';
        pc.CORE_Capture_Channel__c= 'CORE_PKL_CIO';
        pc.CORE_Origin__c= 'origine test';
        pc.Campaign_Medium__c='CORE_PKL_Directory';
        pc.CORE_Campaign_Source__c='source campaign test';
        pc.CORE_Campaign_Term__c='term campaign test';
        pc.CORE_Campaign_Content__c='content campaign test';
        pc.CORE_Campaign_Name__c='name campaign test';
        pc.CORE_Device__c='CORE_PKL_Computer';
        pc.CORE_First_visit_datetime__c=DateTime.newInstance(2021, 07, 01, 12, 0, 0);
        pc.CORE_IP_address__c='IP adresse';
        pc.CORE_First_page__c='first page test';
        pc.CORE_Latitude__c='latitude test';
        pc.CORE_Longitude__c='longitude test';
        pc.CreatedDate = DateTime.newInstance(2021, 07, 01, 12, 0, 0);
        pc.LastModifiedDate = DateTime.newInstance(2021, 07, 07, 12, 0, 0);
        pc.CORE_Proactive_Prompt__c='CORE_Proactive_Prompt__cTest';
        pc.CORE_Proactive_Engaged__c='CORE_Proactive_Engaged__cTest'; 
        pc.CORE_Chat_With__c='CORE_Chat_With__cTest';

        insert pc;
        Test.setCreatedDate(pc.Id, DateTime.newInstance(2021,07,10));
        CORE_Point_of_Contact__c myNewPointContact = [SELECT Id,CORE_Opportunity__c,CORE_Source__c,CORE_Insertion_Mode__c,CORE_Nature__c,CORE_Enrollment_Channel__c,CORE_Capture_Channel__c,CORE_Origin__c,Campaign_Medium__c,CORE_Campaign_Source__c,CORE_Campaign_Term__c,CORE_Campaign_Content__c,CORE_Campaign_Name__c,CORE_Device__c,CORE_First_visit_datetime__c,CORE_First_page__c,CORE_IP_address__c,CORE_Latitude__c,CORE_Longitude__c,CreatedDate,LastModifiedDate, CORE_Proactive_Prompt__c, CORE_Proactive_Engaged__c, CORE_Chat_With__c FROM CORE_Point_of_Contact__c];
        Test.startTest();
        string value = CORE_CUBE_CreateCsv.getListPointOfContact(DateTime.newInstance(2021, 07, 01, 12, 0, 0));
        string testString = CORE_CUBE_CreateCsv.getHeaderPointOfContact();
        testString +='\n';
        testString = testString+'"'+myNewPointContact.Id+'","'+myNewPointContact.CORE_Opportunity__c+'","'+myNewPointContact.CORE_Source__c+'","'+myNewPointContact.CORE_Insertion_Mode__c+'","'+myNewPointContact.CORE_Nature__c+'","'+myNewPointContact.CORE_Enrollment_Channel__c+'","'+myNewPointContact.CORE_Capture_Channel__c+'","'+myNewPointContact.CORE_Origin__c+'","'+myNewPointContact.Campaign_Medium__c+'","'+myNewPointContact.CORE_Campaign_Source__c+'","'+myNewPointContact.CORE_Campaign_Term__c+'","'+myNewPointContact.CORE_Campaign_Content__c+'","'+myNewPointContact.CORE_Campaign_Name__c+'","'+myNewPointContact.CORE_Device__c+'","'+myNewPointContact.CORE_First_visit_datetime__c+'","'+myNewPointContact.CORE_First_page__c+'","'+myNewPointContact.CORE_IP_address__c+'","'+myNewPointContact.CORE_Latitude__c+'","'+myNewPointContact.CORE_Longitude__c+'","'+myNewPointContact.CreatedDate+'","'+myNewPointContact.LastModifiedDate+'","'+myNewPointContact.CORE_Proactive_Prompt__c +'","'+myNewPointContact.CORE_Proactive_Engaged__c + '","'+myNewPointContact.CORE_Chat_With__c +'"\n';
        testString = testString.replace('"null"','');
        testString = testString.replace('"true"','true');
        testString = testString.replace('"false"','false');
        Test.stopTest();
        System.assertEquals(value,testString);
    }
    @isTest
    public static void getHeaderPointOfContactTest(){
        String testHeader = 'Point_contact_id,Opportunity_id,Source_uuid,Insertion_mode,Nature,Enrollment_Channel,Capture_Channel,Origin,Campaign_medium,Campaign_source,Campaign_term,Campaign_content,Campaign_name,Device,First_visit,First_page,IP,Latitude,Longitude,CreatedOn,ModifiedOn,proactivePrompt,proactiveEngaged,chatWith';
        String value = CORE_CUBE_CreateCsv.getHeaderPointOfContact();
        System.assertEquals(value,testHeader);
    }

    @isTest
    public static void getListSubStatusJourneyTest(){

        /*Opportunity myOpp = new Opportunity();
        myOpp.Name ='test opp';
        myOpp.StageName='Applicant';
        myOpp.CORE_OPP_Sub_Status__c='Applicant - No Show';
        myOpp.CloseDate=Date.newInstance(2021, 10, 01);
        insert myOpp;
 
        CORE_Sub_Status_Timestamp_History__c sub =new CORE_Sub_Status_Timestamp_History__c();
        sub.CORE_Opportunity__c = myOpp.Id;
        sub.CORE_Status__c = 'Admitted';
        sub.CORE_Sub_status__c = 'Admitted - Admit';
        sub.CORE_Modification_Sub_status_Date_time__c =DateTime.newInstance(2021, 07, 05, 12, 0, 0);
        sub.CreatedDate= DateTime.newInstance(2021, 07, 01, 12, 0, 0);
        sub.LastModifiedDate= DateTime.newInstance(2021, 07, 07, 12, 0, 0);
        insert sub;*/
        
        List<CORE_Sub_Status_Timestamp_History__c> myNewSubStatusTimestamp = [SELECT Id,CORE_Opportunity__c,CORE_Status__c,CORE_Sub_status__c,CORE_Modification_Sub_status_Date_time__c,CreatedDate,LastModifiedDate FROM CORE_Sub_Status_Timestamp_History__c WHERE CORE_Status__c NOT IN :status AND LastModifiedDate<TODAY];
        Test.startTest();
        string value = CORE_CUBE_CreateCsv.getListSubStatusJourney(DateTime.newInstance(2021, 07, 01, 12, 1, 0));
        string testString = CORE_CUBE_CreateCsv.getHeaderSubStatusJourney();
        testString +='\n';
        //testString = testString+'"'+myNewSubStatusTimestamp.CORE_Opportunity__c+'","'+myNewSubStatusTimestamp.CORE_Status__c+'","'+myNewSubStatusTimestamp.CORE_Sub_status__c+'","'+myNewSubStatusTimestamp.CORE_Modification_Sub_status_Date_time__c+'","'+myNewSubStatusTimestamp.CreatedDate+'","'+myNewSubStatusTimestamp.LastModifiedDate+'"\n';
        testString = testString.replace('"null"','');
        testString = testString.replace('"true"','true');
        testString = testString.replace('"false"','false');
        Test.stopTest();
        System.assertEquals(value,testString);
    }
    @isTest
    public static void getHeaderSubStatusJourneyTest(){
        String testHeader = 'Opportunity_ID,Status,Sub_Status,Datetime,CreatedOn,ModifiedOn';
        String value = CORE_CUBE_CreateCsv.getHeaderSubStatusJourney();
        System.assertEquals(value,testHeader);
    }

    @isTest
    public static void getListStatusJourneyTest(){
        /*Opportunity myOpportunity = [SELECT Id,CORE_OPP_Legacy_CRM_Id__c,CORE_OPP_Main_Opportunity__c,RecordType.Name,CORE_OPP_Academic_Year__c,Study_mode__c,CreatedDate,LastModifiedDate,CORE_OPP_Application_Online__c,AccountId,CORE_OPP_Business_Unit__r.CORE_Business_Unit_ExternalId__c,CORE_OPP_Study_Area__r.CORE_Study_Area_ExternalId__c,CORE_OPP_Curriculum__r.CORE_Curriculum_ExternalId__c,CORE_OPP_Level__r.CORE_Level_ExternalId__c,CORE_OPP_Intake__r.CORE_Intake_ExternalId__c,CORE_Reach_Date__c,CORE_OPP_Agent_Perception__c FROM Opportunity];
        List<CORE_Status_Timestamp_History__c> myStatusTimestamp = [SELECT Id,CORE_Opportunity__c,CORE_Status__c,CORE_Modification_Status_Date_time__c,CreatedDate,LastModifiedDate FROM CORE_Status_Timestamp_History__c WHERE CORE_Status__c NOT IN :status];
        Test.setCreatedDate(myOpportunity.Id, DateTime.newInstance(2021,07,10));
        Test.setCreatedDate(myStatusTimestamp[0].Id, DateTime.newInstance(2021,07,10));
        List<CORE_Status_Timestamp_History__c>  myNewStatusTimestamp = [SELECT Id,CORE_Opportunity__c,CORE_Status__c,CORE_Modification_Status_Date_time__c,CreatedDate,LastModifiedDate FROM CORE_Status_Timestamp_History__c WHERE CORE_Status__c NOT IN :status];
        */Test.startTest();
        string value = CORE_CUBE_CreateCsv.getListStatusJourney(DateTime.newInstance(2021, 07, 01, 12, 1, 0));
        string testString = CORE_CUBE_CreateCsv.getHeaderStatusJourney();
        testString +='\n';
        //testString = testString+'"'+myNewStatusTimestamp[0].CORE_Opportunity__c+'","'+myNewStatusTimestamp[0].CORE_Status__c+'","'+myNewStatusTimestamp[0].CORE_Modification_Status_Date_time__c+'","'+myNewStatusTimestamp[0].CreatedDate+'","'+myNewStatusTimestamp[0].LastModifiedDate+'"\n';
        testString = testString.replace('"null"','');
        testString = testString.replace('"true"','true');
        testString = testString.replace('"false"','false');
        Test.stopTest();
        System.assertEquals(value,testString);
    }
    @isTest
    public static void getHeaderStatusJourneyTest(){
        String testHeader = 'Opportunity_ID,Status,Datetime,CreatedOn,ModifiedOn';
        String value = CORE_CUBE_CreateCsv.getHeaderStatusJourney();
        System.assertEquals(value,testHeader);
    }

    @isTest
    public static void getListOpportunityTest(){
        /*Opportunity myOpportunity = [SELECT Id,CORE_OPP_Legacy_CRM_Id__c,CORE_OPP_Main_Opportunity__c,RecordType.Name,CORE_OPP_Academic_Year__c,Study_mode__c,CreatedDate,LastModifiedDate,CORE_OPP_Application_Online__c,AccountId,CORE_OPP_Business_Unit__r.CORE_Business_Unit_ExternalId__c,CORE_OPP_Study_Area__r.CORE_Study_Area_ExternalId__c,CORE_OPP_Curriculum__r.CORE_Curriculum_ExternalId__c,CORE_OPP_Level__r.CORE_Level_ExternalId__c,CORE_OPP_Intake__r.CORE_Intake_ExternalId__c,CORE_Reach_Date__c,CORE_OPP_Agent_Perception__c FROM Opportunity];
        Test.setCreatedDate(myOpportunity.Id, DateTime.newInstance(2021,07,10));
        Opportunity myNewOpportunity = [SELECT Id,CORE_OPP_Legacy_CRM_Id__c,CORE_OPP_Main_Opportunity__c,RecordType.Name,CORE_OPP_Academic_Year__c,Study_mode__c,CreatedDate,LastModifiedDate,CORE_OPP_Application_Online__c,AccountId,CORE_OPP_Business_Unit__r.CORE_Business_Unit_ExternalId__c,CORE_OPP_Study_Area__r.CORE_Study_Area_ExternalId__c,CORE_OPP_Curriculum__r.CORE_Curriculum_ExternalId__c,CORE_OPP_Level__r.CORE_Level_ExternalId__c,CORE_OPP_Intake__r.CORE_Intake_ExternalId__c,CORE_Reach_Date__c,CORE_OPP_Agent_Perception__c FROM Opportunity];
        */Test.startTest();
        string value = CORE_CUBE_CreateCsv.getListOpportunity(DateTime.newInstance(2021, 07, 01, 12, 1, 0));
        string testString = CORE_CUBE_CreateCsv.getHeaderOpportunity();        
        testString +='\n';
        //testString = testString+'"'+myNewOpportunity.Id+'","'+myNewOpportunity.CORE_OPP_Legacy_CRM_Id__c+'","'+myNewOpportunity.CORE_OPP_Main_Opportunity__c+'","'+myNewOpportunity.RecordType.Name+'","'+myNewOpportunity.CORE_OPP_Academic_Year__c+'","'+myNewOpportunity.Study_mode__c+'","'+myNewOpportunity.CreatedDate+'","'+myNewOpportunity.LastModifiedDate+'","'+myNewOpportunity.CORE_OPP_Application_Online__c+'","'+myNewOpportunity.AccountId+'","'+myNewOpportunity.CORE_OPP_Business_Unit__r.CORE_Business_Unit_ExternalId__c+'","'+myNewOpportunity.CORE_OPP_Study_Area__r.CORE_Study_Area_ExternalId__c+'","'+myNewOpportunity.CORE_OPP_Curriculum__r.CORE_Curriculum_ExternalId__c+'","'+myNewOpportunity.CORE_OPP_Level__r.CORE_Level_ExternalId__c+'","'+myNewOpportunity.CORE_OPP_Intake__r.CORE_Intake_ExternalId__c+'","'+myNewOpportunity.CORE_Reach_Date__c+'","'+myNewOpportunity.CORE_OPP_Agent_Perception__c+'"\n';
        testString = testString.replace('"null"','');
        testString = testString.replace('"true"','true');
        testString = testString.replace('"false"','false');
        Test.stopTest();
        System.assertEquals(value,testString);
    }

    @isTest
    public static void getHeaderOpportunityTest(){
        String testHeader = 'Opportunity_id,Opportunity_legacy_id,Main,Processus_code,Academic_year,Study_mode,CreatedOn,ModifiedOn,application_online,Contact_id,Business_unit_uuid,Study_Area_uuid,Curriculum_uuid,Level_uuid,Intake_uuid,Reached_lead,qualification';
        String value = CORE_CUBE_CreateCsv.getHeaderOpportunity();
        System.assertEquals(value,testHeader);
    }

    @isTest
    public static void getListPersonAccountYopTest(){
        Account myAccount = [SELECT Id,	CORE_LegacyCRMID__c,PersonEmail,CORE_Country_of_birth__pc,CORE_Main_Nationality__pc,FirstName,	CORE_Gender__pc,CreatedDate,LastModifiedDate  FROM Account];
        Test.setCreatedDate(myAccount.Id, DateTime.newInstance(2021,07,10));
        myAccount.PersonEmail='test@yopmail.com';
        update myAccount;
        Account myNewAccount = [SELECT Id,	CORE_LegacyCRMID__c,PersonEmail,CORE_Country_of_birth__pc,CORE_Main_Nationality__pc,FirstName,	CORE_Gender__pc,CreatedDate,LastModifiedDate  FROM Account];
        Test.startTest();
        string value = CORE_CUBE_CreateCsv.getListPersonAccount(DateTime.newInstance(2021, 07, 01, 12, 0, 0));
        string testString = CORE_CUBE_CreateCsv.getHeaderPersonAccount();
        testString +='\n';
        Test.stopTest();
        System.assertEquals(value,testString);
    }
    @isTest
    public static void getListPersonAccountTest(){
        Account myAccount = [SELECT Id,	CORE_LegacyCRMID__c,PersonEmail,CORE_Country_of_birth__pc,CORE_Main_Nationality__pc,FirstName,	CORE_Gender__pc,CreatedDate,LastModifiedDate  FROM Account];
        Test.setCreatedDate(myAccount.Id, DateTime.newInstance(2021,07,10));
        Account myNewAccount = [SELECT Id,	CORE_LegacyCRMID__c,PersonEmail,CORE_Country_of_birth__pc,CORE_Main_Nationality__pc,FirstName,	CORE_Gender__pc,CreatedDate,LastModifiedDate  FROM Account];
        Test.startTest();
        string value = CORE_CUBE_CreateCsv.getListPersonAccount(DateTime.newInstance(2021, 07, 01, 12, 0, 0));
        string testString = CORE_CUBE_CreateCsv.getHeaderPersonAccount();
        testString +='\n';
        testString = testString+'"'+myNewAccount.Id+'","'+myNewAccount.CORE_LegacyCRMID__c+'","'+myNewAccount.PersonEmail.substringAfter('@')+'","'+myNewAccount.CORE_Country_of_birth__pc+'","'+myNewAccount.CORE_Main_Nationality__pc+'","'+myNewAccount.FirstName+'","'+myNewAccount.CORE_Gender__pc+'","'+myNewAccount.CreatedDate+'","'+myNewAccount.LastModifiedDate+'"\n';
        testString = testString.replace('"null"','');
        testString = testString.replace('"true"','true');
        testString = testString.replace('"false"','false');
        Test.stopTest();
        System.assertEquals(value,testString);
    }
    @isTest
    public static void getHeaderPersonAccountTest(){
        String testHeader = 'Person_account_id,Person_account_legacy_id,Domain,Country,Nationality,Firstname,Gender,CreatedOn,ModifiedOn';
        String value = CORE_CUBE_CreateCsv.getHeaderPersonAccount();
        System.assertEquals(value,testHeader);
    }

    @isTest
    public static  void createLogTest(){ 
        string stringTocsv ='Bonjour,Hello\nReBonjour,ReHello';
        string tab = 'myTesTab';
        boolean isPassed = true;
        string message ='test message';
        DateTime todaysDate = System.today();
        String todaysDateStr = todaysDate.format('_yyyy_MM_dd');
        DateTime yesterday = DateTime.Now().AddDays(-1);

        Test.startTest();
        CORE_CUBE_CreateCsv.LogDatas logDatas = new CORE_CUBE_CreateCsv.LogDatas();
        logDatas.dtBeforeFileGeneration = System.today();
        logDatas.dtAfterFileGeneration = System.today();
        logDatas.dtBeforeHttpRequest = System.today();
        logDatas.dtAfterHttpRequest = System.today();
        logDatas.dtBeforeFileEncryption = System.today();
        logDatas.dtAfterFileEncryption = System.today();
        logDatas.dtAfterError = System.today();
        logDatas.dtFutureMethodStart = System.today();
        logDatas.dtFutureMethodEnd = System.today();

        CORE_CUBE_CreateCsv.createLog(stringToCsv, tab, isPassed, 200, message, logDatas);
        CORE_CUBE_Log__c myTestLog = new CORE_CUBE_Log__c();
        myTestLog.Name = tab + todaysDateStr;
        myTestLog.IsPassed__c = isPassed;
        myTestLog.ErrorMessage__c = message;
        myTestLog.ObjectName__c =tab;
        myTestLog.date_donne_extraite__c = yesterday;
        myTestLog.dateOk__c = todaysDate;
        CORE_CUBE_Log__c myLog = [SELECT Id,Name,IsPassed__c,ErrorMessage__c,ObjectName__c,date_donne_extraite__c,dateOk__c FROM CORE_CUBE_Log__c];
        Attachment myAttachement = [SELECT Id,Name,Body,ParentId FROM Attachment];
        Test.stopTest();

        System.assertEquals(myTestLog.Name,myLog.Name);
        System.assertEquals(myTestLog.IsPassed__c,myLog.IsPassed__c);
        System.assertEquals(myTestLog.ErrorMessage__c,myLog.ErrorMessage__c);
        System.assertEquals(myTestLog.ObjectName__c,myLog.ObjectName__c);
        System.assertEquals(myTestLog.date_donne_extraite__c,myLog.date_donne_extraite__c);
        System.assertEquals(myTestLog.dateOk__c,myLog.dateOk__c);
        System.assertEquals(myAttachement.Name,String.valueOf(tab+'.csv'));
        System.assertEquals(myAttachement.Body,Blob.valueOf(stringToCsv));
        System.assertEquals(myAttachement.ParentId,myLog.Id);
    }

    @isTest
    public static void getMapPicklistTest(){
        Map<String,String> myPicklist = CORE_CUBE_CreateCsv.getMapPicklist('Opportunity','CORE_Deleted_Reason__c');
        Map<string,string> myMap = new Map<string,string>();
        myMap.put('CORE_PKL_Test from website','Test from website');
        myMap.put('CORE_PKL_Duplicate','Duplicate');
        myMap.put('CORE_PKL_Manual error entry','Manual error entry');
        myMap.put('CORE_PKL_Bot','Bot');
        System.assertEquals(myPicklist,myMap);
    }

    @isTest
    public static void getPickListTest(){
        String result = CORE_CUBE_CreateCsv.getPickList('English','Opportunity','CORE_Deleted_Reason__c');
        String testValue = '"CORE_Deleted_Reason__c","English","CORE_PKL_Test from website","Test from website"\n"CORE_Deleted_Reason__c","English","CORE_PKL_Duplicate","Duplicate"\n"CORE_Deleted_Reason__c","English","CORE_PKL_Manual error entry","Manual error entry"\n"CORE_Deleted_Reason__c","English","CORE_PKL_Bot","Bot"\n';
        System.assertEquals(result,testValue);
    }

    /*@isTest
    public static void createAllPicklistTest(){
        String myString  = CORE_CUBE_CreateCsv.createAllPicklist();
        System.assertEquals(myString.length(),13659);
    }*/

    @isTest
    public static void getListDeletedTest(){
        /*Account myAccount = [SELECT Id,	LastModifiedDate  FROM Account];
        Test.setCreatedDate(myAccount.Id, DateTime.newInstance(2021,07,10));
        Opportunity myOpportunity = [SELECT Id,LastModifiedDate FROM Opportunity];
        Test.setCreatedDate(myOpportunity.Id, DateTime.newInstance(2021,07,10));
        Account myNewAccount = [SELECT Id,	LastModifiedDate  FROM Account];
        Opportunity myNewOpportunity = [SELECT Id,LastModifiedDate FROM Opportunity];
        delete myNewOpportunity;
        delete myNewAccount;*/
        Test.startTest();
        List<Opportunity> myDeletedOpportunity = [SELECT Id,LastModifiedDate,IsDeleted FROM Opportunity WHERE IsDeleted= true ALL ROWS];
        List<Account> myDeletedAccount = [SELECT Id,	LastModifiedDate,IsDeleted FROM Account WHERE IsDeleted= true ALL ROWS];
        string value = CORE_CUBE_CreateCsv.getListDeleted(DateTime.newInstance(2021, 07, 01, 12, 0, 0));
        string testString = CORE_CUBE_CreateCsv.getHeaderDeleted();
        testString +='\n';
        if(myDeletedOpportunity.size()>0){
            testString = testString+'"Account","'+myDeletedAccount[0].Id+'","'+myDeletedAccount[0].LastModifiedDate+'"\n';
            testString = testString+'"Opportunity","'+myDeletedOpportunity[0].Id+'","'+myDeletedOpportunity[0].LastModifiedDate+'"\n';
        }
        testString = testString.replace('"null"','');
        testString = testString.replace('"true"','true');
        testString = testString.replace('"false"','false');
        Test.stopTest();
        System.assertEquals(value,testString);
    }

    @isTest
    public static void getHeaderDeletedTest(){
        String testHeader = 'Type,Object_id,Deletion_date';
        String value = CORE_CUBE_CreateCsv.getHeaderDeleted();
        System.assertEquals(value,testHeader);
    }
    @isTest
    public static void createAllPicklist(){
        String language='English';

        
        String generatedCSVFile = CORE_CUBE_CreateCsv.getHeaderPicklist();
        generatedCSVFile +='\n';
        generatedCSVFile += CORE_CUBE_CreateCsv.getPickList(language,'Opportunity','StageName');
        generatedCSVFile += CORE_CUBE_CreateCsv.getPickList(language,'Opportunity','CORE_OPP_Sub_Status__c');
        generatedCSVFile += CORE_CUBE_CreateCsv.getPickList(language,'CORE_Level__c','CORE_Year__c');
        generatedCSVFile += CORE_CUBE_CreateCsv.getPickList(language,'CORE_Level__c','CORE_Study_Cycle__c');
        generatedCSVFile += CORE_CUBE_CreateCsv.getPickList(language,'Opportunity','CORE_OPP_Academic_Year__c');
        generatedCSVFile += CORE_CUBE_CreateCsv.getPickList(language,'CORE_Intake__c','CORE_Session__c');
        generatedCSVFile += CORE_CUBE_CreateCsv.getPickList(language,'CORE_Curriculum__c','CORE_Study_Mode__c');
        generatedCSVFile += CORE_CUBE_CreateCsv.getPickList(language,'CORE_Point_of_Contact__c','CORE_Nature__c');
        generatedCSVFile += CORE_CUBE_CreateCsv.getPickList(language,'CORE_Point_of_Contact__c','CORE_Capture_Channel__c');
        generatedCSVFile += CORE_CUBE_CreateCsv.getPickList(language,'CORE_Point_of_Contact__c','Campaign_Medium__c');
        generatedCSVFile += CORE_CUBE_CreateCsv.getPickList(language,'CORE_Point_of_Contact__c','CORE_Device__c');
        generatedCSVFile += CORE_CUBE_CreateCsv.getPickList(language,'CORE_Point_of_Contact__c','CORE_Insertion_Mode__c');
        generatedCSVFile += CORE_CUBE_CreateCsv.getPickList(language,'Opportunity','CORE_OPP_Reached_Lead_Qualification__c');
        generatedCSVFile += CORE_CUBE_CreateCsv.getPickList(language,'Contact','CORE_Gender__c');
        generatedCSVFile += CORE_CUBE_CreateCsv.getPickList(language,'Contact','CORE_Country_of_birth__c');
        generatedCSVFile += CORE_CUBE_CreateCsv.getPickList(language,'Contact','CORE_Main_Nationality__c');
        generatedCSVFile += CORE_CUBE_CreateCsv.getPickList(language,'CORE_Point_of_Contact__c','CORE_Enrollment_Channel__c');
        String value = CORE_CUBE_CreateCsv.createAllPicklist();

        System.assertEquals(value,generatedCSVFile);
    }

    @IsTest
    private static void sendToEndpointTest() {
        Test.setMock(HttpCalloutMock.class, new CubeCalloutMock());
        Test.startTest();
        CORE_CUBE_CreateCsv.sendToEndpoint('Opportunity', 'test', System.now(), System.now());
        Test.stopTest();
        // runs callout and check results
        List<CORE_CUBE_Log__c> myLog = [SELECT Id,Name,IsPassed__c,ErrorMessage__c,ObjectName__c,date_donne_extraite__c,dateOk__c FROM CORE_CUBE_Log__c];
        System.assertEquals(1, myLog.size());
    }

    global class CubeCalloutMock implements HttpCalloutMock {
        global HttpResponse respond(HttpRequest req) {
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"status":"success"}');
        res.setStatusCode(200);
        return res;
        }
    }
}