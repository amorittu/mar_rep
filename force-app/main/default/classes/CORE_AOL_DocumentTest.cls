@IsTest
public with sharing class CORE_AOL_DocumentTest {
    private static final String AOL_EXTERNALID = 'setupAol';

    private static final String REQ_BODY = '{'
    + '"aolExternalId": "' + AOL_EXTERNALID + '",'
    + '"onlineApplicationDocumentUrl": "testUrl",'
    + '"aolDocuments": ['
    + '{'
    + '"externalId": "testExtId1",'
    + '"name": "testName1",'
    + '"externalUrl": "testExtUrl1",'
    + '"type": "Visa",'
    + '"createdDate": "2022-03-30",'
    + '"approvedDate": "2022-03-30",'
    + '"rejectedDate": "2022-03-30",'
    + '"additionalFields": {'
    + '"CORE_Commercial_Name__c": "test value 1"'
    + '}'
    + '},'
    + '{'
    + '"externalId": "testExtId2",'
    + '"name": "testName2",'
    + '"externalUrl": "testExtUrl1",'
    + '"type": "Visa",'
    + '"createdDate": "2022-03-30",'
    + '"approvedDate": "2022-03-30",'
    + '"rejectedDate": "2022-03-30",'
    + '"additionalFields": {'
    + '"CORE_Commercial_Name__c": "test value 2"'
    + '}'
    + '}'
    + '],'
    + '"aolProgressionFields": {'
    + '"aolProgression": 50.09,'
    + '"documentProgression": "Started",'
    + '"firstDocumentUploadDate": "2022-03-29",'
    + '"lastAolEventDate": "2022-03-29T08:47:39.689Z",'
    + '"lastDeaAolLoginDate": "2022-03-29T08:47:39.689Z",'
    + '"isAol": true,'
    + '"isAolOnlineRegistered": true,'
    + '"isAolRetailRegistered": true'
    + '}'
    + '}';

    private static final String REQ_BODY_ERROR_INVALID_ID = '{'
    + '"aolExternalId": "invalidID",'
    + '"onlineApplicationDocumentUrl": "testUrl",'
    + '"aolDocuments": ['
    + '{'
    + '"externalId": "testExtId1",'
    + '"name": "testName1",'
    + '"externalUrl": "testExtUrl1",'
    + '"type": "Visa",'
    + '"createdDate": "2022-03-30",'
    + '"approvedDate": "2022-03-30",'
    + '"rejectedDate": "2022-03-30",'
    + '"additionalFields": {'
    + '"CORE_Commercial_Name__c": "test value 1"'
    + '}'
    + '},'
    + '{'
    + '"externalId": "testExtId2",'
    + '"name": "testName2",'
    + '"externalUrl": "testExtUrl1",'
    + '"type": "Visa",'
    + '"createdDate": "2022-03-30",'
    + '"approvedDate": "2022-03-30",'
    + '"rejectedDate": "2022-03-30",'
    + '"additionalFields": {'
    + '"CORE_Commercial_Name__c": "test value 2"'
    + '}'
    + '}'
    + '],'
    + '"aolProgressionFields": {'
    + '"aolProgression": 50.09,'
    + '"documentProgression": "Started",'
    + '"firstDocumentUploadDate": "2022-03-29",'
    + '"lastAolEventDate": "2022-03-29T08:47:39.689Z",'
    + '"lastDeaAolLoginDate": "2022-03-29T08:47:39.689Z",'
    + '"isAol": true,'
    + '"isAolOnlineRegistered": true,'
    + '"isAolRetailRegistered": true'
    + '}'
    + '}';

    private static final String REQ_BODY_ERROR_NO_ID = '{'
    + '"aolExternalId": "",'
    + '"onlineApplicationDocumentUrl": "testUrl",'
    + '"aolDocuments": ['
    + '{'
    + '"externalId": "testExtId1",'
    + '"name": "testName1",'
    + '"externalUrl": "testExtUrl1",'
    + '"type": "Visa",'
    + '"createdDate": "2022-03-30",'
    + '"approvedDate": "2022-03-30",'
    + '"rejectedDate": "2022-03-30",'
    + '"additionalFields": {'
    + '"CORE_Commercial_Name__c": "test value 1"'
    + '}'
    + '},'
    + '{'
    + '"externalId": "testExtId2",'
    + '"name": "testName2",'
    + '"externalUrl": "testExtUrl1",'
    + '"type": "Visa",'
    + '"createdDate": "2022-03-30",'
    + '"approvedDate": "2022-03-30",'
    + '"rejectedDate": "2022-03-30",'
    + '"additionalFields": {'
    + '"CORE_Commercial_Name__c": "test value 2"'
    + '}'
    + '}'
    + '],'
    + '"aolProgressionFields": {'
    + '"aolProgression": 50.09,'
    + '"documentProgression": "Started",'
    + '"firstDocumentUploadDate": "2022-03-29",'
    + '"lastAolEventDate": "2022-03-29T08:47:39.689Z",'
    + '"lastDeaAolLoginDate": "2022-03-29T08:47:39.689Z",'
    + '"isAol": true,'
    + '"isAolOnlineRegistered": true,'
    + '"isAolRetailRegistered": true'
    + '}'
    + '}';

    private static final String REQ_BODY_ERROR = '{'
    + '"aolExternalId": "' + AOL_EXTERNALID + '",'
    + '"onlineApplicationDocumentUrl": "testUrl",'
    + '"aolDocuments": ['
    + '{'
    + '"externalId": "testExtId1",'
    + '"name": "testName1",'
    + '"externalUrl": "testExtUrl1",'
    + '"type": "invalid_value",'
    + '"createdDate": "2022-03-30",'
    + '"approvedDate": "2022-03-30",'
    + '"rejectedDate": "2022-03-30",'
    + '"additionalFields": {'
    + '"CORE_Commercial_Name__c": "test value 1"'
    + '}'
    + '},'
    + '{'
    + '"externalId": "testExtId2",'
    + '"name": "testName2",'
    + '"externalUrl": "testExtUrl1",'
    + '"type": "Visa",'
    + '"createdDate": "2022-03-30",'
    + '"approvedDate": "2022-03-30",'
    + '"rejectedDate": "2022-03-30",'
    + '"additionalFields": {'
    + '"CORE_Commercial_Name__c": "test value 2"'
    + '}'
    + '}'
    + '],'
    + '"aolProgressionFields": {'
    + '"aolProgression": 50.09,'
    + '"documentProgression": "Started",'
    + '"firstDocumentUploadDate": "2022-03-29",'
    + '"lastAolEventDate": "2022-03-29T08:47:39.689Z",'
    + '"lastDeaAolLoginDate": "2022-03-29T08:47:39.689Z",'
    + '"isAol": true,'
    + '"isAolOnlineRegistered": true,'
    + '"isAolRetailRegistered": true'
    + '}'
    + '}';

    @TestSetup
    static void makeData(){
        CORE_CountrySpecificSettings__c countrySetting = new CORE_CountrySpecificSettings__c(
                CORE_FieldPrefix__c = 'Sta_'
        );
        database.insert(countrySetting, true);

        CORE_CatalogHierarchyModel catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test').catalogHierarchy;
        Account account = CORE_DataFaker_Account.getStudentAccount('test');
        Opportunity  opportunity = CORE_DataFaker_Opportunity.getAOLOpportunity(catalogHierarchy.division.Id, 
            catalogHierarchy.businessUnit.Id, 
            account.Id, 
            CORE_AOL_Constants.PKL_OPP_STAGENAME_PROSPECT, 
            CORE_AOL_Constants.PKL_OPP_SUBSTATUS_PROSPECTAPPSTARTED, 
            AOL_EXTERNALID
        );
    }
    
    @IsTest
    public static void documentProcess_should_upsert_aolDocuments_and_update_aolWorkerOpp_url() {
        CORE_AOL_Wrapper.AOLProgressionFields aolProgressionFields= CORE_DataFaker_AOL_Wrapper.getAolProgressionFieldsWrapper();
        CORE_AOL_GlobalWorker globalWorker = new CORE_AOL_GlobalWorker(aolProgressionFields,AOL_EXTERNALID);
        String onlineApplicationDocumentUrl = 'testUrl';

        CORE_AOL_Wrapper.DocumentWrapper documentWrapper1 = CORE_DataFaker_AOL_Wrapper.getDocumentWrapper(1);
        CORE_AOL_Wrapper.DocumentWrapper documentWrapper2 = CORE_DataFaker_AOL_Wrapper.getDocumentWrapper(2);
        List<CORE_AOL_Wrapper.DocumentWrapper> documentWrappers = new List<CORE_AOL_Wrapper.DocumentWrapper> {documentWrapper1, documentWrapper2};

        List<CORE_AOL_Document__c> aolDocuments = [Select Id from CORE_AOL_Document__c];

        System.assertEquals(null, globalWorker.aolOpportunity.CORE_OPP_AOL_document_folder_Url__c);
        System.assertEquals(0, aolDocuments.size());

        CORE_AOL_Document.documentProcess(globalWorker, onlineApplicationDocumentUrl, documentWrappers);

        aolDocuments = [Select Id,CORE_Related_to_Opportunity__c from CORE_AOL_Document__c];

        System.assertEquals(onlineApplicationDocumentUrl, globalWorker.aolOpportunity.CORE_OPP_AOL_document_folder_Url__c);
        System.assertEquals(documentWrappers.size(), aolDocuments.size());
        System.assertEquals(globalWorker.aolOpportunity.Id, aolDocuments.get(0).CORE_Related_to_Opportunity__c);
        System.assertEquals(globalWorker.aolOpportunity.Id, aolDocuments.get(1).CORE_Related_to_Opportunity__c);

        documentWrapper1.name = 'testUpdate';
        CORE_AOL_Document.documentProcess(globalWorker, onlineApplicationDocumentUrl, documentWrappers);
        aolDocuments = [Select Id,CORE_Related_to_Opportunity__c,Name from CORE_AOL_Document__c where CORE_Document_External_ID__c = :documentWrapper1.externalId];
        System.assertEquals(documentWrapper1.name, aolDocuments.get(0).Name);

        aolDocuments = [Select Id,CORE_Related_to_Opportunity__c from CORE_AOL_Document__c];
        System.assertEquals(documentWrappers.size(), aolDocuments.size());
    }

    @IsTest
    public static void checkMandatoryParameters_should_return_null_if_there_is_no_missing_parameters() {
        CORE_AOL_Wrapper.AOLProgressionFields aolProgressionFields= CORE_DataFaker_AOL_Wrapper.getAolProgressionFieldsWrapper();
        CORE_AOL_GlobalWorker globalWorker = new CORE_AOL_GlobalWorker(aolProgressionFields,AOL_EXTERNALID);
        String onlineApplicationDocumentUrl = 'testUrl';

        CORE_AOL_Wrapper.DocumentWrapper documentWrapper1 = CORE_DataFaker_AOL_Wrapper.getDocumentWrapper(1);
        CORE_AOL_Wrapper.DocumentWrapper documentWrapper2 = CORE_DataFaker_AOL_Wrapper.getDocumentWrapper(2);
        List<CORE_AOL_Wrapper.DocumentWrapper> documentWrappers = new List<CORE_AOL_Wrapper.DocumentWrapper> {documentWrapper1, documentWrapper2};

        CORE_AOL_Wrapper.ResultWrapperGlobal result = CORE_AOL_Document.checkMandatoryParameters(AOL_EXTERNALID, documentWrappers);

        System.assertEquals(null, result);
    }

    @IsTest
    public static void checkMandatoryParameters_should_return_result_wrapper_with_error_status_if_there_is_missing_parameters() {
        CORE_AOL_Wrapper.AOLProgressionFields aolProgressionFields= CORE_DataFaker_AOL_Wrapper.getAolProgressionFieldsWrapper();
        CORE_AOL_GlobalWorker globalWorker = new CORE_AOL_GlobalWorker(aolProgressionFields,AOL_EXTERNALID);
        String onlineApplicationDocumentUrl = 'testUrl';

        CORE_AOL_Wrapper.DocumentWrapper documentWrapper1 = CORE_DataFaker_AOL_Wrapper.getDocumentWrapper(1);
        CORE_AOL_Wrapper.DocumentWrapper documentWrapper2 = CORE_DataFaker_AOL_Wrapper.getDocumentWrapper(2);
        List<CORE_AOL_Wrapper.DocumentWrapper> documentWrappers = new List<CORE_AOL_Wrapper.DocumentWrapper> {documentWrapper1, documentWrapper2};

        CORE_AOL_Wrapper.ResultWrapperGlobal result = CORE_AOL_Document.checkMandatoryParameters(null, documentWrappers);

        System.assertNotEquals(null, result);
        System.assertEquals(CORE_AOL_Constants.ERROR_STATUS, result.status);
        System.assert(result.message.contains('aolExternalId'));

        documentWrapper1.externalId = null;
        result = CORE_AOL_Document.checkMandatoryParameters(AOL_EXTERNALID, documentWrappers);
        System.assertNotEquals(null, result);
        System.assertEquals(CORE_AOL_Constants.ERROR_STATUS, result.status);
        System.assert(!result.message.contains('aolExternalId'));
        System.assert(result.message.contains('aolDocuments[0].externalId'));

        documentWrapper2.name = ' ';
        result = CORE_AOL_Document.checkMandatoryParameters(AOL_EXTERNALID, documentWrappers);
        System.assertNotEquals(null, result);
        System.assertEquals(CORE_AOL_Constants.ERROR_STATUS, result.status);
        System.assert(!result.message.contains('aolExternalId'));
        System.assert(result.message.contains('aolDocuments[0].externalId'));
        System.assert(result.message.contains('aolDocuments[1].name'));
    }

    @IsTest
    private static void execute_should_return_a_success_if_all_upsert_are_ok(){

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/aol/document';

        req.httpMethod = 'POST';
        req.requestBody = Blob.valueOf(REQ_BODY);

        RestContext.request = req;
        RestContext.response= res;

        CORE_AOL_Wrapper.ResultWrapperGlobal result = CORE_AOL_Document.execute();

        System.assertEquals('OK', result.status);
    }

    @IsTest
    private static void execute_should_return_an_error_if_all_upsert_are_not_ok(){

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/aol/document';

        req.httpMethod = 'POST';
        req.requestBody = Blob.valueOf(REQ_BODY_ERROR);

        RestContext.request = req;
        RestContext.response= res;

        CORE_AOL_Wrapper.ResultWrapperGlobal result = CORE_AOL_Document.execute();

        System.assertEquals('KO', result.status);
    }

    @IsTest
    private static void execute_should_return_an_error_if_invalid_externalID(){

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/aol/document';

        req.httpMethod = 'POST';
        req.requestBody = Blob.valueOf(REQ_BODY_ERROR_INVALID_ID);

        RestContext.request = req;
        RestContext.response= res;

        CORE_AOL_Wrapper.ResultWrapperGlobal result = CORE_AOL_Document.execute();

        System.assertEquals('KO', result.status);
    }

    @IsTest
    private static void execute_should_return_an_error_if_mandatory_missing(){

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/aol/document';

        req.httpMethod = 'POST';
        req.requestBody = Blob.valueOf(REQ_BODY_ERROR_NO_ID);

        RestContext.request = req;
        RestContext.response= res;

        CORE_AOL_Wrapper.ResultWrapperGlobal result = CORE_AOL_Document.execute();

        System.assertEquals('KO', result.status);
    }
}