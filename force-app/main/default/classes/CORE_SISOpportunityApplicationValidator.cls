public with sharing class CORE_SISOpportunityApplicationValidator {
    public class CORE_SISOpportunityApplicationValidatorException extends Exception{}
    CORE_SISOpportunityApplicationWrapper.GlobalWrapper data;
    
    @TestVisible
    private static final String MANDATORY_FIELDS_MSG = 'Mandatory fields are missing';
    @TestVisible
    private static final String MANDATORY_FIELDS_MSG_NULL = 'Mandatory fields received as null';
    @TestVisible
    private static final String MANDATORY_FIELDS_MSG_STRING = 'Mandatory fields received as empty string';
    
    @TestVisible
    public static final String UNEXPECTED_MSG = 'An unexpected error occured while processing the request';

    @TestVisible
    public static final String UNEXPECTED_CODE = 'UNEXPECTED_ERROR';

    public DateTime apiCallDate;
    @TestVisible
    private static final String MANDATORY_FIELDS_CODE = 'MANDATORY_FIELDS';

    public Map<String,List<String>>  requiredFields =  new Map<String,List<String>>{
        'mainInterst'=> new List<String> {'SIS_OpportunityId','businessUnit','studyArea','curriculum','level','intake'},
        'personAccount'=> new List<String> {'CORE_SISStudentID__c'}
    };

    public CORE_SISOpportunityApplicationValidator(CORE_SISOpportunityApplicationWrapper.GlobalWrapper body, Datetime apiCalldate ) {
        this.data=body;
        this.apiCallDate=apiCallDate;
    }
    public List<CORE_SISOpportunityApplicationWrapper.ErrorWrapper> getFieldsErrors(){
        // checking obligatory Fields
        List<CORE_SISOpportunityApplicationWrapper.ErrorWrapper> errors = new List<CORE_SISOpportunityApplicationWrapper.ErrorWrapper>();
        for(String key : this.requiredFields.keySet()){
            Map<String,Object> obj = new Map<String,Object> ();
            switch on key {
                when 'mainInterst' {
                     obj= (Map<String,Object>) JSON.deserializeUntyped(JSON.serialize(this.data.mainInterest));
                }
                when 'personAccount' {
                     obj = this.data.personAccount;
                    
                }
            }
            for(String value: this.requiredFields.get(key)){
                Boolean isError = false;
                String errorMessage = '';
                if(!obj.containsKey(value) ){
                    isError = true;
                    errorMessage = MANDATORY_FIELDS_MSG;
                }else if(obj.get(value) == null) {
                    isError = true;
                    errorMessage = MANDATORY_FIELDS_MSG_NULL;
                } else {
                    if(obj.get(value) instanceof String && String.isBlank((String)obj.get(value))){
                        isError = true;
                        errorMessage = MANDATORY_FIELDS_MSG_STRING;
                    }
                }
                if(isError){
                    errors.add(new CORE_SISOpportunityApplicationWrapper.ErrorWrapper( 
                        '['+value+']  : '+errorMessage, 
                        MANDATORY_FIELDS_CODE

                    ));
                    return errors;
                }
            }

        }
        return errors;
    }
}