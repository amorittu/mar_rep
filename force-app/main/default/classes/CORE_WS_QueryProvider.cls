public without sharing class CORE_WS_QueryProvider {

    public static final Set<String> consentsFieldsSet = new Set<String> {
        'Id','CORE_Consent_type__c','CORE_Consent_channel__c','CORE_Consent_description__c','CORE_Opt_in__c','CORE_Opt_in_date__c','CORE_Opt_out__c','CORE_Double_Opt_in_date__c',
        'CORE_Opt_out_date__c','CORE_Division__c','CORE_Business_Unit__c, CORE_Person_Account__c, CORE_Scope__c'
    };

    public static List<CORE_Consent__c> getConsentsListByDate(String lastDate, String lastId, Integer limitPerPage){

        Date lastDateFormat = Date.valueOf(lastDate);

        String whereCondition = 'LastModifiedDate >=:lastDateFormat OR CreatedDate >=:lastDateFormat';

        if(lastId != null){
            whereCondition = '(LastModifiedDate >=:lastDateFormat OR CreatedDate >=:lastDateFormat) AND Id > :lastId';
        }

        system.debug('@getConsentsListByDate > whereCondition: ' + whereCondition);

        return getConsentsList(null, lastId, lastDateFormat, whereCondition, limitPerPage);
    }

    public static List<CORE_Consent__c> getConsentsListByPersonAccountId(String personAccId, String lastId, Integer limitPerPage){

        String whereCondition = 'CORE_Person_Account__c =:personAccId';

        if(lastId != null){
            whereCondition = 'CORE_Person_Account__c =:personAccId AND Id > :lastId';
        }

        system.debug('@getConsentsListByPersonAccountId > whereCondition: ' + whereCondition);

        return getConsentsList(personAccId, lastId, null, whereCondition, limitPerPage);
    }


    public static List<CORE_Consent__c> getConsentsList(String personAccId, String lastId, Datetime lastDateFormat, String whereCondition, Integer limitPerPage){

        Set<String> allFields = new Set<String>();
        String queryFields = '';

        for(String f : consentsFieldsSet){
            if(!allFields.contains(f)){
                queryFields += (queryFields != '') ? ',' + f :f;
            }

            allFields.add(f);
        }

        List<String> queryParams = new List<String> {
                queryFields,
                whereCondition
        };

        String queryString = String.format('SELECT {0} FROM CORE_Consent__c WHERE {1} ORDER BY ID LIMIT :limitPerPage', queryParams);

        system.debug('@getConsentsList > queryString: ' + queryString);

        return Database.query(queryString);
    }
}