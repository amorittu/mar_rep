@IsTest
public class CORE_CampaignUpdateStatusBatchableTest {
    
    private static final String PKL_STATUS_POSTPONED = '	Postponed';
    private static final String PKL_STATUS_INPROGRESS = 'In Progress';
    private static final String PKL_STATUS_Completed = 'Completed';
    
    @TestSetup
    public static void makeData(){
        
        //Ne pas initialiser à Planned pour les test, on rentre dans le before update du CORE_CampaignTriggerHandler sinon
        Campaign campaignTest = CORE_DataFaker_Campaign.getCampaignWithoutInsert('Test1', false, PKL_STATUS_POSTPONED, null, null, date.today());
        campaignTest.CORE_Start_Date_and_hour__c = date.today();

        database.insert(campaignTest);
    }

    @IsTest
    public static void campaign_update_test_status_InProgress() {
        Campaign campaignTest = [SELECT Id, CORE_Start_Date_and_hour__c, CORE_End_Date_and_hour__c FROM Campaign LIMIT 1];
        campaignTest.CORE_End_Date_and_hour__c = campaignTest.CORE_End_Date_and_hour__c.addDays(2);
        
        update campaignTest;
        
        test.startTest();
        CORE_CampaignUpdateStatusBatchable campaignUpdateStatusBatchable = new CORE_CampaignUpdateStatusBatchable();
        database.executebatch(campaignUpdateStatusBatchable);
        test.stopTest();

        Campaign campaignInProgress = [SELECT Id, Status FROM Campaign];
               
        System.assertEquals(PKL_STATUS_INPROGRESS, campaignInProgress.Status);
    }

    @IsTest
    public static void campaign_update_test_status_Completed() {
        Campaign campaignTest = [SELECT Id, CORE_Start_Date_and_hour__c, CORE_End_Date_and_hour__c FROM Campaign LIMIT 1];
        campaignTest.CORE_Start_Date_and_hour__c = campaignTest.CORE_Start_Date_and_hour__c.addDays(-4);
        campaignTest.CORE_End_Date_and_hour__c = campaignTest.CORE_End_Date_and_hour__c.addDays(-2);
        
        update campaignTest;
        
        test.startTest();
        CORE_CampaignUpdateStatusBatchable campaignUpdateStatusBatchable = new CORE_CampaignUpdateStatusBatchable();
        database.executebatch(campaignUpdateStatusBatchable);
        test.stopTest();

        Campaign campaignCompleted = [SELECT Id, Status FROM Campaign];
               
        System.assertEquals(PKL_STATUS_COMPLETED, campaignCompleted.Status);
    }

    @IsTest
    public static void campaign_update_test_status_Unchanged() {
        Campaign campaignTest = [SELECT Id, Status, CORE_Start_Date_and_hour__c, CORE_End_Date_and_hour__c FROM Campaign LIMIT 1];
        campaignTest.CORE_Start_Date_and_hour__c = campaignTest.CORE_Start_Date_and_hour__c.addDays(-4);
        campaignTest.CORE_End_Date_and_hour__c = campaignTest.CORE_End_Date_and_hour__c.addDays(2);
        
        update campaignTest;
        
        test.startTest();
        CORE_CampaignUpdateStatusBatchable campaignUpdateStatusBatchable = new CORE_CampaignUpdateStatusBatchable();
        database.executebatch(campaignUpdateStatusBatchable);
        test.stopTest();

        Campaign campaignUnchanged = [SELECT Id, Status FROM Campaign];
               
        System.assertEquals(campaignTest.Status, campaignUnchanged.Status);
    }
}