@isTest
private class CORE_CampaignTriggerHandler_TEST {

    
    private static final String PKL_STATUS_PLANNED = 'Planned';
    private static final String PKL_STATUS_POSTPONED = 'Postponed';
    
    @testSetup
    static void createData() {
        Id profileId = [SELECT Id FROM Profile WHERE Name = 'Core Marketer' LIMIT 1].Id;

        User usr = new User(
            Username = 'usertest@testcampaign.com',
            LastName = 'TEST_USER',
            Email = 'testemail@yopmail.com',
            Alias = 'T',
            CommunityNickname = 'T',
            TimeZoneSidKey = 'Europe/Paris',
            LocaleSidKey = 'fr_FR_EURO',
            EmailEncodingKey = 'ISO-8859-1',
            ProfileId = profileId,
            LanguageLocaleKey = 'fr'
        );
        database.insert(usr, true);
        
        Account account = CORE_DataFaker_Account.getStudentAccount('testAccount');
        Id contactId = [SELECT PersonContactId FROM Account WHERE Id = :account.Id LIMIT 1].PersonContactId;

        System.runAs(usr){
            Campaign campaignTest = CORE_DataFaker_Campaign.getCampaignWithoutInsert('Test1', true, PKL_STATUS_PLANNED, null, null, date.today().addDays(3));
            campaignTest.CORE_Start_Date_and_hour__c = date.today().addDays(2); //Pour passer la règle de validation & pour les tests (laisser à 2, ou ajouter les tests)
            campaignTest.StartDate = date.today().addDays(2); //FT: fix per validation
            campaignTest.CORE_Timezone__c = '(GMT+02:00) Central European Summer Time (Europe/Paris)';

            database.insert(campaignTest, true);
        
            //Mettre le même status que la campaignTest
        	CampaignMember campaignMemberTest = CORE_DataFaker_CampaignMember.getCampaignMember(campaignTest.Id, contactId, PKL_STATUS_PLANNED);

        }
        
    }

    @IsTest
    static void test_insert(){
        User usr = [SELECT Id FROM User WHERE Username = 'usertest@testcampaign.com' LIMIT 1];

        System.runAs(usr){
            Campaign campaignTest = CORE_DataFaker_Campaign.getCampaignWithoutInsert('Test2', true, 'Planned', null, null, date.today().addDays(-1));
            campaignTest.CORE_Start_Date_and_hour__c = datetime.now();
            campaignTest.StartDate = date.today(); //FT: fix per validation
            campaignTest.CORE_End_Date_and_hour__c = datetime.now().addDays(2);
            campaignTest.CORE_Timezone__c = '(GMT+04:00) Gulf Standard Time (Asia/Dubai)';

            test.startTest();
            database.insert(campaignTest, true);
            test.stopTest();

            campaignTest = [SELECT Id, CORE_Start_hour_Text__c, CORE_End_hour_Text__c, CORE_Start_Date_and_hour__c, CORE_End_Date_and_hour__c
                            FROM Campaign WHERE Id = :campaignTest.Id LIMIT 1];

            String expectedHour = ('00' + String.valueOf(campaignTest.CORE_Start_Date_and_hour__c.hourGMT() + 4)).right(2);
            String expectedMinutes = ('00' + String.valueOf(campaignTest.CORE_Start_Date_and_hour__c.minute())).right(2);

            system.assertEquals(expectedHour + ':' + expectedMinutes, campaignTest.CORE_Start_hour_Text__c);
        }
    }

    @IsTest
    static void test_update(){
        User usr = [SELECT Id FROM User WHERE Username = 'usertest@testcampaign.com' LIMIT 1];

        System.runAs(usr){
            Campaign campaignTest = [SELECT Id, CORE_Start_hour_Text__c, CORE_End_hour_Text__c, CORE_Start_Date_and_hour__c, CORE_End_Date_and_hour__c
                                     FROM Campaign LIMIT 1];

            campaignTest.CORE_Start_Date_and_hour__c = datetime.now();
            campaignTest.CORE_End_Date_and_hour__c = datetime.now().addDays(2);
            campaignTest.CORE_Timezone__c = '(GMT-04:00) Eastern Daylight Time (America/New_York)';

            test.startTest();
            database.update(campaignTest, true);
            test.stopTest();

            campaignTest = [SELECT Id, CORE_Start_hour_Text__c, CORE_End_hour_Text__c, CORE_Start_Date_and_hour__c, CORE_End_Date_and_hour__c
                            FROM Campaign WHERE Id = :campaignTest.Id LIMIT 1];
            
            CampaignMember campaignMemberTest = [SELECT Id, CORE_Campaign_Statut__c FROM CampaignMember WHERE CampaignId = :campaignTest.Id LIMIT 1];

            String expectedHour = ('00' + String.valueOf(campaignTest.CORE_End_Date_and_hour__c.hourGMT() - 4)).right(2);
            String expectedMinutes = ('00' + String.valueOf(campaignTest.CORE_End_Date_and_hour__c.minute())).right(2);
            system.assertEquals(expectedHour + ':' + expectedMinutes, campaignTest.CORE_End_hour_Text__c);
        }
    }

    @IsTest
    static void test_update_status_update_campaignmember(){

        Campaign campaignTest = [SELECT Id, status FROM Campaign LIMIT 1];
        campaignTest.status = PKL_STATUS_POSTPONED;

        test.startTest();
        database.update(campaignTest, true);
        test.stopTest();

        campaignTest = [SELECT Id, status FROM Campaign WHERE Id = :campaignTest.Id LIMIT 1];
            
        CampaignMember campaignMemberTest = [SELECT Id, CORE_Campaign_Statut__c FROM CampaignMember WHERE CampaignId = :campaignTest.Id LIMIT 1];
         
        //On vérifie que la campaignMember associé à un nouveau status égal à celui de la campaign
        //System.assertEquals(campaignTest.status, campaignMemberTest.CORE_Campaign_Statut__c);
    }

    @IsTest
    static void test_update_planned_set_postponed(){
        
        Campaign campaignTest1 = [SELECT Status, CORE_Start_Date_and_hour__c, CORE_End_Date_and_hour__c,CORE_Start_Date_before_postpon__c, CORE_End_Date_before_postpon__c  FROM Campaign LIMIT 1];
        DateTime oldDateTimeStart = campaignTest1.CORE_Start_Date_and_hour__c;
        DateTime oldDateTimeEnd = campaignTest1.CORE_End_Date_and_hour__c;

        System.assertNotEquals(null, campaignTest1.CORE_Start_Date_and_hour__c);
        System.assertNotEquals(null, campaignTest1.CORE_End_Date_and_hour__c);
        System.assertEquals(null, campaignTest1.CORE_Start_Date_before_postpon__c);
        System.assertEquals(null, campaignTest1.CORE_End_Date_before_postpon__c);
        System.assertEquals(PKL_STATUS_PLANNED, campaignTest1.Status);

        campaignTest1.CORE_Start_Date_and_hour__c = campaignTest1.CORE_Start_Date_and_hour__c.addDays(1); //nécéssaire au test

        test.startTest();
        database.update(campaignTest1, true);
        test.stopTest();
        

        Campaign campaignTest2 = [SELECT 	Id, 
                        		Status, 
                                CORE_Start_Date_before_postpon__c, 
                                CORE_Start_Date_and_hour__c,
                                CORE_End_Date_before_postpon__c,
                                CORE_End_Date_and_hour__c
                        FROM Campaign WHERE Id = :campaignTest1.Id LIMIT 1];
            
        CampaignMember campaignMemberTest = [SELECT Id, 
                                             		CORE_Campaign_Statut__c
                                             FROM CampaignMember WHERE CampaignId = :campaignTest2.Id LIMIT 1];
        
        //On vérifie le changement de status
        System.assertEquals(PKL_STATUS_POSTPONED, campaignTest2.Status);
        System.assertEquals(campaignTest2.CORE_Start_Date_before_postpon__c, oldDateTimeStart);
        System.assertEquals(campaignTest2.CORE_End_Date_before_postpon__c, oldDateTimeEnd);
        //On vérifie que la campaignMember associé à un nouveau status égal à celui de la campaign
        //System.assertEquals(campaignTest2.status, campaignMemberTest.CORE_Campaign_Statut__c);
    }

    @IsTest
    static void test_update_null(){
        User usr = [SELECT Id FROM User WHERE Username = 'usertest@testcampaign.com' LIMIT 1];

        System.runAs(usr){
            Campaign campaignTest = [SELECT Id, CORE_Start_hour_Text__c, CORE_End_hour_Text__c, CORE_Start_Date_and_hour__c, CORE_End_Date_and_hour__c
            FROM Campaign LIMIT 1];

            campaignTest.CORE_Start_Date_and_hour__c = null;
            campaignTest.CORE_End_Date_and_hour__c = null;
            campaignTest.CORE_Timezone__c = '(GMT-04:00) Eastern Daylight Time (America/New_York)';

            test.startTest();
            database.update(campaignTest, true);
            test.stopTest();

            campaignTest = [SELECT Id, CORE_Start_hour_Text__c, CORE_End_hour_Text__c, CORE_Start_Date_and_hour__c, CORE_End_Date_and_hour__c
                            FROM Campaign WHERE Id = :campaignTest.Id LIMIT 1];

            system.assertEquals(null, campaignTest.CORE_End_hour_Text__c);
        }
    }
}