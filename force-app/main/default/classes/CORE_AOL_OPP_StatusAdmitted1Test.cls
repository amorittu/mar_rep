@IsTest
public with sharing class CORE_AOL_OPP_StatusAdmitted1Test {
    private static final String AOL_EXTERNALID = 'setupAol';

    @TestSetup
    static void makeData(){
        CORE_CatalogHierarchyModel catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test').catalogHierarchy;
        Account account = CORE_DataFaker_Account.getStudentAccount('test');

        Opportunity  opportunity = CORE_DataFaker_Opportunity.getAOLOpportunity(
            catalogHierarchy.division.Id, 
            catalogHierarchy.businessUnit.Id,
            catalogHierarchy.studyArea.Id, 
            catalogHierarchy.curriculum.Id, 
            catalogHierarchy.level.Id,
            catalogHierarchy.intake.Id,
            account.Id, 
            CORE_AOL_Constants.PKL_OPP_STAGENAME_PROSPECT, 
            CORE_AOL_Constants.PKL_OPP_SUBSTATUS_PROSPECTAPPSTARTED, 
            AOL_EXTERNALID
        );
    }

    @IsTest
    private static void execute_should_return_an_error_if_all_upsert_are_not_ok(){
        CORE_AOL_Wrapper.AOLProgressionFields aolProgressionFields= CORE_DataFaker_AOL_Wrapper.getAolProgressionFieldsWrapper();

        CORE_AOL_Wrapper.ResultWrapperGlobal result = CORE_AOL_OPP_StatusAdmitted1.execute(null, aolProgressionFields);

        System.assertEquals('KO', result.status);

        result = CORE_AOL_OPP_StatusAdmitted1.execute('etstsetsetsetes', aolProgressionFields);

        System.assertEquals('KO', result.status);
    }

    @IsTest
    private static void execute_should_return_a_success_if_all_upsert_are_ok(){
        CORE_AOL_Wrapper.AOLProgressionFields aolProgressionFields= CORE_DataFaker_AOL_Wrapper.getAolProgressionFieldsWrapper();
        Opportunity opp = [Select Id, StageName, CORE_OPP_Sub_Status__c from Opportunity];

        System.assertNotEquals(CORE_AOL_Constants.PKL_OPP_STAGENAME_ADMITTED, opp.StageName);
        System.assertNotEquals(CORE_AOL_Constants.PKL_OPP_SUBSTATUS_ADMITTED_ADMIT, opp.CORE_OPP_Sub_Status__c);

        CORE_AOL_Wrapper.ResultWrapperGlobal result = CORE_AOL_OPP_StatusAdmitted1.execute(AOL_EXTERNALID, aolProgressionFields);

        System.assertEquals('OK', result.status);
        opp = [Select Id, StageName, CORE_OPP_Sub_Status__c from Opportunity];

        System.assertEquals(CORE_AOL_Constants.PKL_OPP_STAGENAME_ADMITTED, opp.StageName);
        System.assertEquals(CORE_AOL_Constants.PKL_OPP_SUBSTATUS_ADMITTED_ADMIT, opp.CORE_OPP_Sub_Status__c);
    }
}