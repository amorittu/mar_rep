@IsTest
public with sharing class CORE_DataFaker_User {
    private static final String TIMEZONE_PARIS = 'Europe/Paris';

    public static User getUser(String uniqueKey){
        User u = getUserWithoutInsert(uniqueKey, getStandardProfile().Id);

        Insert u;
        return u;
    }

    public static User getUserWithoutInsert(String uniqueKey, Id profileId){
        String uid = generateUid();
        String email = uid + '-' + uniqueKey + '@test.com';

        User u = new User(
            Alias = uid, 
            Email= email, 
            EmailEncodingKey='UTF-8', 
            LastName='Testing-' + uniqueKey, 
            LanguageLocaleKey='en_US', 
            LocaleSidKey='en_US', 
            ProfileId = profileId, 
            TimeZoneSidKey=TIMEZONE_PARIS, 
            UserName= email,
            IsActive=true
        );   

        return u;
    }

    public static Profile getStandardProfile(){
        Profile p = [SELECT Id FROM Profile WHERE Name='Standard User']; 
        return p;
    }

    private static String generateUid(){
        Blob b = Crypto.GenerateAESKey(128);
        String h = EncodingUtil.ConvertTohex(b);
        String uid = h.SubString(0,8);

        return uid;
    }
}