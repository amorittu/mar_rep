/*************************************************************************************************************
 * @name            IT_MarketingCloudIntegration
 * @author          Andrea Morittu <amorittu@atlantic-technologies.com> && Lorenzo Senesi 
 * @created         22 / 04 / 2022
 * @description     Class used to make an http call to make a login with marketing cloud
 *
 * Changes (version)
 * -----------------------------------------------------------------------------------------------------------
 *              No.     Date            Author                  Description
 *              ----    ------------    --------------------    ----------------------------------------------
 * @version     1.0     2022-04-22      Andrea Morittu          Creation
 *
**************************************************************************************************************/

public without sharing class IT_MarketingCloudIntegration {
    public static final String CLASS_NAME = IT_MarketingCloudIntegration.class.getName();

    private static IT_MarketingCloudIntegration instance;

    private IT_MarketingCloudAPI__mdt marketingCloudApi;
    private Map<String, IT_MarketingCloudAPI__mdt> marketingCloudApis = new Map<String, IT_MarketingCloudAPI__mdt>();
    private String accessToken;

    private IT_MarketingCloudIntegration(String businessUnitMarketingCloud){
        List<IT_MarketingCloudAPI__mdt> apis = [
            SELECT Id, DeveloperName, IT_ClientId__c, IT_ClientSecret__c, IT_EndpointLogin__c, IT_EndpointRest__c, IT_GrantType__c, IT_FireEventAPI__c, IT_McAccountId__c
            FROM IT_MarketingCloudAPI__mdt
        ];

        System.debug('businessUnitMarketingCloud: ' + businessUnitMarketingCloud);

        for (IT_MarketingCloudAPI__mdt tmpApi : apis) {
            marketingCloudApis.put(tmpApi.DeveloperName, tmpApi);
        }

        System.debug('marketingCloudApis: ' + marketingCloudApis);

        this.marketingCloudApi = marketingCloudApis.get(businessUnitMarketingCloud);
        String marketingCloudAccountId = this.marketingCloudApi.IT_McAccountId__c;
        if (!Test.isRunningTest()) {
            this.accessToken = requestTokenV2(marketingCloudAccountId);
        }
    }

    public static IT_MarketingCloudIntegration getInstance(String businessUnitMarketingCloud){
        if(instance == null){
            instance = new IT_MarketingCloudIntegration(businessUnitMarketingCloud);
        }
        return instance;
    }

    public String requestTokenV2(String marketingCloudAccountId ) {
        JSONGenerator jsonGen = JSON.createGenerator(true);
        jsonGen.writeStartObject();
        jsonGen.writeObjectField('client_id', this.marketingCloudApi.IT_ClientId__c);
        jsonGen.writeObjectField('client_secret', this.marketingCloudApi.IT_ClientSecret__c);
        jsonGen.writeObjectField('grant_type', this.marketingCloudApi.IT_GrantType__c);
        jsonGen.writeObjectField('account_id', marketingCloudAccountId );
        jsonGen.writeEndObject();

        HttpRequest req = new HttpRequest();
        req.setMethod('POST');
        req.setHeader('Content-Type', 'application/json');
        req.setEndpoint(this.marketingCloudApi.IT_EndpointLogin__c + 'v2/token');
        req.setBody(jsonGen.getAsString());

        Http http = new Http();
        HttpResponse res = http.send(req);

        Map<String, Object> fieldMap = (Map<String, Object>) JSON.deserializeUntyped(res.getBody());
        String accessTokenString = (String) fieldMap.get('access_token');

        return accessTokenString;
    }

    public String getAccessToken(){
        return accessToken;
    }

    public IT_MarketingCloudAPI__mdt getMarketingCloudApi(){
        return marketingCloudApi;
    }

}