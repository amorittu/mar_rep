public without sharing class CORE_SIS_QueryProvider {
    private static final String OPP_QUERY_FIELDS = 'AccountId,'+
        'Account.CORE_GovernmentStudentID__c,'+
        'Account.CORE_SISStudentID__c,'+
        'Account.CORE_Social_Security_Number_Fiscal_Code__pc,'+
        'Account.CreatedDate,'+
        'Account.LastModifiedDate,'+
        'Account.CORE_Gender__pc,'+
        'Account.Salutation,'+
        'Account.Firstname,'+
        'Account.Middlename,'+
        'Account.CORE_Nickname__pc,'+
        'Account.Lastname,'+
        'Account.CORE_Birthname__pc,'+
        'Account.CORE_Phonetic_name__pc,'+
        'Account.PersonBirthdate,'+
        'Account.CORE_Year_of_birth__pc,'+
        'Account.CORE_City_of_birth__pc,'+
        'Account.CORE_Department_of_birth__pc,'+
        'Account.CORE_Country_of_birth__pc,'+
        'Account.CORE_Has_handicap__pc,'+
        'Account.CORE_Handicap__pc,'+
        'Account.CORE_Main_Nationality__pc,'+
        'Account.CORE_Additional_Nationality__pc,'+
        'Account.CORE_Language__pc,'+
        'Account.CORE_Language_website__pc,'+
        'Account.CORE_Language_of_relationship__pc,'+
        'Account.Website,'+
        'Account.PersonEmail,'+
        'Account.CORE_Email_2__pc,'+
        'Account.CORE_Email_3__pc,'+
        'Account.CORE_Student_Email__pc,'+
        'Account.CORE_Student_Email_2__pc,'+
        'Account.CORE_Student_Email_3__pc,'+
        'Account.Phone,'+
        'Account.CORE_Phone2__pc,'+
        'Account.PersonMobilePhone,'+
        'Account.CORE_Mobile_2__pc,'+
        'Account.CORE_Mobile_3__pc,'+
        'Account.PersonMailingStreet,'+
        'Account.PersonMailingCity,'+
        'Account.PersonMailingState,'+
        'Account.PersonMailingCountry,'+
        'Account.BillingStreet,'+
        'Account.BillingPostalCode,'+
        'Account.BillingCity,'+
        'Account.BillingState,'+
        'Account.BillingCountry,'+
        'Account.ShippingStreet,'+
        'Account.ShippingPostalCode,'+
        'Account.ShippingCity,'+
        'Account.ShippingState,'+
        'Account.ShippingCountry';
    
    public static final String ACC_QUERY_FIELDS = 'Id,'+
        'CORE_GovernmentStudentID__c,'+
        'CORE_SISStudentID__c,'+
        'CORE_Social_Security_Number_Fiscal_Code__pc,'+
        'CreatedDate,'+
        'LastModifiedDate,'+
        'CORE_Gender__pc,'+
        'Salutation,'+
        'Firstname,'+
        'Middlename,'+
        'CORE_Nickname__pc,'+
        'Lastname,'+
        'CORE_Birthname__pc,'+
        'CORE_Phonetic_name__pc,'+
        'PersonBirthdate,'+
        'CORE_Year_of_birth__pc,'+
        'CORE_City_of_birth__pc,'+
        'CORE_Department_of_birth__pc,'+
        'CORE_Country_of_birth__pc,'+
        'CORE_Has_handicap__pc,'+
        'CORE_Handicap__pc,'+
        'CORE_Main_Nationality__pc,'+
        'CORE_Additional_Nationality__pc,'+
        'CORE_Language__pc,'+
        'CORE_Language_website__pc,'+
        'CORE_Language_of_relationship__pc,'+
        'Website,'+
        'PersonEmail,'+
        'CORE_Email_2__pc,'+
        'CORE_Email_3__pc,'+
        'CORE_Student_Email__pc,'+
        'CORE_Student_Email_2__pc,'+
        'CORE_Student_Email_3__pc,'+
        'Phone,'+
        'CORE_Phone2__pc,'+
        'PersonMobilePhone,'+
        'CORE_Mobile_2__pc,'+
        'CORE_Mobile_3__pc,'+
        'PersonMailingStreet,'+
        'PersonMailingCity,'+
        'PersonMailingState,'+
        'PersonMailingCountry,'+
        'BillingStreet,'+
        'BillingPostalCode,'+
        'BillingCity,'+
        'BillingState,'+
        'BillingCountry,'+
        'ShippingStreet,'+
        'ShippingPostalCode,'+
        'ShippingCity,'+
        'ShippingState,'+
        'ShippingCountry';
    
    public static final Set<String> oppsFieldsSet = new Set<String> {
        'AccountId','Amount','CORE_Alumni__c','CORE_Application_Fee_Transaction_Id__c','CORE_Date_of_withdraw__c','CORE_Document_progression__c',
        'CORE_Document_sending_process_choice__c','CORE_First_document_upload_date__c','CORE_Is_AOL_Register__c','CORE_Is_AOL_Retail_Register__c','CORE_Last_AOL_event_date__c',
        'CORE_Main_Product_Interest__c','CORE_OPP_Academic_Year__c','CORE_OPP_Acceptance_Application_Fee_Date__c','CORE_OPP_Admission_Test_Fee_Needed__c',
        'CORE_OPP_Admission_Test_Fee_Received__c','CORE_OPP_AdmissionTest_Fee_Received_Date__c','CORE_OPP_AdmissionTestFee_Payment_Method__c',
        'CORE_OPP_AOL_document_folder_Url__c','CORE_OPP_Application_completed__c','CORE_OPP_Application_fee_amount__c','CORE_OPP_Application_Fee_Bank__c',
        'CORE_OPP_Application_Fee_Payment_Method__c','CORE_OPP_Application_Fee_Received__c','CORE_OPP_Application_Fee_Received_Date__c',
        'CORE_OPP_Application_Fee_Sender__c', 'CORE_OPP_Application_Final_Decision__c','CORE_OPP_Application_Online__c',
        'CORE_OPP_Application_Online_Advancement__c','CORE_OPP_Application_Online_ID__c','CORE_OPP_Application_Reviewer_1__c',
        'CORE_OPP_Application_Reviewer_1_Decision__c','CORE_OPP_Application_Reviewer_2__c','CORE_OPP_Application_Reviewer_2_Decision__c',
        'CORE_OPP_Application_started__c','CORE_OPP_Business_Unit__c','CORE_OPP_Conditional_Admitted_Deadl__c','CORE_OPP_Conditional_Admitted_Reaso__c',
        'CORE_OPP_Credit_Transfer_Needed__c','CORE_OPP_Credit_Transfer_Number__c','CORE_OPP_Curriculum__c','CORE_OPP_Division__c',
        'CORE_OPP_Down_Payment_Fee_Payment_Method__c','CORE_OPP_Down_Payment_Fee_Received__c','CORE_OPP_Down_Payment_Fee_Received_Date__c',
        'CORE_OPP_Drop_Out_Date__c','CORE_OPP_Intake__c','CORE_OPP_Level__c','CORE_OPP_Loss_Reason__c','CORE_OPP_Main_Opportunity__c','CORE_OPP_Parent_Opportunity__c',
        'CORE_OPP_Prerequisite_Diploma__c','CORE_OPP_Prerequisite_Diploma_Check__c','CORE_OPP_Prerequisite_Diploma_Check_Date__c','CORE_OPP_Registration_Date__c',
        'CORE_OPP_Registration_Form__c','CORE_OPP_Registration_Form_Date__c','CORE_OPP_Requirement_Risk__c','CORE_OPP_Scholarship_Amount__c',
        'CORE_OPP_Scholarship_Assigned__c','CORE_OPP_Scholarship_Holder__c','CORE_OPP_Scholarship_Requested__c','CORE_OPP_Scholarship_Type__c',
        'CORE_OPP_Selection_Process_Needed__c','CORE_OPP_Selection_Test_Date__c','CORE_OPP_Selection_Test_Grade__c','CORE_OPP_Selection_Test_Needed__c',
        'CORE_OPP_Selection_Test_No_Show_Counter__c','CORE_OPP_Selection_Test_Result__c','CORE_OPP_Selection_Test_Show_No_Show__c','CORE_OPP_Selection_Test_Teacher__c',
        'CORE_OPP_SIS_Opportunity_Id__c','CORE_OPP_SIS_Student_Academic_Record_ID__c','CORE_OPP_Study_Area__c','CORE_OPP_Sub_Status__c',
        'CORE_OPP_Tuition_Fee_Payment_Method__c','CORE_OPP_Tuition_Fee_Payment_Type__c','CORE_OPP_Tuition_Fee_Received__c','CORE_OPP_Tuition_Fee_Received_Date__c',
        'CORE_OPP_Visa_Risk__c','CORE_OPP_Visa_Status__c','CORE_Scholarship_Level__c','CORE_Selection_status__c','CORE_Showed_up_to_classes__c',
        'CORE_Tuition_Fee_Acceptance_Date__c','CORE_Tuition_fee_amount__c','CORE_Tuition_Fee_Bank__c','CORE_Tuition_Fee_Sender__c','CORE_Tuition_fee_Transaction_ID__c',
        'CreatedDate','Id','LastModifiedDate','Name','StageName','Study_mode__c',
        'CORE_Last_DEA_AOL_login_date__c','CORE_Skill_test_start_date__c','CORE_Down_payment_acceptance_date__c','CORE_Selection_process_status__c',
        'CORE_Selection_process_start_date__c','CORE_Selection_process_end_date__c','CORE_Selection_Process_Result__c','CORE_Selection_Process_Grade__c',
        'CORE_Selection_process_program_leader__c, RecordType.DeveloperName, CORE_OPP_Parent_Opportunity__r.CORE_OPP_SIS_Opportunity_Id__c'
    };

    public static final Set<String> oppsPutFieldsSet = new Set<String> {
        'AccountId','Amount','CORE_Alumni__c','CORE_Application_Fee_Transaction_Id__c','CORE_Date_of_withdraw__c','CORE_Document_progression__c',
        'CORE_Document_sending_process_choice__c','CORE_First_document_upload_date__c','CORE_Is_AOL_Register__c','CORE_Is_AOL_Retail_Register__c','CORE_Last_AOL_event_date__c',
        'CORE_Main_Product_Interest__c','CORE_OPP_Academic_Year__c','CORE_OPP_Acceptance_Application_Fee_Date__c','CORE_OPP_Admission_Test_Fee_Needed__c',
        'CORE_OPP_Admission_Test_Fee_Received__c','CORE_OPP_AdmissionTest_Fee_Received_Date__c','CORE_OPP_AdmissionTestFee_Payment_Method__c',
        'CORE_OPP_AOL_document_folder_Url__c','CORE_OPP_Application_completed__c','CORE_OPP_Application_fee_amount__c','CORE_OPP_Application_Fee_Bank__c',
        'CORE_OPP_Application_Fee_Payment_Method__c','CORE_OPP_Application_Fee_Received__c','CORE_OPP_Application_Fee_Received_Date__c',
        'CORE_OPP_Application_Fee_Sender__c', 'CORE_OPP_Application_Final_Decision__c','CORE_OPP_Application_Online__c',
        'CORE_OPP_Application_Online_Advancement__c','CORE_OPP_Application_Online_ID__c','CORE_OPP_Application_Reviewer_1__c',
        'CORE_OPP_Application_Reviewer_1_Decision__c','CORE_OPP_Application_Reviewer_2__c','CORE_OPP_Application_Reviewer_2_Decision__c',
        'CORE_OPP_Application_started__c','CORE_OPP_Business_Unit__c','CORE_OPP_Conditional_Admitted_Deadl__c','CORE_OPP_Conditional_Admitted_Reaso__c',
        'CORE_OPP_Credit_Transfer_Needed__c','CORE_OPP_Credit_Transfer_Number__c','CORE_OPP_Curriculum__c','CORE_OPP_Division__c',
        'CORE_OPP_Down_Payment_Fee_Payment_Method__c','CORE_OPP_Down_Payment_Fee_Received__c','CORE_OPP_Down_Payment_Fee_Received_Date__c',
        'CORE_OPP_Drop_Out_Date__c','CORE_OPP_Intake__c','CORE_OPP_Level__c','CORE_OPP_Loss_Reason__c','CORE_OPP_Main_Opportunity__c','CORE_OPP_Parent_Opportunity__c',
        'CORE_OPP_Prerequisite_Diploma__c','CORE_OPP_Prerequisite_Diploma_Check__c','CORE_OPP_Prerequisite_Diploma_Check_Date__c','CORE_OPP_Registration_Date__c',
        'CORE_OPP_Registration_Form__c','CORE_OPP_Registration_Form_Date__c','CORE_OPP_Requirement_Risk__c','CORE_OPP_Scholarship_Amount__c',
        'CORE_OPP_Scholarship_Assigned__c','CORE_OPP_Scholarship_Holder__c','CORE_OPP_Scholarship_Requested__c','CORE_OPP_Scholarship_Type__c',
        'CORE_OPP_Selection_Process_Needed__c','CORE_OPP_Selection_Test_Date__c','CORE_OPP_Selection_Test_Grade__c','CORE_OPP_Selection_Test_Needed__c',
        'CORE_OPP_Selection_Test_No_Show_Counter__c','CORE_OPP_Selection_Test_Result__c','CORE_OPP_Selection_Test_Show_No_Show__c','CORE_OPP_Selection_Test_Teacher__c',
        'CORE_OPP_SIS_Opportunity_Id__c','CORE_OPP_SIS_Student_Academic_Record_ID__c','CORE_OPP_Study_Area__c',
        'CORE_OPP_Tuition_Fee_Payment_Method__c','CORE_OPP_Tuition_Fee_Payment_Type__c','CORE_OPP_Tuition_Fee_Received__c','CORE_OPP_Tuition_Fee_Received_Date__c',
        'CORE_OPP_Visa_Risk__c','CORE_OPP_Visa_Status__c','CORE_Scholarship_Level__c','CORE_Selection_status__c','CORE_Showed_up_to_classes__c',
        'CORE_Tuition_Fee_Acceptance_Date__c','CORE_Tuition_fee_amount__c','CORE_Tuition_Fee_Bank__c','CORE_Tuition_Fee_Sender__c','CORE_Tuition_fee_Transaction_ID__c',
        'CreatedDate','Id','LastModifiedDate','Name','Study_mode__c',
        'CORE_Last_DEA_AOL_login_date__c','CORE_Skill_test_start_date__c','CORE_Down_payment_acceptance_date__c','CORE_Selection_process_status__c',
        'CORE_Selection_process_start_date__c','CORE_Selection_process_end_date__c','CORE_Selection_Process_Result__c','CORE_Selection_Process_Grade__c',
        'CORE_Selection_process_program_leader__c'
    };
                
    private static final Set<String> oliFieldsSet = new Set<String> {
        'Id','OpportunityId','Name','ProductCode','Quantity','Discount','UnitPrice','Subtotal','ListPrice','TotalPrice'
    };
                            
    private static final Set<String> deletedRecordsFieldsSet = new Set<String> {
        'Id','CORE_Object_Name__c','CORE_Record_Type__c','CORE_Deletion_Date__c','CORE_MasterId__c','CORE_MasterLegacyCRMID__c','CORE_MasterSISStudentID__c',
        'CORE_Record_Id__c','CORE_LegacyCRMID__c','CORE_SISStudentID__c'
    };
    public static List<String> getAccountListById(List<String> opportunitystatus,String lastDate,String recordType){
        List<String> idList = new List<String>();
        Date lastDateFormat = Date.valueOf(lastDate);
                   
        List<AggregateResult> opportunities= [SELECT accountId FROM Opportunity WHERE StageName IN :opportunitystatus AND Account.RecordType.DeveloperName =:recordType AND Account.LastModifiedDate >=:lastDateFormat GROUP BY AccountId];
       
        System.debug('opportunities '+opportunities);
        for (AggregateResult opp : opportunities ){
            System.debug('opp '+opp.get('AccountId'));
            idList.add((String)opp.get('AccountId'));
        }
        System.debug('id list '+ idList);
        return idList;
    }

    public static List<Account> getAccountList(List<Id> idList,List<String> additionalFieldsList,String lastId){
        List<String> reqFields = new List<String>();
        Map <String,Schema.SObjectType> gd = Schema.getGlobalDescribe();
        Schema.SObjectType sobjType = gd.get('Account');
        Schema.DescribeSObjectResult r = sobjType.getDescribe();
        Map<String, Schema.SObjectField> mapofField = r.fields.getMap();
        String additionalFields = '';
        for(String fieldName : mapofField.keySet()) {
            reqFields.add(fieldName);
        }
        System.debug(reqFields);
        for(String field : additionalFieldsList){
            if(reqFields.contains(field.toLowercase())) {
                additionalFields = additionalFields + ',' +field;
            }
        }
        String whereIdCondition = 'Id IN :idList';
        String whereLastIdCondition = 'Id > :lastId';
        List<String> queryParams = new List<String>{
            ACC_QUERY_FIELDS,
            additionalFields,
            whereIdCondition,
            whereLastIdCondition
        };
        String queryString = '';
        if(lastId != ''
            && lastId != null){
                queryString = String.format('SELECT {0} {1} FROM Account WHERE {2} AND {3}  ORDER BY ID LIMIT 2000', queryParams);
            }else{
                queryString = String.format('SELECT {0} {1} FROM Account WHERE {2} ORDER BY ID LIMIT 2000', queryParams);
            }            
        
        System.debug(queryString);
        List<Account> accounts = Database.query(queryString);
       
        System.debug(accounts);
        return accounts;
    }
    
    public static Account getAccount(String salesforceId, String sisStudentId, List<String> additionalFieldsList){
        String whereIdCondition = '';
        String additionalFields = '';
        String queryString='';

        if(String.IsBlank(salesforceId)){
            whereIdCondition = 'CORE_SISStudentID__c =:sisStudentId';
        }else{
            whereIdCondition = '(Id = :salesforceId OR CORE_SISStudentID__c = :salesforceId)';
        }

        List<String> reqFields = new List<String>();
        Map <String,Schema.SObjectType> gd = Schema.getGlobalDescribe();
        Schema.SObjectType sobjType = gd.get('Account');
        Schema.DescribeSObjectResult r = sobjType.getDescribe();
        Map<String, Schema.SObjectField> MapofField = r.fields.getMap();
        
        for(String fieldName : MapofField.keySet()) {
            reqFields.add(fieldName);
        }
        for(String field : additionalFieldsList){
            if(reqFields.contains(field.toLowercase())) {
                additionalFields = additionalFields + ',' +field;
            }
        }
        
        List<String> queryParams = new List<String> {ACC_QUERY_FIELDS,additionalFields,whereIdCondition};
            queryString = String.format('SELECT {0} {1} FROM Account WHERE {2}', queryParams);
        List<Account> accounts = Database.query(queryString);
        
        if(!accounts.isEmpty()){
            return accounts.get(0);
        } else {
            return null;
        }
    }
    
    public static List<Opportunity> getOpportunityList(String lastDate, List<String> opportunityStatus, String recordType, 
                                                       String additionalFields, String lastId, Integer limitPerPage){

        Date lastDateFormat = Date.valueOf(lastDate);
        String whereLastDateCondition = 'LastModifiedDate >=:lastDateFormat';
        String whereRecordTypeCondition = 'RecordType.DeveloperName =:recordType';
        String whereOpportunityStatusCondition = 'StageName IN :opportunityStatus';
        //String whereIdCondition = 'Id IN :listAccount';
        String whereIdCondition = 'Id > :lastId';
                                                           
        Set<String> allFields = new Set<String>();
        String queryFields = '';
        
        for(String f : oppsFieldsSet){
            if(!allFields.contains(f)){
                queryFields += (queryFields != '') ? ',' + f :f;
            }
            
            allFields.add(f);
        }
        
        if(additionalFields != null){
            for(String f : CORE_SIS_GlobalWorker.splitValues(additionalFields)){
                if(!allFields.contains(f)){
                    queryFields += (queryFields != '') ? ',' + f :f;
                }
                
                allFields.add(f);
            }
        }
        
        List<String> queryParams = new List<String> {
            queryFields,
            whereIdCondition,
            whereRecordTypeCondition, 
            whereLastDateCondition,
            whereOpportunityStatusCondition
        };
                    
        String queryString = '';
        
        if(lastId != null){
            queryString = String.format('SELECT {0} FROM Opportunity WHERE {1} AND {2} AND {3} AND {4} ORDER BY ID LIMIT :limitPerPage', queryParams);
        }else{
            queryString = String.format('SELECT {0} FROM Opportunity WHERE {2} AND {3} AND {4} ORDER BY ID LIMIT :limitPerPage', queryParams);
        }
        
        system.debug('@getOpportunityList > queryString: ' + queryString);
        
        return Database.query(queryString);
    }
    
    public static List<Opportunity> getOpportunity(String salesforceId, String sisStudentId, String additionalFields){
    
        String whereIdCondition = (salesforceId != null && salesforceId != '') 
                                ? '(Id = :salesforceId OR CORE_OPP_SIS_Opportunity_Id__c = :salesforceId OR CORE_OPP_AOL_Opportunity_Id__c = :salesforceId)' 
                                : 'Account.CORE_SISStudentID__c = :sisStudentId';
        
        Set<String> allFields = new Set<String>();
        String queryFields = '';
        
        for(String f : oppsFieldsSet){
            if(!allFields.contains(f)){
                queryFields += (queryFields != '') ? ',' + f :f;
            }
            
            allFields.add(f);
        }
        
        if(additionalFields != null){
            for(String f : CORE_SIS_GlobalWorker.splitValues(additionalFields)){
                if(!allFields.contains(f)){
                    queryFields += (queryFields != '') ? ',' + f :f;
                }
                
                allFields.add(f);
            }
        }
        
        List<String> queryParams = new List<String> {
            queryFields,
            whereIdCondition
        };
                    
        String queryString = String.format('SELECT {0} FROM Opportunity WHERE {1}', queryParams);
        
        return Database.query(queryString);
    }

    public static List<Opportunity> getOpportunity(String sisStudentId){
        List<Opportunity> opportunities = [select ID,stagename,CORE_OPP_Sub_Status__c FROM opportunity WHERE CORE_OPP_SIS_Opportunity_Id__c =: sisStudentId];
        
        return opportunities;
    }
    
    public static List<OpportunityLineItem> getProductsList(String lastDate, List<String> opportunityStatus, String additionalFields, String lastId, Integer limitPerPage){
        Date lastDateFormat = Date.valueOf(lastDate);
        String whereLastDateCondition = 'LastModifiedDate >=:lastDateFormat';
        String whereOpportunityStatusCondition = 'Opportunity.StageName IN :opportunityStatus';
        
        String whereIdCondition = 'Id > :lastId';
        
        Set<String> allFields = new Set<String>();
        String queryFields = '';
        
        for(String f : oliFieldsSet){
            if(!allFields.contains(f)){
                queryFields += (queryFields != '') ? ',' + f :f;
            }
            
            allFields.add(f);
        }
        
        if(additionalFields != null){
            for(String f : CORE_SIS_GlobalWorker.splitValues(additionalFields)){
                if(!allFields.contains(f)){
                    queryFields += (queryFields != '') ? ',' + f :f;
                }
                
                allFields.add(f);
            }
        }
        
        List<String> queryParams = new List<String> {
            queryFields,
                whereIdCondition,
                whereLastDateCondition,
                whereOpportunityStatusCondition
                };
                    
                    String queryString = '';
        
        if(lastId != null){
            queryString = String.format('SELECT {0} FROM OpportunityLineItem WHERE {1} AND {2} AND {3} ORDER BY ID LIMIT :limitPerPage', queryParams);
        }else{
            queryString = String.format('SELECT {0} FROM OpportunityLineItem WHERE {2} AND {3} ORDER BY ID LIMIT :limitPerPage', queryParams);
        }
        
        system.debug('@getProductsList > queryString: ' + queryString);
        
        return Database.query(queryString);
    }
    
    public static List<OpportunityLineItem> getProduct(String salesforceId, String additionalFields){
        
        String whereIdCondition = 'Id = :salesforceId';
        
        Set<String> allFields = new Set<String>();
        String queryFields = '';
        
        for(String f : oliFieldsSet){
            if(!allFields.contains(f)){
                queryFields += (queryFields != '') ? ',' + f :f;
            }
            
            allFields.add(f);
        }
        
        if(additionalFields != null){
            for(String f : CORE_SIS_GlobalWorker.splitValues(additionalFields)){
                if(!allFields.contains(f)){
                    queryFields += (queryFields != '') ? ',' + f :f;
                }
                
                allFields.add(f);
            }
        }
        
        List<String> queryParams = new List<String> {
            queryFields,
                whereIdCondition
                };
                    
                    String queryString = String.format('SELECT {0} FROM OpportunityLineItem WHERE {1}', queryParams);
        
        return Database.query(queryString);
    }
    
    public static List<CORE_Deleted_record__c> getDeletedRecords(String lastModifiedDate, String recordType, String lastId, Integer limitPerPage){
        Date lastDateFormat = Date.valueOf(lastModifiedDate);
        String whereLastDateCondition = 'LastModifiedDate >=:lastDateFormat';
        String whereRecordTypeCondition = 'CORE_Record_Type__c = :recordType';
        
        String whereIdCondition = 'Id > :lastId';
        
        Set<String> allFields = new Set<String>();
        String queryFields = '';
        
        for(String f : deletedRecordsFieldsSet){
            if(!allFields.contains(f)){
                queryFields += (queryFields != '') ? ',' + f :f;
            }
            
            allFields.add(f);
        }
        
        List<String> queryParams = new List<String> {
            queryFields,
                whereIdCondition,
                whereRecordTypeCondition, 
                whereLastDateCondition
                };
                    
                    String queryString = '';
        
        if(lastId != null){
            queryString = String.format('SELECT {0} FROM CORE_Deleted_record__c WHERE {1} AND {2} AND {3} ORDER BY ID LIMIT :limitPerPage', queryParams);
        }else{
            queryString = String.format('SELECT {0} FROM CORE_Deleted_record__c WHERE {2} AND {3} ORDER BY ID LIMIT :limitPerPage', queryParams);
        }
        
        return Database.query(queryString);
    }

}