public without sharing class CORE_SIS_GlobalWorker {
     @TestVisible
    private String sisOpportunityId;
    public Opportunity sisOpportunity {get;set;}
    public Boolean updateOppIsNeeded {get;set;}
	public CORE_SIS_GlobalWorker(String sisOpportunityId) {
        this.sisOpportunityId = sisOpportunityId;
        this.sisOpportunity = getSisOpportunity();
        this.updateOppIsNeeded = false;
    }
     private Opportunity getSisOpportunity(){
        List<Opportunity> opportunities = CORE_SIS_QueryProvider.getOpportunity(this.sisOpportunityId);

        Opportunity opportunity;
        if(opportunities.size() == 1){
            opportunity = opportunities.get(0);
        }
        return opportunity;
    }
    public static List<String> splitValues(String val){
        List<String> valuesList = new List<String>();

        if(val != null){
            if(val.contains('&#124;')){
                system.debug('@splitValues > val 0: ' + val);
                valuesList = val.split('&#124;');
            }
            else if(val.contains('|')){
                system.debug('@splitValues > val 1: ' + val);
                valuesList = val.split('\\|');
            }
            else{
                system.debug('@splitValues > val 2: ' + val);
                valuesList = val.split(',');
                valuesList = val.split('\\&#124;');
            }
        }

        system.debug('@splitValues > valuesList: ' + valuesList);

        return valuesList;
    }

    public static Boolean isParameterMissing(String parameter){
        if(String.IsBlank(parameter)){
            return true;
        } else {
            return false;
        }
    }

    public static Boolean isParameterMissing(List<String> opportunityStatus){
        if(opportunityStatus.size()==0){
            return true;
        } else {
            return false;
        }
    }

    public static sObject assignValue(sObject obj, String field, Object val){
        system.debug('@assignValue > field: ' + field);
        system.debug('@assignValue > val: ' + val);

        try{
            obj.put(field, String.valueOf(val));
        }
        catch(Exception e0){
            system.debug('@assignValue e0: ' + e0.getMessage() + ' at ' + e0.getStackTraceString());
            try{
                obj.put(field, Boolean.valueOf(val));
            }
            catch(Exception e1){
                system.debug('@assignValue e1: ' + e1.getMessage() + ' at ' + e1.getStackTraceString());
                try{
                    obj.put(field, Date.valueOf((String)val));
                }
                catch(Exception e2){
                    system.debug('@assignValue e2: ' + e2.getMessage() + ' at ' + e2.getStackTraceString());

                    try{
                        obj.put(field, Datetime.valueOf(((String)val).replace('T',' ')));
                    }
                    catch(Exception e3){
                        system.debug('@assignValue e3: ' + e3.getMessage() + ' at ' + e3.getStackTraceString());

                        try{
                            obj.put(field, Double.valueOf(val));
                        }
                        catch(Exception e4){
                            system.debug('@assignValue e4: ' + e4.getMessage() + ' at ' + e4.getStackTraceString());

                            TypeException excep = new TypeException();
                            excep.setMessage('INVALID TYPE FOR FIELD "'+ field + '"');
                            throw excep;
                        }
                    }
                }
            }
        }

        return obj;
    }
    public static CORE_SIS_Wrapper.ResultWrapperGlobal getMissingParameters(List<String> missingParameters){
        String errorMsg = CORE_SIS_Constants.MSG_MISSING_PARAMETERS;

        if(missingParameters != null && !missingParameters.isEmpty()){
            errorMsg += missingParameters;

            return new CORE_SIS_Wrapper.ResultWrapperGlobal(CORE_SIS_Constants.ERROR_STATUS, errorMsg);
        }
        
        return null;
    }
    public static Boolean issisOpportunityIdParameterMissing(String sisOpportunityId){
        if(String.IsBlank(sisOpportunityId)){
            return true;
        } else {
            return false;
        }
    }
}