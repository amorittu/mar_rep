public virtual class CORE_LeadFormAcquisitionProcess {
    @TestVisible
    protected DateTime apiCallDate;

    @TestVisible
    protected CORE_LeadFormAcquisitionWrapper.GlobalWrapper datas;

    public List<CORE_LeadFormAcquisitionWrapper.ErrorWrapper> errors{get;set;}
    public CORE_LeadFormAcquisitionWrapper.ResultWrapper result{get;set;}
    public CORE_OpportunityDataMaker dataMaker{get;set;}

    @TestVisible
    protected List<Task> tasksToCreate{get;set;}
    @TestVisible
    protected Id campaignIdFound;

    @TestVisible
    protected List<String> availableAcademicYears{get;set;}
    public Boolean countryPicklistIsActivated;

    public CORE_LeadFormAcquisitionProcess(DateTime apiCallDate,CORE_LeadFormAcquisitionWrapper.GlobalWrapper datas) {
        this.apiCallDate = apiCallDate;
        this.datas = datas;
        this.errors = new List<CORE_LeadFormAcquisitionWrapper.ErrorWrapper>();
        this.result = new CORE_LeadFormAcquisitionWrapper.ResultWrapper();
        this.dataMaker = new CORE_OpportunityDataMaker();
        this.tasksToCreate = new List<Task>();
        this.countryPicklistIsActivated = CORE_SobjectUtils.doesFieldExist('User', 'Countrycode');
    }

    public virtual void execute(){
        System.debug('CORE_LeadFormAcquisitionProcess.execute() : START');
        Long dt1 = DateTime.now().getTime();
        Long dt2,dt3;
        Savepoint sp = Database.setSavepoint();

        CORE_LeadFormAcquistionInputValidator inputValidator = new CORE_LeadFormAcquistionInputValidator(this.datas, this.apiCallDate, this.countryPicklistIsActivated);
        this.errors = inputValidator.getFieldsErrors();

        if(this.errors.size() > 0){
            dt2 = DateTime.now().getTime();
            System.debug('CORE_LeadFormAcquisitionProcess.execute() time execution = ' + (dt2-dt1) + ' ms');

            System.debug('Missing parameters => end execute function');
            System.debug('CORE_LeadFormAcquisitionProcess.execute() : END');
            return;
        }

        this.availableAcademicYears = inputValidator.pklOppAcademicYear;
        //upsert account part : START ================
        dt3 = DateTime.now().getTime();
        List<Account> existingAccounts = CORE_LeadFormAcquisitionUtils.getExestingAccounts(datas.personAccount);
        dt2 = DateTime.now().getTime();

        boolean oneExistingAccount = existingAccounts.size() == 1;

        System.debug('CORE_LeadFormAcquisitionProcess.execute() -> existingAccounts() time execution = ' + (dt2-dt3) + ' ms');

        dt3 = DateTime.now().getTime();
        Account account;
        try{
            account = getAccount(existingAccounts);
            this.result.account.id = account.id;
        } catch (Exception ex){
            Database.rollback(sp);
            throw ex;
        }
        dt2 = DateTime.now().getTime();


        if(!String.isBlank(this.datas.campaignSubscription?.campaignId)){
            List<Campaign> exsistingCampaigns = [SELECT Id FROM Campaign WHERE CORE_External_Id__c = :this.datas.campaignSubscription.campaignId];
            if(!exsistingCampaigns.isEmpty()){
                this.campaignIdFound = exsistingCampaigns.get(0).Id;
            }
        }

        System.debug('CORE_LeadFormAcquisitionProcess.execute() -> getAccount() time execution = ' + (dt2-dt3) + ' ms');
        //upsert account part : END ===================

        //Opportunity part : START ================
        List<Opportunity> opportunities = new List<Opportunity>();
        List<Opportunity> mainOpportunities = new List<Opportunity>();
        
        dt3 = DateTime.now().getTime();
        try{
            mainOpportunities = getOpportunities(account);
            for(Integer i = 0 ; i < mainOpportunities.size(); i++){
                if(mainOpportunities.get(i).CORE_OPP_Main_Opportunity__c){
                    opportunities.add(mainOpportunities.get(i));
                }
            }
            for(Integer i = 0 ; i < opportunities.size(); i++){
                Opportunity opportunity = opportunities.get(i);
                if(i == 0){
                    this.result.opportunities.mainInterest.id = opportunity.Id;
                } else if(i == 1) {
                    this.result.opportunities.secondInterest.id = opportunity.Id;
                } else if(i == 2) {
                    this.result.opportunities.thirdInterest.id = opportunity.Id;
                }
            }
        } catch (Exception ex){
            Database.rollback(sp);
            throw ex;
        }

        dt2 = DateTime.now().getTime();
        System.debug('CORE_LeadFormAcquisitionProcess.execute() -> Opportunity time execution = ' + (dt2-dt3) + ' ms');

        //opportunity line items : START ==================
        List<OpportunityLineItem> oppLineItems = new List<OpportunityLineItem>();
        
        try{
            oppLineItems = getOppLineItems(opportunities);
        } catch (Exception ex){
            Database.rollback(sp);
            throw ex;
        }

        dt3 = DateTime.now().getTime();
        System.debug('CORE_LeadFormAcquisitionProcess.execute() -> OpportunityLineItem time execution = ' + (dt3-dt2) + ' ms');
        //opportunity line items : END ==================

        CampaignMember campaignMember = new campaignMember();
        try{
            Opportunity mainOpportunity;


            campaignMember = getCampaignMember(account, opportunities.get(0));
    
            this.result.campaignMember.id = campaignMember.Id;
        } catch (Exception ex){
            Database.rollback(sp);
            throw ex;
        }

        //Point of contacts part : START ==================

        if(!String.isBlank(campaignMember?.Id)){
            this.datas.pointOfContact.crmCampaign = new Campaign(CORE_External_Id__c = this.datas.campaignSubscription.campaignId);
            System.debug('campaignMember.CORE_External_Id__c = ' + campaignMember.CORE_External_Id__c);
            System.debug('campaignMember.Id = ' + campaignMember.Id);

            System.debug('this.datas.pointOfContact.crmCampaign = ' + this.datas.pointOfContact.crmCampaign);
        }
        
       
        this.datas.pointOfContact.languageWebsite = this.datas.personAccount.languageWebsite;
        List<CORE_Point_of_Contact__c> pocs = dataMaker.initPocFromAPI(opportunities, this.datas.pointOfContact);
        
        try{    
            insert pocs;

            for(CORE_Point_of_Contact__c poc : pocs){
                CORE_LeadFormAcquisitionWrapper.ResultPointOfContactWrapper pocWrapper = new CORE_LeadFormAcquisitionWrapper.ResultPointOfContactWrapper();
                pocWrapper.id = poc.Id;
                this.result.pointOfContacts.add(pocWrapper);
            }
        } catch (Exception ex){
            Database.rollback(sp);
            throw ex;
        }
        dt2 = DateTime.now().getTime();
        System.debug('CORE_LeadFormAcquisitionProcess.execute() -> PointOfContact process time execution = ' + (dt2-dt3) + ' ms');

        //Point of contacts part : END ==================

        //tasks part : START ==============
        initRelatedTasks(opportunities, pocs, account);
        try{    
            insert tasksToCreate;
        } catch (Exception ex){
            Database.rollback(sp);
            throw ex;
        }

        dt3 = DateTime.now().getTime();
        System.debug('CORE_LeadFormAcquisitionProcess.execute() -> tasks process time execution = ' + (dt3-dt2) + ' ms');
        //tasks part : END ==============


        //Consents part : Start ================== 
        List<CORE_Consent__c> consents = getConsents(account);

        try{
            upsert consents CORE_Consent__c.fields.CORE_TECH_InternalReference__c;

            for(CORE_Consent__c consent : consents){
                CORE_LeadFormAcquisitionWrapper.ResultConsentWrapper consentWrapper = new CORE_LeadFormAcquisitionWrapper.ResultConsentWrapper();
                consentWrapper.id = consent.Id;
                this.result.consents.add(consentWrapper);
            }
        } catch (Exception ex){
            Database.rollback(sp);
            throw ex;
        }

        //Consents part : END ================== 
        dt2 = DateTime.now().getTime();
        System.debug('CORE_LeadFormAcquisitionProcess.execute() -> consents process time execution = ' + (dt3-dt2) + ' ms');

        //AcademicDiplomaHistory part : Start ================== 
        CORE_Academic_Diploma_History__c academicDiplomaHistory = getAcademicDiplomaHistory(account);

        try{
            if(academicDiplomaHistory != null){
                insert academicDiplomaHistory;
            }
        } catch (Exception ex){
            Database.rollback(sp);
            throw ex;
        }

        //AcademicDiplomaHistory part : END ================== 
        dt3 = DateTime.now().getTime();
        System.debug('CORE_LeadFormAcquisitionProcess.execute() -> AcademicDiplomaHistory process time execution = ' + (dt2-dt3) + ' ms');

        System.debug('CORE_LeadFormAcquisitionProcess.execute() : END. [ExecutionTime = ' + (dt2-dt1) + ' ms]');
    }

    @TestVisible
    protected CORE_Academic_Diploma_History__c getAcademicDiplomaHistory(Account account){
        if(this.datas.academicDiplomaHistory != null 
        && !String.isBlank(this.datas.academicDiplomaHistory.level) 
        && !String.isBlank(this.datas.academicDiplomaHistory.name)){
            return CORE_AccountDataMaker.initAcademicDiplomaHistoryFromAPI(account, this.datas.academicDiplomaHistory);
        }

        return null;
    }

    @TestVisible
    protected List<CORE_Consent__c> getConsents(Account account){
        List<String> consentsBuExternalIds = new List<String>();
        List<String> consentsDivisionExternalIds = new List<String>();
        for(CORE_LeadFormAcquisitionWrapper.ConsentWrapper consent : this.datas.consents){
            consentsBuExternalIds.add(consent.businessUnit);
            consentsDivisionExternalIds.add(consent.division);
        }
        
        List<CORE_Business_Unit__c> consentsBusinessUnits = [SELECT Id, CORE_Business_Unit_ExternalId__c, CORE_Brand__c
            FROM CORE_Business_Unit__c 
            WHERE CORE_Business_Unit_ExternalId__c IN :consentsBuExternalIds];

        List<CORE_Division__c> consentsDivisions = [SELECT Id, CORE_Division_ExternalId__c
            FROM CORE_Division__c 
            WHERE CORE_Division_ExternalId__c IN :consentsDivisionExternalIds];
        
        Map<String,Id> buIdsByExternalIds = new Map<String,Id>();
        Map<String,Id> divisionIdsByExternalIds = new Map<String,Id>();
        Map<Id,String> brandByBu = new Map<Id,String>();
        for(CORE_Business_Unit__c bu : consentsBusinessUnits){
            buIdsByExternalIds.put(bu.CORE_Business_Unit_ExternalId__c,bu.Id);
            brandByBu.put(bu.Id,bu.CORE_Brand__c);
        }
        for(CORE_Division__c division : consentsDivisions){
            divisionIdsByExternalIds.put(division.CORE_Division_ExternalId__c,division.Id);
        }


        for(CORE_LeadFormAcquisitionWrapper.ConsentWrapper consent : this.datas.consents){
            consent.businessUnit = buIdsByExternalIds.get(consent.businessUnit);
            consent.division = divisionIdsByExternalIds.get(consent.division);
            consent.brand = brandByBu.get(consent.businessUnit);
        }

        List<CORE_Consent__c> consentsTmp = dataMaker.initConsentsFromAPI(account, this.datas.consents, true);
        List<CORE_Consent__c> consents = new List<CORE_Consent__c>();

        List<String> existingExternalIds = new List<String>();
        for(CORE_Consent__c consent : consentsTmp){
            System.debug('consent.CORE_TECH_InternalReference__c = '+ consent.CORE_TECH_InternalReference__c);
            if(!existingExternalIds.contains(consent.CORE_TECH_InternalReference__c)){
                existingExternalIds.add(consent.CORE_TECH_InternalReference__c);
                consents.add(consent);
            }
        }

        return consents;
    }

    @TestVisible
    protected CampaignMember getCampaignMember(Account account, Opportunity opportunity){
        CampaignMember campaignMember = new campaignMember();
        if(this.datas.campaignSubscription == null || String.IsEmpty(this.datas.campaignSubscription.campaignId)){
            return campaignMember;
        }

        if(!String.isBlank(this.campaignIdFound)){
            String campaignExternalId = this.datas.campaignSubscription.campaignId;
            List<CampaignMember> campaignMembers = [SELECT id FROM CampaignMember 
                WHERE ContactId = :account.PersonContactId 
                AND CampaignId = :this.campaignIdFound];
            
            if(campaignMembers.isEmpty()){
                campaignMember = this.dataMaker.initCampaignMemberFromAPI(account, opportunity, this.datas.campaignSubscription);
                insert campaignMember;
            } else {
                campaignMember.Id = campaignMembers.get(0).Id;
            }
        
        }
        return campaignMember;
    }

    @TestVisible
    protected virtual List<OpportunityLineItem> getOppLineItems(List<Opportunity> opportunities){
        List<OpportunityLineItem> oppLineItems = new List<OpportunityLineItem>();

        Map<Id,Opportunity> oppsMap = new Map<ID, Opportunity>(opportunities);
        System.debug('oppsMap= ' + oppsMap);

        Map<Id,Id> buByProducts = new Map<Id,Id>();
        Map<Id,Id> productsByOpportunities = new Map<Id,Id>();
        opportunities = [Select Id,CORE_Main_Product_Interest__c, CORE_OPP_Business_Unit__r.Id, AccountId FROM Opportunity where id in :oppsMap.keySet()];
        System.debug('opportunities= ' + opportunities);

        for(Integer i = 0 ; i < opportunities.size(); i++){
            Opportunity opportunity = opportunities.get(i);
            
            if(!String.IsBlank(opportunity.CORE_Main_Product_Interest__c) && !String.IsBlank(opportunity.CORE_OPP_Business_Unit__c)){
                buByProducts.put(opportunity.CORE_Main_Product_Interest__c,opportunity.CORE_OPP_Business_Unit__c);
                productsByOpportunities.put(opportunity.Id, opportunity.CORE_Main_Product_Interest__c);
            }
        }
        System.debug('productsByOpportunities.keySet().size() = ' + productsByOpportunities.keySet().size());
        //no need to continue because there is no products
        if(productsByOpportunities.keySet().size() == 0){
            return oppLineItems;
        }

        Map<Id,PricebookEntry> pricebookEntries = this.dataMaker.getPricebookEntryByProducts(buByProducts);
       
        for(Id opportunityId : productsByOpportunities.keySet()){
            Opportunity currentOpportunity = oppsMap.get(opportunityId);
            System.debug('currentOpportunity = ' + currentOpportunity);
            System.debug('currentOpportunity.CORE_ReturningLead__c = ' + currentOpportunity.CORE_ReturningLead__c);

            if(currentOpportunity.CORE_ReturningLead__c == null || currentOpportunity.CORE_ReturningLead__c == true){
                break;
            }
            PricebookEntry pricebookEntry = pricebookEntries.get(productsByOpportunities.get(opportunityId));

            if(pricebookEntry != NULL){
                OpportunityLineItem oppLineItem = this.dataMaker.initOppLineItem(oppsMap.get(opportunityId), pricebookEntry);
                oppLineItems.add(oppLineItem);
            }
        }

        if(oppLineItems.size() > 0){
            insert oppLineItems;
        }
        

        return oppLineItems;
    }

    @TestVisible
    protected virtual List<Opportunity> getOpportunities(Account account){
        long dt1 = DateTime.now().getTime();
        long dt2 = DateTime.now().getTime();
        long dt3 = DateTime.now().getTime();
        List<Opportunity> opportunities = new List<Opportunity>();
        Set<Id> existingOpportunities = new Set<Id>();
        Set<String> aolIds = new Set<String>();

        List<CORE_LeadFormAcquisitionWrapper.OpportunityWrapper> opportunitiesWrapped= new List<CORE_LeadFormAcquisitionWrapper.OpportunityWrapper>();
        Map<Integer,CORE_LeadFormAcquisitionWrapper.OpportunityWrapper> mapOppWrapped = new Map<Integer,CORE_LeadFormAcquisitionWrapper.OpportunityWrapper>();
        Integer currentOppWrapper = 0;

        if(this.datas.mainInterest != null){
            mapOppWrapped.put(currentOppWrapper,this.datas.mainInterest);
            currentOppWrapper += 1;
        }
        if(this.datas.secondInterest != null){
            mapOppWrapped.put(currentOppWrapper,this.datas.secondInterest);
            currentOppWrapper += 1;
        }
        if(this.datas.thirdInterest != null){
            mapOppWrapped.put(currentOppWrapper,this.datas.thirdInterest);
            //currentOppWrapper += 1;
        }

        //currentOppWrapper = 0;
        
        Map<Integer,CORE_CatalogHierarchyModel> catalogHierarchies = new Map<Integer,CORE_CatalogHierarchyModel>();
        List<CORE_Business_Unit__c> businessUnits = new List<CORE_Business_Unit__c>();
        CORE_LeadFormAcquisitionWrapper.OpportunityWrapper currentInterest;

        for(Integer currentOppWrapped : mapOppWrapped.keySet()){
            currentInterest = mapOppWrapped.get(currentOppWrapped);

            CORE_CatalogHierarchyModel catalogHierarchy = new CORE_CatalogHierarchyModel(
                currentInterest.division, 
                currentInterest.businessUnit, 
                currentInterest.studyArea, 
                currentInterest.curriculum, 
                currentInterest.level, 
                currentInterest.intake
            );

            if(!String.isEmpty(currentInterest.applicationOnlineId)){
                aolIds.add(currentInterest.applicationOnlineId);
            }
            businessUnits.add(catalogHierarchy.businessUnit);
            catalogHierarchies.put(currentOppWrapped, catalogHierarchy);            
        }

        catalogHierarchies = CORE_OpportunityDataMaker.getCatalogHierarchies(catalogHierarchies);

        dt2 = DateTime.now().getTime();
        system.debug('catalogHierarchy & map init [ExecutionTime = ' + (dt2-dt1) + ' ms]');

        Set<String> intakeIds = new Set<String>();
        for(CORE_CatalogHierarchyModel catalog : catalogHierarchies.values()){
            if(catalog.intake != null){
                intakeIds.add(catalog.intake.CORE_Intake_ExternalId__c);
            }
        }
        List<Opportunity> opportunitiesMatchingIntake = CORE_OpportunityDataMaker.getOpportunitiesByAccountAndIntake(new Set<Id>{account.Id},intakeIds, true);

        List<Opportunity> aolOpportunities = CORE_OpportunityUtils.getExistingAolOpportunities(aolIds);
        Map<String,Opportunity> opportunnitiesByAolIds = new Map<String, Opportunity>();
        if(!aolOpportunities.isEmpty()){
            for(Opportunity opportunity : aolOpportunities){
                opportunnitiesByAolIds.put(opportunity.CORE_OPP_Application_Online_ID__c, opportunity);
            }
        }

        List<Opportunity> mainOpportunities = getExistingMainOpportunities(businessUnits, account.Id);
        System.debug('mainOpportunities = '+ mainOpportunities);

        Map<String,List<Opportunity>> mainOpportunitiesByBu = new Map<String,List<Opportunity>>();

        dt1 = DateTime.now().getTime();
        for(Opportunity opp : mainOpportunities){
            System.debug('opp.CORE_OPP_Business_Unit__r.CORE_Business_Unit_ExternalId__c = '+ opp.CORE_OPP_Business_Unit__r.CORE_Business_Unit_ExternalId__c);
            System.debug('currentInterest.businessUnit = '+ currentInterest.businessUnit);
            String bu = opp.CORE_OPP_Business_Unit__r.CORE_Business_Unit_ExternalId__c;
            List<Opportunity> mainOpportunitiesForThisBu = mainOpportunitiesByBu.get(bu);
            if(mainOpportunitiesForThisBu == null){
                mainOpportunitiesForThisBu = new List<Opportunity>();
            }

            mainOpportunitiesForThisBu.add(opp);
            mainOpportunitiesByBu.put(bu,mainOpportunitiesForThisBu);
        }
        system.debug('mainOpportunitiesByBu = ' + mainOpportunitiesByBu);

        for(Integer currentOppWrapped : mapOppWrapped.keySet()){

            currentInterest = mapOppWrapped.get(currentOppWrapped);

            
            CORE_CatalogHierarchyModel catalogHierarchy = catalogHierarchies.get(currentOppWrapped);
            catalogHierarchy.interest = currentInterest;

            if(!String.isEmpty(catalogHierarchy.invalidReference)){
                String interestError = '';
                switch on currentOppWrapped {
                    when 1 {
                        interestError = 'Second interest';
                    }
                    when 2 {
                        interestError = 'Third interest';
                    }
                    when else {
                        interestError = 'Main interest';
                    }
                }
                string errorMsg = interestError + ': ' + catalogHierarchy.invalidReference;

                CORE_LeadFormAcquisitionWrapper.ErrorWrapper error = new CORE_LeadFormAcquisitionWrapper.ErrorWrapper(errorMsg,'INVALID_REFERENCE',this.apiCallDate);
                this.errors.add(error);

                throw new IllegalArgumentException(errorMsg);
            }

            catalogHierarchy.isSuspect = currentInterest.isSuspect == null ? false : currentInterest.isSuspect;
            

            String finalAcademicYear = CORE_OpportunityUtils.getAcademicYear(
                dataMaker.getDefaultAcademicYear(),
                currentInterest.academicYear, 
                catalogHierarchy, 
                account.CreatedDate, 
                this.availableAcademicYears
            );

            dt1 = DateTime.now().getTime();

            Boolean mainOnSameAcademicYear = false;
            Opportunity mainOpportunity;

            String bu = currentInterest.businessUnit;
            List<Opportunity> mainOpportunitiesForThisBu = mainOpportunitiesByBu.get(bu);
            system.debug('mainOpportunitiesForThisBu['+ bu +'] =' + mainOpportunitiesForThisBu);
            boolean aolOpp = false;
            Opportunity opportunityToUpsert;
            if(!String.isEmpty(currentInterest.applicationOnlineId)){
                mainOpportunity = opportunnitiesByAolIds.get(currentInterest.applicationOnlineId);
                opportunityToUpsert = this.dataMaker.initAOLOpportunity(account, finalAcademicYear, catalogHierarchy, mainOpportunity, opportunitiesMatchingIntake);
                aolOpp = true;
            }
            system.debug('mainOpportunity1 =' + mainOpportunity);
            system.debug('finalAcademicYear =' + finalAcademicYear);
            Boolean updateNotMain = true;
            if(mainOpportunity == null){
                //existing main for this bu
                if(mainOpportunitiesForThisBu != null && mainOpportunitiesForThisBu.size() > 0){
                    mainOpportunity = mainOpportunitiesForThisBu.get(0);

                    for(Opportunity opp : mainOpportunitiesForThisBu){
                        
                        if(opp.CORE_OPP_Academic_Year__c == finalAcademicYear){
                            mainOnSameAcademicYear = true;
                            mainOpportunity = opp;
                            break;
                        }
                    }
                }
                system.debug('mainOpportunity2 =' + mainOpportunity);

                dt2 = DateTime.now().getTime();
                system.debug('main opp on academic yead [ExecutionTime = ' + (dt2-dt1) + ' ms]');

                
                if(!String.isEmpty(currentInterest.applicationOnlineId)){
                    opportunityToUpsert = this.dataMaker.initAOLOpportunity(account, finalAcademicYear, catalogHierarchy, mainOpportunity, opportunitiesMatchingIntake);
                    if(mainOpportunity != null && opportunityToUpsert != null && mainOpportunity.CORE_OPP_Academic_Year__c == opportunityToUpsert.CORE_OPP_Academic_Year__c && mainOpportunity?.id != opportunityToUpsert?.Id){
                        mainOpportunity.CORE_OPP_Main_Opportunity__c = false;
                        opportunities.add(mainOpportunity);
                        opportunityToUpsert.CORE_OPP_Main_Opportunity__c = true;
                        updateNotMain = false;
                    }
                    aolOpp = true;
                    mainOnSameAcademicYear = true;
                    System.debug('d3');
                } else {
                    opportunityToUpsert = this.dataMaker.initAPIOpportunity(account, finalAcademicYear, catalogHierarchy, mainOpportunity);
                    System.debug('d4');
                }
                
            }

            if(aolOpp && opportunityToUpsert==null){
                System.debug('d5');
                System.debug('opportunityToUpsert==null');
                mainOpportunity.CORE_OPP_Main_Opportunity__c = false;
                if(mainOpportunity.CORE_OPP_Academic_Year__c == finalAcademicYear && updateNotMain){
                    opportunities.add(mainOpportunity);
                }
                opportunityToUpsert = null;
                opportunityToUpsert = this.dataMaker.initAOLOpportunity(account, finalAcademicYear, catalogHierarchy, null, opportunitiesMatchingIntake);
                opportunityToUpsert.OwnerId = mainOpportunity.OwnerId;
                mainOnSameAcademicYear = false;
                
            }

            if(currentInterest.applicationFormShippedByMail != null){
                opportunityToUpsert.CORE_Application_form_shipped_by_mail__c = currentInterest.applicationFormShippedByMail;
            }

            if(currentInterest.brochureShippedByMail != null){
                opportunityToUpsert.CORE_Brochure_shipped_by_mail__c = currentInterest.brochureShippedByMail;
            }

            
            system.debug('opp = ' + opportunityToUpsert);
            system.debug('mainOnSameAcademicYear = ' + mainOnSameAcademicYear);
            system.debug('catalogHierarchy = ' + catalogHierarchy);

            if(opportunityToUpsert.Id != null){
                Task task = CORE_OpportunityUtils.getReturningTask(catalogHierarchy, this.datas.prospectiveStudentComment, opportunityToUpsert, account);
                
                System.debug('task added = ' + task);

                tasksToCreate.add(task);
                if(opportunityToUpsert.CampaignId == null && !String.isBlank(campaignIdFound)){
                    opportunityToUpsert.CampaignId = campaignIdFound;
                }
            } else{
                opportunityToUpsert.CORE_Questions__c = CORE_OpportunityUtils.getShortQuestionForOpportunity(this.datas.prospectiveStudentComment);
                opportunityToUpsert.CORE_OPP_Learning_Material__c = currentInterest.learningMaterial;
                opportunityToUpsert.CORE_Lead_Source_External_Id__c = currentInterest.leadSourceExternalId;
                opportunityToUpsert.CORE_OPP_Application_Online_ID__c = currentInterest.applicationOnlineId;
                opportunityToUpsert.CORE_OPP_Funding__c = currentInterest.funding;
                opportunityToUpsert.CampaignId = campaignIdFound;
            }

            if(opportunities.isEmpty()){
                opportunities.add(opportunityToUpsert);
            } else if(String.IsEmpty(opportunityToUpsert.Id) || !existingOpportunities.contains(opportunityToUpsert.Id)){
                opportunities.add(opportunityToUpsert);
            }
            if(opportunityToUpsert.Id != null){
                existingOpportunities.add(opportunityToUpsert.Id);
            }
        }

        //check for main Opportunities
        dt1 = DateTime.now().getTime();
        upsert opportunities;
        
        dt2 = DateTime.now().getTime();

        system.debug('Insert opportunities [ExecutionTime = ' + (dt2-dt1) + ' ms]');
        return opportunities;
    }

    @TestVisible
    protected virtual void initRelatedTasks(List<Opportunity> opportunities, List<CORE_Point_of_Contact__c> pocs, Account account){
        for(Opportunity opp : opportunities){
            if(opp.CORE_ReturningLead__c == false){
                Task task = CORE_OpportunityUtils.getInterestTask(null, this.datas.prospectiveStudentComment, opp, account);
                tasksToCreate.add(task);
            }
        }

        Map<Id,Id> pocByOpportunities = new Map<Id,Id>();
        for(CORE_Point_of_Contact__c poc : pocs){
            pocByOpportunities.put(poc.CORE_Opportunity__c, poc.Id);
        }

        for(Task task : tasksToCreate){
            task.CORE_Point_of_contact__c = pocByOpportunities.get(task.WhatId);
            task.WhoId = account.PersonContactId;
        }

    }

    @TestVisible
    protected List<Opportunity> getExistingMainOpportunities(List<CORE_Business_Unit__c> businessUnits, id accountId){
        List<Opportunity> mainOpportunities;
        Set<String> businessUnitsExternalIds = new Set<String>();

        for(CORE_Business_Unit__c bu : businessUnits){
            businessUnitsExternalIds.Add(bu.CORE_Business_Unit_ExternalId__c);
        }
        //if(!String.IsBlank(bu.Id)){
        //    mainOpportunities = CORE_OpportunityUtils.getExistingMainOpportunity(bu.Id, accountId);
        //} else if(!String.IsBlank(bu.CORE_Business_Unit_ExternalId__c)){
        //    mainOpportunities = CORE_OpportunityUtils.getExistingMainOpportunityByExternalId(bu.CORE_Business_Unit_ExternalId__c, accountId);
        if(businessUnitsExternalIds.size() > 0){
            mainOpportunities = CORE_OpportunityUtils.getExistingMainOpportunitiesByExternalIds(businessUnitsExternalIds, accountId);
        }
        //}

        return mainOpportunities;
    }

    @TestVisible
    protected Account getAccount(List<Account> existingAccounts){
        System.debug('CORE_LeadFormAcquisitionProcess.getAccount() : START');
        Account account;
        Long dt1 = DateTime.now().getTime();
        Long dt2,dt3;

        Database.DMLOptions dml = new Database.DMLOptions();
        dml.DuplicateRuleHeader.AllowSave = true; 

        dt3 = DateTime.now().getTime();
        switch on existingAccounts.size() {
            when 0 {
                account = CORE_AccountDataMaker.initPersonAccount(this.datas.personAccount,this.countryPicklistIsActivated);
                
                System.debug('0 account found');
                dt2 = DateTime.now().getTime();
                System.debug('CORE_LeadFormAcquisitionProcess.execute() -> initPersonAccount() time execution = ' + (dt2-dt3) + ' ms');
                
                //insert account;
                Database.SaveResult sr = Database.insert(account, dml); 
                if(sr.isSuccess()){
                    account.Id = sr.getId();
                    account = CORE_LeadFormAcquisitionUtils.getAccountFromId(account.Id);
                } else {
                    throw new IllegalArgumentException('Unable to insert account : ' + sr.getErrors().get(0).getMessage());
                }
            }
            when else {
                account = CORE_AccountDataMaker.getUpdatedPersonAccount(existingAccounts.get(0), this.datas.personAccount, this.countryPicklistIsActivated);
                
                System.debug('1 account found');
                dt2 = DateTime.now().getTime();
                System.debug('CORE_LeadFormAcquisitionProcess.execute() -> getUpdatedPersonAccount() time execution = ' + (dt2-dt3) + ' ms');
                
                Boolean isLocked;
                try {
                    Account record = [SELECT ID from account where id = :account.Id FOR UPDATE];
                    isLocked = false;
                } catch(QueryException e) {
                    isLocked = true;
                }

                System.debug('isLocked -> '+isLocked);
                if(!isLocked){
                    //update account;
                    Database.SaveResult sr = Database.update(account, dml);
                    if(!sr.isSuccess()){
                        throw new IllegalArgumentException('Unable to update account : ' + sr.getErrors().get(0).getMessage());
                    }
                }
            }
            /*when else {
                return getAccount(new List<Account> {existingAccounts.get(0)});
            }*/
        }

        dt3 = DateTime.now().getTime();
        System.debug('CORE_LeadFormAcquisitionProcess.execute() -> upsert account time execution = ' + (dt3-dt2) + ' ms');

        System.debug('CORE_LeadFormAcquisitionProcess.getAccount() : END');
        return account;
    }

}