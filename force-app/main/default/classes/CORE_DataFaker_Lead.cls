@IsTest
public with sharing class CORE_DataFaker_Lead {
    public static Lead getLead(String uniqueKey){
        Lead lead = getLeadWithoutInsert(uniqueKey);

        Insert lead;
        return lead;
    }

    public static Lead getLeadWithoutInsert(String uniqueKey){
        Lead lead = new Lead(
            LastName = 'LastName '+ uniqueKey
        );

        return lead;
    }

}