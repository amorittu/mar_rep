@IsTest
public class CORE_OppotunityTriggerHandlerTest {
    @TestSetup
    public static void makeData(){
        CORE_DataFaker_CatalogHierarchy catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test1');
        Account account = CORE_DataFaker_Account.getStudentAccount('test1');
        Opportunity opportunity = CORE_DataFaker_Opportunity.getOpportunity(catalogHierarchy.division.Id, 
            catalogHierarchy.businessUnit.Id, 
            catalogHierarchy.studyArea.Id,
            catalogHierarchy.curriculum.Id,
            catalogHierarchy.level.Id,
            catalogHierarchy.intake.Id, 
            account.Id, 
            'Lead', 
            'Lead - New'
        );
    }

    @IsTest
    public static void beforeUpdate_Should_Verify_Rules(){
        List<Opportunity> opportunities = [SELECT Id,CORE_ReturningLead__c,Owner.Profile.Name,StageName,CORE_OPP_Intake__c,CORE_OPP_Study_Area__c,CORE_OPP_Division__c,CORE_OPP_Level__c,CORE_OPP_Business_Unit__c,CORE_OPP_Curriculum__c,CORE_TECH_Inbound_contact_counter__c, CORE_Set_as_close_Lost__c FROM Opportunity];
        CORE_Division__c myDivision = [SELECT Id,Name FROM CORE_Division__c];
        CORE_Business_Unit__c myBusiness = [SELECT Id,Name FROM CORE_Business_Unit__c];
        CORE_Study_Area__c myStudy = [SELECT Id,Name FROM CORE_Study_Area__c];
        CORE_Curriculum__c myCurriculum = [SELECT Id,CORE_Curriculum_Name__c FROM CORE_Curriculum__c];
        CORE_Level__c myLevel = [SELECT Id,Name FROM CORE_Level__c];
        CORE_Intake__c myIntake = [SELECT Id,Name FROM CORE_Intake__c];

        Opportunity prepareUpdate = opportunities.get(0);
        prepareUpdate.CORE_Set_as_close_Lost__c=true;
        prepareUpdate.CORE_New_Owner__c=UserInfo.getUserId();
        prepareUpdate.CORE_TECH_Inbound_contact_counter__c=4;
        prepareUpdate.CORE_ReturningLead__c=false;
        prepareUpdate.CORE_OPP_Division__c=myDivision.Id;
        prepareUpdate.CORE_OPP_Business_Unit__c=myBusiness.Id;
        prepareUpdate.CORE_OPP_Study_Area__c=myStudy.Id;
        prepareUpdate.CORE_OPP_Curriculum__c=myCurriculum.Id;
        prepareUpdate.CORE_OPP_Intake__c=myIntake.Id;
        prepareUpdate.CORE_OPP_Level__c=myLevel.Id;

        Test.startTest();
        update prepareUpdate;
        Test.stopTest();
        
        List<Opportunity> recupOpportunities = [SELECT Id,CORE_OPP_TLevel__c,CORE_OPP_TIntake__c,CORE_OPP_TCurriculum__c,CORE_OPP_TStudy_Area__c,CORE_OPP_TDivision__c,CORE_OPP_TBusiness_Unit__c,CORE_Last_returning_date__c,CORE_ReturningLead__c,CORE_TECH_Inbound_contact_counter__c,First_Assignation_date__c,StageName,CORE_New_Owner__c,OwnerId FROM Opportunity WHERE Id = :opportunities.get(0).id];
        System.debug('update done'+recupOpportunities.get(0).StageName);

        System.assertEquals('Closed Lost',recupOpportunities.get(0).StageName);
        System.assertEquals(null,recupOpportunities.get(0).CORE_New_Owner__c);
        System.assertEquals(UserInfo.getUserId(),recupOpportunities.get(0).OwnerId);
        System.assertNotEquals(null,recupOpportunities.get(0).CORE_Last_returning_date__c);
        System.assertEquals(true,recupOpportunities.get(0).CORE_ReturningLead__c);
        System.assertEquals(myCurriculum.CORE_Curriculum_Name__c,recupOpportunities.get(0).CORE_OPP_TCurriculum__c);
        System.assertEquals(myStudy.Name,recupOpportunities.get(0).CORE_OPP_TStudy_Area__c);
        System.assertEquals(myBusiness.Name,recupOpportunities.get(0).CORE_OPP_TBusiness_Unit__c);
        System.assertEquals(myDivision.Name,recupOpportunities.get(0).CORE_OPP_TDivision__c);
        System.assertEquals(myIntake.Name,recupOpportunities.get(0).CORE_OPP_TIntake__c);
        System.assertEquals(myLevel.Name,recupOpportunities.get(0).CORE_OPP_TLevel__c);        
    }



    @IsTest
    public static void afterDelete_Should_Upsert_New_DeletedRecords() {
        List<Opportunity> opportunities = [Select Id, StageName, CORE_IsFromDeferal__c, CORE_OPP_Sub_Status__c,IsClosed, RecordTypeId, LastModifiedDate FROM Opportunity];
        Set<Id> recordTypeIds = new Set<Id> {opportunities.get(0).RecordTypeId};
        Map<Id, String> recordTypeDevNames = CORE_RecordTypeUtils.getDeveloperNameByIds('Opportunity', recordTypeIds);


        Test.startTest();
        CORE_OppotunityTriggerHandler.afterDelete(opportunities);
        Test.stopTest();

        List<CORE_Deleted_record__c> results = [Select Id, CORE_Object_Name__c, CORE_Record_Id__c, CORE_Record_Type__c FROM CORE_Deleted_record__c];
        System.assertEquals(1, results.size());
        System.assertEquals('CORE_PKL_Opportunity',results.get(0).CORE_Object_Name__c);
        System.assertEquals(opportunities.get(0).Id, results.get(0).CORE_Record_Id__c);
        System.assertEquals(recordTypeDevNames.get(opportunities.get(0).RecordTypeId), results.get(0).CORE_Record_Type__c);
    }


    @IsTest
    public static void getOpportunitiesWithChangedStatus_Should_return_only_opportunities_with_changed_status() {
        List<Opportunity> opportunities = [Select Id, StageName, CORE_OPP_Sub_Status__c, LastModifiedDate FROM Opportunity];
        Map<Id,Opportunity> oldMapOpportunities = new Map<Id,Opportunity>(opportunities);

        Opportunity newOpportunity = new Opportunity(
            id = opportunities.get(0).Id,
            StageName = 'Prospect',
            CORE_OPP_Sub_Status__c = 'Prospect - new'
        );

        Test.startTest();

        List<Opportunity> opportunitiesWithChanges = CORE_OppotunityTriggerHandler.getOpportunitiesWithChangedStatus(oldMapOpportunities, opportunities);
        System.assertEquals(0, opportunitiesWithChanges.size());

        opportunitiesWithChanges = CORE_OppotunityTriggerHandler.getOpportunitiesWithChangedStatus(oldMapOpportunities, new List<Opportunity>{newOpportunity});
        System.assertEquals(1, opportunitiesWithChanges.size());

        Test.stopTest();
    }

    @IsTest
    public static void getOpportunitiesWithChangedSubStatus_Should_return_only_opportunities_with_changed_subStatus() {
        List<Opportunity> opportunities = [Select Id, StageName, CORE_OPP_Sub_Status__c, LastModifiedDate FROM Opportunity];
        Map<Id,Opportunity> oldMapOpportunities = new Map<Id,Opportunity>(opportunities);

        Opportunity newOpportunity = new Opportunity(
            id = opportunities.get(0).Id,
            StageName = 'Prospect',
            CORE_OPP_Sub_Status__c = 'Prospect - new'
        );

        Test.startTest();

        List<Opportunity> opportunitiesWithChanges = CORE_OppotunityTriggerHandler.getOpportunitiesWithChangedSubStatus(oldMapOpportunities, opportunities);
        System.assertEquals(0, opportunitiesWithChanges.size());

        opportunitiesWithChanges = CORE_OppotunityTriggerHandler.getOpportunitiesWithChangedSubStatus(oldMapOpportunities, new List<Opportunity>{newOpportunity});
        System.assertEquals(1, opportunitiesWithChanges.size());

        Test.stopTest();
    }

    @IsTest
    public static void isStatusChanged_Should_Return_true_if_status_have_changed() {
        Opportunity opp1 = new Opportunity(StageName = 'Lead');
        Opportunity opp2 = new Opportunity(StageName = 'Prospect');

        boolean result =  CORE_OppotunityTriggerHandler.isStatusChanged(opp1,opp2);
        System.assertEquals(true, result);
    }

    @IsTest
    public static void isStatusChanged_Should_Return_false_if_status_have_not_changed() {
        Opportunity opp1 = new Opportunity(StageName = 'Lead');
        Opportunity opp2 = new Opportunity(StageName = 'Lead');

        boolean result =  CORE_OppotunityTriggerHandler.isStatusChanged(opp1,opp2);
        System.assertEquals(false, result);
    }

    @IsTest
    public static void isSubStatusChanged_Should_Return_true_if_subStatus_have_changed() {
        Opportunity opp1 = new Opportunity(CORE_OPP_Sub_Status__c = 'Lead - New');
        Opportunity opp2 = new Opportunity(CORE_OPP_Sub_Status__c = 'Prospect - New');

        boolean result =  CORE_OppotunityTriggerHandler.isSubStatusChanged(opp1,opp2);
        System.assertEquals(true, result);
    }

    @IsTest
    public static void isSubStatusChanged_Should_Return_false_if_subStatus_have_not_changed() {
        Opportunity opp1 = new Opportunity(CORE_OPP_Sub_Status__c = 'Lead - New');
        Opportunity opp2 = new Opportunity(CORE_OPP_Sub_Status__c = 'Lead - New');

        boolean result =  CORE_OppotunityTriggerHandler.isSubStatusChanged(opp1,opp2);
        System.assertEquals(false, result);
    }

    
    @IsTest
    public static void reachLeadDateProcess_should_replace_reachLeadDate_when_conditions_are_met() {
        Opportunity opp1 = [Select Id, StageName,CORE_TECH_Inbound_contact_counter__c,CORE_Reach_Date__c, CORE_Reached_Lead__c,CORE_OPP_Sub_Status__c,LastModifiedDate from opportunity limit 1];
        opp1.CORE_Reach_Date__c = null;
        opp1.CORE_TECH_Inbound_contact_counter__c = 0;
        Opportunity opp2 = opp1.clone(true, true, true, true);
        opp1.StageName = 'Prospect';

        //change status to lead
        CORE_OppotunityTriggerHandler.reachLeadDateProcess(new List<Opportunity> {opp1}, new List<Opportunity> {opp2});
        System.assertEquals(null, opp2.CORE_Reach_Date__c);
        System.assertNotEquals(true, opp2.CORE_Reached_Lead__c);

        opp2.CORE_Reach_Date__c = null;
        opp1.StageName = 'Lead';
        opp2.CORE_OPP_Sub_Status__c = 'Lead - Show';

        //change sub-status (without status changed)
        CORE_OppotunityTriggerHandler.reachLeadDateProcess(new List<Opportunity> {opp1}, new List<Opportunity> {opp2});
        System.assertNotEquals(null, opp2.CORE_Reach_Date__c);
        System.assertEquals(true, opp2.CORE_Reached_Lead__c);
       
        opp2.CORE_Reached_Lead__c = false;
        opp2.CORE_Reach_Date__c = null;
        opp1.StageName = 'Prospect';
        opp2.CORE_OPP_Sub_Status__c = 'Lead - Show';

        //change sub-status (with status changed to lead)
        CORE_OppotunityTriggerHandler.reachLeadDateProcess(new List<Opportunity> {opp1}, new List<Opportunity> {opp2});
        System.assertNotEquals(null, opp2.CORE_Reach_Date__c);
        System.assertEquals(true, opp2.CORE_Reached_Lead__c);

        opp2.CORE_Reached_Lead__c = false;
        opp2.CORE_Reach_Date__c = null;
        opp1.StageName = 'Prospect';
        opp2.StageName = 'Registered';
        opp2.CORE_OPP_Sub_Status__c = 'Lead - Show';


        //change status 
        CORE_OppotunityTriggerHandler.reachLeadDateProcess(new List<Opportunity> {opp1}, new List<Opportunity> {opp2});
        System.assertNotEquals(null, opp2.CORE_Reach_Date__c);
        System.assertNotEquals(true, opp2.CORE_Reached_Lead__c);


        opp2 = opp1.clone(true, true, true, true);
        opp2.CORE_TECH_Inbound_contact_counter__c = 1;
        CORE_OppotunityTriggerHandler.reachLeadDateProcess(new List<Opportunity> {opp1}, new List<Opportunity> {opp2});
        System.assertNotEquals(null, opp2.CORE_Reach_Date__c);
        System.assertEquals(true, opp2.CORE_Reached_Lead__c);
    }
}