/**
* @author Fodil BOUDJEDIEN - Almavia
* @date 2022-03-24
* @group 
* @description Class used by lwc to launch async re-registration process
* @test class 
*/
public class CORE_MassOpportunityReopeningBatch implements Database.Batchable<sobject> {
    
    String academicYear;
    String businessUnitId;
    String listviewWhereConditions;
    
    public CORE_MassOpportunityReopeningBatch(String academicYear,String businessUnitId,String listviewWhereConditions){
        this.academicYear = academicYear;
        this.businessUnitId = businessUnitId;
        this.listviewWhereConditions = listviewWhereConditions;
    }
    
    public Database.QueryLocator start(Database.BatchableContext BC){
        String query = CORE_MassOpportunityReopeningHandler.getMassOpportunityReregistrationQuery(this.businessUnitId,this.academicYear,this.listviewWhereConditions);   
        return Database.getQueryLocator(query);
    }
    public void execute(Database.BatchableContext BC, List<sObject> scope){
        SavePoint sp = Database.setSavePoint();
        CORE_MassOpportunityResultHandler resultHandler = new CORE_MassOpportunityResultHandler();
        resultHandler.settingName = 'Mass_Opportunity_Reregistration';
        List<String> resultsList = new List<String>();
        for (Opportunity opp : (List<Opportunity>) scope){
            resultsList.add('(Name : '+opp.Name+', Id: '+opp.Id+' )');
        }
        try{
            CORE_MassOpportunityReopeningHandler.handleOpportunitiesReregistration(this.academicYear, (List<Opportunity>) scope,this.businessUnitId);
        }
        catch(Exception e){
            Database.rollback(sp);
            resultHandler.resultsList = new List<String>{String.join(resultsList, '\n')};
            resultHandler.sendMail();
        }
    }
    public void finish(Database.BatchableContext BC){
        MassOpportunityReregistration__c settings = MassOpportunityReregistration__c.getOrgDefaults();
        settings.Job_in_Progress__c = false;
        update settings;

    }
}