public with sharing class IT_DynamicLookupHelperWithSharing {
    public IT_DynamicLookupHelperWithSharing() {

    }
    @AuraEnabled(cacheable=true)
    public static list<sObject> fetchLookupData(String searchKey , String sObjectApiName) {    
        List<sObject> returnList = new List<sObject>();
        String userSearchKey = '%' + searchKey + '%';
        String sQuery = 'Select Id,Name From ' + sObjectApiName + ' Where Name Like : userSearchKey order by createdDate DESC LIMIT 5';
        for (sObject obj: database.query(sQuery)) {
            returnList.add(obj);
        }
        return returnList;
    }

    // Method to fetch lookup default value 
    @AuraEnabled
    public static sObject fetchDefaultRecord(String recordId , String sObjectApiName) {
        String sRecId = recordId;    
        String sQuery = 'Select Id,Name From ' + sObjectApiName + ' Where Id = : sRecId LIMIT 1';
        for (sObject obj: database.query(sQuery)) {
            return obj;
        }
        return null;
    }
}