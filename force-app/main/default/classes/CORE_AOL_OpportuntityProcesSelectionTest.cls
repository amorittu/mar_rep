@IsTest
public with sharing class CORE_AOL_OpportuntityProcesSelectionTest {

    private static final String AOL_EXTERNALID = 'setupAol';

    private static final String REQ_BODY = '{'
    + '"aolExternalId": "' + AOL_EXTERNALID + '",'
    + '"selectionTest": {'
    + '"selectionTestDate": "2022-03-29",'
    + '"selectionTestShowNoShow": "CORE_PKL_Cancelled",'
    + '"selectionTestNoShowCounter": 5,'
    + '"selectionTestGrade": 12.50,'
    + '"selectionTestResult": "CORE_PKL_Failed",'
    + '"selectionTestTeacher": "' + UserInfo.getUserId() + '",'
    + '"selectionStatus": "CORE_PKL_Booked",'
    + '"skillTestStartDate": "2022-03-29",'
    + '"selectionProcessStatus": "CORE_PKL_Booked",'
    + '"selectionProcessStartDate": "2022-03-29",'
    + '"selectionProcessEndDate": "2022-03-29",'
    + '"selectionProcessProgramLeader": "Leader",'
    + '"selectionProcessGrade": "12",'
    + '"selectionProcessResult": "CORE_Opp_Failed",'
    + '"selectionTestNeeded": true,'
    + '"selectionProcessNeeded": true'
    + '},'
    + '"additionalFields": {'
    + '"CORE_Campaign_Name__c": "test value"'
    + '},'
    + '"aolProgressionFields": {'
    + '"aolProgression": 50.09,'
    + '"documentProgression": "Started",'
    + '"firstDocumentUploadDate": "2022-03-29",'
    + '"lastAolEventDate": "2022-03-29T08:47:39.689Z",'
    + '"lastDeaAolLoginDate": "2022-03-29T08:47:39.689Z",'
    + '"isAol": true,'
    + '"isAolOnlineRegistered": true,'
    + '"isAolRetailRegistered": true'
    + '}'
    + '}';

    private static final String REQ_BODY_ERROR_INVALID_ID = '{'
    + '"aolExternalId": "invalidID",'
    + '"selectionTest": {'
    + '"selectionTestDate": "2022-03-29",'
    + '"selectionTestShowNoShow": "CORE_PKL_Cancelled",'
    + '"selectionTestNoShowCounter": 5,'
    + '"selectionTestGrade": 12.50,'
    + '"selectionTestResult": "CORE_PKL_Failed",'
    + '"selectionTestTeacher": "' + UserInfo.getUserId() + '",'
    + '"selectionStatus": "CORE_PKL_Booked",'
    + '"skillTestStartDate": "2022-03-29",'
    + '"selectionProcessStatus": "CORE_PKL_Booked",'
    + '"selectionProcessStartDate": "2022-03-29",'
    + '"selectionProcessEndDate": "2022-03-29",'
    + '"selectionProcessProgramLeader": "Leader",'
    + '"selectionProcessGrade": "12",'
    + '"selectionProcessResult": "CORE_Opp_Failed",'
    + '"selectionTestNeeded": true,'
    + '"selectionProcessNeeded": true'
    + '},'
    + '"additionalFields": {'
    + '"CORE_Campaign_Name__c": "test value"'
    + '},'
    + '"aolProgressionFields": {'
    + '"aolProgression": 50.09,'
    + '"documentProgression": "Started",'
    + '"firstDocumentUploadDate": "2022-03-29",'
    + '"lastAolEventDate": "2022-03-29T08:47:39.689Z",'
    + '"lastDeaAolLoginDate": "2022-03-29T08:47:39.689Z",'
    + '"isAol": true,'
    + '"isAolOnlineRegistered": true,'
    + '"isAolRetailRegistered": true'
    + '}'
    + '}';

    private static final String REQ_BODY_ERROR_NO_ID = '{'
    + '"aolExternalId": "",'
    + '"selectionTest": {'
    + '"selectionTestDate": "2022-03-29",'
    + '"selectionTestShowNoShow": "CORE_PKL_Cancelled",'
    + '"selectionTestNoShowCounter": 5,'
    + '"selectionTestGrade": 12.50,'
    + '"selectionTestResult": "CORE_PKL_Failed",'
    + '"selectionTestTeacher": "' + UserInfo.getUserId() + '",'
    + '"selectionStatus": "CORE_PKL_Booked",'
    + '"skillTestStartDate": "2022-03-29",'
    + '"selectionProcessStatus": "CORE_PKL_Booked",'
    + '"selectionProcessStartDate": "2022-03-29",'
    + '"selectionProcessEndDate": "2022-03-29",'
    + '"selectionProcessProgramLeader": "Leader",'
    + '"selectionProcessGrade": "12",'
    + '"selectionProcessResult": "CORE_Opp_Failed",'
    + '"selectionTestNeeded": true,'
    + '"selectionProcessNeeded": true'
    + '},'
    + '"additionalFields": {'
    + '"CORE_Campaign_Name__c": "test value"'
    + '},'
    + '"aolProgressionFields": {'
    + '"aolProgression": 50.09,'
    + '"documentProgression": "Started",'
    + '"firstDocumentUploadDate": "2022-03-29",'
    + '"lastAolEventDate": "2022-03-29T08:47:39.689Z",'
    + '"lastDeaAolLoginDate": "2022-03-29T08:47:39.689Z",'
    + '"isAol": true,'
    + '"isAolOnlineRegistered": true,'
    + '"isAolRetailRegistered": true'
    + '}'
    + '}';

    private static final String REQ_BODY_ERROR = '{'
    + '"aolExternalId": "' + AOL_EXTERNALID + '",'
    + '"selectionTest": {'
    + '"selectionTestDate": "2022-03-29",'
    + '"selectionTestShowNoShow": "invalid_value",'
    + '"selectionTestNoShowCounter": 5,'
    + '"selectionTestGrade": 12.50,'
    + '"selectionTestResult": "CORE_PKL_Failed",'
    + '"selectionTestTeacher": "' + UserInfo.getUserId() + '",'
    + '"selectionStatus": "CORE_PKL_Booked",'
    + '"skillTestStartDate": "2022-03-29",'
    + '"selectionProcessStatus": "CORE_PKL_Booked",'
    + '"selectionProcessStartDate": "2022-03-29",'
    + '"selectionProcessEndDate": "2022-03-29",'
    + '"selectionProcessProgramLeader": "Leader",'
    + '"selectionProcessGrade": "12",'
    + '"selectionProcessResult": "CORE_Opp_Failed",'
    + '"selectionTestNeeded": true,'
    + '"selectionProcessNeeded": true'
    + '},'
    + '"additionalFields": {'
    + '"CORE_Campaign_Name__c": "test value"'
    + '},'
    + '"aolProgressionFields": {'
    + '"aolProgression": 50.09,'
    + '"documentProgression": "Started",'
    + '"firstDocumentUploadDate": "2022-03-29",'
    + '"lastAolEventDate": "2022-03-29T08:47:39.689Z",'
    + '"lastDeaAolLoginDate": "2022-03-29T08:47:39.689Z",'
    + '"isAol": true,'
    + '"isAolOnlineRegistered": true,'
    + '"isAolRetailRegistered": true'
    + '}'
    + '}';

    @TestSetup
    static void makeData(){
        CORE_CountrySpecificSettings__c countrySetting = new CORE_CountrySpecificSettings__c(
                CORE_FieldPrefix__c = CORE_AOL_Constants.PREFIX_CORE
        );
        database.insert(countrySetting, true);

        CORE_CatalogHierarchyModel catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test').catalogHierarchy;
        Account account = CORE_DataFaker_Account.getStudentAccount('test');
        Opportunity  opportunity = CORE_DataFaker_Opportunity.getAOLOpportunity(catalogHierarchy.division.Id, 
            catalogHierarchy.businessUnit.Id, 
            account.Id, 
            CORE_AOL_Constants.PKL_OPP_STAGENAME_PROSPECT, 
            CORE_AOL_Constants.PKL_OPP_SUBSTATUS_PROSPECTAPPSTARTED, 
            AOL_EXTERNALID
        );
    }

    @IsTest
    public static void opportunityProcessSelectionProcess_Should_Update_Opportunity_and_return_it() {
        CORE_AOL_Wrapper.AOLProgressionFields aolProgressionFields= CORE_DataFaker_AOL_Wrapper.getAolProgressionFieldsWrapper();
        CORE_AOL_Wrapper.SelectionTest selectionTest = CORE_DataFaker_AOL_Wrapper.getProcessSelectionWrapper();

        CORE_AOL_GlobalWorker globalWorker = new CORE_AOL_GlobalWorker(aolProgressionFields,AOL_EXTERNALID);

        Opportunity opportunity = CORE_AOL_OpportunityProcessSelection.opportunityProcessSelectionProcess(globalWorker, selectionTest);
        database.update(opportunity, true);

        opportunity = [SELECT Id,CORE_Skill_test_start_date__c,CORE_OPP_Selection_Test_Date__c,CORE_Selection_process_program_leader__c,CORE_OPP_Selection_Test_Teacher__c,CORE_Selection_process_status__c,CORE_OPP_Selection_Test_Result__c,CORE_Selection_status__c,CORE_Selection_process_start_date__c,CORE_Selection_process_end_date__c,CORE_OPP_Selection_Test_Show_No_Show__c,CORE_OPP_Selection_Test_No_Show_Counter__c,CORE_OPP_Selection_Test_Grade__c,CORE_Selection_Process_Grade__c,CORE_Selection_Process_Result__c,CORE_Opp_Selection_Test_Needed__c,CORE_Opp_Selection_Process_Needed__c
        FROM Opportunity];

        System.assertEquals(selectionTest.selectionTestTeacher, opportunity.CORE_OPP_Selection_Test_Teacher__c);
        System.assertEquals(selectionTest.selectionTestResult, opportunity.CORE_OPP_Selection_Test_Result__c);
        System.assertEquals(selectionTest.selectionStatus, opportunity.CORE_Selection_status__c);
        System.assertEquals(selectionTest.selectionProcessStatus, opportunity.CORE_Selection_process_status__c);
        System.assertEquals(selectionTest.selectionProcessStartDate, opportunity.CORE_Selection_process_start_date__c);
        System.assertEquals(selectionTest.selectionProcessEndDate, opportunity.CORE_Selection_process_end_date__c);
        System.assertEquals(selectionTest.selectionTestShowNoShow, opportunity.CORE_OPP_Selection_Test_Show_No_Show__c);
        System.assertEquals(selectionTest.selectionTestNoShowCounter, opportunity.CORE_OPP_Selection_Test_No_Show_Counter__c);
        System.assertEquals(selectionTest.selectionTestGrade, opportunity.CORE_OPP_Selection_Test_Grade__c);
        System.assertEquals(selectionTest.selectionProcessProgramLeader, opportunity.CORE_Selection_process_program_leader__c);
        System.assertEquals(selectionTest.skillTestStartDate, opportunity.CORE_Skill_test_start_date__c);
        System.assertEquals(selectionTest.selectionTestDate, opportunity.CORE_OPP_Selection_Test_Date__c);
        System.assertEquals(selectionTest.selectionProcessGrade, opportunity.CORE_Selection_Process_Grade__c);
        System.assertEquals(selectionTest.selectionProcessResult, opportunity.CORE_Selection_Process_Result__c);
        System.assertEquals(selectionTest.selectionTestNeeded, opportunity.CORE_Opp_Selection_Test_Needed__c);
        System.assertEquals(selectionTest.selectionProcessNeeded, opportunity.CORE_Opp_Selection_Process_Needed__c);
    }

    @IsTest
    private static void execute_should_return_an_error_if_invalid_externalID(){
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/aol/opportunity/process/selection';

        req.httpMethod = 'PUT';
        req.requestBody = Blob.valueOf(REQ_BODY_ERROR_INVALID_ID);

        RestContext.request = req;
        RestContext.response= res;

        CORE_AOL_Wrapper.ResultWrapperGlobal result = CORE_AOL_OpportunityProcessSelection.execute();

        System.assertEquals('KO', result.status);

    }

    @IsTest
    private static void execute_should_return_an_error_if_no_externalID(){
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/aol/opportunity/process/selection';

        req.httpMethod = 'PUT';
        req.requestBody = Blob.valueOf(REQ_BODY_ERROR_NO_ID);

        RestContext.request = req;
        RestContext.response= res;

        CORE_AOL_Wrapper.ResultWrapperGlobal result = CORE_AOL_OpportunityProcessSelection.execute();

        System.assertEquals('KO', result.status);

    }

    @IsTest
    private static void execute_should_return_an_error_if_something_goes_wrong(){
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/aol/opportunity/process/selection';

        req.httpMethod = 'PUT';
        req.requestBody = Blob.valueOf(REQ_BODY_ERROR);

        RestContext.request = req;
        RestContext.response= res;

        CORE_AOL_Wrapper.ResultWrapperGlobal result = CORE_AOL_OpportunityProcessSelection.execute();

        System.assertEquals('KO', result.status);

    }

    @IsTest
    private static void execute_should_return_a_success_if_all_updates_are_ok(){
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/aol/opportunity/process/selection';

        req.httpMethod = 'PUT';
        req.requestBody = Blob.valueOf(REQ_BODY);

        RestContext.request = req;
        RestContext.response= res;

        CORE_AOL_Wrapper.ResultWrapperGlobal result = CORE_AOL_OpportunityProcessSelection.execute();

        System.assertEquals('OK', result.status);
    }
}