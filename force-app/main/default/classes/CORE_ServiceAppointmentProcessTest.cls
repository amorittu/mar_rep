@IsTest
public with sharing class CORE_ServiceAppointmentProcessTest {
    private static final dateTime DT_NOW = DateTime.Now();

    @TestSetup
    static void makeData(){
        CORE_DataFaker_CatalogHierarchy catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test1');
        Account account = CORE_DataFaker_Account.getStudentAccount('test1');

        Opportunity opportunity1 = CORE_DataFaker_Opportunity.getOpportunity(catalogHierarchy.division.Id, 
            catalogHierarchy.businessUnit.Id, 
            catalogHierarchy.studyArea.Id,
            catalogHierarchy.curriculum.Id,
            catalogHierarchy.level.Id,
            catalogHierarchy.intake.Id, 
            account.Id, 
            'Lead', 
            'Lead - New'
        );
        opportunity1.CORE_Lead_Source_External_Id__c = 'test1';

        Opportunity opportunity2 = CORE_DataFaker_Opportunity.getOpportunity(catalogHierarchy.division.Id, 
            catalogHierarchy.businessUnit.Id, 
            catalogHierarchy.studyArea.Id,
            catalogHierarchy.curriculum.Id,
            catalogHierarchy.level.Id,
            catalogHierarchy.intake.Id, 
            account.Id, 
            'Lead', 
            'Lead - New'
        );

        opportunity2.CORE_Lead_Source_External_Id__c = 'test2';

        Opportunity opportunity3 = CORE_DataFaker_Opportunity.getOpportunity(catalogHierarchy.division.Id, 
            catalogHierarchy.businessUnit.Id, 
            catalogHierarchy.studyArea.Id,
            catalogHierarchy.curriculum.Id,
            catalogHierarchy.level.Id,
            catalogHierarchy.intake.Id, 
            account.Id, 
            'Lead', 
            'Lead - New'
        );
        opportunity3.CORE_Lead_Source_External_Id__c = null;

        
        Lead lead1 = CORE_DataFaker_Lead.getLead('test1');
        Lead lead2 = CORE_DataFaker_Lead.getLead('test1');
        Lead lead3 = CORE_DataFaker_Lead.getLead('test1');
        
        ServiceAppointment sa1 = CORE_DataFaker_ServiceAppointment.getServiceAppointmentWithoutInsert(lead1.Id, DT_NOW);
        ServiceAppointment sa2 = CORE_DataFaker_ServiceAppointment.getServiceAppointmentWithoutInsert(lead2.Id, DT_NOW);
        ServiceAppointment sa3 = CORE_DataFaker_ServiceAppointment.getServiceAppointmentWithoutInsert(lead3.Id, DT_NOW);
        sa1.CORE_Lead_form_acquisition_unique_key__c = opportunity1.CORE_Lead_Source_External_Id__c;
        sa2.CORE_Lead_form_acquisition_unique_key__c = opportunity2.CORE_Lead_Source_External_Id__c;
        sa3.CORE_Lead_form_acquisition_unique_key__c = opportunity3.CORE_Lead_Source_External_Id__c;

        Insert new List<ServiceAppointment> {sa1, sa2, sa3};

        Update new List<Opportunity>{opportunity1, opportunity2, opportunity3};

        User user1 = CORE_DataFaker_User.getUser('user1');
        User user2 = CORE_DataFaker_User.getUser('user2');

        ServiceResource sr1 = CORE_DataFaker_ServiceAppointment.getServiceResource(user1.Id,'service resource 1');
        ServiceResource sr2 = CORE_DataFaker_ServiceAppointment.getServiceResource(user2.Id,'service resource 2');
        AssignedResource ar1 = CORE_DataFaker_ServiceAppointment.getAssignedResource(sa1.Id, sr1.Id);
        AssignedResource ar2 = CORE_DataFaker_ServiceAppointment.getAssignedResourceWithoutInsert(sa1.Id, sr2.Id);
        ar2.IsPrimaryResource = false;
        Insert ar2;

        AssignedResource ar3 = CORE_DataFaker_ServiceAppointment.getAssignedResource(sa2.Id, sr1.Id);
        AssignedResource ar4 = CORE_DataFaker_ServiceAppointment.getAssignedResource(sa3.Id, sr2.Id);

    }
    
    @IsTest
    public static void execute_should_duplicate_serviceAppointments_and_assignedResources_if_exist_and_delete_old_references() {
        List<Opportunity> opportunities = [SELECT Id FROM Opportunity];
        Set<Id> opportunitiesIds = (new Map<Id, Opportunity> (opportunities)).keySet();

        List<ServiceAppointment> serviceAppointmentsRelatedToOpps = [SELECT Id FROM ServiceAppointment where ParentRecordId IN :opportunitiesIds];
        List<ServiceAppointment> allServiceAppointments = [SELECT Id FROM ServiceAppointment];
        
        System.assertEquals(true, serviceAppointmentsRelatedToOpps.isEmpty());
        System.assertEquals(3, allServiceAppointments.size());
        
        Set<Id> serviceAppointmentsRelatedToOppsIds = (new Map<Id, ServiceAppointment> (serviceAppointmentsRelatedToOpps)).keySet();
        List<AssignedResource> assignedResourceRelatedToOpps = [SELECT Id FROM AssignedResource where ServiceAppointmentId IN :serviceAppointmentsRelatedToOppsIds];
        List<AssignedResource> allAssignedResource = [SELECT Id FROM AssignedResource];

        System.assertEquals(true, assignedResourceRelatedToOpps.isEmpty());
        System.assertEquals(4, allAssignedResource.size());

        Test.startTest();
        CORE_ServiceAppointmentProcess.execute(opportunitiesIds);
        
        serviceAppointmentsRelatedToOpps = [SELECT Id FROM ServiceAppointment where ParentRecordId IN :opportunitiesIds];
        allServiceAppointments = [SELECT Id FROM ServiceAppointment];

        System.assertEquals(2, serviceAppointmentsRelatedToOpps.size());
        System.assertEquals(3, allServiceAppointments.size());

        serviceAppointmentsRelatedToOppsIds = (new Map<Id, ServiceAppointment> (serviceAppointmentsRelatedToOpps)).keySet();
        assignedResourceRelatedToOpps = [SELECT Id FROM AssignedResource where ServiceAppointmentId IN :serviceAppointmentsRelatedToOppsIds];
        allAssignedResource = [SELECT Id FROM AssignedResource];

        System.assertEquals(3, assignedResourceRelatedToOpps.size());
        System.assertEquals(4, allAssignedResource.size());
        Test.stopTest();

        
    }
}