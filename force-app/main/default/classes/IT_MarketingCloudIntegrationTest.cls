@IsTest
private class IT_MarketingCloudIntegrationTest {

    @IsTest
    static void testBehaviorLogin() {
        Test.setMock(HttpCalloutMock.class, new IT_MockMarketingCloudIntegration('token'));
        IT_MarketingCloudIntegration mcInteg = IT_MarketingCloudIntegration.getInstance('IM');
        // Constants.BU_WG_B2C
        mcInteg.requestTokenV2('IM');
    }

    @IsTest
    static void testBehaviorMCIntegration() {
        Test.setMock(HttpCalloutMock.class, new IT_MockMarketingCloudIntegration('/interaction/v1/events'));
        IT_MarketingCloudIntegration mcInteg = IT_MarketingCloudIntegration.getInstance('IM');
        // Constants.BU_WG_B2C
        mcInteg.requestTokenV2('IM');
    }
}