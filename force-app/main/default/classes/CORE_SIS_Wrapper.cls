global class CORE_SIS_Wrapper {
    
    global class OpportunityWrapper extends CORE_ObjectWrapper {
        global String enrollOppId; 
		global String sisOpportunityId;
        global String accountId;
		global String academicYear;
        global String businessUnit;
        global String curriculum;
		global String level;
        global String intake;
        global String product;
		global String status;
        global String substatus;
        @TestVisible
        OpportunityWrapper(String enrollOppId,String sisOpportunityId,String accountId,String academicYear,String businessUnit,String curriculum,String level,String intake,String product,String status,String substatus){
            this.enrollOppId=enrollOppId; 
		    this.sisOpportunityId=sisOpportunityId;
            this.accountId=accountId;
		    this.academicYear=academicYear;
            this.businessUnit=businessUnit;
            this.curriculum=curriculum;
		    this.level=level;
            this.intake=intake;
            this.product=product;
		    this.status=status;
            this.substatus=substatus;
        }

    }
    global class ResultOpportunityWrapper extends CORE_ObjectWrapper {
		global String Id;
		global String sisOpportunityId;
        
        global ResultOpportunityWrapper(){
        }
        global ResultOpportunityWrapper(Opportunity opportunity){
            this.Id = opportunity.Id;
            this.sisOpportunityId = opportunity?.CORE_OPP_SIS_Opportunity_Id__c;
		}
	}
    global class ResultWrapperGlobal {
		global String status;
        global String message;
        global ResultOpportunityWrapper opportunity;
        global ResultWrapperGlobal(){
		}
        global ResultWrapperGlobal(String status, String message){
			this.status = status;
			this.message = message;
		}
        global ResultWrapperGlobal(String statut, String message, ResultOpportunityWrapper opportunity){
			this.status = statut;
			this.message = message;
            this.opportunity = opportunity;
		}
	}
    global class ResultWrapperAccountList {
        global Double totalSize;
        global String nextRecordsUrl;
		global List<Account> accounts;

        global String status;
        global String message;
        
        global ResultWrapperAccountList(){
		}

		global ResultWrapperAccountList(Double totalSize, String nextRecordsUrl,List<Account> accounts,String status){
            this.nextRecordsUrl= nextRecordsUrl;
            this.accounts = accounts;
            this.totalSize= totalSize;
            this.status= status;
		}
        global ResultWrapperAccountList(String status, String message){
            this.status= status;
            this.message = message;
		}
	}

    global class ResultWrapperAccount {
		global Account account;
        global String status;
        global String message;
        
        global ResultWrapperAccount(){
		}

		global ResultWrapperAccount(Account account,String status){
            this.account = account;
            this.status= status;
		}
        global ResultWrapperAccount(String status, String message){
            this.status= status;
            this.message = message;
		}
	}

    global class ResultWrapperOpportunities {
        global Double totalSize;
        global String nextRecordsUrl;
		global List<Opportunity> opportunities;

        global String status;
        global String message;
        
        global ResultWrapperOpportunities(){
		}

		global ResultWrapperOpportunities(Double totalSize, String nextRecordsUrl, List<Opportunity> opportunities, String status){
            this.nextRecordsUrl = nextRecordsUrl;
            this.opportunities = opportunities;
            this.totalSize = totalSize;
            this.status = status;
		}

        global ResultWrapperOpportunities(String status, String message){
            this.status = status;
            this.message = message;
		}
	}

    global class ResultWrapperOpportunity {
		global Opportunity opportunity;

        global String status;
        global String message;
        
        global ResultWrapperOpportunity(){
		}

		global ResultWrapperOpportunity(Opportunity opportunity, String status){
            this.opportunity = opportunity;
            this.status = status;
		}

        global ResultWrapperOpportunity(String status, String message){
            this.status = status;
            this.message = message;
		}
	}

    global class ResultWrapperProducts {
        global Double totalSize;
        global String nextRecordsUrl;
		global List<OpportunityLineItem> oliList;

        global String status;
        global String message;
        
        global ResultWrapperProducts(){
		}

		global ResultWrapperProducts(Double totalSize, String nextRecordsUrl, List<OpportunityLineItem> oliList, String status){
            this.nextRecordsUrl = nextRecordsUrl;
            this.oliList = oliList;
            this.totalSize = totalSize;
            this.status = status;
		}

        global ResultWrapperProducts(String status, String message){
            this.status = status;
            this.message = message;
		}
	}

    global class ResultWrapperProduct {
		global OpportunityLineItem oli;

        global String status;
        global String message;
        
        global ResultWrapperProduct(){
		}

		global ResultWrapperProduct(OpportunityLineItem oli, String status){
            this.oli = oli;
            this.status = status;
		}

        global ResultWrapperProduct(String status, String message){
            this.status = status;
            this.message = message;
		}
	}

    global class ResultWrapperDeletedRecords {
        global Double totalSize;
        global String nextRecordsUrl;
		global List<CORE_Deleted_record__c> deletedRecords;

        global String status;
        global String message;
        
        global ResultWrapperDeletedRecords(){
		}

		global ResultWrapperDeletedRecords(Double totalSize, String nextRecordsUrl, List<CORE_Deleted_record__c> deletedRecords, String status){
            this.nextRecordsUrl = nextRecordsUrl;
            this.deletedRecords = deletedRecords;
            this.totalSize = totalSize;
            this.status = status;
		}

        global ResultWrapperDeletedRecords(String status, String message){
            this.status = status;
            this.message = message;
		}
	}
}