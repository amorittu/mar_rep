@isTest
public class IT_InvocableBusinessHourCalculationTest { 
	
    @isTest
    public static void calculateBHAdditionTest() {
        List<BusinessHours> bhs=[select id from BusinessHours where IsDefault=true];            
        IT_InvocableBusinessHourCalculation.FlowInputs[] flowinputArray = new IT_InvocableBusinessHourCalculation.FlowInputs[]{};
        
        IT_InvocableBusinessHourCalculation.FlowInputs flowinput1 = new IT_InvocableBusinessHourCalculation.FlowInputs();    
        flowinput1.businessHoursId 		    = bhs.get(0).Id;
        flowinput1.operation				= IT_InvocableBusinessHourCalculation.ADDITION;
        flowinput1.startDateTime			= System.today();
        flowinput1.unitOfMeasure			= 'second';
        flowinput1.value					= 2;
        flowinputArray.add(flowinput1);

        IT_InvocableBusinessHourCalculation.FlowInputs flowinput2 = new IT_InvocableBusinessHourCalculation.FlowInputs();    
        flowinput2.businessHoursId 		    = bhs.get(0).Id;
        flowinput2.operation				= IT_InvocableBusinessHourCalculation.ADDITION;
        flowinput2.startDateTime			= System.today();
        flowinput2.unitOfMeasure			= 'minute';
        flowinput2.value					= 2;
        flowinputArray.add(flowinput2);
        
        IT_InvocableBusinessHourCalculation.FlowInputs flowinput3 = new IT_InvocableBusinessHourCalculation.FlowInputs();    
        flowinput3.businessHoursId 		    = bhs.get(0).Id;
        flowinput3.operation				= IT_InvocableBusinessHourCalculation.ADDITION;
        flowinput3.startDateTime			= System.today();
        flowinput3.unitOfMeasure			= 'day';
        flowinput3.value					= 2;
        flowinputArray.add(flowinput3);

        IT_InvocableBusinessHourCalculation.FlowInputs flowinput4 = new IT_InvocableBusinessHourCalculation.FlowInputs();    
        flowinput4.businessHoursId 		    = bhs.get(0).Id;
        flowinput4.operation				= IT_InvocableBusinessHourCalculation.ADDITION;
        flowinput4.startDateTime			= System.today();
        flowinput4.unitOfMeasure			= 'hour';
        flowinput4.value					= 2;
        flowinputArray.add(flowinput4);

        IT_InvocableBusinessHourCalculation.FlowInputs flowinput5 = new IT_InvocableBusinessHourCalculation.FlowInputs();    
        flowinput5.businessHoursId 		    = bhs.get(0).Id;
        flowinput5.operation				= IT_InvocableBusinessHourCalculation.ADDITION;
        flowinput5.startDateTime			= System.today();
        flowinput5.unitOfMeasure			= 'week';
        flowinput5.value					= 2;
        flowinputArray.add(flowinput5);

        IT_InvocableBusinessHourCalculation.FlowInputs flowinput6 = new IT_InvocableBusinessHourCalculation.FlowInputs();    
        flowinput6.businessHoursId 		    = bhs.get(0).Id;
        flowinput6.operation				= IT_InvocableBusinessHourCalculation.ADDITION;
        flowinput6.startDateTime			= System.today();
        flowinput6.unitOfMeasure			= 'month';
        flowinput6.value					= 2;
        flowinputArray.add(flowinput6);

        IT_InvocableBusinessHourCalculation.FlowInputs flowinput7 = new IT_InvocableBusinessHourCalculation.FlowInputs();    
        flowinput7.businessHoursId 		    = bhs.get(0).Id;
        flowinput7.operation				= IT_InvocableBusinessHourCalculation.ADDITION;
        flowinput7.startDateTime			= System.today();
        flowinput7.unitOfMeasure			= 'year';
        flowinput7.value					= 2;
        flowinputArray.add(flowinput7);
        
        Test.startTest();
        	IT_InvocableBusinessHourCalculation.calculate(flowinputArray);
        Test.stopTest();
    }	
    
    @isTest
    public static void calculateBHSubstractionTest() {
        
        List<BusinessHours> bhs=[select id from BusinessHours where IsDefault=true];            
        IT_InvocableBusinessHourCalculation.FlowInputs[] flowinputArray = new IT_InvocableBusinessHourCalculation.FlowInputs[]{};
            
        IT_InvocableBusinessHourCalculation.FlowInputs flowinput1 = new IT_InvocableBusinessHourCalculation.FlowInputs();    
        flowinput1.businessHoursId 		    = bhs.get(0).Id;
        flowinput1.operation				= IT_InvocableBusinessHourCalculation.SUBSTRACTION;
        flowinput1.startDateTime			= System.today();
        flowinput1.unitOfMeasure			= 'second';
        flowinput1.value					= 2;
        flowinputArray.add(flowinput1);

        IT_InvocableBusinessHourCalculation.FlowInputs flowinput2 = new IT_InvocableBusinessHourCalculation.FlowInputs();    
        flowinput2.businessHoursId 		    = bhs.get(0).Id;
        flowinput2.operation				= IT_InvocableBusinessHourCalculation.SUBSTRACTION;
        flowinput2.startDateTime			= System.today();
        flowinput2.unitOfMeasure			= 'minute';
        flowinput2.value					= 2;
        flowinputArray.add(flowinput2);
        
        IT_InvocableBusinessHourCalculation.FlowInputs flowinput3 = new IT_InvocableBusinessHourCalculation.FlowInputs();    
        flowinput3.businessHoursId 		    = bhs.get(0).Id;
        flowinput3.operation				= IT_InvocableBusinessHourCalculation.SUBSTRACTION;
        flowinput3.startDateTime			= System.today();
        flowinput3.unitOfMeasure			= 'day';
        flowinput3.value					= 2;
        flowinputArray.add(flowinput3);

        IT_InvocableBusinessHourCalculation.FlowInputs flowinput4 = new IT_InvocableBusinessHourCalculation.FlowInputs();    
        flowinput4.businessHoursId 		    = bhs.get(0).Id;
        flowinput4.operation				= IT_InvocableBusinessHourCalculation.SUBSTRACTION;
        flowinput4.startDateTime			= System.today();
        flowinput4.unitOfMeasure			= 'hour';
        flowinput4.value					= 2;
        flowinputArray.add(flowinput4);

        IT_InvocableBusinessHourCalculation.FlowInputs flowinput5 = new IT_InvocableBusinessHourCalculation.FlowInputs();    
        flowinput5.businessHoursId 		    = bhs.get(0).Id;
        flowinput5.operation				= IT_InvocableBusinessHourCalculation.SUBSTRACTION;
        flowinput5.startDateTime			= System.today();
        flowinput5.unitOfMeasure			= 'week';
        flowinput5.value					= 2;
        flowinputArray.add(flowinput5);

        IT_InvocableBusinessHourCalculation.FlowInputs flowinput6 = new IT_InvocableBusinessHourCalculation.FlowInputs();    
        flowinput6.businessHoursId 		    = bhs.get(0).Id;
        flowinput6.operation				= IT_InvocableBusinessHourCalculation.SUBSTRACTION;
        flowinput6.startDateTime			= System.today();
        flowinput6.unitOfMeasure			= 'month';
        flowinput6.value					= 2;
        flowinputArray.add(flowinput6);

        IT_InvocableBusinessHourCalculation.FlowInputs flowinput7 = new IT_InvocableBusinessHourCalculation.FlowInputs();    
        flowinput7.businessHoursId 		    = bhs.get(0).Id;
        flowinput7.operation				= IT_InvocableBusinessHourCalculation.SUBSTRACTION;
        flowinput7.startDateTime			= System.today();
        flowinput7.unitOfMeasure			= 'year';
        flowinput7.value					= 2;
        flowinputArray.add(flowinput7);

        Test.startTest();
            List<IT_InvocableBusinessHourCalculation.FlowOutputs> output = IT_InvocableBusinessHourCalculation.calculate(flowinputArray);
            System.assertNotEquals(output, null);
        Test.stopTest();
        
    }	

    @isTest
    public static void calculateCalendarAdditionTest() {
        IT_InvocableBusinessHourCalculation.FlowInputs[] flowinputArray = new IT_InvocableBusinessHourCalculation.FlowInputs[]{};
        
        IT_InvocableBusinessHourCalculation.FlowInputs flowinput1 = new IT_InvocableBusinessHourCalculation.FlowInputs();    

        flowinput1.operation				= IT_InvocableBusinessHourCalculation.ADDITION;
        flowinput1.startDateTime			= System.today();
        flowinput1.unitOfMeasure			= 'second';
        flowinput1.value					= 2;
        flowinputArray.add(flowinput1);

        IT_InvocableBusinessHourCalculation.FlowInputs flowinput2 = new IT_InvocableBusinessHourCalculation.FlowInputs();    
        flowinput2.operation				= IT_InvocableBusinessHourCalculation.ADDITION;
        flowinput2.startDateTime			= System.today();
        flowinput2.unitOfMeasure			= 'minute';
        flowinput2.value					= 2;
        flowinputArray.add(flowinput2);
        
        IT_InvocableBusinessHourCalculation.FlowInputs flowinput3 = new IT_InvocableBusinessHourCalculation.FlowInputs();    
        flowinput3.operation				= IT_InvocableBusinessHourCalculation.ADDITION;
        flowinput3.startDateTime			= System.today();
        flowinput3.unitOfMeasure			= 'day';
        flowinput3.value					= 2;
        flowinputArray.add(flowinput3);

        IT_InvocableBusinessHourCalculation.FlowInputs flowinput4 = new IT_InvocableBusinessHourCalculation.FlowInputs();    
        flowinput4.operation				= IT_InvocableBusinessHourCalculation.ADDITION;
        flowinput4.startDateTime			= System.today();
        flowinput4.unitOfMeasure			= 'hour';
        flowinput4.value					= 2;
        flowinputArray.add(flowinput4);

        IT_InvocableBusinessHourCalculation.FlowInputs flowinput5 = new IT_InvocableBusinessHourCalculation.FlowInputs();    
        flowinput5.operation				= IT_InvocableBusinessHourCalculation.ADDITION;
        flowinput5.startDateTime			= System.today();
        flowinput5.unitOfMeasure			= 'week';
        flowinput5.value					= 2;
        flowinputArray.add(flowinput5);

        IT_InvocableBusinessHourCalculation.FlowInputs flowinput6 = new IT_InvocableBusinessHourCalculation.FlowInputs();    
        flowinput6.operation				= IT_InvocableBusinessHourCalculation.ADDITION;
        flowinput6.startDateTime			= System.today();
        flowinput6.unitOfMeasure			= 'month';
        flowinput6.value					= 2;
        flowinputArray.add(flowinput6);

        IT_InvocableBusinessHourCalculation.FlowInputs flowinput7 = new IT_InvocableBusinessHourCalculation.FlowInputs();    
        flowinput7.operation				= IT_InvocableBusinessHourCalculation.ADDITION;
        flowinput7.startDateTime			= System.today();
        flowinput7.unitOfMeasure			= 'year';
        flowinput7.value					= 2;
        flowinputArray.add(flowinput7);
        
        Test.startTest();
            List<IT_InvocableBusinessHourCalculation.FlowOutputs> output = IT_InvocableBusinessHourCalculation.calculate(flowinputArray);
            System.assertNotEquals(output, null);
        Test.stopTest();
    }	
    
    @isTest
    public static void calculateCalendarSubstractionTest() {
        
        IT_InvocableBusinessHourCalculation.FlowInputs[] flowinputArray = new IT_InvocableBusinessHourCalculation.FlowInputs[]{};
            
        IT_InvocableBusinessHourCalculation.FlowInputs flowinput1 = new IT_InvocableBusinessHourCalculation.FlowInputs();    

        flowinput1.operation				= IT_InvocableBusinessHourCalculation.SUBSTRACTION;
        flowinput1.startDateTime			= System.today();
        flowinput1.unitOfMeasure			= 'second';
        flowinput1.value					= 2;
        flowinputArray.add(flowinput1);

        IT_InvocableBusinessHourCalculation.FlowInputs flowinput2 = new IT_InvocableBusinessHourCalculation.FlowInputs();    
        flowinput2.operation				= IT_InvocableBusinessHourCalculation.SUBSTRACTION;
        flowinput2.startDateTime			= System.today();
        flowinput2.unitOfMeasure			= 'minute';
        flowinput2.value					= 2;
        flowinputArray.add(flowinput2);
        
        IT_InvocableBusinessHourCalculation.FlowInputs flowinput3 = new IT_InvocableBusinessHourCalculation.FlowInputs();    
        flowinput3.operation				= IT_InvocableBusinessHourCalculation.SUBSTRACTION;
        flowinput3.startDateTime			= System.today();
        flowinput3.unitOfMeasure			= 'day';
        flowinput3.value					= 2;
        flowinputArray.add(flowinput3);

        IT_InvocableBusinessHourCalculation.FlowInputs flowinput4 = new IT_InvocableBusinessHourCalculation.FlowInputs();    
        flowinput4.operation				= IT_InvocableBusinessHourCalculation.SUBSTRACTION;
        flowinput4.startDateTime			= System.today();
        flowinput4.unitOfMeasure			= 'hour';
        flowinput4.value					= 2;
        flowinputArray.add(flowinput4);

        IT_InvocableBusinessHourCalculation.FlowInputs flowinput5 = new IT_InvocableBusinessHourCalculation.FlowInputs();    
        flowinput5.operation				= IT_InvocableBusinessHourCalculation.SUBSTRACTION;
        flowinput5.startDateTime			= System.today();
        flowinput5.unitOfMeasure			= 'week';
        flowinput5.value					= 2;
        flowinputArray.add(flowinput5);

        IT_InvocableBusinessHourCalculation.FlowInputs flowinput6 = new IT_InvocableBusinessHourCalculation.FlowInputs();    
        flowinput6.operation				= IT_InvocableBusinessHourCalculation.SUBSTRACTION;
        flowinput6.startDateTime			= System.today();
        flowinput6.unitOfMeasure			= 'month';
        flowinput6.value					= 2;
        flowinputArray.add(flowinput6);

        IT_InvocableBusinessHourCalculation.FlowInputs flowinput7 = new IT_InvocableBusinessHourCalculation.FlowInputs();    
        flowinput7.operation				= IT_InvocableBusinessHourCalculation.SUBSTRACTION;
        flowinput7.startDateTime			= System.today();
        flowinput7.unitOfMeasure			= 'year';
        flowinput7.value					= 2;
        flowinputArray.add(flowinput7);

        Test.startTest();
            List<IT_InvocableBusinessHourCalculation.FlowOutputs> output = IT_InvocableBusinessHourCalculation.calculate(flowinputArray);
            System.assertNotEquals(output, null);
        Test.stopTest();
        
    }	
    
}