@IsTest
global class CORE_Livestorm_CreateCMMethodTest {
    @TestSetup
    public static void makeData(){
        CORE_Livestorm_DataFaker_InitiateTest.CreateCampaignWithMember();
    }

    @isTest
    public static void fullSuccessExecuteTest() {
        Test.setMock(HttpCalloutMock.class, new FullSuccessCreateCampaignMock());
        SchedulableContext ctx;

        Test.startTest();
        CORE_Livestorm_CreateCMMethod.execute(ctx);
        Test.stopTest();

        List<AsyncApexJob> jobsApexBatch = [select Id, ApexClassID, ApexClass.Name, Status, JobType from AsyncApexJob where JobType = 'BatchApex'];
        System.assertEquals(2, jobsApexBatch.size(), 'expecting two apex batch job');
    }

    @isTest
    public static void FailOutDatedCampaignExecuteTest() {
        Test.setMock(HttpCalloutMock.class, new FullSuccessCreateCampaignMock());
        SchedulableContext ctx;

        Campaign campaign = [SELECT Id FROM  Campaign];
        campaign.CORE_Start_Date_and_Hour__c = date.today().addDays(-2);
        campaign.CORE_End_Date_and_Hour__c = date.today().addDays(-1);

        update campaign;

        Test.startTest();
        CORE_Livestorm_CreateCMMethod.execute(ctx);
        Test.stopTest();

        List<AsyncApexJob> jobsApexBatch = [select Id, ApexClassID, ApexClass.Name, Status, JobType from AsyncApexJob where JobType = 'BatchApex'];
        //System.assertEquals(2, jobsApexBatch.size(), 'expecting one apex batch job'); // 1 for campaign update
    }

    global class FullSuccessCreateCampaignMock implements HttpCalloutMock {
        global HttpResponse respond(HttpRequest req) {
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');

            if (req.getEndpoint().endsWith('events')){
                res.setBody('{"data": {"id": "2b7de38f-d2d9-4a73-beac-a6ed54d8a38b","attributes": {"registration_link": "https://app.livestorm.co/p/2b7de38f-d2d9-4a73-beac-a6ed54d8a38b"}}}');
                res.setStatusCode(201);
            }
            else if (req.getEndpoint().endsWith('sessions')){
                res.setBody('{"data": {"id": "6b3e4bde-2a75-4ed8-a4af-83a08a68a057","attributes": {"room_link": "https://app.livestorm.co/p/2b7de38f-d2d9-4a73-beac-a6ed54d8a38b/live?s=6b3e4bde-2a75-4ed8-a4af-83a08a68a057"}}}');
                res.setStatusCode(201);
            }
            else if (req.getEndpoint().endsWith('people')){
                res.setBody('{"data": {"id": "6b3e4bde-2a75-4ed8-a4af-83a08a68a057","attributes": {"registrant_detail": {"connection_link": "https://app.livestorm.co/p/2b7de38f-d2d9-4a73-beac-a6ed54d8a38b/live?s=6b3e4bde-2a75-4ed8-a4af-83a08a68a057"}}}}');
                res.setStatusCode(201);
            }

            return res;
        }
    }

    @isTest
    public static void fullFailExecuteTest() {
        Test.setMock(HttpCalloutMock.class, new FullFailCreateCampaignMock());
        SchedulableContext ctx;
        
        CampaignMember CampaignMember = [SELECT Id FROM  CampaignMember];
        delete CampaignMember;

        Test.startTest();
        CORE_Livestorm_CreateCMMethod.execute(ctx);
        Test.stopTest();

        List<AsyncApexJob> jobsApexBatch = [select Id, ApexClassID, ApexClass.Name, Status, JobType from AsyncApexJob where JobType = 'BatchApex'];
        System.assertEquals(1, jobsApexBatch.size(), 'expecting one apex batch job');
    }

    global class FullFailCreateCampaignMock implements HttpCalloutMock {
        global HttpResponse respond(HttpRequest req) {
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');

            if (req.getEndpoint().endsWith('events')){
                res.setBody('{"data": {"id": "2b7de38f-d2d9-4a73-beac-a6ed54d8a38b","attributes": {"registration_link": "https://app.livestorm.co/p/2b7de38f-d2d9-4a73-beac-a6ed54d8a38b"}}}');
                res.setStatusCode(201);
            }
            else if (req.getEndpoint().endsWith('sessions')){
                res.setBody('{"data": {"id": "6b3e4bde-2a75-4ed8-a4af-83a08a68a057","attributes": {"room_link": "https://app.livestorm.co/p/2b7de38f-d2d9-4a73-beac-a6ed54d8a38b/live?s=6b3e4bde-2a75-4ed8-a4af-83a08a68a057"}}}');
                res.setStatusCode(201);
            }
            else if (req.getEndpoint().endsWith('people')){
                res.setBody('{"data": {"id": "6b3e4bde-2a75-4ed8-a4af-83a08a68a057","attributes": {"registrant_detail": {"connection_link": "https://app.livestorm.co/p/2b7de38f-d2d9-4a73-beac-a6ed54d8a38b/live?s=6b3e4bde-2a75-4ed8-a4af-83a08a68a057"}}}}');
                res.setStatusCode(201);
            }

            return res;
        }
    }
}