@IsTest
public class CORE_UserTriggerTest {
    @IsTest
    public static void insert_user_should_not_call_api_when_provisioning_is_not_enable() {
        User u = CORE_DataFaker_User.getUser('testCORE_UserTriggerTest1');
        System.assertEquals(1, [Select Id from user where Lastname like '%testCORE_UserTriggerTest1%'].size());
    }

    @IsTest
    public static void update_user_should_not_call_api_when_provisioning_is_not_enable() {
        User u = CORE_DataFaker_User.getUser('testCORE_UserTriggerTest2');
        u.LastNAme = 'testCORE_UserTriggerTest3';
        Update u;
        System.assertEquals(1, [Select Id from user where Lastname = 'testCORE_UserTriggerTest3'].size());
    }
}