public abstract class CORE_ObjectWrapper {
    //method used to get the value of an object attribute
    public Object get(String paramName) {
        String jsonInstance = Json.serialize(this);
        Map<String, Object> untypedInstance;
        untypedInstance= (Map<String, Object>)JSON.deserializeUntyped(jsonInstance);
        return untypedInstance.get(paramName);
    }
}