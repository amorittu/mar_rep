@IsTest
public with sharing class CORE_PN_UniqueKeyTest {
    @TestSetup
    public static void makeData(){
        CORE_DataFaker_CatalogHierarchy catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test1');
        Id profileId = [SELECT Id FROM Profile WHERE Name = 'CORE_API_Profile' LIMIT 1].Id;

        User usr = new User(
            Username = 'usertest@aoltest.com',
            LastName = 'TEST_USER',
            Email = 'testemail@yopmail.com',
            Alias = 'T',
            CommunityNickname = 'T',
            TimeZoneSidKey = 'Europe/Paris',
            LocaleSidKey = 'fr_FR_EURO',
            EmailEncodingKey = 'ISO-8859-1',
            ProfileId = profileId,
            LanguageLocaleKey = 'fr'
        );
        database.insert(usr, true);
    }
    //commentaire
    @IsTest
    public static void insert_phone_number_should_fill_uniquekey() {
        List<CORE_Business_Unit__c> bus =  [SELECT Id FROM CORE_Business_Unit__c LIMIT 1];
        User usr = [SELECT Id FROM User WHERE Username = 'usertest@aoltest.com' LIMIT 1];
        CORE_Phone_Number__c pn = new CORE_Phone_Number__c();
        pn.Name = 'Test';
        pn.CORE_PN_Business_Unit__c = bus.get(0).Id;
        pn.CORE_PN_User__c = usr.Id;

        Test.startTest();
        insert pn;
        Test.stopTest();

    }
}