public without sharing class CORE_ServiceAppointmentTriggerHandler {
    public static void handleTrigger(Map<Id, ServiceAppointment> oldRecordsMap, List<ServiceAppointment> newRecords, System.TriggerOperation triggerEvent) {
        switch on triggerEvent {
            when BEFORE_INSERT {
                System.debug('CORE_ServiceAppointmentTriggerHandler.beforeInsert : START');
                beforeInsert(newRecords);
                System.debug('CORE_ServiceAppointmentTriggerHandler.beforeInsert : END');
            }
            /*when BEFORE_UPDATE {
                System.debug('CORE_ServiceAppointmentTriggerHandler.afterUpdate : START');
                beforeUpdate(oldRecords, newRecords);
                System.debug('CORE_ServiceAppointmentTriggerHandler.afterUpdate : END');
            }*/
            when AFTER_INSERT {
                afterInsert(newRecords);
            }
            when AFTER_UPDATE {
                afterUpdate(oldRecordsMap, newRecords);
            }
            when AFTER_DELETE {
                System.debug('CORE_ServiceAppointmentTriggerHandler.afterDelete : START');
                afterDelete(oldRecordsMap.values());
                System.debug('CORE_ServiceAppointmentTriggerHandler.afterDelete : END');
            }
        }
    }

    @TestVisible
    private static void serviceAppointmentProcess(List<ServiceAppointment> newRecords) {
        List<ServiceAppointment> saWithLeadSource = new List<ServiceAppointment>();

        Map<String, Opportunity> oppsWithExternalIds = getOpportunitiesWhithExternalIds(newRecords);
        System.debug('serviceAppointmentProcess : oppsWithExternalIds = '+ oppsWithExternalIds);
        Set<Id> oldParentIds = new Set<Id>();

        if(!oppsWithExternalIds.isEmpty()){
            for(ServiceAppointment sa : newRecords){
                String leadExternalId = sa.CORE_Lead_form_acquisition_unique_key__c;
                if(!String.isEmpty(leadExternalId) && oppsWithExternalIds.keySet().contains(leadExternalId)){
                    oldParentIds.add(sa.ParentRecordId);
                    sa.ParentRecordId = oppsWithExternalIds.get(leadExternalId).Id;
                    System.debug('Updating sa');
                }
            }
        }

        List<Lead> leadsToDelete = getLeadsToDelete(oldParentIds);

        if(!leadsToDelete.isEmpty()){
            delete leadsToDelete;
        } 
    }

    @TestVisible
    private static Map<String, Opportunity>  getOpportunitiesWhithExternalIds(List<ServiceAppointment> serviceAppointments){
        Map<String, Opportunity> oppsByLeadExternalIds = new Map<String, Opportunity>();
        Set<String> leadExternalIds = new Set<String>();
        for(ServiceAppointment serviceAppointment : serviceAppointments){
            if(!String.isEmpty(serviceAppointment.CORE_Lead_form_acquisition_unique_key__c)){
                leadExternalIds.add(serviceAppointment.CORE_Lead_form_acquisition_unique_key__c);
            }
        }

        if(leadExternalIds.isEmpty()){
            return oppsByLeadExternalIds;
        }
        if(!leadExternalIds.isEmpty()){
            List<Opportunity> opportunities = [SELECT Id, AccountId, CORE_Lead_Source_External_Id__c 
            FROM Opportunity 
            WHERE CORE_Lead_Source_External_Id__c IN :leadExternalIds ];

            for(Opportunity opportunity : opportunities){
                oppsByLeadExternalIds.put(opportunity.CORE_Lead_Source_External_Id__c, opportunity);
            }
        }
        

        return oppsByLeadExternalIds;
    }

    @TestVisible
    private static void beforeInsert(List<ServiceAppointment> newRecords) {
        CORE_CountrySpecificSettings__c countrySetting = CORE_CountrySpecificSettings__c.getOrgDefaults();

        serviceAppointmentProcess(newRecords);
        Map<Id, Account> accountsBySa = getRelatedAccounts(newRecords);
        System.debug('accountsBySa = ' + accountsBySa);

        Map<Id, Opportunity> opportunitiesBySa = getRelatedOpportunities(newRecords);

        for(ServiceAppointment newServiceAppointment : newRecords){
            System.debug('newServiceAppointment = before insert changed = ' + newServiceAppointment);

            if(countrySetting.CORE_TwilioCreateVideoMeeting__c
                && newRecords.size() == 1
                && newServiceAppointment.AppointmentType == 'Video'){//always run when creating manually

                newServiceAppointment.Status = 'None';
            }

            newServiceAppointment = getUpdatedSaFromAccount(newServiceAppointment,accountsBySa);
            
            newServiceAppointment = getUpdatedSaFromOpportunity(newServiceAppointment,opportunitiesBySa, true);

            System.debug('newServiceAppointment = after insert changed = ' + newServiceAppointment);

        }
    }

    @TestVisible
    private static void afterInsert(List<ServiceAppointment> newRecords){
        CORE_CountrySpecificSettings__c countrySetting = CORE_CountrySpecificSettings__c.getOrgDefaults();

        Id serviceAppId = null;

        for(ServiceAppointment newServiceAppointment : newRecords){
            //Create and get video meeting from Twilio
            if(countrySetting.CORE_TwilioCreateVideoMeeting__c
               && newRecords.size() == 1
               && newServiceAppointment.AppointmentType == 'Video'){//always run when creating manually

                serviceAppId = newServiceAppointment.Id;
            }
        }

        if(serviceAppId != null){
            System.enqueueJob(new CORE_QU_ServiceAppointmentBooking(serviceAppId));
        }
    }

    @TestVisible
    private static void afterUpdate(Map<Id, ServiceAppointment> oldRecordsMap, List<ServiceAppointment> newRecords){
        CORE_CountrySpecificSettings__c countrySetting = CORE_CountrySpecificSettings__c.getOrgDefaults();

        ServiceAppointment oldServiceApp = null;
        Id serviceAppId = null;
        Set<String> videoMeetingFieldsSet = new Set<String> { 'SchedStartTime', 'SchedEndTime' };

        for(ServiceAppointment newServiceAppointment : newRecords){
            oldServiceApp = oldRecordsMap.get(newServiceAppointment.Id);

            //Create and get video meeting from Twilio
            if(countrySetting.CORE_TwilioCreateVideoMeeting__c
                && newRecords.size() == 1
                && newServiceAppointment.AppointmentType == 'Video'){//always run when creating manually

                //Check if at least 1 field impacting video meeting has changed
                for(String f : videoMeetingFieldsSet){
                    if(oldServiceApp.get(f) != newServiceAppointment.get(f)){
                        serviceAppId = newServiceAppointment.Id;
                        break;
                    }
                }
            }
        }

        if(serviceAppId != null){
            System.enqueueJob(new CORE_QU_ServiceAppointmentBooking(serviceAppId));
        }
    }

    /*@TestVisible
    private static void beforeUpdate(List<ServiceAppointment> oldRecords, List<ServiceAppointment> newRecords) {
        Map<Id, ServiceAppointment> oldServiceAppointments = new Map<Id, ServiceAppointment> (oldRecords);
        List<ServiceAppointment> newServiceAppointments = getAvailableServiceAppointments(oldServiceAppointments, newRecords);
        Map<Id, Account> accountsBySa = getRelatedAccounts(newServiceAppointments);

        Map<Id, Opportunity> opportunitiesBySa = getRelatedOpportunities(newServiceAppointments);

        for(ServiceAppointment newServiceAppointment : newServiceAppointments){
            Id serviceAppointmentId = newServiceAppointment.Id;
            ServiceAppointment oldServiceAppointment = oldServiceAppointments.get(serviceAppointmentId);

            if(isRelatedAccountChanged(oldServiceAppointment, newServiceAppointment)){
                newServiceAppointment = getUpdatedSaFromAccount(newServiceAppointment,accountsBySa);
            }

            if(isRelatedParentChanged(oldServiceAppointment, newServiceAppointment)){
                newServiceAppointment = getUpdatedSaFromOpportunity(newServiceAppointment,opportunitiesBySa,false);
            }
        }
    }*/

    @TestVisible
    private static void afterDelete(List<ServiceAppointment> oldRecords) {
        System.debug('afterDelete : Start');
        Set<Id> parentIds = new Set<Id>();
        for(ServiceAppointment serviceAppointment : oldRecords){
            parentIds.add(serviceAppointment.ParentRecordId);
        }

        List<Lead> leadsToDelete = getLeadsToDelete(parentIds);

        if(!leadsToDelete.isEmpty()){
            delete leadsToDelete;
        } 
        System.debug('afterDelete : Start');
    }

    private static List<Lead> getLeadsToDelete(Set<Id> parentIds){
        System.debug('getLeadsToDelete : Start');
        Set<Id> relatedLeads = new Set<Id>();
        List<String> leadToDeleteStatus = new List<String> {'Unqualified','New'};

        for(Id parentId : parentIds){
            if(CORE_SobjectUtils.isLeadId(parentId)){
                relatedLeads.add(parentId);
            }
        }

        List<Lead> leadsToDelete = new List<Lead>();
        if(!relatedLeads.isEmpty()){
            leadsToDelete = [SELECT Id, Status 
            FROM Lead 
            WHERE Id IN :relatedLeads AND Status IN :leadToDeleteStatus];
        }
        System.debug(' getLeadsToDelete leadsToDelete.size() = ' + leadsToDelete);

        return leadsToDelete;
    }

    private static ServiceAppointment getUpdatedSaFromOpportunity(ServiceAppointment newServiceAppointment,  Map<Id, Opportunity> opportunitiesBySa, Boolean isInsert){
        Id serviceAppointmentId = newServiceAppointment.Id;

        if(opportunitiesBySa.keySet().contains(serviceAppointmentId)){
            Opportunity relatedOpportunity = opportunitiesBySa.get(serviceAppointmentId);
            newServiceAppointment.CORE_Division__c = relatedOpportunity.CORE_OPP_Division__c; 
            newServiceAppointment.CORE_Business_Unit__c = relatedOpportunity.CORE_OPP_Business_Unit__c;
            newServiceAppointment.CORE_Study_Area__c = relatedOpportunity.CORE_OPP_Study_Area__c;
            newServiceAppointment.CORE_Curriculum__c = relatedOpportunity.CORE_OPP_Curriculum__c;
            newServiceAppointment.CORE_Level__c = relatedOpportunity.CORE_OPP_Level__c;
            newServiceAppointment.CORE_Intake__c = relatedOpportunity.CORE_OPP_Intake__c;
            
            /*if(!isInsert){
                newServiceAppointment.CORE_Lead_form_acquisition_unique_key__c = relatedOpportunity.CORE_Lead_Source_External_Id__c;
            }*/

            Account relatedAccount = relatedOpportunity.Account;
            newServiceAppointment.CORE_Firstname__c = relatedAccount.FirstName;
            newServiceAppointment.CORE_Lastname__c = relatedAccount.LastName;
            newServiceAppointment.CORE_Salutation__c = relatedAccount.Salutation;
            newServiceAppointment.Phone = getAccountPhone(relatedAccount);
            newServiceAppointment.Email = getAccountEmail(relatedAccount);

            if (newServiceAppointment.Subject == null){
                newServiceAppointment.Subject = relatedAccount.FirstName + ' - ' + relatedAccount.LastName;
            }
        } else {
            newServiceAppointment.CORE_Division__c = null;
            newServiceAppointment.CORE_Business_Unit__c = null;
            newServiceAppointment.CORE_Study_Area__c = null;
            newServiceAppointment.CORE_Curriculum__c = null;
            newServiceAppointment.CORE_Level__c = null;
            newServiceAppointment.CORE_Intake__c = null;
        }
        
        return newServiceAppointment;
    }

    private static ServiceAppointment getUpdatedSaFromAccount(ServiceAppointment newServiceAppointment,Map<Id, Account> accountsBySa){
        Id serviceAppointmentId = newServiceAppointment.Id;
        System.debug('accountsBySa.keySet().contains(serviceAppointmentId) = '+ accountsBySa.keySet().contains(serviceAppointmentId));
        if(accountsBySa.keySet().contains(serviceAppointmentId)){
            Account relatedAccount = accountsBySa.get(serviceAppointmentId);
            newServiceAppointment.CORE_Firstname__c = relatedAccount.FirstName;
            newServiceAppointment.CORE_Lastname__c = relatedAccount.LastName;
            newServiceAppointment.CORE_Salutation__c = relatedAccount.Salutation;
            newServiceAppointment.Phone = getAccountPhone(relatedAccount);
            newServiceAppointment.Email = getAccountEmail(relatedAccount);
            newServiceAppointment.subject = relatedAccount.LastName + ' - ' + relatedAccount.FirstName;
        } else {
            newServiceAppointment.CORE_Firstname__c = null;
            newServiceAppointment.CORE_Lastname__c = null;
            newServiceAppointment.CORE_Salutation__c = null;
            newServiceAppointment.Phone = null;
            newServiceAppointment.Email = null;
        }
        
        return newServiceAppointment;
    }

    private static Map<Id, Account> getRelatedAccounts(List<ServiceAppointment> serviceAppointments){
        Set<Id> accountIds = new Set<Id>();
        Map<Id, Account> accountsBySa = new Map<Id, Account>();
        for(ServiceAppointment serviceAppointment : serviceAppointments){
            Boolean isRelatedAccount = CORE_SobjectUtils.isAccountId(serviceAppointment.ParentRecordId);

            if(isRelatedAccount){
                accountIds.add(serviceAppointment.ParentRecordId);
            } else if(serviceAppointment.AccountId != null){
                accountIds.add(serviceAppointment.AccountId);
            }
        }

        if(accountIds.isEmpty()){
            return accountsBySa;
        }

        if(accountIds.isEmpty()){
            return accountsBySa;
        }
        
        List<Account> accounts = [SELECT Id, Name, IsPersonAccount, PersonEmail, CORE_Email_2__pc, CORE_Email_3__pc, CORE_Student_Email__pc,
            FirstName, LastName, Salutation, Phone, CORE_Phone2__pc, PersonMobilePhone, CORE_Mobile_2__pc, CORE_Mobile_3__pc
            FROM Account  
            WHERE Id IN :accountIds];
        
        Map<Id, Account> accountsByIds = new Map<Id, Account>(accounts);

        for(ServiceAppointment serviceAppointment : serviceAppointments){
            Boolean isRelatedAccount = CORE_SobjectUtils.isAccountId(serviceAppointment.ParentRecordId);
            Id accountId;
            if(isRelatedAccount){
                accountId = serviceAppointment.ParentRecordId;
            } else if(serviceAppointment.AccountId != null){
                accountId = serviceAppointment.AccountId;
            }

            if(accountIds.contains(accountId)){
                Account currentAccount = accountsByIds.get(accountId);
                accountsBySa.put(serviceAppointment.Id, currentAccount);
            }
        }

        return accountsBySa;
    }

    private static Map<Id, Opportunity> getRelatedOpportunities(List<ServiceAppointment> serviceAppointments){
        Set<Id> opportunityIds = new Set<Id>();
        Map<Id, Opportunity> opportunitiesByIds = new Map<Id, Opportunity>();

        for(ServiceAppointment serviceAppointment : serviceAppointments){
            Boolean isRelatedOpportunity = CORE_SobjectUtils.isOpportunityId(serviceAppointment.ParentRecordId);

            if(isRelatedOpportunity){
                opportunityIds.add(serviceAppointment.ParentRecordId);
            }
        }
        
        if(opportunityIds.isEmpty()){
            return opportunitiesByIds;
        }
        if(opportunityIds.isEmpty()){
            return opportunitiesByIds;
        }
        List<Opportunity> opportunities = [SELECT Id, CORE_OPP_Division__c, CORE_OPP_Business_Unit__c, CORE_OPP_Study_Area__c, 
            CORE_OPP_Curriculum__c, CORE_OPP_Level__c, CORE_OPP_Intake__c, CORE_Lead_Source_External_Id__c,
            Account.IsPersonAccount, Account.PersonEmail, Account.CORE_Email_2__pc, Account.CORE_Email_3__pc, Account.CORE_Student_Email__pc,
            Account.FirstName, Account.LastName, Account.Salutation, Account.Phone, Account.CORE_Phone2__pc, Account.PersonMobilePhone, 
            Account.CORE_Mobile_2__pc, Account.CORE_Mobile_3__pc
            FROM Opportunity  
            WHERE Id IN :opportunityIds];
        
        opportunitiesByIds = new Map<Id, Opportunity>(opportunities);

        for(ServiceAppointment serviceAppointment : serviceAppointments){
            if(opportunityIds.contains(serviceAppointment.ParentRecordId)){
                Opportunity currentOpportunity = opportunitiesByIds.get(serviceAppointment.ParentRecordId);
                opportunitiesByIds.put(serviceAppointment.Id, currentOpportunity);
            }
        }

        return opportunitiesByIds;
    }

    @TestVisible
    private static String getAccountEmail(Account account){
        String email = '';

        if(!String.isEmpty(account.PersonEmail)){
            email = account.PersonEmail;
        } else if(!String.isEmpty(account.CORE_Email_2__pc)){
            email = account.CORE_Email_2__pc;
        } else if(!String.isEmpty(account.CORE_Email_3__pc)){
            email = account.CORE_Email_3__pc;
        } else if(!String.isEmpty(account.CORE_Student_Email__pc)){
            email = account.CORE_Student_Email__pc;
        }

        return email;
    }

    @TestVisible
    private static String getAccountPhone(Account account){
        String phone = '';
        if(!String.isEmpty(account.Phone)){
            phone = account.Phone;
        } else if(!String.isEmpty(account.CORE_Phone2__pc)){
            phone = account.CORE_Phone2__pc;
        } else if(!String.isEmpty(account.PersonMobilePhone)){
            phone = account.PersonMobilePhone;
        } else if(!String.isEmpty(account.CORE_Mobile_2__pc)){
            phone = account.CORE_Mobile_2__pc;
        } else if(!String.isEmpty(account.CORE_Mobile_3__pc)){
            phone = account.CORE_Mobile_3__pc;
        }

        return phone;
    }
}