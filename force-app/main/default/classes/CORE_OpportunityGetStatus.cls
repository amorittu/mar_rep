global class CORE_OpportunityGetStatus {
    @InvocableMethod
    global static List<List<String>> getNextStatus(List<String> startStatus){
        List<String> resultStatus = new List<String>();
        //List<String> allStatus = CORE_PicklistUtils.getPicklistValues('Opportunity', 'StageName');
        List<String> allStatus = new List<String>();
        //CORE_Timestamp_process_setting__mdt timestampSettings = CORE_Timestamp_process_setting__mdt.getInstance('Default_value');
        CORE_Timestamp_process_setting__mdt timestampSettings = [SELECT Id, CORE_Opportunity_status_order__c FROM CORE_Timestamp_process_setting__mdt WHERE DeveloperName = 'Default_value' LIMIT 1];

        Map<String,Object> statusByRecordTypes = (Map<String,Object>) JSON.deserializeUntyped(timestampSettings.CORE_Opportunity_status_order__c);
        String recordTypeApiName = 'CORE_Enrollment';

        Id recordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get(recordTypeApiName).getRecordTypeId();
        for(Object statusValue : (List<Object>) statusByRecordTypes.get(recordTypeApiName)){
            allStatus.add((String) statusValue);
        }
        Boolean startPoint = false;

        System.debug('Start status = ' + startStatus.get(0));

        for(String currentStatus :allStatus){
            if (currentStatus == startStatus.get(0)){
                startPoint = true;
            }

            if(startPoint){
                resultStatus.add(currentStatus);
            }
        }

        System.debug('Number status returned = ' + startStatus.size());

        return new List<List<String>> {resultStatus};
    }
}