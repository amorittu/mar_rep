@IsTest
public class CORE_LeadFormAcquistionResourceTest {
    @IsTest
    public static void postLeadForm_Should_return_error_500_with_invalid_catalog() {
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();             
        req.requestURI = '/services/apexrest/LeadFormAcquisition/';
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response= res;

        CORE_LeadFormAcquisitionWrapper.PersonAccountWrapper personAccount = CORE_DataFaker_LeadFormAcquWrapper.getPersonAccountWrapper();
        List<CORE_LeadFormAcquisitionWrapper.ConsentWrapper> consents = new List<CORE_LeadFormAcquisitionWrapper.ConsentWrapper> {CORE_DataFaker_LeadFormAcquWrapper.getConsentWrapper()};
        CORE_LeadFormAcquisitionWrapper.OpportunityWrapper mainInterest = CORE_DataFaker_LeadFormAcquWrapper.getOpportunityWrapper();
        string prospectiveStudentComment = CORE_DataFaker_LeadFormAcquWrapper.getProspectiveStudentComment();
        CORE_LeadFormAcquisitionWrapper.OpportunityWrapper secondInterest = CORE_DataFaker_LeadFormAcquWrapper.getOpportunityWrapper();
        CORE_LeadFormAcquisitionWrapper.OpportunityWrapper thirdInterest = CORE_DataFaker_LeadFormAcquWrapper.getOpportunityWrapper();
        CORE_LeadFormAcquisitionWrapper.PointOfContactWrapper pointOfContact = CORE_DataFaker_LeadFormAcquWrapper.getPointOfContactWrapper();
        CORE_LeadFormAcquisitionWrapper.CampaignSubscriptionWrapper campaignSubscription = CORE_DataFaker_LeadFormAcquWrapper.getCampaignSubscriptionWrapper();
        CORE_LeadFormAcquisitionWrapper.AcademicDiplomaHistoryWrapper academicDiplomaHistory = CORE_DataFaker_LeadFormAcquWrapper.getAcademicDiplomaHistoryWrapper();
        Test.startTest();
        CORE_LeadFormAcquistionResource.postLeadForm(personAccount, consents, mainInterest, prospectiveStudentComment, secondInterest, thirdInterest, pointOfContact, campaignSubscription,academicDiplomaHistory);
        Test.stopTest();

        system.assertEquals(500, RestContext.response.statusCode, 'error 500 should be returned because catalog hierarchy does not exist');
        String result = EncodingUtil.base64Decode(EncodingUtil.base64Encode(RestContext.response.responseBody)).toString();
        
        CORE_LeadFormAcquisitionWrapper.ErrorWrapper errorWrapper = (CORE_LeadFormAcquisitionWrapper.ErrorWrapper) JSON.deserialize(result, CORE_LeadFormAcquisitionWrapper.ErrorWrapper.class);
        System.assertEquals(CORE_LeadFormAcquistionInputValidator.UNEXPECTED_CODE, errorWrapper.errorCode);
        System.assert(errorWrapper.message.startsWith(CORE_LeadFormAcquistionInputValidator.UNEXPECTED_MSG));
        System.assertNotEquals(null, errorWrapper.apiCallDate);

    }

    @IsTest
    public static void postLeadForm_Should_return_error_400_when_validation_of_mandatory_and_picklist_failed() {
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();             
        req.requestURI = '/services/apexrest/LeadFormAcquisition/';
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response= res;

        CORE_DataFaker_CatalogHierarchy catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test');

        CORE_LeadFormAcquisitionWrapper.PersonAccountWrapper personAccount = CORE_DataFaker_LeadFormAcquWrapper.getPersonAccountWrapper();
        string prospectiveStudentComment = CORE_DataFaker_LeadFormAcquWrapper.getProspectiveStudentComment();
        CORE_LeadFormAcquisitionWrapper.PointOfContactWrapper pointOfContact = CORE_DataFaker_LeadFormAcquWrapper.getPointOfContactWrapper();
        CORE_LeadFormAcquisitionWrapper.CampaignSubscriptionWrapper campaignSubscription = null;
        CORE_LeadFormAcquisitionWrapper.OpportunityWrapper secondInterest = null;
        CORE_LeadFormAcquisitionWrapper.OpportunityWrapper thirdInterest = null;
        CORE_LeadFormAcquisitionWrapper.AcademicDiplomaHistoryWrapper academicDiplomaHistory = null;

        CORE_LeadFormAcquisitionWrapper.ConsentWrapper consent = CORE_DataFaker_LeadFormAcquWrapper.getConsentWrapper();
        consent.division = catalogHierarchy.division.CORE_Division_ExternalId__c;
        consent.businessUnit = catalogHierarchy.businessUnit.CORE_Business_Unit_ExternalId__c;
        consent.consentType = 'wrongPicklist';

        List<CORE_LeadFormAcquisitionWrapper.ConsentWrapper> consents = new List<CORE_LeadFormAcquisitionWrapper.ConsentWrapper> {consent};

        CORE_LeadFormAcquisitionWrapper.OpportunityWrapper mainInterest = null;

        Test.startTest();
        CORE_LeadFormAcquistionResource.postLeadForm(personAccount, consents, mainInterest, prospectiveStudentComment, secondInterest, thirdInterest, pointOfContact, campaignSubscription, academicDiplomaHistory);
        Test.stopTest();

        system.assertEquals(400, RestContext.response.statusCode, 'Status code should be 400 when parameters validation failed');
        String result = EncodingUtil.base64Decode(EncodingUtil.base64Encode(RestContext.response.responseBody)).toString();
        
        System.debug('result = '+ result);
        
        List<CORE_LeadFormAcquisitionWrapper.ErrorWrapper> errorsWrapper = (List<CORE_LeadFormAcquisitionWrapper.ErrorWrapper>) JSON.deserialize(result, List<CORE_LeadFormAcquisitionWrapper.ErrorWrapper>.class);
        System.assertEquals(2, errorsWrapper.size());
        List<String> errorCodes = new List<String> {CORE_LeadFormAcquistionInputValidator.MANDATORY_FIELDS_CODE, CORE_LeadFormAcquistionInputValidator.INVALID_PICKLIST_CODE};
        List<String> errorMsgs = new List<String> {CORE_LeadFormAcquistionInputValidator.MANDATORY_FIELDS_MSG, CORE_LeadFormAcquistionInputValidator.INVALID_PICKLIST_MSG};

        for(CORE_LeadFormAcquisitionWrapper.ErrorWrapper error : errorsWrapper){
            System.assert(errorCodes.contains(error.errorCode));
            System.assert(errorMsgs.contains(error.message));
        }
        //System.assertEquals(CORE_LeadFormAcquistionInputValidator.UNEXPECTED_MSG, errorWrapper.message);
        //System.assertNotEquals(null, errorWrapper.apiCallDate);
    }

    @IsTest
    public static void postLeadForm_Should_return_200_status_code_when_provided_values_are_ok() {
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();             
        req.requestURI = '/services/apexrest/LeadFormAcquisition/';
        req.httpMethod = 'POST';
        RestContext.request = req;
        RestContext.response= res;

        CORE_DataFaker_CatalogHierarchy catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test');

        CORE_LeadFormAcquisitionWrapper.PersonAccountWrapper personAccount = CORE_DataFaker_LeadFormAcquWrapper.getPersonAccountWrapper();
        string prospectiveStudentComment = CORE_DataFaker_LeadFormAcquWrapper.getProspectiveStudentComment();
        CORE_LeadFormAcquisitionWrapper.PointOfContactWrapper pointOfContact = CORE_DataFaker_LeadFormAcquWrapper.getPointOfContactWrapper();
        CORE_LeadFormAcquisitionWrapper.CampaignSubscriptionWrapper campaignSubscription = null;
        CORE_LeadFormAcquisitionWrapper.OpportunityWrapper secondInterest = null;
        CORE_LeadFormAcquisitionWrapper.OpportunityWrapper thirdInterest = null;
        CORE_LeadFormAcquisitionWrapper.AcademicDiplomaHistoryWrapper academicDiplomaHistory = null;

        CORE_LeadFormAcquisitionWrapper.ConsentWrapper consent = CORE_DataFaker_LeadFormAcquWrapper.getConsentWrapper();
        consent.division = catalogHierarchy.division.CORE_Division_ExternalId__c;
        consent.businessUnit = catalogHierarchy.businessUnit.CORE_Business_Unit_ExternalId__c;

        List<CORE_LeadFormAcquisitionWrapper.ConsentWrapper> consents = new List<CORE_LeadFormAcquisitionWrapper.ConsentWrapper> {consent};

        CORE_LeadFormAcquisitionWrapper.OpportunityWrapper mainInterest = CORE_DataFaker_LeadFormAcquWrapper.getOpportunityWrapper();
        mainInterest.division = catalogHierarchy.division.CORE_Division_ExternalId__c;
        mainInterest.businessUnit = catalogHierarchy.businessUnit.CORE_Business_Unit_ExternalId__c;
        mainInterest.studyArea = catalogHierarchy.studyArea.CORE_Study_Area_ExternalId__c;
        mainInterest.curriculum = catalogHierarchy.curriculum.CORE_Curriculum_ExternalId__c;
        mainInterest.level = catalogHierarchy.level.CORE_Level_ExternalId__c;
        mainInterest.intake = catalogHierarchy.intake.CORE_Intake_ExternalId__c;

        Test.startTest();
        CORE_LeadFormAcquistionResource.postLeadForm(personAccount, consents, mainInterest, prospectiveStudentComment, secondInterest, thirdInterest, pointOfContact, campaignSubscription, academicDiplomaHistory);
        Test.stopTest();

        system.assertEquals(200, RestContext.response.statusCode, 'Status code should be 200 when all values are ok');
        String result = EncodingUtil.base64Decode(EncodingUtil.base64Encode(RestContext.response.responseBody)).toString();
        
        CORE_LeadFormAcquisitionWrapper.ResultWrapper resultWrapper = (CORE_LeadFormAcquisitionWrapper.ResultWrapper) JSON.deserialize(result, CORE_LeadFormAcquisitionWrapper.ResultWrapper.class);

        System.assertNotEquals(null, resultWrapper);
        System.assertNotEquals(null, resultWrapper.account);
        System.assertNotEquals(null, resultWrapper.account.id);
        System.assertNotEquals(null, resultWrapper.opportunities);
        System.assertNotEquals(null, resultWrapper.opportunities.mainInterest);
        System.assertNotEquals(null, resultWrapper.pointOfContacts);
        System.assertEquals(1, resultWrapper.pointOfContacts.size());
        System.assertNotEquals(null, resultWrapper.consents);
        System.assertEquals(1, resultWrapper.consents.size());
    }
}