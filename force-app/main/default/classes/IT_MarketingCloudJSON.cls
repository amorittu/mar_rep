/*************************************************************************************************************
 * @name			IT_MarketingCloudJSON
 * @author			Andrea Morittu <amorittu@atlantic-technologies.com>
 * @created			22 / 04 / 2022
 * @description		class to create the final body to send to Marketing Cloud
 *
 * Changes (version)
 * -----------------------------------------------------------------------------------------------------------
 * 				No.		Date			Author					Description
 * 				----	------------	--------------------	----------------------------------------------
 * @version		1.0		2022-04-22		Andrea Morittu			Creation
 *              1.1     2022-04-26      Andrea Morittu          Mapping New fields needed to MC
 *              1.2     2022-04-27      Andrea Morittu          Mapping New fields needed to MC (x2)
 *              1.3     2022-04-29      Saverio Trovato         Mapping additional fields
 *              1.4     2022-05-02      Andrea Morittu          Mapping new curriculum field (taken by opportunity)
 *              1.5     2022-05-27      Andrea Morittu          Mapping additionalField (ProductName)
 *              1.6     2022-07/22      Andrea Morittu          Mapping additional Field (SenderProfile)
 *
**************************************************************************************************************/
public class IT_MarketingCloudJSON {
    public static final String CLASS_NAME = IT_MarketingCloudJSON.class.getName();

    /*********************************************************************************************************
     * @name			createJSONTemplateEmail
     * @author			Andrea Morittu <amorittu@atlantic-technologies.com>
     * @created			22 / 04 / 2022
     * @description		Description of your code
     * @param			IT_EmailPlatformEvent__e : single record of IT_EmailPlatformEvent__e invoked by the triggerHandler (IT_EmailPlatformEventTriggerHandler)
     * @return			It will return a JSONGenerator (final json to send to HTTP post)
    **********************************************************************************************************/
    public static JSONGenerator createJSONTemplateEmail(IT_EmailPlatformEvent__e PE ) {
        System.debug(CLASS_NAME + '- createJSONTemplateEmail - PE :  ' + JSON.serialize(PE) );

        JSONGenerator JSONGen = JSON.createGenerator(true);
        JSONGen.writeStartObject();

        jsonGen.writeObjectField('ContactKey', PE.IT_SubscriberKey__c);
        jsonGen.writeObjectField('EventDefinitionKey', PE.IT_EventDefinitionKey__c);
        jsonGen.writeBooleanField('EstablishContactKey', true);
        jsonGen.writeFieldName('Data');
        jsonGen.writeStartObject();
        jsonGen.writeObjectField('RequestId', IT_MarketingCloudHandler.generateRandomUid(32));
        jsonGen.writeObjectField('SubscriberKey', PE.IT_SubscriberKey__c);
        jsonGen.writeObjectField('Email', PE.IT_StudentEmail__c);

        if (String.isNotBlank(PE.IT_TaskId__c) ) {
            jsonGen.writeObjectField('taskid', PE.IT_TaskId__c);
        }
        if (String.isNotBlank(PE.IT_CountryRelevance__c) ) {
            jsonGen.writeObjectField('CountryRelevance', PE.IT_CountryRelevance__c);
        }
        if (String.isNotBlank(PE.IT_AccountLanguageOfRelationship__c) ) {
            jsonGen.writeObjectField('LanguageOfRelationship', PE.IT_AccountLanguageOfRelationship__c);
        }
        if (String.isNotBlank(PE.IT_AccountLanguageWebsite__c) ) {
            jsonGen.writeObjectField('LanguageWebsite', PE.IT_AccountLanguageWebsite__c);
        }
        if (String.isNotBlank(PE.IT_AccountLanguagePC__c) ) {
            jsonGen.writeObjectField('Language', PE.IT_AccountLanguagePC__c);
        }
        if (String.isNotBlank(PE.IT_BusinessUnitExternalId__c) ) {
            jsonGen.writeObjectField('BusinessUnit', PE.IT_BusinessUnitExternalId__c);
        }

        // Mapping New fields needed to MC - START
        if (String.isNotBlank(PE.IT_StudentFirstName__c) ) {
            jsonGen.writeObjectField('FirstName', PE.IT_StudentFirstName__c);
        }
        if (String.isNotBlank(PE.IT_StudentLastName__c) ) {
            jsonGen.writeObjectField('LastName', PE.IT_StudentLastName__c);
        }
        // Mapping New fields needed to MC - END
        // Mapping New fields needed to MC (x2) - START
        if (String.isNotBlank(PE.IT_EmailTypology__c) ) {
            jsonGen.writeObjectField('EmailType', PE.IT_EmailTypology__c);
        }
        if (String.isNotBlank(PE.IT_OpportunityId__c) ) {
            jsonGen.writeObjectField('OpportunityId_', PE.IT_OpportunityId__c);
        }
        // Mapping New fields needed to MC (x2) - END
        //2022-04-29 Additional field mapping -START
        if (String.isNotBlank(PE.IT_TaskResult__c) ) {
            jsonGen.writeObjectField('TaskResult', PE.IT_TaskResult__c);
        }
        if (String.isNotBlank(PE.IT_OwnerTelephone__c) ) {
            jsonGen.writeObjectField('OwnerTelephone', PE.IT_OwnerTelephone__c);
        }
        if (String.isNotBlank(PE.IT_OwnerMobile__c) ) {
            jsonGen.writeObjectField('OwnerMobile', PE.IT_OwnerMobile__c);
        }
        if (String.isNotBlank(PE.IT_OwnerName__c) ) {
            jsonGen.writeObjectField('OwnerName', PE.IT_OwnerName__c);
        }
        if (String.isNotBlank(PE.IT_MacroArea__c) ) {
            jsonGen.writeObjectField('MacroArea', PE.IT_MacroArea__c);
        }
        if (String.isNotBlank(PE.IT_MailingCountry__c) ) {
            jsonGen.writeObjectField('MailingCountry', PE.IT_MailingCountry__c);
        }
        //2022-04-29 Additional field mapping -END
        //2022-05-02 Mapping new curriculum field (taken by opportunity) -START
        if (String.isNotBlank(PE.IT_Curriculum__c) ) {
            jsonGen.writeObjectField('MainCurriculum', PE.IT_Curriculum__c);
        }
        //2022-05-02 Mapping new curriculum field (taken by opportunity) -END
        //2022-05-10 Mapping new emailMessageId field - in case of thank you email -START
        if (String.isNotBlank(PE.IT_EmailMessageId__c) ) {
            jsonGen.writeObjectField('EmailMessageId_', PE.IT_EmailMessageId__c);
        }
        //2022-05-10 Mapping new emailMessageId field - in case of thank you emailEND

        // Additional Fields:
        if (String.isNotBlank(PE.IT_COAProcess__c) ) {
            jsonGen.writeObjectField('CoaProcess', PE.IT_COAProcess__c);
        } 

        //2022/05/10/
        if (String.isNotBlank(PE.IT_ApocId__c) ) {
            jsonGen.writeObjectField('corepointcontactid_', PE.IT_ApocId__c);
        } 

        // 2022/05/12 - Mapping additional fields -START
        if (String.isNotBlank(PE.IT_ContactMobilePhone__c) ) {
            jsonGen.writeObjectField('ContactMobilePhone', PE.IT_ContactMobilePhone__c);
        } 
        if (String.isNotBlank(PE.IT_ContactPhone__c) ) {
            jsonGen.writeObjectField('ContactPhone', PE.IT_ContactPhone__c);
        } 
        if (String.isNotBlank(PE.IT_MainProductInterest__c) ) {
            jsonGen.writeObjectField('MainProductInterest', PE.IT_MainProductInterest__c);
        } 
        if (String.isNotBlank(PE.IT_OwnerEmail__c) ) {
            jsonGen.writeObjectField('OwnerEmail', PE.IT_OwnerEmail__c);
        }
        // 2022/05/12 - Mapping additional fields -END
        // 2022/05/27 - Mapping additional fields -START
        if (String.isNotBlank(PE.IT_ProductName__c) ) {
            jsonGen.writeObjectField('ProductName', PE.IT_ProductName__c);
        }

        if (String.isNotBlank(PE.IT_OrientationEmail__c) ) {
            jsonGen.writeObjectField('OrientationEmail', PE.IT_OrientationEmail__c);
        }

        if (String.isNotBlank(PE.IT_PhoneNumberFormula__c) ) {
            jsonGen.writeObjectField('PhoneNumberFormula', PE.IT_PhoneNumberFormula__c);
        }

        // 2022/07/22 - Mapping additional fields -START
        if (String.isNotBlank(PE.IT_EmailSender__c) ) {
            jsonGen.writeObjectField('SenderProfile', PE.IT_EmailSender__c);
        }
        // 2022/05/27 - Mapping additional fields -END

        jsonGen.writeEndObject();
        jsonGen.writeEndObject();
        System.debug(CLASS_NAME + '- createJSONTemplateEmail - jsonGen :  ' + jsonGen );
        return jsonGen;
    }

}