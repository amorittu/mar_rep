global  class CORE_SISOpportunityApplicationWrapper {
    global class GlobalWrapper{
        global Map<String,Object> personAccount;
        global MainInterestWrapper mainInterest;
        global Map<String,Object> opportunity;
        global AcquisitionPointOfContactWrapper acquisitionPointOfContact;
        global List<Object> consents;
    }
	global class MainInterestWrapper extends CORE_ObjectWrapper {
		global String SIS_OpportunityId;
		global String productExternalID;
		global String academicYear;
		global String division;
		global String businessUnit;
		global String studyArea;
		global String curriculum;
		global String level;
		global String intake;
	}

	global class AcquisitionPointOfContactWrapper extends CORE_ObjectWrapper {
		global string insertionDate;
		global string sourceId;
		global string nature;
		global string captureChannel;
		global string enrollementChannel;
		global string insertionMode;
		global string origin;
		global string salesOrigin;
		global string campaignMedium;
		global string campaignSource;
		global string campaignTerm;
		global string campaignContent;
		global string campaignName;
		global string proactivePrompt;
		global string proactiveEngaged;
		global string chatWith;
		global string device;
		global string firstPage;
		global string ipAddress;
		global string latitute;
		global string longitude;
	}
    /* Error Wrapper */
    global class ErrorWrapper{
		global String status = 'KO';
		global String errorCode;
		global String message;

		global ErrorWrapper( String message, String errorCode){
			this.message = message;
			this.errorCode = errorCode;
		}
	}

    /* Result wrappers  */
	global class ResultWrapper {
		global string status = 'OK';
		global ResultAccountWrapper Account = new ResultAccountWrapper();
		global ResultOpportunityWrapper Opportunity = new ResultOpportunityWrapper();
	}
    global class ResultOpportunityWrapper {
		global String id_Salesforce_Opportunity;
		global String id_SIS_Opportunity;
	}
    global class ResultAccountWrapper {
		global String id_Salesforce_PersonAccount;
		global String id_SIS_PersonAccount;
	}

}