@IsTest
public  class CORE_AOL_OpportunityExistTest {

    private static final String AOL_EXTERNALID = 'setupAol';
    private static final String MI_AOL_EXTERNALID = 'miAOLExternalId';
    @TestSetup
    static void makeData(){
        String uniqueKey = 'testMakeData';
        String mobile = '+33646454548';
        String phone = '+33246454645';
        String email = 'testMakeData@Test.com';
        CORE_CatalogHierarchyModel catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test').catalogHierarchy;
        Account account = CORE_DataFaker_Account.getStudentAccount('test');
        Opportunity  opportunity = CORE_DataFaker_Opportunity.getAOLOpportunity(catalogHierarchy.division.Id, 
            catalogHierarchy.businessUnit.Id, 
            account.Id, 
            CORE_AOL_Constants.PKL_OPP_STAGENAME_PROSPECT, 
            CORE_AOL_Constants.PKL_OPP_SUBSTATUS_PROSPECTAPPSTARTED, 
            AOL_EXTERNALID
        );
        CORE_LeadAcquisitionBuffer__c buffer = CORE_DataFaker_LeadAcquisitionBuffer.getBufferWhithoutInsert(uniqueKey + '1', mobile, phone, email, catalogHierarchy);
        buffer.CORE_MI_AOL_ExternalId__c = MI_AOL_EXTERNALID;
        insert buffer;
    }
    @IsTest
    public static void opportunityExist_Should_Find_Opportunity_and_return_it() {

        CORE_AOL_Wrapper.ResultWrapperOpportuntiyExist result = CORE_AOL_OpportunityExist.opportunityExist(AOL_EXTERNALID);

        Opportunity opportunity = [SELECT Id,CORE_OPP_Application_Online_ID__c,CORE_OPP_SIS_Opportunity_Id__c,CORE_OPP_SIS_Student_Academic_Record_ID__c
        FROM Opportunity
        WHERE CORE_OPP_Application_Online_ID__c=:AOL_EXTERNALID];

        System.assertEquals(result.status, 'Yes');
        System.assertEquals(result.opportunity.Id, opportunity.Id);
        System.assertEquals(result.opportunity.CORE_OPP_Application_Online_ID__c, opportunity.CORE_OPP_Application_Online_ID__c);
        System.assertEquals(result.opportunity.CORE_OPP_SIS_Opportunity_Id__c, opportunity.CORE_OPP_SIS_Opportunity_Id__c);
        System.assertEquals(result.opportunity.CORE_OPP_SIS_Student_Academic_Record_ID__c, opportunity.CORE_OPP_SIS_Student_Academic_Record_ID__c);
    }

    @IsTest
    public static void opportunityExist_Should_Find_Opportunity_In_LeadBufferAcquisition_and_return_it() {
        Opportunity opportunity = [SELECT Id FROM Opportunity WHERE CORE_OPP_Application_Online_ID__c =:MI_AOL_EXTERNALID];
        delete opportunity;
        CORE_AOL_Wrapper.ResultWrapperOpportuntiyExist result = CORE_AOL_OpportunityExist.opportunityExist(MI_AOL_EXTERNALID);
        System.assertEquals(result.status, 'Pending');
        System.assertEquals(result.opportunity, null);
    }

    @IsTest
    public static void opportunityExist_Should_Not_Find_Any_Opportunity() {
        CORE_AOL_Wrapper.ResultWrapperOpportuntiyExist result = CORE_AOL_OpportunityExist.opportunityExist('thisOppDoesNotExist');
        System.assertEquals(result.status, 'No');
        System.assertEquals(result.opportunity, null);
    }
}