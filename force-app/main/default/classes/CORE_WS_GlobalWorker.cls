public without sharing class CORE_WS_GlobalWorker {

    public static Boolean isParameterMissing(String parameter){
        if(String.IsBlank(parameter)){
            return true;
        } else {
            return false;
        }
    }

    public static sObject assignValue(sObject obj, String field, Object val){
        system.debug('@assignValue > field: ' + field);
        system.debug('@assignValue > val: ' + val);

        try{
            obj.put(field, String.valueOf(val));
        }
        catch(Exception e0){
            system.debug('@assignValue e0: ' + e0.getMessage() + ' at ' + e0.getStackTraceString());
            try{
                obj.put(field, Boolean.valueOf(val));
            }
            catch(Exception e1){
                system.debug('@assignValue e1: ' + e1.getMessage() + ' at ' + e1.getStackTraceString());
                try{
                    obj.put(field, Datetime.valueOf(((String)val).replace('T',' ')));
                }
                catch(Exception e2){
                    system.debug('@assignValue e2: ' + e2.getMessage() + ' at ' + e2.getStackTraceString());

                    try{
                        obj.put(field, Date.valueOf((String)val));
                    }
                    catch(Exception e3){
                        system.debug('@assignValue e3: ' + e3.getMessage() + ' at ' + e3.getStackTraceString());

                        try{
                            obj.put(field, Double.valueOf(val));
                        }
                        catch(Exception e4){
                            system.debug('@assignValue e4: ' + e4.getMessage() + ' at ' + e4.getStackTraceString());

                            TypeException excep = new TypeException();
                            excep.setMessage('INVALID TYPE FOR FIELD "'+ field + '"');
                            throw excep;
                        }
                    }
                }
            }
        }

        return obj;
    }
}