public with sharing class CORE_CUBE_CreatePicklistBatch implements Schedulable{
    
    public void execute(SchedulableContext ctx) {

        String allPicklist = CORE_CUBE_CreateCsv.createAllPicklist();
        //allPicklist.split('\n');

        string[] allPicklistSplit = allPicklist.split('\n');
        List<CORE_CUBE_Picklist__c> upsertList = new  List<CORE_CUBE_Picklist__c>();
        for(String test : allPicklistSplit){
            CORE_CUBE_Picklist__c newRecord = new CORE_CUBE_Picklist__c(
                CORE_CUBE_Sent_to_CUBE__c = true
            );

            string[] createRecord = test.split(',');
            if(createRecord.size()==4){
                newRecord.CORE_CUBE_List__c = createRecord[0].remove('"');
                newRecord.CORE_CUBE_Language__c = createRecord[1].remove('"');
                newRecord.CORE_CUBE_Code__c = createRecord[2].remove('"');
                newRecord.CORE_CUBE_Label__c = createRecord[3].remove('"');
                string name = createRecord[0] +' - '+createRecord[3];
                string externalId = createRecord[0] + '_' + createRecord[2];
                newRecord.CORE_CUBE_ExternalId__c = externalId.remove('"');
                if(name.length()>79){
                     newRecord.Name = name.substring(0, 79).remove('"');
                }else{
                    newRecord.Name=name.remove('"');
                }
                upsertList.add(newRecord);
            }else if(createRecord.size()==5){
                newRecord.CORE_CUBE_List__c = createRecord[0].remove('"');
                newRecord.CORE_CUBE_Language__c = createRecord[1].remove('"');
                newRecord.CORE_CUBE_Code__c = createRecord[2].remove('"');
                string label = createRecord[3]+ ', '+createRecord[4];
                newRecord.CORE_CUBE_Label__c = label.remove('"');
                string name = createRecord[0] +' - '+createRecord[3] +', ' +createRecord[4];
                string externalId = createRecord[0] + '_' + createRecord[2];
                newRecord.CORE_CUBE_ExternalId__c = externalId.remove('"');
                if(name.length()>79){
                     newRecord.Name = name.substring(0, 79).remove('"');
                }else{
                    newRecord.Name=name.remove('"');
                }
                upsertList.add(newRecord);
            }
        }
        upsertList.remove(0);

        //Generate Other picklist values
        upsertList.addAll(CORE_CreatePicklistValues.generatePicklistValues());

        upsert upsertList CORE_CUBE_ExternalId__c;
    }
}