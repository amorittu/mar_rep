/**
* @author Fodil Boudjedien - Almavia
* @date 2022-03-24
* @group Flow
* @description class used by the flow CORE_Opportunity_Reregistration_ScreenFlow
*/
public without sharing class CORE_OpportunityReregistration {
    public class InputWrapper {

        @InvocableVariable
        public Opportunity enrolledOpportunity;
        
        @InvocableVariable       
        public String academicYear;
        
        @InvocableVariable
        public String businessUnitId;
    }
    public class OutputWrapper {

        @InvocableVariable
        public Boolean isSuccess=false;
        
        @InvocableVariable       
        public String errorMessage ='';
        
        @InvocableVariable
        public Opportunity reregisteredOpportunity;
    }


    @InvocableMethod(label='CORE Opportunity Re-registration')
    public static List<OutputWrapper> invoke(List<InputWrapper> inputWrappers ){
        List<Opportunity> opportunities = new List<Opportunity>();
        List<OutputWrapper> outputs = new List<OutputWrapper>();
        OutputWrapper output = new OutputWrapper();
        try{
            if(inputWrappers.size()>0){
                String oppId = inputWrappers[0].enrolledOpportunity.Id;
                String query = CORE_MassOpportunityReopeningHandler.getMassOpportunityReregistrationQuery(inputWrappers[0].enrolledOpportunity.CORE_OPP_Business_Unit__c,inputWrappers[0].enrolledOpportunity.CORE_OPP_Academic_Year__c ,'');
                String initialQuery = query.substringBeforeLast(' WHERE '); // get the SELECT (list of fields) from Opportunity where id = oppId
                initialQuery += ' WHERE id = :oppId LIMIT 1';
                Opportunity inputOpportunity = Database.query(initialQuery);
                opportunities.addAll(CORE_MassOpportunityReopeningHandler.handleOpportunitiesReregistration(inputWrappers[0].academicYear,new List<Opportunity>{inputOpportunity},inputWrappers[0].businessUnitId));
            
            }

            output.isSuccess=true;
            if(opportunities.size()>0){
                output.reregisteredOpportunity=opportunities[0];
            }
            outputs.add(output);
            return outputs;
        }
        catch(Exception e ){
            output.isSuccess=false;
            output.errorMessage=e.getMessage();
            outputs.add(output);
            return outputs;
        }

    }
}