@IsTest
public with sharing class CORE_LFA_OpportunityManagementTest {
    private static final CORE_LFA_OpportunityManagement.OpportunityType OPP_TYPE_1 = CORE_LFA_OpportunityManagement.OpportunityType.MAIN_INTEREST;
    private static final CORE_LFA_OpportunityManagement.OpportunityType OPP_TYPE_2 = CORE_LFA_OpportunityManagement.OpportunityType.SECOND_INTEREST;
    private static final CORE_LFA_OpportunityManagement.OpportunityType OPP_TYPE_3 = CORE_LFA_OpportunityManagement.OpportunityType.THIRD_INTEREST;
    @IsTest
    public static void isExistingInterestValues_Should_return_true_when_at_least_one_mandatory_parameter_is_filled() {
        CORE_CatalogHierarchyModel catalog = new CORE_DataFaker_CatalogHierarchy('test').catalogHierarchy;

        CORE_LeadAcquisitionBuffer__c bufferRecord1 = new CORE_LeadAcquisitionBuffer__c(CORE_MI_OPPTDivision__c = catalog.division.Id);
        CORE_LeadAcquisitionBuffer__c bufferRecord2 = new CORE_LeadAcquisitionBuffer__c(CORE_SI_OPPTBusiness_Unit__c = catalog.businessUnit.Id);
        CORE_LeadAcquisitionBuffer__c bufferRecord3 = new CORE_LeadAcquisitionBuffer__c(CORE_TI_OPPTStudy_Area__c = catalog.studyArea.Id);

        Test.startTest();
        Boolean result1 = CORE_LFA_OpportunityManagement.isExistingInterestValues(bufferRecord1,OPP_TYPE_1);
        Boolean result2 = CORE_LFA_OpportunityManagement.isExistingInterestValues(bufferRecord2,OPP_TYPE_2);
        Boolean result3 = CORE_LFA_OpportunityManagement.isExistingInterestValues(bufferRecord3,OPP_TYPE_3);

        System.assertEquals(true, result1);
        System.assertEquals(true, result2);
        System.assertEquals(true, result3);
        Test.stopTest();
    }
    @IsTest
    public static void isExistingInterestValues_Should_return_false_when_zero_mandatory_parameter_is_filled() {
        CORE_CatalogHierarchyModel catalog = new CORE_DataFaker_CatalogHierarchy('test').catalogHierarchy;

        CORE_LeadAcquisitionBuffer__c bufferRecord1 = new CORE_LeadAcquisitionBuffer__c(CORE_SI_OPPTBusiness_Unit__c = catalog.businessUnit.Id);
        CORE_LeadAcquisitionBuffer__c bufferRecord2 = new CORE_LeadAcquisitionBuffer__c(CORE_TI_OPPTStudy_Area__c = catalog.studyArea.Id);
        CORE_LeadAcquisitionBuffer__c bufferRecord3 = new CORE_LeadAcquisitionBuffer__c(CORE_MI_OPPTDivision__c = catalog.division.Id);

        Test.startTest();
        Boolean result1 = CORE_LFA_OpportunityManagement.isExistingInterestValues(bufferRecord1,OPP_TYPE_1);
        Boolean result2 = CORE_LFA_OpportunityManagement.isExistingInterestValues(bufferRecord2,OPP_TYPE_2);
        Boolean result3 = CORE_LFA_OpportunityManagement.isExistingInterestValues(bufferRecord3,OPP_TYPE_3);

        System.assertEquals(false, result1);
        System.assertEquals(false, result2);
        System.assertEquals(false, result3);
        Test.stopTest();
    }

    @IsTest
    public static void getOpportunityWrapperFromBufferRecord_should_return_opportunityWrapper_from_buffer_record() {
        CORE_CatalogHierarchyModel catalog = new CORE_DataFaker_CatalogHierarchy('test').catalogHierarchy;

        CORE_LeadAcquisitionBuffer__c bufferRecord1 = CORE_DataFaker_LeadAcquisitionBuffer.getBufferWithInterest('uniqueKey1', catalog, 1, null);
        CORE_LeadAcquisitionBuffer__c bufferRecord2 = CORE_DataFaker_LeadAcquisitionBuffer.getBufferWithInterest('uniqueKey2', catalog, 2, null);
        CORE_LeadAcquisitionBuffer__c bufferRecord3 = CORE_DataFaker_LeadAcquisitionBuffer.getBufferWithInterest('uniqueKey3', catalog, 3, null);

        Test.startTest();
        CORE_LeadFormAcquisitionWrapper.OpportunityWrapper result1 = CORE_LFA_OpportunityManagement.getOpportunityWrapperFromBufferRecord(bufferRecord1,OPP_TYPE_1);
        CORE_LeadFormAcquisitionWrapper.OpportunityWrapper result2 = CORE_LFA_OpportunityManagement.getOpportunityWrapperFromBufferRecord(bufferRecord2,OPP_TYPE_2);
        CORE_LeadFormAcquisitionWrapper.OpportunityWrapper result3 = CORE_LFA_OpportunityManagement.getOpportunityWrapperFromBufferRecord(bufferRecord3,OPP_TYPE_3);

        System.assertEquals(bufferRecord1.CORE_MI_OPPAcademicYear__c, result1.academicYear);
        System.assertEquals(bufferRecord1.CORE_MI_OPPTDivision__c, result1.division);
        System.assertEquals(bufferRecord1.CORE_MI_OPPTBusiness_Unit__c, result1.businessUnit);
        System.assertEquals(bufferRecord1.CORE_MI_OPPTStudy_Area__c, result1.studyArea);
        System.assertEquals(bufferRecord1.CORE_MI_OPPTCurriculum__c, result1.curriculum);
        System.assertEquals(bufferRecord1.CORE_MI_OPPTLevel__c, result1.level);
        System.assertEquals(bufferRecord1.CORE_MI_OPPTIntake__c, result1.intake);
        System.assertEquals(bufferRecord1.CORE_MI_ApplicationShippedByMail__c, result1.applicationFormShippedByMail);
        System.assertEquals(bufferRecord1.CORE_MI_BrochureShippedByMail__c, result1.brochureShippedByMail);
        System.assertEquals(bufferRecord1.CORE_MI_IsSuspect__c , result1.isSuspect);
        System.assertEquals(bufferRecord1.CORE_MI_LeadSourceExternalId__c, result1.leadSourceExternalId);
        System.assertEquals(bufferRecord1.CORE_MI_LearningMaterial__c, result1.learningMaterial );

        System.assertEquals(bufferRecord2.CORE_SI_OPPAcademicYear__c, result2.academicYear);
        System.assertEquals(bufferRecord2.CORE_SI_OPPTDivision__c, result2.division);
        System.assertEquals(bufferRecord2.CORE_SI_OPPTBusiness_Unit__c, result2.businessUnit);
        System.assertEquals(bufferRecord2.CORE_SI_OPPTStudy_Area__c, result2.studyArea);
        System.assertEquals(bufferRecord2.CORE_SI_OPPTCurriculum__c, result2.curriculum);
        System.assertEquals(bufferRecord2.CORE_SI_OPPTLevel__c, result2.level);
        System.assertEquals(bufferRecord2.CORE_SI_OPPTIntake__c, result2.intake);
        System.assertEquals(bufferRecord2.CORE_SI_ApplicationShippedByMail__c, result2.applicationFormShippedByMail);
        System.assertEquals(bufferRecord2.CORE_SI_BrochureShippedByMail__c, result2.brochureShippedByMail);
        System.assertEquals(bufferRecord2.CORE_SI_IsSuspect__c , result2.isSuspect);
        System.assertEquals(bufferRecord2.CORE_SI_LeadSourceExternalId__c, result2.leadSourceExternalId);
        System.assertEquals(bufferRecord2.CORE_SI_LearningMaterial__c, result2.learningMaterial );

        System.assertEquals(bufferRecord3.CORE_TI_OPPAcademicYear__c, result3.academicYear);
        System.assertEquals(bufferRecord3.CORE_TI_OPPTDivision__c, result3.division);
        System.assertEquals(bufferRecord3.CORE_TI_OPPTBusiness_Unit__c, result3.businessUnit);
        System.assertEquals(bufferRecord3.CORE_TI_OPPTStudy_Area__c, result3.studyArea);
        System.assertEquals(bufferRecord3.CORE_TI_OPPTCurriculum__c, result3.curriculum);
        System.assertEquals(bufferRecord3.CORE_TI_OPPTLevel__c, result3.level);
        System.assertEquals(bufferRecord3.CORE_TI_OPPTIntake__c, result3.intake);
        System.assertEquals(bufferRecord3.CORE_TI_ApplicationShippedByMail__c, result3.applicationFormShippedByMail);
        System.assertEquals(bufferRecord3.CORE_TI_BrochureShippedByMail__c, result3.brochureShippedByMail);
        System.assertEquals(bufferRecord3.CORE_TI_IsSuspect__c , result3.isSuspect);
        System.assertEquals(bufferRecord3.CORE_TI_LeadSourceExternalId__c, result3.leadSourceExternalId);
        System.assertEquals(bufferRecord3.CORE_TI_LearningMaterial__c, result3.learningMaterial );

        Test.stopTest();
    }
    @IsTest
    public static void getPocWrapperFromBufferRecord_should_return_pointOfContactWrapper_from_buffer_record() {
        CORE_LeadAcquisitionBuffer__c bufferRecord = CORE_DataFaker_LeadAcquisitionBuffer.getBufferWithPointOfContact(null);

        Test.startTest();
        CORE_LeadFormAcquisitionWrapper.PointOfContactWrapper result = CORE_LFA_OpportunityManagement.getPocWrapperFromBufferRecord(bufferRecord);

        System.AssertEquals(String.valueOf(bufferRecord.CORE_POC_CreatedDate__c.getTime()), result.creationDate);
        System.AssertEquals(bufferRecord.CORE_POC_SourceId__c, result.sourceId);
        System.AssertEquals(bufferRecord.CORE_POC_Nature__c, result.nature);
        System.AssertEquals(bufferRecord.CORE_POC_CaptureChannel__c, result.captureChannel);
        System.AssertEquals(bufferRecord.CORE_POC_Origin__c, result.origin);
        System.AssertEquals(bufferRecord.CORE_POC_SalesOrigin__c, result.salesOrigin);
        System.AssertEquals(bufferRecord.CORE_POC_CampaignMedium__c, result.campaignMedium);
        System.AssertEquals(bufferRecord.CORE_POC_CampaignSource__c, result.campaignSource);
        System.AssertEquals(bufferRecord.CORE_POC_CampaignTerm__c, result.campaignTerm);
        System.AssertEquals(bufferRecord.CORE_POC_CampaignContent__c, result.campaignContent);
        System.AssertEquals(bufferRecord.CORE_POC_CampaignName__c, result.campaignName);
        System.AssertEquals(bufferRecord.CORE_POC_Device__c, result.device);
        System.AssertEquals(bufferRecord.CORE_POC_FirstVisit__c, result.firstVisitDate);
        System.AssertEquals(bufferRecord.CORE_POC_FirstPage__c, result.firstPage);
        System.AssertEquals(bufferRecord.CORE_POC_IPAddress__c, result.ipAddress);
        System.AssertEquals(bufferRecord.CORE_POC_Latitude__c, result.latitute);
        System.AssertEquals(bufferRecord.CORE_POC_Longitude__c, result.longitude);

        bufferRecord.CORE_POC_CreatedDate__c = null;
        result = CORE_LFA_OpportunityManagement.getPocWrapperFromBufferRecord(bufferRecord);
        System.AssertEquals(null, result.creationDate);

        Test.stopTest();
    }

    @IsTest
    public static void getCatalogHierarchiesFromBufferRecords_Should_return_map_of_catolog_hierarchy_by_buffer_ids(){
        CORE_CatalogHierarchyModel catalog1 = new CORE_DataFaker_CatalogHierarchy('test1').catalogHierarchy;
        CORE_CatalogHierarchyModel catalog2 = new CORE_DataFaker_CatalogHierarchy('test2').catalogHierarchy;
        CORE_CatalogHierarchyModel catalog3 = new CORE_DataFaker_CatalogHierarchy('test3').catalogHierarchy;
        CORE_CatalogHierarchyModel catalog4 = new CORE_DataFaker_CatalogHierarchy('test4').catalogHierarchy;
        CORE_CatalogHierarchyModel catalog5 = new CORE_DataFaker_CatalogHierarchy('test5').catalogHierarchy;

        CORE_LeadAcquisitionBuffer__c bufferRecord1 = CORE_DataFaker_LeadAcquisitionBuffer.getBuffer('uniqueKey1','+33600000001', '+33200000001', 'emailTest1@test.com', catalog1);
        CORE_LeadAcquisitionBuffer__c bufferRecord2 = CORE_DataFaker_LeadAcquisitionBuffer.getBuffer('uniqueKey2','+33600000002', '+33200000002', 'emailTest2@test.com', catalog2);
        CORE_LeadAcquisitionBuffer__c bufferRecord3 = CORE_DataFaker_LeadAcquisitionBuffer.getBuffer('uniqueKey3','+33600000003', '+33200000003', 'emailTest3@test.com', catalog3);

        bufferRecord3 = CORE_DataFaker_LeadAcquisitionBuffer.getBufferWithInterest('uniqueKey3', catalog4, 2, bufferRecord3);
        bufferRecord3 = CORE_DataFaker_LeadAcquisitionBuffer.getBufferWithInterest('uniqueKey3', catalog5, 3, bufferRecord3);

        List<CORE_LeadAcquisitionBuffer__c> buffers = new List<CORE_LeadAcquisitionBuffer__c> {bufferRecord1, bufferRecord2, bufferRecord3};
        Test.startTest();
        Map<Id, CORE_LFA_OpportunityManagement.CatalogBuffer> results = CORE_LFA_OpportunityManagement.getCatalogHierarchiesFromBufferRecords(buffers);
        Test.stopTest();

        System.assertEquals(3, results.size());
        System.assertEquals(catalog1.division.Id, results.get(bufferRecord1.Id).mainInterest.division?.Id);
        System.assertEquals(catalog1.businessUnit.Id, results.get(bufferRecord1.Id).mainInterest.businessUnit?.Id);
        System.assertEquals(catalog1.studyArea.Id, results.get(bufferRecord1.Id).mainInterest.studyArea?.Id);
        System.assertEquals(catalog1.curriculum.Id, results.get(bufferRecord1.Id).mainInterest.curriculum?.Id);
        System.assertEquals(catalog1.level.Id, results.get(bufferRecord1.Id).mainInterest.level?.Id);
        System.assertEquals(catalog1.intake.Id, results.get(bufferRecord1.Id).mainInterest.intake?.Id);

        System.assertEquals(catalog2.division.Id, results.get(bufferRecord2.Id).mainInterest.division?.Id);
        System.assertEquals(catalog2.businessUnit.Id, results.get(bufferRecord2.Id).mainInterest.businessUnit?.Id);
        System.assertEquals(catalog2.studyArea.Id, results.get(bufferRecord2.Id).mainInterest.studyArea?.Id);
        System.assertEquals(catalog2.curriculum.Id, results.get(bufferRecord2.Id).mainInterest.curriculum?.Id);
        System.assertEquals(catalog2.level.Id, results.get(bufferRecord2.Id).mainInterest.level?.Id);
        System.assertEquals(catalog2.intake.Id, results.get(bufferRecord2.Id).mainInterest.intake?.Id);

        System.assertEquals(catalog3.division.Id, results.get(bufferRecord3.Id).mainInterest.division?.Id);
        System.assertEquals(catalog3.businessUnit.Id, results.get(bufferRecord3.Id).mainInterest.businessUnit?.Id);
        System.assertEquals(catalog3.studyArea.Id, results.get(bufferRecord3.Id).mainInterest.studyArea?.Id);
        System.assertEquals(catalog3.curriculum.Id, results.get(bufferRecord3.Id).mainInterest.curriculum?.Id);
        System.assertEquals(catalog3.level.Id, results.get(bufferRecord3.Id).mainInterest.level?.Id);
        System.assertEquals(catalog3.intake.Id, results.get(bufferRecord3.Id).mainInterest.intake?.Id);

        System.assertEquals(catalog1.division.Id, results.get(bufferRecord1.Id).secondInterest.division?.Id);
        System.assertEquals(catalog1.businessUnit.Id, results.get(bufferRecord1.Id).secondInterest.businessUnit?.Id);
        System.assertEquals(catalog1.studyArea.Id, results.get(bufferRecord1.Id).secondInterest.studyArea?.Id);
        System.assertEquals(catalog1.curriculum.Id, results.get(bufferRecord1.Id).secondInterest.curriculum?.Id);
        System.assertEquals(catalog1.level.Id, results.get(bufferRecord1.Id).secondInterest.level?.Id);
        System.assertEquals(catalog1.intake.Id, results.get(bufferRecord1.Id).secondInterest.intake?.Id);

        System.assertEquals(catalog2.division.Id, results.get(bufferRecord2.Id).secondInterest.division?.Id);
        System.assertEquals(catalog2.businessUnit.Id, results.get(bufferRecord2.Id).secondInterest.businessUnit?.Id);
        System.assertEquals(catalog2.studyArea.Id, results.get(bufferRecord2.Id).secondInterest.studyArea?.Id);
        System.assertEquals(catalog2.curriculum.Id, results.get(bufferRecord2.Id).secondInterest.curriculum?.Id);
        System.assertEquals(catalog2.level.Id, results.get(bufferRecord2.Id).secondInterest.level?.Id);
        System.assertEquals(catalog2.intake.Id, results.get(bufferRecord2.Id).secondInterest.intake?.Id);

        System.assertEquals(catalog4.division.Id, results.get(bufferRecord3.Id).secondInterest.division?.Id);
        System.assertEquals(catalog4.businessUnit.Id, results.get(bufferRecord3.Id).secondInterest.businessUnit?.Id);
        System.assertEquals(catalog4.studyArea.Id, results.get(bufferRecord3.Id).secondInterest.studyArea?.Id);
        System.assertEquals(catalog4.curriculum.Id, results.get(bufferRecord3.Id).secondInterest.curriculum?.Id);
        System.assertEquals(catalog4.level.Id, results.get(bufferRecord3.Id).secondInterest.level?.Id);
        System.assertEquals(catalog4.intake.Id, results.get(bufferRecord3.Id).secondInterest.intake?.Id);

        System.assertEquals(catalog1.division.Id, results.get(bufferRecord1.Id).thirdInterest.division?.Id);
        System.assertEquals(catalog1.businessUnit.Id, results.get(bufferRecord1.Id).thirdInterest.businessUnit?.Id);
        System.assertEquals(catalog1.studyArea.Id, results.get(bufferRecord1.Id).thirdInterest.studyArea?.Id);
        System.assertEquals(catalog1.curriculum.Id, results.get(bufferRecord1.Id).thirdInterest.curriculum?.Id);
        System.assertEquals(catalog1.level.Id, results.get(bufferRecord1.Id).thirdInterest.level?.Id);
        System.assertEquals(catalog1.intake.Id, results.get(bufferRecord1.Id).thirdInterest.intake?.Id);

        System.assertEquals(catalog2.division.Id, results.get(bufferRecord2.Id).thirdInterest.division?.Id);
        System.assertEquals(catalog2.businessUnit.Id, results.get(bufferRecord2.Id).thirdInterest.businessUnit?.Id);
        System.assertEquals(catalog2.studyArea.Id, results.get(bufferRecord2.Id).thirdInterest.studyArea?.Id);
        System.assertEquals(catalog2.curriculum.Id, results.get(bufferRecord2.Id).thirdInterest.curriculum?.Id);
        System.assertEquals(catalog2.level.Id, results.get(bufferRecord2.Id).thirdInterest.level?.Id);
        System.assertEquals(catalog2.intake.Id, results.get(bufferRecord2.Id).thirdInterest.intake?.Id);

        System.assertEquals(catalog5.division.Id, results.get(bufferRecord3.Id).thirdInterest.division?.Id);
        System.assertEquals(catalog5.businessUnit.Id, results.get(bufferRecord3.Id).thirdInterest.businessUnit?.Id);
        System.assertEquals(catalog5.studyArea.Id, results.get(bufferRecord3.Id).thirdInterest.studyArea?.Id);
        System.assertEquals(catalog5.curriculum.Id, results.get(bufferRecord3.Id).thirdInterest.curriculum?.Id);
        System.assertEquals(catalog5.level.Id, results.get(bufferRecord3.Id).thirdInterest.level?.Id);
        System.assertEquals(catalog5.intake.Id, results.get(bufferRecord3.Id).thirdInterest.intake?.Id);
    }
    @IsTest
    public static void getCatalogHierarchyFromBufferRecord_Should_return_a_catolog_hierarchy_from_buffer(){
        CORE_CatalogHierarchyModel catalog1 = new CORE_DataFaker_CatalogHierarchy('test1').catalogHierarchy;
        CORE_CatalogHierarchyModel catalog2 = new CORE_DataFaker_CatalogHierarchy('test2').catalogHierarchy;
        CORE_CatalogHierarchyModel catalog3 = new CORE_DataFaker_CatalogHierarchy('test3').catalogHierarchy;

        CORE_LeadAcquisitionBuffer__c bufferRecord1 = CORE_DataFaker_LeadAcquisitionBuffer.getBuffer('uniqueKey1','+33600000001', '+33200000001', 'emailTest1@test.com', catalog1);
        CORE_LeadAcquisitionBuffer__c bufferRecord2 = CORE_DataFaker_LeadAcquisitionBuffer.getBuffer('uniqueKey2','+33600000002', '+33200000002', 'emailTest2@test.com', catalog2);
        CORE_LeadAcquisitionBuffer__c bufferRecord3 = CORE_DataFaker_LeadAcquisitionBuffer.getBuffer('uniqueKey3','+33600000003', '+33200000003', 'emailTest3@test.com', catalog3);

        Test.startTest();
        CORE_CatalogHierarchyModel result1 = CORE_LFA_OpportunityManagement.getCatalogHierarchyFromBufferRecord(bufferRecord1, OPP_TYPE_1);
        CORE_CatalogHierarchyModel result2 = CORE_LFA_OpportunityManagement.getCatalogHierarchyFromBufferRecord(bufferRecord2, OPP_TYPE_2);
        CORE_CatalogHierarchyModel result3 = CORE_LFA_OpportunityManagement.getCatalogHierarchyFromBufferRecord(bufferRecord3, OPP_TYPE_3);
        Test.stopTest();


        System.assertEquals(catalog1.division.Id, result1.division?.Id);
        System.assertEquals(catalog1.businessUnit.Id, result1.businessUnit?.Id);
        System.assertEquals(catalog1.studyArea.Id, result1.studyArea?.Id);
        System.assertEquals(catalog1.curriculum.Id, result1.curriculum?.Id);
        System.assertEquals(catalog1.level.Id, result1.level?.Id);
        System.assertEquals(catalog1.intake.Id, result1.intake?.Id);

        System.assertEquals(catalog2.division.Id, result2.division?.Id);
        System.assertEquals(catalog2.businessUnit.Id, result2.businessUnit?.Id);
        System.assertEquals(catalog2.studyArea.Id, result2.studyArea?.Id);
        System.assertEquals(catalog2.curriculum.Id, result2.curriculum?.Id);
        System.assertEquals(catalog2.level.Id, result2.level?.Id);
        System.assertEquals(catalog2.intake.Id, result2.intake?.Id);

        System.assertEquals(catalog3.division.Id, result3.division?.Id);
        System.assertEquals(catalog3.businessUnit.Id, result3.businessUnit?.Id);
        System.assertEquals(catalog3.studyArea.Id, result3.studyArea?.Id);
        System.assertEquals(catalog3.curriculum.Id, result3.curriculum?.Id);
        System.assertEquals(catalog3.level.Id, result3.level?.Id);
        System.assertEquals(catalog3.intake.Id, result3.intake?.Id);

    }

    @IsTest
    public static void getMainOpportunityFromAcademicYear_should_return_first_opportunity_found_on_same_academic_year(){
        String matchingAcademicYear = 'CORE_PKL_2022-2023';
        String notMatchingAcademicYear1 = 'CORE_PKL_2023-2024';
        String notMatchingAcademicYear2 = 'CORE_PKL_2021-2022';
        Opportunity opp1 = new Opportunity(CORE_OPP_Academic_Year__c = matchingAcademicYear, Name = 'test1');
        Opportunity opp2 = new Opportunity(CORE_OPP_Academic_Year__c = matchingAcademicYear, Name = 'test2');
        Opportunity opp3 = new Opportunity(CORE_OPP_Academic_Year__c = notMatchingAcademicYear1, Name = 'test3');
        Opportunity opp4 = new Opportunity(CORE_OPP_Academic_Year__c = notMatchingAcademicYear2, Name = 'test4');

        List<Opportunity> opps = new List<Opportunity> {opp1, opp2, opp3, opp4};

        Test.startTest();
        Opportunity result = CORE_LFA_OpportunityManagement.getMainOpportunityFromAcademicYear(matchingAcademicYear, opps);
        
        System.assertEquals(opp1.Name, result.Name);

        opp1.CORE_OPP_Academic_Year__c = notMatchingAcademicYear1;
        result = CORE_LFA_OpportunityManagement.getMainOpportunityFromAcademicYear(matchingAcademicYear, opps);

        System.assertEquals(opp2.Name, result.Name);

        opp2.CORE_OPP_Academic_Year__c = notMatchingAcademicYear1;
        result = CORE_LFA_OpportunityManagement.getMainOpportunityFromAcademicYear(matchingAcademicYear, opps);

        System.assertEquals(null, result);

        Test.stopTest();
    }

    @IsTest
    public static void getMainOpportunityFromBusinessUnit_should_return_list_of_opportunity_found_on_same_businessUnit(){
        CORE_CatalogHierarchyModel catalog1 = new CORE_DataFaker_CatalogHierarchy('Matching').catalogHierarchy;
        CORE_CatalogHierarchyModel catalog2 = new CORE_DataFaker_CatalogHierarchy('NotMatching1').catalogHierarchy;
        CORE_CatalogHierarchyModel catalog3 = new CORE_DataFaker_CatalogHierarchy('NotMatching2').catalogHierarchy;
        Id matchingId = catalog1.businessUnit.Id;
        Id notMatchingId1 = catalog2.businessUnit.Id;
        Id notMatchingId2 = catalog3.businessUnit.Id;
        Opportunity opp1 = new Opportunity(CORE_OPP_Business_Unit__c = matchingId , Name = 'test1');
        Opportunity opp2 = new Opportunity(CORE_OPP_Business_Unit__c = notMatchingId1, Name = 'test2');
        Opportunity opp3 = new Opportunity(CORE_OPP_Business_Unit__c = notMatchingId1, Name = 'test3');
        Opportunity opp4 = new Opportunity(CORE_OPP_Business_Unit__c = matchingId, Name = 'test4');

        List<Opportunity> opps = new List<Opportunity> {opp1, opp2, opp3, opp4};

        Test.startTest();
        List<Opportunity> results = CORE_LFA_OpportunityManagement.getMainOpportunityFromBusinessUnit(matchingId, opps);
        
        System.assertEquals(2, results.size());
        System.assertEquals(opp1.Name, results.get(0).Name);
        System.assertEquals(opp4.Name, results.get(1).Name);

        opp1.CORE_OPP_Business_Unit__c = notMatchingId2;
        results = CORE_LFA_OpportunityManagement.getMainOpportunityFromBusinessUnit(matchingId, opps);

        System.assertEquals(1, results.size());
        System.assertEquals(opp4.Name, results.get(0).Name);

        opp4.CORE_OPP_Business_Unit__c = notMatchingId2;
        results = CORE_LFA_OpportunityManagement.getMainOpportunityFromBusinessUnit(matchingId, opps);

        System.assertEquals(0, results.size());

        Test.stopTest();
    }
}