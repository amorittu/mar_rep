public class CORE_DataConvertion {
    public static DateTime convertTimeStampToDateTime(String strTimestamp,boolean localValue) {
        if(String.isBlank(strTimestamp)){
            return null;
        }
        
        Long timestamp = Long.valueOf(strTimestamp);

        DateTime gmtDatetime = DateTime.newInstance(timestamp);
        
        System.TimeZone myTz = UserInfo.getTimeZone();
        
        Integer millisecondOffsetGmt = myTz.getOffset(gmtDateTime);
        
        DateTime localDatetime = DateTime.newInstance(timeStamp - millisecondOffsetGmt);

        if(localValue){

            return localDatetime;
        } else {
            if(gmtDatetime > datetime.now()){
                return datetime.now();
            }

            return gmtDatetime;
        }
    }

    public static DateTime convertSpecialStringFormatToDateTime(String strDate) {
        String year = strDate.substring(12, 16);
        Map<String,String> monthNumberByPrefix = new Map<String,String>();
        monthNumberByPrefix.put('Jan','01');
        monthNumberByPrefix.put('Feb','02');
        monthNumberByPrefix.put('Mar','03');
        monthNumberByPrefix.put('Apr','04');
        monthNumberByPrefix.put('May','05');
        monthNumberByPrefix.put('Jun','06');
        monthNumberByPrefix.put('Jul','07');
        monthNumberByPrefix.put('Aug','08');
        monthNumberByPrefix.put('Sep','09');
        monthNumberByPrefix.put('Oct','10');
        monthNumberByPrefix.put('Nov','11');
        monthNumberByPrefix.put('Dec','12');

        String monthValue = strDate.substring(8, 11);
        String month = monthNumberByPrefix.get(monthValue);

        String day = strDate.substring(5, 7);
        String hour = strDate.substring(17, 19);
        String minute = strDate.substring(20, 22);
        String second = strDate.substring(23, 25);

        String finalDate = year + '-' + month + '-' + day + ' ' + hour + ':' + minute +  ':' + second;
        System.debug('finalDate = '+ finalDate);
        Datetime myDate = datetime.valueOfGmt(finalDate);

        return myDate;
    }
}