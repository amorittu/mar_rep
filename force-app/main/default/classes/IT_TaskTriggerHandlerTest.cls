/**
 * Created by ftrapani on 18/11/2021.
 */

@IsTest
public with sharing class IT_TaskTriggerHandlerTest {

    @TestSetup
    static void testSetup() {
        CORE_TriggerUtils.bypassTrigger = true;
        CORE_DataFaker_CatalogHierarchy catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test1');
        PricebookEntry pbe = new PricebookEntry(Product2Id=catalogHierarchy.product.Id, Pricebook2Id=Test.getStandardPricebookId(), UnitPrice=0, IsActive=true);
        insert pbe;

        Account account = CORE_DataFaker_Account.getStudentAccount('test1');
        Opportunity opportunity = CORE_DataFaker_Opportunity.getOpportunity(
                catalogHierarchy.division.Id,
                catalogHierarchy.businessUnit.Id,
                account.Id,
                'Applicant',
                'Applicant - Application Submitted / New'
        );

        opportunity.Pricebook2Id = Test.getStandardPricebookId();
        opportunity.CORE_OPP_Intake__c = catalogHierarchy.intake.Id;
        opportunity.CORE_OPP_Level__c = catalogHierarchy.level.Id;
        opportunity.CORE_OPP_Curriculum__c = catalogHierarchy.curriculum.Id;
        opportunity.CORE_OPP_Study_Area__c = catalogHierarchy.studyArea.Id;
        opportunity.CORE_Capture_Channel__c = 'CORE_PKL_Email';
        opportunity.IT_OpportunityAssigned__c = false;
        update opportunity;

        IT_ExtendedInfo__c extendedInfo = IT_DataFaker_Objects.createExtendedInfo(account.id, opportunity.CORE_Brand__c, true);
        CORE_TriggerUtils.bypassTrigger = false;
    }

    @IsTest
    static void testInformation(){

        Map<String,IT_Role_Type__mdt> getRoleTypeMDT = IT_Utility.getRoleTypeMDT2();
        String nameRole = '';
        for(String rolename : getRoleTypeMDT.keySet()){
            if(getRoleTypeMDT.get(rolename).IT_Type__c=='Information'){
                nameRole = rolename;
                break;
            }

        }
        System.debug('getRoleTypeMDT ' + getRoleTypeMDT);
        UserRole userRole = [SELECT Id FROM userRole WHERE DeveloperName = :nameRole];


        User currentUser = new User (
                Id = UserInfo.getUserId(),
                UserRoleId = userRole.Id
        );
        update currentUser;
        System.runAs (currentUser) {

            Opportunity opportunity = [SELECT Id FROM Opportunity];
             Task newTask = new Task(
                    WhatId = opportunity.Id,
                    CORE_Is_Incoming__c = true,
                    Type = 'CORE_PKL_Call',
                    Status = 'CORE_PKL_Open',
                    CORE_Start_Date__c=null
            );
            insert newTask;

            newTask.CORE_Is_Incoming__c = false;
            newTask.Status = 'CORE_PKL_Completed';
            update newTask;
        }

    }

    @IsTest
    static void testAdmission(){

        Map<String,IT_Role_Type__mdt> getRoleTypeMDT = IT_Utility.getRoleTypeMDT2();
        String nameRole = '';
        for(String rolename : getRoleTypeMDT.keySet()){
            if(getRoleTypeMDT.get(rolename).IT_Type__c=='Admission'){
                nameRole = rolename;
                break;
            }

        }
        System.debug('getRoleTypeMDT ' + getRoleTypeMDT);
        UserRole userRole = [SELECT Id FROM userRole WHERE DeveloperName = :nameRole];


        User currentUser = new User (
                Id = UserInfo.getUserId(),
                UserRoleId = userRole.Id
        );
        update currentUser;
        System.runAs (currentUser) {

            Opportunity opportunity = [SELECT Id FROM Opportunity];
            Task newTask = new Task(
                    WhatId = opportunity.Id,
                    CORE_Is_Incoming__c = true,
                    Type = 'CORE_PKL_Call',
                    Status = 'CORE_PKL_Open',
                    CORE_Start_Date__c=null
            );
            insert newTask;

            newTask.CORE_Is_Incoming__c = false;
            newTask.Status = 'CORE_PKL_Completed';
            update newTask;
        }

    }

    @IsTest
    static void testNoCheckIntakeTask(){
        Opportunity opportunity = [SELECT Id FROM Opportunity];
        Product2 product = [SELECT Id FROM Product2];
        CORE_Intake__c intake = [SELECT Id FROM CORE_Intake__c];

        Pricebook2 pb = new Pricebook2();
        pb.Name = 'test';
        insert pb;

        opportunity.Pricebook2Id = pb.Id;
        opportunity.StageName = 'Lead';

        update opportunity;

        PricebookEntry pbe = new PricebookEntry();
        pbe.Product2Id = product.Id;
        pbe.Pricebook2Id = opportunity.Pricebook2Id;
        pbe.UnitPrice = 15.22;
        insert pbe;

        Task newTask = new Task(
                Subject = 'Returning interest qualification',
                WhatId = opportunity.Id,
                CORE_Product__c = product.Id,
                CORE_Intake__c = intake.Id,
                Type = 'CORE_PKL_Web_Form'
        );
        insert newTask;

    }

    @IsTest
    static void testTaskReassignment(){
        Opportunity opportunity = [SELECT Id FROM Opportunity];
        System.debug('---opportunity ' + opportunity);
        Group queue = new Group(
                Email = 'toAddress@test.com',
                Type = 'Queue',
                Name = 'TestQueue'
        );
        insert queue;
        System.debug('---queue ' + queue);
        System.runAs(new User(Id = UserInfo.getUserId())) {
            QueueSobject testQueue = new QueueSobject(QueueId = queue.Id, SobjectType = 'Task');
            insert testQueue;
            System.debug('---testQueue ' + testQueue);
        }

        Id rtId = Schema.SObjectType.Task.getRecordTypeInfosByDeveloperName().get('CORE_Continuum_of_action').getRecordTypeId();

        Task newTask = new Task(
                WhatId = opportunity.Id,
                CORE_Is_Incoming__c = true,
                Type = 'CORE_PKL_Call',
                RecordTypeId = rtId,
                Status = 'CORE_PKL_Open',
                CORE_Start_Date__c=null,
                OwnerId = queue.Id
        );
        insert newTask;
        System.debug('---newTask ' + newTask);
        Task newTask2 = new Task(
                WhatId = opportunity.Id,
                CORE_Is_Incoming__c = true,
                Type = 'CORE_PKL_Call',
                RecordTypeId = rtId,
                Status = 'CORE_PKL_Open',
                CORE_Start_Date__c=null
        );
        insert newTask2;
        System.debug('---newTask2 ' + newTask2);
        Formula.recalculateFormulas(new List<Task>{newTask2});

        newTask.OwnerId = UserInfo.getUserId();
        System.debug('---newTask update ' + newTask);
        update newTask;
    }

    @IsTest
    static void testUpdateRelatedOpps() {
        Opportunity opp = [SELECT Id FROM Opportunity LIMIT 1];
        String testEmail = 'test@test.com';
        BusinessHours hours = [SELECT Id FROM BusinessHours LIMIT 1];
        insert new IT_Office__c(IT_Type__c = 'Orientation', IT_Email__c = testEmail, IT_UndergraduateBusinessHours__c = hours.Id);
        Task t = new Task(WhatId = opp.Id, CORE_Is_Incoming__c = true, Type = 'CORE_PKL_Email', CORE_Recipient_Incoming_Email_Address__c = 'testOld@test.com');
        insert t;

        Test.startTest();
        t.CORE_Recipient_Incoming_Email_Address__c = testEmail;
        update t;
        Test.stopTest();

        Opportunity oppToCheck = [SELECT IT_OfficeInCharge__c FROM Opportunity LIMIT 1];
        //System.assertEquals('Orientation', oppToCheck.IT_OfficeInCharge__c);
    }

}