global class CORE_OpportunityWithdrawSubStatus {
    @InvocableMethod
    global static List<List<String>> getOpportunityWithdrawStatus(List<OpportunityWithdrawStatusParameters> params){
        List<String> resultStatus = new List<String>();

        if (params.get(0).showedUp.equals('CORE_PKL_We don\'t know yet')){
            resultStatus.add(params.get(0).startStatus + ' - Exit - Waiting for decision');
        }
        else if ((params.get(0).startStatus.equals('Registered') || params.get(0).startStatus.equals('Re-registered')) && params.get(0).showedUp.equals('CORE_PKL_No')){
            resultStatus.add(params.get(0).startStatus + ' - Exit - Term Deferral');
            resultStatus.add(params.get(0).startStatus + ' - Exit - Reoriented');
            resultStatus.add(params.get(0).startStatus + ' - Exit - Abandoned');
            resultStatus.add(params.get(0).startStatus + ' - Exit - Cancelled reimbursement');
            resultStatus.add(params.get(0).startStatus + ' - Exit - Cancelled no reimbursement');
            resultStatus.add(params.get(0).startStatus + ' - Exit - Before course start - deposit deadline');
            resultStatus.add(params.get(0).startStatus + ' - Exit - After course start - withdrawal period');
            resultStatus.add(params.get(0).startStatus + ' - Exit - After course start - deposit period');
            resultStatus.add(params.get(0).startStatus + ' - Exit - After course start - Out of time');
            resultStatus.add(params.get(0).startStatus + ' - Exit - Waiting for decision');
            resultStatus.add(params.get(0).startStatus + ' - Exit - Force majeure');
            resultStatus.add(params.get(0).startStatus + ' - Exit - Archived');
            resultStatus.add(params.get(0).startStatus + ' - Exit - Visa denial');
        }
        else if ((params.get(0).startStatus.equals('Enrolled') || params.get(0).startStatus.equals('Re-enrolled')) && params.get(0).showedUp.equals('CORE_PKL_No')){
            resultStatus.add(params.get(0).startStatus + ' - Exit - Term Deferral');
            resultStatus.add(params.get(0).startStatus + ' - Exit - Abandoned');
            resultStatus.add(params.get(0).startStatus + ' - Exit - Cancelled reimbursement');
            resultStatus.add(params.get(0).startStatus + ' - Exit - Cancelled no reimbursement');
            resultStatus.add(params.get(0).startStatus + ' - Exit - Before course start - deposit deadline');
            resultStatus.add(params.get(0).startStatus + ' - Exit - After course start - withdrawal period');
            resultStatus.add(params.get(0).startStatus + ' - Exit - After course start - deposit period');
            resultStatus.add(params.get(0).startStatus + ' - Exit - After course start - Out of time');
            resultStatus.add(params.get(0).startStatus + ' - Exit - Waiting for decision');
            resultStatus.add(params.get(0).startStatus + ' - Exit - Force majeure');
            resultStatus.add(params.get(0).startStatus + ' - Exit - Visa denial');
        }
        else if (params.get(0).showedUp.equals('CORE_PKL_Yes')){
            resultStatus.add('Withdraw - Term Deferral');
            resultStatus.add('Withdraw - Reoriented');
            resultStatus.add('Withdraw - Force majeure');
            resultStatus.add('Withdraw - Visa Denied');
            resultStatus.add('Withdraw - Waiting for decision');
            resultStatus.add('Withdraw - Show Abandoned');
            resultStatus.add('Withdraw - Cancelled reimbursement');
            resultStatus.add('Withdraw - Cancelled no reimbursement');
        }

        return new List<List<String>> {resultStatus};
    }

    global class OpportunityWithdrawStatusParameters{
        @InvocableVariable(required=true)
        global String startStatus;

        @InvocableVariable(required=true)
        global String showedUp;
    }
}