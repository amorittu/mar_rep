public without sharing class CORE_OppotunityTriggerHandler {
    private static final string OPPORTUNITY_DEFAULT_NAME = 'New Interest';
    /* Edited By atlantic START - 22/06/2022 - Block recursive calls to before update method */
    public static Boolean fireBeforeUpdate                          = true;
    /* Edited By atlantic END - 22/06/2022 - Block recursive calls to before update method */
    /* Edited By atlantic START - 22/06/2022 - Block recursive calls to after update method */
    public static Boolean fireAfterUpdate                           = true;
    /* Edited By atlantic END - 22/06/2022 - Block recursive calls to after update method */
    /* Edited By atlantic START - 22/06/2022 - Block recursive afterUpdate method */
    public static Boolean fireupdateTasks                           = true;
    /* Edited By atlantic END - 22/06/2022 - Block recursive afterUpdate method */
    /* Edited By atlantic START - 22/06/2022 - Block recursive createChangeInterestHistory method */
    public static Boolean fireCreateChangeInterestHistory           = true;
    /* Edited By atlantic END - 22/06/2022 - Block recursive createChangeInterestHistory method */
    /* Edited By atlantic START - 22/06/2022 - Block recursive updateOpportunityTechFields  method */
    public static Boolean fireUpdateOpportunityTechFields           = true;
    /* Edited By atlantic END - 22/06/2022 - Block recursive updateOpportunityTechFields  method */

    @TestVisible
    private static final string DELETED_OBJECT_TYPE = 'CORE_PKL_Opportunity';
    private static final string PROFILE_OWNER_CONDITION = 'CORE_API_Profile';
    private static final String LEAD_STATUS = 'Lead';
    public static final string CLOSED_LOST = 'Closed Lost';
    public static final string CLOSED_WON = 'Closed Won';
    private static final string TASK_OPEN = 'CORE_PKL_Open';
    private static final string TASK_CANCELLED = 'CORE_PKL_Cancelled';
    private static final string WITHDRAW = 'Withdraw';
    public static final List<string> ORDER_STATUS = new List<string>{'Suspect','Prospect','Lead','Applicant','Admitted','Registered','Enrolled','Withdraw'}; 
    private static final List<string> LIST_SUB_STATUS = new List<String>{'Prospect - To Follow Up','Lead - To Follow Up','Prospect - Show','Lead - Show','Lead - Qualified For Intake / Waiting For Application','Lead - Application Started','Prospect - Qualified For Intake / Waiting For Application','Prospect - Application Started'};
    private static final List<string> LIST_STATUS = new List<String>{'Applicant','Admitted', 'Registered', 'Enrolled', 'Withdraw', 'Closed Lost', 'Closed Won'};
                                                                    

    public static void handleTrigger(List<Opportunity> oldRecords, List<Opportunity> newRecords, System.TriggerOperation triggerEvent) {
        switch on triggerEvent {
            when BEFORE_INSERT {
                System.debug('CORE_OppotunityTriggerHandler.beforeInsert : START');
                beforeInsert(newRecords);
                System.debug('CORE_OppotunityTriggerHandler.beforeInsert : END');
            }
            when BEFORE_UPDATE {
                System.debug('CORE_OppotunityTriggerHandler.beforeUpdate : START');
                if(fireBeforeUpdate){
                    beforeUpdate(oldRecords,newRecords);
                    fireBeforeUpdate = false;
                }
                System.debug('CORE_OppotunityTriggerHandler.beforeUpdate : END');
            }
            when AFTER_INSERT {
                System.debug('CORE_OppotunityTriggerHandler.afterInsert : START');
                afterInsert(newRecords);
                System.debug('CORE_OppotunityTriggerHandler.afterInsert : END');
            }
            when AFTER_UPDATE {
                System.debug('CORE_OppotunityTriggerHandler.afterUpdate : START');
                afterUpdate(oldRecords, newRecords);
                System.debug('CORE_OppotunityTriggerHandler.afterUpdate : END');
            }
            when AFTER_DELETE {
                System.debug('CORE_OppotunityTriggerHandler.afterDelete : START');
                afterDelete(oldRecords);
                System.debug('CORE_OppotunityTriggerHandler.afterDelete : END');
            }
        }
    }

    @TestVisible
    private static void beforeInsert(List<Opportunity> newRecords) {
        List<Id> getAllCurriculum = new List<Id>();
        List<Id> getAllBusiness = new List<Id>();
        List<Id> getAllDivision = new List<Id>();
        List<Id> getAllIntake = new List<Id>();
        List<Id> getAllLevel= new List<Id>();
        List<Id> getAllStudy= new List<Id>();
        List<Id> getAllProduct= new List<Id>();
        List<Id> getAllOwner= new List<Id>();
		
		//Edited by Atlantic: All lists populated only if necessary

        for(Opportunity currentRecord : newRecords){
            if(currentRecord.CORE_OPP_Curriculum__c!=null){
                getAllCurriculum.add(currentRecord.CORE_OPP_Curriculum__c);
            }
            if(currentRecord.CORE_OPP_Business_Unit__c!=null){
                getAllBusiness.add(currentRecord.CORE_OPP_Business_Unit__c);
            }
            if(currentRecord.CORE_OPP_Division__c!=null){
                getAllDivision.add(currentRecord.CORE_OPP_Division__c);
            }
            if(currentRecord.CORE_OPP_Intake__c!=null){
                getAllIntake.add(currentRecord.CORE_OPP_Intake__c);
            }
            if(currentRecord.CORE_OPP_Level__c!=null){
                getAllLevel.add(currentRecord.CORE_OPP_Level__c);
            }
            if(currentRecord.CORE_OPP_Study_Area__c!=null){
                getAllStudy.add(currentRecord.CORE_OPP_Study_Area__c);
            }
            if(currentRecord.CORE_Main_Product_Interest__c!=null){
                getAllProduct.add(currentRecord.CORE_Main_Product_Interest__c);
            }
            getAllOwner.add(currentRecord.OwnerId);
        }

        List<User> owners = [SELECT Id,Profile.Name FROM User WHERE Id IN :getAllOwner];


		//Edited by Atlantic: All queries performed only if lists are populated
		
        List<CORE_Curriculum__c> curriculumList = new List<CORE_Curriculum__c>();
        if(getAllCurriculum.size()>0){
            curriculumList = [SELECT Id,CORE_Curriculum_Name__c FROM CORE_Curriculum__c WHERE Id IN :getAllCurriculum];
        }

        List<CORE_Business_Unit__c> businessList = new List<CORE_Business_Unit__c>();
        if(getAllBusiness.size()>0){
            businessList = [SELECT Id,Name, CORE_Brand__c FROM CORE_Business_Unit__c WHERE Id IN :getAllBusiness];
        }

        List<CORE_Division__c> divisionList = new  List<CORE_Division__c>();
        if(getAllDivision.size()>0){
            divisionList = [SELECT Id,Name FROM CORE_Division__c WHERE Id IN :getAllDivision];
        }

        List<CORE_Intake__c> intakeList = new  List<CORE_Intake__c>();
        if(getAllIntake.size()>0){
            intakeList = [SELECT Id,Name FROM CORE_Intake__c WHERE Id IN :getAllIntake];
        }

        List<CORE_Level__c> levelList = new  List<CORE_Level__c>();
        if(getAllLevel.size()>0){
            levelList = [SELECT Id,Name FROM CORE_Level__c WHERE Id IN :getAllLevel];
        }

        List<CORE_Study_Area__c> studyList = new  List<CORE_Study_Area__c>();
        if(getAllStudy.size()>0){
            studyList = [SELECT Id,Name FROM CORE_Study_Area__c WHERE Id IN :getAllStudy];
        }

        List<Product2> productInterestList = new  List<Product2>();
        if(getAllProduct.size()>0){
            productInterestList = [SELECT Id,Name FROM Product2 WHERE Id IN :getAllProduct];
        }

        Map<Id,User> newMapOwner = new Map<Id,User>(owners);
		
		//Edited by Atlantic: All maps populateds only if lists are populated
        Map<Id,CORE_Curriculum__c> newMapCurriculum = curriculumList.size()>0 ? new Map<Id,CORE_Curriculum__c>(curriculumList) : new Map<Id,CORE_Curriculum__c>();
        Map<Id,CORE_Business_Unit__c> newMapBusiness = businessList.size()>0 ? new Map<Id,CORE_Business_Unit__c>(businessList) : new Map<Id,CORE_Business_Unit__c>();
        Map<Id,CORE_Division__c> newMapDivision = divisionList.size()>0 ? new Map<Id,CORE_Division__c>(divisionList) : new Map<Id,CORE_Division__c>();
        Map<Id,CORE_Intake__c> newMapIntake = intakeList.size()>0 ? new Map<Id,CORE_Intake__c>(intakeList) : new Map<Id,CORE_Intake__c>();
        Map<Id,CORE_Level__c> newMapLevel = levelList.size()>0 ? new Map<Id,CORE_Level__c>(levelList) : new Map<Id,CORE_Level__c>();
        Map<Id,CORE_Study_Area__c> newMapStudy = studyList.size()>0 ? new Map<Id,CORE_Study_Area__c>(studyList) : new Map<Id,CORE_Study_Area__c>();
        Map<Id,Product2> newMapProductInterest = productInterestList.size()>0 ? new Map<Id,Product2>(productInterestList) : new Map<Id,Product2>();

          
        for(Opportunity currentRecord : newRecords){
            CORE_Curriculum__c myCurriculum = newMapCurriculum.get(currentRecord.CORE_OPP_Curriculum__c);
            CORE_Business_Unit__c myBusiness = newMapBusiness.get(currentRecord.CORE_OPP_Business_Unit__c);
            CORE_Division__c myDivision = newMapDivision.get(currentRecord.CORE_OPP_Division__c);
            CORE_Intake__c myIntake = newMapIntake.get(currentRecord.CORE_OPP_Intake__c);
            CORE_Level__c myLevel = newMapLevel.get(currentRecord.CORE_OPP_Level__c);
            CORE_Study_Area__c myStudy = newMapStudy.get(currentRecord.CORE_OPP_Study_Area__c);
            Product2 myProduct = newMapProductInterest.get(currentRecord.CORE_Main_Product_Interest__c);
            User owner = newMapOwner.get(currentRecord.OwnerId);

            currentRecord.CORE_OPP_TCurriculum__c= myCurriculum?.CORE_Curriculum_Name__c;
            currentRecord.CORE_OPP_TBusiness_Unit__c= myBusiness?.Name;
            currentRecord.CORE_OPP_TDivision__c= myDivision?.Name;
            currentRecord.CORE_BU_Brand__c = myBusiness?.CORE_Brand__c;
            currentRecord.CORE_OPP_TIntake__c= myIntake?.Name;
            currentRecord.CORE_OPP_TLevel__c= myLevel?.Name; 
            currentRecord.CORE_OPP_TStudy_Area__c= myStudy?.Name; 
            currentRecord.CORE_ProfileOwner__c = owner.Profile.Name;
            currentRecord.Name = getOpportunityName(currentRecord);
        }
    }
    @TestVisible
    private static boolean isSubStatusChanged(Opportunity oldOpportunity, Opportunity newOpportunity){
        if(oldOpportunity.CORE_OPP_Sub_Status__c != newOpportunity.CORE_OPP_Sub_Status__c){
            return true;
        } else{
            return false;
        }
    }

    @TestVisible
    private static boolean isStatusChanged(Opportunity oldOpportunity, Opportunity newOpportunity){
        if(oldOpportunity.StageName != newOpportunity.StageName){
            return true;
        } else{
            return false;
        }
    }

    @TestVisible
    private static void beforeUpdate(List<Opportunity> oldRecords,List<Opportunity> newRecords) {
        List<Id> getAllCurriculum = new List<Id>();
        List<Id> getAllBusiness = new List<Id>();
        List<Id> getAllDivision = new List<Id>();
        List<Id> getAllIntake = new List<Id>();
        List<Id> getAllLevel= new List<Id>();
        List<Id> getAllStudy= new List<Id>();
        List<Id> getAllOwner= new List<Id>();
        // TO DO recupéré le bon businessHour
        //BusinessHours businessHours = [SELECT Id FROM BusinessHours WHERE IsDefault=true];


		//Edited by Atlantic: All lists populated only if necessary
        Map <Id,Opportunity> myMapOldRecord = new Map<Id,Opportunity>(oldRecords);
        for(Opportunity currentRecord : newRecords){
            Opportunity currentOldRecord = myMapOldRecord.get(currentRecord.Id);
            if(currentRecord.CORE_OPP_Curriculum__c != currentOldRecord.CORE_OPP_Curriculum__c){
                getAllCurriculum.add(currentRecord.CORE_OPP_Curriculum__c);
            }

            if(currentRecord.CORE_OPP_Business_Unit__c != currentOldRecord.CORE_OPP_Business_Unit__c){
                getAllBusiness.add(currentRecord.CORE_OPP_Business_Unit__c);
            }

            if(currentRecord.CORE_OPP_Division__c != currentOldRecord.CORE_OPP_Division__c){
                getAllDivision.add(currentRecord.CORE_OPP_Division__c);
            }

            if(currentRecord.CORE_OPP_Intake__c != currentOldRecord.CORE_OPP_Intake__c){
                getAllIntake.add(currentRecord.CORE_OPP_Intake__c);
            }

            if(currentRecord.CORE_OPP_Level__c != currentOldRecord.CORE_OPP_Level__c){
                getAllLevel.add(currentRecord.CORE_OPP_Level__c);
            }

            if(currentRecord.CORE_OPP_Study_Area__c != currentOldRecord.CORE_OPP_Study_Area__c){
                getAllStudy.add(currentRecord.CORE_OPP_Study_Area__c);
            }


            getAllOwner.add(currentRecord.OwnerId);
        }

		//Edited by Atlantic: All queries performed only if lists are populated

        List<CORE_Curriculum__c> curriculumList = new List<CORE_Curriculum__c>();
        if(getAllCurriculum.size()>0){
            curriculumList = [SELECT Id,CORE_Curriculum_Name__c FROM CORE_Curriculum__c WHERE Id IN :getAllCurriculum];
        }

        List<CORE_Business_Unit__c> businessList = new List<CORE_Business_Unit__c>();
        if(getAllBusiness.size()>0){
            businessList = [SELECT Id,Name, CORE_Brand__c FROM CORE_Business_Unit__c WHERE Id IN :getAllBusiness];
        }

        List<CORE_Division__c> divisionList = new  List<CORE_Division__c>();
        if(getAllDivision.size()>0){
            divisionList = [SELECT Id,Name FROM CORE_Division__c WHERE Id IN :getAllDivision];
        }

        List<CORE_Intake__c> intakeList = new  List<CORE_Intake__c>();
        if(getAllIntake.size()>0){
            intakeList = [SELECT Id,Name FROM CORE_Intake__c WHERE Id IN :getAllIntake];
        }

        List<CORE_Level__c> levelList = new  List<CORE_Level__c>();
        if(getAllLevel.size()>0){
            levelList = [SELECT Id,Name FROM CORE_Level__c WHERE Id IN :getAllLevel];
        }

        List<CORE_Study_Area__c> studyList = new  List<CORE_Study_Area__c>();
        if(getAllStudy.size()>0){
            studyList = [SELECT Id,Name FROM CORE_Study_Area__c WHERE Id IN :getAllStudy];
        }


		//Edited by Atlantic: All maps populateds only if lists are populated

		Map<Id,CORE_Curriculum__c> newMapCurriculum = curriculumList.size()>0 ? new Map<Id,CORE_Curriculum__c>(curriculumList) : new Map<Id,CORE_Curriculum__c>();
        Map<Id,CORE_Business_Unit__c> newMapBusiness = businessList.size()>0 ? new Map<Id,CORE_Business_Unit__c>(businessList) : new Map<Id,CORE_Business_Unit__c>();
        Map<Id,CORE_Division__c> newMapDivision = divisionList.size()>0 ? new Map<Id,CORE_Division__c>(divisionList) : new Map<Id,CORE_Division__c>();
        Map<Id,CORE_Intake__c> newMapIntake = intakeList.size()>0 ? new Map<Id,CORE_Intake__c>(intakeList) : new Map<Id,CORE_Intake__c>();
        Map<Id,CORE_Level__c> newMapLevel = levelList.size()>0 ? new Map<Id,CORE_Level__c>(levelList) : new Map<Id,CORE_Level__c>();
        Map<Id,CORE_Study_Area__c> newMapStudy = studyList.size()>0 ? new Map<Id,CORE_Study_Area__c>(studyList) : new Map<Id,CORE_Study_Area__c>();

        List<User> owners = [SELECT Id,Profile.Name FROM User WHERE Id IN :getAllOwner];
        Map<Id,User> newMapOwner = new Map<Id,User>(owners);


        for(Opportunity currentRecord : newRecords){
            boolean changeOpportunityName = false;
            Opportunity currentOldRecord = myMapOldRecord.get(currentRecord.Id);
            User owner = newMapOwner.get(currentRecord.OwnerId);
            
            if(currentRecord.CORE_OPP_Business_Hours__c!=null){
                if(currentRecord.CORE_OPP_First_Outbound_Contact_Date__c!=null && currentOldRecord.CORE_OPP_First_Outbound_Contact_Date__c!=currentRecord.CORE_OPP_First_Outbound_Contact_Date__c){
                    Long diffBetween = System.BusinessHours.diff(currentRecord.CORE_OPP_Business_Hours__c, currentRecord.CreatedDate, currentRecord.CORE_OPP_First_Outbound_Contact_Date__c);

                    Integer seconds = (Integer)diffBetween / 1000;
                    Integer minutes = seconds / 60;
                    /*Integer hours = minutes / 60;
                    Integer days = hours / 24;
                    Integer hoursToShow = hours-days*24;
                    Integer minutesToShow = minutes-hours*60;
                    Integer secondsToShow = seconds-minutes*60;*/
                    currentRecord.CORE_OPP_First_Response_Time__c=minutes;
                }
            }
            if (currentRecord.CORE_Set_as_close_Lost__c == true) {
                currentRecord.StageName=CLOSED_LOST;
                currentRecord.CORE_Change_Status__c = true;
            }
            if (currentRecord.CORE_Set_as_close_Won__c == true) {
                currentRecord.StageName=CLOSED_WON;
                currentRecord.CORE_Change_Status__c = true;
            }
            if (currentRecord.CORE_New_Owner__c != null) {
                currentRecord.OwnerId = currentRecord.CORE_New_Owner__c;
                currentRecord.CORE_New_Owner__c=null;
            }
            if(currentRecord.OwnerId != null && currentRecord.OwnerId != currentOldRecord.OwnerId){
                currentRecord.CORE_ProfileOwner__c= owner.Profile.Name;
            }
            if (owner.Profile.Name != PROFILE_OWNER_CONDITION && currentRecord.First_Assignation_date__c==null) {
                currentRecord.First_Assignation_date__c=Datetime.Now();
            }
            if(currentRecord.CORE_TECH_Inbound_contact_counter__c!=currentOldRecord.CORE_TECH_Inbound_contact_counter__c){
                currentRecord.CORE_Last_returning_date__c=Datetime.Now();
            }
            if(currentRecord.CORE_TECH_Inbound_contact_counter__c>=2 && currentRecord.CORE_ReturningLead__c==false){
                currentRecord.CORE_Last_returning_date__c=Datetime.Now();
                currentRecord.CORE_ReturningLead__c=true;
            }
            if(currentRecord.CORE_ReturningLead__c ==true && currentRecord.CORE_ReturningLead__c!=currentOldRecord.CORE_ReturningLead__c){
                currentRecord.CORE_First_Returning_Date__c=Datetime.Now();
            }

            CORE_Curriculum__c myCurriculum = newMapCurriculum.get(currentRecord.CORE_OPP_Curriculum__c);
            CORE_Business_Unit__c myBusiness = newMapBusiness.get(currentRecord.CORE_OPP_Business_Unit__c);
            CORE_Division__c myDivision = newMapDivision.get(currentRecord.CORE_OPP_Division__c);
            CORE_Intake__c myIntake = newMapIntake.get(currentRecord.CORE_OPP_Intake__c);
            CORE_Level__c myLevel = newMapLevel.get(currentRecord.CORE_OPP_Level__c);
            CORE_Study_Area__c myStudy = newMapStudy.get(currentRecord.CORE_OPP_Study_Area__c);
			
            if(currentRecord.CORE_OPP_Division__c!=currentOldRecord.CORE_OPP_Division__c){
                currentRecord.CORE_OPP_TDivision__c= myDivision?.Name;
                changeOpportunityName=true;
            }
            if(currentRecord.CORE_OPP_Business_Unit__c!=currentOldRecord.CORE_OPP_Business_Unit__c){
                currentRecord.CORE_OPP_TBusiness_Unit__c= myBusiness?.Name;
                currentRecord.CORE_BU_Brand__c = myBusiness?.CORE_Brand__c;	
                changeOpportunityName=true;
            }
            if(String.IsEmpty(currentRecord.CORE_BU_Brand__c)){	
                currentRecord.CORE_BU_Brand__c = myBusiness?.CORE_Brand__c;	
            }
            if(currentRecord.CORE_OPP_Curriculum__c!=currentOldRecord.CORE_OPP_Curriculum__c){
                currentRecord.CORE_OPP_TCurriculum__c= myCurriculum?.CORE_Curriculum_Name__c;
                changeOpportunityName=true;
            }
            if(currentRecord.CORE_OPP_Study_Area__c!=currentOldRecord.CORE_OPP_Study_Area__c){
                currentRecord.CORE_OPP_TStudy_Area__c= myStudy?.Name;
                changeOpportunityName=true;
            }
            if(currentRecord.CORE_OPP_Level__c!=currentOldRecord.CORE_OPP_Level__c){
                currentRecord.CORE_OPP_TLevel__c= myLevel?.Name;
                changeOpportunityName=true;
            }
            if(currentRecord.CORE_OPP_Intake__c!=currentOldRecord.CORE_OPP_Intake__c){
                currentRecord.CORE_OPP_TIntake__c= myIntake?.Name;
                changeOpportunityName=true;
            }
            if(currentRecord.CORE_Main_Product_Interest__c!=currentOldRecord.CORE_Main_Product_Interest__c){
                changeOpportunityName=true;
            }
            if(changeOpportunityName){
                currentRecord.Name = getOpportunityName(currentRecord);
            }
        }

        reachLeadDateProcess(oldRecords, newRecords);
    }

    @TestVisible
    private static void reachLeadDateProcess(List<Opportunity> oldRecords,List<Opportunity> newRecords){
        List<String> status = new List<String> {'Lead'};
        Map<Id, Opportunity> oldOpportunities = new Map<Id,Opportunity> (oldRecords);
        Set<Id> opportunityIds = new Set<Id>();
        for(Opportunity newOpp : newRecords){
            opportunityIds.add(newOpp.Id);
        }
        List<CORE_Status_Timestamp_History__c> statusTimeStamps = [Select Id, CORE_Modification_Status_Date_time__c, CORE_Status__c, CORE_Opportunity__c 
            FROM CORE_Status_Timestamp_History__c
            WHERE CORE_Opportunity__c IN :opportunityIds
            AND CORE_Status__c = 'Applicant'];

        Map<Id, CORE_Status_Timestamp_History__c> timestampByOpportunity = new Map<Id,CORE_Status_Timestamp_History__c>();
        for(CORE_Status_Timestamp_History__c timestamp : statusTimeStamps){
            timestampByOpportunity.put(timestamp.CORE_Opportunity__c,timestamp);
        }
     
        for(Opportunity newOpp : newRecords){
            Opportunity oldOpp = oldOpportunities.get(newOpp.Id);
            Boolean isFieldUpdated = false;
            if(oldOpp.CORE_TECH_Inbound_contact_counter__c != newOpp.CORE_TECH_Inbound_contact_counter__c && newOpp.CORE_TECH_Inbound_contact_counter__c>=1 && newOpp.CORE_Reach_Date__c == null){
                newOpp.CORE_Reach_Date__c = newOpp.LastModifiedDate;
                newOpp.CORE_Reached_Lead__c = true;
                isFieldUpdated = true;
            }
            
            if(!isFieldUpdated && isStatusChanged(oldOpp,newOpp)){
                System.debug('Update reach date ?');
                if(status.contains(newOpp.StageName) && oldOpp.CORE_Reach_Date__c != null){
                    System.debug('Update reach date1 ?');

                    newOpp.CORE_Reach_Date__c = newOpp.LastModifiedDate;
                    isFieldUpdated = true;
                } else if(LIST_STATUS.contains(newOpp.StageName) && newOpp.CORE_Reach_Date__c == null){
                    System.debug('Update reach date 2?');
                    if(timestampByOpportunity.get(newOpp.Id)?.CORE_Modification_Status_Date_time__c == null){
                        newOpp.CORE_Reach_Date__c = System.now();
                    } else {
                        newOpp.CORE_Reach_Date__c = timestampByOpportunity.get(newOpp.Id).CORE_Modification_Status_Date_time__c;
                    }
                    //newOpp.CORE_Reach_Date__c = timestampByOpportunity.get(newOpp.Id) == null ? System.now() : timestampByOpportunity.get(newOpp.Id).CORE_Modification_Status_Date_time__c;
                    isFieldUpdated = true;
                }
            }
            if(!isFieldUpdated && isSubStatusChanged(oldOpp,newOpp) && newOpp.CORE_Reach_Date__c == null && LIST_SUB_STATUS.contains(newOpp.CORE_OPP_Sub_Status__c)){
                newOpp.CORE_Reach_Date__c = newOpp.LastModifiedDate;
                newOpp.CORE_Reached_Lead__c = true;
            }
        }
    }

    @TestVisible
    private static void afterDelete(List<Opportunity> oldRecords) {
        List<CORE_Deleted_record__c> deletedRecordsToUpsert = new List<CORE_Deleted_record__c>();
        Set<Id> recordTypeIds = new Set<Id>();

        for(Opportunity currentRecord : oldRecords){
            recordTypeIds.add(currentRecord.RecordTypeId);
        }

        Map<Id, String> recordTypeDevNames = CORE_RecordTypeUtils.getDeveloperNameByIds('Opportunity', recordTypeIds);

        for(Opportunity currentRecord : oldRecords){
            CORE_Deleted_record__c deletedRecord = new CORE_Deleted_record__c(
                CORE_Deletion_Date__c = System.now(),
                CORE_Object_Name__c = DELETED_OBJECT_TYPE,
                CORE_Record_Id__c = currentRecord.Id,
                CORE_Record_Type__c = recordTypeDevNames.get(currentRecord.RecordTypeId)
            );
            
            deletedRecordsToUpsert.add(deletedRecord);
        }

        if(!deletedRecordsToUpsert.isEmpty()){
            Upsert deletedRecordsToUpsert CORE_Deleted_record__c.fields.CORE_Record_Id__c;
        }
    }

    @TestVisible
    public static void afterInsert(List<Opportunity> newRecords) {
        List<Opportunity> oppsWithExternalIds = getOpportunitiesWhithExternalIds(newRecords);

        if(!oppsWithExternalIds.isEmpty()){
            Set<Id> opportunityIds = (new Map<Id, Opportunity> (oppsWithExternalIds)).keySet();
            CORE_ServiceAppointmentProcess.execute(opportunityIds);
        }
    }

    @TestVisible
    public static void afterUpdate(List<Opportunity> oldRecords, List<Opportunity> newRecords) {
        if(fireupdateTasks){
            updateTasks(oldRecords, newRecords);
            fireupdateTasks = false;
        }

        if(fireCreateChangeInterestHistory){
            createChangeInterestHistory(oldRecords, newRecords);
            fireCreateChangeInterestHistory = false;
        }
    
        if(fireUpdateOpportunityTechFields){
            updateOpportunityTechFields(newRecords);
            fireUpdateOpportunityTechFields = false;
        }
    }

    private static void updateOpportunityTechFields(List<Opportunity> opportunities){
        List<Opportunity> oppsToUpdate = new List<Opportunity>();

        for(Opportunity opportunity : opportunities){
            if(opportunity.CORE_Change_Status__c == true){
                Opportunity oppTmp = new Opportunity(Id = opportunity.Id);
                oppTmp.CORE_Change_Status__c = false;
                oppsToUpdate.add(oppTmp);
            }
        }
        
        if(!oppsToUpdate.isEmpty()){
            CORE_TriggerUtils.setBypassTrigger();
            Update oppsToUpdate;
            CORE_TriggerUtils.setBypassTrigger();
        }
    }

    private static Boolean isInterestChanged(Opportunity oldRecord, Opportunity newRecord){
        if(newRecord.Study_mode__c!=oldRecord.Study_mode__c){
            return true;
        }
        if(newRecord.CORE_OPP_Curriculum__c!=oldRecord.CORE_OPP_Curriculum__c){
            return true;
        }
        if(newRecord.CORE_OPP_Business_Unit__c!=oldRecord.CORE_OPP_Business_Unit__c){
            return true;
        }
        if(newRecord.CORE_OPP_Division__c!=oldRecord.CORE_OPP_Division__c){
            return true;
        }
        if(newRecord.CORE_OPP_Intake__c!=oldRecord.CORE_OPP_Intake__c){
            return true;
        }
        if(newRecord.CORE_OPP_Level__c!=oldRecord.CORE_OPP_Level__c){
            return true;
        }
        if(newRecord.CORE_OPP_Study_Area__c!=oldRecord.CORE_OPP_Study_Area__c){
            return true;
        }
        if(newRecord.CORE_Main_Product_Interest__c!=oldRecord.CORE_Main_Product_Interest__c){
            return true;
        }

        return false;
    }

    private static void createChangeInterestHistory(List<Opportunity> oldRecords, List<Opportunity> newRecords){
        Map <Id,Opportunity> myMapOldRecord = new Map<Id,Opportunity>(oldRecords);
        List<CORE_Change_Interest_History__c> changeHistories = new List<CORE_Change_Interest_History__c>();

        for(Opportunity currentRecord : newRecords){
            Opportunity currentOldRecord = myMapOldRecord.get(currentRecord.Id);

            boolean createChangeHistory = isInterestChanged(currentOldRecord,currentRecord);
            
            if(createChangeHistory){
                CORE_Change_Interest_History__c changeInterestHistory = new CORE_Change_Interest_History__c();
                changeInterestHistory.CORE_Date_of_interest_change__c=currentRecord.LastModifiedDate;
                changeInterestHistory.CORE_Curriculum__c = currentRecord.CORE_OPP_Curriculum__c;
                changeInterestHistory.CORE_Former_Curriculum__c = currentOldRecord.CORE_OPP_Curriculum__c;
                changeInterestHistory.CORE_Intake__c = currentRecord.CORE_OPP_Intake__c;
                changeInterestHistory.CORE_Former_Intake__c = currentOldRecord.CORE_OPP_Intake__c;
                changeInterestHistory.CORE_Level__c = currentRecord.CORE_OPP_Level__c;
                changeInterestHistory.CORE_Former_Level__c = currentOldRecord.CORE_OPP_Level__c;
                changeInterestHistory.CORE_Study_Area__c = currentRecord.CORE_OPP_Study_Area__c;
                changeInterestHistory.CORE_Former_Study_Area__c = currentOldRecord.CORE_OPP_Study_Area__c;
                changeInterestHistory.CORE_Main_Product_Interest__c = currentRecord.CORE_Main_Product_Interest__c;
                changeInterestHistory.CORE_Former_Main_Product_Interest__c = currentOldRecord.CORE_Main_Product_Interest__c;
                changeInterestHistory.CORE_Opportunity__c = currentRecord.Id;
                changeInterestHistory.CORE_Opportunity_Status__c = currentOldRecord.StageName;
                changeInterestHistory.CORE_User_who_has_changed_the_interest__c=currentRecord.LastModifiedById;
                changeInterestHistory.CORE_Study_mode_value__c = currentRecord.Study_mode__c;
                changeInterestHistory.CORE_Former_Study_mode_value__c=currentOldRecord.Study_mode__c;
                changeHistories.add(changeInterestHistory);
            }
        }

        if(!changeHistories.isEmpty()){
            insert changeHistories;
        }
    }

    private static void updateTasks(List<Opportunity> oldRecords, List<Opportunity> newRecords){
        Map<Id,Opportunity> oldMapOpportunities = new Map<Id,Opportunity>(oldRecords);
        Set<Id> listToCancelTask = new Set<Id>();
        
        Map<Id, Task> tasksToUpdateById = new Map<Id,Task>();

        for(Opportunity currentRecord : newRecords){
            Opportunity oldRecord = oldMapOpportunities.get(currentRecord.Id);
            if(oldRecord.StageName != currentRecord.stageName && (currentRecord.stageName==CLOSED_WON || currentRecord.stageName==CLOSED_LOST || currentRecord.stageName==WITHDRAW)){
                listToCancelTask.add(currentRecord.Id);
            }
        }

        List<Task> taskToCancelled = [SELECT Id,Status,WhatId FROM Task WHERE IsClosed = false AND WhatId IN :listToCancelTask];
        for(Task currentTask : taskToCancelled){
            currentTask.Status = TASK_CANCELLED;
            tasksToUpdateById.put(currentTask.Id,currentTask);
        }
        //update taskToCancelled;

        // FAIT AUTOMATIQUEMENT PAR SALESFORCE ?? 
        Set<Id> listToUpdateTask = new Set<Id>();
        Map<Id,Opportunity> newMapOpportunities = new Map<Id,Opportunity>(newRecords);
        for(Opportunity currentRecord : newRecords){
            Opportunity oldRecord = oldMapOpportunities.get(currentRecord.Id);
            if(currentRecord.OwnerId !=oldRecord.OwnerId){
                listToUpdateTask.add(currentRecord.Id);
            }
        }

        List<Task> tasksToUpdate = new List<Task>();

        if(!listToUpdateTask.isEmpty()){
            List<Task> taskToUpdateTmp = [SELECT Id,OwnerId,WhatId FROM Task WHERE WhatId IN :listToUpdateTask AND IsClosed = false];
            for(Task currentTask : taskToUpdateTmp){
                Task taskToUpdate = currentTask;
                if(tasksToUpdateById.containsKey(currentTask.Id)){
                    taskToUpdate = tasksToUpdateById.get(currentTask.Id);
                }
                if(currentTask.OwnerId==oldMapOpportunities.get(currentTask.WhatId).OwnerId){
                    taskToUpdate.OwnerId = newMapOpportunities.get(taskToUpdate.WhatId).OwnerId;
                }
                tasksToUpdateById.put(taskToUpdate.Id,taskToUpdate);
            }

            for(Id taskId : tasksToUpdateById.keySet()){
                tasksToUpdate.add(tasksToUpdateById.get(taskId));
            }
        }

        if(!tasksToUpdate.isEmpty()){
            update tasksToUpdate;
        }
    }

    public static List<Opportunity> getOpportunitiesWithChangedStatus(Map<Id,Opportunity> oldMapOpportunities, List<Opportunity> newOpportunities){
        List<Opportunity> opportunitiesWithChanges = new List<Opportunity>();

        for(Opportunity newOpportunity : newOpportunities){
            Opportunity oldOpportunity = oldMapOpportunities.get(newOpportunity.Id);

            if(oldOpportunity == null || newOpportunity.StageName != oldOpportunity.StageName){
                opportunitiesWithChanges.add(newOpportunity);
            }
        }

        return opportunitiesWithChanges;
    }

    public static List<Opportunity> getOpportunitiesWithChangedSubStatus(Map<Id,Opportunity> oldMapOpportunities, List<Opportunity> newOpportunities){
        List<Opportunity> opportunitiesWithChanges = new List<Opportunity>();

        for(Opportunity newOpportunity : newOpportunities){
            Opportunity oldOpportunity = oldMapOpportunities.get(newOpportunity.Id);

            if(newOpportunity.CORE_OPP_Sub_Status__c != oldOpportunity.CORE_OPP_Sub_Status__c){
                opportunitiesWithChanges.add(newOpportunity);
            }
        }

        return opportunitiesWithChanges;
    }

    public static List<Opportunity> getOpportunitiesWhithoutDeferal(List<Opportunity> opportunities){
        List<Opportunity> oppsWithoutDeferal = new List<Opportunity>();
        for(Opportunity opportunity : opportunities){
            if(opportunity.CORE_IsFromDeferal__c != true && opportunity.CORE_AOL_Re_Orientation__c != true && opportunity.CORE_Re_Orientation_Internally__c != true){
                oppsWithoutDeferal.add(opportunity);
            }
        }

        return oppsWithoutDeferal;
    }

    @TestVisible
    private static List<Opportunity> getOpportunitiesWhithExternalIds(List<Opportunity> opportunities){
        List<Opportunity> oppsWithExternalIds = new List<Opportunity>();
        for(Opportunity opportunity : opportunities){
            if(!String.isEmpty(opportunity.CORE_Lead_Source_External_Id__c)){
                oppsWithExternalIds.add(opportunity);
            }
        }

        return oppsWithExternalIds;
    }

    private static String getOpportunityName(Opportunity opportunity){
        String name = '';
        String businessUnit = opportunity.CORE_OPP_TBusiness_Unit__c;
        String studyArea = opportunity.CORE_OPP_TStudy_Area__c;
        String curriculum = opportunity.CORE_OPP_TCurriculum__c;
        String intake = opportunity.CORE_OPP_TIntake__c;
        
        if(!String.isEmpty(businessUnit) && !String.isEmpty(curriculum) && !String.isEmpty(intake)){
            name =  businessUnit + ' - ' + curriculum + ' - ' + intake;
        } else if (!String.isEmpty(businessUnit) && !String.isEmpty(curriculum)){
            name =  businessUnit + ' - ' + curriculum;
        } else if (!String.isEmpty(businessUnit) && !String.isEmpty(studyArea)){
            name =  businessUnit + ' - ' + studyArea;
        } else if (!String.isEmpty(businessUnit)) {
            name =  businessUnit + ' - ' + OPPORTUNITY_DEFAULT_NAME;
        } else {
            name =  OPPORTUNITY_DEFAULT_NAME;
        }

        if(name.length() > 120){
            name = name.substring(0,119);
        }
        return name;
    }
}