@isTest
private class CORE_OnlineShop_Opportunity_Update_TEST {

    private static final String INTEREST_EXTERNALID = 'setup';
    private static final String ACADEMIC_YEAR = 'CORE_PKL_2023-2024';

    @testSetup 
    static void createData(){
        Id profileId = [SELECT Id FROM Profile WHERE Name = 'CORE_API_Profile' LIMIT 1].Id;

        User usr = new User(
            Username = 'usertest@onlineshop.com',
            LastName = 'TEST_USER',
            Email = 'testemail@yopmail.com',
            Alias = 'T', 
            CommunityNickname = 'T',
            TimeZoneSidKey = 'Europe/Paris',
            LocaleSidKey = 'fr_FR_EURO',
            EmailEncodingKey = 'ISO-8859-1', 
            ProfileId = profileId,
            LanguageLocaleKey = 'fr'      
        );
        database.insert(usr, true);

        CORE_CatalogHierarchyModel catalogHierarchy = new CORE_DataFaker_CatalogHierarchy(INTEREST_EXTERNALID).catalogHierarchy;
        Account account = CORE_DataFaker_Account.getStudentAccount(INTEREST_EXTERNALID);

        Id oppEnroll_RTId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get(CORE_AOL_Constants.OPP_RECORDTYPE_ENROLLMENT).getRecordTypeId();

        Opportunity opp1 = new Opportunity(
            Name ='test',
            StageName = CORE_AOL_Constants.PKL_OPP_STAGENAME_PROSPECT,
            CORE_OPP_Sub_Status__c = CORE_AOL_Constants.PKL_OPP_SUBSTATUS_PROSPECTAPPSTARTED,
            CORE_OPP_Division__c = catalogHierarchy.division.Id,
            CORE_OPP_Business_Unit__c = catalogHierarchy.businessUnit.Id,
            CORE_OPP_Study_Area__c = catalogHierarchy.studyArea.Id,
            CORE_OPP_Curriculum__c = catalogHierarchy.curriculum.Id,
            CORE_OPP_Level__c = catalogHierarchy.level.Id,
            CORE_OPP_Intake__c = catalogHierarchy.intake.Id,
            CORE_OPP_Academic_Year__c = ACADEMIC_YEAR,
            AccountId = account.Id,
            CloseDate = Date.Today().addDays(2),
            OwnerId = UserInfo.getUserId(),
            RecordTypeId = oppEnroll_RTId,
            CORE_OPP_OnlineShop_Id__c = '12345'
        );

        database.insert(new List<Opportunity> { opp1 }, true);

    }

    @isTest
    static void test_UpdateOpp(){
        User usr = [SELECT Id FROM User WHERE Username = 'usertest@onlineshop.com' LIMIT 1];

        List<PermissionSetAssignment> psaList = new List<PermissionSetAssignment>();

        for(PermissionSet ps : [SELECT Id FROM PermissionSet WHERE Name = 'CORE_API_SIS' OR Name = 'CORE_LeadFormAcquisition']){
            psaList.add(
                new PermissionSetAssignment(AssigneeId = usr.id, PermissionSetId = ps.Id)
            );
        }
        
        database.insert(psaList, true);

        System.runAs(usr){

            String reqBody = '{'
            + '"CORE_OPP_OnlineShop_Id__c": "12345",'
            + '"CORE_OPP_Application_Online__c": true,'
            + '"CORE_OPP_Application_Online_Advancement__c": 99,'
            + '"CORE_OPP_Application_started__c": "2022-03-10T12:57:23.364Z"'
            + '}';

            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
            req.requestURI = '/services/apexrest/online-shop/opportunity';

            req.httpMethod = 'PUT';
            req.requestBody = Blob.valueOf(reqBody);

            RestContext.request = req;
            RestContext.response= res;

            test.startTest();
            CORE_OnlineShop_Wrapper.ResultWrapperOpportunity results = CORE_OnlineShop_Opportunity_Update.updateOpportunity();
            test.stopTest();

            System.assertEquals('12345', results.opportunity.CORE_OPP_OnlineShop_Id__c);
        }
    }
    @isTest
    static void test_UpdateOppFail(){
        User usr = [SELECT Id FROM User WHERE Username = 'usertest@onlineshop.com' LIMIT 1];

        List<PermissionSetAssignment> psaList = new List<PermissionSetAssignment>();

        for(PermissionSet ps : [SELECT Id FROM PermissionSet WHERE Name = 'CORE_API_SIS' OR Name = 'CORE_LeadFormAcquisition']){
            psaList.add(
                new PermissionSetAssignment(AssigneeId = usr.id, PermissionSetId = ps.Id)
            );
        }
        
        database.insert(psaList, true);

        System.runAs(usr){

            String reqBody = '{'
            + '"CORE_OPP_OnlineShop_Id__c": "",'
            + '"CORE_OPP_Application_Online__c": true,'
            + '"CORE_OPP_Application_Online_Advancement__c": 99'
            + '}';

            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
            req.requestURI = '/services/apexrest/online-shop/opportunity';

            req.httpMethod = 'PUT';
            req.requestBody = Blob.valueOf(reqBody);

            RestContext.request = req;
            RestContext.response= res;

            test.startTest();
            CORE_OnlineShop_Wrapper.ResultWrapperOpportunity results = CORE_OnlineShop_Opportunity_Update.updateOpportunity();
            test.stopTest();

            System.assertEquals(null, results.opportunity.CORE_OPP_OnlineShop_Id__c);
        }
    }
    @isTest
    static void test_UpdateOppFail2(){
        User usr = [SELECT Id FROM User WHERE Username = 'usertest@onlineshop.com' LIMIT 1];

        List<PermissionSetAssignment> psaList = new List<PermissionSetAssignment>();

        for(PermissionSet ps : [SELECT Id FROM PermissionSet WHERE Name = 'CORE_API_SIS' OR Name = 'CORE_LeadFormAcquisition']){
            psaList.add(
                new PermissionSetAssignment(AssigneeId = usr.id, PermissionSetId = ps.Id)
            );
        }
        
        database.insert(psaList, true);

        System.runAs(usr){

            String reqBody = '{'
            + '"CORE_OPP_OnlineShop_Id__c": "12345",'
            + '"CORE_OPP_Application_Online__c": true,'
            + '"CORE_OPP_Application_Online_Advancements__c": 99'
            + '}';

            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
            req.requestURI = '/services/apexrest/online-shop/opportunity';

            req.httpMethod = 'PUT';
            req.requestBody = Blob.valueOf(reqBody);

            RestContext.request = req;
            RestContext.response= res;

            test.startTest();
            CORE_OnlineShop_Wrapper.ResultWrapperOpportunity results = CORE_OnlineShop_Opportunity_Update.updateOpportunity();
            test.stopTest();

            System.assertEquals(null, results.opportunity.CORE_OPP_OnlineShop_Id__c);
        }
    }
}