@isTest
public without sharing class CORE_LogManagerCleanerSchedulerTest {
    @TestSetup
    static void makeData(){
        CORE_LOG__c log = new CORE_LOG__c(CORE_API_NAME__c = 'LeadForm' );
        insert log;
        CORE_LogManager manager = new CORE_LogManager('LeadForm');
        // put in a date before what's written in the Custom metadata type
        
        DateTime newCreatedDate = Date.today().addDays(- (Integer.ValueOf(manager.getCustomMetadataType().CORE_Time_Storage__c)+15));
        Test.setCreatedDate(log.Id, newCreatedDate);
    }

    @isTest
    public static void testScheduler() {
        Test.startTest();
        new CORE_LogManagerCleanerScheduler().execute(null);
        Test.stopTest();
        System.assert([SELECT Id FROM AsyncApexJob where JobType='BatchApex' and Status ='Completed'].size()>0);    
    }

}