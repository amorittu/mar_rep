/*************************************************************************************************************
 * @name			IT_DynamicLookupController
 * @author			Andrea Morittu <amorittu@atlantic-technologies.com>
 * @created			27 / 09 / 2022
 * @description		Dynamic controller of it_dynamicLookup
 *
 * Changes (version)
 * -----------------------------------------------------------------------------------------------------------
 * 				No.		Date			Author					Description
 * 				----	------------	--------------------	----------------------------------------------
 * @version		1.0		2022-09-27		Andrea Morittu			Changes desription
 *
**************************************************************************************************************/
public class IT_DynamicLookupController {
    
    public IT_DynamicLookupController() {

    }
    @AuraEnabled(cacheable=true)
    public static list<sObject> fetchLookupData(String searchKey , String sObjectApiName, Boolean useSharing) {    
        if ( useSharing) {
            return IT_DynamicLookupHelperWithSharing.fetchLookupData(searchKey, sObjectApiName);
        } else {
            return IT_DynamicLookupHelperWithoutSharing.fetchLookupData(searchKey, sObjectApiName);
        }
    }
    
    // Method to fetch lookup default value 
    @AuraEnabled
    public static sObject fetchDefaultRecord(string recordId , string sObjectApiName, Boolean useSharing) {
        if ( useSharing) {
            return IT_DynamicLookupHelperWithSharing.fetchDefaultRecord(recordId, sObjectApiName);
        } else {
            return IT_DynamicLookupHelperWithoutSharing.fetchDefaultRecord(recordId, sObjectApiName);
        }
    }

    public class SelectedRecordWrapper {
        @AuraEnabled
        public String selectedRecordId;

        @AuraEnabled
        public String selectedRecordName;
    }
}