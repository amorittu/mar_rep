/**
* @author Zameer Khodabocus - Almavia
* @date 2022-01-04
* @group Lightning Web Component
* @description Class used by lwc, coreMassOppAcademicYearConfirmation, to change academic year of Opportunities in mass
* @test class 
*/
public without sharing class CORE_LC_MassOppAcademicYearConfirmation {

    public static final String EMAIL_SETTING_NAME = 'Mass_Opportunity_AcademicYearConfirm';
    public static final Integer defaultBatchSize = 100;

    public class SettingWrapper {
        @AuraEnabled public String settingId { get; set; }
        @AuraEnabled public Integer batchSize { get; set; }
        @AuraEnabled public Boolean jobInProgress { get; set; }
        @AuraEnabled public String listViewFilter { get; set; }
    }

    public class ListViewWrapper {
        @AuraEnabled public String viewId { get; set; }
        @AuraEnabled public String viewLabel { get; set; }
        @AuraEnabled public String viewName { get; set; }
        @AuraEnabled public String viewQuery { get; set; }
    }

    public class CurrentViewWrapper {
        @AuraEnabled public String viewId { get; set; }
        @AuraEnabled public String viewQuery { get; set; }
        @AuraEnabled public Integer viewRecordsSize { get; set; }
    }

    public class BUWrapper {
        @AuraEnabled public String divId { get; set; }
        @AuraEnabled public String buId { get; set; }
        @AuraEnabled public String buName { get; set; }
    }

    public class PriceBookWrapper {
        @AuraEnabled public String pbId { get; set; }
        @AuraEnabled public String pbName { get; set; }
        @AuraEnabled public Integer numOpps { get; set; }
    }

    public class AsyncWrapper {
        public Set<Id> remOppsIdSet { get; set; }
        public List<String> resultsList { get; set; }
    }

    public CORE_LC_MassOppAcademicYearConfirmation(){}

    @RemoteAction
    public static List<ListViewWrapper> getListViewsInfo(){
        SettingWrapper orgSetting = getOrgSetting();
        //system.debug('@getListViews > orgSetting: ' + orgSetting);
        //system.debug('@getListViews > userSetting: ' + userSetting);

        List<ListViewWrapper> listViewsList = new List<ListViewWrapper>();
        String filter = '';
        String query = 'SELECT Id, Name, DeveloperName, SobjectType FROM ListView WHERE SobjectType = \'Opportunity\'';

        if(orgSetting.listViewFilter != null){
            filter = '%' + orgSetting.listViewFilter + '%';
            query += ' AND DeveloperName LIKE :filter';
        }

        //Get list views of object
        for(ListView lv : database.query(query)){
            ListViewWrapper newLV = new ListViewWrapper();

            newLV.viewId = lv.Id;
            newLV.viewLabel = lv.Name;
            newLV.viewName = lv.DeveloperName;

            HttpRequest req = new HttpRequest();
            req.setEndpoint('callout:CORE_MassOpportunityEndpoint' + '/Opportunity/listviews/' + lv.Id + '/describe');
            //req.setEndpoint('https://ggecs--mvpdev.my.salesforce.com/services/data/v50.0/sobjects/Opportunity/listviews/' + lv.Id + '/describe');
            req.setHeader('Authorization', 'Bearer ' + userinfo.getSessionId());
            req.setMethod('GET');
            Http http = new Http();
            HttpResponse res = http.send(req);

            if(res.getStatusCode() == 200){
                Map<String, Object> tokenRespMap = (Map<String, Object>)JSON.deserializeUntyped(res.getBody());

                newLV.viewQuery = (String)tokenRespMap.get('query');//.replaceAll('\'', '"');
            }

            listViewsList.add(newLV);
        }

        system.debug('@getOppListViews > listViewsList: ' + JSON.serializePretty(listViewsList));

        return listViewsList;
    }

    @AuraEnabled (cacheable=true)
    public static SettingWrapper getOrgSetting(){

        SettingWrapper settingWrap = new SettingWrapper();

        Core_MassOppConfirmAcademicYear__c orgsetting = Core_MassOppConfirmAcademicYear__c.getOrgDefaults();

        if(orgsetting.Id == null){
            AuraHandledException e = new AuraHandledException(Label.CORE_MAYC_MSG_NO_SETTINGS);
            e.setMessage(Label.CORE_MAYC_MSG_NO_SETTINGS);
            throw e;
        }

        settingWrap.settingId = orgsetting.Id;
        settingWrap.batchSize = (orgsetting.CORE_BatchSize__c != null) ? orgsetting.CORE_BatchSize__c.intValue() :defaultBatchSize;
        settingWrap.listViewFilter = orgsetting.CORE_ListViewNameFilter__c;
        settingWrap.jobInProgress = orgsetting.CORE_JobInProgress__c;

        return settingWrap;
    }

    /*@AuraEnabled (cacheable=true)
    public static List<ListViewWrapper> getListViews(){
        try {

            SettingWrapper orgSetting = getOrgSetting();
            //system.debug('@getListViews > orgSetting: ' + orgSetting);
            //system.debug('@getListViews > userSetting: ' + userSetting);

            List<ListViewWrapper> listViewsList = new List<ListViewWrapper>();
            String filter = '';
            String query = 'SELECT Id, Name, DeveloperName, SobjectType FROM ListView WHERE SobjectType = \'Opportunity\'';

            if(orgSetting.listViewFilter != null){
                filter = '%' + orgSetting.listViewFilter + '%';
                query += ' AND DeveloperName LIKE :filter';
            }

            //Get list views of object
            for(ListView lv : database.query(query)){
                ListViewWrapper newLV = new ListViewWrapper();

                newLV.viewId = lv.Id;
                newLV.viewLabel = lv.Name;
                newLV.viewName = lv.DeveloperName;

                listViewsList.add(newLV);
            }

            //system.debug('@getOppListViews > listViewsList: ' + JSON.serializePretty(listViewsList));

            return listViewsList;

        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }*/

    @AuraEnabled (cacheable=true)
    public static CurrentViewWrapper getListViewData(Object view) {
        system.debug('@getListViewData > view: ' + view);

        try{
            Map<String, Object> viewsMap = (Map<String, Object>)JSON.deserializeUntyped(JSON.serialize(view));
            system.debug('@getListViewData > viewsMap: ' + viewsMap);

            CurrentViewWrapper newCurrentView = new CurrentViewWrapper();
            newCurrentView.viewId = (String)viewsMap.get('value');
            newCurrentView.viewQuery = ((String)viewsMap.get('query')).unescapeHtml4();
            newCurrentView.viewRecordsSize = database.query(newCurrentView.viewQuery).size();

            return newCurrentView;
        }
        catch(Exception e){
            throw new AuraHandledException(e.getMessage() + ' => ' + e.getStackTraceString());
        }
    }

    /*@AuraEnabled (cacheable=true)
    public static CurrentViewWrapper getListViewData(String viewId) {

	    HttpRequest req = new HttpRequest();
        req.setEndpoint('callout:CORE_MassOpportunityEndpoint' + '/Opportunity/listviews/' + viewId + '/describe');
        req.setMethod('GET');
        Http http = new Http();
        HttpResponse res = http.send(req);

        if(res.getStatusCode() == 200){
            Map<String, Object> tokenRespMap = (Map<String, Object>)JSON.deserializeUntyped(res.getBody());

            String query = ((String)tokenRespMap.get('query'));//.toUpperCase();
            /*Set<String> columnsSet = new Set<String>();

            for(Object col : (List<Object>)tokenRespMap.get('columns')){
                Map<String, Object> colsMap = (Map<String, Object>)JSON.deserializeUntyped((String)JSON.serialize(col));
                columnsSet.add(((String)colsMap.get('fieldNameOrPath')).toUpperCase());
            }*

            CurrentViewWrapper newCurrentView = new CurrentViewWrapper();
            newCurrentView.viewId = viewId;
            newCurrentView.viewQuery = query;
            newCurrentView.viewRecordsSize = database.query(query).size();

            return newCurrentView;
        }
        else{
            throw new AuraHandledException(res.getStatusCode() + ' => ' + res.getStatus());
        }
    }*/

    @AuraEnabled (cacheable=true)
    public static List<BUWrapper> getBUs(CurrentViewWrapper viewSelected) {
        system.debug('@getBUs > viewSelected: ' + viewSelected);

        List<BUWrapper> bulist = new List<BUWrapper>();
        Set<Id> divIdSet = new Set<Id>();

        if(!viewSelected.viewQuery.containsIgnoreCase('CORE_OPP_Division__c')){
            viewSelected.viewQuery = viewSelected.viewQuery.replace('SELECT', 'SELECT CORE_OPP_DIVISION__C,');
        }

        for(Opportunity opp : ((List<Opportunity>)database.query(viewSelected.viewQuery.unescapeHtml4()))){
            if(opp.CORE_OPP_Division__c != null){
                divIdSet.add(opp.CORE_OPP_Division__c);
            }
        }

        system.debug('@getBUs > divIdSet: ' + divIdSet);

        if(divIdSet.size() > 0){
            for(CORE_Business_Unit__c bu : [SELECT Id, Name, CORE_Parent_Division__c FROM CORE_Business_Unit__c WHERE CORE_Parent_Division__c IN :divIdSet]){
                BUWrapper buWrap = new BUWrapper();

                buWrap.divId = bu.CORE_Parent_Division__c;
                buWrap.buId = bu.Id;
                buWrap.buName = bu.Name;
                bulist.add(buWrap);
            }
        }

        return bulist;
    }

    @AuraEnabled (cacheable=true)
    public static List<PriceBookWrapper> getPriceBooks(CurrentViewWrapper viewSelected, String buId, String academicYear) {
        buId = buId.replaceAll('"', '');
        system.debug('@getPriceBooks > buId: ' + buId);
        system.debug('@getPriceBooks > academicYear: ' + academicYear);

        List<PriceBookWrapper> pblist = new List<PriceBookWrapper>();

        for(CORE_PriceBook_BU__c pbBU : [SELECT Id, Name, CORE_TECH_PriceBook_Name__c, CORE_Price_Book__c 
                                         FROM CORE_PriceBook_BU__c
                                         WHERE CORE_Business_Unit__c = :buId
                                         AND CORE_Academic_Year__c = :academicYear]){

            PriceBookWrapper pbWrap = new PriceBookWrapper();
            pbWrap.pbId = pbBU.CORE_Price_Book__c;
            pbWrap.pbName = pbBU.CORE_TECH_PriceBook_Name__c;
            pblist.add(pbWrap);
        }

        if(pblist.size() > 0){
            Map<Id, Opportunity> oppsMap = new Map<Id, Opportunity>(
                ((List<Opportunity>)database.query(viewSelected.viewQuery.unescapeHtml4()))
            );

            pblist[0].numOpps = [SELECT Id FROM Opportunity WHERE Id IN :oppsMap.keySet() AND CORE_OPP_Business_Unit__c = :buId].size();
        }

        return pblist;
    }

    @AuraEnabled
    public static string runChangeAcademicYear(CurrentViewWrapper viewSelected, String divId, String buId, String academicYear, String pbId){
        divId = divId.replaceAll('"', '');
        buId = buId.replaceAll('"', '');
        pbId = pbId .replaceAll('"', '');
        system.debug('@changeAcademicYear > viewSelected: ' + viewSelected);
        system.debug('@changeAcademicYear > buId: ' + buId);
        system.debug('@changeAcademicYear > academicYear: ' + academicYear);
        system.debug('@changeAcademicYear > pbId: ' + pbId);

        try {

            SettingWrapper setting = getOrgSetting();
            system.debug('@changeAcademicYear > setting: ' + setting);

            Map<Id, Opportunity> oppsMap = new Map<Id, Opportunity>(
                ((List<Opportunity>)database.query(viewSelected.viewQuery.unescapeHtml4()))
            );

            List<String> resultsList = new List<String> {
                String.valueOf(oppsMap.size()), '0', '0', ''
            };

            if(setting.batchSize >= viewSelected.viewRecordsSize){//Run sync. update
                changeAcademicYear(oppsMap.keySet(), divId, buId, academicYear, pbId, null, resultsList, true);

                return null;
            }
            else{//Run async. update

                database.update(new Core_MassOppConfirmAcademicYear__c(
                    Id = setting.settingId, CORE_JobInProgress__c = true
                ), true);

                System.enqueueJob(new CORE_QU_MassOppAcademicYearConfirmation(oppsMap.keySet(), divId, buId, academicYear, pbId, resultsList));

                return 'ASYNC';
            }
            
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    public static AsyncWrapper changeAcademicYear(Set<Id> oppsIdSet, String divId, String buId, 
                                                  String academicYear, String pbId, Integer batchSize,
                                                  List<String> resultsList, Boolean sendEmail){
                                                 
        Set<Id> remOppsIdSet = oppsIdSet;
        List<CORE_AcademicYearProcessFlow.AcademicYearProcessParameters> paramsList = new List<CORE_AcademicYearProcessFlow.AcademicYearProcessParameters>();

        String query = 'SELECT Id, AccountId, CORE_Main_Product_Interest__c, CORE_Main_Product_Interest__r.ProductCode, CORE_OPP_Division__c, CORE_OPP_Business_Unit__c'
                     + ' FROM Opportunity'
                     + ' WHERE Id IN :oppsIdSet';

        if(batchSize != null){
            query += ' LIMIT :batchSize';
        }

        for(Opportunity opp : database.query(query)){

            if(opp.CORE_OPP_Division__c == divId
               && opp.CORE_OPP_Business_Unit__c == buId){
                CORE_AcademicYearProcessFlow.AcademicYearProcessParameters param = new CORE_AcademicYearProcessFlow.AcademicYearProcessParameters();

                param.accountId = opp.AccountId;
                param.oppId = opp.Id;
                param.businessUnitId = buId;
                param.academicYear = academicYear;
                param.priceBookId = pbId;
                param.isAcademicYearConfirmed = true;
                param.mainInterestProdCode = (opp.CORE_Main_Product_Interest__c != null) ? opp.CORE_Main_Product_Interest__r.ProductCode :null;

                paramsList.add(param);

            }
            
            remOppsIdSet.remove(opp.Id);
        }

        paramsList[0].resultsList = resultsList;

        paramsList[0].resultsList = CORE_AcademicYearProcessFlow.updateOpportunity(paramsList);

        if(sendEmail){
            CORE_MassOpportunityResultHandler resultHandler = new CORE_MassOpportunityResultHandler();
            resultHandler.settingName = EMAIL_SETTING_NAME;
            resultHandler.resultsList = paramsList[0].resultsList;

            resultHandler.sendMail();
        }

        AsyncWrapper asynWrap = new AsyncWrapper();
        asynWrap.remOppsIdSet = remOppsIdSet;
        asynWrap.resultsList = paramsList[0].resultsList;

        return asynWrap;
    }
}