global class CORE_Flow_Consent_internalReference {
    @InvocableMethod(label='Return a list of consents with InternalReference field filled.')
    global static List<Results> getInternalReferences(List<Parameters> params){
        List<Results> result = new List<Results>();
        Results results = new Results();
        List<CORE_Consent__c> consentsResults = new List<CORE_Consent__c>();

        if (params != null) {
            Parameters param = params.get(0);
            List<CORE_Consent__c> consentsFromParams = param?.consents;

            if(consentsFromParams != null && !consentsFromParams.isEmpty()){
                for(Integer i = 0 ; i < consentsFromParams.size(); i++){
                    CORE_Consent__c consent = consentsFromParams.get(i);
                    consent.CORE_TECH_InternalReference__c = CORE_ConsentTriggerHandler.getInternalReference(consent, consent.CORE_Brand__c);
                    consentsResults.add(consent);
                }
            }
        }
        
        results.consentsWithInternalRefecences = consentsResults;
        result.add(results);
        return result;
    }

    global class Parameters {
        @InvocableVariable(required=true)
        global List<CORE_Consent__c> consents;
    }

    global class Results {
        @InvocableVariable(label='Consents with InternalReference field filled' required=true)
        global List<CORE_Consent__c> consentsWithInternalRefecences;
    }
   
}