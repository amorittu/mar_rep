@RestResource(urlMapping='/aol/opportunity/process/selection')
global class CORE_AOL_OpportunityProcessSelection {
    private static final String SUCCESS_MESSAGE = 'Opportunity update with updated selection process';
    @HttpPut
    global static CORE_AOL_Wrapper.ResultWrapperGlobal execute() {
        RestRequest	request = RestContext.request;

        // Initiating logs
        Long dt1 = DateTime.now().getTime();
        Long dt2;
        CORE_LogManager logSettings;
        CORE_LOG__c log = new CORE_LOG__c(CORE_API_Name__c = 'AOL');
        Boolean isErrorLog = false;
        logSettings = new CORE_LogManager('AOL');

        system.debug('request.requestBody.toString(): ' + request.requestBody.toString());

        Map<String, Object> paramsMap = (Map<String, Object>)JSON.deserializeUntyped(request.requestBody.toString());
        String aolExternalId = (String)paramsMap.get(CORE_AOL_Constants.PARAM_AOL_EXTERNALID);
        CORE_AOL_Wrapper.SelectionTest selectionTest = (CORE_AOL_Wrapper.SelectionTest)JSON.deserialize((String)JSON.serialize(paramsMap.get(CORE_AOL_Constants.PARAM_AOL_SELECTION)), CORE_AOL_Wrapper.SelectionTest.class);
        CORE_AOL_Wrapper.AOLProgressionFields aolProgressionFields = (CORE_AOL_Wrapper.AOLProgressionFields)JSON.deserialize((String)JSON.serialize(paramsMap.get(CORE_AOL_Constants.PARAM_AOL_PROG_FIELDS)), CORE_AOL_Wrapper.AOLProgressionFields.class);

        CORE_AOL_Wrapper.ResultWrapperGlobal result = checkMandatoryParameters(aolExternalId);

        try{
            if(result != null){
                return result;
            }
            CORE_AOL_GlobalWorker aolWorker = new CORE_AOL_GlobalWorker(aolProgressionFields,aolExternalId);
            Opportunity opportunity = aolWorker.aolOpportunity;
            if(opportunity == null){
                result = new CORE_AOL_Wrapper.ResultWrapperGlobal(
                    CORE_AOL_Constants.ERROR_STATUS, 
                    String.format(CORE_AOL_Constants.MSG_OPPORTUNITY_NOT_FOUND, new List<String>{aolExternalId})
                );
            } else {
                opportunity = opportunityProcessSelectionProcess(aolWorker,selectionTest);

                //set Additional fields to update
                CORE_AOL_GlobalWorker.setAdditionalFields(aolWorker.aolOpportunity, request.requestBody.toString());

                aolWorker.updateOppIsNeeded = true;
                aolWorker.updateAolProgressionFields();

                result = new CORE_AOL_Wrapper.ResultWrapperGlobal(CORE_AOL_Constants.SUCCESS_STATUS, SUCCESS_MESSAGE);
            }
            
        } catch(Exception e){
            result = new CORE_AOL_Wrapper.ResultWrapperGlobal(CORE_AOL_Constants.ERROR_STATUS, e.getMessage() + ' at ' + e.getStackTraceString());
        } finally{
            dt2 = DateTime.now().getTime();
            System.debug('CORE_AOL_AcademicDiplomaHistory(urlMapping=\'/aol/academic-diploma-history\') : T0TAL TIME OF EXECUTION = ' + (dt2-dt1) + ' ms');
            // creating log
            log.CORE_Processing_Duration__c =(dt2-dt1);
            log.CORE_HTTP_Status_Code__c = result.status == 'KO' ? 400 : result.status == 'OK' ? 200 : 500;
            log.CORE_Sent_Body__c = JSON.serialize(result);
            log.CORE_Received_Body__c =  request.requestBody.toString();
            
            if(!logSettings.isNone()){
                if(logSettings.isAllLogged()){ // is All logged
                    CORE_LogManager.createLog(log);
                }else if(logSettings.isErrorLogged() && isErrorLog){ // is Error
                    CORE_LogManager.createLog(log);
                }
            }
        }

        return result;
    }

    public static Opportunity opportunityProcessSelectionProcess(CORE_AOL_GlobalWorker aolWorker,
        CORE_AOL_Wrapper.SelectionTest selectionTest){


        if(!String.IsBlank(selectionTest.selectionTestTeacher)){
            aolWorker.aolOpportunity.CORE_OPP_Selection_Test_Teacher__c = selectionTest.selectionTestTeacher;
        }

        if(!String.IsBlank(selectionTest.selectionTestResult)){
            aolWorker.aolOpportunity.CORE_OPP_Selection_Test_Result__c = selectionTest.selectionTestResult;
        }

        if(!String.IsBlank(selectionTest.selectionStatus)){
            aolWorker.aolOpportunity.CORE_Selection_status__c = selectionTest.selectionStatus;
        }

        if(selectionTest.selectionProcessStartDate!=null){
            aolWorker.aolOpportunity.CORE_Selection_process_start_date__c = selectionTest.selectionProcessStartDate;
        }
        if(selectionTest.selectionProcessEndDate!=null){
            aolWorker.aolOpportunity.CORE_Selection_process_end_date__c = selectionTest.selectionProcessEndDate;
        }

        if(!String.IsBlank(selectionTest.selectionTestShowNoShow)){
            aolWorker.aolOpportunity.CORE_OPP_Selection_Test_Show_No_Show__c = selectionTest.selectionTestShowNoShow;
        }

        if(selectionTest.selectionTestNoShowCounter!=null){
            aolWorker.aolOpportunity.CORE_OPP_Selection_Test_No_Show_Counter__c = selectionTest.selectionTestNoShowCounter;
        }
        if(!String.IsBlank(selectionTest.selectionTestGrade)){
            aolWorker.aolOpportunity.CORE_OPP_Selection_Test_Grade__c = selectionTest.selectionTestGrade;
        }
        if(!String.IsBlank(selectionTest.selectionProcessStatus)){
            aolWorker.aolOpportunity.CORE_Selection_process_status__c = selectionTest.selectionProcessStatus;
        }
        if(!String.IsBlank(selectionTest.selectionProcessProgramLeader)){
            aolWorker.aolOpportunity.CORE_Selection_process_program_leader__c = selectionTest.selectionProcessProgramLeader;
        }
        if(selectionTest.skillTestStartDate!=null){
            aolWorker.aolOpportunity.CORE_Skill_test_start_date__c = selectionTest.skillTestStartDate;
        }
        if(selectionTest.selectionTestDate!=null){
            aolWorker.aolOpportunity.CORE_OPP_Selection_Test_Date__c = selectionTest.selectionTestDate;
        }
        if(!String.IsBlank(selectionTest.selectionProcessGrade)){
            aolWorker.aolOpportunity.CORE_Selection_Process_Grade__c = selectionTest.selectionProcessGrade;
        }
        if(!String.IsBlank(selectionTest.selectionProcessResult)){
            aolWorker.aolOpportunity.CORE_Selection_Process_Result__c = selectionTest.selectionProcessResult;
        }
        if(selectionTest.selectionTestNeeded != null){
            aolWorker.aolOpportunity.CORE_OPP_Selection_Test_Needed__c = selectionTest.selectionTestNeeded;
        }
        if(selectionTest.selectionProcessNeeded != null){
            aolWorker.aolOpportunity.CORE_OPP_Selection_Process_Needed__c = selectionTest.selectionProcessNeeded;
        }
        return aolWorker.aolOpportunity;
    }

    private static CORE_AOL_Wrapper.ResultWrapperGlobal checkMandatoryParameters(String aolExternalId){
        List<String> missingParameters = new List<String>();
        Boolean isMissingAolId = CORE_AOL_GlobalWorker.isAolExternalIdParameterMissing(aolExternalId);

        if(isMissingAolId){
            missingParameters.add(CORE_AOL_Constants.PARAM_AOL_EXTERNALID);
        }
        
        return CORE_AOL_GlobalWorker.getMissingParameters(missingParameters);
    }
}