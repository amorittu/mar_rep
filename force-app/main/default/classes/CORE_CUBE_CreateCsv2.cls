public with sharing class CORE_CUBE_CreateCsv2 implements Schedulable{

    public void execute(SchedulableContext ctx) {
        DateTime myLastSuccess= CORE_CUBE_CreateCsv.getLastSuccesDate();

        DateTime dtBeforeFileGeneration1 = System.now();
        String sa = CORE_CUBE_CreateCsv.getListStudyArea(myLastSuccess);
        DateTime dtAfterFileGeneration1 = System.now();

        DateTime dtBeforeFileGeneration2 = System.now();
        String cur = CORE_CUBE_CreateCsv.getListCurriculum(myLastSuccess);
        DateTime dtAfterFileGeneration2 = System.now();

        DateTime dtBeforeFileGeneration3 = System.now();
        String lvl = CORE_CUBE_CreateCsv.getListLevel(myLastSuccess);
        DateTime dtAfterFileGeneration3 = System.now();

        CORE_CUBE_CreateCsv.sendToEndpoint('Study_Area',sa, dtBeforeFileGeneration1, dtAfterFileGeneration1);
        CORE_CUBE_CreateCsv.sendToEndpoint('Curriculum',cur, dtBeforeFileGeneration2, dtAfterFileGeneration2);
        CORE_CUBE_CreateCsv.sendToEndpoint('Level',lvl, dtBeforeFileGeneration3, dtAfterFileGeneration3);
    }
}