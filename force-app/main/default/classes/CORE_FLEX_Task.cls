@RestResource(urlMapping='/twilio/Task/*')
global without sharing class CORE_FLEX_Task {
    @HttpPatch
    global static void execute() {
        RestResponse res = RestContext.response;
        res.addHeader('Content-Type', 'application/json');

        try{
            RestRequest req = RestContext.request;

            String jsonParam =req.requestbody.tostring();
            String taskId = req.requestURI.substring(req.requestURI.lastIndexOf('/')+1);
        
            if(String.isBlank(jsonParam)){
                throw new FlexException('Missing body parameters. Body must not be empty.');
            }
            if(String.isBlank(taskId) || taskId.startsWith('Task')){
                throw new FlexException('Missing task Id');
            }

            Task task = new Task(Id = taskId);
            Map<String, Object> cObjMap = (Map<String, Object>) JSON.deserializeUntyped(jsonParam);

            list<String> invalidFields = new List<String>();
            for(String fieldName : cObjMap.keySet()){
                if(!CORE_SobjectUtils.doesFieldExist('Task', fieldName)){
                    invalidFields.add(fieldName);
                } else {
                    task.put(fieldName, cObjMap.get(fieldName));
                }
            }
            
            if(!invalidFields.isEmpty()){
                throw new FlexException('Invalid fields : '+ invalidFields);
            }
            
            update task;

            res.statusCode = 200;
            //res.responseBody = Blob.valueOf(JSON.serialize(task));
        } catch (FlexException e){
            res.statusCode = 400;
            System.debug('e.getMessage() = '+ e.getMessage());
            res.responseBody = Blob.valueOf(JSON.serialize(new ErrorMessage(e.getMessage())));

        }
    }

    class FlexException extends Exception {}

    class ErrorMessage {
        private String message;
        private String status;

        public ErrorMessage(String message){
            this.status = 'Error';
            this.message = message;
        }
    }
}