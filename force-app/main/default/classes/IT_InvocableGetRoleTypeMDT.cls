/**
 * Created by ftrapani on 12/3/2021.
 */

global class IT_InvocableGetRoleTypeMDT {
    @InvocableMethod
    global static List<FlowOutputs> getRoleTypeMDT(List<FlowInputs> request){

        List<User> userList = [SELECT Id, UserRoleId, UserRole.DeveloperName FROM User WHERE Id = :request[0].userId];
        String roleType = '';
        if(userList[0].UserRole!=null){
            Map<String,IT_Role_Type__mdt> typeRoleMap = IT_Utility.getRoleTypeMDT2();
            roleType = typeRoleMap.get(userList[0].UserRole.DeveloperName)!=null && String.isNotBlank(typeRoleMap.get(userList[0].UserRole.DeveloperName).IT_Type__c) ? typeRoleMap.get(userList[0].UserRole.DeveloperName).IT_Type__c : '';
        }

        FlowOutputs flOutput= new FlowOutputs();
        flOutput.roleType = roleType;

        List<FlowOutputs> returnList = new List<FlowOutputs>();
        returnList.add(flOutput);
        return returnList;
    }

    global class FlowInputs{

        @InvocableVariable (required=true)
        global Id userId;

    }

    global class FlowOutputs{

        @InvocableVariable
        global String roleType;

    }


}