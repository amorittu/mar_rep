@RestResource(urlMapping='/aol/opportunity/status/enrolled/classical')
global without sharing class CORE_AOL_OPP_StatusEnrolled1 {
    private static final String SUCCESS_MESSAGE = 'Opportunity status, sub-status and progression fiedls updated with success';

    @HttpPut
    global static CORE_AOL_Wrapper.ResultWrapperGlobal execute(String aolExternalId,
                                    CORE_AOL_Wrapper.AOLProgressionFields aolProgressionFields) {

        
        // Initiating logs
        Long dt1 = DateTime.now().getTime();
        Long dt2;
        CORE_LogManager logSettings;
        CORE_LOG__c log = new CORE_LOG__c(CORE_API_Name__c = 'AOL');
        Boolean isErrorLog = false;
        logSettings = new CORE_LogManager('AOL');

        CORE_AOL_Wrapper.ResultWrapperGlobal result = checkMandatoryParameters(aolExternalId);

        try{
            if(result != null){
                //missing parameter(s)
                return result;
            }
            CORE_AOL_GlobalWorker aolWorker = new CORE_AOL_GlobalWorker(aolProgressionFields,aolExternalId);
            Opportunity opportunity = aolWorker.aolOpportunity;

            if(opportunity == null){
                result = new CORE_AOL_Wrapper.ResultWrapperGlobal(
                    CORE_AOL_Constants.ERROR_STATUS, 
                    String.format(CORE_AOL_Constants.MSG_OPPORTUNITY_NOT_FOUND, new List<String>{aolExternalId})
                );
            } else {
                aolWorker.aolOpportunity = getOppWithChangedStatus(aolWorker.aolOpportunity);
                aolWorker.updateOppIsNeeded = true;

                aolWorker.updateAolProgressionFields();
                result = new CORE_AOL_Wrapper.ResultWrapperGlobal(CORE_AOL_Constants.SUCCESS_STATUS, SUCCESS_MESSAGE);
            }

        } catch(Exception e){
            result = new CORE_AOL_Wrapper.ResultWrapperGlobal(CORE_AOL_Constants.ERROR_STATUS, e.getMessage() + ' at ' + e.getStackTraceString());
        } finally{
            dt2 = DateTime.now().getTime();
            System.debug('CORE_AOL_AcademicDiplomaHistory(urlMapping=\'/aol/academic-diploma-history\') : T0TAL TIME OF EXECUTION = ' + (dt2-dt1) + ' ms');
            // creating log
            log.CORE_Processing_Duration__c =(dt2-dt1);
            log.CORE_HTTP_Status_Code__c = result.status == 'KO' ? 400 : result.status == 'OK' ? 200 : 500;
            log.CORE_Sent_Body__c = JSON.serialize(result);
            log.CORE_Received_Body__c =  '{"aolExternalId" : "' + aolExternalId + '","aolProgressionFields" : ' + JSON.serialize(aolProgressionFields) + '}';
            
            if(!logSettings.isNone()){
                if(logSettings.isAllLogged()){ // is All logged
                    CORE_LogManager.createLog(log);
                }else if(logSettings.isErrorLogged() && isErrorLog){ // is Error
                    CORE_LogManager.createLog(log);
                }
            }
        }

        return result;
    }

    private static Opportunity getOppWithChangedStatus(Opportunity opportunity){
        opportunity.StageName = CORE_AOL_Constants.PKL_OPP_STAGENAME_ENROLLED;
        opportunity.CORE_OPP_Sub_Status__c = CORE_AOL_Constants.PKL_OPP_SUBSTATUS_ENROLLED_CLASSICAL;

        return opportunity;
    }

    private static CORE_AOL_Wrapper.ResultWrapperGlobal checkMandatoryParameters(String aolExternalId){
        List<String> missingParameters = new List<String>();
        Boolean isMissingAolId = CORE_AOL_GlobalWorker.isAolExternalIdParameterMissing(aolExternalId);

        if(isMissingAolId){
            missingParameters.add('aolExternalId');
        }
        
        return CORE_AOL_GlobalWorker.getMissingParameters(missingParameters);
    }
}