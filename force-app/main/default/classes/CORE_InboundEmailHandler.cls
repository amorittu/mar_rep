global class CORE_InboundEmailHandler implements Messaging.InboundEmailHandler {
    @TestVisible
    private static final String STUDENT_RECORDTYPE_DEVNAME = 'Core_Student';
    @TestVisible
    private static final String STUDENT_DEFAULT_LASTNAME = 'UNKNOWN';
    @TestVisible
    private static final String OPPORTUNITY_FIELDS_TO_QUERY = 'Id, Account.PersonContactId, IsClosed, CORE_OPP_Academic_Year__c, CORE_OPP_Business_Unit__c, CORE_Last_inbound_contact_date__c, LastModifiedDate';
    private static final String POC_INSERTION_MODE = 'CORE_PKL_Automatic';
    private static final String POC_NATURE = 'CORE_PKL_Online';
    private static final String POC_ENROLLMENT_CHANNEL = 'CORE_PKL_Direct';
    private static final String POC_CAPTURE_CHANNEL = 'CORE_PKL_Email';
    private id buSelectedId; // it will contains the id of the selected bu or null if not
	private boolean appearInRecipients = false;
    private String externalAttachmentsURL;
	private Datetime emailDatetime;

    global CORE_InboundEmailHandler(){}
	global CORE_InboundEmailHandler(String externalAttachmentsURL, Datetime emailDatetime){
		this.externalAttachmentsURL = externalAttachmentsURL;
		this.emailDatetime = emailDatetime;
	}

    global Messaging.InboundEmailResult handleInboundEmail(Messaging.inboundEmail email, Messaging.InboundEnvelope env){
        System.debug('CORE_InboundEmailHandler - handleInboundEmail START');
        CORE_Inbound_Email_Setting__mdt inboundEmailSettings = getInboundEmailSettings();
        List<String> toAddresses = validatedAddresses(email.toAddresses);
        List<String> ccAddresses = validatedAddresses(email.ccAddresses);
        
        // Create an InboundEmailResult object for returning the result of the 
        // Apex Email Service
        Messaging.InboundEmailResult result = new Messaging.InboundEmailResult();
        
        EmailMessage emailMessage = new EmailMessage();
        emailMessage.MessageIdentifier = email.messageId;
        emailMessage.Incoming = true;
        emailMessage.Status = '0'; // = new 
        emailMessage.IsClientManaged = true;
        
        String msgBody = (email.plainTextBody != null) ? email.plainTextBody :'';
        if(msgBody.length() > 32000){
            msgBody = msgBody.left(31000) + ' [...]';
        }
        
        String msgHtmlBody = (email.htmlBody != null) ? email.htmlBody :'';
        if(msgHtmlBody.length() > 32000){
            msgHtmlBody = msgHtmlBody.left(31000) + ' [...]';
        }
        
        emailMessage.HtmlBody = msgHtmlBody;
        emailMessage.TextBody = msgBody;
        
        String msgSubject = (email.subject != null) ? email.subject :'';
        if(msgSubject.length() > 4000){
            msgSubject = msgSubject.left(3993) + ' [...]';
        }
        emailMessage.Subject = email.subject;

        //emailMessage.ToAddress = email.toAddresses != null && email.toAddresses.size() > 0 ? email.toAddresses.get(0) : null;
        emailMessage.ToAddress = parseAddress(toAddresses);
    
        emailMessage.CcAddress = parseAddress(ccAddresses);
        
        emailMessage.FromAddress = email.fromAddress;
        emailMessage.FromName = email.fromName;
        Datetime emailDate;
        if(email.headers != null){
            emailMessage.headers = email.headers.toString();
            for(Messaging.InboundEmail.Header header : email.headers){
                if(header.name == 'Date'){
                    DateTime dateTmp;
                    try{
                        dateTmp = CORE_DataConvertion.convertSpecialStringFormatToDateTime(header.value);
                    } catch(Exception ex){
                        System.debug('Unable to convert ' + header.value + ' to datetime');
                    }
                    System.debug('dateTmp = ' + dateTmp);

                    this.emailDatetime = this.emailDatetime == null ? dateTmp : this.emailDatetime;
                    break;
                }
            }
            if(this.emailDatetime != null){
                List<EmailMessage> duplicatedEmailMessages = [SELECT Id,RelatedToId, ThreadIdentifier, MessageDate 
                FROM EmailMessage 
                WHERE MessageDate >= TODAY AND Subject = :emailMessage.Subject];

                if(!duplicatedEmailMessages.isEmpty()){
                    for(EmailMessage potentielDuplicate : duplicatedEmailMessages){
                        Datetime newDateTimeMinusOne = this.emailDatetime.addMinutes(-1);
                        System.debug('this.emailDatetime = '+ this.emailDatetime);
                        System.debug('newDateTimeMinusOne = '+ newDateTimeMinusOne);
                        if(potentielDuplicate.MessageDate.hour() == this.emailDatetime.hour() && 
                            (potentielDuplicate.MessageDate.minute() >= newDateTimeMinusOne.minute() 
                            && potentielDuplicate.MessageDate.minute() <= this.emailDatetime.minute()
                            )
                        ){
                            System.debug('Email already exist : it will not be integrated');
                            result.success = true;
                            return result;
                        }
                    }
                }
            }
            
        }
       
        emailMessage.RelatedToId = null;

        if(!String.IsBlank(email.inReplyTo)){
            String messageIdentifier = email.inReplyTo;
            

            List<EmailMessage> existingEmailMessages = [Select Id,RelatedToId, ThreadIdentifier from EmailMessage where MessageIdentifier = :messageIdentifier];
            if(!existingEmailMessages.isEmpty()){
                EmailMessage existingEmailMessage = existingEmailMessages.get(0);
                emailMessage.ReplyToEmailMessageId = existingEmailMessage.Id;
                emailMessage.ThreadIdentifier = existingEmailMessage.ThreadIdentifier;
                emailMessage.RelatedToId = existingEmailMessage.RelatedToId;
            }
        }
                
        EmailMessageRelation emailMessageRelationFrom = new EmailMessageRelation();
        
        
        String relatedToId = emailMessage.RelatedToId;
        
        String taskOwnerId;
        String emailMessageRelationFromRelationId;
        Boolean taskAlreadyCreated = false;
        
        //if we don't find an existing emailMessage link to an opportunity
        if(relatedToId == null || !relatedToId.startsWith('006')){
            List<String> internalDomains = getInternalDomains(inboundEmailSettings);
            String fromEmail = emailMessage.FromAddress;
            String fromDomain = fromEmail.substringAfterLast('@');
            System.debug('fromDomain = ' + fromDomain);
            System.debug('internalDomains = ' + internalDomains);

            Boolean isFromInternalUser = internalDomains.contains(fromDomain);
            System.debug('isFromInternalUser = ' + isFromInternalUser);
            EmailMessage resultEmailMessage;
            Boolean isNewAccount = false;
            if (!isFromInternalUser){
                resultEmailMessage = handleIncomingEmailFromExternalThirdParty(emailMessage);
            }
            if(resultEmailMessage != null ){
                emailMessage = resultEmailMessage;
                emailMessageRelationFromRelationId = emailMessage.CORE_TECH_WhoId__c;

            } else {
                Account relatedAccount = getRelatedAccount(emailMessage, isFromInternalUser);
                
                if(relatedAccount != null){
                    if(String.isEmpty(relatedAccount.Id)){
                        Insert relatedAccount;
                        relatedAccount = [Select Id, PersonContactId from account where Id = :relatedAccount.Id];
                        isNewAccount = true;
                    }
                    
                    emailMessage.RelatedToId = relatedAccount.Id;
                    emailMessageRelationFromRelationId = relatedAccount.PersonContactId;
                
                Opportunity opp = getRelatedOpportunity(relatedAccount.Id, inboundEmailSettings, toAddresses);
                
                if(opp != null){
                    //taskOwnerId = opp.OwnerId;
                    System.debug('inboundEmailSettings.CORE_AssignToOppOwner__c = ' + inboundEmailSettings.CORE_AssignToOppOwner__c);
                    if(inboundEmailSettings.CORE_AssignToOppOwner__c){
                        taskOwnerId = (String) opp.get(inboundEmailSettings.CORE_Custom_OPP_User_Field__c);
                        System.debug('taskOwnerId = ' + taskOwnerId);
                        
                        if(String.isEmpty(taskOwnerId)){
                            taskOwnerId = (String) opp.get('ownerid');
                            System.debug('owner taskOwnerId = ' + taskOwnerId);
                        }
                    }
                    
                    emailMessage.RelatedToId = opp.Id;
                    emailMessage.CORE_Opportunity__c = opp.Id ;// assign the opp Id
                }
            } else {
                Task taskCreated = createTaskForInternalDomains(emailMessage);
                taskAlreadyCreated = true;
                emailMessage.CORE_TECH_TaskId__c = taskCreated.Id;
                System.debug(emailMessage.CORE_TECH_TaskId__c);
            }
            
        }      
            if(String.isEmpty(taskOwnerId)){
                List<String> selectedAdresses = new List<String>();
                CORE_Business_Unit__c businessunit;
                if(resultEmailMessage != null || !inboundEmailSettings.CORE_Prioritary_Admission_Queue_Assign__c	|| !this.appearInRecipients){// first recipient
                    selectedAdresses.add(toAddresses[0]);// the first
                } else if(inboundEmailSettings.CORE_Prioritary_Admission_Queue_Assign__c && this.appearInRecipients){
                    businessunit = [SELECT Id,CORE_BU_Technical_User__c,CORE_Generic_Admission_contact_email__c FROM CORE_Business_Unit__c where Id=:this.buSelectedId];
                    selectedAdresses.add(businessunit.CORE_Generic_Admission_contact_email__c);
                }

                List<Group> queues = [Select Id from Group where Email IN :selectedAdresses and Type = 'Queue'];

                System.debug('queues = '+ queues);
                
                if(!queues.IsEmpty()){
                    taskOwnerId = queues.get(0).Id;
                }else{
                    taskOwnerId = (businessunit !=null) ? businessunit.CORE_BU_Technical_User__c : UserInfo.getUserId();
                }
            }
            
            
        } else {
            //get the owner of related opportunity for the task
            Opportunity opp = getOpportunityDatas(relatedToId, inboundEmailSettings);
            
            //taskOwnerId = opp.OwnerId;
            taskOwnerId = (String) opp.get(inboundEmailSettings.CORE_Custom_OPP_User_Field__c);
            if(String.isBlank(taskOwnerId)){
                taskOwnerId = (String) opp.get('ownerid');
            }
            emailMessageRelationFromRelationId = opp.Account.PersonContactId;
            emailMessage.CORE_Opportunity__c = opp.Id ;// assign opportunity Id
        }
        emailMessage.CORE_TECH_TaskOwnerId__c = taskOwnerId;

		//SGS-1366
		emailMessage.CORE_ExternalAttachmentURLs__c = (externalAttachmentsURL != null) ? externalAttachmentsURL :'';
		emailMessage.MessageDate = emailDatetime;
        
        insert emailMessage;
        System.debug('emailMessage.Id = '+ emailMessage.Id);
        
        List<EmailMessageRelation> emailMessageRelations = new List<EmailMessageRelation>();
        
        if(!String.isEmpty(emailMessageRelationFromRelationId)){
            emailMessageRelationFrom.RelationId = emailMessageRelationFromRelationId;
            emailMessageRelationFrom.RelationType = 'FromAddress';
            emailMessageRelationFrom.RelationAddress = emailMessage.FromAddress;
            emailMessageRelationFrom.EmailMessageId = emailMessage.Id;
    
            emailMessageRelations.add(emailMessageRelationFrom);
        }

        for(String toAddress : toAddresses){
            EmailMessageRelation emailMessageRelationTo = new EmailMessageRelation(
                EmailMessageId = emailMessage.Id,
                RelationType = 'ToAddress',
                relationAddress = toAddress
            );

            emailMessageRelations.add(emailMessageRelationTo);
        }
        
        insert emailMessageRelations;
        
        createContentDocumentLinks(email.binaryAttachments, emailMessage.Id);
        
        result.success = true;

        // Return the result for the Apex Email Service
        return result;
    }

    private Opportunity getOpportunityDatas(Id opportunityId,CORE_Inbound_Email_Setting__mdt inboundEmailSettings){
        String customField = inboundEmailSettings.CORE_Custom_OPP_User_Field__c.toLowerCase().trim().removeStart(',');
        customField = String.isEmpty(customField) ? 'ownerid' : customField;
        
        if(customField != 'ownerid'){
            customField += ',ownerid';
        }

        String fieldsToQuery = OPPORTUNITY_FIELDS_TO_QUERY + ', ' + customField;
        String oppQuery = 'Select ' + fieldsToQuery + ' FROM Opportunity where Id = :opportunityId';
        system.debug('oppQuery = ' + oppQuery);
        Opportunity opportunity = database.query(oppQuery);

        return opportunity;
    }

    @TestVisible
    private Task createTaskForInternalDomains(EmailMessage emailMessage){
        Task task = new Task(
            Status = 'CORE_PKL_Open',
            Subject = 'Email: ' + emailMessage.Subject,
            Description = emailMessage.TextBody,
            TaskSubtype = 'Email'
        );
        insert task;

        return task;
    }

    public void createContentDocumentLinks(Messaging.InboundEmail.BinaryAttachment[] binAttachList,
        Id insertedEmailMessageId) {
        List<ContentVersion>cvList = new List<ContentVersion>();
        List<ContentDocumentLink> cdlList = new List<ContentDocumentLink>();
        if (binAttachList == null) {
            return;
        }
        for (Messaging.InboundEmail.BinaryAttachment binAttach : binAttachList) {
            ContentVersion testContentInsert = new ContentVersion();
            testContentInsert.Title = binAttach.fileName;
            testContentInsert.VersionData = binAttach.body;
            testContentInsert.PathOnClient = '/' + binAttach.fileName ;
            cvList.add(testContentInsert);

        }
        insert cvList;
        cvList = [select id, ContentDocumentId from ContentVersion WHERE Id in :cvList];
        for (ContentVersion cv : cvList) {

            ContentDocumentLink cl = new ContentDocumentLink();
            cl.ContentDocumentId = cv.ContentDocumentId;
            cl.LinkedEntityId = insertedEmailMessageId;
            cl.ShareType = 'V';
            cl.Visibility = 'AllUsers';
            cdlList.add(cl);


        }
        insert cdlList;
    }

    @TestVisible
    private Opportunity getRelatedOpportunity(Id accountId, CORE_Inbound_Email_Setting__mdt inboundEmailSettings, List<String> toAddresses){
        Opportunity opportunity;

        if(String.isEmpty(accountId)){
            return null;
        }
        String customField = inboundEmailSettings.CORE_Custom_OPP_User_Field__c.toLowerCase().trim().removeStart(',');
        customField = String.isEmpty(customField) ? 'ownerid' : customField;
        
        if(customField != 'ownerid'){
            customField += ',ownerid';
        }

        String fieldsToQuery = OPPORTUNITY_FIELDS_TO_QUERY + ', ' + customField;
        String oppQuery = 'Select {0} FROM Opportunity ';
        oppQuery += 'WHERE AccountId = :accountId AND IsClosed = false {1} ';
        oppQuery += 'ORDER BY CORE_Last_inbound_contact_date__c, LastModifiedDate DESC';
        
        // check if there is only one opportunity in salesforce or noone or many related to this AccountID
        String oppsOfAccountIdformatedQuery = String.format(oppQuery, new List<String> {fieldsToQuery, ''});
        System.debug('oppsOfAccountIdformatedQuery = ' + oppsOfAccountIdformatedQuery);
        List<Opportunity> opportunitiesRelatedToPA = database.query(oppsOfAccountIdformatedQuery);
        if(opportunitiesRelatedToPA.size() == 1){
            Id buID = opportunitiesRelatedToPA[0].CORE_OPP_BUSINESS_UNIT__c;
            string areaCondition = '';
            for(String address : toAddresses){
                areaCondition += 'OR CORE_Generic_Area_contact_email__c like \'%' +  address + '%\' ';
            }

            list<CORE_Business_Unit__c> bus = database.query('SELECT Id FROM CORE_Business_Unit__c where Id =:buID ' +
            'AND (CORE_Generic_contact_email__c  IN :toAddresses OR ' +
            'CORE_Generic_Admission_contact_email__c IN :toAddresses OR ' +
            'CORE_Generic_Orientation_contact_email__c IN :toAddresses ' +
            areaCondition + ')');

            if(bus.size()!=0){
                return opportunitiesRelatedToPA[0];
            }

        //SGS-1754
        } else if(opportunitiesRelatedToPA.size() > 1) {
            Opportunity selectedOpportunity = selectOpportunityInManyOppCase(opportunitiesRelatedToPA,toAddresses);
            if(selectedOpportunity != null){
                return selectedOpportunity;
            }
        }
        // BU Selection process
        Id businessUnitId = buSelectionProcess(toAddresses);
        // a BU is selected ?
        // => No, return no opp
        if(String.IsEmpty(businessUnitId)){
            return null;
        }
        // yes it will be stored
        this.buSelectedId = businessUnitId;

        String currentAcademicYear = CORE_AcademicYearProcess.getAcademicYear();
        String currentAcademicYearCondition = String.isEmpty(currentAcademicYear) ? '' : ' AND CORE_OPP_Academic_Year__c = :currentAcademicYear';
        String otherConditions = 'AND CORE_OPP_Business_Unit__c = :businessUnitId' + currentAcademicYearCondition;

        String formatedQuery = String.format(oppQuery, new List<String> {fieldsToQuery, otherConditions});
        System.debug('formatedQuery = ' + formatedQuery);
        List<Opportunity> opportunities = database.query(formatedQuery);

        if(opportunities.size() >= 1){
            return opportunity = opportunities.get(0);
        }

        System.debug('inboundEmailSettings.CORE_Create_OPP_from_Inbound_email__c = '  + inboundEmailSettings.CORE_Create_OPP_from_Inbound_email__c);
        if(inboundEmailSettings.CORE_Create_OPP_from_Inbound_email__c){
            Opportunity opp = createNewOpportunity(accountId, businessUnitId, currentAcademicYear);
            createNewPointOfContact(opp.Id, accountId);
                //request to get owner or custom field value (for creation)
            return getOpportunityDatas(opp.Id, inboundEmailSettings);
        }

        return null;
    }

    @TestVisible
    private Opportunity selectOpportunityInManyOppCase(List<Opportunity> oppsFound, List<String> recipientAdresses){
        System.debug('selectOpportunityInManyOppCase start');
        if(!oppsFound.isEmpty() && !recipientAdresses.isEmpty()){
            Set<Id> buIdsRelatedToOpps = new Set<Id>();

            for(Opportunity currentOpp : oppsFound){
                buIdsRelatedToOpps.add(currentOpp.CORE_OPP_BUSINESS_UNIT__c);
            }

            string areaCondition = '';
            for(String address : recipientAdresses){
                areaCondition += 'OR CORE_Generic_Area_contact_email__c like \'%' +  address + '%\' ';
            }

            String ttst = 'SELECT Id FROM CORE_Business_Unit__c where Id in :buIdsRelatedToOpps ' +
            'AND (CORE_Generic_contact_email__c  IN :toAddresses OR ' +
            'CORE_Generic_Admission_contact_email__c IN :toAddresses OR ' +
            'CORE_Generic_Orientation_contact_email__c IN :toAddresses' +
            areaCondition + ')';
            list<CORE_Business_Unit__c> bus = database.query('SELECT Id FROM CORE_Business_Unit__c where Id in :buIdsRelatedToOpps ' +
            'AND (CORE_Generic_contact_email__c  IN :recipientAdresses OR ' +
            'CORE_Generic_Admission_contact_email__c IN :recipientAdresses OR ' +
            'CORE_Generic_Orientation_contact_email__c IN :recipientAdresses ' +
            areaCondition + ')');


            /*list<CORE_Business_Unit__c> bus = [ SELECT Id FROM CORE_Business_Unit__c where Id IN :buIdsRelatedToOpps
                AND (CORE_Generic_contact_email__c  IN :recipientAdresses OR
                CORE_Generic_Admission_contact_email__c IN :recipientAdresses OR
                CORE_Generic_Orientation_contact_email__c IN :recipientAdresses OR
                CORE_Generic_Area_contact_email__c IN :recipientAdresses )];
            */
            if(!bus.isEmpty()){
                Set<Id> busIdsRelatedToRecipient = new Map<Id,CORE_Business_Unit__c>(bus).keySet();
                Opportunity oppSelected;
                Integer nbOpp = 0;
                for(Opportunity currentOpp : oppsFound){
                    if(busIdsRelatedToRecipient.contains(currentOpp.CORE_OPP_Business_Unit__c)){
                        nbOpp++;
                        oppSelected = currentOpp;
                    }
                }

                if(nbOpp == 1){
                    return oppSelected;
                }
            }
        }
        
        return null;
    }

    @TestVisible
    private static void createNewPointOfContact(Id opportunityId, Id accountId){
        CORE_Point_of_Contact__c poc = new CORE_Point_of_Contact__c(
            CORE_Opportunity__c = opportunityId,
            CORE_Account__c = accountId,
            CORE_Insertion_Mode__c = POC_INSERTION_MODE,
            CORE_Nature__c = POC_NATURE,
            CORE_Enrollment_Channel__c = POC_ENROLLMENT_CHANNEL,
            CORE_Capture_Channel__c = POC_CAPTURE_CHANNEL,
            CORE_Insertion_Date__c = System.now()
        );

        insert poc;
    }
    
    @TestVisible
    private Opportunity createNewOpportunity(Id accountId, Id businessUnitId, String academicYear){
        CORE_CatalogHierarchyModel catalogHierarchy = new CORE_CatalogHierarchyModel(
            null, 
            businessUnitId, 
            null, 
            null, 
            null, 
            null, 
            null
        );

        catalogHierarchy = CORE_OpportunityDataMaker.getCatalogHierarchy(catalogHierarchy,false);
        
        Account account = new Account();
        account.Id = accountId;

        CORE_OpportunityDataMaker dataMaker = new CORE_OpportunityDataMaker();
        Opportunity opportunity = dataMaker.initOpportunity(account, academicYear, catalogHierarchy, false);
        
        if(!String.isEmpty(catalogHierarchy.businessUnit.CORE_BU_Technical_User__c)){
            opportunity.ownerId = catalogHierarchy.businessUnit.CORE_BU_Technical_User__c;
        }
        insert opportunity;

        return opportunity;
    }

    @TestVisible
    private Id buSelectionProcess(List<String> toAddresses){
        List<RelatedBuEmailType> relatedBuEmailType = new List<RelatedBuEmailType> {
            RelatedBuEmailType.GENERIC,
            RelatedBuEmailType.ADMISSION,
            RelatedBuEmailType.ORIENTATION,
            RelatedBuEmailType.AREA
        };
        
        List<CORE_Business_Unit__c> businessUnits = null;

        for(RelatedBuEmailType emailType : relatedBuEmailType){
            System.debug('emailType = ' + emailType);
            //GENERIC field first
            businessUnits = getRelatedBusinessUnits(toAddresses, emailType);
            System.debug('businessUnits.size() = ' + businessUnits.size());
            if(emailType == CORE_InboundEmailHandler.RelatedBuEmailType.ADMISSION && businessUnits.size()!=0){
                this.appearInRecipients = true;
            }
            switch on businessUnits.size() {
                when 0 {
                    continue;
                }
                when 1 {
                    return businessUnits.get(0).Id;
                }
                //n case
                when else {
                    if(!String.isEmpty(businessUnits.get(0).CORE_Parent_Division__c)){
                        return businessUnits.get(0).CORE_Parent_Division__r.CORE_Main_Business_Unit__c;
                    } else {
                        return null;
                    }
                }
            }
        }

        return null;
    }

    @TestVisible
    public enum RelatedBuEmailType {GENERIC,ADMISSION,ORIENTATION,AREA} 

    @TestVisible
    private List<CORE_Business_Unit__c> getRelatedBusinessUnits(List<String> toEmails, RelatedBuEmailType emailType){
        List<CORE_Business_Unit__c> businessUnits;
        String buEmailField = 'CORE_Generic_contact_email__c';
        Boolean formatString = true;
        String queryString = 'SELECT Id, CORE_Generic_contact_email__c, CORE_Generic_Admission_contact_email__c, CORE_Parent_Division__c, CORE_Parent_Division__r.CORE_Main_Business_Unit__c ' +
        'FROM CORE_Business_Unit__c ' +
        'WHERE {0} IN :toEmails ' +
        'ORDER BY LastModifiedDate DESC';
        
        switch on emailType {
            when GENERIC {
                buEmailField = 'CORE_Generic_contact_email__c';
            }
            when ADMISSION {
                buEmailField = 'CORE_Generic_Admission_contact_email__c';
            }
            when ORIENTATION {
                buEmailField = 'CORE_Generic_Orientation_contact_email__c';
            }
            when AREA {
                buEmailField = 'CORE_Generic_Area_contact_email__c';

                queryString = 'SELECT Id, CORE_Generic_contact_email__c, CORE_Generic_Admission_contact_email__c, CORE_Parent_Division__c, CORE_Parent_Division__r.CORE_Main_Business_Unit__c ';
                queryString += 'FROM CORE_Business_Unit__c';
                
                String firstEmail = toEmails.Get(0);
                String condition = ' WHERE ' + buEmailField + ' like \'%' + firstEmail + '%\'';
                if(toEmails.size()>1){
                    for(Integer i = 1 ; i < toEmails.size(); i++){
                        String currentEmail = toEmails.get(i);
                        condition += ' OR ' + buEmailField + ' like \'%' + currentEmail + '%\'';
                    }
                }
                queryString += condition + ' ORDER BY LastModifiedDate DESC';
                formatString = false;
            }
            when else {
                buEmailField = 'CORE_Generic_contact_email__c';
            }
        }

        String formatedQueryString = formatString ? String.format(queryString, new List<String> {buEmailField}) : queryString;
        
        businessUnits = database.query(formatedQueryString);

        return businessUnits;
    }

    @TestVisible
    private List<String> validatedAddresses(List<String> emailAddresses){
        List<String> addresses = new List<String>();
        if(emailAddresses != null && !emailAddresses.isEmpty()){
            for(String address : emailAddresses){
                if(!String.isEmpty(address)){
                    if(validateEmail(address)){
                        addresses.add(address.trim().toLowercase());
                    }
                }
            }
        }

        return addresses;
    }

    @TestVisible
    private String parseAddress(List<String> emailAddresses){
        String addresses;
        if(!emailAddresses.isEmpty()){
            addresses = String.join(emailAddresses, ';');
            if(addresses.length() > 4000){
                addresses = addresses.left(3993) + ' [...]';
            }
        }

        
        return addresses;
    }
    
    @TestVisible
    private Account getRelatedAccount(EmailMessage emailMessage, Boolean isInternalUser){
        Account relatedAccount;

        List<Account> existingAccounts = [SELECT id,PersonContactId from account 
        WHERE personEmail = :emailMessage.FromAddress Or
        CORE_Email_2__pc = :emailMessage.FromAddress Or
        CORE_Email_3__pc = :emailMessage.FromAddress Or
        CORE_Student_Email__pc = :emailMessage.FromAddress
        ORDER BY lastmodifiedDate DESC];

        //get of create person account if not found
        if(!existingAccounts.isEmpty()){
            relatedAccount = existingAccounts.get(0);
        } else if(!isInternalUser) {
            relatedAccount = new Account();
            Id studentRecordType = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(STUDENT_RECORDTYPE_DEVNAME).getRecordTypeId();

            if(emailMessage.fromname != null && emailMessage.fromname != ''){
                if(emailMessage.fromname.contains(' ')){
                    relatedAccount.FirstName = emailMessage.fromname.substring(0,emailMessage.fromname.indexOf(' ')).trim();
                    relatedAccount.LastName = emailMessage.fromname.substring(emailMessage.fromname.indexOf(' ')).trim();
                } else{
                    relatedAccount.LastName = emailMessage.fromname.trim();
                }
            } else{
                relatedAccount.LastName = STUDENT_DEFAULT_LASTNAME;
            }
            
            relatedAccount.PersonEmail = emailMessage.FromAddress;
            relatedAccount.RecordTypeId = studentRecordType;
            
        }

        return relatedAccount;
    }

    @TestVisible
    private Boolean validateEmail(String emailAdd){
        Boolean isValid = Pattern.matches('([a-zA-Z0-9_\\-\\.]+)@(((\\[a-z]{1,3}\\.[a-z]{1,3}\\.[a-z]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,10}|[0-9]{1,3}))', emailAdd.trim());
        system.debug('@validateEmail for : ' + emailAdd + ' => ' + isValid);
        return isValid;
    }

    @TestVisible
    private List<String> getInternalDomains(CORE_Inbound_Email_Setting__mdt inboundEmailSetting){
        List<String> internalDomains = new List<String>();

        String internalDomainsStr;


        if(inboundEmailSetting != null && !String.isEmpty(inboundEmailSetting.CORE_Internal_domains__c)){
            internalDomainsStr = inboundEmailSetting.CORE_Internal_domains__c.toLowercase();
            internalDomainsStr = internalDomainsStr.replaceAll( '\\s+', '');
        }

        if(!String.isEmpty(internalDomainsStr)){
            internalDomains = internalDomainsStr.split('\\,');
        }

        return internalDomains;
    }

    @TestVisible
    private CORE_Inbound_Email_Setting__mdt getInboundEmailSettings(){
        List<CORE_Inbound_Email_Setting__mdt> inboundEmailSettings = new List<CORE_Inbound_Email_Setting__mdt>();
        CORE_Inbound_Email_Setting__mdt inboundEmailSetting = new CORE_Inbound_Email_Setting__mdt();
        Boolean assignToOppOwnerDefaultValue = true;

        if(Test.isRunningTest()){
            inboundEmailSetting.CORE_Internal_domains__c = null;
            inboundEmailSetting.CORE_AssignToOppOwner__c = assignToOppOwnerDefaultValue;
            inboundEmailSettings.add(inboundEmailSetting);
        } else {
            //inboundEmailSettings = CORE_Inbound_Email_Setting__mdt.getAll().values();
            inboundEmailSettings = [SELECT CORE_Internal_domains__c, CORE_AssignToOppOwner__c, CORE_Custom_OPP_User_Field__c, CORE_Create_OPP_from_Inbound_email__c,CORE_Prioritary_Admission_Queue_Assign__c FROM CORE_Inbound_Email_Setting__mdt LIMIT 1];
        }

        if(!inboundEmailSettings.isEmpty()){
            inboundEmailSetting = inboundEmailSettings.get(0);
            if(String.isEmpty(inboundEmailSetting.CORE_Custom_OPP_User_Field__c) || !CORE_SobjectUtils.doesFieldExist('Opportunity', inboundEmailSetting.CORE_Custom_OPP_User_Field__c)){
                inboundEmailSetting.CORE_Custom_OPP_User_Field__c = null;
            }
        } else {
            inboundEmailSetting.CORE_AssignToOppOwner__c = assignToOppOwnerDefaultValue;
            inboundEmailSetting.CORE_Internal_domains__c = null;
        }

        if(String.isEmpty(inboundEmailSetting.CORE_Custom_OPP_User_Field__c)){
            inboundEmailSetting.CORE_Custom_OPP_User_Field__c = 'OwnerId';
        }

        return inboundEmailSetting;
    }
    @TestVisible
    private EmailMessage handleIncomingEmailFromExternalThirdParty(EmailMessage emailMessage){
        
        Id B2BExternalId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('CORE_B2B_External_Partner').getRecordTypeId();
        Id BusinessContactId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Business_Contact').getRecordTypeId();

        // get contact from third party accounts 
        //does it mean get the accounts of third party record types.
        String fromEmail = emailMessage.FromAddress;
        String fromDomain = fromEmail.substringAfterLast('@');
        List<Contact> contacts = [SELECT ID,AccountId FROM Contact WHERE (Email =:fromEmail OR CORE_Professional_Email__c =: fromEmail)  AND Account.RecordType.Id=:B2BExternalId ORDER BY LastModifiedDate DESC LIMIT 1];
        Contact associatedBusinessContact;
        Account associatedAccount;
        if(contacts.isEmpty()){
            
            string domaincheck='%'+fromDomain+'%';
            List<Account> accountsFromWebsite = [SELECT ID FROM Account WHERE RecordType.ID =:B2BExternalId AND Website like :domaincheck ];
            if(!accountsFromWebsite.isEmpty()){
                associatedAccount = accountsFromWebsite[0];
            } 
            
            // create the Business Contact
            if(associatedAccount == null){
                return null;
            }
            associatedBusinessContact = new Contact(
                RecordTypeId=BusinessContactId,
                Email = fromEmail,
                AccountId=associatedAccount.Id
            );
            
            if(emailMessage.fromname != null && emailMessage.fromname != ''){
                if(emailMessage.fromname.contains(' ')){
                    associatedBusinessContact.FirstName = emailMessage.fromname.substring(0,emailMessage.fromname.indexOf(' ')).trim();
                    associatedBusinessContact.LastName = emailMessage.fromname.substring(emailMessage.fromname.indexOf(' ')).trim();
                } else{
                    associatedBusinessContact.LastName = emailMessage.fromname.trim();
                }
            } else{
                associatedBusinessContact.LastName = STUDENT_DEFAULT_LASTNAME;
            }

            insert associatedBusinessContact;
            
        
        }else{
            associatedBusinessContact = contacts[0];
            associatedAccount = new Account(Id=contacts[0].AccountId); 
        }
        emailMessage.RelatedToId=associatedAccount.Id;
        emailMessage.CORE_TECH_Is_From_Retail__c = true;
        emailMessage.CORE_TECH_WhoId__c = associatedBusinessContact.Id;
        return emailMessage;
    }

}