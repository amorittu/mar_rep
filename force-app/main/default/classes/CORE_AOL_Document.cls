@RestResource(urlMapping='/aol/document')
global without sharing class CORE_AOL_Document {  
    //testGit
    private static final String SUCCESS_MESSAGE = 'AOL documents & progression fields upserted with success';

    @HttpPost
    global static CORE_AOL_Wrapper.ResultWrapperGlobal execute() {

        RestRequest	request = RestContext.request;

        // Initiating logs
        Long dt1 = DateTime.now().getTime();
        Long dt2;
        CORE_LogManager logSettings;
        CORE_LOG__c log = new CORE_LOG__c(CORE_API_Name__c = 'AOL');
        Boolean isErrorLog = false;
        logSettings = new CORE_LogManager('AOL');

        Map<String, Object> paramsMap = (Map<String, Object>)JSON.deserializeUntyped(request.requestBody.toString());
        String aolExternalId = (String)paramsMap.get(CORE_AOL_Constants.PARAM_AOL_EXTERNALID);
        String onlineApplicationDocumentUrl = (String)paramsMap.get(CORE_AOL_Constants.PARAM_AOL_APP_DOC_URL);
        List<CORE_AOL_Wrapper.DocumentWrapper> aolDocuments = (List<CORE_AOL_Wrapper.DocumentWrapper>)JSON.deserialize((String)JSON.serialize(paramsMap.get(CORE_AOL_Constants.PARAM_AOL_DOC)), List<CORE_AOL_Wrapper.DocumentWrapper>.class);
        CORE_AOL_Wrapper.AOLProgressionFields aolProgressionFields = (CORE_AOL_Wrapper.AOLProgressionFields)JSON.deserialize((String)JSON.serialize(paramsMap.get(CORE_AOL_Constants.PARAM_AOL_PROG_FIELDS)), CORE_AOL_Wrapper.AOLProgressionFields.class);


        CORE_AOL_Wrapper.ResultWrapperGlobal result = checkMandatoryParameters(aolExternalId, aolDocuments);

        try{
            if(result != null){
                //missing parameter(s)
                return result;
            }
            CORE_AOL_GlobalWorker aolWorker = new CORE_AOL_GlobalWorker(aolProgressionFields,aolExternalId, paramsMap);
            Opportunity opportunity = aolWorker.aolOpportunity;

            if(opportunity == null){
                result = new CORE_AOL_Wrapper.ResultWrapperGlobal(
                    CORE_AOL_Constants.ERROR_STATUS, 
                    String.format(CORE_AOL_Constants.MSG_OPPORTUNITY_NOT_FOUND, new List<String>{aolExternalId})
                );
            } else {
                documentProcess(aolWorker, onlineApplicationDocumentUrl, aolDocuments);

                aolWorker.updateOppIsNeeded = true;
                aolWorker.updateAolProgressionFields();

                result = new CORE_AOL_Wrapper.ResultWrapperGlobal(CORE_AOL_Constants.SUCCESS_STATUS, SUCCESS_MESSAGE);
            }

        } catch(Exception e){
            result = new CORE_AOL_Wrapper.ResultWrapperGlobal(CORE_AOL_Constants.ERROR_STATUS, e.getMessage() + ' at ' + e.getStackTraceString());
        } finally{
            dt2 = DateTime.now().getTime();
            System.debug('CORE_AOL_AcademicDiplomaHistory(urlMapping=\'/aol/academic-diploma-history\') : T0TAL TIME OF EXECUTION = ' + (dt2-dt1) + ' ms');
            // creating log
            log.CORE_Processing_Duration__c =(dt2-dt1);
            log.CORE_HTTP_Status_Code__c = result.status == 'KO' ? 400 : result.status == 'OK' ? 200 : 500;
            log.CORE_Sent_Body__c = JSON.serialize(result);
            log.CORE_Received_Body__c =  request.requestBody.toString();
            
            if(!logSettings.isNone()){
                if(logSettings.isAllLogged()){ // is All logged
                    CORE_LogManager.createLog(log);
                }else if(logSettings.isErrorLogged() && isErrorLog){ // is Error
                    CORE_LogManager.createLog(log);
                }
            }
        }

        return result;
    }

    public static void documentProcess(CORE_AOL_GlobalWorker aolWorker, String onlineApplicationDocumentUrl, List<CORE_AOL_Wrapper.DocumentWrapper> aolDocuments){
        Map<String, Object> docAdditionFieldsMap = new Map<String, Object>();

        if(aolWorker.paramsMap != null
           && aolWorker.paramsMap.containsKey(CORE_AOL_Constants.PARAM_AOL_DOC)){
            List<Object> aolDocumentsList = (List<Object>)JSON.deserializeUntyped((String)JSON.serialize(aolWorker.paramsMap.get(CORE_AOL_Constants.PARAM_AOL_DOC)));

            for(Object doc : aolDocumentsList){
                Map<String, Object> docMap = (Map<String, Object>)JSON.deserializeUntyped((String)JSON.serialize(doc));

                if(docMap.get('externalId') != null
                   && docMap.containsKey(CORE_AOL_Constants.PARAM_ADDITIONAL_FIELDS)){
                    docAdditionFieldsMap.put((String)docMap.get('externalId'), docMap.get(CORE_AOL_Constants.PARAM_ADDITIONAL_FIELDS));
                }
            }
        }

        if(!String.isBlank(onlineApplicationDocumentUrl)){
            aolWorker.aolOpportunity.CORE_OPP_AOL_document_folder_Url__c = onlineApplicationDocumentUrl;
            //update will be done with aolProgressionFields
            aolWorker.updateOppIsNeeded = true;
        }

        List<CORE_AOL_Document__c> aolDocumentsToUpsert = new List<CORE_AOL_Document__c>();

        for(CORE_AOL_Wrapper.DocumentWrapper aolDocument : aolDocuments){

            CORE_AOL_Document__c currentDocument = new CORE_AOL_Document__c();
            currentDocument.Name = aolDocument.name;
            currentDocument.CORE_Date_Approved__c = aolDocument.approvedDate;
            currentDocument.CORE_Date_Created__c = aolDocument.createdDate;
            currentDocument.CORE_Date_Rejected__c = aolDocument.rejectedDate;
            currentDocument.CORE_Document_External_ID__c = aolDocument.externalId;
            currentDocument.CORE_Document_external_Url__c = aolDocument.externalUrl;
            currentDocument.CORE_Document_type__c = aolDocument.type;
            currentDocument.CORE_Related_to_Opportunity__c = aolWorker.aolOpportunity.Id;

            //set Additional fields to update
            if(docAdditionFieldsMap.containsKey(aolDocument.externalId)){
                JSONGenerator jsonReq = JSON.createGenerator(true);
                jsonReq.writeStartObject();

                jsonReq.writeObjectField(CORE_AOL_Constants.PARAM_ADDITIONAL_FIELDS, docAdditionFieldsMap.get(aolDocument.externalId));

                jsonReq.writeEndObject();

                currentDocument = (CORE_AOL_Document__c)CORE_AOL_GlobalWorker.setAdditionalFields(currentDocument, jsonReq.getAsString());
            }

            aolDocumentsToUpsert.add(currentDocument);
        }

        if(aolDocumentsToUpsert.size() > 0){
            upsert aolDocumentsToUpsert CORE_Document_External_ID__c;
        }
    }

    @TestVisible
    private static CORE_AOL_Wrapper.ResultWrapperGlobal checkMandatoryParameters(String aolExternalId,  List<CORE_AOL_Wrapper.DocumentWrapper> aolDocuments){
        List<String> missingParameters = new List<String>();
        Boolean isMissingAolId = CORE_AOL_GlobalWorker.isAolExternalIdParameterMissing(aolExternalId);

        if(isMissingAolId){
            missingParameters.add('aolExternalId');
        }
        
        for(Integer i = 0 ; i < aolDocuments.size() ; i++){
            missingParameters.addAll(getDocumentMissingParameters(aolDocuments.get(i),i));
        }

        return CORE_AOL_GlobalWorker.getMissingParameters(missingParameters);
    }

    private static List<String> getDocumentMissingParameters(CORE_AOL_Wrapper.DocumentWrapper aolDocuments, Integer iteration){
        List<String> mandatoryParameters = new List<String> {'externalId', 'name'};
        
        List<String> missingParameters = new List<String>();

        for(String mandatoryParameter : mandatoryParameters){
            if(String.IsBlank((String) aolDocuments.get(mandatoryParameter))){
                missingParameters.add(String.format('{0}[{1}].{2}', new List<Object>{CORE_AOL_Constants.PARAM_AOL_DOC, iteration, mandatoryParameter}));
            }
        }
        
        return missingParameters;
    }

}