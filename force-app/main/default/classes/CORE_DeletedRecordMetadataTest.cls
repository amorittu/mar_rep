@IsTest
public class CORE_DeletedRecordMetadataTest {
    @IsTest
    public static void getMetadata_Should_retrieve_and_store_custom_metadata_values() {
        CORE_DeletedRecordMetadata delMetadata = new CORE_DeletedRecordMetadata();

        delMetadata.deletedRecordMetadata = null;
        System.assertEquals(NULL,delMetadata.deletedRecordMetadata);

        Test.startTest();
        delMetadata.getMetadata();
        Test.stopTest();     

        System.assertNotEquals(null,delMetadata.deletedRecordMetadata , 'CORE_Deleted_record_setting__mdt should be initialized');
        System.assertEquals(14,delMetadata.deletedRecordMetadata.CORE_Delay__c , 'Delay value should be initialized');
    }
}