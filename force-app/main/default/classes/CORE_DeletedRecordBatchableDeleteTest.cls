@IsTest
public class CORE_DeletedRecordBatchableDeleteTest {
    @TestSetup
    public static void makeData(){
        Datetime deletionDate = System.now();
        CORE_DataFaker_DeletedRecord.insertDeletedRecord(deletionDate, 'Test1');
        CORE_DataFaker_DeletedRecord.insertDeletedRecord(deletionDate.addDays(-20), 'Test2');
    }

    @IsTest
    public static void deleted_Record_Should_Be_Deleted() {
        test.starttest();
        CORE_DeletedRecordBatchableDelete deletedRecordBatchableDelete = new CORE_DeletedRecordBatchableDelete();
        database.executebatch(deletedRecordBatchableDelete);
        test.stopTest();

        List<CORE_Deleted_record__c> results = [Select Id FROM CORE_Deleted_record__c];
        System.assertEquals(1, results.size());
    }
}