public class CORE_LeadFormAcquisitionUtils {
    public static List<Account> getAccountsFromIds(Set<Id> ids){
        List<Account> accounts = [SELECT Id, PersonContactId, CreatedDate, Salutation, LastName, FirstName, Phone,CORE_Phone2__pc, 
            PersonMobilePhone, CORE_Mobile_2__pc, CORE_Mobile_3__pc, PersonEmail,CORE_Email_2__pc,CORE_Email_3__pc, CORE_Student_Email__pc,
            PersonMailingStreet, PersonMailingPostalCode, PersonMailingCity, CORE_Language_website__pc, PersonMailingCountry, PersonMailingState, OwnerId,
            CORE_GA_User_ID__c, CORE_Gender__pc , CORE_Professional_situation__pc,CORE_Main_Nationality__pc,CORE_Year_of_birth__pc,PersonBirthdate
            FROM Account WHERE ID in :ids];

        return accounts;
    }

    public static Account getAccountFromId(Id id){
        List<Account> accounts = getAccountsFromIds(new Set<Id>{id});
        Account acc;
        if(!accounts.isEmpty()){
            acc = accounts.get(0);
        }

        return acc;
    }

    public static List<Account> getExestingAccounts(CORE_LeadFormAcquisitionWrapper.PersonAccountWrapper personAccountWrapper){
        System.debug('CORE_LeadFormAcquisitionUtils.getExestingAccounts() : START');
        Long dt1 = DateTime.now().getTime();
        Long dt2;
        
        List<Account> accounts = new List<Account>();
        String emailQuerry = personAccountWrapper.emailAddress;
        String phone = personAccountWrapper.phone;
        String mobilePhone = personAccountWrapper.mobilePhone;

        if(!String.isBlank(emailQuerry)){
            accounts = [SELECT Id, PersonContactId, CreatedDate, Salutation, LastName, FirstName, Phone, CORE_Phone2__pc,
                PersonMobilePhone, CORE_Mobile_2__pc, CORE_Mobile_3__pc, PersonEmail,CORE_Email_2__pc,CORE_Email_3__pc, CORE_Student_Email__pc,
                PersonMailingStreet, PersonMailingPostalCode, PersonMailingCity, CORE_Language_website__pc, PersonMailingCountry, PersonMailingState, OwnerId,CORE_GA_User_ID__c,
                CORE_Gender__pc , CORE_Professional_situation__pc,CORE_Main_Nationality__pc,CORE_Year_of_birth__pc,PersonBirthdate
                FROM Account
                WHERE PersonEmail = :emailQuerry OR CORE_Email_2__pc = :emailQuerry OR CORE_Email_3__pc = :emailQuerry OR CORE_Student_Email__pc = :emailQuerry
                ORDER BY CORE_Last_inbound_contact_date__c DESC NULLS LAST, LastModifiedDate Desc];
        }
        
        if(accounts.isEmpty()){
            if(!String.isBlank(phone)){
                if(!String.isBlank(mobilePhone)){
                    List<String> phones = new List<String>{phone,mobilePhone};
                    accounts = [SELECT Id, PersonContactId, CreatedDate, Salutation, LastName, FirstName, Phone, CORE_Phone2__pc, 
                        PersonMobilePhone, CORE_Mobile_2__pc, CORE_Mobile_3__pc, PersonEmail,CORE_Email_2__pc,CORE_Email_3__pc, CORE_Student_Email__pc,
                        PersonMailingStreet, PersonMailingPostalCode, PersonMailingCity, CORE_Language_website__pc, PersonMailingCountry, PersonMailingState, OwnerId,CORE_GA_User_ID__c,
                        CORE_Gender__pc , CORE_Professional_situation__pc,CORE_Main_Nationality__pc,CORE_Year_of_birth__pc,PersonBirthdate
                        FROM Account
                        WHERE Phone in :phones OR PersonMobilePhone in :phones OR CORE_Mobile_2__pc in :phones OR CORE_Mobile_3__pc in :phones
                        ORDER BY CORE_Last_inbound_contact_date__c DESC NULLS LAST, LastModifiedDate Desc];
                } else{
                    accounts = [SELECT Id, PersonContactId, CreatedDate, Salutation, LastName, FirstName, Phone, CORE_Phone2__pc, 
                        PersonMobilePhone, CORE_Mobile_2__pc, CORE_Mobile_3__pc, PersonEmail,CORE_Email_2__pc,CORE_Email_3__pc, CORE_Student_Email__pc,
                        PersonMailingStreet, PersonMailingPostalCode, PersonMailingCity, CORE_Language_website__pc, PersonMailingCountry, PersonMailingState, OwnerId,CORE_GA_User_ID__c,
                        CORE_Gender__pc , CORE_Professional_situation__pc,CORE_Main_Nationality__pc,CORE_Year_of_birth__pc,PersonBirthdate
                        FROM Account
                        WHERE Phone = :phone OR PersonMobilePhone = :phone OR CORE_Mobile_2__pc = :phone OR CORE_Mobile_3__pc = :phone
                        ORDER BY CORE_Last_inbound_contact_date__c DESC NULLS LAST, LastModifiedDate Desc];
                }
            } else if(!String.isBlank(mobilePhone)){
                accounts = [SELECT Id, PersonContactId, CreatedDate, Salutation, LastName, FirstName, Phone, CORE_Phone2__pc, 
                    PersonMobilePhone, CORE_Mobile_2__pc, CORE_Mobile_3__pc, PersonEmail,CORE_Email_2__pc,CORE_Email_3__pc, CORE_Student_Email__pc,
                    PersonMailingStreet, PersonMailingPostalCode, PersonMailingCity, CORE_Language_website__pc, PersonMailingCountry, PersonMailingState, OwnerId,CORE_GA_User_ID__c,
                    CORE_Gender__pc , CORE_Professional_situation__pc,CORE_Main_Nationality__pc,CORE_Year_of_birth__pc,PersonBirthdate
                    FROM Account
                    WHERE Phone = :mobilePhone OR PersonMobilePhone = :mobilePhone OR CORE_Mobile_2__pc = :mobilePhone OR CORE_Mobile_3__pc = :mobilePhone
                    ORDER BY CORE_Last_inbound_contact_date__c DESC NULLS LAST, LastModifiedDate Desc];
            }
        }
        
        dt2 = DateTime.now().getTime();
        System.debug('getExestingAccounts time execution = ' + (dt2-dt1) + ' ms');
        System.debug('CORE_LeadFormAcquisitionUtils.getExestingAccounts() : END');
        System.debug('accounts : ' + accounts);

        return accounts;
    }

    public static List<CORE_Division__c> getDivisionsFromExternalIds(CORE_LeadFormAcquisitionWrapper.GlobalWrapper inputParameters){
        CORE_LeadFormAcquisitionWrapper.OpportunityWrapper mainInterest = inputParameters.mainInterest;
        CORE_LeadFormAcquisitionWrapper.OpportunityWrapper secondInterest = inputParameters.secondInterest;
        CORE_LeadFormAcquisitionWrapper.OpportunityWrapper thirdInterest = inputParameters.thirdInterest;
        List<String> externalIds = new List<String>();
        List<CORE_Division__c> divisions = new List<CORE_Division__c>();

        if(mainInterest != null && !String.isBlank(mainInterest.division)){
            externalIds.add(mainInterest.division);
        }
        if(secondInterest != null && !String.isBlank(secondInterest.division)){
            externalIds.add(secondInterest.division);
        }
        if(thirdInterest != null && !String.isBlank(thirdInterest.division)){
            externalIds.add(thirdInterest.division);
        }

        if(externalIds.size() > 0){
            divisions = [SELECT ID, CORE_Division_ExternalId__c FROM CORE_Division__c WHERE CORE_Division_ExternalId__c in :externalIds];
        }
        
        return divisions;
    }

    public static List<CORE_Business_Unit__c> getBusinessUnitsFromExternalIds(CORE_LeadFormAcquisitionWrapper.GlobalWrapper inputParameters){
        CORE_LeadFormAcquisitionWrapper.OpportunityWrapper mainInterest = inputParameters.mainInterest;
        CORE_LeadFormAcquisitionWrapper.OpportunityWrapper secondInterest = inputParameters.secondInterest;
        CORE_LeadFormAcquisitionWrapper.OpportunityWrapper thirdInterest = inputParameters.thirdInterest;
        List<String> externalIds = new List<String>();
        List<CORE_Business_Unit__c> businessUnits = new List<CORE_Business_Unit__c>();

        if(mainInterest != null && !String.isBlank(mainInterest.businessUnit)){
            externalIds.add(mainInterest.businessUnit);
        }
        if(secondInterest != null && !String.isBlank(secondInterest.businessUnit)){
            externalIds.add(secondInterest.businessUnit);
        }
        if(thirdInterest != null && !String.isBlank(thirdInterest.businessUnit)){
            externalIds.add(thirdInterest.businessUnit);
        }

        if(externalIds.size() > 0){
            businessUnits = [SELECT ID, CORE_Business_Unit_ExternalId__c FROM CORE_Business_Unit__c WHERE CORE_Business_Unit_ExternalId__c in :externalIds];
        }
        
        return businessUnits;
    }

    public static List<CORE_Study_Area__c> getStudyAreasFromExternalIds(CORE_LeadFormAcquisitionWrapper.GlobalWrapper inputParameters){
        CORE_LeadFormAcquisitionWrapper.OpportunityWrapper mainInterest = inputParameters.mainInterest;
        CORE_LeadFormAcquisitionWrapper.OpportunityWrapper secondInterest = inputParameters.secondInterest;
        CORE_LeadFormAcquisitionWrapper.OpportunityWrapper thirdInterest = inputParameters.thirdInterest;
        List<String> externalIds = new List<String>();
        List<CORE_Study_Area__c> studyAreas = new List<CORE_Study_Area__c>();

        if(mainInterest != null && !String.isBlank(mainInterest.studyArea)){
            externalIds.add(mainInterest.studyArea);
        }
        if(secondInterest != null && !String.isBlank(secondInterest.studyArea)){
            externalIds.add(secondInterest.studyArea);
        }
        if(thirdInterest != null && !String.isBlank(thirdInterest.studyArea)){
            externalIds.add(thirdInterest.studyArea);
        }

        if(externalIds.size() > 0){
            studyAreas = [SELECT ID, CORE_Study_Area_ExternalId__c FROM CORE_Study_Area__c WHERE CORE_Study_Area_ExternalId__c in :externalIds];
        }
        
        return studyAreas;
    }

    public static List<CORE_Curriculum__c> getCurriculumsFromExternalIds(CORE_LeadFormAcquisitionWrapper.GlobalWrapper inputParameters){
        CORE_LeadFormAcquisitionWrapper.OpportunityWrapper mainInterest = inputParameters.mainInterest;
        CORE_LeadFormAcquisitionWrapper.OpportunityWrapper secondInterest = inputParameters.secondInterest;
        CORE_LeadFormAcquisitionWrapper.OpportunityWrapper thirdInterest = inputParameters.thirdInterest;
        List<String> externalIds = new List<String>();
        List<CORE_Curriculum__c> curriculums = new List<CORE_Curriculum__c>();

        if(mainInterest != null && !String.isBlank(mainInterest.curriculum)){
            externalIds.add(mainInterest.curriculum);
        }
        if(secondInterest != null && !String.isBlank(secondInterest.curriculum)){
            externalIds.add(secondInterest.curriculum);
        }
        if(thirdInterest != null && !String.isBlank(thirdInterest.curriculum)){
            externalIds.add(thirdInterest.curriculum);
        }

        if(externalIds.size() > 0){
            curriculums = [SELECT ID, CORE_Curriculum_ExternalId__c FROM CORE_Curriculum__c WHERE CORE_Curriculum_ExternalId__c in :externalIds];
        }
        
        return curriculums;
    }

    public static List<CORE_Level__c> getLevelsFromExternalIds(CORE_LeadFormAcquisitionWrapper.GlobalWrapper inputParameters){
        CORE_LeadFormAcquisitionWrapper.OpportunityWrapper mainInterest = inputParameters.mainInterest;
        CORE_LeadFormAcquisitionWrapper.OpportunityWrapper secondInterest = inputParameters.secondInterest;
        CORE_LeadFormAcquisitionWrapper.OpportunityWrapper thirdInterest = inputParameters.thirdInterest;
        List<String> externalIds = new List<String>();
        List<CORE_Level__c> levels = new List<CORE_Level__c>();

        if(mainInterest != null && !String.isBlank(mainInterest.level)){
            externalIds.add(mainInterest.level);
        }
        if(secondInterest != null && !String.isBlank(secondInterest.level)){
            externalIds.add(secondInterest.level);
        }
        if(thirdInterest != null && !String.isBlank(thirdInterest.level)){
            externalIds.add(thirdInterest.level);
        }

        if(externalIds.size() > 0){
            levels = [SELECT ID, CORE_Level_ExternalId__c FROM CORE_Level__c WHERE CORE_Level_ExternalId__c in :externalIds];
        }
        
        return levels;
    }

    public static List<CORE_Intake__c> getIntakesFromExternalIds(CORE_LeadFormAcquisitionWrapper.GlobalWrapper inputParameters){
        CORE_LeadFormAcquisitionWrapper.OpportunityWrapper mainInterest = inputParameters.mainInterest;
        CORE_LeadFormAcquisitionWrapper.OpportunityWrapper secondInterest = inputParameters.secondInterest;
        CORE_LeadFormAcquisitionWrapper.OpportunityWrapper thirdInterest = inputParameters.thirdInterest;
        List<String> externalIds = new List<String>();
        List<CORE_Intake__c> intakes = new List<CORE_Intake__c>();

        if(mainInterest != null && !String.isBlank(mainInterest.intake)){
            externalIds.add(mainInterest.intake);
        }
        if(secondInterest != null && !String.isBlank(secondInterest.intake)){
            externalIds.add(secondInterest.intake);
        }
        if(thirdInterest != null && !String.isBlank(thirdInterest.intake)){
            externalIds.add(thirdInterest.intake);
        }

        if(externalIds.size() > 0){
            intakes = [SELECT ID, CORE_Intake_ExternalId__c FROM CORE_Intake__c WHERE CORE_Intake_ExternalId__c in :externalIds];
        }
        
        return intakes;
    }

    public static Task prefillTaskCatalogFields(Task task, CORE_CatalogHierarchyModel catalogHierarchy){
        Task returningTask = task;

        if(!String.IsBlank(catalogHierarchy.division.Id)){
            returningTask.CORE_Division__c = catalogHierarchy.division.Id ;
        } else if(!String.isBlank(catalogHierarchy.division.CORE_Division_ExternalId__c)){
            returningTask.CORE_Division__r = catalogHierarchy.division;
        }

        if(!String.IsBlank(catalogHierarchy.businessUnit.Id)){
            returningTask.CORE_Business_Unit__c = catalogHierarchy.businessUnit.Id;
        } else if(!String.isBlank(catalogHierarchy.businessUnit.CORE_Business_Unit_ExternalId__c)){
            returningTask.CORE_Business_Unit__r = catalogHierarchy.businessUnit;
        }

        if(!String.IsBlank(catalogHierarchy.studyArea.Id)){
            returningTask.CORE_Study_Area__c = catalogHierarchy.studyArea.Id;
        } else if(!String.isBlank(catalogHierarchy.studyArea.CORE_Study_Area_ExternalId__c)){
            returningTask.CORE_Study_Area__r = catalogHierarchy.studyArea;
        }

        if(!String.IsBlank(catalogHierarchy.curriculum.Id)){
            returningTask.CORE_Curriculum__c = catalogHierarchy.curriculum.Id;
        } else if(!String.isBlank(catalogHierarchy.curriculum.CORE_Curriculum_ExternalId__c)){
            returningTask.CORE_Curriculum__r = catalogHierarchy.curriculum;
        }

        if(!String.IsBlank(catalogHierarchy.level.Id)){
            returningTask.CORE_Level__c = catalogHierarchy.level.Id;
        } else if(!String.isBlank(catalogHierarchy.level.CORE_Level_ExternalId__c)){
            returningTask.CORE_Level__r = catalogHierarchy.level;
        }

        if(!String.IsBlank(catalogHierarchy.intake.Id)){
            returningTask.CORE_Intake__c = catalogHierarchy.intake.Id;
        } else if(!String.isBlank(catalogHierarchy.intake.CORE_Intake_ExternalId__c)){
            returningTask.CORE_Intake__r = catalogHierarchy.intake;
        }

        if(!String.IsBlank(catalogHierarchy.product.Id)){
            returningTask.CORE_Product__c = catalogHierarchy.product.Id;
        }

        return returningTask;
    }
}