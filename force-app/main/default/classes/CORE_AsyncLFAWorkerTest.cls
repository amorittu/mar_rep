@IsTest
public with sharing class CORE_AsyncLFAWorkerTest {
    @TestSetup
    static void makeData(){
        String uniqueKey = 'testMakeData';
        String mobile = '+33646454548';
        String phone = '+33246454645';
        String email = 'testMakeData@Test.com';
        CORE_CatalogHierarchyModel catalog1 = new CORE_DataFaker_CatalogHierarchy(uniqueKey + '1').catalogHierarchy;
        CORE_CatalogHierarchyModel catalog2 = new CORE_DataFaker_CatalogHierarchy(uniqueKey + '2').catalogHierarchy;

        CORE_TriggerUtils.setBypassTrigger();

        CORE_DataFaker_LeadAcquisitionBuffer.getBuffer(uniqueKey + '1', mobile, phone, email, catalog1);
        CORE_DataFaker_LeadAcquisitionBuffer.getBuffer(uniqueKey + '2', mobile, phone, email, catalog2);
        CORE_TriggerUtils.setBypassTrigger();
    }

    @IsTest
    public static void constructor_Should_initialize_attributes() {
        List<CORE_LeadAcquisitionBuffer__c> buffers = [Select Id, CORE_Status__c FROM CORE_LeadAcquisitionBuffer__c];
        CORE_AsyncLFAWorker worker = new CORE_AsyncLFAWorker(buffers);

        System.assertNotEquals(null, worker.bufferRecords);
        System.assertNotEquals(0, worker.bufferRecords.size());
        System.assertNotEquals(null, worker.bufferRecordByIds);
        System.assertNotEquals(null, worker.buffersInError);
        System.assertNotEquals(null, worker.dmlOptions);
        System.assertNotEquals(null, worker.accountUpserted);
        System.assertNotEquals(null, worker.opportunityUpserted);
        System.assertNotEquals(null, worker.bufferIdsByOpportunityIds);
        System.assertNotEquals(null, worker.availableAcademicYears);
        System.assertNotEquals(0, worker.availableAcademicYears.size());
        System.assertNotEquals(null, worker.tasksToCreate);
        System.assertNotEquals(null, worker.dataMaker);
    }

    @IsTest
    public static void initBufferRecordByIds_should_return_map_of_buffer_by_ids() {
        List<CORE_LeadAcquisitionBuffer__c> buffers = [Select Id, CORE_Status__c, CORE_ErrorDescription__c FROM CORE_LeadAcquisitionBuffer__c];
        CORE_AsyncLFAWorker worker = new CORE_AsyncLFAWorker(buffers);

        Test.startTest();
        Map<Id, CORE_LeadAcquisitionBuffer__c> results = worker.initBufferRecordByIds(buffers);
        Test.stopTest();

        System.assertEquals(2, results.size());
        System.assertEquals(buffers.get(0).Id, results.get(buffers.get(0).Id).Id);
        System.assertEquals('', results.get(buffers.get(0).Id).CORE_ErrorDescription__c);
        System.assertEquals(buffers.get(1).Id, results.get(buffers.get(1).Id).Id);
        System.assertEquals('', results.get(buffers.get(1).Id).CORE_ErrorDescription__c);
    }

    @IsTest
    public static void getBufferDatasWithoutOkStatus_should_return_buffers_which_are_not_in_status_ok() {
        List<CORE_LeadAcquisitionBuffer__c> buffers = [Select Id, CORE_Status__c, CORE_ErrorDescription__c FROM CORE_LeadAcquisitionBuffer__c];
        buffers.get(0).CORE_Status__c = CORE_AsyncLFAWorker.SUCCESS_STATUS;

        CORE_AsyncLFAWorker worker = new CORE_AsyncLFAWorker(buffers);

        Test.startTest();
        List<CORE_LeadAcquisitionBuffer__c> results = worker.getBufferDatasWithoutOkStatus(buffers);
        Test.stopTest();

        System.assertEquals(1, results.size());
        System.assertEquals(buffers.get(1).Id, results.get(0).Id);
    }

    @IsTest
    public static void getDmlOptions_should_return_parameters_for_database_operations() {
        List<CORE_LeadAcquisitionBuffer__c> buffers = [Select Id, CORE_Status__c, CORE_ErrorDescription__c FROM CORE_LeadAcquisitionBuffer__c];
        buffers.get(0).CORE_Status__c = CORE_AsyncLFAWorker.SUCCESS_STATUS;

        CORE_AsyncLFAWorker worker = new CORE_AsyncLFAWorker(buffers);

        Test.startTest();
        Database.DMLOptions result = worker.getDmlOptions();
        Test.stopTest();

        System.assertEquals(true, result.DuplicateRuleHeader.AllowSave);
        System.assertEquals(false, result.OptAllOrNone);
    }

    @IsTest
    public static void execute_should_upsert_account_and_opportunities_and_consents_and_pocs_and_tasks_and_campaignmember() {
        String uniqueKey = 'testExecute';
        String mobile = '+33600000000';
        String phone = '+33200000000';
        String email = 'testExecute@Test.com';
        CORE_CatalogHierarchyModel catalog3 = new CORE_DataFaker_CatalogHierarchy(uniqueKey + '3').catalogHierarchy;
        CORE_CatalogHierarchyModel catalog4 = new CORE_DataFaker_CatalogHierarchy(uniqueKey + '4').catalogHierarchy;
        id pricebookId = CORE_DataFaker_PriceBook.getStandardPricebookId();
        PricebookEntry pbe1 = CORE_DataFaker_PriceBook.getPriceBookEntry(catalog3.product.Id, pricebookId, 100.0);
        PricebookEntry pbe2 = CORE_DataFaker_PriceBook.getPriceBookEntry(catalog4.product.Id, pricebookId, 2000.0);
        CORE_PriceBook_BU__c pbu1 = CORE_DataFaker_PriceBook.getPriceBookBu(pricebookId, catalog3.businessUnit.Id, uniqueKey + '3');
        CORE_PriceBook_BU__c pbu2 = CORE_DataFaker_PriceBook.getPriceBookBu(pricebookId, catalog4.businessUnit.Id, uniqueKey + '4');

        Campaign campaign = CORE_DataFaker_Campaign.getCampaign(uniqueKey);
        
        CORE_TriggerUtils.setBypassTrigger();
        CORE_LeadAcquisitionBuffer__c buffer1 = CORE_DataFaker_LeadAcquisitionBuffer.getBuffer(uniqueKey + '3', mobile, phone, email, catalog3);
        buffer1 = CORE_DataFaker_LeadAcquisitionBuffer.getBufferWithCampaignMember(buffer1, campaign.Id);
        CORE_LeadAcquisitionBuffer__c buffer2 = CORE_DataFaker_LeadAcquisitionBuffer.getBuffer(uniqueKey + '4', mobile, phone, email, catalog4);
        buffer2 = CORE_DataFaker_LeadAcquisitionBuffer.getBufferWithCampaignMember(buffer2, campaign.Id);

        List<CORE_LeadAcquisitionBuffer__c> buffers = new List<CORE_LeadAcquisitionBuffer__c> {buffer1, buffer2};
        Update buffers; 

        CORE_TriggerUtils.setBypassTrigger();

        CORE_AsyncLFAWorker worker = new CORE_AsyncLFAWorker(buffers);

        Test.startTest();
        worker.execute();
        Test.stopTest();

        List<Account> accounts = [SELECT Id,CORE_Main_Nationality__pc FROM Account];
        List<Opportunity> opportunities = [SELECT Id FROM Opportunity];
        List<OpportunityLineItem> opportunityLineItem = [SELECT Id FROM OpportunityLineItem];
        List<CORE_Point_of_Contact__c> pocs = [SELECT Id FROM CORE_Point_of_Contact__c];
        List<CORE_Consent__c> consents = [SELECT Id FROM CORE_Consent__c];
        List<CampaignMember> campaignMembers = [SELECT Id FROM CampaignMember];
        List<Task> tasks = [SELECT Id FROM Task WHERE Type = 'CORE_PKL_Web_Form'];
        System.debug('account' + accounts.get(0).CORE_Main_Nationality__pc);

        System.assertEquals(false, accounts.isEmpty());
        System.assertEquals(false, opportunities.isEmpty());
        System.assertEquals(false, opportunityLineItem.isEmpty());
        System.assertEquals(false, pocs.isEmpty());
        System.assertEquals(false, consents.isEmpty());
        System.assertEquals(false, campaignMembers.isEmpty());
        System.assertEquals(false, tasks.isEmpty());

        List<CORE_LeadAcquisitionBuffer__c> buffersAfterUpdate = [Select Id, CORE_Status__c FROM CORE_LeadAcquisitionBuffer__c where CORE_Status__c != :CORE_AsyncLFAWorker.PENDING_STATUS];
        System.assertEquals(buffers.size(), buffersAfterUpdate.size());

        for(CORE_LeadAcquisitionBuffer__c bufferAfterUpdate : buffersAfterUpdate){
            System.assertNotEquals(null, bufferAfterUpdate.CORE_Status__c);
            System.assertNotEquals(CORE_AsyncLFAWorker.PENDING_STATUS, bufferAfterUpdate.CORE_Status__c);
        }
    }
}