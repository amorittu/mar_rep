public without sharing class IT_DynamicLookupHelperWithoutSharing {
    public IT_DynamicLookupHelperWithoutSharing() {

    }

    public static list<sObject> fetchLookupData(String searchKey , String sObjectApiName) {    
        List < sObject > returnList = new List < sObject > ();
        String sWildCardText = '%' + searchKey + '%';
        String sQuery = 'Select Id,Name From ' + sObjectApiName + ' Where Name Like : sWildCardText order by createdDate DESC LIMIT 5';
        for (sObject obj: database.query(sQuery)) {
            returnList.add(obj);
        }
        return returnList;
    }

    // Method to fetch lookup default value 
    @AuraEnabled
    public static sObject fetchDefaultRecord(string recordId , string sObjectApiName) {
        string sRecId = recordId;    
        string sQuery = 'Select Id,Name From ' + sObjectApiName + ' Where Id = : sRecId LIMIT 1';
        for (sObject obj: database.query(sQuery)) {
            return obj;
        }
        return null;
    }
}