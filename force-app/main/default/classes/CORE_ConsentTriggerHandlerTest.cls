@IsTest
public with sharing class CORE_ConsentTriggerHandlerTest {

    @IsTest
    public static void updateInternalReference_Should_return_consents_with_CORE_TECH_InternalReference_field_filled(){
        Account account = CORE_DataFaker_Account.getStudentAccount('test');
        CORE_CatalogHierarchyModel catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test2').catalogHierarchy;


        CORE_Consent__c consent1 = new CORE_Consent__c();
        consent1.CORE_Person_Account__c = account.Id;
        consent1.CORE_Business_Unit__c = catalogHierarchy.businessUnit.Id;
        consent1.CORE_Consent_type__c = 'CORE_Optin_Optout';
        consent1.CORE_Consent_channel__c = 'CORE_PKL_Email';

        CORE_ConsentTriggerHandler.updateInternalReference(new List<CORE_Consent__c>{consent1});

        System.assertNotEquals(null, consent1.CORE_TECH_InternalReference__c);
    }

    @IsTest
    public static void getInternalReference_should_return_formated_internalReference(){
        Account account = CORE_DataFaker_Account.getStudentAccount('test');
        CORE_CatalogHierarchyModel catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test2').catalogHierarchy;


        CORE_Consent__c consent1 = new CORE_Consent__c();
        consent1.CORE_Person_Account__c = account.Id;
        consent1.CORE_Business_Unit__c = catalogHierarchy.businessUnit.Id;
        consent1.CORE_Consent_type__c = 'CORE_Optin_Optout';
        consent1.CORE_Consent_channel__c = 'CORE_PKL_Email';

        String result = CORE_ConsentTriggerHandler.getInternalReference(consent1, '');//CompleteBrand

        String internalRef = consent1.CORE_Person_Account__c 
        + '_' +  consent1.CORE_Business_Unit__c 
        + consent1.CORE_Consent_type__c 
        + consent1.CORE_Consent_channel__c;

        System.assertEquals(internalRef, result);
    }

    @IsTest
    public static void beforeUpdate_must_update_internal_reference_if_needed(){
        Account account = CORE_DataFaker_Account.getStudentAccount('test');
        CORE_CatalogHierarchyModel catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test2').catalogHierarchy;


        CORE_Consent__c consent1 = new CORE_Consent__c();
        consent1.CORE_Person_Account__c = account.Id;        
        consent1.CORE_Division__c = catalogHierarchy.division.Id;
        consent1.CORE_Business_Unit__c = catalogHierarchy.businessUnit.Id;
        consent1.CORE_Consent_type__c = 'CORE_Optin_Optout';
        consent1.CORE_Consent_channel__c = 'CORE_PKL_Email';
        consent1.CORE_Opt_in__c = true;

        insert consent1;
        consent1 = [SELECT ID,CORE_Person_Account__c,CORE_Business_Unit__c,CORE_Consent_type__c,CORE_Consent_channel__c,CORE_TECH_InternalReference__c FROM CORE_Consent__c];
        String internalRefAfterInsert = consent1.CORE_TECH_InternalReference__c;

        System.assertNotEquals(null, internalRefAfterInsert);

        consent1.CORE_Consent_channel__c = 'CORE_PKL_Phone';
        update consent1;
        consent1 = [SELECT ID,CORE_Person_Account__c,CORE_Business_Unit__c,CORE_Consent_type__c,CORE_Consent_channel__c,CORE_TECH_InternalReference__c FROM CORE_Consent__c];

        System.assertNotEquals(consent1.CORE_TECH_InternalReference__c, internalRefAfterInsert);
    }

    /*@IsTest
    public static void afterUpdateOptOutBecameTrueBU(){
        Account account = CORE_DataFaker_Account.getStudentAccount('test');
        Account account2 = CORE_DataFaker_Account.getStudentAccount('test2');
        CORE_CatalogHierarchyModel catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test2').catalogHierarchy;
        CORE_CatalogHierarchyModel catalogHierarchy2 = new CORE_DataFaker_CatalogHierarchy('test3').catalogHierarchy;

        CORE_DataFaker_Consent dataFakerConsent = new CORE_DataFaker_Consent();

        List<CORE_Consent__c> consents = new List<CORE_Consent__c>
        {
            //CORE_PKL_Email Should be added in CORE_Opt_out_channels__c
            //CORE_PKL_Email Should not be added in CORE_Opt_out_channels_marketing__c (type CORE_PKL_Authorisation_to_be_contacted_by_channel)
            dataFakerConsent.Initiate_Consent(catalogHierarchy, account.Id, null,
            null, 'CORE_PKL_Authorisation_to_be_contacted_by_channel', 'CORE_PKL_Email', false, true, 'CORE_PKL_BusinessUnit'),

            // CORE_PKL_Facebook_Messenger Should not be added in CORE_Opt_out_channels__c (TYPE CORE_Optin_Optout)
            // CORE_PKL_Facebook_Messenger Should not be added in CORE_Opt_out_channels_Marketing__c (TYPE CORE_Optin_Optout)
            dataFakerConsent.Initiate_Consent(catalogHierarchy, account.Id, null,
            null, 'CORE_Optin_Optout', 'CORE_PKL_Facebook_Messenger', false, true, 'CORE_PKL_BusinessUnit'),

            //CORE_PKL_Fax Should not be added in CORE_Opt_out_channels__c (type CORE_PKL_Authorisation_to_be_contacted_by_channel_marketing)
            //CORE_PKL_Fax Should be added in CORE_Opt_out_channels_marketing__c 
            dataFakerConsent.Initiate_Consent(catalogHierarchy, account.Id, null,
            null, 'CORE_PKL_Authorisation_to_be_contacted_by_channel_marketing', 'CORE_PKL_Fax', false, true, 'CORE_PKL_BusinessUnit'),

            //CORE_PKL_Instagram Should be added in CORE_Opt_out_channels__c
            //CORE_PKL_Instagram Should be added in CORE_Opt_out_channels_marketing__c 
            dataFakerConsent.Initiate_Consent(catalogHierarchy, account.Id, null,
            null, 'CORE_PKL_Global_consent', 'CORE_PKL_Instagram', false, true, 'CORE_PKL_BusinessUnit'),

            //CORE_PKL_Linkedin Should not be added in CORE_Opt_out_channels__c (CORE_Person_Account__c)
            //CORE_PKL_Linkedin Should not be added in CORE_Opt_out_channels_marketing__c (CORE_Person_Account__c)
            dataFakerConsent.Initiate_Consent(catalogHierarchy, account2.Id, null,
            null, 'CORE_PKL_Global_consent', 'CORE_PKL_Linkedin', false, true, 'CORE_PKL_BusinessUnit'),

            
            //CORE_PKL_Mobile Should not be added in CORE_Opt_out_channels__c (CORE_Person_Account__c)
            //CORE_PKL_Mobile Should not be added in CORE_Opt_out_channels_marketing__c (CORE_Person_Account__c)
            dataFakerConsent.Initiate_Consent(catalogHierarchy, account.Id, null,
            catalogHierarchy2.businessUnit.Id, 'CORE_PKL_Global_consent', 'CORE_PKL_Mobile', false, true, 'CORE_PKL_BusinessUnit'),

            //CORE_PKL_SMS Should be added in CORE_Opt_out_channels__c
            //CORE_PKL_SMS Should not be added in CORE_Opt_out_channels_marketing__c (type CORE_PKL_Authorisation_to_be_contacted_by_channel)
            dataFakerConsent.Initiate_Consent(catalogHierarchy, account.Id, null,
            null, 'CORE_PKL_Authorisation_to_be_contacted_by_channel', 'CORE_PKL_SMS', false, true, null)
        };

        insert consents;

        Opportunity opportunity = CORE_DataFaker_Opportunity.getOpportunity(
            catalogHierarchy.division.Id, 
            catalogHierarchy.businessUnit.Id, 
            account.Id, 
            'Prospect', 
            'Prospect - New'
        );

        for (CORE_Consent__c actualConsent : consents){
            actualConsent.CORE_Opt_out__c = true;
            actualConsent.CORE_Opt_in__c = false;
            actualConsent.CORE_Scope__c = 'CORE_PKL_BusinessUnit';
        }

        update consents;

        opportunity = [SELECT ID,CORE_Opt_out_channels__c, CORE_Opt_out_channels_Marketing__c FROM Opportunity];

        System.assert(opportunity.CORE_Opt_out_channels__c.Contains('CORE_PKL_Email'));
        System.assert(!opportunity.CORE_Opt_out_channels__c.Contains('CORE_PKL_Facebook_Messenger'));
        System.assert(!opportunity.CORE_Opt_out_channels__c.Contains('CORE_PKL_Linkedin'));
        System.assert(!opportunity.CORE_Opt_out_channels__c.Contains('CORE_PKL_Mobile'));
        System.assert(opportunity.CORE_Opt_out_channels__c.Contains('CORE_PKL_Instagram'));
        System.assert(!opportunity.CORE_Opt_out_channels__c.Contains('CORE_PKL_Fax'));
        System.assert(opportunity.CORE_Opt_out_channels__c.Contains('CORE_PKL_SMS'));
        System.assertEquals(46, opportunity.CORE_Opt_out_channels__c.Length());

        System.assert(!opportunity.CORE_Opt_out_channels_Marketing__c.Contains('CORE_PKL_Email'));
        System.assert(!opportunity.CORE_Opt_out_channels_Marketing__c.Contains('CORE_PKL_Facebook_Messenger'));
        System.assert(!opportunity.CORE_Opt_out_channels_Marketing__c.Contains('CORE_PKL_Linkedin'));
        System.assert(!opportunity.CORE_Opt_out_channels_Marketing__c.Contains('CORE_PKL_Mobile'));
        System.assert(opportunity.CORE_Opt_out_channels_Marketing__c.Contains('CORE_PKL_Instagram'));
        System.assert(opportunity.CORE_Opt_out_channels_Marketing__c.Contains('CORE_PKL_Fax'));
        System.assert(!opportunity.CORE_Opt_out_channels_Marketing__c.Contains('CORE_PKL_SMS'));
        System.assertEquals(31, opportunity.CORE_Opt_out_channels_Marketing__c.Length());
    }

    @IsTest
    public static void afterUpdateOptOutBecameTrueDivision(){
        CORE_ConsentTriggerHandler.testScope = 'CORE_PKL_Division';

        Account account = CORE_DataFaker_Account.getStudentAccount('test');
        CORE_CatalogHierarchyModel catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test2').catalogHierarchy;
        CORE_CatalogHierarchyModel catalogHierarchy2 = new CORE_DataFaker_CatalogHierarchy('test3').catalogHierarchy;

        CORE_DataFaker_Consent dataFakerConsent = new CORE_DataFaker_Consent();

        List<CORE_Consent__c> consents = new List<CORE_Consent__c>
        {
            //CORE_PKL_Phone Should be added in CORE_Opt_out_channels__c
            //CORE_PKL_Phone Should not be added in CORE_Opt_out_channels_marketing__c (type CORE_PKL_Authorisation_to_be_contacted_by_channel)
            dataFakerConsent.Initiate_Consent(catalogHierarchy, account.Id, null,
            null, 'CORE_PKL_Authorisation_to_be_contacted_by_channel', 'CORE_PKL_Phone', false, true, 'CORE_PKL_Division'),

            //CORE_PKL_Skype Should not be added in CORE_Opt_out_channels__c (Division Id)
            //CORE_PKL_Skype Should not be added in CORE_Opt_out_channels_marketing__c (type CORE_PKL_Authorisation_to_be_contacted_by_channel)
            dataFakerConsent.Initiate_Consent(catalogHierarchy, account.Id, catalogHierarchy2.division.Id,
            null, 'CORE_PKL_Authorisation_to_be_contacted_by_channel', 'CORE_PKL_Skype', false, true, 'CORE_PKL_Division')
        };

        insert consents;

        Opportunity opportunity = CORE_DataFaker_Opportunity.getOpportunity(
            catalogHierarchy.division.Id, 
            catalogHierarchy.businessUnit.Id, 
            account.Id, 
            'Prospect', 
            'Prospect - New'
        );

        for (CORE_Consent__c actualConsent : consents){
            actualConsent.CORE_Opt_out__c = true;
            actualConsent.CORE_Opt_in__c = false;
            actualConsent.CORE_Scope__c = 'CORE_PKL_Division';
        }

        update consents;

        opportunity = [SELECT ID,CORE_Opt_out_channels__c, CORE_Opt_out_channels_Marketing__c FROM Opportunity];

        System.assert(opportunity.CORE_Opt_out_channels__c.Contains('CORE_PKL_Phone'));
        System.assert(!opportunity.CORE_Opt_out_channels__c.Contains('CORE_PKL_Skype'));
        System.assertEquals(14, opportunity.CORE_Opt_out_channels__c.Length());

        System.assertEquals(null, opportunity.CORE_Opt_out_channels_Marketing__c);
    }

    @IsTest
    public static void afterUpdateOptOutBecameTrueBrand(){
        CORE_ConsentTriggerHandler.testScope = 'CORE_PKL_Brand';

        Account account = CORE_DataFaker_Account.getStudentAccount('test');
        CORE_CatalogHierarchyModel catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test2').catalogHierarchy;
        CORE_DataFaker_Consent dataFakerConsent = new CORE_DataFaker_Consent();

        List<CORE_Consent__c> consents = new List<CORE_Consent__c>
        {
            //CORE_PKL_Tiktok Should be added in CORE_Opt_out_channels__c
            //CORE_PKL_Tiktok Should not be added in CORE_Opt_out_channels_marketing__c (type CORE_PKL_Authorisation_to_be_contacted_by_channel)
            dataFakerConsent.Initiate_Consent(catalogHierarchy, account.Id, null,
            null, 'CORE_PKL_Authorisation_to_be_contacted_by_channel', 'CORE_PKL_Tiktok', false, true, 'CORE_PKL_Brand'),

            //CORE_PKL_Twitter Should be added in CORE_Opt_out_channels__c (Brand )
            //CORE_PKL_Twitter Should not be added in CORE_Opt_out_channels_marketing__c (type CORE_PKL_Authorisation_to_be_contacted_by_channel)
            dataFakerConsent.Initiate_Consent(catalogHierarchy, account.Id, null,
            catalogHierarchy.businessUnit.Id, 'CORE_PKL_Authorisation_to_be_contacted_by_channel', 'CORE_PKL_Twitter', false, true, 'CORE_PKL_Brand')
        };

        insert consents;

        Opportunity opportunity = CORE_DataFaker_Opportunity.getOpportunity(
            catalogHierarchy.division.Id, 
            catalogHierarchy.businessUnit.Id, 
            account.Id, 
            'Prospect', 
            'Prospect - New'
        );

        for (CORE_Consent__c actualConsent : consents){
            actualConsent.CORE_Opt_out__c = true;
            actualConsent.CORE_Opt_in__c = false;
            actualConsent.CORE_Scope__c = 'CORE_PKL_Brand';
        }

        update consents;

        opportunity = [SELECT ID,CORE_Opt_out_channels__c, CORE_Opt_out_channels_Marketing__c FROM Opportunity];

        System.assert(opportunity.CORE_Opt_out_channels__c.Contains('CORE_PKL_Tiktok'));
        System.assert(opportunity.CORE_Opt_out_channels__c.Contains('CORE_PKL_Twitter'));
        System.assertEquals(32, opportunity.CORE_Opt_out_channels__c.Length());

        System.assertEquals(null, opportunity.CORE_Opt_out_channels_Marketing__c);
    }

    @IsTest
    public static void afterUpdateOptInBecameTrueBU(){
        Account account = CORE_DataFaker_Account.getStudentAccount('test');
        Account account2 = CORE_DataFaker_Account.getStudentAccount('test2');
        CORE_CatalogHierarchyModel catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test2').catalogHierarchy;

        CORE_DataFaker_Consent dataFakerConsent = new CORE_DataFaker_Consent();

        List<CORE_Consent__c> consents = new List<CORE_Consent__c>
        {
            //CORE_PKL_Email Should be removed in CORE_Opt_out_channels__c
            //CORE_PKL_Email Should not be removed in CORE_Opt_out_channels_marketing__c (type CORE_PKL_Authorisation_to_be_contacted_by_channel)
            dataFakerConsent.Initiate_Consent(catalogHierarchy, account.Id, null,
            null, 'CORE_PKL_Authorisation_to_be_contacted_by_channel', 'CORE_PKL_Email', true, false, 'CORE_PKL_BusinessUnit'),

            // CORE_PKL_Facebook_Messenger Should not be added in CORE_Opt_out_channels__c (TYPE CORE_Optin_Optout)
            // CORE_PKL_Facebook_Messenger Should not be added in CORE_Opt_out_channels_Marketing__c (TYPE CORE_Optin_Optout)
            dataFakerConsent.Initiate_Consent(catalogHierarchy, account.Id, null,
            null, 'CORE_Optin_Optout', 'CORE_PKL_Facebook_Messenger', true, false, 'CORE_PKL_BusinessUnit'),

            //CORE_PKL_Instagram Should be added in CORE_Opt_out_channels__c
            //CORE_PKL_Instagram Should be added in CORE_Opt_out_channels_marketing__c 
            dataFakerConsent.Initiate_Consent(catalogHierarchy, account.Id, null,
            null, 'CORE_PKL_Global_consent', 'CORE_PKL_Instagram', true, false, 'CORE_PKL_BusinessUnit'),

            //CORE_PKL_Linkedin Should not be added in CORE_Opt_out_channels__c (CORE_Person_Account__c)
            //CORE_PKL_Linkedin Should not be added in CORE_Opt_out_channels_marketing__c (CORE_Person_Account__c)
            dataFakerConsent.Initiate_Consent(catalogHierarchy, account2.Id, null,
            null, 'CORE_PKL_Global_consent', 'CORE_PKL_Linkedin', true, false, 'CORE_PKL_BusinessUnit'),

            //CORE_PKL_SMS Should be added in CORE_Opt_out_channels__c
            //CORE_PKL_SMS Should not be added in CORE_Opt_out_channels_marketing__c (type CORE_PKL_Authorisation_to_be_contacted_by_channel)
            dataFakerConsent.Initiate_Consent(catalogHierarchy, account.Id, null,
            null, 'CORE_PKL_Authorisation_to_be_contacted_by_channel_marketing', 'CORE_PKL_SMS', true, false, null)
        };

        insert consents;

        Opportunity opportunity = CORE_DataFaker_Opportunity.getOpportunity(
            catalogHierarchy.division.Id, 
            catalogHierarchy.businessUnit.Id, 
            account.Id, 
            'Prospect', 
            'Prospect - New'
        );

        opportunity.CORE_Opt_out_channels__c = 'CORE_PKL_Email;CORE_PKL_Facebook_Messenger;CORE_PKL_Instagram;CORE_PKL_Linkedin;CORE_PKL_SMS';
        opportunity.CORE_Opt_out_channels_Marketing__c = 'CORE_PKL_Email;CORE_PKL_Facebook_Messenger;CORE_PKL_Instagram;CORE_PKL_Linkedin;CORE_PKL_SMS';

        update opportunity;

        for (CORE_Consent__c actualConsent : consents){
            actualConsent.CORE_Opt_out__c = false;
            actualConsent.CORE_Opt_in__c = true;
        }

        update consents;

        opportunity = [SELECT ID,CORE_Opt_out_channels__c, CORE_Opt_out_channels_Marketing__c FROM Opportunity];

        System.assert(!opportunity.CORE_Opt_out_channels__c.Contains('CORE_PKL_Email'));
        System.assert(opportunity.CORE_Opt_out_channels__c.Contains('CORE_PKL_Facebook_Messenger'));
        System.assert(!opportunity.CORE_Opt_out_channels__c.Contains('CORE_PKL_Instagram'));
        System.assert(opportunity.CORE_Opt_out_channels__c.Contains('CORE_PKL_Linkedin'));
        System.assert(opportunity.CORE_Opt_out_channels__c.Contains('CORE_PKL_SMS'));
        System.assertEquals(58, opportunity.CORE_Opt_out_channels__c.Length());

        System.assert(opportunity.CORE_Opt_out_channels_Marketing__c.Contains('CORE_PKL_Email'));
        System.assert(opportunity.CORE_Opt_out_channels_Marketing__c.Contains('CORE_PKL_Facebook_Messenger'));
        System.assert(!opportunity.CORE_Opt_out_channels_Marketing__c.Contains('CORE_PKL_Instagram'));
        System.assert(opportunity.CORE_Opt_out_channels_Marketing__c.Contains('CORE_PKL_Linkedin'));
        System.assert(!opportunity.CORE_Opt_out_channels_Marketing__c.Contains('CORE_PKL_SMS'));
        System.assertEquals(60, opportunity.CORE_Opt_out_channels_Marketing__c.Length());
    }

    
    @IsTest
    public static void afterUpdateOptInBecameTrueBrand(){
        CORE_ConsentTriggerHandler.testScope = 'CORE_PKL_Brand';

        Account account = CORE_DataFaker_Account.getStudentAccount('test');
        CORE_CatalogHierarchyModel catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test2').catalogHierarchy;

        CORE_DataFaker_Consent dataFakerConsent = new CORE_DataFaker_Consent();

        List<CORE_Consent__c> consents = new List<CORE_Consent__c>
        {
            //CORE_PKL_Email Should be removed in CORE_Opt_out_channels__c
            //CORE_PKL_Email Should not be removed in CORE_Opt_out_channels_marketing__c (type CORE_PKL_Authorisation_to_be_contacted_by_channel)
            dataFakerConsent.Initiate_Consent(catalogHierarchy, account.Id, null,
            null, 'CORE_PKL_Authorisation_to_be_contacted_by_channel', 'CORE_PKL_Email', true, false, 'CORE_PKL_Brand'),

            //CORE_PKL_Linkedin Should be added in CORE_Opt_out_channels__c (CORE_Person_Account__c)
            //CORE_PKL_Linkedin Should be added in CORE_Opt_out_channels_marketing__c (CORE_Person_Account__c)
            dataFakerConsent.Initiate_Consent(catalogHierarchy, account.Id, null,
            null, 'CORE_PKL_Global_consent', 'CORE_PKL_Facebook_Messenger', true, false, 'CORE_PKL_Brand'),

            //CORE_PKL_SMS Should be added in CORE_Opt_out_channels__c
            //CORE_PKL_SMS Should not be added in CORE_Opt_out_channels_marketing__c (type CORE_PKL_Authorisation_to_be_contacted_by_channel)
            dataFakerConsent.Initiate_Consent(catalogHierarchy, account.Id, null,
            null, 'CORE_PKL_Authorisation_to_be_contacted_by_channel_marketing', 'CORE_PKL_Instagram', true, false, 'CORE_PKL_Brand')
        };

        insert consents;

        Opportunity opportunity = CORE_DataFaker_Opportunity.getOpportunity(
            catalogHierarchy.division.Id, 
            catalogHierarchy.businessUnit.Id, 
            account.Id, 
            'Prospect', 
            'Prospect - New'
        );

        opportunity.CORE_Opt_out_channels__c = 'CORE_PKL_Email;CORE_PKL_Facebook_Messenger;CORE_PKL_Instagram';
        opportunity.CORE_Opt_out_channels_Marketing__c = 'CORE_PKL_Email;CORE_PKL_Facebook_Messenger;CORE_PKL_Instagram';

        update opportunity;

        for (CORE_Consent__c actualConsent : consents){
            actualConsent.CORE_Opt_out__c = false;
            actualConsent.CORE_Opt_in__c = true;
        }

        update consents;

        opportunity = [SELECT ID,CORE_Opt_out_channels__c, CORE_Opt_out_channels_Marketing__c FROM Opportunity];

        System.assert(!opportunity.CORE_Opt_out_channels__c.Contains('CORE_PKL_Email'));
        System.assert(!opportunity.CORE_Opt_out_channels__c.Contains('CORE_PKL_Facebook_Messenger'));
        System.assert(opportunity.CORE_Opt_out_channels__c.Contains('CORE_PKL_Instagram'));
        System.assertEquals(18, opportunity.CORE_Opt_out_channels__c.Length());

        System.assert(opportunity.CORE_Opt_out_channels_Marketing__c.Contains('CORE_PKL_Email'));
        System.assert(!opportunity.CORE_Opt_out_channels_Marketing__c.Contains('CORE_PKL_Facebook_Messenger'));
        System.assert(!opportunity.CORE_Opt_out_channels_Marketing__c.Contains('CORE_PKL_Instagram'));
        System.assertEquals(14, opportunity.CORE_Opt_out_channels_Marketing__c.Length());
    }

    @IsTest
    public static void afterUpdateOptInBecameTrueDivision(){
        CORE_ConsentTriggerHandler.testScope = 'CORE_PKL_Division';

        Account account = CORE_DataFaker_Account.getStudentAccount('test');
        CORE_CatalogHierarchyModel catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test2').catalogHierarchy;

        CORE_DataFaker_Consent dataFakerConsent = new CORE_DataFaker_Consent();

        List<CORE_Consent__c> consents = new List<CORE_Consent__c>
        {
            //CORE_PKL_Email Should be removed in CORE_Opt_out_channels__c
            //CORE_PKL_Email Should not be removed in CORE_Opt_out_channels_marketing__c (type CORE_PKL_Authorisation_to_be_contacted_by_channel)
            dataFakerConsent.Initiate_Consent(catalogHierarchy, account.Id, null,
            null, 'CORE_PKL_Authorisation_to_be_contacted_by_channel', 'CORE_PKL_Email', true, false, 'CORE_PKL_Division'),

            //CORE_PKL_Linkedin Should be added in CORE_Opt_out_channels__c (CORE_Person_Account__c)
            //CORE_PKL_Linkedin Should be added in CORE_Opt_out_channels_marketing__c (CORE_Person_Account__c)
            dataFakerConsent.Initiate_Consent(catalogHierarchy, account.Id, null,
            null, 'CORE_PKL_Global_consent', 'CORE_PKL_Facebook_Messenger', true, false, 'CORE_PKL_Division'),

            //CORE_PKL_SMS Should be added in CORE_Opt_out_channels__c
            //CORE_PKL_SMS Should not be added in CORE_Opt_out_channels_marketing__c (type CORE_PKL_Authorisation_to_be_contacted_by_channel)
            dataFakerConsent.Initiate_Consent(catalogHierarchy, account.Id, null,
            null, 'CORE_PKL_Authorisation_to_be_contacted_by_channel_marketing', 'CORE_PKL_Instagram', true, false, 'CORE_PKL_Division')
        };

        insert consents;

        Opportunity opportunity = CORE_DataFaker_Opportunity.getOpportunity(
            catalogHierarchy.division.Id, 
            catalogHierarchy.businessUnit.Id, 
            account.Id, 
            'Prospect', 
            'Prospect - New'
        );

        opportunity.CORE_Opt_out_channels__c = 'CORE_PKL_Email;CORE_PKL_Facebook_Messenger;CORE_PKL_Instagram';
        opportunity.CORE_Opt_out_channels_Marketing__c = 'CORE_PKL_Email;CORE_PKL_Facebook_Messenger;CORE_PKL_Instagram';

        update opportunity;

        for (CORE_Consent__c actualConsent : consents){
            actualConsent.CORE_Opt_out__c = false;
            actualConsent.CORE_Opt_in__c = true;
        }

        update consents;

        opportunity = [SELECT ID,CORE_Opt_out_channels__c, CORE_Opt_out_channels_Marketing__c FROM Opportunity];

        System.assert(!opportunity.CORE_Opt_out_channels__c.Contains('CORE_PKL_Email'));
        System.assert(!opportunity.CORE_Opt_out_channels__c.Contains('CORE_PKL_Facebook_Messenger'));
        System.assert(opportunity.CORE_Opt_out_channels__c.Contains('CORE_PKL_Instagram'));
        System.assertEquals(18, opportunity.CORE_Opt_out_channels__c.Length());

        System.assert(opportunity.CORE_Opt_out_channels_Marketing__c.Contains('CORE_PKL_Email'));
        System.assert(!opportunity.CORE_Opt_out_channels_Marketing__c.Contains('CORE_PKL_Facebook_Messenger'));
        System.assert(!opportunity.CORE_Opt_out_channels_Marketing__c.Contains('CORE_PKL_Instagram'));
        System.assertEquals(14, opportunity.CORE_Opt_out_channels_Marketing__c.Length());
    }*/
}