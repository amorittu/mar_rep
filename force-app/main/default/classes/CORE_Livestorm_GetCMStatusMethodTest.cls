@IsTest
global class CORE_Livestorm_GetCMStatusMethodTest {
    @TestSetup
    public static void makeData(){
        CORE_Livestorm_DataFaker_InitiateTest.InitiateMemberInLivestorm();
    }

    @isTest
    public static void successNotAttemptExecuteTest() {
        SchedulableContext ctx;

        Test.startTest();
        CORE_Livestorm_GetCMStatusMethod.execute(ctx);
        Test.stopTest();

        List<AsyncApexJob> jobsApexBatch = [select Id, ApexClassID, ApexClass.Name, Status, JobType from AsyncApexJob where JobType = 'BatchApex'];
        //System.assertEquals(4, jobsApexBatch.size(), 'expecting one apex batch job');
    }

    @isTest
    public static void FailExecuteTest() {
        SchedulableContext ctx;

        CampaignMember CampaignMember = [SELECT Id FROM  CampaignMember];
        delete CampaignMember;

        Test.startTest();
        CORE_Livestorm_GetCMStatusMethod.execute(ctx);
        Test.stopTest();

        List<AsyncApexJob> jobsApexBatch = [select Id, ApexClassID, ApexClass.Name, Status, JobType from AsyncApexJob where JobType = 'BatchApex'];
        //System.assertEquals(3, jobsApexBatch.size(), 'expecting one apex batch job');
    }
}