/*******************************************************/
/**     @Author: Valentin Pitel                       **/
/**     @OriginClass: CORE_CatalogHierarchyModel      **/
/*******************************************************/
@IsTest
public class CORE_CatalogHierarchyModelTest {
    @IsTest
    public static void CORE_CatalogHierarchyModel_default_constructor_Should_initialise_sObject_attributes() {
        CORE_CatalogHierarchyModel catalogHierarchy = new CORE_CatalogHierarchyModel();

        System.assertNotEquals(NULL,catalogHierarchy.division,'"division" attribute must not be equal to NULL');
        System.assertNotEquals(NULL,catalogHierarchy.businessUnit,'"businessUnit" attribute must not be equal to NULL');
        System.assertNotEquals(NULL,catalogHierarchy.studyArea,'"studyArea" attribute must not be equal to NULL');
        System.assertNotEquals(NULL,catalogHierarchy.curriculum,'""curriculum" attribute must not be equal to NULL');
        System.assertNotEquals(NULL,catalogHierarchy.level,'"level" attribute must not be equals to NULL');
        System.assertNotEquals(NULL,catalogHierarchy.intake,'"intake" attribute must not be equals to NULL');
        System.assertNotEquals(NULL,catalogHierarchy.product,'"product" attribute must not be equals to NULL');
        System.assertEquals(false,catalogHierarchy.isFullHierarchy,'isFullHierarchy attribute must not be equals to FALSE');
    }

    @IsTest
    public static void CORE_CatalogHierarchyModel_parameterized_constructor_Should_initialise_sObject_attributes_with_ids() {
        CORE_DataFaker_CatalogHierarchy testCatalog = new CORE_DataFaker_CatalogHierarchy('key1');
        CORE_Division__c division = testCatalog.division;
        CORE_Business_Unit__c businessUnit = testCatalog.businessUnit;
        CORE_Study_Area__c studyArea = testCatalog.studyArea;
        CORE_Curriculum__c curriculum = testCatalog.curriculum;
        CORE_Level__c level = testCatalog.level;
        CORE_Intake__c intake = testCatalog.intake;
        Product2 product = testCatalog.product;

        CORE_CatalogHierarchyModel catalogHierarchy = new CORE_CatalogHierarchyModel(division.Id, businessUnit.Id, studyArea.Id, curriculum.Id, level.Id, intake.Id, product.Id);

        System.assertEquals(division.Id,catalogHierarchy.division.Id,'division id must be initialized');
        System.assertEquals(businessUnit.Id,catalogHierarchy.businessUnit.Id,'businessUnit id must be initialized');
        System.assertEquals(studyArea.Id,catalogHierarchy.studyArea.Id,'studyArea id must be initialized');
        System.assertEquals(curriculum.Id,catalogHierarchy.curriculum.Id,'curriculum id must be initialized');
        System.assertEquals(level.Id,catalogHierarchy.level.Id,'level id must be initialized');
        System.assertEquals(intake.Id,catalogHierarchy.intake.Id,'intake id must be initialized');
        System.assertEquals(product.Id,catalogHierarchy.product.Id,'product id must be initialized');
        System.assertEquals(false,catalogHierarchy.isFullHierarchy,'isFullHierarchy attribute must not be equals to FALSE');
    }

    @IsTest
    public static void CORE_CatalogHierarchyModel_parameterized_constructor_Should_initialise_sObject_attributes_with_externalIds() {
        CORE_DataFaker_CatalogHierarchy testCatalog = new CORE_DataFaker_CatalogHierarchy('key1');
        CORE_Division__c division = testCatalog.division;
        CORE_Business_Unit__c businessUnit = testCatalog.businessUnit;
        CORE_Study_Area__c studyArea = testCatalog.studyArea;
        CORE_Curriculum__c curriculum = testCatalog.curriculum;
        CORE_Level__c level = testCatalog.level;
        CORE_Intake__c intake = testCatalog.intake;
        Product2 product = testCatalog.product;

        CORE_CatalogHierarchyModel catalogHierarchy = new CORE_CatalogHierarchyModel(
            division.CORE_Division_ExternalId__c, 
            businessUnit.CORE_Business_Unit_ExternalId__c, 
            studyArea.CORE_Study_Area_ExternalId__c, 
            curriculum.CORE_Curriculum_ExternalId__c, 
            level.CORE_Level_ExternalId__c, 
            intake.CORE_Intake_ExternalId__c
        );

        System.assertEquals(division.CORE_Division_ExternalId__c,catalogHierarchy.division.CORE_Division_ExternalId__c,'division externalId must be initialized');
        System.assertEquals(businessUnit.CORE_Business_Unit_ExternalId__c,catalogHierarchy.businessUnit.CORE_Business_Unit_ExternalId__c,'businessUnit externalId must be initialized');
        System.assertEquals(studyArea.CORE_Study_Area_ExternalId__c,catalogHierarchy.studyArea.CORE_Study_Area_ExternalId__c,'studyArea externalId must be initialized');
        System.assertEquals(curriculum.CORE_Curriculum_ExternalId__c,catalogHierarchy.curriculum.CORE_Curriculum_ExternalId__c,'curriculum externalId must be initialized');
        System.assertEquals(level.CORE_Level_ExternalId__c,catalogHierarchy.level.CORE_Level_ExternalId__c,'level externalId must be initialized');
        System.assertEquals(intake.CORE_Intake_ExternalId__c,catalogHierarchy.intake.CORE_Intake_ExternalId__c,'intake externalId must be initialized');
        System.assertEquals(false,catalogHierarchy.isFullHierarchy,'isFullHierarchy attribute must not be equals to FALSE');
    }
}