/**
 * Created by ftrapani on 18/11/2021.
 */
@IsTest
public with sharing class IT_DataFaker_Objects {

    public static IT_ExtendedInfo__c createExtendedInfo(Id accountId, String brand, Boolean saveData){
        IT_ExtendedInfo__c extendedInfo = new IT_ExtendedInfo__c(
                IT_Account__c = accountId,
                IT_Brand__c = brand
        );

        if(saveData){
            insert extendedInfo;
        }

        return extendedInfo;


    }
}