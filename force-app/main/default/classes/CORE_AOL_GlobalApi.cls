@RestResource(urlMapping='/aol/global-api')
global class CORE_AOL_GlobalApi {

    private static final String MSG_AOL_MISSING_AOL_PARAMS = 'Missing mandatory parameters: ';
    private static final String PARAM_AOL_EXTERNALID = 'aolExternalId';
    private static final String PARAM_AOL_ACCOUNT = 'account';
    private static final String PARAM_AOL_ACADEMIC_DIPLOMA_HISTORY = 'academicDiplomaHistory';
    private static final String PARAM_AOL_FOREIGN_LANG = 'foreignLanguages';
    private static final String PARAM_AOL_SCHOLARSHIP = 'scholarship';
    private static final String PARAM_AOL_APP_FEES_PAID = 'applicationFeesPaid';
    private static final String PARAM_AOL_DOC_SEND_FIELDS = 'documentSendingFields';
    private static final String PARAM_AOL_ONLINE_APP_DOCURL = 'onlineApplicationDocumentUrl';
    private static final String PARAM_AOL_DOC = 'aolDocuments';
    private static final String PARAM_AOL_SELECT_TEST = 'selectionTest';
    private static final String PARAM_AOL_ENROLL_FEES_PAID = 'enrollmentFeesPaid';
    private static final String PARAM_AOL_TUITION_FEES_PAID = 'tuitionFeesPaid';
    private static final String PARAM_AOL_PROG_FIELDS = 'aolProgressionFields';

    @HttpPost
    global static CORE_AOL_Wrapper.ResultWrapperGlobal execute() {

        RestRequest	request = RestContext.request;

        // Initiating logs
        Long dt1 = DateTime.now().getTime();
        Long dt2;
        CORE_LogManager logSettings;
        CORE_LOG__c log = new CORE_LOG__c(CORE_API_Name__c = 'AOL');
        Boolean isErrorLog = false;
        logSettings = new CORE_LogManager('AOL');

        CORE_AOL_Wrapper.ResultWrapperGlobal result = null;

        try {
            Map<String, Object> paramsMap = (Map<String, Object>)JSON.deserializeUntyped(request.requestBody.toString());
            CORE_AOL_Wrapper.AOLProgressionFields aolProgressionFields = (CORE_AOL_Wrapper.AOLProgressionFields)JSON.deserialize((String)JSON.serialize(paramsMap.get(CORE_AOL_Constants.PARAM_AOL_PROG_FIELDS)), CORE_AOL_Wrapper.AOLProgressionFields.class);

            system.debug('@CORE_AOL_GlobalApi > paramsMap: ' + JSON.serializePretty(paramsMap));

            result = checkMandatoryParams(paramsMap);

            if(result != null){
                return result;
            }

            String aolExternalId = (String)paramsMap.get(PARAM_AOL_EXTERNALID);

            CORE_AOL_GlobalWorker aolWorker = new CORE_AOL_GlobalWorker(aolProgressionFields,aolExternalId, paramsMap);
			Opportunity opportunity = aolWorker.aolOpportunity;

            if(opportunity == null){
                result = new CORE_AOL_Wrapper.ResultWrapperGlobal(
                    CORE_AOL_Constants.ERROR_STATUS, 
                    String.format(CORE_AOL_Constants.MSG_OPPORTUNITY_NOT_FOUND, new List<String>{aolExternalId})
                );
            } else {
                /* Check if contains Account related updates  */
                accountProcess(paramsMap, aolWorker);

                /* Check if contains Opportunity related updates */
                opportunityProcess(paramsMap, aolWorker);

                /* Check if contains AOL Documents related upsert */
                documentProcess(paramsMap, aolWorker);

                /* Check if contains Academic Diploma History related upsert */
                academicDiplomaHistoryProcess(paramsMap, aolWorker);
                result = new CORE_AOL_Wrapper.ResultWrapperGlobal(
                    CORE_AOL_Constants.SUCCESS_STATUS,
                    null
                );
            }
        }
        catch(Exception e){

            result = new CORE_AOL_Wrapper.ResultWrapperGlobal(
                    CORE_AOL_Constants.ERROR_STATUS, e.getMessage() + ' at ' + e.getStackTraceString()
            );

            system.debug('@Error Exception: ' + e.getMessage() + ' at ' + e.getStackTraceString());

            //Database.rollback(sp);
        } finally{
            dt2 = DateTime.now().getTime();
            System.debug('CORE_AOL_AcademicDiplomaHistory(urlMapping=\'/aol/academic-diploma-history\') : T0TAL TIME OF EXECUTION = ' + (dt2-dt1) + ' ms');
            // creating log
            log.CORE_Processing_Duration__c =(dt2-dt1);
            log.CORE_HTTP_Status_Code__c = result?.status == CORE_AOL_Constants.ERROR_STATUS ? 400 : result?.status == CORE_AOL_Constants.SUCCESS_STATUS ? 200 : 500;
            log.CORE_Sent_Body__c = JSON.serialize(result);
            log.CORE_Received_Body__c =  request?.requestBody?.toString();
            
            if(!logSettings.isNone()){
                if(logSettings.isAllLogged()){ // is All logged
                    CORE_LogManager.createLog(log);
                }else if(logSettings.isErrorLogged() && isErrorLog){ // is Error
                    CORE_LogManager.createLog(log);
                }
            }
        }

        return result;
    }

    private static void accountProcess(Map<String, Object> paramsMap, CORE_AOL_GlobalWorker aolWorker){

        Account account = null;

        if(checkBlankObject(paramsMap, PARAM_AOL_ACCOUNT)){//Account Civil Status Update
            CORE_AOL_Wrapper.AccountWrapper accWrapper = (CORE_AOL_Wrapper.AccountWrapper)JSON.deserialize(
                    (String)JSON.serialize(paramsMap.get(PARAM_AOL_ACCOUNT), true), CORE_AOL_Wrapper.AccountWrapper.class
            );

            system.debug('@accountProcess > accWrapper: ' + accWrapper);

            account = CORE_AOL_AccountCivilStatus.civilStatusProcess(aolWorker, accWrapper);
        }

        if(checkBlankObject(paramsMap, PARAM_AOL_FOREIGN_LANG)){//Account Language Update
            CORE_AOL_Wrapper.ForeignLanguages foreignLanguages = (CORE_AOL_Wrapper.ForeignLanguages)JSON.deserialize(
                    (String)JSON.serialize(paramsMap.get(PARAM_AOL_FOREIGN_LANG), true), CORE_AOL_Wrapper.ForeignLanguages.class
            );

            account = CORE_AOL_AccountLanguages.languagesProcess(aolWorker, foreignLanguages);
        }

        system.debug('@CORE_AOL_GlobalApi > account: ' + JSON.serializePretty(account));

        if(account != null){
            database.update(account, true);
        }

    }

    private static void opportunityProcess(Map<String, Object> paramsMap, CORE_AOL_GlobalWorker aolWorker){

        Opportunity opportunity = null;

        //Opportunity Scholarship Update
        if(checkBlankObject(paramsMap, PARAM_AOL_SCHOLARSHIP)){
            CORE_AOL_Wrapper.Scholarship scholarship = (CORE_AOL_Wrapper.Scholarship)JSON.deserialize(
                (String)JSON.serialize(paramsMap.get(PARAM_AOL_SCHOLARSHIP), true), CORE_AOL_Wrapper.Scholarship.class
            );

            opportunity = CORE_AOL_OPP_Scholarship.scolarshipProcess(aolWorker.aolOpportunity, scholarship);
        }

        //Opportunity Application Fees Update
        if(checkBlankObject(paramsMap, PARAM_AOL_APP_FEES_PAID)){
            CORE_AOL_Wrapper.ApplicationFeesPaid applicationFeesPaid = (CORE_AOL_Wrapper.ApplicationFeesPaid)JSON.deserialize(
                (String)JSON.serialize(paramsMap.get(PARAM_AOL_APP_FEES_PAID), true), CORE_AOL_Wrapper.ApplicationFeesPaid.class
            );

            opportunity = CORE_AOL_OpportunityFeesPaidApplication.opportunityFeesPaidApplicationProcess(aolWorker, applicationFeesPaid);
        }

        //Opportunity Selection Update
        if(checkBlankObject(paramsMap, PARAM_AOL_SELECT_TEST)){
            CORE_AOL_Wrapper.SelectionTest selection = (CORE_AOL_Wrapper.SelectionTest)JSON.deserialize(
                (String)JSON.serialize(paramsMap.get(PARAM_AOL_SELECT_TEST), true), CORE_AOL_Wrapper.SelectionTest.class
            );

            opportunity = CORE_AOL_OpportunityProcessSelection.opportunityProcessSelectionProcess(aolWorker, selection);
        }

        //Opportunity Fees Paid for Enrollment Update
        if(checkBlankObject(paramsMap, PARAM_AOL_ENROLL_FEES_PAID)){
            CORE_AOL_Wrapper.enrollmentFeesPaid enrollmentFeesPaid = (CORE_AOL_Wrapper.enrollmentFeesPaid)JSON.deserialize(
                (String)JSON.serialize(paramsMap.get(PARAM_AOL_ENROLL_FEES_PAID), true), CORE_AOL_Wrapper.enrollmentFeesPaid.class
            );

            opportunity = CORE_AOL_OpportunityFeesPaidEnrollement.opportunityFeesPaidEnrollementProcess(aolWorker, enrollmentFeesPaid);
        }

        //Opportunity Fees Paid for Enrollment Update
        if(checkBlankObject(paramsMap, PARAM_AOL_TUITION_FEES_PAID)){
            CORE_AOL_Wrapper.TuitionFeesPaid tuitionFeesPaid = (CORE_AOL_Wrapper.TuitionFeesPaid)JSON.deserialize(
                (String)JSON.serialize(paramsMap.get(PARAM_AOL_TUITION_FEES_PAID), true), CORE_AOL_Wrapper.TuitionFeesPaid.class
            );

            opportunity = CORE_AOL_OpportunityFeesPaidTuition.opportunityFeesPaidTuitionProcess(aolWorker, tuitionFeesPaid);
        }

        //Opportunity Document Sending Fields Update
        if(checkBlankObject(paramsMap, PARAM_AOL_DOC_SEND_FIELDS)){
            CORE_AOL_Wrapper.DocumentSendingFields documentSendingFields = (CORE_AOL_Wrapper.DocumentSendingFields)JSON.deserialize(
                (String)JSON.serialize(paramsMap.get(PARAM_AOL_DOC_SEND_FIELDS), true), CORE_AOL_Wrapper.DocumentSendingFields.class
            );

            opportunity = CORE_AOL_OpportunityProcessDocument.opportunityProcessDocumentProcess(aolWorker, documentSendingFields);
        }

        //Opportunity Online Application Document Url Update
        if(!checkNull(paramsMap, PARAM_AOL_ONLINE_APP_DOCURL)){
            opportunity.CORE_OPP_AOL_document_folder_Url__c = (String)paramsMap.get(PARAM_AOL_ONLINE_APP_DOCURL);
        }

        //Opportunity AOL Progression Fields Update
        if(checkBlankObject(paramsMap, PARAM_AOL_PROG_FIELDS)){
            CORE_AOL_Wrapper.AOLProgressionFields aolProgressionFields = (CORE_AOL_Wrapper.AOLProgressionFields)JSON.deserialize(
                (String)JSON.serialize(paramsMap.get(PARAM_AOL_PROG_FIELDS), true), CORE_AOL_Wrapper.AOLProgressionFields.class
            );

            opportunity = CORE_AOL_GlobalWorker.getAolProgressionFields(opportunity, aolProgressionFields);
        }

        system.debug('@CORE_AOL_GlobalApi > opportunity: ' + JSON.serializePretty(opportunity));

        if(opportunity != null){
            database.update(opportunity, true);
        }

    }

    private static void documentProcess(Map<String, Object> paramsMap, CORE_AOL_GlobalWorker aolWorker) {

        //AOL Documents Upsert
        if(checkEmptyArray(paramsMap, PARAM_AOL_DOC)){
            List<CORE_AOL_Wrapper.DocumentWrapper> aolDocuments = (List<CORE_AOL_Wrapper.DocumentWrapper>)JSON.deserialize(
                (String)JSON.serialize(paramsMap.get(PARAM_AOL_DOC), true), List<CORE_AOL_Wrapper.DocumentWrapper>.class
            );

            CORE_AOL_Document.documentProcess(aolWorker, null, aolDocuments);
        }

    }

    private static void academicDiplomaHistoryProcess(Map<String, Object> paramsMap, CORE_AOL_GlobalWorker aolWorker) {
        //Academic Diploma History Upsert
        if(checkEmptyArray(paramsMap, PARAM_AOL_ACADEMIC_DIPLOMA_HISTORY)){
            List<CORE_AOL_Wrapper.AcademicDiplomaHistory> academicDiplomaHistory = (List<CORE_AOL_Wrapper.AcademicDiplomaHistory>)JSON.deserialize(
                    (String)JSON.serialize(paramsMap.get(PARAM_AOL_ACADEMIC_DIPLOMA_HISTORY), true), List<CORE_AOL_Wrapper.AcademicDiplomaHistory>.class
            );

            CORE_AOL_AcademicDiplomaHistory.academicDiplomaHistoryProcess(aolWorker, academicDiplomaHistory);
        }

    }

    private static CORE_AOL_Wrapper.ResultWrapperGlobal checkMandatoryParams(Map<String, Object> paramsMap){
        CORE_AOL_Wrapper.ResultWrapperGlobal result = null;
        String errMsg = '';

        if(checkNull(paramsMap, PARAM_AOL_EXTERNALID)){

            errMsg += MSG_AOL_MISSING_AOL_PARAMS + PARAM_AOL_EXTERNALID;
        }

        if(errMsg != ''){
            result = new CORE_AOL_Wrapper.ResultWrapperGlobal(CORE_AOL_Constants.ERROR_STATUS, errMsg);
        }

        return result;
    }

    private static Boolean checkNull(Map<String, Object> paramsMap, String paramName){
        if(!paramsMap.containsKey(paramName)
            || (paramsMap.containsKey(paramName)
                && (paramsMap.get(paramName) == ''
                    || paramsMap.get(paramName) == null))){

            return true;
        }

        return false;
    }

    private static Boolean checkBlankObject(Map<String, Object> paramsMap, String paramName){
        system.debug('@checkBlankParam > paramName: ' + paramName);
        system.debug('@checkBlankParam > paramsMap.get(paramName): ' + paramsMap.get(paramName));

        if(!paramsMap.containsKey(paramName)){
            return false;
        }
        else{
            Map<String, Object> objMap = (Map<String, Object>)JSON.deserializeUntyped((String)JSON.serialize(paramsMap.get(paramName), true));
            system.debug('@checkBlankParam > objMap: ' + objMap);

            if(objMap.size() == 0){
                return false;
            }
        }

        return true;
    }

    private static Boolean checkEmptyArray(Map<String, Object> paramsMap, String paramName){
        system.debug('@checkEmptyArray > paramName: ' + paramName);
        system.debug('@checkEmptyArray > paramsMap.get(paramName): ' + paramsMap.get(paramName));

        if(!paramsMap.containsKey(paramName)){
            return false;
        }
        else{
            List<Object> objList = (List<Object>)JSON.deserializeUntyped((String)JSON.serialize(paramsMap.get(paramName), true));
            system.debug('@checkEmptyArray > objList: ' + objList);

            if(objList.size() == 0){
                return false;
            }
            else{
                Map<String, Object> objMap = (Map<String, Object>)JSON.deserializeUntyped((String)JSON.serialize(objList[0], true));

                if(objMap.size() == 0){
                    return false;
                }
            }
        }

        return true;
    }
}