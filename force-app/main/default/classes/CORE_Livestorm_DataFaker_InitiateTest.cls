public class CORE_Livestorm_DataFaker_InitiateTest {
    public static void CreateMetadata()
    {
        CORE_DataFaker_CatalogHierarchy catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test1');
        CORE_Business_Unit__c bu = [SELECT Id,CORE_Livestorm_Metadata_Name__c FROM  CORE_Business_Unit__c WHERE Id = :catalogHierarchy.businessUnit.Id];
        bu.CORE_Livestorm_Metadata_Name__c = 'CORE_Unitary_Testing';
        Update bu;
    }

    public static void CreateCampaignWithMember()
    {
        CORE_Livestorm_DataFaker_InitiateTest.CreateMetadata();

        Contact contact = new Contact();
        contact.LastName = 'Test';
        Insert contact;

        List<CORE_Business_Unit__c> bus = [SELECT Id,CORE_Livestorm_Metadata_Name__c FROM  CORE_Business_Unit__c];

        Campaign campaignInit = CORE_DataFaker_Campaign.getCampaign('', true, 'Planned', bus.get(0).Id, '(GMT+03:00) Eastern European Summer Time (Europe/Bucharest)', date.today().addDays(1), date.today().addDays(2));
        campaignInit.CORE_Livestorm_Event_Id__c = 'A';
        update campaignInit;
        CampaignMember campaignMemberInit = CORE_DataFaker_Campaign.getCampaignMember(campaignInit.Id, contact.Id);

    }

    public static void InitiateMemberInLivestorm(){
        CORE_Livestorm_DataFaker_InitiateTest.CreateCampaignWithMember();

        Test.setMock(HttpCalloutMock.class, new successMock());
        SchedulableContext ctx;
        CORE_Livestorm_CreateCMMethod.execute(ctx);
        
        List<Campaign> campaigns = [SELECT Id,CORE_Start_Date_and_Hour__c, CORE_End_Date_and_Hour__c FROM  Campaign];
        Campaign campaign = campaigns.get(0);

        campaign.CORE_Start_Date_and_Hour__c = date.today().addDays(-2);
        campaign.CORE_End_Date_and_Hour__c = date.today().addDays(-1);

        update campaign;
    }

    public class successMock implements HttpCalloutMock {
        public HttpResponse respond(HttpRequest req) {
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');

            if (req.getEndpoint().endsWith('events')){
                res.setBody('{"data": {"id": "2b7de38f-d2d9-4a73-beac-a6ed54d8a38b","attributes": {"registration_link": "https://app.livestorm.co/p/2b7de38f-d2d9-4a73-beac-a6ed54d8a38b"}}}');
                res.setStatusCode(201);
            }
            else if (req.getEndpoint().endsWith('sessions')){
                res.setBody('{"data": {"id": "6b3e4bde-2a75-4ed8-a4af-83a08a68a057","attributes": {"room_link": "https://app.livestorm.co/p/2b7de38f-d2d9-4a73-beac-a6ed54d8a38b/live?s=6b3e4bde-2a75-4ed8-a4af-83a08a68a057"}}}');
                res.setStatusCode(201);
            }
            else if (req.getEndpoint().endsWith('people') && req.getMethod() == 'POST'){
                res.setBody('{"data": {"id": "6b3e4bde-2a75-4ed8-a4af-83a08a68a057","attributes": {"registrant_detail": {"connection_link": "https://app.livestorm.co/p/2b7de38f-d2d9-4a73-beac-a6ed54d8a38b/live?s=6b3e4bde-2a75-4ed8-a4af-83a08a68a057"}}}}');
                res.setStatusCode(201);
            }
            else if (req.getEndpoint().endsWith('people') && req.getMethod() == 'GET'){
                res.setBody('{"data": [{"id": "6b3e4bde-2a75-4ed8-a4af-83a08a68a057","type": "people","attributes": {"registrant_detail": {"attended": false}}}],"meta": {"record_count": 1}}');
                res.setStatusCode(200);
            }

            return res;
        }
    }
}