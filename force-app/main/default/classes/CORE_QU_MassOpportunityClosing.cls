/**
* @author Zameer Khodabocus - Almavia
* @date 2021-12-16
* @group Queueable
* @description Class used by lwc, coreMassOppClosing, to close won/lost opportunities in mass asynchronously
*/
public without sharing class CORE_QU_MassOpportunityClosing implements Queueable {

    public Set<Id> oppsIdSet;
    public String status;
    public List<String> resultsList;

    public CORE_QU_MassOpportunityClosing(Set<Id> oppsIdSet, String status, List<String> resultsList){
        this.oppsIdSet = oppsIdSet;
        this.status = status;
        this.resultsList = resultsList;
    }

    public void execute(QueueableContext context){ 

        CORE_LC_MassOpportunityClosing.SettingWrapper setting = CORE_LC_MassOpportunityClosing.getOrgSetting();

        try {
            Set<Id> remOppsIdSet = oppsIdSet;
            Integer batchSize = (setting.batchSize != null) ? setting.batchSize :CORE_LC_MassOpportunityClosing.defaultBatchSize;

            List<Opportunity> oppsList = [SELECT Id, StageName FROM Opportunity WHERE Id IN :oppsIdSet LIMIT :batchSize];

            for(Opportunity opp : oppsList){
                opp.StageName = status;

                remOppsIdSet.remove(opp.Id);
            }

            List<Database.SaveResult> results = database.update(oppsList, false);

            CORE_MassOpportunityResultHandler resultHandler = new CORE_MassOpportunityResultHandler();
            resultHandler.settingName = CORE_LC_MassOpportunityClosing.EMAIL_SETTING_NAME;
            resultHandler.results = results;
            resultHandler.recordsList = oppsList;
            resultHandler.resultsList = resultsList;
            resultHandler.sendEmail = false;
            resultsList = resultHandler.handleResults();

            if(remOppsIdSet.size() > 0
               && !test.isRunningTest()){//Test class does not support chaining jobs

                System.enqueueJob(new CORE_QU_MassOpportunityClosing(remOppsIdSet, status, resultsList));
            }
            //Job has finished
            else{
                database.update(new CORE_MassOpportunityClosing__c(
                    Id = setting.settingId, CORE_JobInProgress__c = false
                ), true);

                resultHandler.sendMail();
            }
        }
        catch(Exception e){
            system.debug('Exception: ' + e.getMessage() + ' => ' + e.getStackTraceString());

            database.update(new CORE_MassOpportunityClosing__c(
                Id = setting.settingId, CORE_JobInProgress__c = false
            ), true);

            CORE_MassOpportunityResultHandler resultHandler = new CORE_MassOpportunityResultHandler();
            resultHandler.settingName = CORE_LC_MassOpportunityClosing.EMAIL_SETTING_NAME;
            resultHandler.resultsList = resultsList;
            resultHandler.exceptionError = 'APEX Exception Error: ' + e.getMessage() + ' => ' + e.getStackTraceString();

            resultHandler.sendMail();
        }
    }
}