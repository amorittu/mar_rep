/**
 * Created By Fodil 06-05-2022
 */
public with sharing class CORE_BusinessHoursUtils {
    public static DateTime getNextDateTime(String BusinessHourId, DateTime startDate, Long hours) {
        return BusinessHours.add(BusinessHourId,BusinessHours.nextStartDate(businessHourId,startDate),hours*3600*1000);
    }
}