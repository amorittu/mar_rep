public without sharing class IT_CampaignMemberExpirationChecker implements Database.Batchable<sObject>, Database.Stateful   {
    public static final String CLASS_NAME = IT_CampaignMemberExpirationChecker.class.getName();
    public static final String[] DISCRIMINATING_STATUSES = new String[]{'No Show', 'Attended' };
    
    //INPUT PARAMETERS START
    String[] campaignMembersId = new String[]{};
    //INPUT PARAMETERS END

    // OUTPUT PARAMS START
    public Map<String, IT_MainObjectExpirationChecker__mdt> customMetadataStructure = new Map<String, IT_MainObjectExpirationChecker__mdt>();
    public Map<String, IT_ObjectExpirationCheckerBU__mdt> childrenMetadataStructure = new Map<String, IT_ObjectExpirationCheckerBU__mdt>();

    public IT_CampaignMemberExpirationChecker(String campaignMemberId) {
        System.debug(CLASS_NAME + 'constructor single id has been fired ! ');
        this.campaignMembersId.add(campaignMemberId);
        
    }

    public IT_CampaignMemberExpirationChecker(String[] campaignMembersId) {
        System.debug(CLASS_NAME + 'constructor multiple ids has been fired ! ');
        this.campaignMembersId = campaignMembersId;

    }

    public IT_CampaignMemberExpirationChecker() {

    }

    public Database.QueryLocator start(Database.BatchableContext BC){
        
        IT_MainObjectExpirationChecker__mdt[] mainMetadatas = [
            SELECT Id, 
                IT_DaysNumber__c, 
                IT_FieldChecker__c, 
                IT_ObjectIdentifier__c,
                IT_DivisionExternalId__c,
                DeveloperName
            FROM IT_MainObjectExpirationChecker__mdt
            WHERE IT_ObjectIdentifier__c = 'CampaignMember'
        ];

        for (IT_MainObjectExpirationChecker__mdt mainMetadata : mainMetadatas) {
            this.customMetadataStructure.put(mainMetadata.IT_DivisionExternalId__c, mainMetadata);
        }

        IT_ObjectExpirationCheckerBU__mdt[] childrenMetadataBusinessUnits = [
            SELECT Id, 
                IT_BusinessUnitExternalId__c, 
                IT_DaysNumber__c
            FROM IT_ObjectExpirationCheckerBU__mdt
        ];

        for (IT_ObjectExpirationCheckerBU__mdt buMetadata : childrenMetadataBusinessUnits) {
            this.childrenMetadataStructure.put(buMetadata.IT_BusinessUnitExternalId__c, buMetadata);
        }
        
        String soql = 'SELECT Id, Status, CampaignMember.CORE_Opportunity__r.IT_DivisionExternalId__c, ' +
            ' CampaignMember.CORE_Opportunity__r.IT_BusinessUnitExternalId__c, Campaign.StartDate ' +
            ' FROM CampaignMember ';
        if ( campaignMembersId.isEmpty() ) {
            soql += ' WHERE Status NOT IN: DISCRIMINATING_STATUSES AND Campaign.EndDate < TODAY AND Campaign.StartDate != NULL ';
        } else {
            soql += ' WHERE Id IN: campaignMembersId ';
        }

        System.debug(CLASS_NAME + ' - soql : ' + JSON.serialize(soql));
        System.debug(CLASS_NAME + ' - mainMetadatas : ' + JSON.serialize(mainMetadatas));
        System.debug(CLASS_NAME + ' - childrenMetadataBusinessUnits : ' + JSON.serialize(childrenMetadataBusinessUnits));
        return Database.getQueryLocator(soql);
    }

    public void execute(Database.BatchableContext BC, List<CampaignMember> scope){
        System.debug(CLASS_NAME + ' - execute fired ');
        
        
        Map<String, CampaignMember[]> campaign_cms = new Map<String, CampaignMember[]>();
        CampaignMember[] campaignMembersToUpdate = new CampaignMember[]{};
        for(CampaignMember cm : scope){
            System.debug(CLASS_NAME + ' - cm is : ' + cm );
            System.debug(CLASS_NAME + ' - customMetadataStructure is : ' + this.customMetadataStructure.get(cm?.CORE_Opportunity__r?.IT_DivisionExternalId__c) );
            System.debug(CLASS_NAME + ' - cm.CORE_Opportunity__r.IT_DivisionExternalId__c is : ' + cm.CORE_Opportunity__r.IT_DivisionExternalId__c );
            if (customMetadataStructure.containsKey(cm?.CORE_Opportunity__r?.IT_DivisionExternalId__c) ) {
                Date dateChecker = cm.Campaign.StartDate;
                Integer numberOfDays = 0;
                if (customMetadataStructure.get(cm.CORE_Opportunity__r.IT_DivisionExternalId__c).IT_FieldChecker__c  == 'Division') {
                    System.debug(CLASS_NAME + ' - IT_FieldChecker__c == Division ');
                    numberOfDays = Integer.valueOf(customMetadataStructure.get(cm.CORE_Opportunity__r.IT_DivisionExternalId__c).IT_DaysNumber__c) ;
                } else {
                    System.debug(CLASS_NAME + ' - IT_FieldChecker__c == BusinessUnit ');
                    numberOfDays = Integer.valueOf(childrenMetadataStructure.get(cm.CORE_Opportunity__r.IT_BusinessUnitExternalId__c).IT_DaysNumber__c);
                }

                Boolean isCampaignMemberObsolete = calculateDaysFromCampaign(dateChecker, numberOfDays );
                System.debug(CLASS_NAME + ' - isCampaignMemberObsolete : ' + isCampaignMemberObsolete);    
                if (isCampaignMemberObsolete == true) {
                    campaignMembersToUpdate.add(cm);
                }
            }
        }
        if (! campaignMembersToUpdate.isEmpty() ) {
            for (CampaignMember cm : campaignMembersToUpdate) {
                cm.Status = 'No Show';
            }
            System.debug(CLASS_NAME + ' - campaignMembersToUpdate : ' + JSON.serialize(campaignMembersToUpdate));
            update campaignMembersToUpdate;
        }
        System.debug(CLASS_NAME + ' - execute finished ');
    }

    public Boolean calculateDaysFromCampaign(Date dateChecker, Integer numberOfDays) {
        Date finalDate = dateChecker.addDays(numberOfDays);
        System.debug(CLASS_NAME + ' - finalDate ' + finalDate);
        if (finalDate < System.today() ) {
            return true;
        }
        return false;
    }

    public void finish(Database.BatchableContext BC){

    }

}