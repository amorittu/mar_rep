/**
 * Created by amorittu on 21/12/2021.
 * Test Class: IT_EmailChecker_FC_Test
 */
global without sharing class IT_EmailChecker_FC {
    
    @InvocableMethod
    global static List<FlowOutputs> checkEmails(List<FlowInputs> request){
        Boolean isIncluded = false;
        Boolean emailChecks = true;

        System.debug('IT_EmailChecker_FC - checkEmails - request is : ' + JSON.serialize(request) ); 
        //INCOMING PARAMS START 
        Account incomingAccount          = (Account) request.get(0).account;
        Opportunity incomingOpportunity  = (Opportunity)  request.get(0).opportunity;
        //INCOMING PARAMS END 

        System.debug('incomingAccount is : ' + incomingAccount);
        System.debug('incomingOpportunity is : ' + incomingOpportunity);
        EmailMessage[] emailMessages = [SELECT Id, FromAddress, ToAddress 
                                        FROM EmailMessage 
                                        WHERE RelatedToId =: incomingOpportunity.Id 
                                        ORDER BY CreatedDate ASC ];        

        Map<String, Boolean> mapFromEmailToAddress = new Map<String, Boolean>();
        for (IT_Email_Receiver_Checker__mdt singleChecker : [SELECT IT_Receiver_Address__c,IT_is_Included__c FROM IT_Email_Receiver_Checker__mdt ] ) {
            mapFromEmailToAddress.put(singleChecker.IT_Receiver_Address__c, Boolean.valueOf(singleChecker.IT_is_Included__c) );
        }

        for (EmailMessage emailMsg : emailMessages) {
            if ( mapFromEmailToAddress.containsKey(emailMsg.ToAddress) && mapFromEmailToAddress.get(emailMsg.ToAddress) == true) {
                isIncluded = true;
                break;
            }
        }
/*        
        if (!isIncluded) {
            Integer counter = 0;
            for (EmailMessage emailMsg : emailMessages) {
                if ( mapFromEmailToAddress.containsKey(emailMsg.ToAddress) && mapFromEmailToAddress.get(emailMsg.ToAddress) == false  && incomingAccount.PersonEmail == emailMsg.FromAddress ) {
                    counter++;
                }
            }   

            if (counter > 1 ) { // If counter major than one it means we have other emails related to that opportunity with the same from and same to
                emailChecks = false;
            }
        }
*/
        //Escudo, rework on this method.
        //getting latest email that started the process
        Integer emailSize = emailMessages.size();
        EmailMessage lastEmail = emailMessages.remove(emailSize - 1); //should return the last email
        
        if (!isIncluded) {
            Integer counter = 0;
        	for (EmailMessage emailMsg : emailMessages) {
                if (lastEmail.FromAddress == emailMsg.FromAddress &&  lastEmail.ToAddress == emailMsg.ToAddress) {
                    counter++;
                }
            }  
            if (counter > 0 ) { //if more than 0 one email has the same sender and receiver
                emailChecks = false;
            }
        }
        FlowOutputs floOtp = new FlowOutputs();
        floOtp.isIncluded = isIncluded;
        floOtp.emailAccountMatch = emailChecks;

        FlowOutputs[] floOtpList = new FlowOutputs[]{};
        floOtpList.add(floOtp);

        return floOtpList;
    }

    global class FlowInputs{

        @InvocableVariable (required=true)
        global Account account;

        @InvocableVariable (required=true)
        global Opportunity opportunity;
    }

    global class FlowOutputs{

        @InvocableVariable
        global Boolean isIncluded;

        @InvocableVariable
        global Boolean emailAccountMatch;

    }

}