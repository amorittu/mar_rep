@isTest
private class CORE_SIS_DeletedRecords_TEST {

    private static final String INTEREST_EXTERNALID = 'setup';
    private static final String ACADEMIC_YEAR = 'CORE_PKL_2023-2024';

    @testSetup 
    static void createData(){
        Id profileId = [SELECT Id FROM Profile WHERE Name = 'CORE_API_Profile' LIMIT 1].Id;

        User usr = new User(
            Username = 'usertest@aoltest.com',
            LastName = 'TEST_USER',
            Email = 'testemail@yopmail.com',
            Alias = 'T', 
            CommunityNickname = 'T',
            TimeZoneSidKey = 'Europe/Paris',
            LocaleSidKey = 'fr_FR_EURO',
            EmailEncodingKey = 'ISO-8859-1', 
            ProfileId = profileId,
            LanguageLocaleKey = 'fr'      
        );
        database.insert(usr, true);

        CORE_CatalogHierarchyModel catalogHierarchy = new CORE_DataFaker_CatalogHierarchy(INTEREST_EXTERNALID).catalogHierarchy;
        Account account = CORE_DataFaker_Account.getStudentAccount(INTEREST_EXTERNALID);

        Id oppEnroll_RTId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get(CORE_SIS_Constants.OPP_RECORDTYPE_ENROLLMENT).getRecordTypeId();

        Opportunity opp1 = new Opportunity(
            Name ='test',
            StageName = CORE_SIS_Constants.PKL_OPP_STAGENAME_PROSPECT,
            CORE_OPP_Sub_Status__c = CORE_SIS_Constants.PKL_OPP_SUBSTATUS_PROSPECTAPPSTARTED,
            CORE_OPP_Division__c = catalogHierarchy.division.Id,
            CORE_OPP_Business_Unit__c = catalogHierarchy.businessUnit.Id,
            CORE_OPP_Study_Area__c = catalogHierarchy.studyArea.Id,
            CORE_OPP_Curriculum__c = catalogHierarchy.curriculum.Id,
            CORE_OPP_Level__c = catalogHierarchy.level.Id,
            CORE_OPP_Intake__c = catalogHierarchy.intake.Id,
            CORE_OPP_Academic_Year__c = ACADEMIC_YEAR,
            AccountId = account.Id,
            CloseDate = Date.Today().addDays(2),
            OwnerId = UserInfo.getUserId(),
            RecordTypeId = oppEnroll_RTId
        );

        Opportunity opp2 = new Opportunity(
            Name ='test',
            StageName = CORE_SIS_Constants.PKL_OPP_STAGENAME_PROSPECT,
            CORE_OPP_Sub_Status__c = CORE_SIS_Constants.PKL_OPP_SUBSTATUS_PROSPECTAPPSTARTED,
            CORE_OPP_Division__c = catalogHierarchy.division.Id,
            CORE_OPP_Business_Unit__c = catalogHierarchy.businessUnit.Id,
            CORE_OPP_Study_Area__c = catalogHierarchy.studyArea.Id,
            CORE_OPP_Curriculum__c = catalogHierarchy.curriculum.Id,
            CORE_OPP_Level__c = catalogHierarchy.level.Id,
            CORE_OPP_Intake__c = catalogHierarchy.intake.Id,
            CORE_OPP_Academic_Year__c = ACADEMIC_YEAR,
            AccountId = account.Id,
            CloseDate = Date.Today().addDays(2),
            OwnerId = UserInfo.getUserId(),
            CORE_OPP_Main_Opportunity__c = true,
            RecordTypeId = oppEnroll_RTId
        );

        database.insert(new List<Opportunity> { opp1, opp2 }, true);

        List<Opportunity> oppsList = [SELECT Id FROM Opportunity];
        database.delete(oppsList, true);

        List<Account> accounts = [SELECT Id FROM Account];
        database.delete(accounts, true);

        //List<CORE_Deleted_record__c> deletedRecords = [SELECT Id, CORE_Record_Type__c, LastModifiedDate FROM CORE_Deleted_record__c ORDER BY ID];
        //system.debug('@deletedRecords: ' + deletedRecords);
    }

    @isTest
    static void test_HasDeletedOpps(){
        User usr = [SELECT Id FROM User WHERE Username = 'usertest@aoltest.com' LIMIT 1];

        List<PermissionSetAssignment> psaList = new List<PermissionSetAssignment>();

        for(PermissionSet ps : [SELECT Id FROM PermissionSet WHERE Name = 'CORE_API_SIS' OR Name = 'CORE_LeadFormAcquisition']){
            psaList.add(
                new PermissionSetAssignment(AssigneeId = usr.id, PermissionSetId = ps.Id)
            );
        }
        
        database.insert(psaList, true);

        System.runAs(usr){

            String lastDate = date.today().year() + '-' + date.today().month() + '-' + date.today().day();

            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
            req.requestURI = '/services/apexrest/sis/deletedRecords';

            req.httpMethod = 'GET';
            req.params.put(CORE_SIS_Constants.PARAM_LASTMODIFIEDDATE, lastDate);
            req.params.put(CORE_SIS_Constants.PARAM_RECTYPE, CORE_SIS_Constants.OPP_RECORDTYPE_ENROLLMENT);

            RestContext.request = req;
            RestContext.response= res;

            test.startTest();
            CORE_SIS_Wrapper.ResultWrapperDeletedRecords results = CORE_SIS_DeletedRecords.getDeletedRecords();
            test.stopTest();

            System.assertEquals(2, results.totalSize);
        }
    }

    @isTest
    static void test_HasDeletedAccount(){
        User usr = [SELECT Id FROM User WHERE Username = 'usertest@aoltest.com' LIMIT 1];

        List<PermissionSetAssignment> psaList = new List<PermissionSetAssignment>();

        for(PermissionSet ps : [SELECT Id FROM PermissionSet WHERE Name = 'CORE_API_SIS' OR Name = 'CORE_LeadFormAcquisition']){
            psaList.add(
                new PermissionSetAssignment(AssigneeId = usr.id, PermissionSetId = ps.Id)
            );
        }
        
        database.insert(psaList, true);

        System.runAs(usr){

            String lastDate = date.today().year() + '-' + date.today().month() + '-' + date.today().day();

            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
            req.requestURI = '/services/apexrest/sis/deletedRecords';

            req.httpMethod = 'GET';
            req.params.put(CORE_SIS_Constants.PARAM_LASTMODIFIEDDATE, lastDate);
            req.params.put(CORE_SIS_Constants.PARAM_RECTYPE, CORE_SIS_Constants.OPP_RECORDTYPE_STUDENT);

            RestContext.request = req;
            RestContext.response= res;

            test.startTest();
            CORE_SIS_Wrapper.ResultWrapperDeletedRecords results = CORE_SIS_DeletedRecords.getDeletedRecords();
            test.stopTest();

            System.assertEquals(1, results.totalSize);
        }
    }

    @isTest
    static void test_HasDeletedRecordsExceedLimit(){
        User usr = [SELECT Id FROM User WHERE Username = 'usertest@aoltest.com' LIMIT 1];

        List<PermissionSetAssignment> psaList = new List<PermissionSetAssignment>();

        for(PermissionSet ps : [SELECT Id FROM PermissionSet WHERE Name = 'CORE_API_SIS' OR Name = 'CORE_LeadFormAcquisition']){
            psaList.add(
                new PermissionSetAssignment(AssigneeId = usr.id, PermissionSetId = ps.Id)
            );
        }
        
        database.insert(psaList, true);

        System.runAs(usr){

            CORE_SIS_DeletedRecords.limitPerPage = 1;

            String lastDate = date.today().year() + '-' + date.today().month() + '-' + date.today().day();

            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
            req.requestURI = '/services/apexrest/sis/deletedRecords';

            req.httpMethod = 'GET';
            req.params.put(CORE_SIS_Constants.PARAM_LASTMODIFIEDDATE, lastDate);
            req.params.put(CORE_SIS_Constants.PARAM_RECTYPE, CORE_SIS_Constants.OPP_RECORDTYPE_ENROLLMENT);

            RestContext.request = req;
            RestContext.response= res;

            test.startTest();
            CORE_SIS_Wrapper.ResultWrapperDeletedRecords results = CORE_SIS_DeletedRecords.getDeletedRecords();
            test.stopTest();

            System.assertEquals(1, results.totalSize);
        }
    }

    @isTest
    static void test_HasOppsNextPage(){
        User usr = [SELECT Id FROM User WHERE Username = 'usertest@aoltest.com' LIMIT 1];

        List<PermissionSetAssignment> psaList = new List<PermissionSetAssignment>();

        for(PermissionSet ps : [SELECT Id FROM PermissionSet WHERE Name = 'CORE_API_SIS' OR Name = 'CORE_LeadFormAcquisition']){
            psaList.add(
                new PermissionSetAssignment(AssigneeId = usr.id, PermissionSetId = ps.Id)
            );
        }
        
        database.insert(psaList, true);

        System.runAs(usr){

            List<CORE_Deleted_record__c> deletedRecords = [SELECT Id FROM CORE_Deleted_record__c ORDER BY ID];

            CORE_SIS_DeletedRecords.limitPerPage = 1;

            String lastDate = date.today().year() + '-' + date.today().month() + '-' + date.today().day();

            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
            req.requestURI = '/services/apexrest/sis/deletedRecords';

            req.httpMethod = 'GET';
            req.params.put(CORE_SIS_Constants.PARAM_LASTMODIFIEDDATE, lastDate);
            req.params.put(CORE_SIS_Constants.PARAM_RECTYPE, CORE_SIS_Constants.OPP_RECORDTYPE_ENROLLMENT);
            req.params.put(CORE_SIS_Constants.PARAM_LAST_ID, deletedRecords[0].Id);

            RestContext.request = req;
            RestContext.response= res;

            test.startTest();
            CORE_SIS_Wrapper.ResultWrapperDeletedRecords results = CORE_SIS_DeletedRecords.getDeletedRecords();
            test.stopTest();

            System.assertEquals(1, results.totalSize);
            System.assertEquals(deletedRecords[1].Id, results.deletedRecords[0].Id);
        }
    }

    @isTest
    static void test_MissingParams(){
        User usr = [SELECT Id FROM User WHERE Username = 'usertest@aoltest.com' LIMIT 1];

        List<PermissionSetAssignment> psaList = new List<PermissionSetAssignment>();

        for(PermissionSet ps : [SELECT Id FROM PermissionSet WHERE Name = 'CORE_API_SIS' OR Name = 'CORE_LeadFormAcquisition']){
            psaList.add(
                new PermissionSetAssignment(AssigneeId = usr.id, PermissionSetId = ps.Id)
            );
        }
        
        database.insert(psaList, true);

        System.runAs(usr){

            String lastDate = date.today().year() + '-' + date.today().month() + '-' + date.today().day();

            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
            req.requestURI = '/services/apexrest/sis/deletedRecords';

            req.httpMethod = 'GET';

            RestContext.request = req;
            RestContext.response= res;

            test.startTest();
            CORE_SIS_Wrapper.ResultWrapperDeletedRecords results = CORE_SIS_DeletedRecords.getDeletedRecords();
            test.stopTest();

            System.assertEquals(CORE_SIS_Constants.ERROR_STATUS, results.status);
        }
    }
}