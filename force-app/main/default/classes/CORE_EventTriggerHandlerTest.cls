@IsTest
public without sharing class CORE_EventTriggerHandlerTest {
    @TestSetup
    static void makeData(){
        CORE_DataFaker_CatalogHierarchy catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test1');
        Account account = CORE_DataFaker_Account.getStudentAccount('test1');
        Opportunity opportunity = CORE_DataFaker_Opportunity.getOpportunity(catalogHierarchy.division.Id, 
            catalogHierarchy.businessUnit.Id, 
            catalogHierarchy.studyArea.Id,
            catalogHierarchy.curriculum.Id,
            catalogHierarchy.level.Id,
            catalogHierarchy.intake.Id, 
            account.Id, 
            'Lead', 
            'Lead - New'
        ); 
    }

    @IsTest
    public static void insert_with_Opportunity_should_prefil_catalog_fields() {
        Opportunity opportunity = [SELECT Id, CORE_OPP_Division__c, CORE_OPP_Business_Unit__c, CORE_OPP_Study_Area__c, CORE_OPP_Curriculum__c, CORE_OPP_Level__c, CORE_OPP_Intake__c FROM Opportunity];
        Account account = [SELECT Id, FirstName, LastName, Salutation FROM Account];
        Datetime dtNow = DateTime.Now();

        test.startTest();
        User user = CORE_DataFaker_User.getUser('user');
        ServiceResource sr = CORE_DataFaker_ServiceAppointment.getServiceResource(user.Id,'service resource');
        OperatingHours oh = new OperatingHours(NAme='oh 1', Timezone='Europe/Paris');
        insert oh;
        WorkType wtype =  new WorkType();
        wtype.Name='Work Type';
        wtype.CORE_Division__c  = opportunity.CORE_OPP_Division__c;
        wtype.CORE_Business_Unit__c = opportunity.CORE_OPP_Business_Unit__c;
        wtype.CORE_Study_Area__c = opportunity.CORE_OPP_Study_Area__c;
        wtype.CORE_Curriculum__c  = opportunity.CORE_OPP_Curriculum__c;
        wtype.CORE_Level__c  = opportunity.CORE_OPP_Level__c;
        wtype.DurationType='Hours';
        wtype.EstimatedDuration=1.00;
        wtype.CORE_Meeting_Type__c = 'CORE_PKL_Orientation_meeting';
        wtype.OperatingHoursId = oh.Id ;
		insert wtype;
        

        ServiceAppointment sa = CORE_DataFaker_ServiceAppointment.getServiceAppointment(opportunity.Id,wtype.Id, dtNow);
        AssignedResource ar = CORE_DataFaker_ServiceAppointment.getAssignedResource(sa.Id, sr.Id);
        test.stopTest();

        List<Event> event = [SELECT Id, Subject FROM Event];
        System.debug(event);
        System.debug(event.size());
        ServiceAppointment serviceAppointment = [SELECT ID, Subject FROM ServiceAppointment where Id = :sa.Id];

        System.assertEquals(sa.Subject,null);

    }

    @IsTest
    public static void getSaSubject_should_return_valid_subject(){
        String test1 = 'test1';
        String test2 = 'test2';
        String test3 = 'test3';
        String test4 = 'test4';
        String newSubject = test1;

        newSubject += !String.isEmpty(test2) ? test2 + ' - ' + test3 : test3;
        newSubject += !String.isEmpty(newSubject) ?  ' - ' +  test4 : test4;
        newSubject = newSubject.length() >= 255 ? newSubject.left(254) : newSubject;

        String subject = CORE_EventTriggerHandler.getSaSubject(test1, test2, test4, test3);

        System.assertEquals(newSubject, subject);
    }
}