global class CORE_WS_Wrapper {

    global class ResultWrapperConsents {
        global Double totalSize;
        global String nextRecordsUrl;
        global List<CORE_Consent__c> consentsList;

        global String status;
        global String message;

        global ResultWrapperConsents(){
        }

        global ResultWrapperConsents(Double totalSize, String nextRecordsUrl, List<CORE_Consent__c> consentsList, String status){
            this.nextRecordsUrl = nextRecordsUrl;
            this.consentsList = consentsList;
            this.totalSize = totalSize;
            this.status = status;
        }

        global ResultWrapperConsents(String status, String message){
            this.status = status;
            this.message = message;
        }
    }
}