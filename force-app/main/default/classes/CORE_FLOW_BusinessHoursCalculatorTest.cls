/**
 * Test class for CORE_FLOW_BusinessHoursCalculator
 */
@isTest
public  class CORE_FLOW_BusinessHoursCalculatorTest {
    @isTest
    public static void testCORE_FLOW_BusinessHoursCalculatorTest() {
        DateTime dtstart = DateTime.newInstance(2022, 05, 12, 10, 00, 00);
        DateTime dt;
        Test.startTest();
        List<BusinessHours> bhs = [SELECT Id FROM BusinessHours where IsDefault = true LIMIT 1];
        CORE_FLOW_BusinessHoursCalculator.RequestWrapper req = new CORE_FLOW_BusinessHoursCalculator.RequestWrapper();
        List<DateTime> datetimes;
        req.businessHourId = bhs[0].Id;
        req.hours=1;
        req.startDate=DateTime.newInstance(2022, 05, 12, 10, 00, 00);
        if(bhs.size()>0){
             datetimes = CORE_FLOW_BusinessHoursCalculator.getNextDateTime(new List<CORE_FLOW_BusinessHoursCalculator.RequestWrapper>{req});
        }
        dt = datetimes[0];
        Test.stopTest();
        System.assertEquals(dt ,req.startDate.addHours(1) );
    }
}