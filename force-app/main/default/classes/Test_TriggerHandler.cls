@isTest
public class Test_TriggerHandler {

    static final String msgNoBypass     =   'Expected to not bypass since not set custom setting';
    static final String msgBypassAll    =   'Expected to bypass since set bypass All';

    static final String msgHandlerNoEvtBypass   =   'Not expected to bypass since using handler that '+
                                                    'does not bypass single event';

    static final String msgHandlerEvtBypass     =   'Expected to bypass since using handler that '+
                                                    'bypasses each single event';

    static List<Opportunity> listOpp = new List<Opportunity>{new Opportunity()};

    //******    BEFORE INSERT   *********
    static testMethod void beforeInsertNoCustSett() {

        Test.startTest();

        delete getTriggerMgrCustSett();

        MyHandlerNoEvtBypass mhNoByp    = new MyHandlerNoEvtBypass();
        system.assert(!mhNoByp.isBypassBeforeInsert(listOpp), msgNoBypass);

        MyHandlerEvtBypass mhByp    = new MyHandlerEvtBypass();
        system.assert(!mhByp.isBypassBeforeInsert(listOpp), msgNoBypass);

        Test.stopTest();
    }
    
    static testMethod void beforeInsertDisableAll() {

        Test.startTest();

        AT_TriggerManager__c atmCS  =   getTriggerMgrCustSett();
        atmCS.disableAll__c         =   true;
        update atmCS;

        MyHandlerNoEvtBypass mhNoEvtByp    = new MyHandlerNoEvtBypass();
        system.assert(mhNoEvtByp.isBypassBeforeInsert(listOpp), msgBypassAll);

        MyHandlerEvtBypass mhByp    = new MyHandlerEvtBypass();
        system.assert(mhByp.isBypassBeforeInsert(listOpp), msgBypassAll);

        Test.stopTest();
    }
    
    static testMethod void beforeInsertDisable() {

        Test.startTest();

        AT_TriggerManager__c atmCS      =   getTriggerMgrCustSett();
        atmCS.disableBeforeInsert__c    =   true;
        update atmCS;
        
        MyHandlerNoEvtBypass mhNoEvtByp    = new MyHandlerNoEvtBypass();
        system.assert(!mhNoEvtByp.isBypassBeforeInsert(listOpp), msgHandlerNoEvtBypass);

        MyHandlerEvtBypass mhByp    = new MyHandlerEvtBypass();
        system.assert(mhByp.isBypassBeforeInsert(listOpp), msgHandlerEvtBypass);

        Test.stopTest();
    }
    
    //******    BEFORE UPDATE   *********
    static testMethod void beforeUpdateNoCustSett() {

        Test.startTest();

        delete getTriggerMgrCustSett();

        MyHandlerNoEvtBypass mhNoByp    = new MyHandlerNoEvtBypass();
        system.assert(!mhNoByp.isBypassBeforeUpdate(listOpp), msgNoBypass);

        MyHandlerEvtBypass mhByp    = new MyHandlerEvtBypass();
        system.assert(!mhByp.isBypassBeforeUpdate(listOpp), msgNoBypass);

        Test.stopTest();
    }
    
    static testMethod void beforeUpdateDisableAll() {

        Test.startTest();

        AT_TriggerManager__c atmCS  =   getTriggerMgrCustSett();
        atmCS.disableAll__c         =   true;
        update atmCS;

        MyHandlerNoEvtBypass mhNoEvtByp    = new MyHandlerNoEvtBypass();
        system.assert(mhNoEvtByp.isBypassBeforeUpdate(listOpp), msgBypassAll);

        MyHandlerEvtBypass mhByp    = new MyHandlerEvtBypass();
        system.assert(mhByp.isBypassBeforeUpdate(listOpp), msgBypassAll);

        Test.stopTest();
    }
    
    static testMethod void beforeUpdateDisable() {

        Test.startTest();

        AT_TriggerManager__c atmCS      =   getTriggerMgrCustSett();
        atmCS.disableBeforeUpdate__c    =   true;
        update atmCS;
        
        MyHandlerNoEvtBypass mh    = new MyHandlerNoEvtBypass();
        system.assert(!mh.isBypassBeforeUpdate(listOpp), msgHandlerNoEvtBypass);

        MyHandlerEvtBypass mhb = new MyHandlerEvtBypass();
        system.assert(mhb.isBypassBeforeUpdate(listOpp), msgHandlerEvtBypass);

        Test.stopTest();
    }
    
    //******    BEFORE DELETE   *********
    static testMethod void beforeDeleteNoCustSett() {

        Test.startTest();

        delete getTriggerMgrCustSett();

        MyHandlerNoEvtBypass mhNoByp    = new MyHandlerNoEvtBypass();
        system.assert(!mhNoByp.isBypassBeforeDelete(listOpp), msgNoBypass);

        MyHandlerEvtBypass mhByp    = new MyHandlerEvtBypass();
        system.assert(!mhByp.isBypassBeforeDelete(listOpp), msgNoBypass);

        Test.stopTest();
    }
    
    static testMethod void beforeDeleteDisableAll() {

        Test.startTest();

        AT_TriggerManager__c atmCS  =   getTriggerMgrCustSett();
        atmCS.disableAll__c         =   true;
        update atmCS;

        MyHandlerNoEvtBypass mhNoEvtByp    = new MyHandlerNoEvtBypass();
        system.assert(mhNoEvtByp.isBypassBeforeDelete(listOpp), msgBypassAll);

        MyHandlerEvtBypass mhByp    = new MyHandlerEvtBypass();
        system.assert(mhByp.isBypassBeforeDelete(listOpp), msgBypassAll);

        Test.stopTest();
    }
    
    static testMethod void beforeDeleteDisable() {

        Test.startTest();

        AT_TriggerManager__c atmCS      =   getTriggerMgrCustSett();
        atmCS.disableBeforeDelete__c    =   true;
        update atmCS;
        
        MyHandlerNoEvtBypass mh    = new MyHandlerNoEvtBypass();
        system.assert(!mh.isBypassBeforeDelete(listOpp), msgHandlerNoEvtBypass);

        MyHandlerEvtBypass mhb = new MyHandlerEvtBypass();
        system.assert(mhb.isBypassBeforeDelete(listOpp), msgHandlerEvtBypass);

        Test.stopTest();
    }
    
    //******    AFTER INSERT    *********
    static testMethod void afterInsertNoCustSett() {

        Test.startTest();

        delete getTriggerMgrCustSett();

        MyHandlerNoEvtBypass mhNoByp    = new MyHandlerNoEvtBypass();
        system.assert(!mhNoByp.isBypassAfterInsert(listOpp), msgNoBypass);

        MyHandlerEvtBypass mhByp    = new MyHandlerEvtBypass();
        system.assert(!mhByp.isBypassAfterInsert(listOpp), msgNoBypass);

        Test.stopTest();
    }
    
    static testMethod void afterInsertDisableAll() {

        Test.startTest();

        AT_TriggerManager__c atmCS  =   getTriggerMgrCustSett();
        atmCS.disableAll__c         =   true;
        update atmCS;

        MyHandlerNoEvtBypass mhNoEvtByp    = new MyHandlerNoEvtBypass();
        system.assert(mhNoEvtByp.isBypassAfterInsert(listOpp), msgBypassAll);

        MyHandlerEvtBypass mhByp    = new MyHandlerEvtBypass();
        system.assert(mhByp.isBypassAfterInsert(listOpp), msgBypassAll);

        Test.stopTest();
    }
    
    static testMethod void afterInsertDisable() {

        Test.startTest();

        AT_TriggerManager__c atmCS  =   getTriggerMgrCustSett();
        atmCS.disableAfterInsert__c =   true;
        update atmCS;
        
        Opportunity opp =   new Opportunity();
        
        MyHandlerNoEvtBypass mh    = new MyHandlerNoEvtBypass();
        system.assert(!mh.isBypassAfterInsert(listOpp), msgHandlerNoEvtBypass);

        MyHandlerEvtBypass mhb = new MyHandlerEvtBypass();
        system.assert(mhb.isBypassAfterInsert(listOpp), msgHandlerEvtBypass);

        Test.stopTest();
    }
    
    //******    AFTER UPDATE    *********
    static testMethod void afterUpdateNoCustSett() {

        Test.startTest();

        delete getTriggerMgrCustSett();

        MyHandlerNoEvtBypass mhNoByp    = new MyHandlerNoEvtBypass();
        system.assert(!mhNoByp.isBypassAfterUpdate(listOpp), msgNoBypass);

        MyHandlerEvtBypass mhByp    = new MyHandlerEvtBypass();
        system.assert(!mhByp.isBypassAfterUpdate(listOpp), msgNoBypass);

        Test.stopTest();
    }
    
    static testMethod void afterUpdateDisableAll() {

        Test.startTest();

        AT_TriggerManager__c atmCS  =   getTriggerMgrCustSett();
        atmCS.disableAll__c         =   true;
        update atmCS;

        MyHandlerNoEvtBypass mhNoEvtByp    = new MyHandlerNoEvtBypass();
        system.assert(mhNoEvtByp.isBypassAfterUpdate(listOpp), msgBypassAll);

        MyHandlerEvtBypass mhByp    = new MyHandlerEvtBypass();
        system.assert(mhByp.isBypassAfterUpdate(listOpp), msgBypassAll);

        Test.stopTest();
    }
    
    static testMethod void afterUpdateDisable() {

        Test.startTest();

        AT_TriggerManager__c atmCS  =   getTriggerMgrCustSett();
        atmCS.disableAfterUpdate__c =   true;
        update atmCS;
        
        MyHandlerNoEvtBypass mh    = new MyHandlerNoEvtBypass();
        system.assert(!mh.isBypassAfterUpdate(listOpp), msgHandlerNoEvtBypass);

        MyHandlerEvtBypass mhb = new MyHandlerEvtBypass();
        system.assert(mhb.isBypassAfterUpdate(listOpp), msgHandlerEvtBypass);

        Test.stopTest();
    }
    
    //******    AFTER DELETE    *********
    static testMethod void afterDeleteNoCustSett() {

        Test.startTest();

        delete getTriggerMgrCustSett();

        MyHandlerNoEvtBypass mhNoByp    = new MyHandlerNoEvtBypass();
        system.assert(!mhNoByp.isBypassAfterDelete(listOpp), msgNoBypass);

        MyHandlerEvtBypass mhByp    = new MyHandlerEvtBypass();
        system.assert(!mhByp.isBypassAfterDelete(listOpp), msgNoBypass);

        Test.stopTest();
    }
    
    static testMethod void afterDeleteDisableAll() {

        Test.startTest();

        AT_TriggerManager__c atmCS  =   getTriggerMgrCustSett();
        atmCS.disableAll__c         =   true;
        update atmCS;

        MyHandlerNoEvtBypass mhNoEvtByp    = new MyHandlerNoEvtBypass();
        system.assert(mhNoEvtByp.isBypassAfterDelete(listOpp), msgBypassAll);

        MyHandlerEvtBypass mhByp    = new MyHandlerEvtBypass();
        system.assert(mhByp.isBypassAfterDelete(listOpp), msgBypassAll);

        Test.stopTest();
    }
    
    static testMethod void afterDeleteDisable() {
        
        Test.startTest();

        AT_TriggerManager__c atmCS  =   getTriggerMgrCustSett();
        atmCS.disableAfterDelete__c =   true;
        update atmCS;

        MyHandlerNoEvtBypass mh    = new MyHandlerNoEvtBypass();
        system.assert(!mh.isBypassAfterDelete(listOpp), msgHandlerNoEvtBypass);

        MyHandlerEvtBypass mhb = new MyHandlerEvtBypass();
        system.assert(mhb.isBypassAfterDelete(listOpp), msgHandlerEvtBypass);

        Test.stopTest();
    }
    
    //******    AFTER UNDELETE    *********
    static testMethod void afterUndeleteNoCustSett() {

        Test.startTest();

        delete getTriggerMgrCustSett();

        MyHandlerNoEvtBypass mhNoByp    = new MyHandlerNoEvtBypass();
        system.assert(!mhNoByp.isBypassAfterUndelete(listOpp), msgNoBypass);

        MyHandlerEvtBypass mhByp    = new MyHandlerEvtBypass();
        system.assert(!mhByp.isBypassAfterUndelete(listOpp), msgNoBypass);

        Test.stopTest();
    }
    
    static testMethod void afterUndeleteDisableAll() {

        Test.startTest();

        AT_TriggerManager__c atmCS  =   getTriggerMgrCustSett();
        atmCS.disableAll__c         =   true;
        update atmCS;

        MyHandlerNoEvtBypass mhNoEvtByp    = new MyHandlerNoEvtBypass();
        system.assert(mhNoEvtByp.isBypassAfterUndelete(listOpp), msgBypassAll);

        MyHandlerEvtBypass mhByp    = new MyHandlerEvtBypass();
        system.assert(mhByp.isBypassAfterUndelete(listOpp), msgBypassAll);

        Test.stopTest();
    }
    
    static testMethod void afterUndeleteDisable() {

        Test.startTest();

        AT_TriggerManager__c atmCS      =   getTriggerMgrCustSett();
        atmCS.disableAfterUndelete__c   =   true;
        update atmCS;
        
        MyHandlerNoEvtBypass mh    = new MyHandlerNoEvtBypass();
        system.assert(!mh.isBypassAfterUndelete(listOpp), msgHandlerNoEvtBypass);

        MyHandlerEvtBypass mhb = new MyHandlerEvtBypass();
        system.assert(mhb.isBypassAfterUndelete(listOpp), msgHandlerEvtBypass);

        Test.stopTest();
    }
    
    //******    COVERAGE QUERY ON EXISTING METADATA *********
    static testMethod void getMetadata(){
        
        TriggerHandler    th  =   new TriggerHandler();
        th.queryMetadata(null);
    }
    //
    
    static  AT_TriggerManager__c    getTriggerMgrCustSett(){
        
        return  [select id, disableAll__c, disableBeforeInsert__c, disableBeforeUpdate__c, disableBeforeDelete__c,
                        disableAfterInsert__c, disableAfterUpdate__c, disableAfterDelete__c, disableAfterUndelete__c
                from    AT_TriggerManager__c
                limit 1];
    }
    
    @testSetup
    static void generateData(){
        
        AT_TriggerManager__c atmCS  =   new AT_TriggerManager__c(SetupOwnerId=UserInfo.getUserId());
        insert atmCS;
    }
    
    public  class MyHandlerNoEvtBypass extends TriggerHandler{
        
        //Avoid referencing existing metadata
        override    List<AT_SObjTriggerMgr__mdt>    queryMetadata(String sObjName){
            
            return  new List<AT_SObjTriggerMgr__mdt>{
                        new AT_SObjTriggerMgr__mdt( BypassAfterDelete__c    =   false,
                                                    BypassAfterInsert__c    =   false,
                                                    BypassAfterUndelete__c  =   false,
                                                    BypassAfterUpdate__c    =   false,
                                                    BypassBeforeDelete__c   =   false,
                                                    BypassBeforeInsert__c   =   false,
                                                    BypassBeforeUpdate__c   =   false)
                    };
        }
        //
    }
    
    public  class MyHandlerEvtBypass extends TriggerHandler{
        
        //Avoid referencing existing metadata
        override    List<AT_SObjTriggerMgr__mdt>    queryMetadata(String sObjName){
            
            return  new List<AT_SObjTriggerMgr__mdt>{
                        new AT_SObjTriggerMgr__mdt( BypassAfterDelete__c    =   true,
                                                    BypassAfterInsert__c    =   true,
                                                    BypassAfterUndelete__c  =   true,
                                                    BypassAfterUpdate__c    =   true,
                                                    BypassBeforeDelete__c   =   true,
                                                    BypassBeforeInsert__c   =   true,
                                                    BypassBeforeUpdate__c   =   true)
                    };
        }
        //
    }

}