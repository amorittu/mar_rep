/**
* @author Fodil BOUDJEDIEN - Almavia
* @date 2022-04-01
* @group LogManager
* @description Test Class for CORE_LogManager
* @test class CORE_LogManagerTest
*/
@isTest
public without sharing class CORE_LogManagerTest {

    @IsTest
    public static void testLogManager(){
        
        Test.startTest();
        //LeadForm Custom MetadataType should be created in the org
        CORE_LogManager logSettings = new CORE_LogManager('LeadForm');
        logSettings.isAllLogged();
        logSettings.isErrorLogged();
        logSettings.isNone();
        logSettings.getCustomMetadataType();
        CORE_LogManager.createLog(new CORE_LOG__c( CORE_Sent_Body__c='{}' ));
        
        Test.stopTest();
        System.assertEquals(1,[Select Id from CORE_LOG__c].size() );
        
    }
}