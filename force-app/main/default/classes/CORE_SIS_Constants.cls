public with sharing class CORE_SIS_Constants {

     //CORE Prefix
     public static final String PREFIX_CORE = 'CORE_';

     //PARAM values
     public static final String PARAM_SF_ID = 'salesforceId';
     public static final String PARAM_SISSTUDENT_ID = 'sisStudentId';
     public static final String PARAM_SISOPP_ID = 'sisOpportunityId';
     public static final String PARAM_AOLOPP_ID = 'aolOpportunityId';
     public static final String PARAM_ADD_FIELDS = 'additionalFields';
     public static final String PARAM_LASTDATE = 'lastDate';
     public static final String PARAM_LASTMODIFIEDDATE = 'lastModifiedDate';
     public static final String PARAM_OPPSTATUS = 'opportunityStatus';
     public static final String PARAM_RECTYPE = 'recordType';
     public static final String PARAM_OPPRECTYPE = 'opportunityRecordTypeName';
     public static final String PARAM_LAST_ID = 'lastId';
     public static final String PARAM_OPPPROD_ID = 'opportunityProductId';
     public static final String PARAM_PA_ID = 'personAccountId';

     /* ACCOUNT OBJECT */
     //recordTypes
     public static final String OPP_RECORDTYPE_STUDENT = 'Core_Student';

     /* OPPORTUNITY OBJECT */
     //recordTypes
     public static final String OPP_RECORDTYPE_ENROLLMENT = 'CORE_Enrollment';

     //StageName
     public static final String PKL_OPP_STAGENAME_PROSPECT = 'Prospect';
     public static final String PKL_OPP_STAGENAME_LEAD = 'Lead';
     public static final String PKL_OPP_STAGENAME_APPLICANT = 'Applicant';
     public static final String PKL_OPP_STAGENAME_ADMITTED = 'Admitted';
     public static final String PKL_OPP_STAGENAME_REGISTERED = 'Registered';
     public static final String PKL_OPP_STAGENAME_ENROLLED = 'Enrolled';
     public static final String PKL_OPP_STAGENAME_WITHDRAW = 'Withdraw';
    
     //Sub-status
     public static final String PKL_OPP_SUBSTATUS_PROSPECTNEW = 'Prospect - New';
     public static final String PKL_OPP_SUBSTATUS_PROSPECTAPPSTARTED = 'Prospect - Application Started';
     public static final String PKL_OPP_SUBSTATUS_LEADAPPSTARTED = 'Lead - Application Started';
     public static final String PKL_OPP_SUBSTATUS_APPLICANT_APP_SUBMITTED = 'Applicant - Application Submitted / New';
     public static final String PKL_OPP_SUBSTATUS_APPLICANT_APP_PROGRESSING = 'Applicant - Application Incomplete / Progressing';
     public static final String PKL_OPP_SUBSTATUS_APPLICANT_APP_COMPLETED = 'Applicant - Application Completed / Eligible';
     public static final String PKL_OPP_SUBSTATUS_ADMITTED_ADMIT = 'Admitted - Admit';	
     public static final String PKL_OPP_SUBSTATUS_REGISTERED_CONTRACT_RECEIVED = 'Registered - Contract received';
     public static final String PKL_OPP_SUBSTATUS_REGISTERED_CLASSICAL = 'Registered - Classical';
     public static final String PKL_OPP_SUBSTATUS_REGISTERED_TUITION_FEES_PAYMENTS ='Registered - Tuition Fees payments';
     public static final String PKL_OPP_SUBSTATUS_REGISTERED_AWAITING_FUNDING = 'Registered - Awaiting Funding';
     public static final String PKL_OPP_SUBSTATUS_REGISTERED_ENROLLMENT_FEES_PAID = 'Registered - Enrollment Fees Paid';
     public static final String PKL_OPP_SUBSTATUS_REGISTERED_ENROLLMENT_AND_TUITION_FEES_PAID = 'Registered - Enrollment & Tuition Fees Paid';
     public static final String PKL_OPP_SUBSTATUS_REGISTERED_FEES_FULLY_PAID = 'Registered - Fees Fully Paid';
     public static final String PKL_OPP_SUBSTATUS_REGISTERED_AWAITING_VISA = 'Registered - Awaiting Visa';
     public static final String PKL_OPP_SUBSTATUS_REGISTERED_VISA_ISSUED = 'Registered - Visa Issued';
     public static final String PKL_OPP_SUBSTATUS_REGISTERED_EXIT_WITHDRAW = 'Registered - Exit - Withdraw';
     public static final String PKL_OPP_SUBSTATUS_REGISTERED_EXIT_ABANDONED = 'Registered - Exit - Abandoned';
     public static final String PKL_OPP_SUBSTATUS_REGISTERED_EXIT_CANCELLED_REIMBURSEMENT = 'Registered - Exit - Cancelled reimbursement';
     public static final String PKL_OPP_SUBSTATUS_REGISTERED_EXIT_BEFORE_COURSE_START_DEPOSIT_DEADLINE = 'Registered - Exit - Before course start - deposit deadline';
     public static final String PKL_OPP_SUBSTATUS_REGISTERED_EXIT_AFTER_COURSE_START_WITHDRAWAL_PERIOD = 'Registered - Exit - After course start - withdrawal period';
     public static final String PKL_OPP_SUBSTATUS_REGISTERED_EXIT_AFTER_COURSE_START_DEPOSIT_PERIOD = 'Registered - Exit - After course start - deposit period';
     public static final String PKL_OPP_SUBSTATUS_REGISTERED_EXIT_AFTER_COURSE_START_OUT_OF_TIME = 'Registered - Exit - After course start - Out of time';
     public static final String PKL_OPP_SUBSTATUS_REGISTERED_EXIT_WAITING_FOR_DECISION = 'Registered - Exit - Waiting for decision';
     public static final String PKL_OPP_SUBSTATUS_REGISTERED_EXIT_FORCE_MAJEURE = 'Registered - Exit - Force majeure';
     public static final String PKL_OPP_SUBSTATUS_REGISTERED_EXIT_CANCELLED_NO_REIMBURSEMENT = 'Registered - Exit - Cancelled no reimbursement';
     public static final String PKL_OPP_SUBSTATUS_REGISTERED_EXIT_VISA_DENIAL = 'Registered - Exit - Visa denial';
     public static final String PKL_OPP_SUBSTATUS_WITHDRAW_FORCE_MAJEURE = 'Withdraw - Force majeure';
     public static final String PKL_OPP_SUBSTATUS_WITHDRAW_VISA_DENIED = 'Withdraw - Visa Denied';
     public static final String PKL_OPP_SUBSTATUS_WITHDRAW_WAITING_FOR_DECISION = 'Withdraw - Waiting for decision';
     public static final String PKL_OPP_SUBSTATUS_WITHDRAW_NO_SHOW = 'Withdraw - No show';
     public static final String PKL_OPP_SUBSTATUS_WITHDRAW_SHOW_ABANDONED = 'Withdraw - Show Abandoned';
     public static final String PKL_OPP_SUBSTATUS_WITHDRAW_CANCELLED_REIMBURSEMENT = 'Withdraw - Cancelled reimbursement';
     public static final String PKL_OPP_SUBSTATUS_WITHDRAW_CANCELLED_NO_REIMBURSEMENT = 'Withdraw - Cancelled no reimbursement';
     public static final String PKL_OPP_SUBSTATUS_WITHDRAW_TERM_DEFERRAL = 'Withdraw - Term Deferral';
     public static final String PKL_OPP_SUBSTATUS_WITHDRAW_REORIENTED = 'Withdraw - Reoriented';
     public static final String PKL_OPP_SUBSTATUS_ENROLLED_CLASSICAL = 'Enrolled - Classical';
     public static final String PKL_OPP_SUBSTATUS_ENROLLED_EXIT_ABANDONED = 'Enrolled - Exit - Abandoned';
     public static final String PKL_OPP_SUBSTATUS_ENROLLED_EXIT_CANCELLED_REIMBURSEMENT = 'Enrolled - Exit - Cancelled reimbursement';
     public static final String PKL_OPP_SUBSTATUS_ENROLLED_EXIT_BEFORE_COURSE_START_DEPOSIT_DEADLINE = 'Enrolled - Exit - Before course start - deposit deadline';
     public static final String PKL_OPP_SUBSTATUS_ENROLLED_EXIT_AFTER_COURSE_START_WITHDRAWAL_PERIOD = 'Enrolled - Exit - After course start - withdrawal period';
     public static final String PKL_OPP_SUBSTATUS_ENROLLED_EXIT_AFTER_COURSE_START_DEPOSIT_PERIOD = 'Enrolled - Exit - After course start - deposit period';
     public static final String PKL_OPP_SUBSTATUS_ENROLLED_EXIT_AFTER_COURSE_START_OUT_OF_TIME = 'Enrolled - Exit - After course start - Out of time';
     public static final String PKL_OPP_SUBSTATUS_ENROLLED_EXIT_WAITING_FOR_DECISION = 'Enrolled - Exit - Waiting for decision';
     public static final String PKL_OPP_SUBSTATUS_ENROLLED_EXIT_FORCE_MAJEURE = 'Enrolled - Exit - Force majeure';
     public static final String PKL_OPP_SUBSTATUS_ENROLLED_EXIT_CANCELLED_NO_REIMBURSEMENT = 'Enrolled - Exit - Cancelled no reimbursement';
     public static final String PKL_OPP_SUBSTATUS_ENROLLED_EXIT_VISA_DENIAL = 'Enrolled - Exit - Visa denial';   

     /* Product object */
     //CORE_Product_Type__c
     public static final String PKL_PRODUCT_TYPE = 'CORE_PKL_Short_Course';

     /* Acquisition Point of contact OBJECT */
     //CORE_Enrollment_Channel__c
     public static final String PKL_ACQPOC_ENROLCHANNEL_DIRECT = 'CORE_PKL_Direct';
     public static final String PKL_ACQPOC_ENROLCHANNEL_RETAIL = 'CORE_PKL_Retail';

     //CORE_Insertion_Mode__c
     public static final String PKL_ACQPOC_INSERTMODE_AUTOMATIC = 'CORE_PKL_Automatic';

     //CORE_Nature__c
     public static final String PKL_ACQPOC_NATURE_ONLINE = 'CORE_PKL_Online';

     //CORE_Capture_Channel__c
     public static final String PKL_ACQPOC_CAPCHANNEL_SIS = 'CORE_PKL_SIS';//SIS value missing

     /* API values */
     //API SUCCESS STATUS 
     public static final String SUCCESS_STATUS = 'OK';

     //API ERROR STATUS 
     public static final String ERROR_STATUS = 'KO';

     //API ERROR MESSAGES
     public static final String MSG_MISSING_PARAMETERS = 'Missing mandatory parameter : ';
     public static final String MSG_INVALID_FIELDS = 'INVALID FIELD IN REQUEST';

     //API STATUS OPP EXIST
     public static final String OPPORTUNITY_EXIST = 'Yes';
     public static final String OPPORTUNITY_PENDING = 'Pending';
     public static final String OPPORTUNTIY_NOT_EXIST = 'No';

     //OPPORTUNITY NOT FOUND msg
     public static final String MSG_OPPORTUNITY_NOT_FOUND = 'Opportunity not found with sisStudentID = "{0}"';   
     public static final String MSG_OPPORTUNITY_PENDING = 'Opportunity with sisStudentID = "{0}" Pending';
     public static final String MSG_OPPORTUNITY_NOT_EXIST = 'Opportunity not exist in the CRM';
     public static final String MSG_OPPORTUNITY_NOT_EXIST_YET = 'Opportunity not exist yet in the CRM';
     public static final String MSG_OPPORTUNITY_CHANGE_INTEREST_LOCKED_STATUS = 'Opportunity locked status upper Lead. SIS has to create a new Opportunity';
     public static final String MSG_OPPORTUNITY_CHANGE_INTEREST_DIFFERENT_BU = 'Business Unit is différent from selected Opportunity. SIS has to create a new Opportunity';
     public static final String MSG_OPPORTUNITY_CHANGE_INTEREST_DIFFERENT_AY = 'Intake AcademicYear is différent from selected Opportunity. SIS has to create a new Opportunity';
     public static final String MSG_OPPORTUNITY_CHANGE_INTEREST_ALREADY_EXIST = 'Same Opportunity already exist in CRM';
}