@RestResource(urlMapping='/sis/opportunity')
global class CORE_SIS_Opportunity {

    @HttpGet
    global static CORE_SIS_Wrapper.ResultWrapperOpportunity getOpportunity(){

        String urlCallback = '';
        RestRequest	request = RestContext.request;

        // Initiating logs
        Long dt1 = DateTime.now().getTime();
        Long dt2;
        CORE_LogManager logSettings;
        CORE_LOG__c log = new CORE_LOG__c(CORE_API_Name__c = 'SIS');
        Boolean isErrorLog = false;
        logSettings = new CORE_LogManager('SIS');

        String salesforceId = request.params.get(CORE_SIS_Constants.PARAM_SF_ID);
        String sisStudentId = request.params.get(CORE_SIS_Constants.PARAM_SISSTUDENT_ID);
        String additionalFields = request.params.get(CORE_SIS_Constants.PARAM_ADD_FIELDS);

        CORE_SIS_Wrapper.ResultWrapperOpportunity result = checkMandatoryParameters(salesforceId, sisStudentId);

        try{
            if(result != null){
                return result;
            }
            List<Opportunity> oppsList = CORE_SIS_QueryProvider.getOpportunity(salesforceId, sisStudentId, additionalFields);
            system.debug('@updateOpportunity > oppsList: ' + JSON.serializePretty(oppsList[0]));
            result = new CORE_SIS_Wrapper.ResultWrapperOpportunity(oppsList[0], CORE_SIS_Constants.SUCCESS_STATUS);

        } catch(Exception e){
            result = new CORE_SIS_Wrapper.ResultWrapperOpportunity(CORE_SIS_Constants.ERROR_STATUS, e.getMessage() + ' at ' + e.getStackTraceString());
        } finally{
            dt2 = DateTime.now().getTime();
            System.debug('CORE_AOL_AcademicDiplomaHistory(urlMapping=\'/aol/academic-diploma-history\') : T0TAL TIME OF EXECUTION = ' + (dt2-dt1) + ' ms');
            // creating log
            log.CORE_Processing_Duration__c =(dt2-dt1);
            log.CORE_HTTP_Status_Code__c = result.status == 'KO' ? 400 : result.status == 'OK' ? 200 : 500;
            log.CORE_Sent_Body__c = JSON.serialize(result);
            log.CORE_Received_Body__c =  request?.requestBody?.toString();
            
            if(!logSettings.isNone()){
                if(logSettings.isAllLogged()){ // is All logged
                    CORE_LogManager.createLog(log);
                }else if(logSettings.isErrorLogged() && isErrorLog){ // is Error
                    CORE_LogManager.createLog(log);
                }
            }
        }

        return result;
    }

    private static CORE_SIS_Wrapper.ResultWrapperOpportunity checkMandatoryParameters(String salesforceId, String sisStudentId){
        List<String> missingParameters = new List<String>();
        Boolean isMissingSisStudentId = CORE_SIS_GlobalWorker.isParameterMissing(sisStudentId);
        Boolean isMissingSFId = CORE_SIS_GlobalWorker.isParameterMissing(salesforceId);

        if(isMissingSFId
            && isMissingSisStudentId){

            missingParameters.add(CORE_SIS_Constants.PARAM_SF_ID);
            missingParameters.add(CORE_SIS_Constants.PARAM_SISSTUDENT_ID);
        }
        
        return getMissingParameters(missingParameters);
    }

    public static CORE_SIS_Wrapper.ResultWrapperOpportunity getMissingParameters(List<String> missingParameters){
        String errorMsg = CORE_SIS_Constants.MSG_MISSING_PARAMETERS;

        if(missingParameters.size() > 0){
            errorMsg += missingParameters;

            return new CORE_SIS_Wrapper.ResultWrapperOpportunity(CORE_AOL_Constants.ERROR_STATUS, errorMsg);
        }
        
        return null;
    }
}