/*******************************************************/
/**     @Author: Valentin Pitel                       **/
/**     @UnitTestClass: CORE_OpportunityUtilsTest     **/
/*******************************************************/
/**  Descritption :                                   **/
/**  Utility class used for functional rules          **/
/*******************************************************/
public class CORE_OpportunityUtils {
    private static final List<String> STATUS_FOR_MAIN_OPPORTUNITIES = new List<String> {
        'Suspect',
        'Prospect',
        'Lead',
        'Applicant',
        'Admitted',
        'Registered',
        'Enrolled',
        'Withdraw'
    };

    public static String getShortQuestionForOpportunity(String question){
        if(!String.isBlank(question) && question.length() > 255){
            return question.left(150) + Label.CORE_LFA_TooLongQuestionMsg;
        }

        return question;
    }
    
    public static String getCreationStatus(boolean isFullHierarchy, boolean isSuspect, CORE_OpportunityMetadata metadata, boolean isAol){
        String status;
        if(isAol){
            status = 'Prospect';
            if(isFullHierarchy){
                status = 'Lead';
            }
        } else {
            status = metadata.statusPartial;
            if(isSuspect){
                status = metadata.statusSuspect;
            } else if(isFullHierarchy){
                status = metadata.statusFull;
            }
        }
        

        return status;
    }

    public static String getCreationSubStatus(boolean isFullHierarchy, boolean isSuspect, CORE_OpportunityMetadata metadata, boolean isAol){
        String subStatus;

        if(isAol){
            subStatus = 'Prospect - Qualified For Intake / Waiting For Application';
            if(isFullHierarchy){
                subStatus = 'Lead - Qualified For Intake / Waiting For Application';
            }
        } else {
            subStatus = metadata.subStatusPartial;
            if(isSuspect){
                subStatus = metadata.subStatusSuspect;
            } else if(isFullHierarchy){
                subStatus = metadata.subStatusFull;
            }
        }
        

        return subStatus;
    }

    public static String getAcademicYear(String defaultAcademicYear, String academicYear, CORE_CatalogHierarchyModel catalogHierarchy, DateTime creationDate, List<String> allAcademicYears){
        if (catalogHierarchy != null && catalogHierarchy.intake != null && !String.isBlank(catalogHierarchy.intake.CORE_Academic_Year__c)){
            return catalogHierarchy.intake.CORE_Academic_Year__c;
        } else if (catalogHierarchy != null && catalogHierarchy.division != null && !String.isBlank(catalogHierarchy.division.CORE_Default_Academic_Year__c)){
            return catalogHierarchy.division.CORE_Default_Academic_Year__c;
        } else if(!String.isBlank(academicYear)){
            String currentAcademicYear = calculateCurrentAcademicYear(creationDate, allAcademicYears);
            return academicYear < currentAcademicYear ? currentAcademicYear : academicYear;
        } else if(!String.isEmpty(defaultAcademicYear)){
            return defaultAcademicYear;
        } else {
            return calculateCurrentAcademicYear(creationDate, allAcademicYears);
        }
    }

    private static String calculateCurrentAcademicYear(DateTime creationDate, List<String> allAcademicYears){
        if(creationDate == null){
            return null;
        }

        Integer month = creationDate.month();
        Integer currentYear = creationDate.year();
        List<String> availableAcademicYear = new List<String>();
        
        Integer nbAcademicYear = 0;
        for(String academicYear : allAcademicYears){
            if(academicYear.contains(String.valueOf(currentYear))){
                availableAcademicYear.add(academicYear);
                nbAcademicYear += 1;
            }
            if(nbAcademicYear == 2){
                break;
            }
        }

        for(String academicYear : availableAcademicYear){
            if(month < 9 && academicYear.contains(String.valueOf(currentYear-1))){
                return academicYear;
            } else if( month >= 9 && academicYear.contains(String.valueOf(currentYear + 1))){
                return academicYear;
            }
        }
        return null;
    }

    //functionnal rule : https://ggeedu.atlassian.net/wiki/spaces/GSSGD/pages/2087813274/Automatic+creation+process
    //Rule BR4 => manual creation
    public static boolean isExistingMainOpportunity(Id businessUnit,Id accountId, String academicYear){
        List<String> statusToCheck = STATUS_FOR_MAIN_OPPORTUNITIES;

        Boolean mainOpportunityExist = ([SELECT count() from Opportunity 
                                         WHERE CORE_OPP_Business_Unit__c = :businessUnit 
                                         AND AccountId = :accountId
                                         AND StageName IN :statusToCheck
                                         AND CORE_OPP_Academic_Year__c = :academicYear
                                         AND CORE_OPP_Main_Opportunity__c = true
                                        ]) > 0;

        return mainOpportunityExist;

    }

    //Rule BR4 => automatic creation 
    public static List<Opportunity> getExistingMainOpportunitiesByExternalIds(Set<String> businessUnitExternalIds,Id accountId){
        List<String> statusToCheck = STATUS_FOR_MAIN_OPPORTUNITIES;

        List<Opportunity> mainOpportunities = [SELECT Id,OwnerId,CORE_Main_Product_Interest__r.CORE_Product_Type__c, CORE_OPP_Main_Opportunity__c,CORE_OPP_Curriculum__r.CORE_Curriculum_ExternalId__c,CORE_OPP_Academic_Year__c, CORE_OPP_Application_Online_ID__c,CORE_OPP_Sub_Status__c,StageName,CORE_Main_Product_Interest__c,CORE_Questions__c,
            CORE_OPP_Business_Unit__r.CORE_Business_Unit_ExternalId__c,CORE_OPP_Business_Unit__c,CORE_OPP_Division__c, AccountId , CORE_Lead_Source_External_Id__c, CORE_ReturningLead__c,
            CORE_OPP_Funding__c, CampaignId,CORE_OPP_OnlineShop_Id__c, CORE_OPP_Application_Online_Advancement__c, CORE_OPP_Application_started__c,CORE_Technical_Sub_Statut__c
            FROM Opportunity 
            WHERE CORE_OPP_Business_Unit__r.CORE_Business_Unit_ExternalId__c in :businessUnitExternalIds  
            AND AccountId = :accountId
            AND StageName IN :statusToCheck
            AND CORE_OPP_Main_Opportunity__c = true];

        return mainOpportunities;
    }

    public static List<Opportunity> getExistingMainOpportunitiesByBuIds(Set<Id> businessUnitIds,Set<Id> accountIds){
        List<String> statusToCheck = STATUS_FOR_MAIN_OPPORTUNITIES;

        List<Opportunity> mainOpportunities = [SELECT Id,OwnerId,CORE_Main_Product_Interest__r.CORE_Product_Type__c, CORE_OPP_Main_Opportunity__c,CORE_OPP_Academic_Year__c, StageName, CORE_OPP_Sub_Status__c,CORE_Main_Product_Interest__c,CORE_Questions__c,
            CORE_OPP_Business_Unit__r.CORE_Business_Unit_ExternalId__c,CORE_OPP_Business_Unit__c, AccountId ,CORE_Lead_Source_External_Id__c, CORE_OPP_Application_Online_ID__c, CORE_ReturningLead__c,
            CORE_OPP_Funding__c, CampaignId,CORE_OPP_OnlineShop_Id__c, CORE_OPP_Application_Online_Advancement__c, CORE_OPP_Application_started__c,CORE_Technical_Sub_Statut__c
            FROM Opportunity 
            WHERE CORE_OPP_Business_Unit__c IN :businessUnitIds  
            AND AccountId IN :accountIds
            AND StageName IN :statusToCheck
            AND CORE_OPP_Main_Opportunity__c = true];

        return mainOpportunities;
    }
    public static List<Opportunity> getExistingAolOpportunities(Set<String> aolIds){
        List<Opportunity> aolOpportunities = new List<Opportunity>();

        if(aolIds.isEmpty()){
            return aolOpportunities;
        }

        aolOpportunities = [SELECT Id,OwnerId, CORE_Main_Product_Interest__r.CORE_Product_Type__c ,CORE_OPP_Academic_Year__c, CORE_OPP_Main_Opportunity__c,StageName, CORE_OPP_Sub_Status__c,CORE_Main_Product_Interest__c,CORE_Questions__c,
            CORE_OPP_Business_Unit__r.CORE_Business_Unit_ExternalId__c,CORE_OPP_Business_Unit__c, AccountId ,CORE_Lead_Source_External_Id__c, CORE_OPP_Application_Online_ID__c,CORE_ReturningLead__c,
            CORE_OPP_Funding__c, CampaignId,CORE_OPP_OnlineShop_Id__c,CORE_OPP_Application_Online_Advancement__c, CORE_OPP_Application_started__c, CORE_Technical_Sub_Statut__c
            FROM Opportunity 
            WHERE CORE_OPP_Application_Online_ID__c IN :aolIds];

        return aolOpportunities;
    }

    public static List<Opportunity> getExistingOnlineShopOpportunities(Set<String> onlineShopIds){
        List<Opportunity> onlineShopOpportunities = new List<Opportunity>();

        if(onlineShopIds.isEmpty()){
            return onlineShopOpportunities;
        }

        onlineShopOpportunities = [SELECT Id,OwnerId, CORE_Main_Product_Interest__r.CORE_Product_Type__c ,CORE_OPP_Academic_Year__c, CORE_OPP_Main_Opportunity__c,StageName, CORE_OPP_Sub_Status__c,CORE_Main_Product_Interest__c,CORE_Questions__c,
            CORE_OPP_Business_Unit__r.CORE_Business_Unit_ExternalId__c,CORE_OPP_Business_Unit__c, AccountId ,CORE_Lead_Source_External_Id__c, CORE_OPP_Application_Online_ID__c,CORE_ReturningLead__c,
            CORE_OPP_Funding__c, CampaignId,
            CORE_OPP_OnlineShop_Id__c,CORE_Technical_Sub_Statut__c
            FROM Opportunity 
            WHERE CORE_OPP_OnlineShop_Id__c IN :onlineShopIds];

        return onlineShopOpportunities;
    }
//
    public static Task getInterestTask(CORE_CatalogHierarchyModel catalogHierarchy, String studentComment, Opportunity opportunity, Account account){
        String subject = Label.CORE_LFA_Task_subject_RequestFromWebsite;
        String type = 'CORE_PKL_Web_Form';
        String priority = 'CORE_PKL_Normal';
        String status = null;
        String description = null;
        Date activityDate = null;

        if(!String.isBlank(opportunity.CORE_OPP_OnlineShop_Id__c)){
            subject = Label.CORE_LFA_Task_subject_RequestFromWebsiteOnlineShop;
            priority = 'CORE_PKL_Normal';
            type = 'CORE_PKL_Online_Shop_Form';
            status = 'CORE_PKL_Completed';
        } else if(!String.IsEmpty(studentComment)){
            priority = 'CORE_PKL_High';
            description = 'Student question : ' + studentComment;
            activityDate = System.today();
        } else {
            priority = 'CORE_PKL_Normal';
            status = 'CORE_PKL_Completed';
        }

        Task task = new Task(
            Subject = subject,
            WhatId = opportunity.Id,
            WhoId = account.PersonContactId,
            Type = type,
            CORE_TECH_CallTaskType__c = 'CORE_PKL_Question',
            CORE_Is_Incoming__c = true,
            OwnerId = opportunity.OwnerId,
            Priority = priority
        );
        
        if(description != null){
            task.Description = description;
        }
        if(activityDate != null){
            task.ActivityDate = activityDate;
        }
        if(status != null){
            task.Status = status;
        }

        return task;
    }
    public static Task getReturningTask(CORE_CatalogHierarchyModel catalogHierarchy, String studentComment, Opportunity opportunity, Account account){
        String catalogString = '';
        catalogString += !String.isBlank(catalogHierarchy.division?.Name) ? 'Division : ' + catalogHierarchy.division.Name + '\n' : '';
        catalogString += !String.isBlank(catalogHierarchy.businessUnit?.Name) ? 'Business Unit : ' + catalogHierarchy.businessUnit.Name + '\n'  : '';
        catalogString += !String.isBlank(catalogHierarchy.studyArea?.Name) ? 'Study Area : ' + catalogHierarchy.studyArea.Name + '\n'  : '';
        catalogString += !String.isBlank(catalogHierarchy.curriculum?.Name) ? 'Curriculum : ' + catalogHierarchy.curriculum.Name + '\n'  : '';
        catalogString += !String.isBlank(catalogHierarchy.level?.Name) ? 'Level : ' + catalogHierarchy.level.Name + '\n'  : '';
        catalogString += !String.isBlank(catalogHierarchy.intake?.Name) ? 'Intake : ' + catalogHierarchy.intake.Name + '\n'  : '';
        catalogString += !String.isBlank(catalogHierarchy.product?.Name) ? 'Product : ' + catalogHierarchy.product.Name : '';
        
        String subject = Label.CORE_LFA_Task_subject_returning;
        String priority = 'CORE_PKL_High';
        String type = 'CORE_PKL_Web_Form';
        if(!String.isBlank(catalogHierarchy.interest.onlineShopId)){
            subject = Label.CORE_LFA_Task_subject_returning_onlineShop;
            priority = 'CORE_PKL_Normal';
            type = 'CORE_PKL_Online_Shop_Form';
        } else if(!String.isBlank(catalogHierarchy.interest.applicationOnlineId)){
            subject = Label.CORE_LFA_Task_subject_returning_aol;
            priority = 'CORE_PKL_Normal';
        }
        Task task = new Task(
            Subject = subject,
            Priority = priority,
            Description = Label.CORE_LFA_Task_question_comment + ' : ' + studentComment + '\n' + catalogString,
            WhatId = opportunity.Id,
            WhoId = account.PersonContactId,
            Type = type,
            CORE_Is_Incoming__c = true,
            ActivityDate = System.today(),
            CORE_IsReturningFromLeadForm__c = true,
            OwnerId = opportunity.OwnerId
        );

        task = CORE_LeadFormAcquisitionUtils.prefillTaskCatalogFields(task, catalogHierarchy);

        return task;
    }

    public static List<ContentDocumentLink> getDuplicatedAttachments(List<ContentDocumentLink> attachments, Id newOpportunityId){
        List<ContentDocumentLink> duplicates = new List<ContentDocumentLink>();
        for(ContentDocumentLink attachment : attachments){
            ContentDocumentLink attachmentTmp = attachment.clone(false, true, false, false);
            attachmentTmp.Id = null;
            attachmentTmp.LinkedEntityId = newOpportunityId;
            duplicates.add(attachmentTmp);
        }

        insert duplicates;
        return duplicates;
    }

    public static List<OpportunityContactRole> getDuplicatedContactRoles(List<OpportunityContactRole> contactRoles, Id newOpportunityId){
        List<OpportunityContactRole> duplicates = new List<OpportunityContactRole>();
        for(OpportunityContactRole contactRole : contactRoles){
            OpportunityContactRole contactRoleTmp = contactRole.clone(false, true, false, false);
            contactRoleTmp.Id = null;
            contactRoleTmp.OpportunityId = newOpportunityId;
            duplicates.add(contactRoleTmp);
        }

        insert duplicates;
        return duplicates;
    }

    public static List<CORE_Sub_Status_Timestamp_History__c> getDuplicatedSubStatusTimeStamps(List<CORE_Sub_Status_Timestamp_History__c> subStatuses, Id newOpportunityId){
        List<CORE_Sub_Status_Timestamp_History__c> duplicates = new List<CORE_Sub_Status_Timestamp_History__c>();
        for(CORE_Sub_Status_Timestamp_History__c subStatus : subStatuses){
            CORE_Sub_Status_Timestamp_History__c subStatusTmp = subStatus.clone(false, true, false, false);
            subStatusTmp.Id = null;
            subStatusTmp.CORE_External_Id__c = null;
            subStatusTmp.CORE_Opportunity__c = newOpportunityId;
            duplicates.add(subStatusTmp);
        }

        insert duplicates;
        return duplicates;
    }

    public static List<CORE_Status_Timestamp_History__c> getDuplicatedStatusTimeStamps(List<CORE_Status_Timestamp_History__c> statuses, Id newOpportunityId){
        List<CORE_Status_Timestamp_History__c> duplicates = new List<CORE_Status_Timestamp_History__c>();
        for(CORE_Status_Timestamp_History__c status : statuses){
            CORE_Status_Timestamp_History__c statusTmp = status.clone(false, true, false, false);
            statusTmp.Id = null;
            statusTmp.CORE_Opportunity__c = newOpportunityId;
            statusTmp.CORE_External_Id__c = null;
            duplicates.add(statusTmp);
        }

        insert duplicates;
        return duplicates;
    }

    public static List<CORE_Point_of_Contact__c> getDuplicatedPointOfContacts(List<CORE_Point_of_Contact__c> pointOfContacts, Id newOpportunityId){
        List<CORE_Point_of_Contact__c> duplicates = new List<CORE_Point_of_Contact__c>();
        for(CORE_Point_of_Contact__c pointOfContact : pointOfContacts){
            CORE_Point_of_Contact__c pointOfContactTmp = pointOfContact.clone(false, false, false, false);
            pointOfContactTmp.Id = null;
            pointOfContactTmp.CORE_Opportunity__c = newOpportunityId;
            pointOfContactTmp.CORE_External_Id__c = null;
            pointOfContactTmp.CORE_IsDuplicated__c = true;
            duplicates.add(pointOfContactTmp);
        }

        insert duplicates;
        return duplicates;
    }
    /* not use for now
    public static Map<String ,Id> getTechnicalUsersByBusinessUnit(Map<Integer,CORE_CatalogHierarchyModel> catalogHierarchies){
        Map<String, Id> technicalUsersByBusinessUnit = new Map<String, Id>();

        for(Integer i : catalogHierarchies.keySet()){
            CORE_Business_Unit__c businessUnit = catalogHierarchies.get(i).businessUnit;
            if(businessUnit != null && !String.isEmpty(businessUnit.CORE_BU_Technical_User__c) && !String.isEmpty(businessUnit.CORE_Business_Unit_ExternalId__c)){
                technicalUsersByBusinessUnit.put(businessUnit.CORE_Business_Unit_ExternalId__c, businessUnit.CORE_BU_Technical_User__c);
            }
        }

        return technicalUsersByBusinessUnit;
    }*/
}