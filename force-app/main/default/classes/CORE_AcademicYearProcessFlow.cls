global class CORE_AcademicYearProcessFlow {

    @InvocableMethod
    global static List<String> updateOpportunity(List<AcademicYearProcessParameters> params) {
        system.debug('@invocable updateOpportunity: ' + params);

        if(params != null){
            Savepoint sp = Database.setSavepoint();

            List<Database.SaveResult> results = null;

            try {
                Set<Id> accountsIdSet = new Set<Id>();
                Set<Id> businessUnitIdSet = new Set<Id>();
                Set<String> academicYearSet = new Set<String>();
                Set<String> prodCodesSet = new Set<String>();
                Map<String, Opportunity> existingOppsMap = new Map<String, Opportunity>();
                Map<Id, Opportunity> oppsMap = new Map<Id, Opportunity>();
                Map<Id, Id> pbIdMap = new Map<Id, Id>();
                Map<Id, List<OpportunityLineItem>> oliMap = new Map<Id, List<OpportunityLineItem>>();
                Boolean hasOLI = false;

                for(AcademicYearProcessParameters param : params){

                    accountsIdSet.add(param.accountId);
                    businessUnitIdSet.add(param.businessUnitId);
                    academicYearSet.add(param.academicYear);

                    if(param.mainInterestProdCode != null){
                        prodCodesSet.add(param.mainInterestProdCode);
                    }

                    oppsMap.put(
                        param.oppId,
                        new Opportunity(
                            Id = param.oppId,
                            AccountId = param.accountId,
                            CORE_OPP_Business_Unit__c = param.businessUnitId,
                            CORE_OPP_Academic_Year__c = param.academicYear,
                            CORE_Academic_Year_Confirmed__c = param.isAcademicYearConfirmed,
                            Pricebook2Id = param.priceBookId,
                            CORE_OPP_Main_Opportunity__c = true,
                            CORE_Main_Product_Interest__c = null,
                            CORE_OPP_Intake__c = null
                        )
                    );

                    pbIdMap.put(param.oppId, param.priceBookId);
                }

                String key = '';

                //Get main opportunities on the same account
                for(Opportunity opp : [SELECT Id, AccountId, CORE_OPP_Business_Unit__c, CORE_OPP_Academic_Year__c
                                    FROM Opportunity 
                                    WHERE AccountId IN :accountsIdSet AND CORE_OPP_Business_Unit__c <> NULL AND CORE_OPP_Business_Unit__c IN :businessUnitIdSet
                                    AND CORE_OPP_Academic_Year__c <> NULL AND CORE_OPP_Academic_Year__c IN :academicYearSet]){

                    key = opp.AccountId + '_' + opp.CORE_OPP_Business_Unit__c + '_' + opp.CORE_OPP_Academic_Year__c;

                    existingOppsMap.put(key, opp);
                }

                for(Opportunity opp : oppsMap.values()){
                    key = opp.AccountId + '_' + opp.CORE_OPP_Business_Unit__c + '_' + opp.CORE_OPP_Academic_Year__c;

                    //Has existing opportunity with same Account, Business Unit and new Academic Year => set current opportunity to not main
                    if(existingOppsMap.containsKey(key)){
                        opp.CORE_OPP_Main_Opportunity__c = false;
                    }
                }

                //Get existing related product line items
                Set<String> oliFieldsSet = Schema.getGlobalDescribe().get('OpportunityLineItem').getDescribe().fields.getMap().keySet();
                System.debug('OliFieldSet '+ oliFieldsSet);
                // Add fields to list
                List<String> oliFieldsList = new List<String>();
                oliFieldsList.addAll(oliFieldsSet);

                Set<Id> oppIdSet = new Set<Id>();
                oppIdSet.addAll(oppsMap.keySet());
 
                // Construct query
                String oliQuery = 'SELECT ' + string.join(oliFieldsList, ',') + ' FROM OpportunityLineItem WHERE OpportunityId IN :oppIdSet';

                List<OpportunityLineItem> existingOppsLineItemsList = database.query(oliQuery);

                for(OpportunityLineItem oli : existingOppsLineItemsList){
                    if(oliMap.containsKey(oli.OpportunityId)){
                        oliMap.get(oli.OpportunityId).add(oli);
                    }
                    else{
                        oliMap.put(oli.OpportunityId, new List<OpportunityLineItem> { oli });
                    }
                }

                //Delete existing related product line items
                database.delete(existingOppsLineItemsList, true);

                system.debug('@invocable updateOpportunity > prodCodesSet: ' + prodCodesSet);

                //Get new Product
                if(prodCodesSet.size() > 0){
                    Map<String, Product2> productsMap = new Map<String, Product2>();

                    for(Product2 prod : [SELECT Id, CORE_IntakeID__c, ProductCode, CORE_TECH_IntakeAcademicYear__c
                                        FROM Product2 WHERE ProductCode <> NULL AND ProductCode IN :prodCodesSet 
                                        AND CORE_TECH_IntakeAcademicYear__c <> NULL AND CORE_TECH_IntakeAcademicYear__c IN :academicYearSet
                                        AND IsActive = TRUE]){

                        productsMap.put(prod.ProductCode + '_' + prod.CORE_TECH_IntakeAcademicYear__c, prod);
                    }

                    system.debug('@invocable updateOpportunity > productsMap: ' + productsMap);

                    if(productsMap.size() > 0){
                        Opportunity currentOpp = null;
                        Product2 newProduct = null;

                        for(AcademicYearProcessParameters param : params){
                            currentOpp = oppsMap.get(param.oppId);
                            newProduct = productsMap.get(param.mainInterestProdCode + '_' + param.academicYear);

                            if(newProduct != null){
                                currentOpp.CORE_Main_Product_Interest__c = newProduct.Id;
                                currentOpp.CORE_OPP_Intake__c = newProduct.CORE_IntakeID__c;
                            }
                        }

                        system.debug('@HAS OLI!');
                        hasOLI = true;
                    }
                    else{
                        system.debug('@DOES NOT HAVE OLI!');
                    }
                }
                else{
                    system.debug('@DOES NOT HAVE OLI!');
                }

                system.debug('@oppsMap: ' + JSON.serializePretty(oppsMap));

                results = database.update(oppsMap.values(), false);
                
                if(hasOLI){
                    cloneOppLineItems(oliMap, pbIdMap, prodCodesSet);
                }

                if(params[0].resultsList != null){
                    CORE_MassOpportunityResultHandler resultHandler = new CORE_MassOpportunityResultHandler();
                    resultHandler.settingName = CORE_LC_MassOppAcademicYearConfirmation.EMAIL_SETTING_NAME;
                    resultHandler.results = results;
                    resultHandler.recordsList = oppsMap.values();
                    resultHandler.resultsList = params[0].resultsList;
                    resultHandler.sendEmail = false;

                    params[0].resultsList = resultHandler.handleResults();
                }
                
            }
            catch(Exception e){
                system.debug('Error: ' + e.getMessage() + ' => ' + e.getStackTraceString());
                Database.rollback(sp);

                throw new OpportunityException('Error: ' + e.getMessage() + ' => ' + e.getStackTraceString());
            }
            
        }
        
        return params[0].resultsList;
    }

    public static void cloneOppLineItems(Map<Id, List<OpportunityLineItem>> oliMap, Map<Id, Id> pbIdMap, Set<String> prodCodesSet){

        Map<String, PricebookEntry> pbeMap = new Map<String, PricebookEntry>();
        List<OpportunityLineItem> oliList = new List<OpportunityLineItem>();

        //system.debug('@cloneOppLineItems > prodCodesSet: ' + prodCodesSet);
        system.debug('@cloneOppLineItems > pbIdMap: ' + JSON.serializePretty(pbIdMap));

        for(PricebookEntry pbe : [SELECT Id, Pricebook2Id, Product2Id, ProductCode, UnitPrice, IsActive 
                                  FROM PricebookEntry
                                  WHERE Pricebook2Id IN :pbIdMap.values()
                                  AND ProductCode IN :prodCodesSet
                                  AND IsActive = true
                                  AND Product2.IsActive = true]){
            
            pbeMap.put(pbe.ProductCode, pbe);
        }

        //system.debug('@cloneOppLineItems > pbeMap: ' + pbeMap);

        for(Id oppId : oliMap.keySet()){
            system.debug('oppId: ' + oppId);

            for(OpportunityLineItem oli : oliMap.get(oppId)){
                system.debug('@cloneOppLineItems > oli: ' + oli);
                PricebookEntry pbe = pbeMap.get(oli.ProductCode);
                system.debug('@cloneOppLineItems > pbe: ' + pbe);

                if(pbe != null){
                    OpportunityLineItem cloneOli = oli.clone(false, true, false, false);

                    cloneOli.UnitPrice = null;
                    cloneOli.CORE_External_Id__c = null;
                    cloneOli.PricebookEntryId = pbe.Id;

                    oliList.add(cloneOli);
                }
                
            }

            system.debug('oliList: ' + oliList);
        }

        if(oliList.size() > 0){
            database.insert(oliList, false);
        }
    }

    global class AcademicYearProcessParameters {
        @InvocableVariable(required=true)
        global Id accountId;

        @InvocableVariable(required=true)
        global Id oppId;

        @InvocableVariable(required=true)
        global Id businessUnitId;

        @InvocableVariable(required=true)
        global String academicYear;

        @InvocableVariable
        global String priceBookId;

        @InvocableVariable
        global Boolean isAcademicYearConfirmed;

        @InvocableVariable
        global String mainInterestProdCode;

        global List<String> resultsList;

    }

    class OpportunityException extends Exception {}
}