@isTest
private class CORE_AOL_OpportunityDeferral_TEST {

    private static final String AOL_EXTERNALID = 'setupAol';
    private static final String INTEREST_EXTERNALID = 'setup';

    @testSetup 
    static void createData(){
        Id profileId = [SELECT Id FROM Profile WHERE Name = 'CORE_API_Profile' LIMIT 1].Id;

        User usr = new User(
            Username = 'usertest@aoltest.com',
            LastName = 'TEST_USER',
            Email = 'testemail@yopmail.com',
            Alias = 'T', 
            CommunityNickname = 'T',
            TimeZoneSidKey = 'Europe/Paris',
            LocaleSidKey = 'fr_FR_EURO',
            EmailEncodingKey = 'ISO-8859-1', 
            ProfileId = profileId,
            LanguageLocaleKey = 'fr'      
        );
        database.insert(usr, true);

        CORE_AOL_OpportunityDeferral.overrideAcademicYear = 'CORE_PKL_2023-2024';

        CORE_CatalogHierarchyModel catalogHierarchy = new CORE_DataFaker_CatalogHierarchy(INTEREST_EXTERNALID).catalogHierarchy;
        Account account = CORE_DataFaker_Account.getStudentAccount(INTEREST_EXTERNALID);

        Id oppEnroll_RTId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get(CORE_AOL_Constants.OPP_RECORDTYPE_ENROLLMENT).getRecordTypeId();

        Opportunity aol_opportunity = new Opportunity(
            Name ='test',
            StageName = CORE_AOL_Constants.PKL_OPP_STAGENAME_PROSPECT,
            CORE_OPP_Sub_Status__c = CORE_AOL_Constants.PKL_OPP_SUBSTATUS_PROSPECTAPPSTARTED,
            CORE_OPP_Division__c = catalogHierarchy.division.Id,
            CORE_OPP_Business_Unit__c = catalogHierarchy.businessUnit.Id,
            CORE_OPP_Study_Area__c = catalogHierarchy.studyArea.Id,
            CORE_OPP_Curriculum__c = catalogHierarchy.curriculum.Id,
            CORE_OPP_Level__c = catalogHierarchy.level.Id,
            CORE_OPP_Intake__c = catalogHierarchy.intake.Id,
            CORE_OPP_Academic_Year__c = CORE_AOL_OpportunityDeferral.overrideAcademicYear,
            AccountId = account.Id,
            CloseDate = Date.Today().addDays(2),
            OwnerId = UserInfo.getUserId(),
            CORE_OPP_Application_Online_ID__c = AOL_EXTERNALID,
            RecordTypeId = oppEnroll_RTId
        );

        Opportunity main_opportunity = new Opportunity(
            Name ='test',
            StageName = CORE_AOL_Constants.PKL_OPP_STAGENAME_PROSPECT,
            CORE_OPP_Sub_Status__c = CORE_AOL_Constants.PKL_OPP_SUBSTATUS_PROSPECTAPPSTARTED,
            CORE_OPP_Division__c = catalogHierarchy.division.Id,
            CORE_OPP_Business_Unit__c = catalogHierarchy.businessUnit.Id,
            CORE_OPP_Study_Area__c = catalogHierarchy.studyArea.Id,
            CORE_OPP_Curriculum__c = catalogHierarchy.curriculum.Id,
            CORE_OPP_Level__c = catalogHierarchy.level.Id,
            CORE_OPP_Intake__c = catalogHierarchy.intake.Id,
            CORE_OPP_Academic_Year__c = CORE_AOL_OpportunityDeferral.overrideAcademicYear,
            AccountId = account.Id,
            CloseDate = Date.Today().addDays(2),
            OwnerId = UserInfo.getUserId(),
            CORE_OPP_Main_Opportunity__c = true,
            RecordTypeId = oppEnroll_RTId
        );

        database.insert(new List<Opportunity> { aol_opportunity, main_opportunity }, true);

        Id pricebookId = CORE_DataFaker_PriceBook.getStandardPricebookId();
        PricebookEntry pbe1 = CORE_DataFaker_PriceBook.getPriceBookEntry(catalogHierarchy.product.Id, pricebookId, 100.0);

        CORE_PriceBook_BU__c pricebookBu = CORE_DataFaker_PriceBook.getPriceBookBuWithoutInsert(pricebookId, catalogHierarchy.businessUnit.Id, 'setup');
        pricebookBu.CORE_Academic_Year__c = CORE_AOL_OpportunityDeferral.overrideAcademicYear;

        database.insert(pricebookBu, true);

    }

    @isTest
    static void test_HasMain(){
        User usr = [SELECT Id FROM User WHERE Username = 'usertest@aoltest.com' LIMIT 1];

        List<PermissionSetAssignment> psaList = new List<PermissionSetAssignment>();

        for(PermissionSet ps : [SELECT Id FROM PermissionSet WHERE Name = 'CORE_API_AOL' OR Name = 'CORE_LeadFormAcquisition']){
            psaList.add(
                new PermissionSetAssignment(AssigneeId = usr.id, PermissionSetId = ps.Id)
            );
        }
        
        database.insert(psaList, true);

        System.runAs(usr){
            CORE_AOL_OpportunityDeferral.overrideAcademicYear = 'CORE_PKL_2023-2024';

            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
            req.requestURI = '/services/apexrest/aol/opportunity/deferal';
            req.httpMethod = 'PUT';

            RestContext.request = req;
            RestContext.response= res;

            CORE_AOL_Wrapper.OpportunityWrapper opp = new CORE_AOL_Wrapper.OpportunityWrapper();

            CORE_AOL_Wrapper.InterestWrapper interest = new CORE_AOL_Wrapper.InterestWrapper(); 
            interest.businessUnit = INTEREST_EXTERNALID;
            interest.studyArea = INTEREST_EXTERNALID;
            interest.curriculum = INTEREST_EXTERNALID;
            interest.level = INTEREST_EXTERNALID;
            interest.intake = INTEREST_EXTERNALID;   

            CORE_AOL_Wrapper.AOLProgressionFields aolProgressionFields = new CORE_AOL_Wrapper.AOLProgressionFields();
            //aolProgressionFields.

            test.startTest();
            CORE_AOL_OpportunityDeferral.opportunityDeferral(AOL_EXTERNALID, 'new ' + AOL_EXTERNALID, interest, aolProgressionFields);
            test.stopTest();

            Opportunity oldAolOpp = [SELECT Id, CORE_OPP_Parent_Opportunity__c FROM Opportunity WHERE CORE_OPP_Application_Online_ID__c = :AOL_EXTERNALID LIMIT 1];
            List<Opportunity> newAolOpp = [SELECT Id, CORE_OPP_Parent_Opportunity__c FROM Opportunity WHERE CORE_OPP_Application_Online_ID__c = :'new ' + AOL_EXTERNALID LIMIT 1];

            System.assertEquals(1, newAolOpp.size());
            System.assertEquals(newAolOpp[0].Id, oldAolOpp.CORE_OPP_Parent_Opportunity__c);
        }
    }

    @isTest
    static void test_HasMainShortCourse(){
        User usr = [SELECT Id FROM User WHERE Username = 'usertest@aoltest.com' LIMIT 1];

        List<PermissionSetAssignment> psaList = new List<PermissionSetAssignment>();

        for(PermissionSet ps : [SELECT Id FROM PermissionSet WHERE Name = 'CORE_API_AOL' OR Name = 'CORE_LeadFormAcquisition']){
            psaList.add(
                new PermissionSetAssignment(AssigneeId = usr.id, PermissionSetId = ps.Id)
            );
        }
        
        database.insert(psaList, true);

        System.runAs(usr){
            CORE_AOL_OpportunityDeferral.overrideAcademicYear = 'CORE_PKL_2023-2024';

            Product2 prod = [SELECT Id, CORE_Product_Type__c FROM Product2 LIMIT 1];
            prod.CORE_Product_Type__c = CORE_AOL_Constants.PKL_PRODUCT_TYPE;
            database.update(prod, true);

            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
            req.requestURI = '/services/apexrest/aol/opportunity/deferal';
            req.httpMethod = 'PUT';

            RestContext.request = req;
            RestContext.response= res;

            CORE_AOL_Wrapper.OpportunityWrapper opp = new CORE_AOL_Wrapper.OpportunityWrapper();

            CORE_AOL_Wrapper.InterestWrapper interest = new CORE_AOL_Wrapper.InterestWrapper(); 
            interest.businessUnit = INTEREST_EXTERNALID;
            interest.studyArea = INTEREST_EXTERNALID;
            interest.curriculum = INTEREST_EXTERNALID;
            interest.level = INTEREST_EXTERNALID;
            interest.intake = INTEREST_EXTERNALID;   

            CORE_AOL_Wrapper.AOLProgressionFields aolProgressionFields = new CORE_AOL_Wrapper.AOLProgressionFields();
            //aolProgressionFields.

            test.startTest();
            CORE_AOL_OpportunityDeferral.opportunityDeferral(AOL_EXTERNALID, 'new ' + AOL_EXTERNALID, interest, aolProgressionFields);
            test.stopTest();
        }
    }

    @isTest
    static void test_NoMain(){
        User usr = [SELECT Id FROM User WHERE Username = 'usertest@aoltest.com' LIMIT 1];

        List<PermissionSetAssignment> psaList = new List<PermissionSetAssignment>();

        for(PermissionSet ps : [SELECT Id FROM PermissionSet WHERE Name = 'CORE_API_AOL' OR Name = 'CORE_LeadFormAcquisition']){
            psaList.add(
                new PermissionSetAssignment(AssigneeId = usr.id, PermissionSetId = ps.Id)
            );
        }
        
        database.insert(psaList, true);

        System.runAs(usr){
            CORE_AOL_OpportunityDeferral.overrideAcademicYear = 'CORE_PKL_2023-2024';

            Opportunity main_opp = [SELECT Id FROM Opportunity WHERE CORE_OPP_Main_Opportunity__c = TRUE AND CORE_OPP_Application_Online_ID__c = NULL LIMIT 1];
            database.delete(main_opp, true);

            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
            req.requestURI = '/services/apexrest/aol/opportunity/deferal';
            req.httpMethod = 'PUT';

            RestContext.request = req;
            RestContext.response= res;

            CORE_AOL_Wrapper.OpportunityWrapper opp = new CORE_AOL_Wrapper.OpportunityWrapper();

            CORE_AOL_Wrapper.InterestWrapper interest = new CORE_AOL_Wrapper.InterestWrapper(); 
            interest.businessUnit = INTEREST_EXTERNALID;
            interest.studyArea = INTEREST_EXTERNALID;
            interest.curriculum = INTEREST_EXTERNALID;
            interest.level = INTEREST_EXTERNALID;
            interest.intake = INTEREST_EXTERNALID;   

            CORE_AOL_Wrapper.AOLProgressionFields aolProgressionFields = new CORE_AOL_Wrapper.AOLProgressionFields();
            //aolProgressionFields.

            test.startTest();
            CORE_AOL_OpportunityDeferral.opportunityDeferral(AOL_EXTERNALID, 'new ' + AOL_EXTERNALID, interest, aolProgressionFields);
            test.stopTest();
        }
    }

    @isTest
    static void test_NoExistingAol(){
        User usr = [SELECT Id FROM User WHERE Username = 'usertest@aoltest.com' LIMIT 1];

        List<PermissionSetAssignment> psaList = new List<PermissionSetAssignment>();

        for(PermissionSet ps : [SELECT Id FROM PermissionSet WHERE Name = 'CORE_API_AOL' OR Name = 'CORE_LeadFormAcquisition']){
            psaList.add(
                new PermissionSetAssignment(AssigneeId = usr.id, PermissionSetId = ps.Id)
            );
        }
        
        database.insert(psaList, true);

        System.runAs(usr){
            CORE_AOL_OpportunityDeferral.overrideAcademicYear = 'CORE_PKL_2023-2024';

            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
            req.requestURI = '/services/apexrest/aol/opportunity/deferal';
            req.httpMethod = 'PUT';

            RestContext.request = req;
            RestContext.response= res;

            CORE_AOL_Wrapper.OpportunityWrapper opp = new CORE_AOL_Wrapper.OpportunityWrapper();

            CORE_AOL_Wrapper.InterestWrapper interest = new CORE_AOL_Wrapper.InterestWrapper(); 
            interest.businessUnit = INTEREST_EXTERNALID;
            interest.studyArea = INTEREST_EXTERNALID;
            interest.curriculum = INTEREST_EXTERNALID;
            interest.level = INTEREST_EXTERNALID;
            interest.intake = INTEREST_EXTERNALID;   

            CORE_AOL_Wrapper.AOLProgressionFields aolProgressionFields = new CORE_AOL_Wrapper.AOLProgressionFields();
            //aolProgressionFields.

            test.startTest();
            CORE_AOL_OpportunityDeferral.opportunityDeferral('TEST', 'new ' + AOL_EXTERNALID, interest, aolProgressionFields);
            test.stopTest();
        }
    }
}