@isTest
public class IT_InvocableTwilioCheckBHTest {

    @isTest
    public static void testInvocableMethod() {
        BusinessHours businessHour = [SELECT Id FROM BusinessHours WHERE IsDefault = true];

        List<IT_InvocableTwilioCheckBH.FlowInputs> requests = new List<IT_InvocableTwilioCheckBH.FlowInputs>();
        IT_InvocableTwilioCheckBH.FlowInputs request = new IT_InvocableTwilioCheckBH.FlowInputs();
        request.incomingBhId = businessHour.Id;
        request.targetDate = System.now();
        requests.add(request);

        List<IT_InvocableTwilioCheckBH.FlowOutputs> outcome = IT_InvocableTwilioCheckBH.checkIncomingDate(requests );
        System.assertNotEquals(outcome[0].isOpen, null);
    }
}