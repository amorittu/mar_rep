@RestResource(urlMapping='/sis/opportunity/re-registration-opportunity')
global class CORE_SIS_ReregistrationOpp {  
    List<String> availableAcademicYears;
    public CORE_SIS_ReregistrationOpp(){
        this.availableAcademicYears = CORE_PicklistUtils.getPicklistValues('Opportunity', 'CORE_OPP_Academic_Year__c');

    }
    public static CORE_SIS_Wrapper.ResultWrapperGlobal result { get; set; }


    @HttpPost
    global  static CORE_SIS_Wrapper.ResultWrapperGlobal Opportunity(CORE_SIS_Wrapper.OpportunityWrapper opportunity) {
        CORE_SIS_ReregistrationOpp sisrer = new CORE_SIS_ReregistrationOpp();

        // Initiating logs
        Long dt1 = DateTime.now().getTime();
        Long dt2;
        CORE_LogManager logSettings;
        CORE_LOG__c log = new CORE_LOG__c(CORE_API_Name__c = 'SIS');
        Boolean isErrorLog = false;
        logSettings = new CORE_LogManager('SIS');

        Savepoint sp = Database.setSavepoint();
        System.debug('opp '+ opportunity );
       // List<String>  pklOppAcademicYear = CORE_PicklistUtils.getPicklistValues('Opportunity', 'CORE_OPP_Academic_Year__c');
        //check parameters
        result = checkMandatoryParameters(opportunity);
        try {
            if(result != null){
                //missing parameter(s)
                return result;
            }
            RestResponse res = RestContext.response;
            System.debug('res '+res);
            res.addHeader('Content-Type', 'application/json');

            Opportunity init_opp = sisrer.createOpportunityProcess(opportunity);

            CORE_SIS_Wrapper.ResultOpportunityWrapper op = new CORE_SIS_Wrapper.ResultOpportunityWrapper();

           result = new CORE_SIS_Wrapper.ResultWrapperGlobal(CORE_SIS_Constants.SUCCESS_STATUS, '', op);

           
           result.opportunity.Id = init_opp.Id;
           result.opportunity.sisOpportunityId = init_opp?.CORE_OPP_SIS_Opportunity_Id__c;
           
           res.responseBody = Blob.valueOf(JSON.serialize(result, false));
           res.statusCode = 200;
           
           system.debug('result: ' + result);
           //system.debug('res: ' + res);

       }
       catch(Exception e){

           result = new CORE_SIS_Wrapper.ResultWrapperGlobal(CORE_SIS_Constants.ERROR_STATUS, e.getMessage() + ' at ' + e.getStackTraceString(), new CORE_SIS_Wrapper.ResultOpportunityWrapper());
           system.debug('@Error Exception: ' + e.getMessage() + ' at ' + e.getStackTraceString());

           Database.rollback(sp);
       } finally{
        dt2 = DateTime.now().getTime();
        System.debug('CORE_AOL_AcademicDiplomaHistory(urlMapping=\'/aol/academic-diploma-history\') : T0TAL TIME OF EXECUTION = ' + (dt2-dt1) + ' ms');
        // creating log
        log.CORE_Processing_Duration__c =(dt2-dt1);
        log.CORE_HTTP_Status_Code__c = result.status == 'KO' ? 400 : result.status == 'OK' ? 200 : 500;
        log.CORE_Sent_Body__c = JSON.serialize(result);
        log.CORE_Received_Body__c =  '{"opportunity" : ' + JSON.serialize(opportunity) + '}';
        
        if(!logSettings.isNone()){
            if(logSettings.isAllLogged()){ // is All logged
                CORE_LogManager.createLog(log);
            }else if(logSettings.isErrorLogged() && isErrorLog){ // is Error
                CORE_LogManager.createLog(log);
            }
        }
    }

       return result;
    }

    public Opportunity createOpportunityProcess(CORE_SIS_Wrapper.OpportunityWrapper opp){
        CORE_Business_Unit__c bu = [SELECT Id FROM CORE_Business_Unit__c WHERE CORE_Business_Unit_ExternalId__c = :opp.businessUnit LIMIT 1];
        CORE_Curriculum__c curriculum = [SELECT Id FROM CORE_Curriculum__c WHERE CORE_Curriculum_ExternalId__c = :opp.curriculum LIMIT 1];
        CORE_Level__c level = [SELECT Id FROM CORE_Level__c WHERE CORE_Level_ExternalId__c = :opp.level LIMIT 1];
        CORE_Intake__c intake = [SELECT Id FROM CORE_Intake__c WHERE CORE_Intake_ExternalId__c = :opp.intake LIMIT 1];
        CORE_CatalogHierarchyModel catalogHierarchy = new CORE_CatalogHierarchyModel(
            null, 
            bu.Id, 
            null,//studyAreaId, 
            (curriculum!=null)?curriculum.Id:null,//curriculumId, 
            (level!=null)?level.Id:null,//levelId, 
            (intake!=null)?intake.Id:null,//intakeId, 
            null
        );
        System.debug('catalog '+ catalogHierarchy );
        CORE_OpportunityDataMaker dataMaker = new CORE_OpportunityDataMaker();
        catalogHierarchy = CORE_OpportunityDataMaker.getCatalogHierarchy(catalogHierarchy, true);
        if(!String.isBlank(opp.product)){

            List<Product2> products = [SELECT Id FROM Product2 WHERE SIS_External_Id__c = :opp.product];

            if(!products.isEmpty()){

                catalogHierarchy.product.Id= products.get(0).Id;

            }
        }
        System.debug('product '+ catalogHierarchy.product.Id );
        Boolean mainOpportunityExist = CORE_OpportunityUtils.isExistingMainOpportunity(catalogHierarchy.businessUnit.Id,opp.accountId,opp.academicYear);
        Account account = new Account(Id = opp.AccountId);
        Boolean ayvalid = (this.availableAcademicYears.contains(opp.academicYear))?true:false;
        String finalAcademicYear = CORE_OpportunityUtils.getAcademicYear(
            dataMaker.getDefaultAcademicYear(),
            ayvalid?opp.academicYear:null,
            catalogHierarchy,
            System.now(),
            this.availableAcademicYears
            );
        System.debug('ay '+ finalAcademicYear );
        Opportunity init_opp = dataMaker.initOpportunity(account, finalAcademicYear, catalogHierarchy, mainOpportunityExist);

        //init_opp.CORE_OPP_Parent_Opportunity__c = opp.enrollOppId;
        init_opp.CORE_OPP_SIS_Opportunity_Id__c = opp.sisOpportunityId;
        
        init_opp.StageName = opp.status;
        init_opp.CORE_OPP_Sub_Status__c = opp.substatus; 
        System.debug('aypbu '+ init_opp.CORE_OPP_Academic_Year__c );
        System.debug('aypbu '+ init_opp.CORE_OPP_Business_Unit__c);
        
        CORE_PriceBook_BU__c pbBU = [SELECT Id, CORE_Price_Book__c,CORE_Business_Unit__r.CORE_BU_Technical_User__c
                                    FROM CORE_PriceBook_BU__c 
                                    WHERE CORE_Academic_Year__c = :init_opp.CORE_OPP_Academic_Year__c 
                                    AND CORE_Business_Unit__c = :init_opp.CORE_OPP_Business_Unit__c LIMIT 1];

        init_opp.Pricebook2Id = pbBU.CORE_Price_Book__c;
        init_opp.OwnerId = pbBU.CORE_Business_Unit__r.CORE_BU_Technical_User__c;
       
        insert init_opp;

        if(!String.IsBlank(init_opp.CORE_Main_Product_Interest__c) 
               && !String.IsBlank(init_opp.Pricebook2Id)){
            PricebookEntry pricebookEntry = dataMaker.getProductPricebookEntryByPb(init_opp.Pricebook2Id, init_opp.CORE_Main_Product_Interest__c);
            if(pricebookEntry != NULL){
                    OpportunityLineItem oppLineItem = dataMaker.initOppLineItem(init_opp, pricebookEntry);

                   
                    insert oppLineItem;
            }
        }
        Opportunity formeropp = [SELECT Id,CORE_Re_registered_opportunity__c FROM Opportunity WHERE Id = :opp.enrollOppId];
        formeropp.CORE_Re_registered_opportunity__c = init_opp.Id;
        update formeropp;
            
        return init_opp;
    }
    @TestVisible
    private static CORE_SIS_Wrapper.ResultWrapperGlobal checkMandatoryParameters(CORE_SIS_Wrapper.OpportunityWrapper opp){
        List<String> missingParameters = new List<String>();
        List<String> mandatoryParameters = new List<String> {'enrollOppId','sisOpportunityId','accountId','businessUnit','status','substatus'};
        for(String mandatoryParameter : mandatoryParameters){
            if(String.IsBlank((String) opp.get(mandatoryParameter))){
                missingParameters.add(mandatoryParameter);
            }
        }
        return CORE_SIS_GlobalWorker.getMissingParameters(missingParameters);
    }
}