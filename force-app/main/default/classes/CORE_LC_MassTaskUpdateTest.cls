@isTest
private class CORE_LC_MassTaskUpdateTest {
    private static final String VIEW_NAME = 'OpenTasks';
    private static final String EXTERNALID = '1234';
    private static final String ACADEMIC_YEAR = 'CORE_PKL_2023-2024';

    @testSetup 
    static void createData(){
        Id profileId = [SELECT Id FROM Profile WHERE Name = 'CORE_API_Profile' LIMIT 1].Id;

        User usr = new User(
            Username = 'usertest@masstest.com',
            LastName = 'TEST_USER',
            Email = 'testemail@yopmail.com',
            Alias = 'T', 
            CommunityNickname = 'T',
            TimeZoneSidKey = 'Europe/Paris',
            LocaleSidKey = 'fr_FR_EURO',
            EmailEncodingKey = 'ISO-8859-1', 
            ProfileId = profileId,
            LanguageLocaleKey = 'fr'      
        );
        database.insert(usr, true);
        CORE_CatalogHierarchyModel catalogHierarchy = new CORE_DataFaker_CatalogHierarchy(EXTERNALID).catalogHierarchy;
        System.runAs(usr){
            Account account = CORE_DataFaker_Account.getStudentAccount(EXTERNALID);
            Opportunity opp1 = new Opportunity(
                Name ='test1',
                StageName = CORE_AOL_Constants.PKL_OPP_STAGENAME_PROSPECT,
                CORE_OPP_Sub_Status__c = CORE_AOL_Constants.PKL_OPP_SUBSTATUS_PROSPECTAPPSTARTED,
                CORE_OPP_Division__c = catalogHierarchy.division.Id,
                CORE_OPP_Business_Unit__c = catalogHierarchy.businessUnit.Id,
                CORE_OPP_Study_Area__c = catalogHierarchy.studyArea.Id,
                CORE_OPP_Curriculum__c = catalogHierarchy.curriculum.Id,
                CORE_OPP_Level__c = catalogHierarchy.level.Id,
                CORE_OPP_Intake__c = catalogHierarchy.intake.Id,
                CORE_OPP_Academic_Year__c = ACADEMIC_YEAR,
                AccountId = account.Id,
                CloseDate = Date.Today().addDays(2),
                OwnerId = UserInfo.getUserId(),
                CORE_OPP_Main_Opportunity__c = true,
                CORE_Main_Product_Interest__c = catalogHierarchy.product.Id
            );

            Opportunity opp2 = new Opportunity(
                Name ='test2',
                StageName = CORE_AOL_Constants.PKL_OPP_STAGENAME_PROSPECT,
                CORE_OPP_Sub_Status__c = CORE_AOL_Constants.PKL_OPP_SUBSTATUS_PROSPECTAPPSTARTED,
                CORE_OPP_Division__c = catalogHierarchy.division.Id,
                CORE_OPP_Business_Unit__c = catalogHierarchy.businessUnit.Id,
                CORE_OPP_Study_Area__c = catalogHierarchy.studyArea.Id,
                CORE_OPP_Curriculum__c = catalogHierarchy.curriculum.Id,
                CORE_OPP_Level__c = catalogHierarchy.level.Id,
                CORE_OPP_Intake__c = catalogHierarchy.intake.Id,
                CORE_OPP_Academic_Year__c = ACADEMIC_YEAR,
                AccountId = account.Id,
                CloseDate = Date.Today().addDays(2),
                OwnerId = UserInfo.getUserId()
            );
        
            database.insert(new List<Opportunity> { opp1, opp2  }, true);
            System.debug('inserting done opp product '+ opp1.CORE_Main_Product_Interest__c);
            Id rtId = Schema.SObjectType.Task.getRecordTypeInfosByDeveloperName().get('Master').getRecordTypeId();
            System.debug('record type '+rtId);
            Task tache1 = new Task(Status ='CORE_PKL_Open',RecordTypeId = rtId,CORE_TECH_CallTaskType__c = 'CORE_PKL_ContactEmail',WhatId = opp1.Id);
            Task tache2 = new Task(Status ='CORE_PKL_Open',RecordTypeId = rtId,CORE_TECH_CallTaskType__c = 'CORE_PKL_ContactEmail',WhatId = opp2.Id);
            database.insert(new List<Task> { tache1, tache2  }, true);
        }
    }

    @isTest
    static void test_NoSetting(){

        User usr = [SELECT Id FROM User WHERE Username = 'usertest@masstest.com' LIMIT 1];

        System.runAs(usr){
            test.startTest();

            try{
                CORE_LC_MassTaskUpdate.getOrgSetting();
            }
            catch(Exception e){
                system.debug('@test_NoSetting > e: ' + e);
                system.assertEquals(Label.CORE_MTU_MSG_NO_SETTINGS, e.getMessage());
            }

            test.stopTest();
        }
    }

   @isTest
    static void test_Sync(){
        CORE_MassTasksUpdate__c orgSetting = new CORE_MassTasksUpdate__c(
            CORE_BatchSize__c = 20, CORE_ListViewNameFilter__c = VIEW_NAME
        );
        database.insert(orgSetting, true);

        User usr = [SELECT Id FROM User WHERE Username = 'usertest@masstest.com' LIMIT 1];

        System.runAs(usr){
            List<Opportunity> opps=[SELECT Id FROM Opportunity];
            List<Task> tasks=[SELECT Id FROM Task];
            System.debug('size opportunities'+opps.size());
            System.debug('size tasks'+tasks.size());
            StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
            mock.setStaticResource('CORE_MassTasksupdate_VIEW_RESP');
            mock.setStatusCode(200);
            // Set the mock callout mode
            Test.setMock(HttpCalloutMock.class, mock);

            test.startTest();
            CORE_LC_MassTaskUpdate.SettingWrapper setting = CORE_LC_MassTaskUpdate.getOrgSetting();
            System.debug('setting list view filter'+ setting.listViewFilter);
            system.assertEquals(setting.listViewFilter, VIEW_NAME);

            List<CORE_LC_MassTaskUpdate.ListViewWrapper> listViews = CORE_LC_MassTaskUpdate.getListViewsInfo();
            system.assertEquals(listViews[0].viewName, VIEW_NAME);
            System.debug('list views 0 '+listViews[0].viewName);

            Map<String, String> theObj = new Map<String, String>();
            theObj.put('value',listViews[0].viewId);
            theObj.put('query',listViews[0].viewQuery);
            CORE_LC_MassTaskUpdate.CurrentViewWrapper view = new CORE_LC_MassTaskUpdate.CurrentViewWrapper();
            view.viewQuery = listViews[0].viewQuery;
            view.viewId = listViews[0].viewId;
            view.viewRecordsSize = 20;
            List<Task> tasksList = database.query(view.viewQuery);
            System.debug('tasks '+ tasksList);
            //system.assertEquals(view.viewRecordsSize, oppsList.size());

            List<CORE_LC_MassTaskUpdate.BUWrapper> buWrapList = CORE_LC_MassTaskUpdate.getBUs(view);
            List<CORE_LC_MassTaskUpdate.ayWrapper> ayWrapList = CORE_LC_MassTaskUpdate.getAYs(view,buWrapList[0].buId);
            List<CORE_LC_MassTaskUpdate.subtypeWrapper> stWrapList = CORE_LC_MassTaskUpdate.getTaskSubtype(view,buWrapList[0].buId, ACADEMIC_YEAR);
            List<CORE_LC_MassTaskUpdate.pklWrapper> pklWrapper1 =  CORE_LC_MassTaskUpdate.getPicklistValues('Task','Core_Task_Result__c');
            System.debug('bu wrapper '+ buWrapList);

            String result = CORE_LC_MassTaskUpdate.runUpdateTasks(view, buWrapList[0].buId, ACADEMIC_YEAR,'CORE_PKL_ContactEmail','CORE_PKL_Completed',	
            'CORE_PKL_Reached_Hot_Invited_To_An_Event',null );
            //system.assertEquals(null, result);
            test.stopTest();
            
                
        }

            
        }


    @isTest
    static void test_Async(){
        CORE_MassTasksUpdate__c orgSetting = new CORE_MassTasksUpdate__c(
            CORE_BatchSize__c = 1, CORE_ListViewNameFilter__c = VIEW_NAME
        );
        database.insert(orgSetting, true);

        User usr = [SELECT Id FROM User WHERE Username = 'usertest@masstest.com' LIMIT 1];

        System.runAs(usr){

            StaticResourceCalloutMock mock = new StaticResourceCalloutMock();
            mock.setStaticResource('CORE_MassTasksupdate_VIEW_RESP');
            mock.setStatusCode(200);
            
            // Set the mock callout mode
            Test.setMock(HttpCalloutMock.class, mock);

            test.startTest();
            CORE_LC_MassTaskUpdate.SettingWrapper setting = CORE_LC_MassTaskUpdate.getOrgSetting();
            system.assertEquals(setting.listViewFilter, VIEW_NAME);

            List<CORE_LC_MassTaskUpdate.ListViewWrapper> listViews = CORE_LC_MassTaskUpdate.getListViewsInfo();
            system.assertEquals(listViews[0].viewName, VIEW_NAME);

            Map<String, String> theObj = new Map<String, String>();
            theObj.put('value',listViews[0].viewId);
            theObj.put('query',listViews[0].viewQuery);
            CORE_LC_MassTaskUpdate.CurrentViewWrapper view = new CORE_LC_MassTaskUpdate.CurrentViewWrapper();
            view.viewQuery = listViews[0].viewQuery;
            view.viewId = listViews[0].viewId;
            view.viewRecordsSize = 20;
            List<Task> tasksList = database.query(view.viewQuery);

            //system.assertEquals(view.viewRecordsSize, oppsList.size());

            List<CORE_LC_MassTaskUpdate.BUWrapper> buWrapList = CORE_LC_MassTaskUpdate.getBUs(view);
            
            System.debug('bu wrapper '+ buWrapList);


            String result = CORE_LC_MassTaskUpdate.runUpdateTasks(view, buWrapList[0].buId, ACADEMIC_YEAR,'CORE_PKL_ContactEmail','CORE_PKL_Completed',	
            'CORE_PKL_Reached_Hot_Invited_To_An_Event',null );

          
            test.stopTest();
            System.debug('job in progress'+ orgSetting.CORE_JobInProgress__c);
        }
    }

    @isTest
    static void test_Async_Error(){
        CORE_MassTasksUpdate__c orgSetting = new CORE_MassTasksUpdate__c(
            CORE_BatchSize__c = 10, CORE_ListViewNameFilter__c = VIEW_NAME
        );
        database.insert(orgSetting, true);

        User usr = [SELECT Id FROM User WHERE Username = 'usertest@masstest.com' LIMIT 1];

        System.runAs(usr){

            test.startTest();
            try{
                System.enqueueJob(new CORE_QU_MassTaskUpdate(null, null, null, null, null, null,null,null));
            }
            catch(Exception e){
                system.assertEquals('AuraHandledException', e.getTypeName());
            }

            test.stopTest();
        }
    }
}