/*************************************************************************************************************
 * @name            IT_CampaignMemberTriggerHandler
 * @author          Andrea Morittu <amorittu@atlantic-technologies.com>
 * @created         22 / 04 / 2022
 * @description     handler of IT_CampaignMemberTrigger
 *
 * Changes (version)
 * -----------------------------------------------------------------------------------------------------------
 *              No.     Date            Author                  Description
 *              ----    ------------    --------------------    ----------------------------------------------
 * @version     1.0     2022-04-22      Andrea Morittu          Creation
 * @version     1.1     2022-08-30      Andrea Morittu          TK-000487 Logics
 *
**************************************************************************************************************/
public class IT_CampaignMemberTriggerHandler extends TriggerHandler {
    public static final String CLASS_NAME = IT_CampaignMemberTriggerHandler.class.getName();
    public static final String OPEN_DAY_PROCESS = 'Open day';

    public static final String[] CAMPAIGN_MEDIUM_VALUES = new String[]{'IT_PKL_Walkin' , 'CORE_PKL_Walkin' };
   
    public static Boolean fireWalkInLogics = true;

    /*********************************************************************************************************
     * @name            The name of your class or method
     * @author          Andrea Morittu <amorittu@atlantic-technologies.com>
     * @created         22 / 04 / 2022
     * @description     afterInsert logics
     * @param           String param : Explanation
     * @return          Explanation of the return value
    **********************************************************************************************************/
    public void afterInsert(List<CampaignMember> newRecordsList, Map<Id, CampaignMember> oldRecordsMap) {
        System.debug(CLASS_NAME + ' - afterInsert has been fired -- START, newRecordsList: '+newRecordsList);
        IT_OpportunityStatusMgm.checkCampaignAdding(newRecordsList, null);
        // performCoaLogics(newRecordsList, oldRecordsMap);
        if (fireWalkInLogics) {
            performAttendanceWithNoRegistrationLogics(newRecordsList);
        }
        
        System.debug(CLASS_NAME + ' - afterInsert finished -- END');
    }

    public void afterUpdate(List<CampaignMember> newRecordsList, Map<Id, CampaignMember> oldRecordsMap) {
        System.debug(CLASS_NAME + ' - afterUpdate has been fired -- START, newRecordsList: '+newRecordsList);
        IT_OpportunityStatusMgm.checkCampaignAdding(newRecordsList, oldRecordsMap);
       
       
        // query users that are named with 'integration' - We'll have to exclude the coa process because it will be fired by API (leadProcess)
        List<User> integrationAPIusers = [SELECT Id FROM User WHERE Name LIKE '%Integration%'];
        Set<String> integrationAPIusersIDS = new Set<String>();
        
        //itarate integration Users and put them inside a set
        for (User currentIntegrationUser : integrationAPIusers) {
            integrationAPIusersIDS.add(currentIntegrationUser.Id);
        }

        Map<String, CampaignMember> campaign_CampaignMember = new Map<String, CampaignMember>();
        
        //iterate campaignMember (record trigger)
        for (CampaignMember campaignMembRec : newRecordsList) {
            System.debug('IntegrationId is the current campaign.creator ? : ' + integrationAPIusersIDS.contains(campaignMembRec.CreatedById) );
            // fire coa if: a) we have an oppotunity; b) opportunityId is changing, c) the trigger is not invoked by an integration user
            if ( String.isNotBlank(campaignMembRec.CORE_Opportunity__c) && oldRecordsMap.get(campaignMembRec.Id) != null 
                    && oldRecordsMap.get(campaignMembRec.Id).CORE_Opportunity__c != campaignMembRec.CORE_Opportunity__c
                    && !integrationAPIusersIDS.contains(campaignMembRec.CreatedById) ) {
                campaign_CampaignMember.put(campaignMembRec.CampaignId, campaignMembRec);
            }
        }

        Map<Campaign, CampaignMember> openDayCampaign_CampaignMember = new Map<Campaign, CampaignMember>();

        if (campaign_CampaignMember.keySet().size() > 0 ) {
            for(Campaign openDayCampaign : [SELECT Id FROM Campaign WHERE Id IN: campaign_CampaignMember.keySet()  AND Type =: OPEN_DAY_PROCESS AND IT_COANotApplied__c = FALSE  ]) {

                if (campaign_CampaignMember.containsKey(openDayCampaign.Id) ) {
                    CampaignMember currentCampaignMember = campaign_CampaignMember.get(openDayCampaign.Id);

                    if(!openDayCampaign_CampaignMember.containsKey(openDayCampaign)) {
                        openDayCampaign_CampaignMember.put(openDayCampaign, currentCampaignMember);
                    }
                }
            }

        }
        
        if (openDayCampaign_CampaignMember.keySet().size() > 0) {
            System.enqueueJob(new IT_PrepareCampaignMemberCOA(openDayCampaign_CampaignMember));
        }
        System.debug(CLASS_NAME + ' - afterUpdate finished -- END');
    }


    public static void performCoaLogics(List<CampaignMember> newRecordsList, Map<Id, CampaignMember> oldRecordsMap) {
        System.debug(CLASS_NAME + ' - performCoaLogics - START ');

        // query users that are named with 'integration' - We'll have to exclude the coa process because it will be fired by API (leadProcess)
        List<User> integrationAPIusers = [SELECT Id FROM User WHERE Name LIKE '%Integration%'];
        Set<String> integrationAPIusersIDS = new Set<String>();
        
        //iterate integration Users and put them inside a set
        for (User currentIntegrationUser : integrationAPIusers) {
            integrationAPIusersIDS.add(currentIntegrationUser.Id);
        }

        Map<String, CampaignMember> campaign_CampaignMember = new Map<String, CampaignMember>();

        //iterate campaignMember (record trigger)
        for (CampaignMember campaignMembRec : newRecordsList) {
            if ( String.isNotBlank(campaignMembRec.CORE_Opportunity__c) ) {
                System.debug(CLASS_NAME + ' - performCoaLogics - integrationAPIusersIDS.contains(campaignMembRec.CampaignId) ? : ' + integrationAPIusersIDS.contains(campaignMembRec.CampaignId) );
                // if the campaign Id not contains the integration user id, it means he is not an integration user, so proceed with the process
                if ( !integrationAPIusersIDS.contains(campaignMembRec.CreatedById) ) {
                    campaign_CampaignMember.put(campaignMembRec.CampaignId, campaignMembRec);
                } 
            }
        }

        Map<Campaign, CampaignMember> openDayCampaign_CampaignMember = new Map<Campaign, CampaignMember>();

        if ( !campaign_CampaignMember.keySet().isEmpty()  ) {
            for(Campaign openDayCampaign : [SELECT Id FROM Campaign WHERE Id IN: campaign_CampaignMember.keySet()  AND Type =: OPEN_DAY_PROCESS AND IT_COANotApplied__c = FALSE  ]) {
                if (campaign_CampaignMember.containsKey(openDayCampaign.Id) ) {
                    CampaignMember currentCampaignMember = campaign_CampaignMember.get(openDayCampaign.Id);

                    if(!openDayCampaign_CampaignMember.containsKey(openDayCampaign)) {
                        openDayCampaign_CampaignMember.put(openDayCampaign, currentCampaignMember);
                    }
                }
            }

        }
        
        if ( !openDayCampaign_CampaignMember.keySet().isEmpty() ) {
            System.enqueueJob(new IT_PrepareCampaignMemberCOA(openDayCampaign_CampaignMember));
        }
        System.debug(CLASS_NAME + ' - performCoaLogics - END ');
    }

    /*********************************************************************************************************
     * @name			The name of your class or method
     * @author			Andrea Morittu <amorittu@atlantic-technologies.com>
     * @created			30 / 08 / 2022
     * @description		Main 'Container' method. It calls a method to search the records that match the filters and a method that initialize a list of task.
     *                  At the end, it will insert the list of records
     *                  It refers to TK-000487: Walk-In Logic
     * @param			String param : Explanation
     * @return			Explanation of the return value
    **********************************************************************************************************/
    public static void performAttendanceWithNoRegistrationLogics(List<CampaignMember> newRecordsList) {
        CampaignMember[] walkInCmembers = checkAttendanceWithoutRegistration(newRecordsList);
		System.debug('IT_CampaignMemberTriggerHandler - walkInCmembers ' + walkInCmembers);
        if (! walkInCmembers.isEmpty() ) {
            Task[] tasksToCreate = new Task[]{};
            for (CampaignMember cm : walkInCmembers) {
                cm.Status = 'Walk-in';

                Task taskToCreate = initializeWalkInTask(cm);
                tasksToCreate.add(taskToCreate);
            }
            System.debug(CLASS_NAME + ' performAttendanceWithNoRegistrationLogics - walkInCmembers : ' + walkInCmembers );
            update walkInCmembers;
           
            System.debug(CLASS_NAME + ' performAttendanceWithNoRegistrationLogics - tasksToCreate : ' + tasksToCreate );
            if ( !tasksToCreate.isEmpty() ) {
                insert tasksToCreate;
                fireWalkInLogics = false;
            }

        }
    }

    /*********************************************************************************************************
     * @name			The name of your class or method
     * @author			Andrea Morittu <amorittu@atlantic-technologies.com>
     * @created			30 / 08 / 2022
     * @description		It checkes if there are opportunities that match to the criteria for each division (!= 'IM' it means Naba and Domus)
     *                  It refers to TK-000487: Walk-In Logic
     * @param			CampaignMember records After Insert
     * @return			return all the records that fit the filters
    **********************************************************************************************************/
    public static CampaignMember[] checkAttendanceWithoutRegistration(List<CampaignMember> newRecordsList ) {
        String[] campaignMembersIds = new String[]{};
        Map<String, CampaignMember[]> campaign_cMembersMap = new Map<String, CampaignMember[]>();
        Map<String, CampaignMember[]> opportunity_cMembersMap = new Map<String, CampaignMember[]>();
        for(CampaignMember cm : newRecordsList) {
            campaignMembersIds.add(cm.Id);
        }
        
        if (campaignMembersIds.isEmpty() ) {
            return null;
        }

        fireWalkInLogics = false;
		System.debug(' checkAttendanceWithoutRegistration - campaignMembersIds : ' + campaignMembersIds );
        return [
            SELECT Id, toLabel(Campaign.Type), Status, CORE_Opportunity__r.CORE_Campaign_Medium__c, CORE_Opportunity__r.IT_OpportunityAssigned__c, 
            CORE_Opportunity__r.IT_InformationQueue__c, CORE_Opportunity__r.IT_InformationQueue__r.OwnerId
            FROM CampaignMember
            WHERE Id IN: campaignMembersIds
            AND CORE_Opportunity__c IN (
                SELECT CORE_Opportunity__c 
                FROM CORE_Point_of_Contact__c 
                WHERE (
                    ( 
                        CORE_Opportunity__r.IT_DivisionExternalId__c = 'IM' 
                        AND CORE_Origin__c LIKE '%ipad%' 
                        AND CORE_Capture_Channel__c IN ('CORE_PKL_Open_Day_JPO_SPO', 'IT_PKL_VirtualOpendayAttendance')
                    )
                    OR  (
                        CORE_Opportunity__r.IT_DivisionExternalId__c != 'IM' 
                        AND Campaign_Medium__c IN: CAMPAIGN_MEDIUM_VALUES 
                    )
                )
            )
        ];
    }

    /*********************************************************************************************************
     * @name			The name of your class or method
     * @author			Andrea Morittu <amorittu@atlantic-technologies.com>
     * @created			30 / 08 / 2022
     * @description		Initialize Task inside a loop related to each campaignMember. 
     *                  It refers to TK-000487: Walk-In Logic
     * @param			CampaignMember ( Main Object that gives lookups to the record )
     * @return			it returns a task
    **********************************************************************************************************/
    public static Task initializeWalkInTask( CampaignMember cm ) {
        Task task                               = new Task();

        task.CORE_TECH_CallTaskType__c          = 'IT_PKL_Event';
        task.WhatId                             = cm.CORE_Opportunity__c;
        task.CORE_Campaign__c                   = cm.CampaignId;
        task.Status                             = 'CORE_PKL_Completed';
        task.CORE_Is_Incoming__c                = true;
        task.Core_Task_Result__c                = null;
        // task.Type                               = TBD;
        task.CORE_TECH_CallTaskType__c          = 'IT_PKL_Event';
        task.Subject                            = cm.Campaign.Type + ' Attendance - Walk-in';
        
        if ( cm.CORE_Opportunity__r.IT_OpportunityAssigned__c ) {
            task.OwnerId        = cm.CORE_Opportunity__r.OwnerId;
        } else if (String.isNotBlank(cm.CORE_Opportunity__r.IT_InformationQueue__c) ) {
            task.OwnerId        =  cm.CORE_Opportunity__r.IT_InformationQueue__r.OwnerId;
        } else {
            task.OwnerId        = UserInfo.getUserId();
        }
        return task;
    }
}