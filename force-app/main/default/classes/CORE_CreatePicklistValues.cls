/**
* @author Zameer Khodabocus - Almavia
* @date 2022-04-28
* @group Twilio
* @description Class used create picklist values in object, CORE_CUBE_Picklist__c
* @TestClass CORE_CUBE_CreatePicklistBatchTest
*/
public without sharing class CORE_CreatePicklistValues {

    /*
    *   FORMAT: Map of Object API Name => Field Name => Sent to Cube Boolean
    */
    public static Map<String, Map<String, Boolean>> objPicklistFieldsMap = new Map<String, Map<String, Boolean>> {
        'Contact' => new Map<String, Boolean> {
            'Salutation' => FALSE, 'CORE_Professional_situation__c' => FALSE, 'CORE_Language_website__c' => FALSE
        },
        'User' => new Map<String, Boolean> {
            'StateCode' => FALSE, 'CountryCode' => FALSE
        },
        'Opportunity' => new Map<String, Boolean> {
            'CORE_OPP_Learning_Material__c' => FALSE,
            'CORE_OPP_Funding__c' => FALSE
        },
        'CORE_Consent__c' => new Map<String, Boolean> {
            'CORE_Consent_type__c' => FALSE,
            'CORE_Consent_channel__c' => FALSE
        },
        'CORE_Point_of_Contact__c'=> new Map<String, Boolean> {
            'CORE_Form__c' => TRUE,
            'CORE_Form_area__c' => TRUE
        },
        'CORE_PriceBook_BU__c'=> new Map<String, Boolean> {
            'CORE_Commercial_Business_Unit__c' => FALSE
        }
    };

    public static List<CORE_CUBE_Picklist__c> getPicklistRecords(String objectName, String language){
        List<CORE_CUBE_Picklist__c> picklistValuesList = new List<CORE_CUBE_Picklist__c>();

        Map<String, Map<String, String>> fieldsPicklistMap = getFieldsPicklistMap(objectName);
        Map<String, Boolean> fieldSendToCUBEMap = objPicklistFieldsMap.get(objectName);

        for(String field : fieldsPicklistMap.keySet()){
            for(String plApi : fieldsPicklistMap.get(field).keySet()){
                String plLbl = fieldsPicklistMap.get(field).get(plApi);

                picklistValuesList.add(
                    new CORE_CUBE_Picklist__c(
                        CORE_CUBE_List__c = field,
                        CORE_CUBE_Language__c = language,
                        CORE_CUBE_Code__c = plApi,
                        CORE_CUBE_Label__c = plLbl,
                        Name = field +' - '+ plLbl,
                        CORE_CUBE_ExternalId__c = field +'_'+ plApi,
                        CORE_CUBE_Sent_to_CUBE__c = fieldSendToCUBEMap.get(field)
                    )
                );
            }
        }

        return picklistValuesList;
    }

    public static List<CORE_CUBE_Picklist__c> generatePicklistValues(){
        List<CORE_CUBE_Picklist__c> picklistValuesList = new List<CORE_CUBE_Picklist__c>();

        //Get Current User Language
        String language = '';
        String userLanguage = UserInfo.getLanguage();

        for(PicklistEntry value: User.LanguageLocalekey.getDescribe().getPicklistValues()) {
            if(value.getValue() == userLanguage) {
                language = value.getLabel();
                break;
            }
        }

        picklistValuesList.addAll(getPicklistRecords('Contact',language));
        picklistValuesList.addAll(getPicklistRecords('User',language));
        picklistValuesList.addAll(getPicklistRecords('Opportunity',language));
        picklistValuesList.addAll(getPicklistRecords('CORE_Consent__c',language));
        picklistValuesList.addAll(getPicklistRecords('CORE_Point_of_Contact__c',language));
        picklistValuesList.addAll(getPicklistRecords('CORE_PriceBook_BU__c',language));

        //system.debug('@CORE_CreatePicklistValues > picklistValuesList: ' + JSON.serializePretty(picklistValuesList));

        return picklistValuesList;
    }

    public static Map<String, Map<String, String>> getFieldsPicklistMap(String objName) {
        //system.debug('@getFieldsPicklistMap > objName: ' + objName);
        Map<String, Map<String, String>> fieldPicklistValuesMap = new Map<String, Map<String, String>>();

        if(objPicklistFieldsMap.containsKey(objName)) {
            Map<String, Schema.SObjectField> fields = Schema.getGlobalDescribe().get(objName).getDescribe().SObjectType.getDescribe().fields.getMap();
            //system.debug('@getFieldsPicklistMap > fields: ' + fields);

            for(Schema.SObjectField field : fields.values()) {
                //system.debug('@getFieldsPicklistMap > field: ' + field);

                if(objPicklistFieldsMap.get(objName).containsKey(field.getDescribe().getName())){
                    for(Schema.PicklistEntry f : field.getDescribe().getPicklistValues()) {
                        //system.debug('@getFieldsPicklistMap > f: ' + f);

                        if(fieldPicklistValuesMap.containsKey(field.getDescribe().getName())) {
                            fieldPicklistValuesMap.get(field.getDescribe().getName()).put(f.getValue(), f.getLabel());
                        }
                        else{
                            fieldPicklistValuesMap.put(field.getDescribe().getName(), new Map<String, String> { f.getValue() => f.getLabel() });
                        }
                    }
                }
            }
        }

        return fieldPicklistValuesMap;
    }
}