@isTest
public class IT_ScriptClassTest {
    
    @isTest
    public static void testScriptClass() {
        CORE_DataFaker_CatalogHierarchy catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test1');
		Account account = CORE_DataFaker_Account.getStudentAccount('test1');
        Opportunity opportunity = CORE_DataFaker_Opportunity.getOpportunity(
				catalogHierarchy.division.Id,
				catalogHierarchy.businessUnit.Id,
				account.Id,
				'Applicant',
				'Applicant - Application Submitted / New'
		);
        Test.startTest();
            Database.executeBatch(new IT_ScriptClass(new String []{opportunity.Id} ) );
        Test.stopTest();
    }
}