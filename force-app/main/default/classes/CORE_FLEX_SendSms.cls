public class CORE_FLEX_SendSms {
	@InvocableMethod
	public static List<Integer> execute(List<FlowInput> params) {
        List<Integer> result = new List<Integer>();
        if(params==null) {
            result.add(-1);
            return result;
        }        

        String userId = UserInfo.getUserId();        
        system.debug('params : '+params.get(0));


        FlowInput input = params.get(0);
        String phone = input.phone;
        Account account = input.account;
        system.debug('start send sms with userId : '+userId+', to :'+phone+', account:'+account);

        if(phone==null || phone=='') {
            result.add(-1);
            return result;
        }

        
        User user = [SELECT Email FROM User WHERE Id=:userId];
        String email = user.Email;
        system.debug('email : '+email);
        
        CORE_FLEX_Setting__mdt setting = [SELECT UrlFlex__c, TokenFlex__c FROM CORE_FLEX_Setting__mdt LIMIT 1];
        string url = setting.UrlFlex__c;
        string accountToken = setting.TokenFlex__c;
        
        string pathname = '/message/smsoutbound';
        
        //string phone='+33645802109';
        
        string urlSign = url+pathname;
        
        Blob signatureBlob = Crypto.generateMac('hmacSHA1', Blob.valueOf(urlSign), Blob.valueOf(accountToken));
        string token = EncodingUtil.base64Encode(signatureBlob);
        
        HttpRequest req = new HttpRequest();
     	req.setEndpoint(url+pathname);
        req.setHeader('Content-Type','application/json');
     	req.setMethod('POST');
     
 	    req.setHeader('X-Twilio-Signature', token);
        string name = 'no name account';
        if(account!=null) {
            name = account.Name;
        }

		JSONGenerator gen = JSON.createGenerator(true);    
		gen.writeStartObject();      
		gen.writeStringField('phone', phone);
        gen.writeStringField('emailWorker', email);
        gen.writeStringField('customerName', name);
		gen.writeEndObject();    
		String jsonS = gen.getAsString();
        
        req.setBody(jsonS);
   
        try {
            Http http = new Http();
            HTTPResponse res = http.send(req);
            
            String resultS = res.getBody();            
           system.debug('Result : '+resultS+', status:'+res.getStatusCode());
           result.add(res.getStatusCode()==200?0:-1);
        } catch(Exception e) {
            System.debug('Unable to send sms, exception:'+e);
            result.add(-1);
        }
        
        return result;
        
	}

    public class FlowInput {
        @InvocableVariable
        public String phone;

        @InvocableVariable
        public Account account;
    }
}