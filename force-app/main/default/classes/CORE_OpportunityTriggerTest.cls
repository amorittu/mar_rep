@IsTest
public class CORE_OpportunityTriggerTest {
    @TestSetup
    public static void makeData(){
        CORE_DataFaker_CatalogHierarchy catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test1');
        Account account = CORE_DataFaker_Account.getStudentAccount('test1');
        Opportunity opportunity = CORE_DataFaker_Opportunity.getOpportunity(catalogHierarchy.division.Id, 
            catalogHierarchy.businessUnit.Id, 
            catalogHierarchy.studyArea.Id,
            catalogHierarchy.curriculum.Id,
            catalogHierarchy.level.Id,
            catalogHierarchy.intake.Id, 
            account.Id, 
            'Lead', 
            'Lead - New'
        ); 
    }

    @IsTest
    public static void delete_opportunity_should_upsert_deletedObject() {
        List<Opportunity> opportunities = [Select Id, StageName, CORE_OPP_Sub_Status__c, LastModifiedDate FROM Opportunity];

        Test.startTest();
        delete opportunities;
        Test.stopTest();

        List<CORE_Deleted_record__c> results = [Select Id from CORE_Deleted_record__c];

        System.assert(0 < results.size());
    }

    @IsTest
    public static void insert_opportunity_should_insert_timestamps() {
        CORE_DataFaker_CatalogHierarchy catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test2');
        Account account = CORE_DataFaker_Account.getStudentAccount('test2');

        List<CORE_Sub_Status_Timestamp_History__c> subStatusHistory = [SELECT Id FROM CORE_Sub_Status_Timestamp_History__c];
        List<CORE_Status_Timestamp_History__c> statusHistory = [SELECT Id FROM CORE_Status_Timestamp_History__c];

        delete subStatusHistory;
        delete statusHistory;

        subStatusHistory = [SELECT Id FROM CORE_Sub_Status_Timestamp_History__c];
        statusHistory = [SELECT Id FROM CORE_Status_Timestamp_History__c];
        
        System.assertEquals(0, subStatusHistory.size());
        System.assertEquals(0, statusHistory.size());

        Test.startTest();
        Opportunity opportunity = CORE_DataFaker_Opportunity.getOpportunity(catalogHierarchy.division.Id, 
            catalogHierarchy.businessUnit.Id, 
            catalogHierarchy.studyArea.Id,
            catalogHierarchy.curriculum.Id,
            catalogHierarchy.level.Id,
            catalogHierarchy.intake.Id, 
            account.Id, 
            'Lead', 
            'Lead - New'
        ); 
        Test.stopTest();

        subStatusHistory = [SELECT Id FROM CORE_Sub_Status_Timestamp_History__c];
        statusHistory = [SELECT Id FROM CORE_Status_Timestamp_History__c];

        System.assert(0 < subStatusHistory.size());
        System.assert(0 < statusHistory.size());
    }

    @IsTest
    public static void update_opportunity_should_upsert_timestamps() {
        List<Opportunity> opportunities = [Select Id, StageName, CORE_OPP_Sub_Status__c, LastModifiedDate FROM Opportunity];

        List<CORE_Sub_Status_Timestamp_History__c> subStatusHistory = [SELECT Id FROM CORE_Sub_Status_Timestamp_History__c];
        List<CORE_Status_Timestamp_History__c> statusHistory = [SELECT Id FROM CORE_Status_Timestamp_History__c];

        delete subStatusHistory;
        delete statusHistory;

        subStatusHistory = [SELECT Id FROM CORE_Sub_Status_Timestamp_History__c];
        statusHistory = [SELECT Id FROM CORE_Status_Timestamp_History__c];
        System.assertEquals(0, subStatusHistory.size());
        System.assertEquals(0, statusHistory.size());

        Test.startTest();
        Opportunity opp = opportunities.get(0);
        opp.StageName = 'Prospect';
        opp.CORE_OPP_Sub_Status__c = 'Prospect - New';
        opp.CORE_Change_Status__c = true;
        Update opp;
        Test.stopTest();

        subStatusHistory = [SELECT Id FROM CORE_Sub_Status_Timestamp_History__c];
        statusHistory = [SELECT Id FROM CORE_Status_Timestamp_History__c];

        System.assert(0 < subStatusHistory.size());
        System.assert(0 < statusHistory.size());
    }
}