public class CORE_Livestorm_DeleteCampaign implements Database.Batchable<SObject>, Database.AllowsCallouts{
    Set<Id> campaignIds;

    public CORE_Livestorm_DeleteCampaign(Set<Id> c){
        System.debug('CORE_Livestorm_DeleteCampaign : START');
        campaignIds = c;
}


    public Database.QueryLocator start(Database.BatchableContext bc){
        return Database.getQueryLocator([SELECT Id, CORE_Campaign_Full_Name__c, CORE_Online_event__c, CORE_Online_event_url__c, status, Description, CORE_Start_Date_and_hour__c, CORE_Livestorm_Session_Id__c, CORE_Livestorm_Event_Id__c
        FROM Campaign where Id in :this.campaignIds ALL ROWS]);
    }

    public void execute(Database.BatchableContext bc, List<Campaign> campaigns){
        System.debug('CORE_Livestorm_DeleteCampaign.execute() : START');

        for(Campaign campaign : campaigns){
            System.debug('Delete session in livestorm for : ' + campaign.CORE_Campaign_Full_Name__c);
            if (!deleteSession(campaign)){
                System.debug('Session deleted');
                if(!deleteEvent(campaign)){
                    System.debug('Event deleted');
                } else {
                    System.debug('Event cannot be deleted');
                }
            } else{
                System.debug('Cannot delete event because session was not created');
            }
        }
        System.debug('CORE_Livestorm_DeleteCampaign.execute() : END');
    }

    public void finish(Database.BatchableContext bc){
        System.debug('CORE_Livestorm_DeleteCampaign END.');
    }

    public Boolean deleteEvent(Campaign campaign){
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        
        // Remove constant parameter
        request.setMethod('DELETE');
        request.setEndpoint('https://api.livestorm.co/v1/events/' + campaign.CORE_Livestorm_Event_Id__c);
        request.setHeader('Accept', 'application/vnd.api+json');
        request.setHeader('Authorization', 'eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJhcGkubGl2ZXN0b3JtLmNvIiwianRpIjoiYTY0YjhhZWItYmVkYi00NjA0LTgyODAtZmRhOWI2MjMyZjc1IiwiaWF0IjoxNjM3MDczODgxLCJvcmciOiJkNTg1NzA5NS0xMWM5LTQwZjYtOWY2Ni04YmZlNDJiMGM2MzkifQ.1K-13dhjoBTSE7mx6A_O1WBZYNafE5MaAwXQT8s4yW8');
        
        
        Long dt1 = DateTime.now().getTime();
        DateTime dt1Date = DateTime.now();

        HttpResponse response = http.send(request);

        long dt2 = DateTime.now().getTime(); 
        DateTime dt2Date = DateTime.now();
        System.debug('http delete event execution time = ' + (dt2-dt1) + ' ms');

        if(response.getStatusCode() != 204) {
            System.debug('Event :The status code returned was not expected: ' + response.getStatusCode() + ' ' + response.getStatus());
            return true;
        } else {
            return false;
        }
    }

    public Boolean deleteSession(Campaign campaign){
        System.debug('Start deleting Session : ' + campaign.CORE_Livestorm_Session_Id__c);

        Http http = new Http();
        HttpRequest request = new HttpRequest();
        
        // Remove constant parameter
        request.setMethod('DELETE');
        request.setEndpoint('https://api.livestorm.co/v1/sessions/' + campaign.CORE_Livestorm_Session_Id__c);
        request.setHeader('Accept', 'application/vnd.api+json');
        request.setHeader('Authorization', 'eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJhcGkubGl2ZXN0b3JtLmNvIiwianRpIjoiYTY0YjhhZWItYmVkYi00NjA0LTgyODAtZmRhOWI2MjMyZjc1IiwiaWF0IjoxNjM3MDczODgxLCJvcmciOiJkNTg1NzA5NS0xMWM5LTQwZjYtOWY2Ni04YmZlNDJiMGM2MzkifQ.1K-13dhjoBTSE7mx6A_O1WBZYNafE5MaAwXQT8s4yW8');
        
        Long dt1 = DateTime.now().getTime();
        DateTime dt1Date = DateTime.now();

        HttpResponse response = http.send(request);

        long dt2 = DateTime.now().getTime(); 
        DateTime dt2Date = DateTime.now();
        System.debug('http delete session execution time = ' + (dt2-dt1) + ' ms');

        if(response.getStatusCode() != 204) {
            System.debug('Session : The status code returned was not expected: ' + response.getStatusCode() + ' ' + response.getStatus());
            return true;
        } else {
            return false;
        }
    }
}