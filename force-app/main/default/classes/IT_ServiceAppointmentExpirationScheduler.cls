public without sharing class IT_ServiceAppointmentExpirationScheduler implements Schedulable {

    public void execute(SchedulableContext sc) {
        IT_ServiceAppointmentExpirationChecker batch = new IT_ServiceAppointmentExpirationChecker(); //ur batch class
        Database.executebatch(batch);
    }
}