public without sharing class CORE_CampaignUpdateStatusSchedulable implements Schedulable {
    public void execute(SchedulableContext sC) {
        CORE_CampaignUpdateStatusBatchable campaignUpdateStatusBatchable = new CORE_CampaignUpdateStatusBatchable();
        database.executebatch(campaignUpdateStatusBatchable);
     }
}