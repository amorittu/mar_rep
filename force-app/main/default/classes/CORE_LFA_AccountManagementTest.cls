@IsTest
public with sharing class CORE_LFA_AccountManagementTest {
    private static final String mobile1 = '+3320000001';
    private static final String mobile2 = '+3320000002';
    private static final String phone1 = '+3320000001';
    private static final String phone2 = '+3320000002';
    private static final String uniqueKey1 = 'test1';
    private static final String uniqueKey2 = 'test2';
    private static final String email1 = uniqueKey1 + '@test.com';
    private static final String email2 = uniqueKey2 + '@test.com';

    @TestSetup
    static void makeData(){
        CORE_CatalogHierarchyModel catalog = new CORE_DataFaker_CatalogHierarchy('test').catalogHierarchy;
        CORE_TriggerUtils.setBypassTrigger();
        CORE_LeadAcquisitionBuffer__c buffer1 = CORE_DataFaker_LeadAcquisitionBuffer.getBuffer(uniqueKey1, mobile1, phone1, email1, catalog);
        CORE_LeadAcquisitionBuffer__c buffer2 = CORE_DataFaker_LeadAcquisitionBuffer.getBuffer(uniqueKey2, mobile2, phone2, email2, catalog);
        CORE_TriggerUtils.setBypassTrigger();
    }

    @IsTest
    public static void searchAccountsByEmails_Should_Return_set_of_accound_find_using_emails(){
        Account account1 = CORE_DataFaker_Account.getStudentAccount('test1');
        Account account2 = CORE_DataFaker_Account.getStudentAccount('test2');
        Account account3 = CORE_DataFaker_Account.getStudentAccountWithoutInsert('test3');

        String emailTest1 = 'email1@test.com';
        String emailTest2 = 'email2@test.com';
        String emailTest3 = 'email3@test.com';
        String emailTest4 = 'email4@test.com';
        String emailTest5 = 'notFound@test.com';

        account3.PersonEmail = emailTest1;
        account3.CORE_Email_2__pc = emailTest2;
        account3.CORE_Email_3__pc = emailTest3;
        account3.CORE_Student_Email__pc = emailTest4;
        Insert account3;

        Set<String> emails = new Set<String> {emailTest5};
        Set<Account> results = new Set<Account> ();

        test.startTest();

        results = CORE_LFA_AccountManagement.searchAccountsByEmails(emails);
        System.assertEquals(0, results.size());

        emails = new Set<String> {account1.PersonEmail};
        results = CORE_LFA_AccountManagement.searchAccountsByEmails(emails);
        System.assertEquals(1, results.size());

        emails = new Set<String> {account1.PersonEmail, account2.PersonEmail};
        results = CORE_LFA_AccountManagement.searchAccountsByEmails(emails);
        System.assertEquals(2, results.size());

        emails = new Set<String> {account1.PersonEmail, account2.PersonEmail, emailTest1};
        results = CORE_LFA_AccountManagement.searchAccountsByEmails(emails);
        System.assertEquals(3, results.size());

        emails = new Set<String> {account1.PersonEmail, account2.PersonEmail, emailTest2};
        results = CORE_LFA_AccountManagement.searchAccountsByEmails(emails);
        System.assertEquals(3, results.size());

        emails = new Set<String> {account1.PersonEmail, account2.PersonEmail, emailTest3};
        results = CORE_LFA_AccountManagement.searchAccountsByEmails(emails);
        System.assertEquals(3, results.size());

        emails = new Set<String> {account1.PersonEmail, account2.PersonEmail, emailTest4};
        results = CORE_LFA_AccountManagement.searchAccountsByEmails(emails);
        System.assertEquals(3, results.size());

        emails = new Set<String> {account1.PersonEmail, account2.PersonEmail, emailTest1, emailTest2, emailTest3, emailTest4, emailTest5};
        results = CORE_LFA_AccountManagement.searchAccountsByEmails(emails);
        System.assertEquals(3, results.size());
        test.stopTest();
    }

    @IsTest
    public static void searchAccountsByPhones_Should_Return_set_of_accound_find_using_phones(){
        Account account1 = CORE_DataFaker_Account.getStudentAccountWithoutInsert('test1');
        Account account2 = CORE_DataFaker_Account.getStudentAccountWithoutInsert('test2');
        Account account3 = CORE_DataFaker_Account.getStudentAccountWithoutInsert('test3');

        String phoneTest1 = '+33200000001';
        String phoneTest2 = '+33200000002';
        String mobileTest1 = '+33600000001';
        String mobileTest2 = '+33600000002';
        String mobileTest3 = '+33600000003';
        String unknown = '+33684849845';

        account1.Phone = phoneTest1;
        account2.Phone = phoneTest2;
        account3.Phone = null;
        account3.PersonMobilePhone = mobileTest1;
        account3.CORE_Mobile_2__pc = mobileTest2;
        account3.CORE_Mobile_3__pc = mobileTest3;
        Insert new List<Account> {account1,account2,account3};

        Set<String> phones = new Set<String> {};
        Set<Account> results = new Set<Account> ();

        test.startTest();

        results = CORE_LFA_AccountManagement.searchAccountsByPhones(phones);
        System.assertEquals(0, results.size());

        phones = new Set<String> {unknown};
        results = CORE_LFA_AccountManagement.searchAccountsByPhones(phones);
        System.assertEquals(0, results.size());

        phones = new Set<String> {phoneTest1};
        results = CORE_LFA_AccountManagement.searchAccountsByPhones(phones);
        System.assertEquals(1, results.size());

        phones = new Set<String> {phoneTest1,phoneTest2};
        results = CORE_LFA_AccountManagement.searchAccountsByPhones(phones);
        System.assertEquals(2, results.size());

        phones = new Set<String> {phoneTest1,phoneTest2,mobileTest1};
        results = CORE_LFA_AccountManagement.searchAccountsByPhones(phones);
        System.assertEquals(3, results.size());

        phones = new Set<String> {phoneTest1,phoneTest2,mobileTest2};
        results = CORE_LFA_AccountManagement.searchAccountsByPhones(phones);
        System.assertEquals(3, results.size());

        phones = new Set<String> {phoneTest1,phoneTest2,mobileTest3};
        results = CORE_LFA_AccountManagement.searchAccountsByPhones(phones);
        System.assertEquals(3, results.size());

        phones = new Set<String> {phoneTest1,phoneTest2,mobileTest1,mobileTest2,mobileTest3};
        results = CORE_LFA_AccountManagement.searchAccountsByPhones(phones);
        System.assertEquals(3, results.size());
        test.stopTest();
    }

    @IsTest
    public static void sortAccountsByEmails_should_return_map_of_accounts_organized_by_emails(){
        Account account1 = CORE_DataFaker_Account.getStudentAccountWithoutInsert('test1');
        Account account2 = CORE_DataFaker_Account.getStudentAccountWithoutInsert('test2');
        Account account3 = CORE_DataFaker_Account.getStudentAccountWithoutInsert('test3');
        Account account4 = CORE_DataFaker_Account.getStudentAccountWithoutInsert('test4');
        Account account5 = CORE_DataFaker_Account.getStudentAccountWithoutInsert('test5');

        List<String> emailLst1 = new List<String>{'email1_1@test.com', 'email1_2@test.com', 'email1_3@test.com', 'email1_4@test.com'};
        List<String> emailLst2 = new List<String>{'email2_1@test.com', 'email2_2@test.com', 'email2_3@test.com', 'email2_4@test.com'};
        account1.PersonEmail = emailLst1.get(0);
        account1.CORE_Email_2__pc = emailLst1.get(1);
        account1.CORE_Email_3__pc = emailLst1.get(2);
        account1.CORE_Student_Email__pc = emailLst1.get(3);

        account2.PersonEmail = emailLst2.get(0);
        account2.CORE_Email_2__pc = emailLst2.get(1);
        account2.CORE_Email_3__pc = emailLst2.get(2);
        account2.CORE_Student_Email__pc = emailLst2.get(3);

        account3.PersonEmail = emailLst1.get(0);
        account3.CORE_Email_2__pc = emailLst1.get(1);
        account3.CORE_Email_3__pc = emailLst2.get(2);
        account3.CORE_Student_Email__pc = emailLst2.get(3);

        account4.PersonEmail = emailLst1.get(0);
        account4.CORE_Email_2__pc = emailLst2.get(1);
        account4.CORE_Email_3__pc = null;
        account4.CORE_Student_Email__pc = null;

        account5.PersonEmail = null;
        account5.CORE_Email_2__pc = null;
        account5.CORE_Email_3__pc = null;
        account5.CORE_Student_Email__pc = null;
        account5.Phone = '+33205050505';

        Set<Account> accounts = new Set<Account> {account1, account2, account3, account4, account5};
        insert new List<Account> (accounts);

        test.startTest();
        Map<String,Set<Account>> results = CORE_LFA_AccountManagement.sortAccountsByEmails(accounts);
        test.stopTest();

        System.assertEquals(emailLst1.size() + emailLst2.size(), results.size());
        System.assertEquals(3, results.get(emailLst1.get(0)).size());
        System.assertEquals(2, results.get(emailLst1.get(1)).size());
        System.assertEquals(1, results.get(emailLst1.get(2)).size());
        System.assertEquals(1, results.get(emailLst1.get(3)).size());

        System.assertEquals(1, results.get(emailLst2.get(0)).size());
        System.assertEquals(2, results.get(emailLst2.get(1)).size());
        System.assertEquals(2, results.get(emailLst2.get(2)).size());
        System.assertEquals(2, results.get(emailLst2.get(3)).size());
    }

    @IsTest
    public static void sortAccountsByPhones_should_return_map_of_accounts_organized_by_phones(){
        Account account1 = CORE_DataFaker_Account.getStudentAccountWithoutInsert('test1');
        Account account2 = CORE_DataFaker_Account.getStudentAccountWithoutInsert('test2');
        Account account3 = CORE_DataFaker_Account.getStudentAccountWithoutInsert('test3');
        Account account4 = CORE_DataFaker_Account.getStudentAccountWithoutInsert('test4');
        Account account5 = CORE_DataFaker_Account.getStudentAccountWithoutInsert('test5');

        List<String> phoneLst1 = new List<String>{'+33210000000', '+33610000001', '+33610000002', '+33610000003'};
        List<String> phoneLst2 = new List<String>{'+33220000000', '+33620000001', '+33620000002', '+33620000003'};
        account1.Phone = phoneLst1.get(0);
        account1.PersonMobilePhone = phoneLst1.get(1);
        account1.CORE_Mobile_2__pc = phoneLst1.get(2);
        account1.CORE_Mobile_3__pc = phoneLst1.get(3);

        account2.Phone = phoneLst2.get(0);
        account2.PersonMobilePhone = phoneLst2.get(1);
        account2.CORE_Mobile_2__pc = phoneLst2.get(2);
        account2.CORE_Mobile_3__pc = phoneLst2.get(3);

        account3.Phone = phoneLst1.get(0);
        account3.PersonMobilePhone = phoneLst1.get(1);
        account3.CORE_Mobile_2__pc = phoneLst2.get(2);
        account3.CORE_Mobile_3__pc = phoneLst2.get(3);

        account4.Phone = phoneLst1.get(0);
        account4.PersonMobilePhone = phoneLst2.get(1);
        account4.CORE_Mobile_2__pc = null;
        account4.CORE_Mobile_3__pc = null;

        account5.Phone = null;
        account5.PersonMobilePhone = null;
        account5.CORE_Mobile_2__pc = null;
        account5.CORE_Mobile_3__pc = null;
        account5.PersonEmail = 'test@test.com';

        Set<Account> accounts = new Set<Account> {account1, account2, account3, account4, account5};
        insert new List<Account> (accounts);

        test.startTest();
        Map<String,Set<Account>> results = CORE_LFA_AccountManagement.sortAccountsByPhones(accounts);
        test.stopTest();

        System.assertEquals(phoneLst1.size() + phoneLst2.size(), results.size());
        System.assertEquals(3, results.get(phoneLst1.get(0)).size());
        System.assertEquals(2, results.get(phoneLst1.get(1)).size());
        System.assertEquals(1, results.get(phoneLst1.get(2)).size());
        System.assertEquals(1, results.get(phoneLst1.get(3)).size());

        System.assertEquals(1, results.get(phoneLst2.get(0)).size());
        System.assertEquals(2, results.get(phoneLst2.get(1)).size());
        System.assertEquals(2, results.get(phoneLst2.get(2)).size());
        System.assertEquals(2, results.get(phoneLst2.get(3)).size());
    }

    @IsTest
    public static void getExestingAccounts_Should_Return_Map_of_related_account_by_buffer_records(){
        List<CORE_LeadAcquisitionBuffer__c> buffers = [SELECT Id, CORE_PA_Email__c, CORE_PA_Phone__c, CORE_PA_Mobile__c FROM CORE_LeadAcquisitionBuffer__c ORDER BY CORE_PA_Email__c ASC];
        CORE_LeadAcquisitionBuffer__c buffer1 = buffers.get(0);
        CORE_LeadAcquisitionBuffer__c buffer2 = buffers.get(1);

        Account account1 = CORE_DataFaker_Account.getStudentAccountWithoutInsert('test1');
        Account account2 = CORE_DataFaker_Account.getStudentAccountWithoutInsert('test2');
        Account account3 = CORE_DataFaker_Account.getStudentAccountWithoutInsert('test3');

        List<Account> accounts = new List<Account> {account1, account2, account3};
        insert accounts;

        
        System.debug('accounts = '+ [SELECT ID, Phone, PersonEmail FROM Account]);
        Map<Id,Set<Account>> result = new Map<Id,Set<Account>>();

        Test.startTest();
        
        // No Account founds
        result = CORE_LFA_AccountManagement.getExestingAccounts(buffers);
        System.assertEquals(buffers.size(), result.size());
        System.debug('result = '+ result);
        System.assertEquals(0, result.get(buffer1.Id).size());
        System.assertEquals(0, result.get(buffer2.Id).size());

        account1.PersonEmail = email1;
        account2.CORE_Email_2__pc = email1;
        update accounts;

        //results found for buffer1
        result = CORE_LFA_AccountManagement.getExestingAccounts(buffers);
        System.assertEquals(buffers.size(), result.size());
        System.assertEquals(2, result.get(buffer1.Id).size());
        System.assertEquals(0, result.get(buffer2.Id).size());

        account2.PersonMobilePhone = mobile2;
        account2.Phone = phone2;
        account2.CORE_Email_3__pc = email2;
        update accounts;

        //results found for buffer1 and 2
        result = CORE_LFA_AccountManagement.getExestingAccounts(buffers);
        System.assertEquals(buffers.size(), result.size());
        System.assertEquals(2, result.get(buffer1.Id).size());
        System.assertEquals(1, result.get(buffer2.Id).size());

        account1.PersonEmail = 'notMatch@test.fr';
        account2.CORE_Email_2__pc = 'notMatch@test.fr';

        update accounts;

        //results found for buffer2
        result = CORE_LFA_AccountManagement.getExestingAccounts(buffers);
        System.assertEquals(buffers.size(), result.size());
        System.assertEquals(0, result.get(buffer1.Id).size());
        System.assertEquals(1, result.get(buffer2.Id).size());

        Test.stopTest();
    }

    @IsTest 
    public static void getAccountWrapperFromBufferRecord_Should_return_a_person_wrapper_filled_by_buffer_values(){
        String uniqueKey = 'test';
        String mobile = '+33600000001';
        String phone = '+33200000001';
        String email = 'test@test.com';

        CORE_LeadAcquisitionBuffer__c  bufferRecord = CORE_DataFaker_LeadAcquisitionBuffer.getBufferWithPersonAccountDatas(uniqueKey, mobile, phone, email, null);
        Test.startTest();
        CORE_LeadFormAcquisitionWrapper.PersonAccountWrapper result = CORE_LFA_AccountManagement.getAccountWrapperFromBufferRecord(bufferRecord);
        Test.stopTest();

        System.assertEquals(bufferRecord.CORE_PA_Salutation__c, result.salutation);
        System.assertEquals(String.valueOf(bufferRecord.CORE_PA_CreatedDate__c.getTime()), result.creationDate);
		System.assertEquals(bufferRecord.CORE_PA_LastName__c, result.lastName);
		System.assertEquals(bufferRecord.CORE_PA_FirstName__c, result.firstName);
		System.assertEquals(bufferRecord.CORE_PA_Phone__c, result.phone);
		System.assertEquals(bufferRecord.CORE_PA_Mobile__c, result.mobilePhone);
		System.assertEquals(bufferRecord.CORE_PA_Email__c, result.emailAddress);
		System.assertEquals(bufferRecord.CORE_PA_MailingStreet__c, result.street);
		System.assertEquals(bufferRecord.CORE_PA_MailingPostalCode__c, result.postalCode);
		System.assertEquals(bufferRecord.CORE_PA_MailingCity__c, result.city);
		System.assertEquals(bufferRecord.CORE_PA_MailingState__c, result.stateCode);
		System.assertEquals(bufferRecord.CORE_PA_MailingCountryCode__c, result.countryCode);
		System.assertEquals(bufferRecord.CORE_PA_LanguageWebsite__c, result.languageWebsite);
		System.assertEquals(bufferRecord.CORE_PA_GAUserId__c, result.gaUserId);
		System.assertEquals(bufferRecord.CORE_PA_updateIfNotNull__c, result.updateIfNotNull);
        System.assertEquals(bufferRecord.CORE_PA_MainNationality__c, result.mainNationality);

        bufferRecord.CORE_PA_CreatedDate__c = null;
        result = CORE_LFA_AccountManagement.getAccountWrapperFromBufferRecord(bufferRecord);

        System.assertEquals(null, result.creationDate);
    }
    @IsTest 
    public static void getBufferWithConsentDatas_Should_return_a_consent_wrapper_filled_by_buffer_values(){
        CORE_CatalogHierarchyModel catalog = new CORE_DataFaker_CatalogHierarchy('test1').catalogHierarchy;
        
        CORE_LeadAcquisitionBuffer__c bufferRecord1 = CORE_DataFaker_LeadAcquisitionBuffer.getBufferWithConsentDatas(catalog, 1,null);
        CORE_LeadAcquisitionBuffer__c bufferRecord2 = CORE_DataFaker_LeadAcquisitionBuffer.getBufferWithConsentDatas(catalog, 2,null);
        CORE_LeadAcquisitionBuffer__c bufferRecord3 = CORE_DataFaker_LeadAcquisitionBuffer.getBufferWithConsentDatas(catalog, 3,null);
        CORE_LeadAcquisitionBuffer__c bufferRecord4 = CORE_DataFaker_LeadAcquisitionBuffer.getBufferWithConsentDatas(catalog, 4,null);
        CORE_LeadAcquisitionBuffer__c bufferRecord5 = CORE_DataFaker_LeadAcquisitionBuffer.getBufferWithConsentDatas(catalog, 5,null);

        Test.startTest();
        CORE_LeadFormAcquisitionWrapper.ConsentWrapper result1 = CORE_LFA_AccountManagement.getConsentWrapperFromBufferRecord(bufferRecord1,1);
        CORE_LeadFormAcquisitionWrapper.ConsentWrapper result2 = CORE_LFA_AccountManagement.getConsentWrapperFromBufferRecord(bufferRecord2,2);
        CORE_LeadFormAcquisitionWrapper.ConsentWrapper result3 = CORE_LFA_AccountManagement.getConsentWrapperFromBufferRecord(bufferRecord3,3);
        CORE_LeadFormAcquisitionWrapper.ConsentWrapper result4 = CORE_LFA_AccountManagement.getConsentWrapperFromBufferRecord(bufferRecord4,4);
        CORE_LeadFormAcquisitionWrapper.ConsentWrapper result5 = CORE_LFA_AccountManagement.getConsentWrapperFromBufferRecord(bufferRecord5,5);
        

        System.assertEquals(bufferRecord1.CORE_CO1_ConsentType__c, result1.consentType);
        System.assertEquals(bufferRecord1.CORE_CO1_ConsentChannel__c, result1.channel);
        System.assertEquals(bufferRecord1.CORE_CO1_ConsentDescription__c, result1.description);
        System.assertEquals(bufferRecord1.CORE_CO1_OptIn__c, result1.optIn);
        System.assertEquals(String.valueOf(bufferRecord1.CORE_CO1_OptInDate__c.getTime()), result1.optInDate);
        System.assertEquals(bufferRecord1.CORE_CO1_OptOut__c, result1.optOut);
        System.assertEquals(String.valueOf(bufferRecord1.CORE_CO1_OptOutDate__c.getTime()), result1.optOutDate);
        System.assertEquals(bufferRecord1.CORE_CO1_Division__c, result1.division);
        System.assertEquals(bufferRecord1.CORE_CO1_BusinessUnit__c, result1.businessUnit);

        System.assertEquals(bufferRecord2.CORE_CO2_ConsentType__c, result2.consentType);
        System.assertEquals(bufferRecord2.CORE_CO2_ConsentChannel__c, result2.channel);
        System.assertEquals(bufferRecord2.CORE_CO2_ConsentDescription__c, result2.description);
        System.assertEquals(bufferRecord2.CORE_CO2_OptIn__c, result2.optIn);
        System.assertEquals(String.valueOf(bufferRecord2.CORE_CO2_OptInDate__c.getTime()), result2.optInDate);
        System.assertEquals(bufferRecord2.CORE_CO2_OptOut__c, result2.optOut);
        System.assertEquals(String.valueOf(bufferRecord2.CORE_CO2_OptOutDate__c.getTime()), result2.optOutDate);
        System.assertEquals(bufferRecord2.CORE_CO2_Division__c, result2.division);
        System.assertEquals(bufferRecord2.CORE_CO2_BusinessUnit__c, result2.businessUnit);

        System.assertEquals(bufferRecord3.CORE_CO3_ConsentType__c, result3.consentType);
        System.assertEquals(bufferRecord3.CORE_CO3_ConsentChannel__c, result3.channel);
        System.assertEquals(bufferRecord3.CORE_CO3_ConsentDescription__c, result3.description);
        System.assertEquals(bufferRecord3.CORE_CO3_OptIn__c, result3.optIn);
        System.assertEquals(String.valueOf(bufferRecord3.CORE_CO3_OptInDate__c.getTime()), result3.optInDate);
        System.assertEquals(bufferRecord3.CORE_CO3_OptOut__c, result3.optOut);
        System.assertEquals(String.valueOf(bufferRecord3.CORE_CO3_OptOutDate__c.getTime()), result3.optOutDate);
        System.assertEquals(bufferRecord3.CORE_CO3_Division__c, result3.division);
        System.assertEquals(bufferRecord3.CORE_CO3_BusinessUnit__c, result3.businessUnit);

        System.assertEquals(bufferRecord4.CORE_CO4_ConsentType__c, result4.consentType);
        System.assertEquals(bufferRecord4.CORE_CO4_ConsentChannel__c, result4.channel);
        System.assertEquals(bufferRecord4.CORE_CO4_ConsentDescription__c, result4.description);
        System.assertEquals(bufferRecord4.CORE_CO4_OptIn__c, result4.optIn);
        System.assertEquals(String.valueOf(bufferRecord4.CORE_CO4_OptInDate__c.getTime()), result4.optInDate);
        System.assertEquals(bufferRecord4.CORE_CO4_OptOut__c, result4.optOut);
        System.assertEquals(String.valueOf(bufferRecord4.CORE_CO4_OptOutDate__c.getTime()), result4.optOutDate);
        System.assertEquals(bufferRecord4.CORE_CO4_Division__c, result4.division);
        System.assertEquals(bufferRecord4.CORE_CO4_BusinessUnit__c, result4.businessUnit);

        System.assertEquals(bufferRecord5.CORE_CO5_ConsentType__c, result5.consentType);
        System.assertEquals(bufferRecord5.CORE_CO5_ConsentChannel__c, result5.channel);
        System.assertEquals(bufferRecord5.CORE_CO5_ConsentDescription__c, result5.description);
        System.assertEquals(bufferRecord5.CORE_CO5_OptIn__c, result5.optIn);
        System.assertEquals(String.valueOf(bufferRecord5.CORE_CO5_OptInDate__c.getTime()), result5.optInDate);
        System.assertEquals(bufferRecord5.CORE_CO5_OptOut__c, result5.optOut);
        System.assertEquals(String.valueOf(bufferRecord5.CORE_CO5_OptOutDate__c.getTime()), result5.optOutDate);
        System.assertEquals(bufferRecord5.CORE_CO5_Division__c, result5.division);
        System.assertEquals(bufferRecord5.CORE_CO5_BusinessUnit__c, result5.businessUnit);

        bufferRecord1.CORE_CO1_OptInDate__c = null;
        bufferRecord1.CORE_CO1_OptOutDate__c = null;

        bufferRecord2.CORE_CO2_OptInDate__c = null;
        bufferRecord2.CORE_CO2_OptOutDate__c = null;

        bufferRecord3.CORE_CO3_OptInDate__c = null;
        bufferRecord3.CORE_CO3_OptOutDate__c = null;

        bufferRecord4.CORE_CO4_OptInDate__c = null;
        bufferRecord4.CORE_CO4_OptOutDate__c = null;

        bufferRecord5.CORE_CO5_OptInDate__c = null;
        bufferRecord5.CORE_CO5_OptOutDate__c = null;

        result1 = CORE_LFA_AccountManagement.getConsentWrapperFromBufferRecord(bufferRecord1,1);
        result2 = CORE_LFA_AccountManagement.getConsentWrapperFromBufferRecord(bufferRecord2,2);
        result3 = CORE_LFA_AccountManagement.getConsentWrapperFromBufferRecord(bufferRecord3,3);
        result4 = CORE_LFA_AccountManagement.getConsentWrapperFromBufferRecord(bufferRecord4,4);
        result5 = CORE_LFA_AccountManagement.getConsentWrapperFromBufferRecord(bufferRecord5,5);

        System.assertEquals(null, result1.optInDate);
        System.assertEquals(null, result1.optOutDate);
        System.assertEquals(null, result2.optInDate);
        System.assertEquals(null, result2.optOutDate);
        System.assertEquals(null, result3.optInDate);
        System.assertEquals(null, result3.optOutDate);
        System.assertEquals(null, result4.optInDate);
        System.assertEquals(null, result4.optOutDate);
        System.assertEquals(null, result5.optInDate);
        System.assertEquals(null, result5.optOutDate);

        Test.stopTest();
    }

    @IsTest 
    public static void isExistingConsentValue_Should_return_true_if_consent_have_one_field_filled(){
        CORE_CatalogHierarchyModel catalog = new CORE_DataFaker_CatalogHierarchy('test1').catalogHierarchy;

        CORE_LeadAcquisitionBuffer__c bufferRecord1 = new CORE_LeadAcquisitionBuffer__c(CORE_CO1_ConsentType__c = CORE_DataFaker_LeadAcquisitionBuffer.CONSENT_TYPE);
        CORE_LeadAcquisitionBuffer__c bufferRecord2 = new CORE_LeadAcquisitionBuffer__c(CORE_CO2_ConsentChannel__c = CORE_DataFaker_LeadAcquisitionBuffer.CONSENT_CHANNEL);
        CORE_LeadAcquisitionBuffer__c bufferRecord3 = new CORE_LeadAcquisitionBuffer__c(CORE_CO3_ConsentDescription__c = 'desc');
        CORE_LeadAcquisitionBuffer__c bufferRecord4 = new CORE_LeadAcquisitionBuffer__c(CORE_CO4_Division__c = catalog.division.Id);
        CORE_LeadAcquisitionBuffer__c bufferRecord5 = new CORE_LeadAcquisitionBuffer__c(CORE_CO5_BusinessUnit__c = catalog.businessUnit.Id);

        Test.startTest();
        Boolean result1 = CORE_LFA_AccountManagement.isExistingConsentValue(bufferRecord1, 1);
        Boolean result2 = CORE_LFA_AccountManagement.isExistingConsentValue(bufferRecord2, 2);
        Boolean result3 = CORE_LFA_AccountManagement.isExistingConsentValue(bufferRecord3, 3);
        Boolean result4 = CORE_LFA_AccountManagement.isExistingConsentValue(bufferRecord4, 4);
        Boolean result5 = CORE_LFA_AccountManagement.isExistingConsentValue(bufferRecord5, 5);
        Test.stopTest();

        System.assertEquals(true, result1);
        System.assertEquals(true, result2);
        System.assertEquals(true, result3);
        System.assertEquals(true, result4);
        System.assertEquals(true, result5);
    }

    @IsTest 
    public static void isExistingConsentValue_Should_return_false_if_consent_have_one_required_field_filled(){
        CORE_LeadAcquisitionBuffer__c bufferRecord1 = new CORE_LeadAcquisitionBuffer__c();
        CORE_LeadAcquisitionBuffer__c bufferRecord2 = new CORE_LeadAcquisitionBuffer__c(CORE_CO1_ConsentChannel__c = CORE_DataFaker_LeadAcquisitionBuffer.CONSENT_CHANNEL);
        CORE_LeadAcquisitionBuffer__c bufferRecord3 = new CORE_LeadAcquisitionBuffer__c(CORE_CO4_ConsentDescription__c = 'desc');
        CORE_LeadAcquisitionBuffer__c bufferRecord4 = new CORE_LeadAcquisitionBuffer__c(CORE_CO1_ConsentType__c = CORE_DataFaker_LeadAcquisitionBuffer.CONSENT_TYPE);
        CORE_LeadAcquisitionBuffer__c bufferRecord5 = new CORE_LeadAcquisitionBuffer__c(CORE_CO5_OptIn__c = true);

        Test.startTest();
        Boolean result1 = CORE_LFA_AccountManagement.isExistingConsentValue(bufferRecord1, 1);
        Boolean result2 = CORE_LFA_AccountManagement.isExistingConsentValue(bufferRecord2, 2);
        Boolean result3 = CORE_LFA_AccountManagement.isExistingConsentValue(bufferRecord3, 3);
        Boolean result4 = CORE_LFA_AccountManagement.isExistingConsentValue(bufferRecord4, 4);
        Boolean result5 = CORE_LFA_AccountManagement.isExistingConsentValue(bufferRecord5, 5);
        Test.stopTest();

        System.assertEquals(false, result1);
        System.assertEquals(false, result2);
        System.assertEquals(false, result3);
        System.assertEquals(false, result4);
        System.assertEquals(false, result5);
    }
}