@IsTest
public class CORE_OptOutSchedulableUpdateTest {
    @IsTest
    public static void Opt_out_Schedulable_update_Should_Be_Launch() {
        test.starttest();
        CORE_OptOutSchedulableUpdate myClass = new CORE_OptOutSchedulableUpdate();   
        String chron = '0 0 23 * * ?';        
        system.schedule('Test Sched', chron, myClass);
        test.stopTest();

        List<AsyncApexJob> jobsScheduled = [select Id, ApexClassID, ApexClass.Name, Status, JobType from AsyncApexJob where JobType = 'ScheduledApex'];
        System.assertEquals(1, jobsScheduled.size(), 'expecting one scheduled job');
        System.assertEquals('CORE_OptOutSchedulableUpdate', jobsScheduled[0].ApexClass.Name, 'expecting specific scheduled job');

        List<AsyncApexJob> jobsApexBatch = [select Id, ApexClassID, ApexClass.Name, Status, JobType from AsyncApexJob where JobType = 'BatchApex'];
        System.assertEquals(1, jobsApexBatch.size(), 'expecting one apex batch job');
        System.assertEquals('CORE_OptOutBatchableUpdate', jobsApexBatch[0].ApexClass.Name, 'expecting specific batch job');
    }
}