global class CORE_OppLineItemsForFlows {
    @InvocableMethod
    global static List<Result> getOppLineItems(List<OppLineItemsForFlowsParameters> params){
        List<Result> resultWrapper = new List<Result>();

        OppLineItemsForFlowsParameters param = params.get(0);

        Id priceBookId = param.priceBookId;
        //List<String> productCodes = param.productCodes;
        List<OpportunityLineItem> opportunityLineItems = param.opportunityLineItems;

        System.debug('opportunityLineItems = ' + opportunityLineItems);
        Set<String> productCodes = new Set<String>();
        Map<String,List<opportunityLineItem>> oppLineItemsByProductCode = new Map<String,List<opportunityLineItem>>();

        for(opportunityLineItem opportunityLineItem : opportunityLineItems){
            String currentProductCode = opportunityLineItem.ProductCode;
            if(!String.isEmpty(currentProductCode)){
                productCodes.Add(currentProductCode);
                if(oppLineItemsByProductCode.get(currentProductCode) == null){
                    oppLineItemsByProductCode.put(currentProductCode, new List<opportunityLineItem>());
                }
                //List<opportunityLineItem> lineItems = oppLineItemsByProductCode.get(currentProductCode);
                oppLineItemsByProductCode.get(currentProductCode).add(opportunityLineItem);
            }
        }
        System.debug('productCodes = '+ productCodes);
        System.debug('oppLineItemsByProductCode = '+ oppLineItemsByProductCode);

        if(productCodes.isEmpty()){
            return resultWrapper;
        }
        
        List<PricebookEntry> pricebookEntries = [Select Id,Pricebook2Id,Product2Id, ProductCode, UnitPrice, IsActive from PricebookEntry
            where Pricebook2Id = :priceBookId
            and ProductCode IN :productCodes
            and IsActive = true
            and Product2.IsActive = true];

        System.debug('pricebookEntries = '+ pricebookEntries);

        List<opportunityLineItem> lineItemsToUpdate = new List<opportunityLineItem>();

        for(PricebookEntry pricebookEntry : pricebookEntries){
            String currentProductCode = pricebookEntry.ProductCode;
            List<opportunityLineItem> lineItems = oppLineItemsByProductCode.get(currentProductCode);
            
            List<opportunityLineItem> lineItemsToUpdateTmp = getLinesItemsWithPricebookEntriesValues(lineItems,pricebookEntry);
            if(!lineItemsToUpdateTmp.isEmpty()){
                lineItemsToUpdate.addAll(lineItemsToUpdateTmp);
            }
        }
        System.debug('lineItemsToUpdate = '+ lineItemsToUpdate);

        if(!lineItemsToUpdate.isEmpty()){
            for(OpportunityLineItem lineItem : lineItemsToUpdate){
                lineItem.Id = null;
            }
            Insert lineItemsToUpdate;
        }

        Result wrapper = new Result();

        wrapper.opportunityLineItems = lineItemsToUpdate;
        resultWrapper.add(wrapper);
        return resultWrapper;
    }

    @TestVisible
    private static List<opportunityLineItem> getLinesItemsWithPricebookEntriesValues(List<OpportunityLineItem> lineItems, PricebookEntry pricebookEntry){
        List<OpportunityLineItem> lineItemsToUpdate = new List<OpportunityLineItem>();

        if(lineItems == null || lineItems.isEmpty()){
            return lineItemsToUpdate;
        }

        for(opportunityLineItem lineItem : lineItems){
            opportunityLineItem lineItemTmp = lineItem.clone(false, true, false, false);
            lineItemTmp.UnitPrice = null;
            lineItemTmp.CORE_External_Id__c = null;
            lineItemTmp.PricebookEntryId = pricebookEntry.Id;

            lineItemsToUpdate.add(lineItemTmp);
        }

        System.debug('lineItemsToUpdate = ' + lineItemsToUpdate);
        System.debug('lineItems = ' + lineItems);


        return lineItemsToUpdate;
    }

    global class Result{
        @InvocableVariable
        global List<opportunityLineItem> opportunityLineItems;
    }
    
    global class OppLineItemsForFlowsParameters{
        @InvocableVariable(required=true)
        global Id priceBookId;

        @InvocableVariable(required=true)
        global List<OpportunityLineItem> opportunityLineItems;
    }
}