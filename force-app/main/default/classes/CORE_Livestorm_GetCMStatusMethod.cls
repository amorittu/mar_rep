global class CORE_Livestorm_GetCMStatusMethod implements Schedulable  {
    global static void execute(SchedulableContext sc){
        List<Campaign> campaigns = [SELECT Id, CORE_Number_of_Member_in_Livestorm__c, CORE_Number_of_Member_Status_Livestorm__c, CORE_End_Date_and_hour__c FROM Campaign WHERE CORE_Online_event__c = TRUE AND CORE_Livestorm_Event_Id__c != NULL];

        Set<Id> campaignIds = new Set<Id>();
        for(Campaign campaign : campaigns){
            if (campaign.CORE_Number_of_Member_Status_Livestorm__c < campaign.CORE_Number_of_Member_in_Livestorm__c && campaign.CORE_End_Date_and_hour__c < date.today()){
                campaignIds.add(campaign.Id);
            }
        }

        if(campaignIds.size() > 0){
            CORE_Livestorm_GetCampaignMemberStatus getCampaignMemberStatusBatchable = new CORE_Livestorm_GetCampaignMemberStatus(campaignIds);
            database.executebatch(getCampaignMemberStatusBatchable, 1);
        }
    }
}