public with sharing class CORE_WS_Constants {

    /* API values */
    //API SUCCESS STATUS
    public static final String SUCCESS_STATUS = 'OK';

    //API ERROR STATUS
    public static final String ERROR_STATUS = 'KO';

    //API ERROR MESSAGES
    public static final String MSG_MISSING_PARAMETERS = 'Missing mandatory parameter : ';
    public static final String MSG_INVALID_FIELDS = 'INVALID FIELD IN REQUEST';

    //PARAM values
    public static final String PARAM_LASTDATE = 'lastDate';
    public static final String PARAM_LAST_ID = 'lastId';
    public static final String PARAM_PA_ID = 'personAccountId';
}