@IsTest
global class CORE_Livestorm_GetCMStatusTest {
    @TestSetup
    public static void makeData(){
        CORE_Livestorm_DataFaker_InitiateTest.InitiateMemberInLivestorm();
    }

    @isTest
    public static void successNotAttemptExecuteTest() {
        Test.setMock(HttpCalloutMock.class, new successNotAttemptMock());
        SchedulableContext ctx;

        Test.startTest();
        CORE_Livestorm_GetCMStatusMethod.execute(ctx);
        Test.stopTest();

        List<CampaignMember> campaignMembers = [SELECT Id, Status FROM CampaignMember];
        List<Campaign> campaigns = [SELECT Id,CORE_Number_of_Member_in_Livestorm__c, CORE_Number_of_Member_Status_Livestorm__c FROM Campaign];
        System.assertEquals(1, campaignMembers.size());
        System.assertEquals(1, campaigns.size());

        CampaignMember campaignMember = campaignMembers.get(0);
        System.assertEquals('No Show', campaignMember.status);

        Campaign campaign = campaigns.get(0);
        System.assertEquals(1, campaign.CORE_Number_of_Member_in_Livestorm__c);
        System.assertEquals(1, campaign.CORE_Number_of_Member_Status_Livestorm__c);
    }

    global class successNotAttemptMock implements HttpCalloutMock {
        global HttpResponse respond(HttpRequest req) {
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');

            if (req.getEndpoint().endsWith('events')){
                res.setBody('{"data": {"id": "2b7de38f-d2d9-4a73-beac-a6ed54d8a38b","attributes": {"registration_link": "https://app.livestorm.co/p/2b7de38f-d2d9-4a73-beac-a6ed54d8a38b"}}}');
                res.setStatusCode(201);
            }
            else if (req.getEndpoint().endsWith('sessions')){
                res.setBody('{"data": {"id": "6b3e4bde-2a75-4ed8-a4af-83a08a68a057","attributes": {"room_link": "https://app.livestorm.co/p/2b7de38f-d2d9-4a73-beac-a6ed54d8a38b/live?s=6b3e4bde-2a75-4ed8-a4af-83a08a68a057"}}}');
                res.setStatusCode(201);
            }
            else if (req.getEndpoint().endsWith('people') && req.getMethod() == 'POST'){
                res.setBody('{"data": {"id": "6b3e4bde-2a75-4ed8-a4af-83a08a68a057","attributes": {"registrant_detail": {"connection_link": "https://app.livestorm.co/p/2b7de38f-d2d9-4a73-beac-a6ed54d8a38b/live?s=6b3e4bde-2a75-4ed8-a4af-83a08a68a057"}}}}');
                res.setStatusCode(201);
            }
            else if (req.getEndpoint().endsWith('people') && req.getMethod() == 'GET'){
                res.setBody('{"data": [{"id": "6b3e4bde-2a75-4ed8-a4af-83a08a68a057","type": "people","attributes": {"registrant_detail": {"attended": false}}}],"meta": {"record_count": 1}}');
                res.setStatusCode(200);
            }

            return res;
        }
    }

    @isTest
    public static void successAttemptExecuteTest() {
        Test.setMock(HttpCalloutMock.class, new successAttemptMock());
        SchedulableContext ctx;

        Test.startTest();
        CORE_Livestorm_GetCMStatusMethod.execute(ctx);
        Test.stopTest();

        List<CampaignMember> campaignMembers = [SELECT Id, Status FROM CampaignMember];
        List<Campaign> campaigns = [SELECT Id,CORE_Number_of_Member_in_Livestorm__c, CORE_Number_of_Member_Status_Livestorm__c FROM Campaign];
        System.assertEquals(1, campaignMembers.size());
        System.assertEquals(1, campaigns.size());

        CampaignMember campaignMember = campaignMembers.get(0);
        System.assertEquals('Attended', campaignMember.status);

        Campaign campaign = campaigns.get(0);
        System.assertEquals(1, campaign.CORE_Number_of_Member_in_Livestorm__c);
        System.assertEquals(1, campaign.CORE_Number_of_Member_Status_Livestorm__c);
    }

    global class successAttemptMock implements HttpCalloutMock {
        global HttpResponse respond(HttpRequest req) {
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');

            if (req.getEndpoint().endsWith('events')){
                res.setBody('{"data": {"id": "2b7de38f-d2d9-4a73-beac-a6ed54d8a38b","attributes": {"registration_link": "https://app.livestorm.co/p/2b7de38f-d2d9-4a73-beac-a6ed54d8a38b"}}}');
                res.setStatusCode(201);
            }
            else if (req.getEndpoint().endsWith('sessions')){
                res.setBody('{"data": {"id": "6b3e4bde-2a75-4ed8-a4af-83a08a68a057","attributes": {"room_link": "https://app.livestorm.co/p/2b7de38f-d2d9-4a73-beac-a6ed54d8a38b/live?s=6b3e4bde-2a75-4ed8-a4af-83a08a68a057"}}}');
                res.setStatusCode(201);
            }
            else if (req.getEndpoint().endsWith('people') && req.getMethod() == 'POST'){
                res.setBody('{"data": {"id": "6b3e4bde-2a75-4ed8-a4af-83a08a68a057","attributes": {"registrant_detail": {"connection_link": "https://app.livestorm.co/p/2b7de38f-d2d9-4a73-beac-a6ed54d8a38b/live?s=6b3e4bde-2a75-4ed8-a4af-83a08a68a057"}}}}');
                res.setStatusCode(201);
            }
            else if (req.getEndpoint().endsWith('people') && req.getMethod() == 'GET'){
                res.setBody('{"data": [{"id": "6b3e4bde-2a75-4ed8-a4af-83a08a68a057","type": "people","attributes": {"registrant_detail": {"attended": true}}}],"meta": {"record_count": 1}}');
                res.setStatusCode(200);
            }

            return res;
        }
    }

    @isTest
    public static void FailExecuteTest() {
        Test.setMock(HttpCalloutMock.class, new failMock());
        SchedulableContext ctx;

        Test.startTest();
        CORE_Livestorm_GetCMStatusMethod.execute(ctx);
        Test.stopTest();

        List<CampaignMember> campaignMembers = [SELECT Id, Status, CampaignId FROM CampaignMember];
        List<Campaign> campaigns = [SELECT Id,CORE_Number_of_Member_in_Livestorm__c, CORE_Number_of_Member_Status_Livestorm__c FROM Campaign];
        System.assertEquals(1, campaignMembers.size());
        CampaignMember campaignMember = campaignMembers.get(0);
        List<CampaignMemberStatus> CampaignMemberStatus = [SELECT Id,Label, IsDefault,CampaignId from CampaignMemberStatus where CampaignId = :campaignMember.CampaignId  order by SortOrder, IsDefault asc];
        System.assert(!CampaignMemberStatus.isEmpty());
        System.debug('CampaignMemberStatus = ' + CampaignMemberStatus);
        //System.assertEquals(CampaignMemberStatus.get(0).Label, campaignMember.status);

        System.assertEquals(1, campaigns.size());

        Campaign campaign = campaigns.get(0);
        System.assertEquals(1, campaign.CORE_Number_of_Member_in_Livestorm__c);
        System.assertEquals(0, campaign.CORE_Number_of_Member_Status_Livestorm__c);
    }

    global class FailMock implements HttpCalloutMock {
        global HttpResponse respond(HttpRequest req) {
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');

            if (req.getEndpoint().endsWith('events')){
                res.setBody('{"data": {"id": "2b7de38f-d2d9-4a73-beac-a6ed54d8a38b","attributes": {"registration_link": "https://app.livestorm.co/p/2b7de38f-d2d9-4a73-beac-a6ed54d8a38b"}}}');
                res.setStatusCode(201);
            }
            else if (req.getEndpoint().endsWith('sessions')){
                res.setBody('{"data": {"id": "6b3e4bde-2a75-4ed8-a4af-83a08a68a057","attributes": {"room_link": "https://app.livestorm.co/p/2b7de38f-d2d9-4a73-beac-a6ed54d8a38b/live?s=6b3e4bde-2a75-4ed8-a4af-83a08a68a057"}}}');
                res.setStatusCode(201);
            }
            else if (req.getEndpoint().endsWith('people') && req.getMethod() == 'POST'){
                res.setBody('{"data": {"id": "6b3e4bde-2a75-4ed8-a4af-83a08a68a057","attributes": {"registrant_detail": {"connection_link": "https://app.livestorm.co/p/2b7de38f-d2d9-4a73-beac-a6ed54d8a38b/live?s=6b3e4bde-2a75-4ed8-a4af-83a08a68a057"}}}}');
                res.setStatusCode(201);
            }
            else if (req.getEndpoint().endsWith('people') && req.getMethod() == 'GET'){
                res.setBody('{"data2": [{"id": "6b3e4bde-2a75-4ed8-a4af-83a08a68a057","type": "people","attributes": {"registrant_detail": {"attended": true}}}],"meta": {"record_count": 1}}');
                res.setStatusCode(200);
            }

            return res;
        }
    }

    @isTest
    public static void failWrongStatusExecuteTest() {
        Test.setMock(HttpCalloutMock.class, new failWrongStatusMock());
        SchedulableContext ctx;

        Test.startTest();
        CORE_Livestorm_GetCMStatusMethod.execute(ctx);
        Test.stopTest();

        List<CampaignMember> campaignMembers = [SELECT Id, Status, CampaignId FROM CampaignMember];
        List<Campaign> campaigns = [SELECT Id,CORE_Number_of_Member_in_Livestorm__c, CORE_Number_of_Member_Status_Livestorm__c FROM Campaign];
        System.assertEquals(1, campaignMembers.size());
        CampaignMember campaignMember = campaignMembers.get(0);

        List<CampaignMemberStatus> CampaignMemberStatus = [SELECT Id,Label, IsDefault,CampaignId from CampaignMemberStatus where CampaignId = :campaignMember.CampaignId  order by SortOrder, IsDefault asc];
        System.assert(!CampaignMemberStatus.isEmpty());
        //System.assertEquals(CampaignMemberStatus.get(0).Label, campaignMember.status);

        System.assertEquals(1, campaigns.size());

        Campaign campaign = campaigns.get(0);
        System.assertEquals(1, campaign.CORE_Number_of_Member_in_Livestorm__c);
        System.assertEquals(0, campaign.CORE_Number_of_Member_Status_Livestorm__c);
    }

    global class failWrongStatusMock implements HttpCalloutMock {
        global HttpResponse respond(HttpRequest req) {
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');

            if (req.getEndpoint().endsWith('events')){
                res.setBody('{"data": {"id": "2b7de38f-d2d9-4a73-beac-a6ed54d8a38b","attributes": {"registration_link": "https://app.livestorm.co/p/2b7de38f-d2d9-4a73-beac-a6ed54d8a38b"}}}');
                res.setStatusCode(201);
            }
            else if (req.getEndpoint().endsWith('sessions')){
                res.setBody('{"data": {"id": "6b3e4bde-2a75-4ed8-a4af-83a08a68a057","attributes": {"room_link": "https://app.livestorm.co/p/2b7de38f-d2d9-4a73-beac-a6ed54d8a38b/live?s=6b3e4bde-2a75-4ed8-a4af-83a08a68a057"}}}');
                res.setStatusCode(201);
            }
            else if (req.getEndpoint().endsWith('people') && req.getMethod() == 'POST'){
                res.setBody('{"data": {"id": "6b3e4bde-2a75-4ed8-a4af-83a08a68a057","attributes": {"registrant_detail": {"connection_link": "https://app.livestorm.co/p/2b7de38f-d2d9-4a73-beac-a6ed54d8a38b/live?s=6b3e4bde-2a75-4ed8-a4af-83a08a68a057"}}}}');
                res.setStatusCode(201);
            }
            else if (req.getEndpoint().endsWith('people') && req.getMethod() == 'GET'){
                res.setBody('{"data": [{"id": "6b3e4bde-2a75-4ed8-a4af-83a08a68a057","type": "people","attributes": {"registrant_detail": {"attended": false}}}],"meta": {"record_count": 1}}');
                res.setStatusCode(201);
            }

            return res;
        }
    }
}