@isTest
private class CORE_AOL_OPP_RetailAgent_TEST {

    private static final String AOL_EXTERNALID = 'setupAol';

    @TestSetup
    static void makeData(){
        CORE_CountrySpecificSettings__c countrySetting = new CORE_CountrySpecificSettings__c(
                CORE_FieldPrefix__c = CORE_AOL_Constants.PREFIX_CORE
        );
        database.insert(countrySetting, true);

        CORE_CatalogHierarchyModel catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test').catalogHierarchy;
        Account account = CORE_DataFaker_Account.getStudentAccount('test');
        Opportunity  opportunity = CORE_DataFaker_Opportunity.getAOLOpportunity(catalogHierarchy.division.Id,
                catalogHierarchy.businessUnit.Id,
                account.Id,
                CORE_AOL_Constants.PKL_OPP_STAGENAME_PROSPECT,
                CORE_AOL_Constants.PKL_OPP_SUBSTATUS_PROSPECTAPPSTARTED,
                AOL_EXTERNALID
        );

        Account businessAccount = new Account(
            Name = 'Business Account Test',
            CORE_LegacyCRMID__c = 'testBA',
            RecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('CORE_Business_Account').getRecordTypeId()
        );
        database.insert(businessAccount, true);

        Contact contact = new Contact(
            LastName = 'TEST Business Contact',
            AccountId = businessAccount.Id,
            CORE_LegacyContactCRMID__c = 'testBAC'
        );
        database.insert(contact, true);
    }

    @IsTest
    public static void retailAgentProcess_Should_Update_return_an_opp_with_updated_retailAgent_fields() {

        Contact contact = [SELECT Id, CORE_LegacyContactCRMID__c, AccountId, Account.CORE_LegacyCRMID__c FROM Contact WHERE LastName = 'TEST Business Contact' LIMIT 1];

        CORE_AOL_Wrapper.ExternalThirdParty retailAgency = new CORE_AOL_Wrapper.ExternalThirdParty();
        retailAgency.retailAgency = contact.Account.CORE_LegacyCRMID__c;
        retailAgency.retailAgencyBusinessContact = contact.CORE_LegacyContactCRMID__c;

        CORE_AOL_Wrapper.AOLProgressionFields aolProgressionFields= CORE_DataFaker_AOL_Wrapper.getAolProgressionFieldsWrapper();

        CORE_AOL_Wrapper.ResultWrapperGlobal result = CORE_AOL_OPP_RetailAgent.execute(AOL_EXTERNALID, retailAgency, aolProgressionFields);

        System.assertEquals(CORE_AOL_OPP_RetailAgent.SUCCESS_MESSAGE, result.message);
    }

    @IsTest
    public static void retailAgentProcess_Should_return_an_error_if_something_goes_wrong() {

        Contact contact = [SELECT Id, AccountId FROM Contact WHERE LastName = 'TEST Business Contact' LIMIT 1];

        CORE_AOL_Wrapper.ExternalThirdParty retailAgency = new CORE_AOL_Wrapper.ExternalThirdParty();
        retailAgency.retailAgency = contact.AccountId;
        retailAgency.retailAgencyBusinessContact = 'invalid salesforce ID';

        CORE_AOL_Wrapper.AOLProgressionFields aolProgressionFields= CORE_DataFaker_AOL_Wrapper.getAolProgressionFieldsWrapper();

        CORE_AOL_Wrapper.ResultWrapperGlobal result = CORE_AOL_OPP_RetailAgent.execute(AOL_EXTERNALID, retailAgency, aolProgressionFields);

        System.assertEquals(CORE_AOL_Constants.ERROR_STATUS, result.status);
    }

    @IsTest
    public static void retailAgentProcess_Should_return_an_error_if_mandatory_missing() {

        Contact contact = [SELECT Id, AccountId FROM Contact WHERE LastName = 'TEST Business Contact' LIMIT 1];

        CORE_AOL_Wrapper.ExternalThirdParty retailAgency = new CORE_AOL_Wrapper.ExternalThirdParty();
        retailAgency.retailAgency = contact.AccountId;
        retailAgency.retailAgencyBusinessContact = 'invalid salesforce ID';

        CORE_AOL_Wrapper.AOLProgressionFields aolProgressionFields= CORE_DataFaker_AOL_Wrapper.getAolProgressionFieldsWrapper();

        CORE_AOL_Wrapper.ResultWrapperGlobal result = CORE_AOL_OPP_RetailAgent.execute('', retailAgency, aolProgressionFields);

        System.assertEquals(CORE_AOL_Constants.ERROR_STATUS, result.status);
    }

    @IsTest
    public static void retailAgentProcess_Should_return_an_error_if_invalid_externalID() {

        Contact contact = [SELECT Id, AccountId FROM Contact WHERE LastName = 'TEST Business Contact' LIMIT 1];

        CORE_AOL_Wrapper.ExternalThirdParty retailAgency = new CORE_AOL_Wrapper.ExternalThirdParty();
        retailAgency.retailAgency = contact.AccountId;
        retailAgency.retailAgencyBusinessContact = 'invalid salesforce ID';

        CORE_AOL_Wrapper.AOLProgressionFields aolProgressionFields= CORE_DataFaker_AOL_Wrapper.getAolProgressionFieldsWrapper();

        CORE_AOL_Wrapper.ResultWrapperGlobal result = CORE_AOL_OPP_RetailAgent.execute('invalidID', retailAgency, aolProgressionFields);

        System.assertEquals(CORE_AOL_Constants.ERROR_STATUS, result.status);
    }
}