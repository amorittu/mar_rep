/**
 * Created by szocchi on 2022-03-22.
 */

public with sharing class IT_BatchExtendedInfoTaskLastModified  implements Database.Batchable<SObject>{
    Boolean runNext = false;

    public IT_UtilSetting__c utilSetting = IT_UtilSetting__c.getInstance();
    public String[] accountIds = new String[]{};

    public IT_BatchExtendedInfoTaskLastModified() {
    }

    public IT_BatchExtendedInfoTaskLastModified(Boolean runNextBatch, String[] accountIds) {
        this.runNext = runNextBatch;
        this.accountIds = accountIds;
    }

    public IT_BatchExtendedInfoTaskLastModified(Boolean runNextBatch) {
        this.runNext = runNextBatch;
    }
    public Database.QueryLocator start(Database.BatchableContext bc) {

        Integer daysSystemMode = utilSetting.IT_DaysSystemModeBatch__c != null ? Integer.valueOf(utilSetting.IT_DaysSystemModeBatch__c) : 365;

        if(Test.isRunningTest()) {
            return Database.getQueryLocator(
                    'SELECT Id , WhatId, CreatedDate, LastModifiedDate, OwnerId, CompletedDateTime, Status, CORE_Is_Incoming__c, AccountId ' +
                            'FROM Task'
            );
        }else {
            if( ! accountIds.isEmpty() ) {
                return Database.getQueryLocator(
                    'SELECT Id , WhatId, CreatedDate, LastModifiedDate, OwnerId, CompletedDateTime, Status, CORE_Is_Incoming__c, AccountId FROM Task ' +
                            'WHERE AccountId IN: accountIds ORDER BY AccountId, LastModifiedDate ASC ALL ROWS'
                );
            }
            return Database.getQueryLocator(
                    'SELECT Id , WhatId, CreatedDate, LastModifiedDate, OwnerId, CompletedDateTime, Status, CORE_Is_Incoming__c, AccountId FROM Task ' +
                            'WHERE What.Type = \'Opportunity\' AND WhatId != \'\' AND Owner.Type = \'User\' AND CompletedDateTime != null AND SystemModstamp = LAST_N_DAYS:' + daysSystemMode + ' AND isDeleted = FALSE ORDER BY AccountId, LastModifiedDate ASC ALL ROWS'
            );
        }
    }
    public void execute(Database.BatchableContext bc, List<Task> records) {
        System.debug('records.size(): ' + records.size());
        IT_TaskTriggerHandler.upsertExtendedInfo(records, null, true);
    }
    public void finish(Database.BatchableContext bc) {
        if(this.runNext){
            //Database.executeBatch(new IT_BatchExtendedInfoOpportunityCreated(true), 1000);
        }
    }
}