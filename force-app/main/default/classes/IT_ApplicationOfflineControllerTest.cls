/**
 * Created by SaverioTrovatoNigith on 19/04/2022.
 */

@IsTest
private class IT_ApplicationOfflineControllerTest {
    @TestSetup
    static void setup(){
        CORE_DataFaker_CatalogHierarchy catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('IM.SH0');
        Account account = CORE_DataFaker_Account.getStudentAccount('test1');
        Opportunity opportunity = CORE_DataFaker_Opportunity.getOpportunity(
                catalogHierarchy.division.Id,
                catalogHierarchy.businessUnit.Id,
                account.Id,
                'Prospect',
                'Prospect - New'
        );

        Opportunity opportunity2 = CORE_DataFaker_Opportunity.getOpportunity(
                catalogHierarchy.division.Id,
                catalogHierarchy.businessUnit.Id,
                account.Id,
                'Prospect',
                'Prospect - New'
        );

        CORE_AOL_Document__c doc = new CORE_AOL_Document__c();
        doc.Name = 'test';
        doc.CORE_Related_to_Opportunity__c = opportunity.Id;
        insert doc;
    }

	@IsTest
	static void testBehavior() {
        List<Opportunity> opties = [SELECT Id,CORE_OPP_Application_completed__c,CORE_OPP_Application_started__c,CORE_OPP_Division__r.CORE_Division_ExternalId__c, IT_BusinessUnitExternalId__c,CORE_First_document_upload_date__c,CORE_OPP_Selection_Process_Needed__c FROM Opportunity LIMIT 2];
        test.startTest();
        IT_ApplicationOfflineController.getLabels();
        IT_ApplicationOfflineController.getOpty(opties[0].Id);
        IT_ApplicationOfflineController.getOpty(opties[1].Id);
        IT_ApplicationOfflineController.getEntryAssessmentTestTypeValue();
        IT_ApplicationOfflineController.retrieveDocConfig(opties[0].Id,'NB');
        IT_ApplicationOfflineController.retrieveDocConfig(opties[1].Id,'NB');
        IT_ApplicationOfflineController.retrieveDocConfig(opties[0].Id,'DA');
        IT_ApplicationOfflineController.retrieveDocConfig(opties[1].Id,'DA');
        IT_ApplicationOfflineController.retrieveDocConfig(opties[0].Id,'IM');
        IT_ApplicationOfflineController.retrieveDocConfig(opties[1].Id,'IM');

        List<IT_ApplicationOfflineController.DocumentFileWrap> testWraps1 = new List<IT_ApplicationOfflineController.DocumentFileWrap>();
        List<IT_ApplicationOfflineController.DocumentFileWrap> testWraps2 = new List<IT_ApplicationOfflineController.DocumentFileWrap>();
        IT_ApplicationOfflineController.DocumentFileWrap testWrap = IT_ApplicationOfflineController.retrieveDocConfig(opties[0].Id,'IM')[0];
        IT_ApplicationOfflineController.DocumentFileWrap testWrap2 = IT_ApplicationOfflineController.retrieveDocConfig(opties[0].Id,'IM')[0];
        testWrap.isReceived = true;
        testWrap2.isReceived = false;
        testWraps1.add(testWrap);
        testWraps2.add(testWrap2);
        IT_ApplicationOfflineController.saveOptyAndDocs(opties[0],JSON.serialize(testWraps1));
        IT_ApplicationOfflineController.saveOptyAndDocs(opties[0],JSON.serialize(testWraps2));
        IT_ApplicationOfflineController.saveOptyAndDocs(opties[1],JSON.serialize(testWraps1));
        IT_ApplicationOfflineController.saveOptyAndDocs(opties[1],JSON.serialize(testWraps2));
        test.stopTest();
	}
}