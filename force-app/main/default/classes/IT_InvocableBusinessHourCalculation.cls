/**
 * Created by a morittu on 02/02/2022.
 * Test class: IT_InvocableBusinessHourCalculationTest
 */
global class IT_InvocableBusinessHourCalculation {

    public static final String ADDITION = 'addition';
    public static final String SUBSTRACTION = 'substraction'; 
    
    // MAP CONTAINING UNIT OF MEASURE AND TOTAL MILLISECONDS
    public static final Map<String, Long> TIME_PARSE_CONSTANT = new Map<String, Long> {
        'second'        => 1000, 
        'minute'        => 60000, 
        'hour'          => 3600000,
        'day'           => 86400000, 
        'week'          => 604800016, 
        'month'         => 2629800000L,
        'year'          => 31557600000L
    };

    // CONSTANT THAT RAPPRESENT NUMBER OF DAYS IN ONE WEEK
    public static final Integer ONE_WEEK_DAYS_NUMBER = 7;

    @InvocableMethod
    global static List<FlowOutputs> calculate(List<FlowInputs> requests){
        System.debug('IT_InvocableBusinessHourCalculation.calculate requests is : ' + JSON.serialize(requests) );

        List<FlowOutputs> returnList    = new List<FlowOutputs>();

        for (FlowInputs request : requests) {
            DateTime finalDateTime;
            if ( String.isNotBlank( request.businessHoursId) ) {
                finalDateTime = businessHoursCalculation(request);
            } else {
                finalDateTime = calendarDateCalculation(request);
            }
            
            if (String.isBlank(String.valueOf(request.value) )  ) {
               request.value = 0; 
            }

            FlowOutputs flOutput            = new FlowOutputs();
            flOutput.nextDateTime           = finalDateTime;
            flOutput.nextDate               = Date.newInstance(finalDateTime.year() , finalDateTime.month() , finalDateTime.day() );
            flOutput.day                    = finalDateTime.day();
            flOutput.month                  = finalDateTime.month();
            flOutput.year                   = finalDateTime.year();

            
            returnList.add(flOutput);
        }
        
        return returnList;

    }

    public static DateTime businessHoursCalculation(FlowInputs request) {
        DateTime checker = BusinessHours.nextStartDate(request.businessHoursId, request.startDateTime  );

        DateTime finalDateTime;
        Date finalDate;
        switch on request.operation {
            when 'addition' {
                finalDateTime = addDate( checker, request.value, request.unitOfMeasure, request.businessHoursId );
            }
            when 'substraction' {
                finalDateTime = substractDate(checker, request.value, request.unitOfMeasure, request.businessHoursId);
            }
            when else {
            //    throw new Exception('operation parameter has unaccepted value!');
            }     
        }
        return finalDateTime;
    }
    
    public static DateTime addDate(DateTime startDateTime, Long valueToAdd, String unitOfMeasure, String businessHourId) {
        Long        endTimeMillisecond    = valueToAdd * TIME_PARSE_CONSTANT.get(unitOfMeasure); 
        return  BusinessHours.addGMT(businessHourId, startDateTime, endTimeMillisecond);
    }

    public static DateTime substractDate(DateTime startDateTime, Long valueToAdd, String unitOfMeasure, String businessHourId) {
        Long        endTimeMillisecond    = (-valueToAdd) * TIME_PARSE_CONSTANT.get(unitOfMeasure); 
        return  BusinessHours.addGMT(businessHourId, startDateTime, endTimeMillisecond);
    }

    public static DateTIme calendarDateCalculation(FlowInputs request) {

        DateTime finalDateTime;
        Date finalDate;
        switch on request.operation {
            when 'addition' {
                finalDateTime = addCalendarDate( request.startDateTime, request.value, request.unitOfMeasure );
            }
            when 'substraction' {
                finalDateTime = substractCalendarDate(request.startDateTime, request.value, request.unitOfMeasure);
            }
            when else {
            //    throw new Exception('operation parameter has unaccepted value!');
            }     
        }
        return finalDateTime;
        
    }

    public static DateTime addCalendarDate(DateTime startDateTime, Long valueToAdd, String unitOfMeasure) {
        DateTime finalDateTime;
        Integer valueToAddInt = Integer.valueOf(valueToAdd);

        switch on unitOfMeasure {
            when 'second' {
                finalDateTime = startDateTime.addSeconds(valueToAddInt);
            }
            when 'minute' {
                finalDateTime = startDateTime.addMinutes(valueToAddInt);
            }
            when 'hour' {
                finalDateTime = startDateTime.addHours(valueToAddInt);
            }  
            when 'day' {
                finalDateTime = startDateTime.addDays(valueToAddInt);
            }   
            when 'week' {
                valueToAddInt = valueToAddInt * ONE_WEEK_DAYS_NUMBER;
                finalDateTime = startDateTime.addDays(valueToAddInt);
            }  
            when 'month' {
                finalDateTime = startDateTime.addMonths(valueToAddInt);
            } 
            when 'year' {
                finalDateTime = startDateTime.addYears(valueToAddInt);
            }  
            when else {

            }
        }
        return finalDateTime;
    }

    public static DateTime substractCalendarDate(DateTime startDateTime, Long valueToAdd, String unitOfMeasure) {
        DateTime finalDateTime;
        Integer valueToAddInt = Integer.valueOf(-valueToAdd);

        switch on unitOfMeasure {
            when 'second' {
                finalDateTime = startDateTime.addSeconds(valueToAddInt);
            }
            when 'minute' {
                finalDateTime = startDateTime.addMinutes(valueToAddInt);
            }
            when 'hour' {
                finalDateTime = startDateTime.addHours(valueToAddInt);
            }  
            when 'day' {
                finalDateTime = startDateTime.addDays(valueToAddInt);
            }   
            when 'week' {
                valueToAddInt = valueToAddInt * ONE_WEEK_DAYS_NUMBER;
                finalDateTime = startDateTime.addDays(valueToAddInt);
            }  
            when 'month' {
                finalDateTime = startDateTime.addMonths(valueToAddInt);
            } 
            when 'year' {
                finalDateTime = startDateTime.addYears(valueToAddInt);
            }  
            when else {

            }
        }
        return finalDateTime;
    }

    global class FlowInputs{

        @InvocableVariable (label='Insert a businessHour id if you want to calculate it considerating business days and hours')
        global String businessHoursId;

        @InvocableVariable (label='startDateTime.' required=true )
        global DateTime startDateTime;
        
        @InvocableVariable (label='which operation. Possible values are: "addition", "subtraction"' required=true)
        global String operation;

        @InvocableVariable (label='which unit of measure. Possible values are: "seconds", "minute", "hour", "day", "week", "month", "year" ' required=true)
        global String unitOfMeasure;

        @InvocableVariable (label='value to calculate')
        global Long value;

    }

    global class FlowOutputs{

        @InvocableVariable
        global Datetime nextDateTime;

        @InvocableVariable
        global Date nextDate;

        @InvocableVariable
        global Integer day;

        @InvocableVariable
        global Integer month;

        @InvocableVariable
        global Integer year;

    }
}