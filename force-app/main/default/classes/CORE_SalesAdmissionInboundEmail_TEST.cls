@isTest
private class CORE_SalesAdmissionInboundEmail_TEST  {

	private static final String INTEREST_EXTERNALID = 'setup';
    private static final String ACADEMIC_YEAR = 'CORE_PKL_2023-2024';

    @testSetup 
    static void createData(){
        Id profileId = [SELECT Id FROM Profile WHERE Name = 'CORE_API_Profile' LIMIT 1].Id;

        User usr = new User(
            Username = 'usertest@onlineshop.com',
            LastName = 'TEST_USER',
            Email = 'testemail@yopmail.com',
            Alias = 'T', 
            CommunityNickname = 'T',
            TimeZoneSidKey = 'Europe/Paris',
            LocaleSidKey = 'fr_FR_EURO',
            EmailEncodingKey = 'ISO-8859-1', 
            ProfileId = profileId,
            LanguageLocaleKey = 'fr'      
        );
        database.insert(usr, true);

        CORE_CatalogHierarchyModel catalogHierarchy = new CORE_DataFaker_CatalogHierarchy(INTEREST_EXTERNALID).catalogHierarchy;
        Account account = CORE_DataFaker_Account.getStudentAccount(INTEREST_EXTERNALID);

        Id oppEnroll_RTId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get(CORE_AOL_Constants.OPP_RECORDTYPE_ENROLLMENT).getRecordTypeId();
		Product2 product = [Select Id, ProductCode from Product2 WHERE ProductCode = 'setup' LIMIT 1];

        Opportunity opp1 = new Opportunity(
            Name ='test',
            StageName = CORE_AOL_Constants.PKL_OPP_STAGENAME_PROSPECT,
            CORE_OPP_Sub_Status__c = CORE_AOL_Constants.PKL_OPP_SUBSTATUS_PROSPECTAPPSTARTED,
            CORE_OPP_Division__c = catalogHierarchy.division.Id,
            CORE_OPP_Business_Unit__c = catalogHierarchy.businessUnit.Id,
            CORE_OPP_Study_Area__c = catalogHierarchy.studyArea.Id,
            CORE_OPP_Curriculum__c = catalogHierarchy.curriculum.Id,
            CORE_OPP_Level__c = catalogHierarchy.level.Id,
            CORE_OPP_Intake__c = catalogHierarchy.intake.Id,
            CORE_OPP_Academic_Year__c = ACADEMIC_YEAR,
            AccountId = account.Id,
            CloseDate = Date.Today().addDays(2),
            OwnerId = UserInfo.getUserId(),
            RecordTypeId = oppEnroll_RTId,
            CORE_OPP_OnlineShop_Id__c = '12345',
			CORE_Main_Product_Interest__c = product.Id
        );

        database.insert(new List<Opportunity> { opp1 }, true);

        Id pricebookId = CORE_DataFaker_PriceBook.getStandardPricebookId();

        PricebookEntry pricebookEntry = CORE_DataFaker_PriceBook.getPriceBookEntry(product.Id, pricebookId, 10);
        OpportunityLineItem lineItem = CORE_DataFaker_Opportunity.getOpportunityLineItem(opp1.Id, pricebookEntry.Id, 'test');
    }

    @isTest
    static void test_Email(){
        User usr = [SELECT Id FROM User WHERE Username = 'usertest@onlineshop.com' LIMIT 1];

        List<PermissionSetAssignment> psaList = new List<PermissionSetAssignment>();

        for(PermissionSet ps : [SELECT Id FROM PermissionSet WHERE Name = 'CORE_LeadFormAcquisition']){
            psaList.add(
                new PermissionSetAssignment(AssigneeId = usr.id, PermissionSetId = ps.Id)
            );
        }
        
        database.insert(psaList, true);

        System.runAs(usr){

            String reqBody = '{'
			+ '"fromName": "fromTest",'
 			+ '"fromEmail": "fromtest@test.com",'
			+ '"toEmail": ["totest1@test.com", "totest2@test.com"],'
 			+ '"ccEmail": ["test1@test.com","test2@test.com"],'
			+ '"subject": "TEST SUBJECT",'
			+ '"htmlBody": "This is test<br/>body",'
			+ '"plainTextBody": "This is test body",'
			+ '"emailDatetime": "' + Datetime.now().format('yyyy-MM-dd')+'T00:00:00Z' + '",'
            + '"headers": ["test1", "test2", "", "2022-03-21"],'
			+ '"messageId": "",'
			+ '"inReplyTo": [],'
			+ '"attachments": ['
			+ '""'
			+ ']'
			+ '}';

            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
            req.requestURI = '/sales-and-admission-incoming-email';

            req.httpMethod = 'POST';
            req.requestBody = Blob.valueOf(reqBody);

            RestContext.request = req;
            RestContext.response= res;

            test.startTest();
            CORE_SalesAdmissionInboundEmail_Wrapper.ResultWrapper results = CORE_SalesAdmissionInboundEmail.inboundEmail();
            test.stopTest();
        }
    }
}