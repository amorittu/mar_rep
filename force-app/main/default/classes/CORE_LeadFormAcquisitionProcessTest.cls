@IsTest
public class CORE_LeadFormAcquisitionProcessTest {
    @TestSetup
    static void makeData(){
        CORE_DataFaker_CatalogHierarchy catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test');

        Account account = CORE_DataFaker_Account.getStudentAccount('test');
        Opportunity opportunity = CORE_DataFaker_Opportunity.getOpportunity(catalogHierarchy.division.Id, 
            catalogHierarchy.businessUnit.Id, 
            catalogHierarchy.studyArea.Id,
            catalogHierarchy.curriculum.Id,
            catalogHierarchy.level.Id,
            catalogHierarchy.intake.Id, 
            account.Id, 
            'Lead', 
            'Lead - New'
        );
    }

    @IsTest
    public static void CORE_LeadFormAcquisitionProcess_constructor_Should_initialize_attributes(){
        Datetime dt= System.now();
        CORE_LeadFormAcquisitionWrapper.GlobalWrapper globalWrapper = CORE_DataFaker_LeadFormAcquWrapper.getGlobalWrapper();

        Test.startTest();
        CORE_LeadFormAcquisitionProcess lfap = new CORE_LeadFormAcquisitionProcess(dt, globalWrapper);
        Test.stopTest();
        
        System.assertEquals(dt, lfap.apiCallDate);
        System.assertEquals(globalWrapper, lfap.datas);
        System.assertNotEquals(null, lfap.errors);
        System.assertEquals(0, lfap.errors.size());
        System.assertNotEquals(null, lfap.result);
        System.assertNotEquals(null, lfap.dataMaker);
    }

    @IsTest
    public static void execute_Should_upsert_account_and_opportunities_and_oppLineItems_and_insert_consents_and_pointOfContact_and_campaignMember(){
        Account account = [Select id, PersonEmail from account];
        Campaign campaign = CORE_DataFaker_Campaign.getCampaign('test');
        CORE_Division__c division = [Select Id,CORE_Division_ExternalId__c from CORE_Division__c];
        CORE_Business_Unit__c businessUnit = [Select Id,CORE_Business_Unit_ExternalId__c from CORE_Business_Unit__c];
        CORE_Study_Area__c studyArea = [Select Id,CORE_Study_Area_ExternalId__c from CORE_Study_Area__c];
        CORE_Curriculum__c curriculum = [Select Id,CORE_Curriculum_ExternalId__c from CORE_Curriculum__c];
        CORE_Level__c level = [Select Id,CORE_Level_ExternalId__c from CORE_Level__c];
        CORE_Intake__c intake = [Select Id,CORE_Intake_ExternalId__c from CORE_Intake__c];
        Product2 product = [Select Id From Product2];
        Opportunity opportunity = [Select Id, CORE_OPP_Academic_Year__c From Opportunity];
    

        CORE_LeadFormAcquisitionWrapper.GlobalWrapper globalWrapper = CORE_DataFaker_LeadFormAcquWrapper.getGlobalWrapper();
        globalWrapper.mainInterest.division = division.CORE_Division_ExternalId__c;
        globalWrapper.mainInterest.businessUnit = businessUnit.CORE_Business_Unit_ExternalId__c;
        globalWrapper.mainInterest.studyArea = studyArea.CORE_Study_Area_ExternalId__c;
        globalWrapper.mainInterest.curriculum = curriculum.CORE_Curriculum_ExternalId__c;
        globalWrapper.mainInterest.level = level.CORE_Level_ExternalId__c;
        globalWrapper.mainInterest.intake = intake.CORE_Intake_ExternalId__c;
        globalWrapper.mainInterest.academicYear = opportunity.CORE_OPP_Academic_Year__c;

        globalWrapper.secondInterest.division = division.CORE_Division_ExternalId__c;
        globalWrapper.secondInterest.businessUnit = businessUnit.CORE_Business_Unit_ExternalId__c;
        globalWrapper.secondInterest.studyArea = studyArea.CORE_Study_Area_ExternalId__c;
        globalWrapper.secondInterest.curriculum = curriculum.CORE_Curriculum_ExternalId__c;
        globalWrapper.secondInterest.level = level.CORE_Level_ExternalId__c;
        globalWrapper.secondInterest.intake = intake.CORE_Intake_ExternalId__c;
        globalWrapper.secondInterest.academicYear = opportunity.CORE_OPP_Academic_Year__c;

        globalWrapper.thirdInterest.division = division.CORE_Division_ExternalId__c;
        globalWrapper.thirdInterest.businessUnit = businessUnit.CORE_Business_Unit_ExternalId__c;
        globalWrapper.thirdInterest.studyArea = studyArea.CORE_Study_Area_ExternalId__c;
        globalWrapper.thirdInterest.curriculum = curriculum.CORE_Curriculum_ExternalId__c;
        globalWrapper.thirdInterest.level = level.CORE_Level_ExternalId__c;
        globalWrapper.thirdInterest.intake = intake.CORE_Intake_ExternalId__c;
        globalWrapper.thirdInterest.academicYear = opportunity.CORE_OPP_Academic_Year__c;
        
        globalWrapper.campaignSubscription.campaignId = campaign.CORE_External_Id__c;
        globalWrapper.consents.get(0).division = division.CORE_Division_ExternalId__c;
        globalWrapper.consents.get(0).businessUnit = businessUnit.CORE_Business_Unit_ExternalId__c;
        globalWrapper.personAccount.emailAddress = account.PersonEmail;
        globalWrapper.prospectiveStudentComment = 'test';

        Id pricebookId = CORE_DataFaker_PriceBook.getStandardPricebookId();
        PricebookEntry pricebookEntry =  CORE_DataFaker_PriceBook.getPriceBookEntry(product.Id, pricebookId, 100.1);
        CORE_PriceBook_BU__c pricebookBu =  CORE_DataFaker_PriceBook.getPriceBookBu(pricebookId, businessUnit.Id, 'test');

        Datetime dt= System.now();
        CORE_LeadFormAcquisitionProcess lfap = new CORE_LeadFormAcquisitionProcess(dt, globalWrapper);
        delete opportunity;

        Test.startTest();
            lfap.execute();
        Test.stopTest();

        System.assertNotEquals(null, lfap.result);
        System.assertNotEquals(null, lfap.result.account);
        System.assertNotEquals(null, lfap.result.campaignMember);
        System.assertNotEquals(null, lfap.result.consents);
        System.assertNotEquals(0, lfap.result.consents.size());
        System.assertNotEquals(null, lfap.result.pointOfContacts);
        System.assertNotEquals(0, lfap.result.pointOfContacts.size());
        System.assertNotEquals(null, lfap.result.opportunities);
        System.assertNotEquals(null, lfap.result.opportunities.mainInterest);

        System.assert(String.IsNotBlank(lfap.result.account.Id));
        System.assert(String.IsNotBlank(lfap.result.campaignMember.Id));
        System.assert(String.IsNotBlank(lfap.result.consents.get(0).Id));
        System.assert(String.IsNotBlank(lfap.result.pointOfContacts.get(0).Id));
        System.assert(String.IsNotBlank(lfap.result.opportunities.mainInterest.Id));

        List<Account> accounts = [Select Id From Account];
        List<CampaignMember> campaignMembers = [Select Id From CampaignMember];
        List<CORE_Point_of_Contact__c> pocs = [Select Id From CORE_Point_of_Contact__c];
        List<CORE_Consent__c> consents = [Select Id From CORE_Consent__c];
        List<Opportunity> opportunities = [Select Id From Opportunity];
        List<OpportunityLineItem> oppLineItems = [Select Id,Quantity From OpportunityLineItem];

        System.assert(accounts.size() > 0);
        System.assert(campaignMembers.size() > 0);
        System.assert(pocs.size() > 0);
        System.assert(consents.size() > 0);
        System.assert(opportunities.size() > 0);
        System.assert(oppLineItems.size() > 0);
    }

    @IsTest
    public static void execute_Should_do_nothing_when_there_is_an_error(){
        CORE_LeadFormAcquisitionWrapper.GlobalWrapper globalWrapper = CORE_DataFaker_LeadFormAcquWrapper.getGlobalWrapper();
        globalWrapper.personAccount = null;

        Datetime dt= System.now();
        CORE_LeadFormAcquisitionProcess lfap = new CORE_LeadFormAcquisitionProcess(dt, globalWrapper);
        Test.startTest();
        lfap.execute();
        Test.stopTest();

        System.assertNotEquals(null, lfap.result);
        System.assertNotEquals(null, lfap.errors);
        System.assert(String.isNotBlank(lfap.errors.get(0).errorCode));
    }


    @IsTest
    public static void getCampaignMember_Should_Insert_campagnMember_When_campaignSubscription_is_not_empty_and_there_is_no_existing_campaignMember_related_to_the_specified_account_and_campaign(){
        Datetime dt= System.now();
        Campaign campaign = CORE_DataFaker_Campaign.getCampaign('test');
        CORE_LeadFormAcquisitionWrapper.GlobalWrapper globalWrapper = CORE_DataFaker_LeadFormAcquWrapper.getGlobalWrapper();
        globalWrapper.campaignSubscription.campaignId = campaign.CORE_External_Id__c;
        CORE_LeadFormAcquisitionProcess lfap = new CORE_LeadFormAcquisitionProcess(dt, globalWrapper);

        Account account = [Select Id, PersonContactId from Account];
        Opportunity opportunity = [Select Id from Opportunity];

        List<Campaign> exsistingCampaigns = [SELECT Id FROM Campaign WHERE CORE_External_Id__c = :globalWrapper.campaignSubscription.campaignId];
        if(!exsistingCampaigns.isEmpty()){
            lfap.campaignIdFound = exsistingCampaigns.get(0).Id;
        }

        Test.startTest();
        CampaignMember result = lfap.getCampaignMember(account, opportunity);
        Test.stopTest();

        List<CampaignMember> campaignMembers = [Select Id from campaignMember where ContactId = :account.PersonContactId];

        System.assertNotEquals(null, result);
        System.assertNotEquals(0, campaignMembers.size());
        System.assert(String.isNotBlank(result.Id));
        System.assertEquals(result.Id, campaignMembers.get(0).Id);
    }

    @IsTest
    public static void getCampaignMember_Should_not_Insert_campagnMember_When_campaignSubscription_is_empty(){
        Datetime dt= System.now();
        CORE_LeadFormAcquisitionWrapper.GlobalWrapper globalWrapper = CORE_DataFaker_LeadFormAcquWrapper.getGlobalWrapper();
        globalWrapper.campaignSubscription = null;

        CORE_LeadFormAcquisitionProcess lfap = new CORE_LeadFormAcquisitionProcess(dt, globalWrapper);
        Campaign campaign = CORE_DataFaker_Campaign.getCampaign('test');

        Account account = [Select Id, PersonContactId from Account];
        Opportunity opportunity = [Select Id from Opportunity];
        
        Test.startTest();
        CampaignMember result = lfap.getCampaignMember(account, opportunity);
        Test.stopTest();

        List<CampaignMember> campaignMembers = [Select Id from campaignMember where ContactId = :account.PersonContactId];

        System.assertNotEquals(null, result);
        System.assert(String.isBlank(result.Id));
        System.assertEquals(0, campaignMembers.size());
    }

    @IsTest
    public static void getCampaignMember_Should_not_Insert_campagnMember_When_there_is_existing_campaignMember_related_to_the_specified_accound_and_campaign(){
        Datetime dt= System.now();
        Campaign campaign = CORE_DataFaker_Campaign.getCampaign('test');
        CORE_LeadFormAcquisitionWrapper.GlobalWrapper globalWrapper = CORE_DataFaker_LeadFormAcquWrapper.getGlobalWrapper();
        globalWrapper.campaignSubscription.campaignId = campaign.CORE_External_Id__c;

        CORE_LeadFormAcquisitionProcess lfap = new CORE_LeadFormAcquisitionProcess(dt, globalWrapper);
        Account account = [Select Id, PersonContactId from Account];
        Opportunity opportunity = [Select Id from Opportunity];
        CampaignMember campaignMember = CORE_DataFaker_Campaign.getCampaignMember(campaign.Id, account.PersonContactId);
        
      
        lfap.campaignIdFound = campaign.Id;

        Test.startTest();
        CampaignMember result = lfap.getCampaignMember(account, opportunity);
        Test.stopTest();

        List<CampaignMember> campaignMembers = [Select Id from campaignMember where ContactId = :account.PersonContactId];

        System.assertNotEquals(null, result);
        System.assertEquals(1, campaignMembers.size());
        System.assertEquals(campaignMembers.get(0).Id,result.Id);
    }

    @IsTest
    public static void getOppLineItems_Should_return_empty_list_when_opportunities_have_no_products(){
        Datetime dt= System.now();
        CORE_LeadFormAcquisitionWrapper.GlobalWrapper globalWrapper = CORE_DataFaker_LeadFormAcquWrapper.getGlobalWrapper();
        CORE_LeadFormAcquisitionProcess lfap = new CORE_LeadFormAcquisitionProcess(dt, globalWrapper);
        Account account = [Select Id from Account];
        Opportunity opp1 = [Select Id from Opportunity];
        CORE_Division__c division = [Select Id From CORE_Division__c];
        CORE_Business_Unit__c businessUnit = [Select Id From CORE_Business_Unit__c];

        Opportunity opp2 = CORE_DataFaker_Opportunity.getOpportunity(division.id, businessUnit.Id, account.Id, 'Prospect', 'Prospect - New');

        Test.startTest();
        List<OpportunityLineItem> results = lfap.getOppLineItems(new List<Opportunity>{opp1,opp2});
        Test.stopTest();

        System.AssertNotEquals(null, results);
        System.AssertEquals(0, results.size());
    }

    @IsTest
    public static void getOppLineItems_Should_return_empty_list_when_opportunities_are_tag_as_returningLead(){
        Datetime dt= System.now();
        CORE_LeadFormAcquisitionWrapper.GlobalWrapper globalWrapper = CORE_DataFaker_LeadFormAcquWrapper.getGlobalWrapper();
        CORE_LeadFormAcquisitionProcess lfap = new CORE_LeadFormAcquisitionProcess(dt, globalWrapper);
        Product2 product = [Select Id from Product2];
        Account account = [Select Id from Account];

        Opportunity opp1 = [Select Id from Opportunity];
        opp1.CORE_Main_Product_Interest__c = product.Id;
        opp1.CORE_ReturningLead__c = true;
        update opp1;

        CORE_Division__c division = [Select Id From CORE_Division__c];
        CORE_Business_Unit__c businessUnit = [Select Id From CORE_Business_Unit__c];

        Opportunity opp2 = CORE_DataFaker_Opportunity.getOpportunity(division.id, businessUnit.Id, account.Id, 'Prospect', 'Prospect - New');

        Test.startTest();
        List<OpportunityLineItem> results = lfap.getOppLineItems(new List<Opportunity>{opp1,opp2});
        Test.stopTest();

        System.AssertNotEquals(null, results);
        System.AssertEquals(0, results.size());
    }

    @IsTest
    public static void getOppLineItems_Should_return_empty_list_when_pricebookEntry_does_not_exist_for_the_product(){
        Datetime dt= System.now();
        CORE_LeadFormAcquisitionWrapper.GlobalWrapper globalWrapper = CORE_DataFaker_LeadFormAcquWrapper.getGlobalWrapper();
        CORE_LeadFormAcquisitionProcess lfap = new CORE_LeadFormAcquisitionProcess(dt, globalWrapper);
        Product2 product = [Select Id from Product2];
        Account account = [Select Id from Account];
        Opportunity opp1 = [Select Id from Opportunity];
        opp1.CORE_ReturningLead__c = false;
        opp1.CORE_Main_Product_Interest__c = product.Id;
        CORE_Business_Unit__c businessUnit = [Select Id From CORE_Business_Unit__c];
        opp1.CORE_OPP_Business_Unit__c = businessUnit.Id;
        update opp1;

        Test.startTest();
        List<OpportunityLineItem> results = lfap.getOppLineItems(new List<Opportunity>{opp1});
        Test.stopTest();

        System.AssertNotEquals(null, results);
        System.AssertEquals(0, results.size());
    }

    @IsTest
    public static void getOppLineItems_Should_return_list_of_oppLineItems_when_opportunity_have_product_and_all_conditions_are_met(){
        Datetime dt= System.now();
        CORE_LeadFormAcquisitionWrapper.GlobalWrapper globalWrapper = CORE_DataFaker_LeadFormAcquWrapper.getGlobalWrapper();
        CORE_LeadFormAcquisitionProcess lfap = new CORE_LeadFormAcquisitionProcess(dt, globalWrapper);
        Product2 product = [Select Id from Product2];
        CORE_Business_Unit__c bu = [Select Id, CORE_Business_Unit_ExternalId__c from CORE_Business_Unit__c];
        Account account = [Select Id from Account];
        Opportunity opp1 = [Select Id from Opportunity];
        opp1.CORE_ReturningLead__c = false;
        opp1.CORE_Main_Product_Interest__c = product.Id;
        opp1.CORE_OPP_Business_Unit__c = bu.Id;
        update opp1;

        Id pricebookId = CORE_DataFaker_PriceBook.getStandardPricebookId();
        PricebookEntry pricebookEntry =  CORE_DataFaker_PriceBook.getPriceBookEntry(opp1.CORE_Main_Product_Interest__c, pricebookId, 100.1);
        CORE_PriceBook_BU__c pricebookBu =  CORE_DataFaker_PriceBook.getPriceBookBu(pricebookId, opp1.CORE_OPP_Business_Unit__c, 'test');

        Test.startTest();
        List<OpportunityLineItem> results = lfap.getOppLineItems(new List<Opportunity>{opp1});
        Test.stopTest();

        System.AssertNotEquals(null, results);
        System.AssertEquals(1, results.size());
    }

    @IsTest
    public static void getOpportunities_Should_return_list_of_upserted_opportunities_when_there_is_existing_main_opportunities_on_same_bu_and_academicYear(){
        Datetime dt= System.now();
        CORE_LeadFormAcquisitionWrapper.GlobalWrapper globalWrapper = CORE_DataFaker_LeadFormAcquWrapper.getGlobalWrapper();
        CORE_Division__c division = [Select Id,CORE_Division_ExternalId__c from CORE_Division__c];
        CORE_Business_Unit__c businessUnit = [Select Id,CORE_Business_Unit_ExternalId__c from CORE_Business_Unit__c];
        CORE_Study_Area__c studyArea = [Select Id,CORE_Study_Area_ExternalId__c from CORE_Study_Area__c];
        CORE_Curriculum__c curriculum = [Select Id,CORE_Curriculum_ExternalId__c from CORE_Curriculum__c];
        CORE_Level__c level = [Select Id,CORE_Level_ExternalId__c from CORE_Level__c];
        CORE_Intake__c intake = [Select Id,CORE_Intake_ExternalId__c from CORE_Intake__c];
        
        Opportunity opportunity = [Select Id, AccountId, CORE_OPP_Academic_Year__c From Opportunity];
        opportunity.CORE_OPP_Business_Unit__c = businessUnit.Id;
        opportunity.CORE_ReturningLead__c = false;
        update opportunity;

        globalWrapper.secondInterest = null;
        globalWrapper.thirdInterest = null;
        globalWrapper.mainInterest.division = division.CORE_Division_ExternalId__c;
        globalWrapper.mainInterest.businessUnit = businessUnit.CORE_Business_Unit_ExternalId__c;
        globalWrapper.mainInterest.studyArea = studyArea.CORE_Study_Area_ExternalId__c;
        globalWrapper.mainInterest.curriculum = curriculum.CORE_Curriculum_ExternalId__c;
        globalWrapper.mainInterest.level = level.CORE_Level_ExternalId__c;
        globalWrapper.mainInterest.intake = intake.CORE_Intake_ExternalId__c;
        globalWrapper.mainInterest.academicYear = opportunity.CORE_OPP_Academic_Year__c;
        
        CORE_LeadFormAcquisitionProcess lfap = new CORE_LeadFormAcquisitionProcess(dt, globalWrapper);
        Account account1 = [Select Id from Account];
        Account accountWithFields = CORE_LeadFormAcquisitionUtils.getAccountFromId(account1.Id);

        Test.startTest();
        List<Opportunity> results = lfap.getOpportunities(accountWithFields);
        Test.stopTest();

        System.assertNotEquals(null, results);
        System.assertEquals(1, results.size());
    }

    @IsTest
    public static void getOpportunities_Should_return_list_of_opportunities_to_upsert(){
        Datetime dt= System.now();
        CORE_LeadFormAcquisitionWrapper.GlobalWrapper globalWrapper = CORE_DataFaker_LeadFormAcquWrapper.getGlobalWrapper();
        /*CORE_Division__c division = [Select Id,CORE_Division_ExternalId__c from CORE_Division__c];
        CORE_Business_Unit__c businessUnit = [Select Id,CORE_Business_Unit_ExternalId__c from CORE_Business_Unit__c];
        CORE_Study_Area__c studyArea = [Select Id,CORE_Study_Area_ExternalId__c from CORE_Study_Area__c];
        CORE_Curriculum__c curriculum = [Select Id,CORE_Curriculum_ExternalId__c from CORE_Curriculum__c];
        CORE_Level__c level = [Select Id,CORE_Level_ExternalId__c from CORE_Level__c];
        CORE_Intake__c intake = [Select Id,CORE_Intake_ExternalId__c from CORE_Intake__c];
        */
        CORE_DataFaker_CatalogHierarchy catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test2');
        Opportunity opportunity = [Select Id, AccountId, CORE_OPP_Academic_Year__c From Opportunity];
        opportunity.CORE_ReturningLead__c = false;
        update opportunity;

        globalWrapper.mainInterest.division = catalogHierarchy.division.CORE_Division_ExternalId__c;
        globalWrapper.mainInterest.businessUnit = catalogHierarchy.businessUnit.CORE_Business_Unit_ExternalId__c;
        globalWrapper.mainInterest.studyArea = catalogHierarchy.studyArea.CORE_Study_Area_ExternalId__c;
        globalWrapper.mainInterest.curriculum = catalogHierarchy.curriculum.CORE_Curriculum_ExternalId__c;
        globalWrapper.mainInterest.level = catalogHierarchy.level.CORE_Level_ExternalId__c;
        globalWrapper.mainInterest.intake = catalogHierarchy.intake.CORE_Intake_ExternalId__c;

        globalWrapper.secondInterest.division = catalogHierarchy.division.CORE_Division_ExternalId__c;
        globalWrapper.secondInterest.businessUnit = catalogHierarchy.businessUnit.CORE_Business_Unit_ExternalId__c;
        globalWrapper.secondInterest.studyArea = catalogHierarchy.studyArea.CORE_Study_Area_ExternalId__c;
        globalWrapper.secondInterest.curriculum = catalogHierarchy.curriculum.CORE_Curriculum_ExternalId__c;
        globalWrapper.secondInterest.level = catalogHierarchy.level.CORE_Level_ExternalId__c;
        globalWrapper.secondInterest.intake = catalogHierarchy.intake.CORE_Intake_ExternalId__c;

        globalWrapper.thirdInterest.division = catalogHierarchy.division.CORE_Division_ExternalId__c;
        globalWrapper.thirdInterest.businessUnit = catalogHierarchy.businessUnit.CORE_Business_Unit_ExternalId__c;
        globalWrapper.thirdInterest.studyArea = catalogHierarchy.studyArea.CORE_Study_Area_ExternalId__c;
        globalWrapper.thirdInterest.curriculum = catalogHierarchy.curriculum.CORE_Curriculum_ExternalId__c;
        globalWrapper.thirdInterest.level = catalogHierarchy.level.CORE_Level_ExternalId__c;
        globalWrapper.thirdInterest.intake = catalogHierarchy.intake.CORE_Intake_ExternalId__c;
        
        CORE_LeadFormAcquisitionProcess lfap = new CORE_LeadFormAcquisitionProcess(dt, globalWrapper);
        Account account1 = [Select Id from Account];
        Account accountWithFields = CORE_LeadFormAcquisitionUtils.getAccountFromId(account1.Id);

        Test.startTest();
        List<Opportunity> results = lfap.getOpportunities(accountWithFields);
        Test.stopTest();

        System.assertNotEquals(null, results);
        System.assertEquals(3, results.size());
    }

    @IsTest
    public static void getExistingMainOpportunities_should_return_existing_main_opportunities_related_to_an_account() {
        Account account = [Select Id from Account];
        Opportunity opp = [Select Id from Opportunity];
        CORE_Division__c division = [Select Id, CORE_Division_ExternalId__c from CORE_Division__c];
		CORE_Business_Unit__c businessUnit = [Select Id,CORE_Business_Unit_ExternalId__c from CORE_Business_Unit__c];

		List<CORE_Business_Unit__c> businessUnits = new List<CORE_Business_Unit__c>{businessUnit};
        Datetime dt= System.now();
        CORE_LeadFormAcquisitionWrapper.GlobalWrapper globalWrapper = CORE_DataFaker_LeadFormAcquWrapper.getGlobalWrapper();

        CORE_LeadFormAcquisitionProcess lfap = new CORE_LeadFormAcquisitionProcess(dt, globalWrapper);

        CORE_DataFaker_CatalogHierarchy catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test2');

        Opportunity newOpp = CORE_DataFaker_Opportunity.getOpportunity(catalogHierarchy.division.id, catalogHierarchy.businessUnit.Id, account.Id, 'Prospect', 'Prospect - New');

        Test.startTest();
        List<Opportunity> results = lfap.getExistingMainOpportunities(businessUnits, account.Id);
        Test.stopTest();

        System.assertNotEquals(null, results);
        System.assertEquals(1, results.size());
        System.assertEquals(opp.Id, results.get(0).Id);
    }

    @IsTest
    public static void getAccount_Should_insert_an_account_and_return_it_Xhen_there_is_no_existing_account() {
        Datetime dt= System.now();
        CORE_LeadFormAcquisitionWrapper.GlobalWrapper globalWrapper = CORE_DataFaker_LeadFormAcquWrapper.getGlobalWrapper();

        CORE_LeadFormAcquisitionProcess lfap = new CORE_LeadFormAcquisitionProcess(dt, globalWrapper);

        Test.startTest();
        Account result = lfap.getAccount(new List<Account>());
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assert(String.isNotBlank(result.Id));
    }

    @IsTest
    public static void getAccount_Should_insert_an_account_and_return_it_When_there_is_multiple_existing_account() {
        Datetime dt= System.now();
        Account account1 = [Select Id from Account];
        Account account2 = CORE_DataFaker_Account.getStudentAccount('test2');

        CORE_LeadFormAcquisitionWrapper.GlobalWrapper globalWrapper = CORE_DataFaker_LeadFormAcquWrapper.getGlobalWrapper();

        CORE_LeadFormAcquisitionProcess lfap = new CORE_LeadFormAcquisitionProcess(dt, globalWrapper);

        Test.startTest();
        Account result = lfap.getAccount(new List<Account>{account2, account1});
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assert(String.isNotBlank(result.Id));
        System.assertNotEquals(account1.Id, result.Id);
        System.assertEquals(account2.Id, result.Id);
    }

    @IsTest
    public static void getAccount_Should_update_an_account_and_return_it_When_there_is_only_one_existing_account() {
        Datetime dt= System.now();
        Account account1 = [Select Id from Account];
        Account accountWithFields = CORE_LeadFormAcquisitionUtils.getAccountFromId(account1.Id);

        CORE_LeadFormAcquisitionWrapper.GlobalWrapper globalWrapper = CORE_DataFaker_LeadFormAcquWrapper.getGlobalWrapper();

        CORE_LeadFormAcquisitionProcess lfap = new CORE_LeadFormAcquisitionProcess(dt, globalWrapper);

        Test.startTest();
        Account result = lfap.getAccount(new List<Account>{accountWithFields});
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assert(String.isNotBlank(result.Id));
        System.assertEquals(account1.Id, result.Id);
    }
}