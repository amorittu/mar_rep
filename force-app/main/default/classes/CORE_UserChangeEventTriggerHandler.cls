public class CORE_UserChangeEventTriggerHandler extends CORE_TriggerHandler {
    @TestVisible
    private Map<Id, UserChangeEvent> oldMap;
    @TestVisible
    private List<UserChangeEvent> newRecords;

    @TestVisible
    private OmniChannelConfig__mdt omniConfig;

    @TestVisible
    private static boolean forceProvisioningForTest = false;

    public CORE_UserChangeEventTriggerHandler() {
        this.oldMap = Trigger.oldMap != null ? (Map<Id, UserChangeEvent>) Trigger.oldMap : new Map<Id, UserChangeEvent>();
        this.newRecords = Trigger.new != null ? (List<UserChangeEvent>) Trigger.new : new List<UserChangeEvent>();

        if(Test.isRunningTest()){
            this.omniConfig = new OmniChannelConfig__mdt(
                MasterLabel = 'UNIT_TEST',
                DeveloperName = 'UNIT_TEST',
                CORE_Active_User_Deprovisioning__c = false,
                CORE_Active_User_Provisioning__c = forceProvisioningForTest
            );
        } else {
            this.omniConfig = OmniChannelConfig__mdt.getInstance('Default_Value');
        }
    }

    public override void afterInsert() {
        // String emailTest = '';
        // emailTest = ' this.newRecords.size() = '+ this.newRecords.size();
        if(this.newRecords.size() != 1 ){
            // test(emailTest);
            return;
        }

        UserChangeEvent userEvent = this.newRecords.get(0);
        EventBus.ChangeEventHeader header = userEvent.ChangeEventHeader; 

        // emailTest += '\n UserChangeEvent =' + JSON.serialize(event);
        // emailTest += '\n header.changedFields = ' + header.changedFields;
        // emailTest += '\n event.Email = ' + event.Email; 
        
        // System.debug('header.changedFields = ' + header.changedFields); 
         System.debug('userEvent.Email = ' + userEvent.Email); 
        
        if(omniConfig.CORE_Active_User_Provisioning__c && header.changedFields.contains('Email') && header.changeType == 'UPDATE'){ 
            userProvisioningProcess(userEvent, header);
        }
        // test(emailTest);

    }

    private void userProvisioningProcess(UserChangeEvent userEvent, EventBus.ChangeEventHeader header){
        Id recordId = header.recordIds.size() == 1 ? header.recordIds.get(0) : null;
        Id commitUser = header.commitUser;
        Id userId = recordId != null ? recordId : commitUser;
        User oldUser = [SELECT Id,IsActive, FirstName, LastName, Name, CallcenterId, Email, CORE_FLEX_Twilio_SID__c FROM user WHERE Id = :userId];
        User newUser = oldUser.clone(true, true, true, true);
        newUser.Email = userEvent.Email;
        System.debug('oldUser.Email = ' + oldUser.Email);
        System.debug('newUser.Email = ' + newUser.Email);
        // emailTest += '\n oldUser.Email = ' +  JSON.serialize(oldUser.Email);
        // emailTest += '\n newUser.Email = ' +  JSON.serialize(newUser.Email);

        CORE_FLEX_User.executeWorkerProcess(
            new List<User> {newUser}, 
            new Map<Id, User> {oldUser.Id => oldUser},
            this.omniConfig
        );
    }
    // private static void test(String emailBody){
    //     Messaging.reserveSingleEmailCapacity(2);

    //     //We instantiate our single email message object
    //     Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
 
    //     // Strings to hold the email addresses to which you are sending the email.
    //     String[] toAddresses = new String[] {'valentin.pitel@almaviacx.com'};
    //     //Assign the TO address to our mail object
    //     mail.setToAddresses(toAddresses);
 
    //     // Specify the name used as the display name.
    //     mail.setSenderDisplayName('ValSalesforce');
 
    //     // Set the subject line for your email address.
    //     mail.setSubject('CORE_UserChangeEventTrigger From Salesforce');
 
    //     // You can set this to true if you want to BCC yourself on the email
    //     mail.setBccSender(false);
 
    //     // You can specify your plain text here
    //     mail.setPlainTextBody(emailBody);
 
    //     // Send the email
    //     Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
    // }
}