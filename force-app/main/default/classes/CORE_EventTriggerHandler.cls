public without sharing class CORE_EventTriggerHandler {
    public static void handleTrigger(List<Event> oldRecords, List<Event> newRecords, System.TriggerOperation triggerEvent) {
        switch on triggerEvent {
            when BEFORE_INSERT {
                System.debug('CORE_ServiceAppointmentTriggerHandler.beforeInsert : START');
                beforeInsert(newRecords);
                System.debug('CORE_ServiceAppointmentTriggerHandler.beforeInsert : END');
            }
        }
    }

    @TestVisible
    private static void serviceAppointmentProcess(List<Event> newRecords) {
        List<Id> serviceAppointmentIds = new List<Id>();

        Map<Id,Contact> contactsMap = new Map<Id,Contact>();
        List<Contact> contacts = new List<Contact>();
        Map<Id,Id> accountsMap = new Map<Id,Id>();

        for (Event e : newRecords){
            if(e.ServiceAppointmentId != null && !serviceAppointmentIds.contains(e.ServiceAppointmentId)){
                serviceAppointmentIds.add(e.ServiceAppointmentId);
            }
        }

        if(serviceAppointmentIds.isEmpty()){
            return;
        }

        List<ServiceAppointment> serviceAppointments = [SELECT Id, Subject, WorkTypeId, Description, AccountId, WorkType.Name, AppointmentType, WorkType.CORE_Meeting_Type__c
        FROM ServiceAppointment
        WHERE Id IN :serviceAppointmentIds ];

        for(ServiceAppointment sp : serviceAppointments){
            accountsMap.put(sp.accountId, sp.Id);
        }

        contacts = [SELECT Id,AccountId FROM CONTACT WHERE AccountId in :accountsMap.keySet()];

        for(Contact c : contacts){
            contactsMap.put(accountsMap.get(c.AccountId), c);
        }

        Map<Id, ServiceAppointment> serviceAppointmentByIds = new Map<Id, ServiceAppointment>(serviceAppointments);

        for(Event newEvent : newRecords){
            if(serviceAppointmentByIds.keySet().contains(newEvent.ServiceAppointmentId)){
                ServiceAppointment serviceAppointment = serviceAppointmentByIds.get(newEvent.ServiceAppointmentId);

                if(serviceAppointment.workTypeId == null){
                    continue;
                }
                if(newEvent.Subject != null){
                    if(!newEvent.Subject.startsWith(serviceAppointment.WorkType.Name)){
                        newEvent.Subject = newEvent.Subject.substringBefore(':') + ': ';
                    } else {
                        newEvent.Subject = '';
                    }
                }                

                newEvent.Subject = getSaSubject(newEvent.Subject, serviceAppointment.Subject, serviceAppointment.AppointmentType, serviceAppointment.workType.Name);
                    //newEvent.Subject = serviceAppointment.Subject;

                newEvent.Type = 'CORE_PKL_Meeting';

                try {
                    newEvent.CORE_TECH_CallTaskType__c = serviceAppointment.WorkType.CORE_Meeting_Type__c;
                }
                catch(Exception e){//leave empty if value does not match between the 2 picklists
                    newEvent.CORE_TECH_CallTaskType__c = null;
                }

                if(serviceAppointment.Description != null){
                    newEvent.Description = serviceAppointment.Description;
                }
                if(serviceAppointment.AccountId != null){
                    newEvent.WhoId = contactsMap.get(serviceAppointment.Id).Id;
                }
                    
                System.debug('New Event ----->' + newEvent);
            }
            System.debug('newEvent.Subject = ' + newEvent.Subject);
        }
    }

    @TestVisible
    private static void beforeInsert(List<Event> newRecords) {
        serviceAppointmentProcess(newRecords);
    }

    public static String getSaSubject(String currentSubject, String appointmentSubject, String appointmentType, String worktypeName){
        String newSubject = currentSubject;

        newSubject += !String.isEmpty(appointmentSubject) ? appointmentSubject + ' - ' + worktypeName : worktypeName;
        newSubject += !String.isEmpty(newSubject) ?  ' - ' +  appointmentType : appointmentType;
        newSubject = newSubject.length() >= 255 ? newSubject.left(254) : newSubject;

        return newSubject;
    }
}