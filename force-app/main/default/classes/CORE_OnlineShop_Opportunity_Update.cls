@RestResource(urlMapping='/online-shop/opportunity')
global class CORE_OnlineShop_Opportunity_Update {

    public static final Set<String> invalidFieldsSet = new Set<String> { 'StageName', 'CORE_OPP_Sub_Status__c' };

    @HttpPut
    global static CORE_OnlineShop_Wrapper.ResultWrapperOpportunity updateOpportunity(){

        RestRequest	request = RestContext.request;
        CORE_OnlineShop_Wrapper.ResultWrapperOpportunity result = null;

        try{
            CORE_CountrySpecificSettings__c countrySetting = CORE_CountrySpecificSettings__c.getOrgDefaults();

            Map<String, Object> paramsMap = (Map<String, Object>)JSON.deserializeUntyped(request.requestBody.toString());
            system.debug('@updateOpportunity > paramsMap: ' + JSON.serializePretty(paramsMap));

            String onlineShopId = (String)paramsMap.get('CORE_OPP_OnlineShop_Id__c');

            result = checkMandatoryParameters(onlineShopId);
            RestResponse res = RestContext.response;
            if(result != null){
                return result;
            }

            Opportunity opp = CORE_OnlineShop_QueryProvider.getOpportunity(onlineShopId);
            if(opp == null){
                result = new CORE_OnlineShop_Wrapper.ResultWrapperOpportunity(CORE_OnlineShop_Constants.ERROR_STATUS, CORE_OnlineShop_Constants.MSG_OPPORTUNITY_NOT_FOUND );
               
                //res.responseBody = Blob.valueOf(JSON.serialize(result, false));
                res.statusCode = 400;
                return result;


            }

            //system.debug('@updateOpportunity > oppsList GET: ' + JSON.serializePretty(oppsList[0]));

            CORE_SIS_QueryProvider.oppsFieldsSet.removeAll(invalidFieldsSet);

            for(String param : paramsMap.keySet()){
                system.debug('@updateOpportunity > param: ' + param);

                if(CORE_OnlineShop_QueryProvider.oppsFieldsSet.contains(param)
                   || (countrySetting != null
                       && countrySetting.CORE_FieldPrefix__c != null
                       && countrySetting.CORE_FieldPrefix__c != CORE_OnlineShop_Constants.PREFIX_CORE
                       && param.startsWith(countrySetting.CORE_FieldPrefix__c))){

                        opp = (Opportunity)CORE_OnlineShop_GlobalWorker.assignValue(opp, param, paramsMap.get(param));
                    
                }
                else{
                    result = new CORE_OnlineShop_Wrapper.ResultWrapperOpportunity(CORE_OnlineShop_Constants.ERROR_STATUS, CORE_OnlineShop_Constants.MSG_INVALID_FIELDS);
                    res.statusCode = 400 ;
                    return result;
                }
            }
            //system.debug('@updateOpportunity > oppsList PUT: ' + JSON.serializePretty(oppsList[0]));

            database.update(opp, true);
            //system.debug('@updateOpportunity > oppsList after update: ' + JSON.serializePretty(oppsList[0]));

            result = new CORE_OnlineShop_Wrapper.ResultWrapperOpportunity(opp, CORE_OnlineShop_Constants.SUCCESS_STATUS);

        } catch(Exception e){
            result = new CORE_OnlineShop_Wrapper.ResultWrapperOpportunity(CORE_OnlineShop_Constants.ERROR_STATUS, e.getMessage() + ' at ' + e.getStackTraceString());
        }

        return result;
    }

    private static CORE_OnlineShop_Wrapper.ResultWrapperOpportunity checkMandatoryParameters(String onlineShopId){
        List<String> missingParameters = new List<String>();
        Boolean isMissingId = CORE_SIS_GlobalWorker.isParameterMissing(onlineShopId);

        if(isMissingId){
            missingParameters.add(CORE_OnlineShop_Constants.PARAM_ONLINESHOP_ID);
        }
        
        return getMissingParameters(missingParameters);
    }

    public static CORE_OnlineShop_Wrapper.ResultWrapperOpportunity getMissingParameters(List<String> missingParameters){
        String errorMsg = CORE_OnlineShop_Constants.MSG_MISSING_PARAMETERS;

        if(missingParameters.size() > 0){
            errorMsg += missingParameters;

            return new CORE_OnlineShop_Wrapper.ResultWrapperOpportunity(CORE_OnlineShop_Constants.ERROR_STATUS, errorMsg);
        }
        
        return null;
    }
}