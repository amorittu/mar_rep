@IsTest
public class CORE_OppLineItemsForFlowsTest {
    @TestSetup
    static void makeData(){
        CORE_DataFaker_CatalogHierarchy catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test');
        Account account = CORE_DataFaker_Account.getStudentAccount('test');
        Opportunity opportunity =  CORE_DataFaker_Opportunity.getOpportunity(
            catalogHierarchy.division.Id, 
            catalogHierarchy.businessUnit.Id, 
            catalogHierarchy.studyArea.Id,
            catalogHierarchy.curriculum.Id,
            catalogHierarchy.level.Id,
            catalogHierarchy.intake.Id,
            account.Id, 'Lead',
            'Lead - New'
        );

    }

    @IsTest
    public static void getLinesItemsWithPricebookEntriesValues_Should_Return_cloned_oppLineItem_list_with_new_pricebooEntryId_without_id() {
        Product2 product = [Select Id, ProductCode from Product2];
        Opportunity opp = [Select Id from Opportunity];
        Id pricebookId = CORE_DataFaker_PriceBook.getStandardPricebookId();

        PricebookEntry pricebookEntry = CORE_DataFaker_PriceBook.getPriceBookEntry(product.Id, pricebookId, 10);
        OpportunityLineItem lineItem = CORE_DataFaker_Opportunity.getOpportunityLineItem(opp.Id, pricebookEntry.Id, 'test');

        PriceBook2 priceBook2 = CORE_DataFaker_PriceBook.getPricebook('testA'); 
        PricebookEntry pricebookEntry2 = CORE_DataFaker_PriceBook.getPriceBookEntry(product.Id, priceBook2.Id, 10);

        List<OpportunityLineItem> lineItems = new List<OpportunityLineItem> {lineItem};

        System.assertEquals(1, lineItems.Size());
        System.assertNotEquals(null, lineItems.get(0).Id);

        Test.startTest();
        List<opportunityLineItem> newlineItems = CORE_OppLineItemsForFlows.getLinesItemsWithPricebookEntriesValues(lineItems, pricebookEntry2);
        Test.stopTest();

        System.assertEquals(1, newlineItems.Size());
        System.assertEquals(null, newlineItems.get(0).Id);
        System.assertEquals(pricebookEntry2.Id, newlineItems.get(0).PricebookEntryId);
        System.assertEquals(null, newlineItems.get(0).UnitPrice);
        System.assertEquals(null, newlineItems.get(0).CORE_External_Id__c);
    }

    @IsTest
    public static void getOppLineItems_Should_insert_oppLineItems_related_to_specified_pricebook(){
        Product2 product = [Select Id from Product2];
        Opportunity opp = [Select Id from Opportunity];
        Id pricebookId = CORE_DataFaker_PriceBook.getStandardPricebookId();

        PricebookEntry pricebookEntry = CORE_DataFaker_PriceBook.getPriceBookEntry(product.Id, pricebookId, 10);
        OpportunityLineItem lineItem = CORE_DataFaker_Opportunity.getOpportunityLineItem(opp.Id, pricebookEntry.Id, 'test');
        lineItem = [Select Id, Product2Id, ProductCode,UnitPrice,TotalPrice, Quantity, PricebookEntryId,CORE_External_Id__c, OpportunityId from OpportunityLineItem];
        Delete lineItem;

        PriceBook2 priceBook2 = CORE_DataFaker_PriceBook.getPricebook('testA'); 

        opp.priceBook2Id = priceBook2.Id;
        Update opp;
        PricebookEntry pricebookEntry2 = CORE_DataFaker_PriceBook.getPriceBookEntry(product.Id, priceBook2.Id, 10);

        CORE_OppLineItemsForFlows.OppLineItemsForFlowsParameters param = new CORE_OppLineItemsForFlows.OppLineItemsForFlowsParameters();
        param.priceBookId = priceBook2.Id;
        param.opportunityLineItems = new List<OpportunityLineItem> {lineItem};

        Test.startTest();
        List<CORE_OppLineItemsForFlows.Result> results = CORE_OppLineItemsForFlows.getOppLineItems(new List<CORE_OppLineItemsForFlows.OppLineItemsForFlowsParameters> {param});
        Test.stopTest();

        System.assertEquals(1, results.size());
        List<OpportunityLineItem> resultLineItems = results.get(0).opportunityLineItems;
        System.assertEquals(param.opportunityLineItems.size(), resultLineItems.size());
        System.assertEquals(pricebookEntry2.Id, resultLineItems.get(0).PricebookEntryId);
        System.assertNotEquals(null, resultLineItems.get(0).Id);
        List<OpportunityLineItem> lineItems = [Select Id from OpportunityLineItem];
        System.assertEquals(1, lineItems.size());

    }
}