@IsTest
public with sharing class CORE_AOL_GlobalWorkerTest {
    private static final String AOL_EXTERNALID = 'setupAol';

    @TestSetup
    static void makeData(){
        CORE_CatalogHierarchyModel catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test').catalogHierarchy;
        Account account = CORE_DataFaker_Account.getStudentAccount('test');
        Opportunity  opportunity = CORE_DataFaker_Opportunity.getAOLOpportunity(catalogHierarchy.division.Id, 
            catalogHierarchy.businessUnit.Id, 
            account.Id, 
            CORE_AOL_Constants.PKL_OPP_STAGENAME_PROSPECT, 
            CORE_AOL_Constants.PKL_OPP_SUBSTATUS_PROSPECTAPPSTARTED, 
            AOL_EXTERNALID
        );
    }

    @IsTest
    public static void constructor_Should_initialize_datas() {
        CORE_AOL_Wrapper.AOLProgressionFields aolProgressionFields= CORE_DataFaker_AOL_Wrapper.getAolProgressionFieldsWrapper();

        CORE_AOL_GlobalWorker globalWorker = new CORE_AOL_GlobalWorker(aolProgressionFields,AOL_EXTERNALID);

        System.assertEquals(AOL_EXTERNALID,globalWorker.aolExternalId);
        //opportunity found
        System.assertNotEquals(null,globalWorker.aolOpportunity);
        System.assertEquals(aolProgressionFields,globalWorker.aolProgressionFields);

        globalWorker = new CORE_AOL_GlobalWorker(aolProgressionFields,'testRandom');
        //opportunity not found
        System.assertEquals(null,globalWorker.aolOpportunity);
    }

    @IsTest
    public static void updateAolProgressionFields_should_update_opportunity_fields_with_aolProgressionFieldsValues(){
        CORE_AOL_Wrapper.AOLProgressionFields aolProgressionFields= CORE_DataFaker_AOL_Wrapper.getAolProgressionFieldsWrapper();

        CORE_AOL_GlobalWorker globalWorker = new CORE_AOL_GlobalWorker(aolProgressionFields,AOL_EXTERNALID);
        globalWorker.updateAolProgressionFields();

        Opportunity opp = [SELECT Id, CORE_OPP_Application_Online_Advancement__c, CORE_Document_progression__c, 
            CORE_First_document_upload_date__c, CORE_Last_AOL_event_date__c,
            CORE_OPP_Application_Online__c,CORE_Is_AOL_Register__c, CORE_Is_AOL_Retail_Register__c 
            FROM Opportunity 
            WHERE CORE_OPP_Application_Online_ID__c = :AOL_EXTERNALID];

        System.assertEquals(aolProgressionFields.aolProgression, opp.CORE_OPP_Application_Online_Advancement__c);
        System.assertEquals(aolProgressionFields.documentProgression, opp.CORE_Document_progression__c);
        System.assertEquals(aolProgressionFields.firstDocumentUploadDate, opp.CORE_First_document_upload_date__c);
        System.assertEquals(aolProgressionFields.lastAolEventDate, opp.CORE_Last_AOL_event_date__c);
        System.assertEquals(aolProgressionFields.isAol, opp.CORE_OPP_Application_Online__c);
        System.assertEquals(aolProgressionFields.isAolOnlineRegistered, opp.CORE_Is_AOL_Register__c);
        System.assertEquals(aolProgressionFields.isAolRetailRegistered, opp.CORE_Is_AOL_Retail_Register__c);
    }

    @IsTest
    public static void getRelatedAccount_should_return_account_related_to_AolOpportunity(){
        CORE_AOL_Wrapper.AOLProgressionFields aolProgressionFields= CORE_DataFaker_AOL_Wrapper.getAolProgressionFieldsWrapper();
        CORE_AOL_GlobalWorker globalWorker = new CORE_AOL_GlobalWorker(aolProgressionFields,AOL_EXTERNALID);
        Account account = globalWorker.getRelatedAccount();

        System.assertNotEquals(null, account);
    }

    @IsTest
    private static void getExistingMainOpps_should_return_existing_main_opportunities_related_to_account_and_bu_and_ay(){
        Opportunity opportunity = [SELECT Id,AccountId, CORE_OPP_Academic_Year__c, CORE_OPP_Division__c, CORE_OPP_Business_Unit__r.CORE_Business_Unit_ExternalId__c FROM Opportunity WHERE CORE_OPP_Application_Online_ID__c = :AOL_EXTERNALID];
        Account account = [SELECT Id,PersonContactId FROM Account Where Id = :opportunity.AccountId];

        String aolExternalId = 'getExistingMainOppsId';
        Opportunity  opportunity2 = CORE_DataFaker_Opportunity.getAOLOpportunity(
            opportunity.CORE_OPP_Division__c, 
            opportunity.CORE_OPP_Business_Unit__c,
            opportunity.AccountId, 
            CORE_AOL_Constants.PKL_OPP_STAGENAME_PROSPECT, 
            CORE_AOL_Constants.PKL_OPP_SUBSTATUS_PROSPECTAPPSTARTED, 
            aolExternalId
        );

        Opportunity result = CORE_AOL_GlobalWorker.getExistingMainOpp(opportunity.AccountId, 
            opportunity.CORE_OPP_Business_Unit__r.CORE_Business_Unit_ExternalId__c,
            opportunity.CORE_OPP_Academic_Year__c
        );

        System.assertEquals(null, result);

        Opportunity  opportunity3 = CORE_DataFaker_Opportunity.getAOLOpportunity(
            opportunity.CORE_OPP_Division__c, 
            opportunity.CORE_OPP_Business_Unit__c,
            opportunity.AccountId, 
            CORE_AOL_Constants.PKL_OPP_STAGENAME_PROSPECT, 
            CORE_AOL_Constants.PKL_OPP_SUBSTATUS_PROSPECTAPPSTARTED, 
            null
        );

        result = CORE_AOL_GlobalWorker.getExistingMainOpp(opportunity.AccountId, 
            opportunity.CORE_OPP_Business_Unit__r.CORE_Business_Unit_ExternalId__c,
            opportunity.CORE_OPP_Academic_Year__c
        );

        System.assertEquals(opportunity3.Id, result.Id);
    }

    @IsTest
    private static void getAolOpportunity_should_return_opportunity_with_specified_aolExternalId(){
        Opportunity opportunity = [SELECT Id,AccountId, CORE_OPP_Academic_Year__c, CORE_OPP_Division__c, CORE_OPP_Business_Unit__c FROM Opportunity WHERE CORE_OPP_Application_Online_ID__c = :AOL_EXTERNALID];
        CORE_AOL_Wrapper.AOLProgressionFields aolProgressionFields= CORE_DataFaker_AOL_Wrapper.getAolProgressionFieldsWrapper();
        CORE_AOL_GlobalWorker globalWorker = new CORE_AOL_GlobalWorker(aolProgressionFields,'test');

        Opportunity result = globalWorker.getAolOpportunity();

        System.assertEquals(null, result);

        globalWorker = new CORE_AOL_GlobalWorker(aolProgressionFields, AOL_EXTERNALID);
        result = globalWorker.getAolOpportunity();

        System.assertEquals(opportunity.Id, result.Id);
    }

    @IsTest
    private static void getIntakeFromExternalId_should_return_null_if_there_is_no_intake_found(){
        CORE_Intake__c result = CORE_AOL_GlobalWorker.getIntakeFromExternalId('fakeExternalId');
        System.assertEquals(null, result);

        result = CORE_AOL_GlobalWorker.getIntakeFromExternalId(null);
        System.assertEquals(null, result);

        result = CORE_AOL_GlobalWorker.getIntakeFromExternalId(' ');
        System.assertEquals(null, result);
    }

    @IsTest
    private static void getIntakeFromExternalId_should_return_an_intake_if_exist(){
        CORE_Intake__c intake = [SELECT Id, CORE_Intake_ExternalId__c FROM CORE_Intake__c];
        CORE_Intake__c result = CORE_AOL_GlobalWorker.getIntakeFromExternalId(intake.CORE_Intake_ExternalId__c);
        System.assertEquals(intake.Id, result.Id);
    }

    @IsTest
    private static void getMissingParameters_should_return_null_if_parameters_is_null_or_empty(){
        CORE_AOL_Wrapper.ResultWrapperGlobal result = CORE_AOL_GlobalWorker.getMissingParameters(null);
        System.assertEquals(null, result);
        result = CORE_AOL_GlobalWorker.getMissingParameters(new List<String>());
        System.assertEquals(null, result);
    }

    @IsTest
    private static void getMissingParameters_should_return_a_ResultWrapperGlobal_if_parameters_is_not_empty(){
        String errorMsg = 'testError';

        CORE_AOL_Wrapper.ResultWrapperGlobal result = CORE_AOL_GlobalWorker.getMissingParameters(new List<String>{errorMsg});
        System.assertNotEquals(null, result);
        System.assert(result.message.contains(errorMsg));
        System.assertEquals(CORE_AOL_Constants.ERROR_STATUS, result.status);
    }

    @IsTest
    private static void removeUniqueFields_should_set_all_unique_fields_to_null(){
        Opportunity opportunity = new Opportunity();
        String uniqueValue = 'test';
        opportunity.CORE_Application_Fee_Transaction_Id__c = uniqueValue;
        opportunity.CORE_Lead_Source_External_Id__c = uniqueValue;
        opportunity.CORE_OPP_AOL_Opportunity_Id__c = uniqueValue;
        opportunity.CORE_OPP_Application_Online_ID__c = uniqueValue;
        opportunity.CORE_OPP_Legacy_CRM_Id__c = uniqueValue;
        opportunity.CORE_OPP_SIS_Opportunity_Id__c = uniqueValue;
        opportunity.CORE_OPP_SIS_Student_Academic_Record_ID__c = uniqueValue;
        opportunity.CORE_Tuition_fee_Transaction_ID__c = uniqueValue;

        opportunity = CORE_AOL_GlobalWorker.removeUniqueFields(opportunity);

        System.assertEquals(null, opportunity.CORE_Application_Fee_Transaction_Id__c);
        System.assertEquals(null, opportunity.CORE_Lead_Source_External_Id__c);
        System.assertEquals(null, opportunity.CORE_OPP_AOL_Opportunity_Id__c);
        System.assertEquals(null, opportunity.CORE_OPP_Application_Online_ID__c);
        System.assertEquals(null, opportunity.CORE_OPP_Legacy_CRM_Id__c);
        System.assertEquals(null, opportunity.CORE_OPP_SIS_Opportunity_Id__c);
        System.assertEquals(null, opportunity.CORE_OPP_SIS_Student_Academic_Record_ID__c);
        System.assertEquals(null, opportunity.CORE_Tuition_fee_Transaction_ID__c);
    }

    @IsTest
    private static void isAolExternalIdParameterMissing_should_return_true_if_parameter_isEmpty(){
        Boolean result = CORE_AOL_GlobalWorker.isAolExternalIdParameterMissing(null);

        System.assertEquals(true, result);

        result = CORE_AOL_GlobalWorker.isAolExternalIdParameterMissing(' ');

        System.assertEquals(true, result);
    }

    @IsTest
    private static void isAolExternalIdParameterMissing_should_return_false_if_parameter_is_not_empty(){
        Boolean result = CORE_AOL_GlobalWorker.isAolExternalIdParameterMissing('test');

        System.assertEquals(false, result);
    }

}