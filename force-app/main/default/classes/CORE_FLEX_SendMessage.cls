public class CORE_FLEX_SendMessage {

  public class Option {
    @AuraEnabled public String label;
    @AuraEnabled public String value;
    @AuraEnabled public boolean isMobile;

    public Option(String value, String label, boolean isMobile) {
      this.value = value;
      this.label = label==null?value:label;
      this.isMobile = isMobile;
    }

  }

  public class Phones {
    @AuraEnabled public List<Option> CustomerPhones;
    @AuraEnabled public List<Option> TwilioPhones;
    @AuraEnabled public string Name;
  }


  @AuraEnabled(cacheable=false)
  public static Phones loadPhones(Id recordId) {
    Phones phones = new Phones();
    phones.CustomerPhones = new List<Option>();
    phones.TwilioPhones = new List<Option>();

    Account[] accounts = [SELECT Name, Phone, CORE_Phone2__pc, PersonMobilePhone, CORE_Mobile_2__pc, CORE_Mobile_3__pc FROM Account WHERE Id=:recordId];
    Account account;
    if(accounts.size()==1) {
      account = accounts[0];
      phones.Name = account.Name;
    } else {
      Opportunity[] opportunities = [SELECT AccountId FROM Opportunity WHERE Id=:recordId];
      if(opportunities.size()==1) {
        Opportunity opportunity = opportunities[0];
        account = [SELECT Name,Phone, CORE_Phone2__pc, PersonMobilePhone, CORE_Mobile_2__pc, CORE_Mobile_3__pc FROM Account WHERE Id=:opportunity.AccountId];
        phones.Name = account.Name;
      } else {
        return phones;
      }
    }

    //addPhone('All', phones.CustomerPhones, false);
    addPhone(account.Phone, phones.CustomerPhones, false);
    addPhone(account.CORE_Phone2__pc, phones.CustomerPhones, false);
    addPhone(account.PersonMobilePhone, phones.CustomerPhones, true);
    addPhone(account.CORE_Mobile_2__pc, phones.CustomerPhones, true);
    addPhone(account.CORE_Mobile_3__pc, phones.CustomerPhones, true);

    //addPhone('All', phones.TwilioPhones, false);
    phones.TwilioPhones.addAll(getTwilioPhones());
    return phones;
  }

  private static void addPhone(string phone, List<Option> phones, boolean isMobile) {
    if(phone!=null) {
      phones.add(new Option(phone,null, isMobile));
    }
  }

  private static List<Option> getTwilioPhones() {
    String userId = UserInfo.getUserId();        
    User user = [SELECT Email FROM User WHERE Id=:userId];
    String email = user.Email;
    system.debug('email : '+email);

    string pathname = '/sfdc/getPhones';

    HttpRequest req = CORE_FLEX_Service.createRequestTwilio(pathname);

    JSONGenerator gen = JSON.createGenerator(true);    
    gen.writeStartObject();
    gen.writeStringField('emailWorker', email);
    gen.writeEndObject();    
    String jsonS = gen.getAsString();

    req.setBody(jsonS);

    try {
      Http http = new Http();
      HTTPResponse res = http.send(req);

      String resultS = res.getBody();
      system.debug('Result : '+resultS+', status:'+res.getStatusCode());
      if(res.getStatusCode()==200) {
        return (List<Option>)JSON.deserialize(resultS,List<Option>.class);
      } else {
        return null;
      }
    } catch(Exception e) {
      System.debug('Unable to send sms, exception:'+e);
      return null;
    }        
  }

  @AuraEnabled(cacheable=false)
  public static String sendFlexSms(String customerPhone, String twilioPhone, String recordId) {
    String userId = UserInfo.getUserId();        

    system.debug('start send sms with userId : '+userId+', to :'+customerPhone+' using : '+twilioPhone+', recordId:'+recordId);

    if(customerPhone==null || customerPhone=='') {
      return 'KO';
    }


    User user = [SELECT Email FROM User WHERE Id=:userId];
    String email = user.Email;
    system.debug('email : '+email);

    string pathname = '/sfdc/message/smsOutbound';

    HttpRequest req = CORE_FLEX_Service.createRequestTwilio(pathname);

    string name = 'no name account';
    string opportunityId;
    string accountId;
    Account[] accounts = [SELECT Name FROM Account WHERE Id=:recordId];
    if(accounts.Size()>0) {
      name = accounts[0].Name;
      accountId = recordId;
    } else {
      Opportunity opportunity = [SELECT AccountId FROM Opportunity WHERE Id=:recordId];
      Account account = [SELECT Name FROM Account WHERE Id=:opportunity.AccountId];
      name = account.Name;
      opportunityId = recordId;
    }

    JSONGenerator gen = JSON.createGenerator(true);    
    gen.writeStartObject();
    gen.writeStringField('phone', customerPhone);
    gen.writeStringField('twilioPhone', twilioPhone);
    gen.writeStringField('emailWorker', email);
    gen.writeStringField('customerName', name);
    if(opportunityId!=null)
    gen.writeStringField('opportunityId', opportunityId);
    if(accountId!=null)
    gen.writeStringField('accountId', accountId);
    gen.writeEndObject();    
    String jsonS = gen.getAsString();

    req.setBody(jsonS);

    try {
      Http http = new Http();
      HTTPResponse res = http.send(req);

      String resultS = res.getBody();
      system.debug('Result : '+resultS+', status:'+res.getStatusCode());
      if(res.getStatusCode()==200) {
        return resultS;
      } else {
        return 'KO';
      }
    } catch(Exception e) {
      System.debug('Unable to send sms, exception:'+e);
      return 'KO';
    }
  }


  @AuraEnabled(cacheable=false)
  public static String sendFlexWhatsapp(String customerPhone, String twilioPhone, String recordId) {
    String userId = UserInfo.getUserId();        

    system.debug('start send whatsapp with userId : '+userId+', to :'+customerPhone+' using : '+twilioPhone+', recordId:'+recordId);

    if(customerPhone==null || customerPhone=='') {
      return 'KO';
    }


    User user = [SELECT Email FROM User WHERE Id=:userId];
    String email = user.Email;
    system.debug('email : '+email);

    string pathname = '/sfdc/message/whatsappOutbound';


    HttpRequest req = CORE_FLEX_Service.createRequestTwilio(pathname);

    string name = 'no name account';
    string opportunityId;
    string accountId;        
    Account[] accounts = [SELECT Name FROM Account WHERE Id=:recordId];
    if(accounts.Size()>0) {
      name = accounts[0].Name;
      accountId =recordId;
    } else {
      Opportunity opportunity = [SELECT AccountId FROM Opportunity WHERE Id=:recordId];
      Account account = [SELECT Name FROM Account WHERE Id=:opportunity.AccountId];
      name = account.Name;
      opportunityId = recordId;
    }        


    RequestInput reqInput = new RequestInput();
    reqInput.contact = new Contact();
    reqInput.agent = new Agent();
    reqInput.contact.phone=customerPhone;
    reqInput.contact.name=name;
    if(accountId!=null)
    reqInput.contact.accountId=accountId;
    if(opportunityId!=null)
    reqInput.contact.opportunityId=opportunityId;
    reqInput.agent.email=email;
    reqInput.agent.phone=twilioPhone;
    String jsonS = JSON.serialize(reqInput);

    req.setBody(jsonS);

    try {
      Http http = new Http();
      HTTPResponse res = http.send(req);

      String resultS = res.getBody();            
      system.debug('Result : '+resultS+', status:'+res.getStatusCode());
      if(res.getStatusCode()==200) {
        return resultS;
      } else {
        return 'KO';
      }            
    } catch(Exception e) {
      System.debug('Unable to send sms, exception:'+e);
      return 'KO';
    }
  }

  @AuraEnabled(cacheable=false)
  public static Map<String,Boolean> hide() {
    User user = [SELECT CORE_FLEX_Twilio_SMS__c,CORE_FLEX_Twilio_Whatsapp__c FROM User WHERE Id=:UserInfo.getUserId()];
    Map<String, Boolean> myMap = new Map<String,Boolean>();
    myMap.put('hideSms',!user.CORE_FLEX_Twilio_SMS__c);
    myMap.put('hideWhatsapp',!user.CORE_FLEX_Twilio_Whatsapp__c);

    return myMap;
  }


  class RequestInput {
    public Contact contact;
    public Agent agent;
  }

  class Contact {
    public string phone;
    public string name;
    public string accountId;
    public string opportunityId;
  }
  class Agent {
    public string email;
    public string phone;
  }

}