/**
 * Created by Gabriele Vitanza on 15/11/2021.
 */

@IsTest
private class IT_ExtInfoComponentControllerTest {
    @isTest
    static void getPageLayoutMetadata_emptyPageLayoutNameTest() {
        List<IT_ExtendedInfo__c> extInfoList = new List<IT_ExtendedInfo__c>();
        IT_ExtendedInfo__c extInfo = new IT_ExtendedInfo__c(Name = 'Test');
        extInfoList.add(extInfo);
        insert extInfoList;

        IT_ExtInformationsComponentController.getExtInformationsIds(extInfo.Id);
        IT_ExtInformationsComponentController.PageLayout pageLayout = IT_ExtInformationsComponentController.getFieldsByLayout('IT_ExtendedInfo__c-Extended Information Layout');
        IT_ExtInformationsComponentController.getLabelsFields();
        IT_ExtInformationsComponentController.getRecordsUserHasAccess(extInfoList);
        IT_ExtInformationsComponentController.getWrapperMethod();

        assertEmptyPageLayout(pageLayout);
    }

    static void assertEmptyPageLayout(IT_ExtInformationsComponentController.PageLayout pageLayout) {
        system.assert(pageLayout != null, 'The page layout should not be null.');
        system.assert(pageLayout.Sections != null, 'The page layout\'s sections should not be null.');
    }
}