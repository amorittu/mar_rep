@IsTest
global class CORE_Livestorm_DeleteSessionTest {
    @TestSetup
    public static void makeData(){
        CORE_Livestorm_DataFaker_InitiateTest.CreateMetadata();
    }

    @IsTest
    public static void fullSuccessExecuteTest() {  
        Test.setMock(HttpCalloutMock.class, new FullSuccessUpdateCampaignMock());
        List<CORE_Business_Unit__c> bus = [Select Id FROM CORE_Business_Unit__c];
        Campaign campaignCreated = CORE_DataFaker_Campaign.getCampaign('', true, 'Planned', bus.get(0).Id, null, date.today().addDays(1), date.today().addDays(2));
  
        Campaign campaignInit2 = [SELECT Id, status FROM Campaign LIMIT 1];
        campaignInit2.status = 'Aborted';

        Test.startTest();
        update campaignInit2;
        CORE_Livestorm_DeleteSession campaignBatchableDeleted = new CORE_Livestorm_DeleteSession(new Set<Id> {campaignInit2.Id});
        database.executebatch(campaignBatchableDeleted, 25);
        Test.stopTest();

        List<Campaign> campaignsUpdated = [SELECT Id, CORE_Is_Up_to_Date_With_Livestorm__c FROM Campaign];
        System.assertEquals(1, campaignsUpdated.size());

        Campaign campaign = campaignsUpdated.get(0);
        //System.assertEquals(true, campaign.CORE_Is_Up_to_Date_With_Livestorm__c);

        
        List<AsyncApexJob> jobsApexBatch = [select Id, ApexClassID, ApexClass.Name, Status, JobType from AsyncApexJob where JobType = 'BatchApex'];
        //System.assertEquals(2, jobsApexBatch.size(), 'expecting two apex batch job');
    }

    global class FullSuccessUpdateCampaignMock implements HttpCalloutMock {
        global HttpResponse respond(HttpRequest req) {
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');

            if(req.getEndpoint().contains('sessions')){
                res.setStatusCode(204);
            }

            return res;
        }
    }
    
    @IsTest
    public static void fullFailExecuteTest() {  
        Test.setMock(HttpCalloutMock.class, new FullFailUpdateCampaignMock());
        List<CORE_Business_Unit__c> bus = [Select Id FROM CORE_Business_Unit__c];
        Campaign campaignCreated = CORE_DataFaker_Campaign.getCampaign('TestBen', true, 'Planned', bus.get(0).Id, null, date.today().addDays(1), date.today().addDays(2));
  
        Campaign campaign = [SELECT Id, status, Description FROM Campaign LIMIT 1];
        campaign.Status = 'Aborted';

        Test.startTest();
        update campaign;
        CORE_Livestorm_DeleteSession campaignBatchableDeleted = new CORE_Livestorm_DeleteSession(new Set<Id> {campaign.Id});
        database.executebatch(campaignBatchableDeleted, 25);
        Test.stopTest();

        List<Campaign> campaignsUpdated = [SELECT Id, CORE_Is_Up_to_Date_With_Livestorm__c FROM Campaign];
        System.assertEquals(1, campaignsUpdated.size());
                
        Campaign campaignToTest = campaignsUpdated.get(0);
        System.assertEquals(false, campaignToTest.CORE_Is_Up_to_Date_With_Livestorm__c);
        
        List<AsyncApexJob> jobsApexBatch = [select Id, ApexClassID, ApexClass.Name, Status, JobType from AsyncApexJob where JobType = 'BatchApex'];
        //System.assertEquals(1, jobsApexBatch.size(), 'expecting one apex batch job');
    }

    
    global class FullFailUpdateCampaignMock implements HttpCalloutMock {
        global HttpResponse respond(HttpRequest req) {
            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');

            if(req.getEndpoint().contains('sessions')){
                res.setStatusCode(202);
            }

            return res;
        }
    }
}