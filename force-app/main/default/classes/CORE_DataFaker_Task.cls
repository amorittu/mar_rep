@IsTest
public class CORE_DataFaker_Task {
    private static final String MASTER_RECORDTYPE = 'Master';

    public static Task getMasterTask(String uniqueKey, String status, Id whatId){
        Task task = getMasterTaskithoutInsert(uniqueKey,status, whatId);

        Insert task;
        return task;
    }

    public static Task getMasterTaskithoutInsert(String uniqueKey, String status, Id whatId){
        Id recordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByDeveloperName().get(MASTER_RECORDTYPE).getRecordTypeId();

        Task task = new Task(
            Status = status,
            Subject = 'subjectTest' + ' - ' + uniqueKey,
            Description = 'descriptioTest' + ' - ' + uniqueKey,
            TaskSubtype = 'Task',
            WhatId = whatId
        );
        return task;
    }
}