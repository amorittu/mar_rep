public class CORE_LFAOnlineShopProcess extends CORE_LeadFormAcquisitionProcess{
    public CORE_LFAOnlineShopProcess(DateTime apiCallDate,CORE_LeadFormAcquisitionWrapper.GlobalWrapper datas) {
        super(apiCallDate,datas);
    }
    
    public override void execute(){
        System.debug('CORE_LeadFormAcquisitionOnlineShopProcess.execute() : START');
        Long dt1 = DateTime.now().getTime();
        Long dt2,dt3;
        Savepoint sp = Database.setSavepoint();

        CORE_LeadFormAcquistionInputValidator inputValidator = new CORE_LeadFormAcquistionInputValidator(this.datas, this.apiCallDate, this.countryPicklistIsActivated);
        
        this.errors = inputValidator.getFieldsErrors();

        if(this.errors.size() > 0){
            dt2 = DateTime.now().getTime();
            System.debug('CORE_LeadFormAcquisitionOnlineShopProcess.execute() time execution = ' + (dt2-dt1) + ' ms');

            System.debug('Missing parameters => end execute function');
            System.debug('CORE_LeadFormAcquisitionOnlineShopProcess.execute() : END');
            return;
        }

        this.availableAcademicYears = inputValidator.pklOppAcademicYear;
        //upsert account part : START ================
        dt3 = DateTime.now().getTime();
        List<Account> existingAccounts = CORE_LeadFormAcquisitionUtils.getExestingAccounts(datas.personAccount);
        dt2 = DateTime.now().getTime();

        boolean oneExistingAccount = existingAccounts.size() == 1;

        System.debug('CORE_LeadFormAcquisitionOnlineShopProcess.execute() -> existingAccounts() time execution = ' + (dt2-dt3) + ' ms');

        dt3 = DateTime.now().getTime();
        Account account;
        try{
            account = getAccount(existingAccounts);
            this.result.account.id = account.id;
        } catch (Exception ex){
            Database.rollback(sp);
            throw ex;
        }
        dt2 = DateTime.now().getTime();

        System.debug('CORE_LeadFormAcquisitionOnlineShopProcess.execute() -> getAccount() time execution = ' + (dt2-dt3) + ' ms');
        //upsert account part : END ===================

        //Opportunity part : START ================
        List<Opportunity> opportunities = new List<Opportunity>();
        List<Opportunity> mainOpportunities = new List<Opportunity>();
        
        dt3 = DateTime.now().getTime();
        try{
            opportunities = getOpportunities(account);
            if(opportunities.size() == 1){
                Opportunity opportunity = opportunities.get(0);
                this.result.opportunities.mainInterest.id = opportunity.Id;
            }
        } catch (Exception ex){
            Database.rollback(sp);
            throw ex;
        }

        dt2 = DateTime.now().getTime();
        System.debug('CORE_LeadFormAcquisitionOnlineShopProcess.execute() -> Opportunity time execution = ' + (dt2-dt3) + ' ms');

        //opportunity line items : START ==================
        List<OpportunityLineItem> oppLineItems = new List<OpportunityLineItem>();
        
        try{
            oppLineItems = getOppLineItems(opportunities);
        } catch (Exception ex){
            Database.rollback(sp);
            throw ex;
        }

        dt3 = DateTime.now().getTime();
        System.debug('CORE_LeadFormAcquisitionOnlineShopProcess.execute() -> OpportunityLineItem time execution = ' + (dt3-dt2) + ' ms');
        //opportunity line items : END ==================

        //Point of contacts part : START ==================

        List<CORE_Point_of_Contact__c> pocs = dataMaker.initPocFromAPI(opportunities, this.datas.pointOfContact);
        
        try{    
            insert pocs;

            for(CORE_Point_of_Contact__c poc : pocs){
                CORE_LeadFormAcquisitionWrapper.ResultPointOfContactWrapper pocWrapper = new CORE_LeadFormAcquisitionWrapper.ResultPointOfContactWrapper();
                pocWrapper.id = poc.Id;
                this.result.pointOfContacts.add(pocWrapper);
            }
        } catch (Exception ex){
            Database.rollback(sp);
            throw ex;
        }
        dt2 = DateTime.now().getTime();
        System.debug('CORE_LeadFormAcquisitionOnlineShopProcess.execute() -> PointOfContact process time execution = ' + (dt2-dt3) + ' ms');

        //Point of contacts part : END ==================

        //tasks part : START ==============
        initRelatedTasks(opportunities, pocs, account);
        try{    
            insert tasksToCreate;
        } catch (Exception ex){
            Database.rollback(sp);
            throw ex;
        }
        //tasks part : END ==============


        //Consents part : Start ================== 
        List<CORE_Consent__c> consents = getConsents(account);

        try{
            upsert consents CORE_Consent__c.fields.CORE_TECH_InternalReference__c;

            for(CORE_Consent__c consent : consents){
                CORE_LeadFormAcquisitionWrapper.ResultConsentWrapper consentWrapper = new CORE_LeadFormAcquisitionWrapper.ResultConsentWrapper();
                consentWrapper.id = consent.Id;
                this.result.consents.add(consentWrapper);
            }
        } catch (Exception ex){
            Database.rollback(sp);
            throw ex;
        }

        //Consents part : END ================== 
        dt3 = DateTime.now().getTime();
        System.debug('CORE_LeadFormAcquisitionOnlineShopProcess.execute() -> consents process time execution = ' + (dt3-dt2) + ' ms');

        //AcademicDiplomaHistory part : Start ================== 
        CORE_Academic_Diploma_History__c academicDiplomaHistory = getacAdemicDiplomaHistory(account);

        try{
            if(academicDiplomaHistory != null){
                insert academicDiplomaHistory;
            }
        } catch (Exception ex){
            Database.rollback(sp);
            throw ex;
        }

        //AcademicDiplomaHistory part : END ================== 
        dt3 = DateTime.now().getTime();
        System.debug('CORE_LeadFormAcquisitionOnlineShopProcess.execute() -> AcademicDiplomaHistory process time execution = ' + (dt2-dt3) + ' ms');

        System.debug('CORE_LeadFormAcquisitionOnlineShopProcess.execute() : END. [ExecutionTime = ' + (dt2-dt1) + ' ms]');
    }

    public override List<Opportunity> getOpportunities(Account account){
        long dt1 = DateTime.now().getTime();
        long dt2 = DateTime.now().getTime();
        long dt3 = DateTime.now().getTime();
        List<Opportunity> opportunities = new List<Opportunity>();
        Set<Id> existingOpportunities = new Set<Id>();
        Set<String> onlineShopIds = new Set<String>();

        List<CORE_LeadFormAcquisitionWrapper.OpportunityWrapper> opportunitiesWrapped= new List<CORE_LeadFormAcquisitionWrapper.OpportunityWrapper>();
        Map<Integer,CORE_LeadFormAcquisitionWrapper.OpportunityWrapper> mapOppWrapped = new Map<Integer,CORE_LeadFormAcquisitionWrapper.OpportunityWrapper>();
        Integer currentOppWrapper = 0;

        CORE_LeadFormAcquisitionWrapper.OpportunityWrapper currentInterest;

        currentInterest = this.datas.mainInterest;

        //currentOppWrapper = 0;
        
        Map<Integer,CORE_CatalogHierarchyModel> catalogHierarchies = new Map<Integer,CORE_CatalogHierarchyModel>();
        List<CORE_Business_Unit__c> businessUnits = new List<CORE_Business_Unit__c>();

        CORE_CatalogHierarchyModel catalogHierarchy = new CORE_CatalogHierarchyModel(
            currentInterest.division, 
            currentInterest.businessUnit, 
            currentInterest.studyArea, 
            currentInterest.curriculum, 
            currentInterest.level, 
            currentInterest.intake
        );

        if(!String.isBlank(currentInterest.product)){
            List<Product2> products = [SELECT Id FROM Product2 WHERE SIS_External_Id__c = :currentInterest.product];
            if(!products.isEmpty()){
                catalogHierarchy.product.Id= products.get(0).Id;
            } 
        }

        if(!String.isEmpty(currentInterest.onlineShopId)){
            onlineShopIds.add(currentInterest.onlineShopId);
        }
        businessUnits.add(catalogHierarchy.businessUnit);
        catalogHierarchies.put(currentOppWrapper, catalogHierarchy);            
    

        catalogHierarchies = CORE_OpportunityDataMaker.getCatalogHierarchies(catalogHierarchies);

        dt2 = DateTime.now().getTime();
        system.debug('catalogHierarchy & map init [ExecutionTime = ' + (dt2-dt1) + ' ms]');

        List<Opportunity> onlineShopOpportunities = CORE_OpportunityUtils.getExistingOnlineShopOpportunities(onlineShopIds);
        List<Opportunity> mainOpportunities = getExistingMainOpportunities(businessUnits, account.Id);

        Map<String,Opportunity> opportunnitiesByOnlineShopIds = new Map<String, Opportunity>();
        if(!onlineShopOpportunities.isEmpty()){
            for(Opportunity opportunity : onlineShopOpportunities){
                opportunnitiesByOnlineShopIds.put(opportunity.CORE_OPP_OnlineShop_Id__c, opportunity);
            }
        }

        System.debug('mainOpportunities = '+ mainOpportunities);

        Map<String,List<Opportunity>> mainOpportunitiesByBu = new Map<String,List<Opportunity>>();

        dt1 = DateTime.now().getTime();
        for(Opportunity opp : mainOpportunities){
            System.debug('opp.CORE_OPP_Business_Unit__r.CORE_Business_Unit_ExternalId__c = '+ opp.CORE_OPP_Business_Unit__r.CORE_Business_Unit_ExternalId__c);
            System.debug('currentInterest.businessUnit = '+ currentInterest.businessUnit);
            String bu = opp.CORE_OPP_Business_Unit__r.CORE_Business_Unit_ExternalId__c;
            List<Opportunity> mainOpportunitiesForThisBu = mainOpportunitiesByBu.get(bu);
            if(mainOpportunitiesForThisBu == null){
                mainOpportunitiesForThisBu = new List<Opportunity>();
            }

            mainOpportunitiesForThisBu.add(opp);
            mainOpportunitiesByBu.put(bu,mainOpportunitiesForThisBu);
        }

        System.debug('mainOpportunitiesByBu = ' + mainOpportunitiesByBu);

        catalogHierarchy.interest = currentInterest;

        if(!String.isEmpty(catalogHierarchy.invalidReference)){
            String interestError = 'Main interest';
            string errorMsg = interestError + ': ' + catalogHierarchy.invalidReference;

            CORE_LeadFormAcquisitionWrapper.ErrorWrapper error = new CORE_LeadFormAcquisitionWrapper.ErrorWrapper(errorMsg,'INVALID_REFERENCE',this.apiCallDate);
            this.errors.add(error);

            throw new IllegalArgumentException(errorMsg);
        }

        catalogHierarchy.isSuspect = currentInterest.isSuspect == null ? false : currentInterest.isSuspect;

        String finalAcademicYear = CORE_OpportunityUtils.getAcademicYear(
            dataMaker.getDefaultAcademicYear(),
            currentInterest.academicYear, 
            catalogHierarchy, 
            account.CreatedDate, 
            this.availableAcademicYears
        );

        dt1 = DateTime.now().getTime();

        Boolean mainOnSameAcademicYear = false;
        Opportunity mainOpportunity;

        String bu = currentInterest.businessUnit;
        List<Opportunity> mainOpportunitiesForThisBu = mainOpportunitiesByBu.get(bu);
        system.debug('mainOpportunitiesForThisBu['+ bu +'] =' + mainOpportunitiesForThisBu);
        Opportunity opportunityToUpsert;

        if(!String.isEmpty(currentInterest.onlineShopId)){
            mainOpportunity = opportunnitiesByOnlineShopIds.get(currentInterest.onlineShopId);
            opportunityToUpsert = this.dataMaker.initOnlineShopOpportunity(account, finalAcademicYear, catalogHierarchy, mainOpportunity);
        }
        system.debug('mainOpportunity1 =' + mainOpportunity);
        system.debug('finalAcademicYear =' + finalAcademicYear);

        if(mainOpportunity == null){
            //existing main for this bu
            if(mainOpportunitiesForThisBu != null && mainOpportunitiesForThisBu.size() > 0){
                mainOpportunity = mainOpportunitiesForThisBu.get(0);

                for(Opportunity opp : mainOpportunitiesForThisBu){
                    
                    if(opp.CORE_OPP_Academic_Year__c == finalAcademicYear){
                        mainOnSameAcademicYear = true;
                        mainOpportunity = opp;
                        break;
                    }
                }
            }
            system.debug('mainOpportunity2 =' + mainOpportunity);

            dt2 = DateTime.now().getTime();
            system.debug('main opp on academic yead [ExecutionTime = ' + (dt2-dt1) + ' ms]');
            
            opportunityToUpsert = this.dataMaker.initOnlineShopOpportunity(account, finalAcademicYear, catalogHierarchy, mainOpportunity);
            
        }

        if(currentInterest.applicationFormShippedByMail != null){
            opportunityToUpsert.CORE_Application_form_shipped_by_mail__c = currentInterest.applicationFormShippedByMail;
        }

        if(currentInterest.brochureShippedByMail != null){
            opportunityToUpsert.CORE_Brochure_shipped_by_mail__c = currentInterest.brochureShippedByMail;
        }

        
        system.debug('opp = ' + opportunityToUpsert);
        system.debug('mainOnSameAcademicYear = ' + mainOnSameAcademicYear);
        system.debug('catalogHierarchy = ' + catalogHierarchy);

        if(opportunityToUpsert.Id != null){
            Task task = CORE_OpportunityUtils.getReturningTask(catalogHierarchy, this.datas.prospectiveStudentComment, opportunityToUpsert, account);
            
            System.debug('task added = ' + task);

            tasksToCreate.add(task);
        } else{
            opportunityToUpsert.CORE_Questions__c = CORE_OpportunityUtils.getShortQuestionForOpportunity(this.datas.prospectiveStudentComment);
            opportunityToUpsert.CORE_OPP_Learning_Material__c = currentInterest.learningMaterial;
            opportunityToUpsert.CORE_Lead_Source_External_Id__c = currentInterest.leadSourceExternalId;
            opportunityToUpsert.CORE_OPP_Application_Online_ID__c = currentInterest.applicationOnlineId;
            opportunityToUpsert.CORE_OPP_Funding__c = currentInterest.funding;
        }

        if(opportunities.isEmpty()){
            opportunities.add(opportunityToUpsert);
        } else if(String.IsEmpty(opportunityToUpsert.Id) || !existingOpportunities.contains(opportunityToUpsert.Id)){
            opportunities.add(opportunityToUpsert);
        }
        if(opportunityToUpsert.Id != null){
            existingOpportunities.add(opportunityToUpsert.Id);
        }

        //check for main Opportunities
        dt1 = DateTime.now().getTime();
        upsert opportunities;
        
        dt2 = DateTime.now().getTime();

        system.debug('Insert opportunities [ExecutionTime = ' + (dt2-dt1) + ' ms]');
        return opportunities;
    }

    public override List<OpportunityLineItem> getOppLineItems(List<Opportunity> opportunities){
        List<OpportunityLineItem> oppLineItems = new List<OpportunityLineItem>();

        Map<Id,Opportunity> oppsMap = new Map<ID, Opportunity>(opportunities);
        System.debug('oppsMap= ' + oppsMap);

        Map<Id,Id> buByProducts = new Map<Id,Id>();
        Map<Id,Id> productsByOpportunities = new Map<Id,Id>();
        opportunities = [Select Id,CORE_Main_Product_Interest__c, CORE_OPP_Business_Unit__r.Id, AccountId FROM Opportunity where id in :oppsMap.keySet()];
        System.debug('opportunities= ' + opportunities);

        for(Integer i = 0 ; i < opportunities.size(); i++){
            Opportunity opportunity = opportunities.get(i);
            
            if(!String.IsBlank(opportunity.CORE_Main_Product_Interest__c) && !String.IsBlank(opportunity.CORE_OPP_Business_Unit__c)){
                buByProducts.put(opportunity.CORE_Main_Product_Interest__c,opportunity.CORE_OPP_Business_Unit__c);
                productsByOpportunities.put(opportunity.Id, opportunity.CORE_Main_Product_Interest__c);
            }
        }
        System.debug('productsByOpportunities.keySet().size() = ' + productsByOpportunities.keySet().size());
        //no need to continue because there is no products
        if(productsByOpportunities.keySet().size() == 0){
            return oppLineItems;
        }

        Map<Id,PricebookEntry> pricebookEntries = this.dataMaker.getPricebookEntryByProducts(buByProducts);
        System.debug('pricebookEntries = ' + pricebookEntries);

        for(Id opportunityId : productsByOpportunities.keySet()){
            Opportunity currentOpportunity = oppsMap.get(opportunityId);
            System.debug('currentOpportunity = ' + currentOpportunity);
            System.debug('currentOpportunity.CORE_ReturningLead__c = ' + currentOpportunity.CORE_ReturningLead__c);

            if(currentOpportunity.CORE_ReturningLead__c == null || currentOpportunity.CORE_ReturningLead__c == true){
                break;
            }
            PricebookEntry pricebookEntry = pricebookEntries.get(productsByOpportunities.get(opportunityId));
            System.debug('pricebookEntry = ' + pricebookEntry);

            if(pricebookEntry != NULL){
                OpportunityLineItem oppLineItem = this.dataMaker.initOppLineItem(oppsMap.get(opportunityId), pricebookEntry);
                oppLineItem.Quantity = this.datas.mainInterest.quantity != null ? this.datas.mainInterest.quantity : oppLineItem.Quantity;
                oppLineItem.UnitPrice = this.datas.mainInterest.salesPrice != null ? this.datas.mainInterest.salesPrice : oppLineItem.UnitPrice;
                        
                oppLineItems.add(oppLineItem);
            }
        }

        if(oppLineItems.size() > 0){
            insert oppLineItems;
        }
        

        return oppLineItems;
    }
}