@IsTest
public with sharing class CORE_AOL_AcademicDiplomaHistoryTest {
    private static final String AOL_EXTERNALID = 'setupAol';

    private static final String REQ_BODY = '{'
    + '"aolExternalId": "' + AOL_EXTERNALID + '",'
    + '"academicDiplomaHistory": ['
    + '{'
    + '"diplomaExternalId": "testExtId1",'
    + '"diplomaName": "testName1",'
    + '"diplomaLevel": "CORE_PKL_Apprenticeship",'
    + '"diplomaSchool": "School",'
    + '"diplomaCity": "Paris",'
    + '"diplomaCountry": "FRA",'
    + '"diplomaStatus": "CORE_PKL_In_Progress",'
    + '"diplomaExpectedGraduationDate": "2022-03-30",'
    + '"diplomaModificationDate": "2022-03-30",'
    + '"diplomaDate": "2022-03-30",'
    + '"additionalFields": {'
    + '"CORE_Commercial_Name__c": "test value 1"'
    + '}'
    + '},'
    + '{'
    + '"diplomaExternalId": "testExtId2",'
    + '"diplomaName": "testName2",'
    + '"diplomaLevel": "CORE_PKL_Apprenticeship",'
    + '"diplomaSchool": "School",'
    + '"diplomaCity": "Paris",'
    + '"diplomaCountry": "FRA",'
    + '"diplomaStatus": "CORE_PKL_In_Progress",'
    + '"diplomaExpectedGraduationDate": "2022-03-30",'
    + '"diplomaModificationDate": "2022-03-30",'
    + '"diplomaDate": "2022-03-30",'
    + '"additionalFields": {'
    + '"CORE_Commercial_Name__c": "test value 2"'
    + '}'
    + '}'
    + '],'
    + '"aolProgressionFields": {'
    + '"aolProgression": 50.09,'
    + '"documentProgression": "Started",'
    + '"firstDocumentUploadDate": "2022-03-29",'
    + '"lastAolEventDate": "2022-03-29T08:47:39.689Z",'
    + '"lastDeaAolLoginDate": "2022-03-29T08:47:39.689Z",'
    + '"isAol": true,'
    + '"isAolOnlineRegistered": true,'
    + '"isAolRetailRegistered": true'
    + '}'
    + '}';

    private static final String REQ_BODY_ERROR_NO_ID = '{'
    + '"aolExternalId": "",'
    + '"academicDiplomaHistory": ['
    + '{'
    + '"diplomaExternalId": "testExtId1",'
    + '"diplomaName": "testName1",'
    + '"diplomaLevel": "CORE_PKL_Apprenticeship",'
    + '"diplomaSchool": "School",'
    + '"diplomaCity": "Paris",'
    + '"diplomaCountry": "FRA",'
    + '"diplomaStatus": "CORE_PKL_In_Progress",'
    + '"diplomaExpectedGraduationDate": "2022-03-30",'
    + '"diplomaModificationDate": "2022-03-30",'
    + '"diplomaDate": "2022-03-30",'
    + '"additionalFields": {'
    + '"CORE_Commercial_Name__c": "test value 1"'
    + '}'
    + '},'
    + '{'
    + '"diplomaExternalId": "testExtId2",'
    + '"diplomaName": "testName2",'
    + '"diplomaLevel": "CORE_PKL_Apprenticeship",'
    + '"diplomaSchool": "School",'
    + '"diplomaCity": "Paris",'
    + '"diplomaCountry": "FRA",'
    + '"diplomaStatus": "CORE_PKL_In_Progress",'
    + '"diplomaExpectedGraduationDate": "2022-03-30",'
    + '"diplomaModificationDate": "2022-03-30",'
    + '"diplomaDate": "2022-03-30",'
    + '"additionalFields": {'
    + '"CORE_Commercial_Name__c": "test value 2"'
    + '}'
    + '}'
    + '],'
    + '"aolProgressionFields": {'
    + '"aolProgression": 50.09,'
    + '"documentProgression": "Started",'
    + '"firstDocumentUploadDate": "2022-03-29",'
    + '"lastAolEventDate": "2022-03-29T08:47:39.689Z",'
    + '"lastDeaAolLoginDate": "2022-03-29T08:47:39.689Z",'
    + '"isAol": true,'
    + '"isAolOnlineRegistered": true,'
    + '"isAolRetailRegistered": true'
    + '}'
    + '}';

    private static final String REQ_BODY_ERROR_INVALID_ID = '{'
    + '"aolExternalId": "invalidID",'
    + '"academicDiplomaHistory": ['
    + '{'
    + '"diplomaExternalId": "testExtId1",'
    + '"diplomaName": "testName1",'
    + '"diplomaLevel": "CORE_PKL_Apprenticeship",'
    + '"diplomaSchool": "School",'
    + '"diplomaCity": "Paris",'
    + '"diplomaCountry": "FRA",'
    + '"diplomaStatus": "CORE_PKL_In_Progress",'
    + '"diplomaExpectedGraduationDate": "2022-03-30",'
    + '"diplomaModificationDate": "2022-03-30",'
    + '"diplomaDate": "2022-03-30",'
    + '"additionalFields": {'
    + '"CORE_Commercial_Name__c": "test value 1"'
    + '}'
    + '},'
    + '{'
    + '"diplomaExternalId": "testExtId2",'
    + '"diplomaName": "testName2",'
    + '"diplomaLevel": "CORE_PKL_Apprenticeship",'
    + '"diplomaSchool": "School",'
    + '"diplomaCity": "Paris",'
    + '"diplomaCountry": "FRA",'
    + '"diplomaStatus": "CORE_PKL_In_Progress",'
    + '"diplomaExpectedGraduationDate": "2022-03-30",'
    + '"diplomaModificationDate": "2022-03-30",'
    + '"diplomaDate": "2022-03-30",'
    + '"additionalFields": {'
    + '"CORE_Commercial_Name__c": "test value 2"'
    + '}'
    + '}'
    + '],'
    + '"aolProgressionFields": {'
    + '"aolProgression": 50.09,'
    + '"documentProgression": "Started",'
    + '"firstDocumentUploadDate": "2022-03-29",'
    + '"lastAolEventDate": "2022-03-29T08:47:39.689Z",'
    + '"lastDeaAolLoginDate": "2022-03-29T08:47:39.689Z",'
    + '"isAol": true,'
    + '"isAolOnlineRegistered": true,'
    + '"isAolRetailRegistered": true'
    + '}'
    + '}';

    private static final String REQ_BODY_ERROR = '{'
    + '"aolExternalId": "' + AOL_EXTERNALID + '",'
    + '"academicDiplomaHistory": ['
    + '{'
    + '"diplomaExternalId": "testExtId1",'
    + '"diplomaName": "testName1",'
    + '"diplomaLevel": "invalid_value",'
    + '"diplomaSchool": "School",'
    + '"diplomaCity": "Paris",'
    + '"diplomaCountry": "FRA",'
    + '"diplomaStatus": "CORE_PKL_In_Progress",'
    + '"diplomaExpectedGraduationDate": "2022-03-30",'
    + '"diplomaModificationDate": "2022-03-30",'
    + '"diplomaDate": "2022-03-30",'
    + '"additionalFields": {'
    + '"CORE_Commercial_Name__c": "test value 1"'
    + '}'
    + '},'
    + '{'
    + '"diplomaExternalId": "testExtId2",'
    + '"diplomaName": "testName2",'
    + '"diplomaLevel": "CORE_PKL_Apprenticeship",'
    + '"diplomaSchool": "School",'
    + '"diplomaCity": "Paris",'
    + '"diplomaCountry": "FRA",'
    + '"diplomaStatus": "CORE_PKL_In_Progress",'
    + '"diplomaExpectedGraduationDate": "2022-03-30",'
    + '"diplomaModificationDate": "2022-03-30",'
    + '"diplomaDate": "2022-03-30",'
    + '"additionalFields": {'
    + '"CORE_Commercial_Name__c": "test value 2"'
    + '}'
    + '}'
    + '],'
    + '"aolProgressionFields": {'
    + '"aolProgression": 50.09,'
    + '"documentProgression": "Started",'
    + '"firstDocumentUploadDate": "2022-03-29",'
    + '"lastAolEventDate": "2022-03-29T08:47:39.689Z",'
    + '"lastDeaAolLoginDate": "2022-03-29T08:47:39.689Z",'
    + '"isAol": true,'
    + '"isAolOnlineRegistered": true,'
    + '"isAolRetailRegistered": true'
    + '}'
    + '}';

    @TestSetup
    static void makeData(){
        CORE_CountrySpecificSettings__c countrySetting = new CORE_CountrySpecificSettings__c(
                CORE_FieldPrefix__c = 'CORE_'
        );
        database.insert(countrySetting, true);

        CORE_CatalogHierarchyModel catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test').catalogHierarchy;
        Account account = CORE_DataFaker_Account.getStudentAccount('test');
        Opportunity  opportunity = CORE_DataFaker_Opportunity.getAOLOpportunity(catalogHierarchy.division.Id, 
            catalogHierarchy.businessUnit.Id, 
            account.Id, 
            CORE_AOL_Constants.PKL_OPP_STAGENAME_PROSPECT, 
            CORE_AOL_Constants.PKL_OPP_SUBSTATUS_PROSPECTAPPSTARTED, 
            AOL_EXTERNALID
        );
    }

    @IsTest
    public static void academicDiplomaHistoryProcess_should_upsert_aolDocuments_and_update_aolWorkerOpp_url() {
        CORE_AOL_Wrapper.AOLProgressionFields aolProgressionFields= CORE_DataFaker_AOL_Wrapper.getAolProgressionFieldsWrapper();
        CORE_AOL_GlobalWorker globalWorker = new CORE_AOL_GlobalWorker(aolProgressionFields,AOL_EXTERNALID);

        CORE_AOL_Wrapper.AcademicDiplomaHistory academicDiplomaHistory1 = CORE_DataFaker_AOL_Wrapper.getAcademicDiplomaHistoryWrapper(1);
        CORE_AOL_Wrapper.AcademicDiplomaHistory academicDiplomaHistory2 = CORE_DataFaker_AOL_Wrapper.getAcademicDiplomaHistoryWrapper(2);
        List<CORE_AOL_Wrapper.AcademicDiplomaHistory> academicDiplomaHistory = new List<CORE_AOL_Wrapper.AcademicDiplomaHistory> {academicDiplomaHistory1, academicDiplomaHistory2};

        List<CORE_Academic_Diploma_History__c> academicDiplomaHistoryList = [Select Id from CORE_Academic_Diploma_History__c];

        System.assertEquals(0, academicDiplomaHistoryList.size());

        CORE_AOL_AcademicDiplomaHistory.academicDiplomaHistoryProcess(globalWorker, academicDiplomaHistory);

        academicDiplomaHistoryList = [Select Id from CORE_Academic_Diploma_History__c];

        System.assertEquals(academicDiplomaHistory.size(), academicDiplomaHistoryList.size());

        academicDiplomaHistory1.diplomaName = 'testUpdate';
        CORE_AOL_AcademicDiplomaHistory.academicDiplomaHistoryProcess(globalWorker, academicDiplomaHistory);
        academicDiplomaHistoryList = [Select Id, Name from CORE_Academic_Diploma_History__c where CORE_Diploma_ExternalId__c = :academicDiplomaHistory1.diplomaExternalId];
        System.assertEquals(academicDiplomaHistory1.diplomaName, academicDiplomaHistoryList.get(0).Name);

        academicDiplomaHistoryList = [Select Id from CORE_Academic_Diploma_History__c];
        System.assertEquals(academicDiplomaHistory.size(), academicDiplomaHistoryList.size());
    }

    @IsTest
    public static void checkMandatoryParameters_should_return_null_if_there_is_no_missing_parameters() {
        CORE_AOL_Wrapper.AOLProgressionFields aolProgressionFields= CORE_DataFaker_AOL_Wrapper.getAolProgressionFieldsWrapper();
        CORE_AOL_GlobalWorker globalWorker = new CORE_AOL_GlobalWorker(aolProgressionFields,AOL_EXTERNALID);

        CORE_AOL_Wrapper.AcademicDiplomaHistory academicDiplomaHistory1 = CORE_DataFaker_AOL_Wrapper.getAcademicDiplomaHistoryWrapper(1);
        CORE_AOL_Wrapper.AcademicDiplomaHistory academicDiplomaHistory2 = CORE_DataFaker_AOL_Wrapper.getAcademicDiplomaHistoryWrapper(2);
        List<CORE_AOL_Wrapper.AcademicDiplomaHistory> academicDiplomaHistory = new List<CORE_AOL_Wrapper.AcademicDiplomaHistory> {academicDiplomaHistory1, academicDiplomaHistory2};

        CORE_AOL_Wrapper.ResultWrapperGlobal result = CORE_AOL_AcademicDiplomaHistory.checkMandatoryParameters(AOL_EXTERNALID, academicDiplomaHistory);

        System.assertEquals(null, result);
    }

    @IsTest
    public static void checkMandatoryParameters_should_return_result_wrapper_with_error_status_if_there_is_missing_parameters() {
        CORE_AOL_Wrapper.AOLProgressionFields aolProgressionFields= CORE_DataFaker_AOL_Wrapper.getAolProgressionFieldsWrapper();
        CORE_AOL_GlobalWorker globalWorker = new CORE_AOL_GlobalWorker(aolProgressionFields,AOL_EXTERNALID);

        CORE_AOL_Wrapper.AcademicDiplomaHistory academicDiplomaHistory1 = CORE_DataFaker_AOL_Wrapper.getAcademicDiplomaHistoryWrapper(1);
        CORE_AOL_Wrapper.AcademicDiplomaHistory academicDiplomaHistory2 = CORE_DataFaker_AOL_Wrapper.getAcademicDiplomaHistoryWrapper(2);
        List<CORE_AOL_Wrapper.AcademicDiplomaHistory> academicDiplomaHistory = new List<CORE_AOL_Wrapper.AcademicDiplomaHistory> {academicDiplomaHistory1, academicDiplomaHistory2};

        CORE_AOL_Wrapper.ResultWrapperGlobal result = CORE_AOL_AcademicDiplomaHistory.checkMandatoryParameters(null, academicDiplomaHistory);

        System.assertNotEquals(null, result);
        System.assertEquals(CORE_AOL_Constants.ERROR_STATUS, result.status);
        System.assert(result.message.contains('aolExternalId'));

        academicDiplomaHistory1.diplomaExternalId = null;
        result = CORE_AOL_AcademicDiplomaHistory.checkMandatoryParameters(AOL_EXTERNALID, academicDiplomaHistory);
        System.assertNotEquals(null, result);
        System.assertEquals(CORE_AOL_Constants.ERROR_STATUS, result.status);
        System.assert(!result.message.contains('aolExternalId'));
        System.assert(result.message.contains('academicDiplomaHistory[0].diplomaExternalId'));

        AcademicDiplomaHistory2.diplomaName = ' ';
        result = CORE_AOL_AcademicDiplomaHistory.checkMandatoryParameters(AOL_EXTERNALID, academicDiplomaHistory);
        System.assertNotEquals(null, result);
        System.assertEquals(CORE_AOL_Constants.ERROR_STATUS, result.status);
        System.assert(!result.message.contains('aolExternalId'));
        System.assert(result.message.contains('academicDiplomaHistory[0].diplomaExternalId'));
        System.assert(result.message.contains('academicDiplomaHistory[1].diplomaName'));
    }

    @IsTest
    private static void execute_should_return_a_success_if_all_upsert_are_ok(){

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/aol/academic-diploma-history';

        req.httpMethod = 'POST';
        req.requestBody = Blob.valueOf(REQ_BODY);

        RestContext.request = req;
        RestContext.response= res;

        CORE_AOL_Wrapper.ResultWrapperGlobal result = CORE_AOL_AcademicDiplomaHistory.execute();

        System.assertEquals('OK', result.status);
    }

    @IsTest
    private static void execute_should_return_an_error_if_mandatory_missing(){

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/aol/academic-diploma-history';

        req.httpMethod = 'POST';
        req.requestBody = Blob.valueOf(REQ_BODY_ERROR_NO_ID);

        RestContext.request = req;
        RestContext.response= res;

        CORE_AOL_Wrapper.ResultWrapperGlobal result = CORE_AOL_AcademicDiplomaHistory.execute();

        System.assertEquals('KO', result.status);
    }

    @IsTest
    private static void execute_should_return_an_error_if_invalid_externalID(){

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/aol/academic-diploma-history';

        req.httpMethod = 'POST';
        req.requestBody = Blob.valueOf(REQ_BODY_ERROR_INVALID_ID);

        RestContext.request = req;
        RestContext.response= res;

        CORE_AOL_Wrapper.ResultWrapperGlobal result = CORE_AOL_AcademicDiplomaHistory.execute();

        System.assertEquals('KO', result.status);
    }

    @IsTest
    private static void execute_should_return_an_error_if_all_upsert_are_not_ok(){

        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/aol/academic-diploma-history';

        req.httpMethod = 'POST';
        req.requestBody = Blob.valueOf(REQ_BODY_ERROR);

        RestContext.request = req;
        RestContext.response= res;

        CORE_AOL_Wrapper.ResultWrapperGlobal result = CORE_AOL_AcademicDiplomaHistory.execute();

        System.assertEquals('KO', result.status);
    }

}