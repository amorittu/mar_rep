/**
 * Created By Fodil
 * Created Date : 12-04-2022
 */
public virtual with sharing class CORE_SISOpportunityApplicationProcess {
    public class SISException extends Exception {}

    @TestVisible
    protected DateTime apiCallDate;

    @TestVisible
    protected CORE_SISOpportunityApplicationWrapper.GlobalWrapper datas;

    public List<CORE_SISOpportunityApplicationWrapper.ErrorWrapper> errors{get;set;}
    public CORE_SISOpportunityApplicationWrapper.ResultWrapper result{get;set;}
    public CORE_OpportunityDataMaker dataMaker{get;set;}

    @TestVisible
    protected List<Task> tasksToCreate{get;set;}

    public Boolean countryPicklistIsActivated;
    public Account selectedPA = new Account();
    public Opportunity selectedOpportunity = new Opportunity();
    @TestVisible
    protected List<String> availableAcademicYears{get;set;}
    public CORE_SISOpportunityApplicationProcess(DateTime apiCallDate,CORE_SISOpportunityApplicationWrapper.GlobalWrapper datas) {
        this.apiCallDate = apiCallDate;
        this.datas = datas;
        this.errors = new List<CORE_SISOpportunityApplicationWrapper.ErrorWrapper>();
        this.result = new CORE_SISOpportunityApplicationWrapper.ResultWrapper();
        this.dataMaker = new CORE_OpportunityDataMaker();
        this.tasksToCreate = new List<Task>();
        this.countryPicklistIsActivated = CORE_SobjectUtils.doesFieldExist('User', 'Countrycode');

    }

    public void execute(){

        /* Validating input */
        CORE_SISOpportunityApplicationValidator inputValidator = new CORE_SISOpportunityApplicationValidator(this.datas,this.apiCallDate);
        this.errors = inputValidator.getFieldsErrors();

        if(this.errors.size()>0){
            System.debug('CORE_SISOpportunityApplicationProcess.execute() error');

            return ;
        }
        // if there is no error

        Account existedPersonAccount ;
        if(this.datas.mainInterest.SIS_OpportunityId != null && this.datas.mainInterest.SIS_OpportunityId.trim() != '' ){
            Map<String,Object> personAccountsMap = this.datas.personAccount;
            CORE_SISOpportunityApplicationWrapper.MainInterestWrapper mainInterest = this.datas.mainInterest;
            String phoneNumber='';
            String email = '';
            String PersonMobilePhone='';
            /* in a case of we have an SIS check if at least the email or PersonMobilePhone or phone */
            
            email = (personAccountsMap.containsKey('PersonEmail'))? (String) personAccountsMap.get('PersonEmail') : email;
            phoneNumber = (personAccountsMap.containsKey('Phone'))? (String) personAccountsMap.get('Phone') : phoneNumber;
            PersonMobilePhone = (personAccountsMap.containsKey('PersonMobilePhone'))? (String) personAccountsMap.get('PersonMobilePhone') : PersonMobilePhone;
            
            if(String.isBlank(email) && String.isBlank(email) && String.isBlank(PersonMobilePhone)) {
                throw new SISException ('At least to enter the values of the following fields: PersonEmail, Phone, PersonMobilePhone');
            }
            
            
            // Created By SIS
            List<Account> personAccounts = new List<Account>();
            if( this.datas.personAccount.containsKey('CORE_SISStudentID__c') && !String.isBlank((String)this.datas.personAccount.get('CORE_SISStudentID__c'))){
                String paSISId = (String) this.datas.personAccount.get('CORE_SISStudentID__c');
                personAccounts = [ SELECT Id FROM Account where CORE_SISStudentID__c = :paSISId  ];
            }
            
            Opportunity_modification_settings__mdt oppSettings = Opportunity_modification_settings__mdt.getInstance('SIS_Settings');
            Opportunity currentOpportunity = new Opportunity();
            // checking if there is a PA similar to the one called by WS.
            Boolean created = false; // create an opportunity indicator

            Boolean isChangeToNotMain=false;
            List<Opportunity> oppsChangeToNotMain = new List<Opportunity>();

            if(personAccounts.size()==0){ //  no PA with SIS with the same SIS Id
                personAccounts = CORE_SISOpportunityApplicationUtils.getExistingAccounts(email,phoneNumber);                
            } else {
                // PA exist with the same SIS ID
                personAccounts = new List<Account>{CORE_SIS_QueryProvider.getAccount(personAccounts[0].Id, null, new List<String>())};
            }
            
            try{
                system.debug('person Accounts : ' +  personAccounts);
                // get the PA with getAccount created if it doesn't exist or updated
                existedPersonAccount = getAccount(personAccounts);
                this.selectedPA = existedPersonAccount;
                CORE_SISOpportunityApplicationWrapper.MainInterestWrapper currentInterest = this.datas.mainInterest;
                CORE_CatalogHierarchyModel catalogHierarchy;
                Boolean productSearch =false;
                CORE_Business_Unit__c bu ;
                if(!String.IsBlank(currentInterest.productExternalID)){
                    List<Product2>  currentproducts = [SELECT ID,SIS_External_Id__c, CORE_IntakeID__c,
                    CORE_IntakeID__r.CORE_Parent_Level__r.CORE_Parent_Curriculum__r.CORE_Parent_Study_Area__r.CORE_Parent_Business_Unit__r.CORE_Business_Unit_ExternalId__c
                    , CORE_IntakeID__r.CORE_Parent_Level__r.CORE_Parent_Curriculum__r.CORE_Curriculum_ExternalId__c, 
                    CORE_IntakeID__r.CORE_Intake_ExternalId__c, CORE_IntakeID__r.CORE_Parent_Level__r.CORE_Level_ExternalId__c, CORE_IntakeID__r.CORE_Academic_Year__c,
                    CORE_IntakeID__r.CORE_Parent_Level__r.CORE_Parent_Curriculum__r.CORE_Parent_Study_Area__r.CORE_Study_Area_ExternalId__c 
                    FROM Product2 where SIS_External_Id__c = :currentInterest.productExternalID];
                    
                    if(currentproducts.isEmpty()){
                        throw new IllegalArgumentException('Product ' + currentInterest.productExternalID + 'not found');
                    }

                    Product2 currentproduct = currentproducts.get(0);

                    bu = [SELECT Id,CORE_Business_Unit_ExternalId__c, CORE_BU_Technical_User__c,CORE_Parent_Division__r.CORE_Division_ExternalId__c, CORE_Parent_Division__c,CORE_Parent_Division__r.CORE_Default_Academic_Year__c FROM CORE_Business_Unit__c where CORE_Business_Unit_ExternalId__c = :currentproduct.CORE_IntakeID__r.CORE_Parent_Level__r.CORE_Parent_Curriculum__r.CORE_Parent_Study_Area__r.CORE_Parent_Business_Unit__r.CORE_Business_Unit_ExternalId__c LIMIT 1 ];

                    if(currentproduct !=null){
                        catalogHierarchy = new CORE_CatalogHierarchyModel(
                        (bu.CORE_Parent_Division__r.CORE_Division_ExternalId__c == '') ? null :  bu.CORE_Parent_Division__r.CORE_Division_ExternalId__c, 
                        (bu.CORE_Business_Unit_ExternalId__c == '') ? null :  bu.CORE_Business_Unit_ExternalId__c, 
                        (currentproduct.CORE_IntakeID__r.CORE_Parent_Level__r.CORE_Parent_Curriculum__r.CORE_Parent_Study_Area__r.CORE_Study_Area_ExternalId__c == '') ? null :  currentproduct.CORE_IntakeID__r.CORE_Parent_Level__r.CORE_Parent_Curriculum__r.CORE_Parent_Study_Area__r.CORE_Study_Area_ExternalId__c, 
                        (currentproduct.CORE_IntakeID__r.CORE_Parent_Level__r.CORE_Parent_Curriculum__r.CORE_Curriculum_ExternalId__c == '') ? null :  currentproduct.CORE_IntakeID__r.CORE_Parent_Level__r.CORE_Parent_Curriculum__r.CORE_Curriculum_ExternalId__c, 
                        (currentproduct.CORE_IntakeID__r.CORE_Parent_Level__r.CORE_Level_ExternalId__c == '') ? null : currentproduct.CORE_IntakeID__r.CORE_Parent_Level__r.CORE_Level_ExternalId__c,
                        (currentproduct.CORE_IntakeID__r.CORE_Intake_ExternalId__c == '') ? null : currentproduct.CORE_IntakeID__r.CORE_Intake_ExternalId__c
                        );
                        catalogHierarchy.product = currentproduct;
                        catalogHierarchy.intake.CORE_Academic_Year__c = currentproduct?.CORE_IntakeID__r?.CORE_Academic_Year__c;
                        catalogHierarchy.intake.Id = currentproduct?.CORE_IntakeID__c;
                        catalogHierarchy.division.CORE_Default_Academic_Year__c = bu?.CORE_Parent_Division__r?.CORE_Default_Academic_Year__c;
                        catalogHierarchy.division.Id = bu?.CORE_Parent_Division__c;
                        catalogHierarchy.businessUnit.Id = bu?.Id;
                        productSearch=true;
                    } 

                    
                    
                }
                if(!productSearch) {
                    catalogHierarchy = new CORE_CatalogHierarchyModel(
                        (currentInterest.division == '') ? null : currentInterest.division, 
                        (currentInterest.businessUnit == '') ? null : currentInterest.businessUnit, 
                        (currentInterest.studyArea == '') ? null : currentInterest.studyArea, 
                        (currentInterest.curriculum == '') ? null : currentInterest.curriculum, 
                        (currentInterest.level == '') ? null : currentInterest.level,
                        (currentInterest.intake == '') ? null : currentInterest.intake
                        );
                        bu = [SELECT Id, CORE_BU_Technical_User__c,CORE_Business_Unit_ExternalId__c FROM CORE_Business_Unit__c where CORE_Business_Unit_ExternalId__c = :currentInterest.businessUnit LIMIT 1 ];
                        
                        Map<Integer,CORE_CatalogHierarchyModel> catalogHierarchies = new Map<Integer,CORE_CatalogHierarchyModel>();
                        catalogHierarchies.put(0, catalogHierarchy);
                        catalogHierarchies = CORE_OpportunityDataMaker.getCatalogHierarchies(catalogHierarchies);
                        catalogHierarchy = catalogHierarchies.get(0);
                }
                
                    if(personAccounts.size() == 0){ // A person account is created
                        // create the opportunity
                        currentOpportunity = initOpportunitySIS(this.selectedPA,this.datas.mainInterest.academicYear,catalogHierarchy,null);
                        currentOpportunity.OwnerId = (bu.CORE_BU_Technical_User__c != null) ? bu.CORE_BU_Technical_User__c :UserInfo.getUserId() ;
                        created = true;
                    }
                    else { // a person account is not created (updated)
                        String SISOpp_Id = mainInterest.SIS_OpportunityId;
                        
                    List<Opportunity> oppsList = [ SELECT ID FROM Opportunity WHERE CORE_OPP_SIS_Opportunity_Id__c = :SISOpp_Id AND AccountId = :this.selectedPA.Id ];
                    
                    if(oppsList.size() != 0){ 
                     throw new SISException('Opportunity already exist with the same SIS Opportunity ID');
                    } else {// not the same SIS Opp Id
                        List<Opportunity> oppsWithSameBUs = CORE_OpportunityUtils.getExistingMainOpportunitiesByExternalIds(new Set<String>{catalogHierarchy.businessUnit.CORE_Business_Unit_ExternalId__c}, this.selectedPA.Id);
                        List<Opportunity> oppsWithSameBUsAndAY = new List<Opportunity>();
                        List<Opportunity> oppsWithSameBUsAndAYAndCurriculum = new List<Opportunity>();
                        Boolean isOppWithSameAY = false;
                        Boolean isOppWithSameCuriculum = false;
                        for(Opportunity opp : oppsWithSameBUs){
                                    if(opp.CORE_OPP_Academic_Year__c == catalogHierarchy.intake.CORE_Academic_Year__c || opp.CORE_OPP_Academic_Year__c == catalogHierarchy.division.CORE_Default_Academic_Year__c){
                                        isOppWithSameAY = true;
                                        oppsWithSameBUsAndAY.add(opp);
                                    }
                        }

                        for(Opportunity opp : oppsWithSameBUsAndAY){
                            if(opp.CORE_OPP_Curriculum__r.CORE_Curriculum_ExternalId__c == mainInterest.curriculum){
                                isOppWithSameCuriculum = true;
                                oppsWithSameBUsAndAYAndCurriculum.add(opp);
                            }
                        }


                        if( oppsWithSameBUs.size()==0){
                            // create a new Opp
                            System.debug('CORE_SISOpportunityApplicationProcess > execute > without Same BU');
                            currentOpportunity = initOpportunitySIS(this.selectedPA,this.datas.mainInterest.academicYear,catalogHierarchy,null);
                            currentOpportunity.OwnerId = (bu.CORE_BU_Technical_User__c != null) ? bu.CORE_BU_Technical_User__c :UserInfo.getUserId() ;
                            created = true;
                        }
                        else{
                            if(isOppWithSameAY){
                                if(isOppWithSameCuriculum  && oppSettings.SIS_Allowed_Statuses__c.containsIgnoreCase(oppsWithSameBUsAndAYAndCurriculum[0].StageName)){
                                    // select the opportunity
                                    System.debug('CORE_SISOpportunityApplicationProcess > execute > isOppWithSameCuriculum');
                                    currentOpportunity = initOpportunitySIS(this.selectedPA,this.datas.mainInterest.academicYear,catalogHierarchy,oppsWithSameBUsAndAYAndCurriculum[0]);
                                    update currentOpportunity;
                                }
                                else {
                                    // Create a new Opp
                                    System.debug('CORE_SISOpportunityApplicationProcess > execute > without Same Curriculum');
                                    currentOpportunity = initOpportunitySIS(this.selectedPA,this.datas.mainInterest.academicYear,catalogHierarchy,null);
                                    currentOpportunity.OwnerId = oppsWithSameBUs[0].OwnerId;
                                    created = true;
                                    isChangeToNotMain = true;
                                    oppsChangeToNotMain = oppsWithSameBUsAndAY;
                                }
                            }
                            else { //  create a new Opp
                                System.debug('CORE_SISOpportunityApplicationProcess > execute > without Same AY AND Same BU');
                                currentOpportunity = initOpportunitySIS(this.selectedPA,this.datas.mainInterest.academicYear,catalogHierarchy,null);
                                currentOpportunity.OwnerId = oppsWithSameBUs[0].OwnerId;
                                created = true;
    
                            
                            }
                        }
                    
                    
                    }
                }
                // initOppLineItem
                if (created){
                    Database.insert(currentOpportunity);
                    getOppLineItems(new List<Opportunity>{currentOpportunity});
                    if(isChangeToNotMain){
                        update changeToNotMain(oppsChangeToNotMain);
                    }
                    // Flow to do custom logic
                    Map<String,Object> flowMap = new Map<String,Object>();
                    flowMap.put('recordId',currentOpportunity.Id );
                    Flow.Interview.CORE_Set_Applicant_Registered_OPP_Status customFlow= new Flow.Interview.CORE_Set_Applicant_Registered_OPP_Status(flowMap);
                    customFlow.start();
                }
                
                this.selectedOpportunity = currentOpportunity;

                CORE_Point_OF_CONTACT__c currentPoc = new CORE_Point_OF_CONTACT__c();
                List<CORE_Consent__c> consents = new List<CORE_Consent__c>();
                
                if(this.datas.acquisitionPointOfContact != null ){
                    // there is acquisitionPointOfContact
                    System.debug('CORE_SISOpportunityApplicationProcess > execute > acquisitionPointOfContact');
                    CORE_SISOpportunityApplicationWrapper.AcquisitionPointOfContactWrapper poc = this.datas.acquisitionPointOfContact;
                    currentPOC = CORE_SISOpportunityApplicationUtils.getPocFromWrapper(createDefaultPointOfContact(currentOpportunity),poc);
                }
                else{
                    // create default acquistions Point of contact
                    System.debug('CORE_SISOpportunityApplicationProcess > execute > create default acquistions Point of contact');
                    currentPOC = createDefaultPointOfContact(currentOpportunity);
                }
                
                if(this.datas.consents != null && this.datas.consents.size() !=0 ){
                    // there is Consents to create
                    System.debug('CORE_SISOpportunityApplicationProcess > execute > consents');
                    consents = initConsentsFromAPI(this.selectedPA,this.datas.consents);
                }
                else {
                    // create default consents
                    System.debug('CORE_SISOpportunityApplicationProcess > execute > create default consents');
                    consents = createDefaultConsents(this.selectedPA,currentOpportunity);
                }
                
                insert currentPoc;

                // inserting Task
                Task createdTask =  new Task(
                    WhatId = currentOpportunity.Id,
                    OwnerId = currentOpportunity.OwnerId,
                    Subject = (created)? System.Label.SIS_Task_Creation : System.Label.SIS_Task_Modifiction,
                    Priority = 'Normal',
                    Type='CORE_PKL_SIS',
                    Status = (created)? 'CORE_PKL_Completed':'CORE_PKL_Open',
                    ActivityDate = (created)? null : Date.today(),
                    CORE_Is_Incoming__c = true,
                    CORE_Point_of_contact__c = currentPoc.Id
                );

                insert createdTask;

                upsert consents CORE_TECH_InternalReference__c;

            } catch (Exception ex){
                throw ex;
            }
        }
        else{
            throw new SISException('SIS_OpportunityId not provided or provided with null/empty-string value');
        }
    }


    @TestVisible
    protected Account getAccount(List<Account> existingAccounts){
        System.debug('CORE_SISOpportunityApplicationProcess.getAccount() : START');
        Account account;
        Long dt1 = DateTime.now().getTime();
        Long dt2,dt3;

        Database.DMLOptions dml = new Database.DMLOptions();
        dml.DuplicateRuleHeader.AllowSave = true; 
        dt3 = DateTime.now().getTime();
        switch on existingAccounts.size() {
            when 0 {
                account = CORE_SISOpportunityApplicationUtils.initPersonAccountFromMap(this.datas.personAccount,this.countryPicklistIsActivated);
                
                System.debug('0 account found');
                dt2 = DateTime.now().getTime();
                System.debug('CORE_SISOpportunityApplicationProcess.execute() -> initPersonAccount() time execution = ' + (dt2-dt3) + ' ms');
                
                //insert account;
                Database.SaveResult sr = Database.insert(account, dml); 
                if(sr.isSuccess()){
                    account.Id = sr.getId();
                    account = CORE_SIS_QueryProvider.getAccount(account.Id, null, new List<String>());
                } else {
                    throw new IllegalArgumentException('Unable to insert account : ' + sr.getErrors().get(0).getMessage());
                }
            }
            when else {
                account = CORE_SISOpportunityApplicationUtils.initPersonAccountFromMap(this.datas.personAccount,this.countryPicklistIsActivated);
                account.Id = existingAccounts[0].Id;
                /*getAccountWithPhoneFields / getAccountWithEmailFields */
                if(this.datas.personAccount.containsKey('Phone')) existingAccounts[0] = CORE_AccountDataMaker.getAccountWithPhoneFields(existingAccounts[0],(String)this.datas.personAccount.get('Phone'),false,true);
                if(this.datas.personAccount.containsKey('PersonMobilePhone')) existingAccounts[0] = CORE_AccountDataMaker.getAccountWithPhoneFields(existingAccounts[0],(String)this.datas.personAccount.get('PersonMobilePhone'),true,true);
                if(this.datas.personAccount.containsKey('PersonEmail')) existingAccounts[0] = CORE_AccountDataMaker.getAccountWithEmailFields(existingAccounts[0],(String)this.datas.personAccount.get('PersonEmail'),true);
                account.Phone = existingAccounts[0].Phone;
                account.CORE_Phone2__pc = existingAccounts[0].CORE_Phone2__pc;
                account.PersonMobilePhone = existingAccounts[0].PersonMobilePhone;
                account.CORE_Mobile_2__pc = existingAccounts[0].CORE_Mobile_2__pc;
                account.CORE_Mobile_3__pc = existingAccounts[0].CORE_Mobile_3__pc;
                account.PersonEmail = existingAccounts[0].PersonEmail;
                account.CORE_Student_Email__pc = existingAccounts[0].CORE_Student_Email__pc;
                account.CORE_Email_2__pc = existingAccounts[0].CORE_Email_2__pc;
                account.CORE_Email_3__pc = existingAccounts[0].CORE_Email_3__pc;
                System.debug('1 account found');
                dt2 = DateTime.now().getTime();
                System.debug('CORE_SISOpportunityApplicationProcess.execute() -> getUpdatedPersonAccount() time execution = ' + (dt2-dt3) + ' ms');
                
                Boolean isLocked;
                try {
                    Account record = [SELECT ID from account where id = :existingAccounts[0].Id FOR UPDATE];
                    isLocked = false;
                } catch(QueryException e) {
                    isLocked = true;
                }

                System.debug('isLocked -> '+isLocked);
                if(!isLocked){
                    //update account;
                    Database.SaveResult sr = Database.update(account, dml);
                    if(!sr.isSuccess()){
                        throw new IllegalArgumentException('Unable to update account : ' + sr.getErrors().get(0).getMessage());
                    }
                }
            }
        }

        dt3 = DateTime.now().getTime();
        System.debug('CORE_SISOpportunityApplicationProcess.execute() -> upsert account time execution = ' + (dt3-dt2) + ' ms');

        System.debug('CORE_SISOpportunityApplicationProcess.getAccount() : END');
        return account;
    }

    public Opportunity initOpportunitySIS(Account account, String academicYear, CORE_CatalogHierarchyModel catalogHierarchy, Opportunity mainOpportunity){
        Opportunity opp ;
        Set<String> specialStatus = new Set<String>{'Lead','Suspect','Prospect'};
        if(mainOpportunity == null){
            opp = this.dataMaker.initOpportunityDefaultValues(account,academicYear,catalogHierarchy,false,false);
            opp = this.dataMaker.initOpportunityCatalog(opp, catalogHierarchy);
        }else {
            opp = new Opportunity(Id =  mainOpportunity.Id,OwnerId=mainOpportunity.OwnerId,StageName=mainOpportunity.StageName,
            CORE_OPP_BUSINESS_UNIT__c = mainOpportunity.CORE_OPP_BUSINESS_UNIT__c,
            CORE_OPP_Division__c = mainOpportunity.CORE_OPP_Division__c
            );
            if(specialStatus.contains(opp.StageName)){
                opp = this.dataMaker.initOpportunityCatalog(opp, catalogHierarchy);
            }
        }
        opp = CORE_SISOpportunityApplicationUtils.initOpportunityFromMap(this.datas.opportunity,opp);
        opp.CORE_OPP_SIS_Opportunity_Id__c = this.datas.mainInterest.SIS_OpportunityId;
        //opp.CORE_Main_Product_Interest__r = new Product2(SIS_External_Id__c = this.datas.mainInterest?.productExternalID);   
        // AcademicYear is created from the intake
        if(mainOpportunity == null){
            String finalAcademicYear = CORE_OpportunityUtils.getAcademicYear(
                dataMaker.getDefaultAcademicYear(),
                this.datas.mainInterest.academicYear, 
                catalogHierarchy, 
                account.CreatedDate, 
                CORE_PicklistUtils.getPicklistValues('Opportunity', 'CORE_OPP_Academic_Year__c')
            );
            opp.CORE_OPP_Academic_Year__c = finalAcademicYear;
        }
        return opp;
    }

    public List<OpportunityLineItem> getOppLineItems(List<Opportunity> opportunities){
        List<OpportunityLineItem> oppLineItems = new List<OpportunityLineItem>();

        Map<Id,Id> buByProducts = new Map<Id,Id>();
        Opportunity opportunity = opportunities.get(0);

        if(!String.IsBlank(opportunity.CORE_Main_Product_Interest__c) && !String.IsBlank(opportunity.CORE_OPP_Business_Unit__c)){
            buByProducts.put(opportunity.CORE_Main_Product_Interest__c,opportunity.CORE_OPP_Business_Unit__c);
        } else {
            return oppLineItems;
        }

        Map<Id,PricebookEntry> pricebookEntries = this.dataMaker.getPricebookEntryByProducts(buByProducts);

        PricebookEntry pricebookEntry = pricebookEntries.get(opportunity.CORE_Main_Product_Interest__c);
        System.debug('VPI getOppLineItems > pricebookEntry = ' + pricebookEntry);

        if(pricebookEntry != NULL){
            System.debug('VPI getOppLineItems > pricebookEntry != NULL condition ok');

            OpportunityLineItem oppLineItem = this.dataMaker.initOppLineItem(opportunity, pricebookEntry);
            oppLineItems.add(oppLineItem);
        }

        if(oppLineItems.size() > 0){
            insert oppLineItems;
        }
        

        return oppLineItems;
    }
    // change the staus of main opportunities to not main
    public List<Opportunity> changeToNotMain(List<Opportunity> opportunities){
        List<Opportunity> oppsNotMain = new List<Opportunity>();
        for (Opportunity opp : opportunities){
            if(opp.CORE_OPP_Main_Opportunity__c){
                oppsNotMain.add(new Opportunity(id=opp.Id,CORE_OPP_Main_Opportunity__c =false));
            }
        }
        return oppsNotMain;
    }
    public CORE_Point_Of_Contact__c createDefaultPointOfContact(Opportunity relatedOpportunity){
        CORE_OpportunityDataMaker dataMaker = new CORE_OpportunityDataMaker();
        CORE_Point_of_Contact__c poc = dataMaker.initPocDefaultValues(relatedOpportunity,'CORE_PKL_Offline','CORE_PKL_SIS',null);
        poc.CORE_Insertion_Date__c = date.today();
        poc.CORE_Insertion_Mode__c = 'CORE_PKL_Manual';
        return poc;
    }

    public List<CORE_Consent__c> createDefaultConsents(Account acct,Opportunity relatedOpportunity){
        List<CORE_Consent__c> consents = new List<CORE_Consent__c>();
        
        CORE_Business_Unit__c consentBusinessUnit = [SELECT Id, CORE_Business_Unit_ExternalId__c, CORE_Brand__c
        FROM CORE_Business_Unit__c 
        WHERE /*CORE_Business_Unit_ExternalId__c = :relatedOpportunity.CORE_OPP_Business_Unit__r.CORE_Business_Unit_ExternalId__c*/ Id= :relatedOpportunity.CORE_OPP_Business_Unit__c ];

        /*CORE_Division__c consentDivision = [SELECT Id, CORE_Division_ExternalId__c
        FROM CORE_Division__c WHERE CORE_Division_ExternalId__c = :relatedOpportunity.CORE_OPP_Division__r.CORE_Division_ExternalId__c];*/
        
        consents.add(new CORE_Consent__c(
            CORE_Person_Account__c = acct.Id,
            CORE_Consent_type__c = 'CORE_PKL_Processing_Collection_Storage_of_data_consent',
            CORE_Consent_channel__c ='CORE_PKL_Not_applicable' ,
            CORE_Opt_in__c = true,
            CORE_Opt_in_date__c = DateTime.now() ,
            CORE_Division__c = relatedOpportunity.CORE_OPP_Division__c,
            CORE_Business_Unit__c = consentBusinessUnit.Id,
            CORE_Scope__c = 'CORE_PKL_Division'
        ));

        consents.add(new CORE_Consent__c(
            CORE_Person_Account__c = acct.Id,
            CORE_Consent_type__c = 'CORE_PKL_Authorisation_to_be_contacted_by_channel',
            CORE_Consent_channel__c ='CORE_PKL_All',
            CORE_Opt_in__c = true,
            CORE_Opt_in_date__c = DateTime.now() ,
            CORE_Division__c = relatedOpportunity.CORE_OPP_Division__c,
            CORE_Business_Unit__c = consentBusinessUnit.Id,
            CORE_Scope__c = 'CORE_PKL_Division'
        ));
        consents[0].CORE_TECH_InternalReference__c = CORE_ConsentTriggerHandler.getInternalReference(consents[0], consentBusinessUnit.CORE_Brand__c);
        consents[1].CORE_TECH_InternalReference__c = CORE_ConsentTriggerHandler.getInternalReference(consents[1], consentBusinessUnit.CORE_Brand__c);
        return consents;
    }


    // init consents from api
    public List<CORE_Consent__c> initConsentsFromAPI(Account account, List<Object> consentsList){
        // consents by bus and divisions
        List<String> consentsBuExternalIds = new List<String>();
        List<String> consentsDivisionExternalIds = new List<String>();
        for(Object consent : consentsList){
            Map<String,Object> consentMap = (Map<String,Object>) consent ;
            if(consentMap.get('CORE_Business_Unit__c') != null) {
                consentsBuExternalIds.add(String.valueOf(consentMap.get('CORE_Business_Unit__c')));
            }
            if(consentMap.get('CORE_Division__c') != null) {
                consentsDivisionExternalIds.add(String.valueOf(consentMap.get('CORE_Division__c')));
            }
        }
        List<CORE_Business_Unit__c> consentsBusinessUnits = [SELECT Id, CORE_Business_Unit_ExternalId__c, CORE_Brand__c
            FROM CORE_Business_Unit__c 
            WHERE CORE_Business_Unit_ExternalId__c IN :consentsBuExternalIds];

        List<CORE_Division__c> consentsDivisions = [SELECT Id, CORE_Division_ExternalId__c
            FROM CORE_Division__c 
            WHERE CORE_Division_ExternalId__c IN :consentsDivisionExternalIds];
        
        Map<String,CORE_Business_Unit__c> buByExternalIds = new Map<String,CORE_Business_Unit__c>();
        Map<String,CORE_Division__c> divisionsByExternalIds = new Map<String,CORE_Division__c>();
        for(CORE_Business_Unit__c bu : consentsBusinessUnits){
            buByExternalIds.put(bu.CORE_Business_Unit_ExternalId__c,bu);
        }
        for(CORE_Division__c division : consentsDivisions){
            divisionsByExternalIds.put(division.CORE_Division_ExternalId__c,division);
        }
        List<CORE_Consent__c> consents = new List<CORE_Consent__c>();
        for(Object consentWrapper : consentsList){
            CORE_Consent__c consent = new CORE_Consent__c();
            Map<String,Object> consentMap  = (Map<String,Object>)consentWrapper;
            consent = CORE_SISOpportunityApplicationUtils.initConsentFromMap(consentMap,new CORE_Consent__c());
            consent.CORE_Scope__c = 'CORE_PKL_Division';
            consent.CORE_Business_Unit__c=null;
            consent.CORE_Division__c=null;
            consent.CORE_Person_Account__c = this.selectedPA.Id;
            if(!String.isBlank((String)consentMap.get('CORE_Business_Unit__c'))) consent.CORE_Business_Unit__c = buByExternalIds.get((String)consentMap.get('CORE_Business_Unit__c')).Id;
            if(!String.isBlank((String)consentMap.get('CORE_Division__c'))) consent.CORE_Division__c = divisionsByExternalIds.get((String)consentMap.get('CORE_Division__c')).Id;
            consent.CORE_TECH_InternalReference__c = CORE_ConsentTriggerHandler.getInternalReference(consent,buByExternalIds.get((String)consentMap.get('CORE_Business_Unit__c')).CORE_Brand__c);
            System.debug('initConsentsFromAPI 0'+consent.CORE_TECH_InternalReference__c);
     
            consents.add(consent);
        }
        return consents;
    }   
}