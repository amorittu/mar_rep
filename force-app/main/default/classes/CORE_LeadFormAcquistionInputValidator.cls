public class CORE_LeadFormAcquistionInputValidator {
    @TestVisible
    public static final String UNEXPECTED_MSG = 'An unexpected error occured while processing the request';
    @TestVisible
    public static final String UNEXPECTED_CODE = 'UNEXPECTED_ERROR';

    @TestVisible
    private static final String MANDATORY_FIELDS_MSG = 'Mandatory fields are missing';
    @TestVisible
    private static final String MANDATORY_FIELDS_CODE = 'MANDATORY_FIELDS';

    @TestVisible
    private static final String INVALID_PICKLIST_MSG = 'Bad value for restricted picklist';
    @TestVisible
    private static final String INVALID_PICKLIST_CODE = 'PICKLIST_FIELDS';

    @TestVisible
    private CORE_LeadFormAcquisitionWrapper.GlobalWrapper inputParameters;

    @TestVisible
    private DateTime apiCallDate;

    @TestVisible
    private List<String> pklSalutation;
    @TestVisible
    private List<String> pklMainNationality;
    @TestVisible
    private List<String> pklStateCode;
    @TestVisible
    private List<String> pklCountryCode;
    @TestVisible
    private List<String> pklLanguageWebsite;
    @TestVisible
    private List<String> pklConsentType;
    @TestVisible
    private List<String> pklConsentChannel;
    @TestVisible
    private List<String> pklPocCaptureChannel;
    @TestVisible
    private List<String> pklPocCampaignMedium;
    @TestVisible
    private List<String> pklPocDevice;
    @TestVisible
    private List<String> pklPocNature;
    @TestVisible
    private List<String> pklForm;
    @TestVisible
    private List<String> pklFormArea;
    public List<String> pklOppAcademicYear{get;set;}
    @TestVisible
    private List<String> pklLearningMaterial;
    public Boolean countryPicklistIsActivated;

    public CORE_LeadFormAcquistionInputValidator(CORE_LeadFormAcquisitionWrapper.GlobalWrapper inputParameters,DateTime apiCallDate, Boolean countryPicklistIsActivated){
        this.inputParameters = inputParameters;
        this.apiCallDate = apiCallDate;
        this.countryPicklistIsActivated = countryPicklistIsActivated;

        this.initPicklistValues();
    }

    private void initPicklistValues(){
        System.debug('CORE_LeadFormAcquistionInputValidator.initPicklistValues() : START');
        Long dt1 = DateTime.now().getTime();
        Long dt2;


        this.pklSalutation = CORE_PicklistUtils.getPicklistValues('Account', 'Salutation');
        if(this.countryPicklistIsActivated){
            this.pklStateCode = CORE_PicklistUtils.getPicklistValues('User', 'statecode');
            this.pklCountryCode = CORE_PicklistUtils.getPicklistValues('User', 'Countrycode');
        }
        this.pklLanguageWebsite = CORE_PicklistUtils.getPicklistValues('Account', 'CORE_Language_website__pc');
        this.pklMainNationality = CORE_PicklistUtils.getPicklistValues('Account', 'CORE_Main_Nationality__pc');
        this.pklConsentType = CORE_PicklistUtils.getPicklistValues('CORE_Consent__c', 'CORE_Consent_type__c');
        this.pklConsentChannel = CORE_PicklistUtils.getPicklistValues('CORE_Consent__c', 'CORE_Consent_channel__c');
        this.pklPocCaptureChannel = CORE_PicklistUtils.getPicklistValues('CORE_Point_of_Contact__c', 'CORE_Capture_Channel__c');
        this.pklPocCampaignMedium = CORE_PicklistUtils.getPicklistValues('CORE_Point_of_Contact__c', 'Campaign_Medium__c');
        this.pklPocDevice = CORE_PicklistUtils.getPicklistValues('CORE_Point_of_Contact__c', 'CORE_Device__c');
        this.pklPocNature = CORE_PicklistUtils.getPicklistValues('CORE_Point_of_Contact__c', 'CORE_Nature__c');
        this.pklOppAcademicYear = CORE_PicklistUtils.getPicklistValues('Opportunity', 'CORE_OPP_Academic_Year__c');
        this.pklLearningMaterial = CORE_PicklistUtils.getPicklistValues('Opportunity', 'CORE_OPP_Learning_Material__c');
        this.pklForm = CORE_PicklistUtils.getPicklistValues('CORE_Point_of_Contact__c', 'CORE_Form__c');
        this.pklFormArea = CORE_PicklistUtils.getPicklistValues('CORE_Point_of_Contact__c', 'CORE_Form_area__c');

        dt2 = DateTime.now().getTime();
        System.debug('CORE_LeadFormAcquistionInputValidator.initPicklistValues() : END. [ExecutionTime = ' + (dt2-dt1) + ' ms]');
    }

    public List<CORE_LeadFormAcquisitionWrapper.ErrorWrapper> getFieldsErrors(){
        System.debug('CORE_LeadFormAcquistionInputValidator.getFieldsErrors() : START');
        Long dt1 = DateTime.now().getTime();
        Long dt2;

        List<CORE_LeadFormAcquisitionWrapper.ErrorWrapper> errors = new List<CORE_LeadFormAcquisitionWrapper.ErrorWrapper>();

        CORE_LeadFormAcquisitionWrapper.ErrorWrapper mandatoryFieldError = getMissingParameters();
        if(mandatoryFieldError != null){
            errors.add(mandatoryFieldError);
        }

        CORE_LeadFormAcquisitionWrapper.ErrorWrapper picklistErrors = getPicklistErrors();
        if(picklistErrors != null){
            errors.add(picklistErrors);
        }

        dt2 = DateTime.now().getTime();
        System.debug('CORE_LeadFormAcquistionInputValidator.getFieldsErrors() : END. [ExecutionTime = ' + (dt2-dt1) + ' ms]');
        return errors;
    }
    
    public static List<CORE_LeadAcquisitionBuffer__c> getMissingParameters(List<CORE_LeadAcquisitionBuffer__c> buffers){
        List<CORE_LeadAcquisitionBuffer__c> buffersWithErrors = new List<CORE_LeadAcquisitionBuffer__c>();
        for(CORE_LeadAcquisitionBuffer__c currentRecord : buffers){
            CORE_LeadFormAcquisitionWrapper.PersonAccountWrapper account = CORE_LFA_AccountManagement.getAccountWrapperFromBufferRecord(currentRecord);
            List<CORE_LeadFormAcquisitionWrapper.ConsentWrapper> consents = new List<CORE_LeadFormAcquisitionWrapper.ConsentWrapper>();
            CORE_LeadFormAcquisitionWrapper.OpportunityWrapper mainInterest;
            CORE_LeadFormAcquisitionWrapper.OpportunityWrapper secondInterest;
            CORE_LeadFormAcquisitionWrapper.OpportunityWrapper thirdInterest;
            CORE_LeadFormAcquisitionWrapper.PointOfContactWrapper pointOfContact;
            CORE_LeadFormAcquisitionWrapper.CampaignSubscriptionWrapper campaignSubscription;

            CORE_LFA_OpportunityManagement.OpportunityType opportunityType = CORE_LFA_OpportunityManagement.OpportunityType.MAIN_INTEREST;
            boolean isExistingInterest = CORE_LFA_OpportunityManagement.isExistingInterestValues(currentRecord, opportunityType);
            boolean isPocMandatory = false;
            List<String> missingParameters = new List<String>();

            for(Integer i = 1; i <= 5 ; i++){
                if(CORE_LFA_AccountManagement.isExistingConsentValue(currentRecord,i)){
                    CORE_LeadFormAcquisitionWrapper.ConsentWrapper consent = CORE_LFA_AccountManagement.getConsentWrapperFromBufferRecord(currentRecord, i);
                    consents.add(consent);
                }
            }
            if(isExistingInterest){
                mainInterest = CORE_LFA_OpportunityManagement.getOpportunityWrapperFromBufferRecord(currentRecord, opportunityType);
                isPocMandatory = true;
            }


            opportunityType = CORE_LFA_OpportunityManagement.OpportunityType.SECOND_INTEREST;
            isExistingInterest = CORE_LFA_OpportunityManagement.isExistingInterestValues(currentRecord, opportunityType);
            if(isExistingInterest){
                secondInterest = CORE_LFA_OpportunityManagement.getOpportunityWrapperFromBufferRecord(currentRecord, opportunityType);
                isPocMandatory = true;
            }

            opportunityType = CORE_LFA_OpportunityManagement.OpportunityType.THIRD_INTEREST;
            isExistingInterest = CORE_LFA_OpportunityManagement.isExistingInterestValues(currentRecord, opportunityType);
            if(isExistingInterest){
                thirdInterest = CORE_LFA_OpportunityManagement.getOpportunityWrapperFromBufferRecord(currentRecord, opportunityType);
                isPocMandatory = true;
            }

            pointOfContact = CORE_LFA_OpportunityManagement.getPocWrapperFromBufferRecord(currentRecord);

            if(!String.isEmpty(currentRecord.CORE_CS_CampaignId__c)){
                campaignSubscription = new CORE_LeadFormAcquisitionWrapper.CampaignSubscriptionWrapper ();
                campaignSubscription.campaignId = currentRecord.CORE_CS_CampaignId__c;
            }

            missingParameters.addAll(getAccountMissingParameters(account));
            missingParameters.addAll(getConsentsMissingParameters(consents,false));
            missingParameters.addAll(getOpportunityMissingParameters(mainInterest, true, false, 'mainInterest'));
            missingParameters.addAll(getOpportunityMissingParameters(secondInterest, false, false, 'secondInterest'));
            missingParameters.addAll(getOpportunityMissingParameters(thirdInterest, false, false, 'thirdInterest'));
            missingParameters.addAll(getPOCMissingParameters(pointOfContact, isPocMandatory));
            missingParameters.addAll(getCampaignSubscriptionMissingParameters(campaignSubscription));

            if(missingParameters.size() > 0){
                CORE_LeadAcquisitionBuffer__c bufferWithErrors = new CORE_LeadAcquisitionBuffer__c();
                bufferWithErrors.Id = currentRecord.Id;
                bufferWithErrors.CORE_LastTime__c = System.now();
                bufferWithErrors.CORE_Status__c = CORE_AsyncLFAWorker.ERROR_STATUS;
                bufferWithErrors.CORE_ErrorCode__c = MANDATORY_FIELDS_CODE;
                bufferWithErrors.CORE_ErrorDescription__c = string.join(missingParameters,',');
                buffersWithErrors.add(bufferWithErrors);    
            }
        }

        return buffersWithErrors;
    }
    

    @TestVisible
    private CORE_LeadFormAcquisitionWrapper.ErrorWrapper getMissingParameters(){
        System.debug('CORE_LeadFormAcquistionInputValidator.getMissingParameters() : START');
        Long dt1 = DateTime.now().getTime();
        Long dt2;

        CORE_LeadFormAcquisitionWrapper.ErrorWrapper error;

        CORE_LeadFormAcquisitionWrapper.PersonAccountWrapper account = this.inputParameters.personAccount;
        List<CORE_LeadFormAcquisitionWrapper.ConsentWrapper> consents = this.inputParameters.consents;
        CORE_LeadFormAcquisitionWrapper.OpportunityWrapper mainInterest = this.inputParameters.mainInterest;
        CORE_LeadFormAcquisitionWrapper.OpportunityWrapper secondInterest = this.inputParameters.secondInterest;
        CORE_LeadFormAcquisitionWrapper.OpportunityWrapper thirdInterest = this.inputParameters.thirdInterest;
        CORE_LeadFormAcquisitionWrapper.PointOfContactWrapper pointOfContact = this.inputParameters.pointOfContact;
        CORE_LeadFormAcquisitionWrapper.CampaignSubscriptionWrapper campaignSubscription  = this.inputParameters.campaignSubscription;
        List<String> missingParameters = new List<String>();

        //check account
        missingParameters.addAll(getAccountMissingParameters(account));
        missingParameters.addAll(getConsentsMissingParameters(consents, true));
        missingParameters.addAll(getOpportunityMissingParameters(mainInterest, true, true, 'mainInterest'));
        missingParameters.addAll(getOpportunityMissingParameters(secondInterest, false, true, 'secondInterest'));
        missingParameters.addAll(getOpportunityMissingParameters(thirdInterest, false, true, 'thirdInterest'));
        missingParameters.addAll(getPOCMissingParameters(pointOfContact,true));
        missingParameters.addAll(getCampaignSubscriptionMissingParameters(campaignSubscription));

        if(missingParameters.size() > 0){
            error = new CORE_LeadFormAcquisitionWrapper.ErrorWrapper(
                missingParameters, 
                MANDATORY_FIELDS_MSG, 
                MANDATORY_FIELDS_CODE, 
                this.apiCallDate
            );
        }

        dt2 = DateTime.now().getTime();
        System.debug('CORE_LeadFormAcquistionInputValidator.getMissingParameters() : END. [ExecutionTime = ' + (dt2-dt1) + ' ms]');

        return error;
    }

    @TestVisible
    private CORE_LeadFormAcquisitionWrapper.ErrorWrapper getPicklistErrors(){
        System.debug('CORE_LeadFormAcquistionInputValidator.getPicklistErrors() : START');
        Long dt1 = DateTime.now().getTime();
        Long dt2;

        CORE_LeadFormAcquisitionWrapper.ErrorWrapper error;

        CORE_LeadFormAcquisitionWrapper.PersonAccountWrapper account = this.inputParameters.personAccount;
        List<CORE_LeadFormAcquisitionWrapper.ConsentWrapper> consents = this.inputParameters.consents;
        CORE_LeadFormAcquisitionWrapper.OpportunityWrapper mainInterest = this.inputParameters.mainInterest;
        CORE_LeadFormAcquisitionWrapper.OpportunityWrapper secondInterest = this.inputParameters.secondInterest;
        CORE_LeadFormAcquisitionWrapper.OpportunityWrapper thirdInterest = this.inputParameters.thirdInterest;
        CORE_LeadFormAcquisitionWrapper.PointOfContactWrapper pointOfContact = this.inputParameters.pointOfContact;
        CORE_LeadFormAcquisitionWrapper.CampaignSubscriptionWrapper campaignSubscription  = this.inputParameters.campaignSubscription;

        List<String> invalidPicklistFields = new List<String>();
        if(account != null){
            if(!String.IsBlank(account.salutation) && !this.pklSalutation.contains(account.salutation)){
                invalidPicklistFields.add('personAccount.salutation');
            }
            if(!String.IsBlank(account.languageWebsite) && !this.pklLanguageWebsite.contains(account.languageWebsite)){
                invalidPicklistFields.add('personAccount.languageWebsite');
            }
            if(!String.IsBlank(account.mainNationality) && !this.pklMainNationality.contains(account.mainNationality)){
                invalidPicklistFields.add('personAccount.CORE_Main_Nationality__pc');
            }
            if(this.countryPicklistIsActivated && !String.IsBlank(account.countryCode) && !this.pklCountryCode.contains(account.countryCode)){
                invalidPicklistFields.add('personAccount.countryCode');
            }
            if(this.countryPicklistIsActivated && !String.IsBlank(account.stateCode) && !this.pklStateCode.contains(account.stateCode)){
                invalidPicklistFields.add('personAccount.stateCode');
            }
        }

        if(consents != null && consents.size() > 0){
            Integer i = 0;
            for(CORE_LeadFormAcquisitionWrapper.ConsentWrapper consent : consents){
                if(!String.IsBlank(consent.consentType) && !this.pklConsentType.contains(consent.consentType)){
                    invalidPicklistFields.add('consents[' + i + '].consentType');
                }
                if(!String.IsBlank(consent.channel) && !this.pklConsentChannel.contains(consent.channel)){
                    invalidPicklistFields.add('consents[' + i + '].channel');
                }
                i++;
            }
        }

        if(pointOfContact != null){
            if(!String.IsBlank(pointOfContact.captureChannel) && !this.pklPocCaptureChannel.contains(pointOfContact.captureChannel)){
                invalidPicklistFields.add('pointOfContact.captureChannel');
            }
            if(!String.IsBlank(pointOfContact.campaignMedium) && !this.pklPocCampaignMedium.contains(pointOfContact.campaignMedium)){
                invalidPicklistFields.add('pointOfContact.campaignMedium');
            }
            if(!String.IsBlank(pointOfContact.device) && !this.pklPocDevice.contains(pointOfContact.device)){
                invalidPicklistFields.add('pointOfContact.device');
            }
            if(!String.IsBlank(pointOfContact.nature) && !this.pklPocNature.contains(pointOfContact.nature)){
                invalidPicklistFields.add('pointOfContact.nature');
            }
            if(!String.IsBlank(pointOfContact.form) && !this.pklForm.contains(pointOfContact.form)){
                invalidPicklistFields.add('pointOfContact.form');
            }
            if(!String.IsBlank(pointOfContact.formArea) && !this.pklFormArea.contains(pointOfContact.formArea)){
                invalidPicklistFields.add('pointOfContact.formArea');
            }
        }

        invalidPicklistFields.addAll(getOppPicklistErrors(mainInterest, 'mainInterest'));
        invalidPicklistFields.addAll(getOppPicklistErrors(secondInterest, 'secondInterest'));
        invalidPicklistFields.addAll(getOppPicklistErrors(thirdInterest, 'thirdInterest'));

        if(invalidPicklistFields.size() > 0){
            error = new CORE_LeadFormAcquisitionWrapper.ErrorWrapper(
                invalidPicklistFields, 
                INVALID_PICKLIST_MSG, 
                INVALID_PICKLIST_CODE, 
                this.apiCallDate
            );
        }
        
        dt2 = DateTime.now().getTime();
        System.debug('CORE_LeadFormAcquistionInputValidator.getPicklistErrors() : END. [ExecutionTime = ' + (dt2-dt1) + ' ms]');
        return error;
    }

    @TestVisible
    private List<String> getOppPicklistErrors(CORE_LeadFormAcquisitionWrapper.OpportunityWrapper opportunity, String jsonObjectName){
        List<String> picklistErrors = new List<String>();
        if(opportunity != null){
            if(!String.IsBlank(opportunity.academicYear) && !this.pklOppAcademicYear.contains(opportunity.academicYear)){
                picklistErrors.add(jsonObjectName + '.academicYear');
            }
            if(!String.IsBlank(opportunity.learningMaterial) && !this.pklLearningMaterial.contains(opportunity.learningMaterial)){
                picklistErrors.add(jsonObjectName + '.learningMaterial');
            }
        }

        return picklistErrors;
    }

    @TestVisible
    private static List<String> getAccountMissingParameters(CORE_LeadFormAcquisitionWrapper.PersonAccountWrapper account){
        String jsonObjectName = 'personAccount';
        List<String> mandatoryParameters = new List<String> {'creationDate', 'lastName', 'firstName'};
        List<String> atLeastOneParameters = new List<String> {'mobilePhone', 'phone', 'emailAddress'};
        List<String> missingParameters = new List<String>();

        if(account == null){
            missingParameters.add(jsonObjectName);
        } else {
            for(String mandatoryParameter : mandatoryParameters){
                if(String.IsBlank((String) account.get(mandatoryParameter))){
                    missingParameters.add(jsonObjectName + '.' + mandatoryParameter);
                }
            }

            boolean missingParameter = true;
            for(String parameter : atLeastOneParameters){
                if(!String.IsBlank((String) account.get(parameter))){
                    missingParameter = false;
                    break;
                }
            }

            if(missingParameter){
                missingParameters.add(jsonObjectName + '.' + 'mobilePhone');
                missingParameters.add(jsonObjectName + '.' + 'phone');
                missingParameters.add(jsonObjectName + '.' + 'emailAddress');
            }

        }
        
        return missingParameters;
    }

    @TestVisible
    private static List<String> getConsentsMissingParameters(List<CORE_LeadFormAcquisitionWrapper.ConsentWrapper> consents, Boolean fromSyncLFA){
        String jsonObjectName = 'consents';
        List<String> missingParameters = new List<String>();
        List<String> mandatoryParameters = new List<String> {'consentType', 'channel', 'description', 'businessUnit'};
        
        if(consents == null || consents.size() == 0){
            missingParameters.add(jsonObjectName);
        } else {
            Integer currentConsent = fromSyncLFA ? 0 : 1;
            for(CORE_LeadFormAcquisitionWrapper.ConsentWrapper consent : consents){
                
                for(String mandatoryParameter : mandatoryParameters){
                    if(String.IsBlank((String) consent.get(mandatoryParameter))){
                        String missingParameter = String.format('{0}[{1}].{2}',new List<Object>{jsonObjectName, currentConsent, mandatoryParameter});
                        missingParameters.add(missingParameter);
                    }
                }
                currentConsent++;
            }
        }
        
        return missingParameters;
    }

    @TestVisible
    private static List<String> getOpportunityMissingParameters(CORE_LeadFormAcquisitionWrapper.OpportunityWrapper opportunity, boolean isMainInterrest, boolean isSyncLFA, String jsonObjectName){
        
        List<String> mandatoryParameters = new List<String> {'businessUnit'};
        
        
        List<String> missingParameters = new List<String>();

        if(isSyncLFA && isMainInterrest && opportunity == null){
            missingParameters.add(jsonObjectName);
        } else if(opportunity != null) {
            for(String mandatoryParameter : mandatoryParameters){
                if(String.IsBlank((String) opportunity.get(mandatoryParameter))){
                    missingParameters.add(jsonObjectName + '.' + mandatoryParameter);
                }
            }
        }
        
        return missingParameters;
    }

    @TestVisible
    private static List<String> getPOCMissingParameters(CORE_LeadFormAcquisitionWrapper.PointOfContactWrapper pointOfContact, Boolean isObjectMandatory){
        String jsonObjectName = 'pointOfContact';

        List<String> mandatoryParameters = new List<String> {'creationDate', 'nature', 'captureChannel', 'origin'};
        List<String> missingParameters = new List<String>();

        if(isObjectMandatory && pointOfContact == null){
            missingParameters.add(jsonObjectName);
        } else {
            for(String mandatoryParameter : mandatoryParameters){
                if(String.IsBlank((String) pointOfContact.get(mandatoryParameter))){
                    missingParameters.add(jsonObjectName + '.' + mandatoryParameter);
                }
            }
        }
        
        return missingParameters;
    }

    @TestVisible
    private static List<String> getCampaignSubscriptionMissingParameters(CORE_LeadFormAcquisitionWrapper.CampaignSubscriptionWrapper campaignSubscription){
        String jsonObjectName = 'campaignSubscription';

        List<String> mandatoryParameters = new List<String> {'campaignId'};
        List<String> missingParameters = new List<String>();

        if(campaignSubscription != null) {
            for(String mandatoryParameter : mandatoryParameters){
                if(String.IsBlank((String) campaignSubscription.get(mandatoryParameter))){
                    missingParameters.add(jsonObjectName + '.' + mandatoryParameter);
                }
            }
        }
        
        return missingParameters;
    }
}