@IsTest
public with sharing class CORE_TriggerUtilsTest {
    @IsTest
    public static void setBypassTrigger_Should_Set_bypassTrigger_to_true(){
        System.assertEquals(false, CORE_TriggerUtils.bypassTrigger);
        CORE_TriggerUtils.setBypassTrigger();
        System.assertEquals(true, CORE_TriggerUtils.bypassTrigger);

    }

    @IsTest
    public static void shouldRunTrigger_should_return_true_if_bypassTrigger_is_false() {
        System.assertEquals(false, CORE_TriggerUtils.bypassTrigger);
        System.assertEquals(!CORE_TriggerUtils.bypassTrigger, CORE_TriggerUtils.shouldRunTrigger());
        CORE_TriggerUtils.bypassTrigger = true;
        System.assertEquals(!CORE_TriggerUtils.bypassTrigger, CORE_TriggerUtils.shouldRunTrigger());
    }
}