@IsTest
public class CORE_DataFaker_LeadFormAcquWrapper {
    private static final String ACADEMIC_YEAR = 'CORE_PKL_2022-2023';
    private static final String ADL_LEVEL = 'CORE_PKL_Certificates';

    public static CORE_LeadFormAcquisitionWrapper.GlobalWrapper getGlobalWrapper(){

        CORE_LeadFormAcquisitionWrapper.GlobalWrapper globalWrapper = new CORE_LeadFormAcquisitionWrapper.GlobalWrapper();

        globalWrapper.personAccount = getPersonAccountWrapper();
        globalWrapper.consents = new List<CORE_LeadFormAcquisitionWrapper.ConsentWrapper>{getConsentWrapper()};
        globalWrapper.prospectiveStudentComment = getProspectiveStudentComment();
        globalWrapper.mainInterest = getOpportunityWrapper();
        globalWrapper.secondInterest = getOpportunityWrapper();
        globalWrapper.thirdInterest = getOpportunityWrapper();
        globalWrapper.pointOfContact = getPointOfContactWrapper();
        globalWrapper.campaignSubscription = getCampaignSubscriptionWrapper();
        globalWrapper.academicDiplomaHistory = getAcademicDiplomaHistoryWrapper();

        return globalWrapper;
    } 
    public static CORE_LeadFormAcquisitionWrapper.PersonAccountWrapper getPersonAccountWrapper(){
        Boolean countryPicklistIsActivated = CORE_SobjectUtils.doesFieldExist('User', 'Countrycode');
        CORE_LeadFormAcquisitionWrapper.PersonAccountWrapper personAccount = new CORE_LeadFormAcquisitionWrapper.PersonAccountWrapper();
        personAccount.creationDate = '1624869311000';
		personAccount.salutation = 'Mr.';
		personAccount.lastName = 'lastName';
		personAccount.firstName = 'firstName';
		personAccount.phone = '+33200000000';
		personAccount.mobilePhone = '+33600000000';
		personAccount.emailAddress = 'test@gmail.com';
		personAccount.street = 'street';
		personAccount.postalCode = '75010';
		personAccount.city = 'Paris';
        if(countryPicklistIsActivated){
            personAccount.countryCode = 'FR';
        } else{
            personAccount.countryCode = 'France';
            personAccount.stateCode = 'state';
        }
		personAccount.languageWebsite = 'FRA';
        personAccount.gaUserId = '1234';

        return personAccount;
    }

    public static CORE_LeadFormAcquisitionWrapper.ConsentWrapper getConsentWrapper(){
        CORE_LeadFormAcquisitionWrapper.ConsentWrapper consent = new CORE_LeadFormAcquisitionWrapper.ConsentWrapper();
        consent.consentType = 'CORE_PKL_Processing_of_data_consent';
		consent.channel = 'CORE_PKL_Email';
		consent.description = 'desc';
		consent.optIn = false;
		consent.optInDate = '1624869311000';
		consent.optOut = true;
		consent.optOutDate = '1624869311000';
		consent.division = 'div';
		consent.businessUnit = 'bu';

        return consent;
    }

    public static CORE_LeadFormAcquisitionWrapper.OpportunityWrapper getOpportunityWrapper(){
        CORE_LeadFormAcquisitionWrapper.OpportunityWrapper opp = new CORE_LeadFormAcquisitionWrapper.OpportunityWrapper();
        opp.academicYear = ACADEMIC_YEAR;
		opp.division = 'division';
		opp.businessUnit = 'Bu';
		opp.studyArea = 'studyArea';
		opp.curriculum = 'curriculum';
		opp.level = 'level';
		opp.intake = 'intake';
		opp.brochureShippedByMail = true;
		opp.applicationFormShippedByMail = false;

        return opp;
    }

    public static CORE_LeadFormAcquisitionWrapper.PointOfContactWrapper getPointOfContactWrapper(){
        CORE_LeadFormAcquisitionWrapper.PointOfContactWrapper pointOfContact = new CORE_LeadFormAcquisitionWrapper.PointOfContactWrapper();
        pointOfContact.creationDate = '1624869311000';
		pointOfContact.sourceId = 'sourceId';
		pointOfContact.nature = 'CORE_PKL_Online';
		pointOfContact.captureChannel = 'CORE_PKL_Phone_call';
		pointOfContact.origin = 'origin';
		pointOfContact.campaignMedium = 'CORE_PKL_Sms';
		pointOfContact.campaignSource = 'campaignSource';
		pointOfContact.campaignTerm = 'campaignTerm';
		pointOfContact.campaignContent = 'campaignContent';
		pointOfContact.campaignName = 'campaignName';
		pointOfContact.device = 'CORE_PKL_Phone';
		pointOfContact.firstPage = 'firstPage';
		pointOfContact.ipAddress = '192.168.1.1';
		pointOfContact.latitute = 'latitude';
		pointOfContact.longitude = 'longitude';

        return pointOfContact;
    }

    public static CORE_LeadFormAcquisitionWrapper.CampaignSubscriptionWrapper getCampaignSubscriptionWrapper(){
        CORE_LeadFormAcquisitionWrapper.CampaignSubscriptionWrapper campaignSubscription = new CORE_LeadFormAcquisitionWrapper.CampaignSubscriptionWrapper();
        campaignSubscription.campaignId = 'campaignId';

        return campaignSubscription;
    }

    public static CORE_LeadFormAcquisitionWrapper.AcademicDiplomaHistoryWrapper getAcademicDiplomaHistoryWrapper(){
        CORE_LeadFormAcquisitionWrapper.AcademicDiplomaHistoryWrapper academicDiplomaHistory = new CORE_LeadFormAcquisitionWrapper.AcademicDiplomaHistoryWrapper();
        academicDiplomaHistory.name = 'testName';
        academicDiplomaHistory.level = ADL_LEVEL;
        academicDiplomaHistory.level = ADL_LEVEL;
        academicDiplomaHistory.diplomaDate = System.today();
        
        return academicDiplomaHistory;
    }

    public static String getProspectiveStudentComment(){
        return 'prospectiveStudentComment';
    }

}