/*
Created By Fodil Boudjedien
Created Date 25-04-2022
Test class for
CORE_SISOpportunityApplicationWrapper
*/

@isTest
public with sharing class CORE_SISOpportunityApplicationWrappTest {
    @isTest
    public static void testWrapperClass() {
        // global wrapper
        CORE_SISOpportunityApplicationWrapper.GlobalWrapper gwrapper = new CORE_SISOpportunityApplicationWrapper.GlobalWrapper();
        CORE_SISOpportunityApplicationWrapper.MainInterestWrapper mainWrapper = (CORE_SISOpportunityApplicationWrapper.MainInterestWrapper)JSON.deserialize('{"SIS_OpportunityId" :"00000030" ,"productExternalID":"SIS-GGEIT-IM-MIL-Fashion-FD-MA1-2022-09",'+
                  '"academicYear": "CORE_PKL_2021-2022","division":"IM", "businessUnit": "MD", "studyArea": "FSA", "curriculum": "MAMFL", "level": "TCMA1FA", "intake": "SFA1"}'
                  , CORE_SISOpportunityApplicationWrapper.MainInterestWrapper.class);
        
        CORE_SISOpportunityApplicationWrapper.AcquisitionPointOfContactWrapper pocWrapper = (CORE_SISOpportunityApplicationWrapper.AcquisitionPointOfContactWrapper)JSON.deserialize(
                '{"createdDate": 1621951200000,"sourceId": "IDAPITEST","nature": "CORE_PKL_Online", "captureChannel": "CORE_PKL_Phone_call" ,'+
                '"origin": "TEST API", "campaignMedium": "CORE_PKL_Directory","campaignSource": "source campaign", "campaignTerm": "campaign term",'+
                '"campaignContent": "campaign content","campaignName": "campaign name Test","device": "CORE_PKL_Computer","firstVisitDate": "2021-05-19",'+
                '"firstPage": "https://www.mba-esg.com/master-production-audiovisuelle.html?gclid=CjwKCAjwwbHWBRBWEiwAMIV7E2dd4Vyedrh5-jC1xDkw5dacsDrjtlOuAzYlyFpxZICKfW98nzHG7BoC9PkQAvD_BwE" ,'+
                '"ipAddress": "192.168.1.1", "latitute": "0.132400", "longitude": "34.750000", "salesOrigin" : "testOriginSales"}'
                , CORE_SISOpportunityApplicationWrapper.AcquisitionPointOfContactWrapper.class);
        
        // error wrapper
        CORE_SISOpportunityApplicationWrapper.ErrorWrapper ewrapper = new CORE_SISOpportunityApplicationWrapper.ErrorWrapper('error message','error code');        
        // result wrapper     
        CORE_SISOpportunityApplicationWrapper.ResultWrapper rwrapper = new CORE_SISOpportunityApplicationWrapper.ResultWrapper();
        System.assertEquals('IDAPITEST', pocWrapper.sourceId);
        System.assertEquals('IM', mainWrapper.division);
    }
}