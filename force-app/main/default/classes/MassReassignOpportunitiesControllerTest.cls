@IsTest
public class MassReassignOpportunitiesControllerTest {

    @IsTest
    public static void testReassign(){
        
        Account testAccount = CORE_DataFaker_Account.getStudentAccount('test');
		testAccount = [Select Id, Name from Account Where id = :testAccount.Id];
        CORE_DataFaker_CatalogHierarchy catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test');

        /*Opportunity testOpty = new Opportunity();
        testOpty.StageName = 'Lead';
        testOpty.CORE_OPP_Sub_Status__c = 'Lead - New';
        testOpty.CloseDate = System.today()+1;
        testOpty.AccountId=testAccount.Id;
        testOpty.Name='testOpty';
        testOpty.Type = 'testType';*/
        Opportunity opportunity = CORE_DataFaker_Opportunity.getOpportunity(catalogHierarchy.division.Id, 
            catalogHierarchy.businessUnit.Id, 
            catalogHierarchy.studyArea.Id,
            catalogHierarchy.curriculum.Id,
            catalogHierarchy.level.Id,
            catalogHierarchy.intake.Id, 
            testAccount.Id, 
            'Lead', 
            'Lead - New'
        );
        opportunity.Type = 'testType';
        update opportunity;
        
        MassReassignOpportunitiesController controller = new MassReassignOpportunitiesController();
        controller.refreshOptyList();
        controller.filterId = controller.listviewoptions[1].getValue();
        controller.refreshOptyList();
        controller.searchRecord.stageName = 'Lead';
        controller.helperRecord.ClosedDate_From__c=System.today();
        controller.helperRecord.ClosedDate_To__c=System.today()+2;
        controller.helperRecord.From__c=System.today();
        controller.helperRecord.To__c=System.today()+1;
        controller.searchRecord.Type = 'testType';
        controller.refreshOptyListBySearch();
        
        System.assert(controller.optyList.size()>0);
        
        controller.optyList[0].selected = true;
        controller.helperRecord.Assign_to__c = UserInfo.getUserId();
        controller.Assign();
        
        
        
    }
}