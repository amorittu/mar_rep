public class CORE_OnlineShop_Constants {

    //CORE Prefix
    public static final String PREFIX_CORE = 'CORE_';

    //PARAM values
     public static final String PARAM_ONLINESHOP_ID = 'onlineShopId';

    //API SUCCESS STATUS 
    public static final String ERROR_STATUS = 'KO';
    public static final String SUCCESS_STATUS = 'OK';

    //API SUCCESS MESSAGES
    public static final String MSG_SUCCESSFUL = 'Opportunity updated successfully';

    //API ERROR MESSAGES
    public static final String MSG_MISSING_PARAMETERS = 'Missing mandatory parameter : ';
    public static final String MSG_INVALID_FIELDS = 'INVALID FIELD IN REQUEST';
    public static final String MSG_OPPORTUNITY_NOT_FOUND = 'Opportunity not found';

    /* OPPORTUNITY OBJECT */
    //recordTypes
    public static final String OPP_RECORDTYPE_ENROLLMENT = 'CORE_Enrollment';

    //StageName
    public static final String PKL_OPP_STAGENAME_PROSPECT = 'Prospect';
    public static final String PKL_OPP_STAGENAME_APPLICANT = 'Applicant';
    public static final String PKL_OPP_STAGENAME_REGISTERED = 'Registered';

    //Sub-status
    public static final String PKL_OPP_SUBSTATUS_PROSPECTNEW = 'Prospect - New';
    public static final String PKL_OPP_SUBSTATUS_APPCOMPELIGIBLE = 'Applicant - Application Completed / Eligible';
    public static final String PKL_OPP_SUBSTATUS_REGFEESFULLYPAID = 'Registered - Fees Fully Paid';
}