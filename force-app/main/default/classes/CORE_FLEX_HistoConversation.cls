public class CORE_FLEX_HistoConversation {

  public class Conversation {
    @AuraEnabled public String Content;
    @AuraEnabled public DateTime CreatedDate;
    @AuraEnabled public String Type;
    @AuraEnabled public String Agent;
    @AuraEnabled public Integer Duration;
    @AuraEnabled public String CustomerPhone;
    @AuraEnabled public String TwilioPhone;
    @AuraEnabled public String CallDisposition;
    @AuraEnabled public String UrlFlex;
  }

  @AuraEnabled(cacheable=true)
  public static String find(Id recordId, String customerPhone, String twilioPhone) {
    system.debug('find histoconv with '+recordId+', customerPhone:'+customerPhone+', twilioPhone:'+twilioPhone);
    String query = 'SELECT CORE_FLEX_HistoConversation__c FROM Task '+
    'WHERE (WhoId=:recordId OR WhatId=:recordId) AND ';
    if(customerPhone!='All') {
      query+='CORE_Customer_phone_used__c=:customerPhone AND ';
    }
    if(twilioPhone!='All') {
      query+='CORE_Galileo_phone_used__c=:twilioPhone AND ';
    }
    query +='CORE_FLEX_HistoConversation__c!=NULL ORDER BY CreatedDate DESC LIMIT 1';
    Task[] tasks = Database.query(query);
    if(tasks.Size()>0) {
      return tasks[0].CORE_FLEX_HistoConversation__c;            
    } else {
      return null;
    }
  }

  @AuraEnabled(cacheable=false)
  public static Conversation[] loadContent(Id convId, String customerPhone, String twilioPhone, Integer offset, Integer max) {
    system.debug('loadContent: id :'+convId+', offset:'+offset);
    Task[] ts = [SELECT CORE_Galileo_phone_used__c, CORE_Customer_phone_used__c,CreatedDate,Type,WhoId,WhatId,CallDurationInSeconds, CallDisposition FROM Task WHERE CORE_FLEX_HistoConversation__c=:convId LIMIT 1];
    Task task = null;
    if(ts.size()>0) {
      task = ts[0];
    } else {
      return null;
    }  
    if(customerPhone==null && twilioPhone==null) {
      system.debug('retrieve phones from conv');
      customerPhone = task.CORE_Customer_phone_used__c;
      twilioPhone = task.CORE_Galileo_phone_used__c;
    }
    string whoId = task.WhoId;
    string whatId = task.WhatId;
    Datetime createdDate = task.CreatedDate;

    system.debug('found task '+task+', gphone: '+twilioPhone+', cphone:'+customerPhone);
    List<Id> convIds = new List<Id>();
    Map<Id, Task> tasks = new Map<Id, Task>();
    if(twilioPhone!=null && customerPhone!=null) {
      String query = 'SELECT CORE_FLEX_HistoConversation__c,Type,CallDurationInSeconds,CORE_Galileo_phone_used__c, CORE_Customer_phone_used__c,CallDisposition FROM Task '+
      'WHERE ';
      if(customerPhone!='All') {
        query+='CORE_Customer_phone_used__c=:customerPhone AND ';
      }
      if(twilioPhone!='All') {
        query+='CORE_Galileo_phone_used__c=:twilioPhone AND ';
      }      
      //query += 'WhoId=:whoId AND WhatId=:whatId ';
      query += 'WhatId=:whatId ';
      if(customerPhone!='All' || twilioPhone!='All') {
        query += 'AND CreatedDate <= :createdDate ';
      }
      query += 'ORDER BY CreatedDate DESC LIMIT :max OFFSET :offset';
      system.debug('query:'+query);
      for(Task t : Database.query(query)) {
        convIds.add(t.CORE_FLEX_HistoConversation__c);
        tasks.put(t.CORE_FLEX_HistoConversation__c, t);                              
      }            
    } else if(offset==0){
      convIds.add(convId);
      tasks.put(convId, task);
    }
    system.debug('found '+convIds);
    
    CORE_FLEX_Settings__c setting = CORE_FLEX_Settings__c.getOrgDefaults();
    string urlFlex = setting.UrlFlex__c;
    List<Conversation> result = new List<Conversation>();
    for(CORE_FLEX_HistoConversation__c conv : [
        SELECT Id, Content__c, CreatedDate,Owner.Name 
        FROM CORE_FLEX_HistoConversation__c WHERE Id IN :convIds ORDER BY CreatedDate DESC
      ]) {
      Conversation c = new Conversation();
      Task t = tasks.get(conv.Id);
      c.Content = conv.Content__c;
      c.CreatedDate = conv.CreatedDate;
      c.Type = t.Type;
      c.Duration  = t.CallDurationInSeconds;            
      c.Agent = conv.Owner.Name;
      c.CustomerPhone = t.CORE_Customer_phone_used__c;
      c.TwilioPhone = t.CORE_Galileo_phone_used__c;
      c.CallDisposition = t.CallDisposition;
      c.UrlFlex = urlFlex;
      result.add(c);
    }
    system.debug('result '+result);
    return result;
  }
}