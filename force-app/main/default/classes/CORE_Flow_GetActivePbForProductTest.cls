@IsTest
public class CORE_Flow_GetActivePbForProductTest {
    @IsTest 
    public static void getPricebooksShouldReturnListOfActivePricebookInSameAcademicYear() {
        CORE_DataFaker_CatalogHierarchy catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test');
        Id standardPbId = CORE_DataFaker_PriceBook.getStandardPricebookId();
        Pricebook2 pb = CORE_DataFaker_PriceBook.getPricebook('test');
        CORE_PriceBook_BU__c pricebookBu = CORE_DataFaker_PriceBook.getPriceBookBu(pb.Id, catalogHierarchy.businessUnit.Id, 'test');
        PricebookEntry pbEntry0 = CORE_DataFaker_PriceBook.getPriceBookEntry(catalogHierarchy.product.Id, standardPbId,1.0);

        PricebookEntry pbEntry = CORE_DataFaker_PriceBook.getPriceBookEntry(catalogHierarchy.product.Id, pb.Id,1.1);

        CORE_Flow_GetActivePricebookForProduct.Parameter parameter = new CORE_Flow_GetActivePricebookForProduct.Parameter();
        parameter.relatedProductId = catalogHierarchy.product.Id;
        parameter.relatedAcademicYear = pricebookBu.CORE_Academic_Year__c;
        List<CORE_Flow_GetActivePricebookForProduct.Parameter> params = new List<CORE_Flow_GetActivePricebookForProduct.Parameter> {parameter};
        List<CORE_Flow_GetActivePricebookForProduct.Result> results = CORE_Flow_GetActivePricebookForProduct.getPricebooks(params);

        System.assertEquals(1, results.size());
        CORE_Flow_GetActivePricebookForProduct.Result result = results.get(0);
        System.assertEquals(1, result.pricebookIds.size());
        System.assertEquals(pb.Id, result.pricebookIds.get(0));
        System.assertEquals('(\'' + pb.Id + '\')', result.pricebookIdsAsString);

        Pricebook2 pb2 = CORE_DataFaker_PriceBook.getPricebook('test2');
        CORE_DataFaker_CatalogHierarchy catalogHierarchy2 = new CORE_DataFaker_CatalogHierarchy('test2');
        PricebookEntry pbEntry02 = CORE_DataFaker_PriceBook.getPriceBookEntry(catalogHierarchy2.product.Id, standardPbId,1.0);
        PricebookEntry pbEntry2 = CORE_DataFaker_PriceBook.getPriceBookEntry(catalogHierarchy2.product.Id, pb2.Id,2.2);

        parameter.relatedProductId = catalogHierarchy2.product.Id;
        parameter.relatedAcademicYear = pricebookBu.CORE_Academic_Year__c;

        params = new List<CORE_Flow_GetActivePricebookForProduct.Parameter> {parameter};
        results = CORE_Flow_GetActivePricebookForProduct.getPricebooks(params);

        System.assertEquals(1,results.size());
        result = results.get(0);
        System.assertEquals(0,result.pricebookIds.size());
        System.assertEquals(null,result.pricebookIdsAsString);
    }
}