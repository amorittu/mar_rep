/**
 * Created by SaverioTrovatoNigith on 19/04/2022.
 */

@IsTest
private class IT_InvocableGetRoleTypeMDTTest {
    @IsTest
    static void testBehavior() {
        User u = [SELECT Id, UserRoleId, UserRole.DeveloperName FROM User WHERE Id = :System.UserInfo.getUserId()];
        List<IT_InvocableGetRoleTypeMDT.FlowInputs> inputs = new List<IT_InvocableGetRoleTypeMDT.FlowInputs>();
        Test.startTest();
        IT_InvocableGetRoleTypeMDT.FlowInputs input = new IT_InvocableGetRoleTypeMDT.FlowInputs();
        input.userId = u.Id;
        inputs.add(input);
        IT_InvocableGetRoleTypeMDT.getRoleTypeMDT(inputs);
        Test.stopTest();
    }
}