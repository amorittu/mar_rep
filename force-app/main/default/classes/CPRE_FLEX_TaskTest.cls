@IsTest
public with sharing class CPRE_FLEX_TaskTest {
    private static final String USERNAME_TEST = 'usertest@Flextest.com';
    private static final String API_PROFILE = 'CORE_API_Profile';
    private static final String FLEX_PERMISSION_SET = 'CORE_API_Twilio';

    @TestSetup
    static void makeData(){
        Id profileId = [SELECT Id FROM Profile WHERE Name = :API_PROFILE LIMIT 1].Id;

        User usr = new User(
            Username = USERNAME_TEST,
            LastName = 'TEST_USER',
            Email = 'testemail@yopmail.com',
            Alias = 'T', 
            CommunityNickname = 'T',
            TimeZoneSidKey = 'Europe/Paris',
            LocaleSidKey = 'fr_FR_EURO',
            EmailEncodingKey = 'ISO-8859-1', 
            ProfileId = profileId,
            LanguageLocaleKey = 'fr'      
        );
        database.insert(usr, true);

        Task task = new Task(subject = 'testTask');
        insert task;
        
    }

    @IsTest
    public static void execute_Should_return_an_error_if_fields_does_not_exist() {
        User usr = [SELECT Id FROM User WHERE Username = :USERNAME_TEST LIMIT 1];
        Task task = [SELECT Id FROM Task LIMIT 1];

        List<PermissionSetAssignment> psaList = new List<PermissionSetAssignment>();

        for(PermissionSet ps : [SELECT Id FROM PermissionSet WHERE Name = :FLEX_PERMISSION_SET]){
            psaList.add(
                new PermissionSetAssignment(AssigneeId = usr.id, PermissionSetId = ps.Id)
            );
        }
        
        database.insert(psaList, true);

        System.runAs(usr){
            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
            req.requestURI = '/services/apexrest/twilio/Task/' + task.Id;
            req.httpMethod = 'PATCH';

            Map<String,Object> parameters = new Map<String,Object>();
            parameters.put('invalidField', 8);
            req.requestbody = Blob.valueOf(JSON.serialize(parameters));

            RestContext.request = req;
            RestContext.response= res;
            
            Test.startTest();
            CORE_FLEX_Task.execute();
            Test.stopTest();

            System.assertEquals(400, res.statusCode);
        }
    }

    @IsTest
    public static void execute_Should_return_an_error_if_taskId_is_not_sent() {
        User usr = [SELECT Id FROM User WHERE Username = :USERNAME_TEST LIMIT 1];
        Task task = [SELECT Id FROM Task LIMIT 1];

        List<PermissionSetAssignment> psaList = new List<PermissionSetAssignment>();

        for(PermissionSet ps : [SELECT Id FROM PermissionSet WHERE Name = :FLEX_PERMISSION_SET]){
            psaList.add(
                new PermissionSetAssignment(AssigneeId = usr.id, PermissionSetId = ps.Id)
            );
        }
        
        database.insert(psaList, true);

        System.runAs(usr){
            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
            req.requestURI = '/services/apexrest/twilio/Task/';
            req.httpMethod = 'PATCH';

            Map<String,Object> parameters = new Map<String,Object>();
            parameters.put('Description', 'descTest');
            req.requestbody = Blob.valueOf(JSON.serialize(parameters));

            RestContext.request = req;
            RestContext.response= res;
            
            Test.startTest();
            CORE_FLEX_Task.execute();
            Test.stopTest();

            System.assertEquals(400, res.statusCode);
        }
    }

    @IsTest
    public static void execute_Should_return_an_error_if_json_is_empty() {
        User usr = [SELECT Id FROM User WHERE Username = :USERNAME_TEST LIMIT 1];
        Task task = [SELECT Id FROM Task LIMIT 1];

        List<PermissionSetAssignment> psaList = new List<PermissionSetAssignment>();

        for(PermissionSet ps : [SELECT Id FROM PermissionSet WHERE Name = :FLEX_PERMISSION_SET]){
            psaList.add(
                new PermissionSetAssignment(AssigneeId = usr.id, PermissionSetId = ps.Id)
            );
        }
        
        database.insert(psaList, true);
        
        System.runAs(usr){
            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
            req.requestURI = '/services/apexrest/twilio/Task/' + task.Id;
            req.httpMethod = 'PATCH';

            Map<String,Object> parameters = new Map<String,Object>();
            parameters.put('Description', 'descTest');
            req.requestbody = Blob.valueOf('');

            RestContext.request = req;
            RestContext.response= res;
            
            Test.startTest();
            CORE_FLEX_Task.execute();
            Test.stopTest();

            System.assertEquals(400, res.statusCode);
        }
    }

    @IsTest
    public static void execute_Should_return_a_success_if_all_params_are_ok() {
        User usr = [SELECT Id FROM User WHERE Username = :USERNAME_TEST LIMIT 1];
        Task task = [SELECT Id FROM Task LIMIT 1];

        List<PermissionSetAssignment> psaList = new List<PermissionSetAssignment>();

        for(PermissionSet ps : [SELECT Id FROM PermissionSet WHERE Name = :FLEX_PERMISSION_SET]){
            psaList.add(
                new PermissionSetAssignment(AssigneeId = usr.id, PermissionSetId = ps.Id)
            );
        }
        
        database.insert(psaList, true);
        
        System.runAs(usr){
            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
            req.requestURI = '/services/apexrest/twilio/Task/' + task.Id;
            req.httpMethod = 'PATCH';

            Map<String,Object> parameters = new Map<String,Object>();
            parameters.put('Description', 'descTest');
            req.requestbody = Blob.valueOf(JSON.serialize(parameters));

            RestContext.request = req;
            RestContext.response= res;
            
            Test.startTest();
            CORE_FLEX_Task.execute();
            Test.stopTest();

            System.assertEquals(200, res.statusCode);
        }
    }
}