global class CORE_Livestorm_CreateCMMethod implements Schedulable  {
    global static void execute(SchedulableContext sc){
        List<Campaign> campaigns = [SELECT Id, CORE_Number_of_Member__c, CORE_Number_of_Member_in_Livestorm__c, CORE_Start_Date_and_hour__c FROM Campaign WHERE CORE_Online_event__c = TRUE AND CORE_Livestorm_Event_Id__c != NULL];

        List<Id> campaignIds = new List<Id>();
        for(Campaign campaign : campaigns){
            if (campaign.CORE_Number_of_Member_in_Livestorm__c < campaign.CORE_Number_of_Member__c && campaign.CORE_Start_Date_and_hour__c > date.today()){
                campaignIds.add(campaign.Id);
            }
        }

        if(campaignIds.size() > 0){
            List<CampaignMember> campaignMembers = [SELECT Id FROM CampaignMember WHERE CampaignId in :campaignIds AND (CORE_Livestorm_People_Id__c = NULL OR CORE_Livestorm_People_Id__c = '')];

            Set<Id> campaignMemberIds = new Set<Id>();
            for(CampaignMember campaignMember : campaignMembers){
                campaignMemberIds.add(campaignMember.Id);
            }
    
            if (campaignMemberIds.size() > 0){
                CORE_Livestorm_CreateCampaignMember campaignMemberBatchableCreate = new CORE_Livestorm_CreateCampaignMember(campaignMemberIds);
                database.executebatch(campaignMemberBatchableCreate, 25);
            }
        }
    }
}