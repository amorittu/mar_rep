@IsTest
public class CORE_DataFaker_CampaignMember {
    public static CampaignMember getCampaignMember(Id campaignId, Id contactId, String status) {
        CampaignMember campaignMember = new CampaignMember(
            CampaignId = campaignId,
            ContactId = contactId,
            CORE_Campaign_Statut__c = status
        );

        insert campaignMember;
        return campaignMember;
    }

}