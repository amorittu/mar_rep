public without sharing class CORE_OptOutSchedulableUpdate implements Schedulable {
    public void execute(SchedulableContext sC) {
        CORE_OptOutBatchableUpdate optOutBatchableUpdate = new CORE_OptOutBatchableUpdate();
        database.executebatch(optOutBatchableUpdate);
     }
}