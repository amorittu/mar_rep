@IsTest
public class CORE_OpportunityWithdrawSubStatusTest {
	@IsTest
	public static void ShouldResturnDontKnowShowedUpSubStatus() {
        List<CORE_OpportunityWithdrawSubStatus.OpportunityWithdrawStatusParameters> params = new List<CORE_OpportunityWithdrawSubStatus.OpportunityWithdrawStatusParameters>();
        CORE_OpportunityWithdrawSubStatus.OpportunityWithdrawStatusParameters param = new CORE_OpportunityWithdrawSubStatus.OpportunityWithdrawStatusParameters();

        param.startStatus = 'Enrolled';
        param.showedUp = 'CORE_PKL_We don\'t know yet';
        params.add(param);

        List<List<String>> result = CORE_OpportunityWithdrawSubStatus.getOpportunityWithdrawStatus(params);

        System.assertEquals(1, result.get(0).size());
        System.assertEquals('Enrolled - Exit - Waiting for decision', result.get(0).get(0));
    }

    @IsTest
	public static void ShouldResturnDontKnowShowedUpSubStatusReEnrolled() {
        List<CORE_OpportunityWithdrawSubStatus.OpportunityWithdrawStatusParameters> params = new List<CORE_OpportunityWithdrawSubStatus.OpportunityWithdrawStatusParameters>();
        CORE_OpportunityWithdrawSubStatus.OpportunityWithdrawStatusParameters param = new CORE_OpportunityWithdrawSubStatus.OpportunityWithdrawStatusParameters();

        param.startStatus = 'Re-enrolled';
        param.showedUp = 'CORE_PKL_We don\'t know yet';
        params.add(param);

        List<List<String>> result = CORE_OpportunityWithdrawSubStatus.getOpportunityWithdrawStatus(params);

        System.assertEquals(1, result.get(0).size());
        System.assertEquals('Re-enrolled - Exit - Waiting for decision', result.get(0).get(0));
    }

    @IsTest
	public static void ShouldResturnRegisteredNoShowedUpSubStatus() {
        List<CORE_OpportunityWithdrawSubStatus.OpportunityWithdrawStatusParameters> params = new List<CORE_OpportunityWithdrawSubStatus.OpportunityWithdrawStatusParameters>();
        CORE_OpportunityWithdrawSubStatus.OpportunityWithdrawStatusParameters param = new CORE_OpportunityWithdrawSubStatus.OpportunityWithdrawStatusParameters();

        param.startStatus = 'Registered';
        param.showedUp = 'CORE_PKL_No';
        params.add(param);

        List<List<String>> result = CORE_OpportunityWithdrawSubStatus.getOpportunityWithdrawStatus(params);

        System.assertEquals(13, result.get(0).size());
    }

    @IsTest
	public static void ShouldResturnRegisteredNoShowedUpSubStatusReRegistered() {
        List<CORE_OpportunityWithdrawSubStatus.OpportunityWithdrawStatusParameters> params = new List<CORE_OpportunityWithdrawSubStatus.OpportunityWithdrawStatusParameters>();
        CORE_OpportunityWithdrawSubStatus.OpportunityWithdrawStatusParameters param = new CORE_OpportunityWithdrawSubStatus.OpportunityWithdrawStatusParameters();

        param.startStatus = 'Re-registered';
        param.showedUp = 'CORE_PKL_No';
        params.add(param);

        List<List<String>> result = CORE_OpportunityWithdrawSubStatus.getOpportunityWithdrawStatus(params);

        System.assertEquals(13, result.get(0).size());
    }

    @IsTest
	public static void ShouldResturnEnrolledNoShowedUpSubStatus() {
        List<CORE_OpportunityWithdrawSubStatus.OpportunityWithdrawStatusParameters> params = new List<CORE_OpportunityWithdrawSubStatus.OpportunityWithdrawStatusParameters>();
        CORE_OpportunityWithdrawSubStatus.OpportunityWithdrawStatusParameters param = new CORE_OpportunityWithdrawSubStatus.OpportunityWithdrawStatusParameters();

        param.startStatus = 'Enrolled';
        param.showedUp = 'CORE_PKL_No';
        params.add(param);

        List<List<String>> result = CORE_OpportunityWithdrawSubStatus.getOpportunityWithdrawStatus(params);

        System.assertEquals(11, result.get(0).size());
    }

    @IsTest
	public static void ShouldResturnEnrolledNoShowedUpSubStatusReEnrolled() {
        List<CORE_OpportunityWithdrawSubStatus.OpportunityWithdrawStatusParameters> params = new List<CORE_OpportunityWithdrawSubStatus.OpportunityWithdrawStatusParameters>();
        CORE_OpportunityWithdrawSubStatus.OpportunityWithdrawStatusParameters param = new CORE_OpportunityWithdrawSubStatus.OpportunityWithdrawStatusParameters();

        param.startStatus = 'Re-enrolled';
        param.showedUp = 'CORE_PKL_No';
        params.add(param);

        List<List<String>> result = CORE_OpportunityWithdrawSubStatus.getOpportunityWithdrawStatus(params);

        System.assertEquals(11, result.get(0).size());
    }

    @IsTest
	public static void ShouldResturnYesShowedUpSubStatus() {
        List<CORE_OpportunityWithdrawSubStatus.OpportunityWithdrawStatusParameters> params = new List<CORE_OpportunityWithdrawSubStatus.OpportunityWithdrawStatusParameters>();
        CORE_OpportunityWithdrawSubStatus.OpportunityWithdrawStatusParameters param = new CORE_OpportunityWithdrawSubStatus.OpportunityWithdrawStatusParameters();

        param.startStatus = 'Enrolled';
        param.showedUp = 'CORE_PKL_Yes';
        params.add(param);

        List<List<String>> result = CORE_OpportunityWithdrawSubStatus.getOpportunityWithdrawStatus(params);

        System.assertEquals(8, result.get(0).size());
    }

    @IsTest
	public static void ShouldResturnYesShowedUpSubStatusReEnrolled() {
        List<CORE_OpportunityWithdrawSubStatus.OpportunityWithdrawStatusParameters> params = new List<CORE_OpportunityWithdrawSubStatus.OpportunityWithdrawStatusParameters>();
        CORE_OpportunityWithdrawSubStatus.OpportunityWithdrawStatusParameters param = new CORE_OpportunityWithdrawSubStatus.OpportunityWithdrawStatusParameters();

        param.startStatus = 'Re-enrolled';
        param.showedUp = 'CORE_PKL_Yes';
        params.add(param);

        List<List<String>> result = CORE_OpportunityWithdrawSubStatus.getOpportunityWithdrawStatus(params);

        System.assertEquals(8, result.get(0).size());
    }
}