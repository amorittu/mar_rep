/*******************************************************/
/**     @Author: Valentin Pitel                       **/
/**     @UnitTestClass: CORE_CatalogHierarchyModelTest**/
/*******************************************************/
/**  Descritption :                                   **/
/**  Model class used to have all catalog hierarchy   **/
/**  store in one structured object                   **/
/*******************************************************/
public class CORE_CatalogHierarchyModel {
    public CORE_Division__c division {get;set;}
    public CORE_Business_Unit__c businessUnit {get;set;}
    public CORE_Study_Area__c studyArea {get;set;}
    public CORE_Curriculum__c curriculum {get;set;}
    public CORE_Level__c level {get;set;}
    public CORE_Intake__c intake {get;set;}
    public Product2 product {get;set;}
    public Boolean isFullHierarchy {get;set;}
    public Id priceBook2Id {get;set;}
    public boolean isSuspect {get;set;}
    public boolean useExternalIds {get;set;}
    public String invalidReference {get;set;}
    public CORE_LeadFormAcquisitionWrapper.OpportunityWrapper interest {get;set;}
    
    public CORE_CatalogHierarchyModel() {
        this.division = new CORE_Division__c();
        this.businessUnit = new CORE_Business_Unit__c();
        this.studyArea = new CORE_Study_Area__c();
        this.curriculum = new CORE_Curriculum__c();
        this.level = new CORE_Level__c();
        this.intake = new CORE_Intake__c();
        this.product = new Product2();
        this.isFullHierarchy = false;
        this.isSuspect = false;
        this.useExternalIds = true;
    }

    public CORE_CatalogHierarchyModel(Id divisionId, Id businessUnitId, Id studyAreaId, Id curriculumId, Id levelId, Id intakeId, Id productId) {
        this.division = new CORE_Division__c(Id = divisionId);
        this.businessUnit = new CORE_Business_Unit__c(Id = businessUnitId);
        this.studyArea = new CORE_Study_Area__c(Id = studyAreaId);
        this.curriculum = new CORE_Curriculum__c(Id = curriculumId);
        this.level = new CORE_Level__c(Id = levelId);
        this.intake = new CORE_Intake__c(Id = intakeId);
        this.product = new Product2(Id = productId);
        this.isFullHierarchy = false;
        this.isSuspect = false;
        this.useExternalIds = false;
    }

    public CORE_CatalogHierarchyModel(String divisionExternalId, String businessUnitExternalId, String studyAreaExternalId, String curriculumExternalId, String levelExternalId, String intakeExternalId) {
        this.division = new CORE_Division__c(CORE_Division_ExternalId__c = divisionExternalId);
        this.businessUnit = new CORE_Business_Unit__c(CORE_Business_Unit_ExternalId__c = businessUnitExternalId);
        this.studyArea = new CORE_Study_Area__c(CORE_Study_Area_ExternalId__c = studyAreaExternalId);
        this.curriculum = new CORE_Curriculum__c(CORE_Curriculum_ExternalId__c = curriculumExternalId);
        this.level = new CORE_Level__c(CORE_Level_ExternalId__c = levelExternalId);
        this.intake = new CORE_Intake__c(CORE_Intake_ExternalId__c = intakeExternalId);
        this.product = new Product2();
        this.isFullHierarchy = false;
        this.isSuspect = false;
        this.useExternalIds = true;
    }


}