/**
 * Test class By Fodil 10-05-2022
 */
@isTest
public  class CORE_BusinessHoursUtilsTest {
    @isTest
    public static void testGetNextDateTime() {
        DateTime dtstart = Datetime.newInstance(2022,05,12,10,00,00);
        DateTime dt;
        Test.startTest();
        List<BusinessHours> bhs = [SELECT Id FROM BusinessHours where IsDefault = true LIMIT 1];
       // if(bhs.size()>0){
             dt = CORE_BusinessHoursUtils.getNextDateTime(bhs[0].Id,dtstart,1);
        //}
        Test.stopTest();
        System.assertEquals(dt,dtstart.addHours(1)); // check that adding hour to the first datetime equals the one generating from the business hours

    }
}