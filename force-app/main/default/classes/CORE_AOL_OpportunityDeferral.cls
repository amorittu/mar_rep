@RestResource(urlMapping='/aol/opportunity/deferal')
global class CORE_AOL_OpportunityDeferral {  
    
    public static CORE_AOL_Wrapper.ResultWrapper result { get; set; }
    public static String overrideAcademicYear;

    @HttpPut
    global static CORE_AOL_Wrapper.ResultWrapper opportunityDeferral(String formerAolExternalId,
                                                                     String newAolExternalId, 
                                                                     CORE_AOL_Wrapper.InterestWrapper interest, 
                                                                     CORE_AOL_Wrapper.AOLProgressionFields aolProgressionFields) {

        Savepoint sp = Database.setSavepoint();

        RestResponse res = RestContext.response;                                                                

        // Initiating logs
        Long dt1 = DateTime.now().getTime();
        Long dt2;
        CORE_LogManager logSettings;
        CORE_LOG__c log = new CORE_LOG__c(CORE_API_Name__c = 'AOL');
        Boolean isErrorLog = false;
        logSettings = new CORE_LogManager('AOL');
        CORE_AOL_Wrapper.ResultWrapper result = checkMandatoryParameters(formerAolExternalId, newAolExternalId, interest);

        try {
            if(result != null){
                //missing parameter(s)
                return result;
            }
            res.addHeader('Content-Type', 'application/json');

            //Get new Intake
            CORE_Intake__c intake = (CORE_Intake__c)getRelatedObj(
                'CORE_Intake__c', 
                'Id, CORE_Academic_Year__c',
                'CORE_Intake_ExternalId__c', 
                interest.intake
            );

            String academicYear = (overrideAcademicYear != null) ? overrideAcademicYear :intake?.CORE_Academic_Year__c;

            //Get old AOL Opportunity
            List<Opportunity> oldAolOpp = CORE_AOL_QueryProvider.getAolOpportunities(formerAolExternalId);

            if(oldAolOpp.size() == 0){//No existing Aol Opp => return error
                result = new CORE_AOL_Wrapper.ResultWrapper(
                    CORE_AOL_Constants.ERROR_STATUS, 'No existing AOL Opportunity found', new CORE_AOL_Wrapper.OpportunityWrapper()
                );

                return result;
            }
            else{
                oldAolOpp[0].CORE_OPP_Sub_Status__c = String.format(
                    CORE_AOL_Constants.PKL_OPP_SUBSTATUS_TERM_DEFERRAL, 
                    new List<String> { oldAolOpp[0].StageName }
                );
            }

            /*system.debug('@opportunityDeferral > oldAolOpp[0].AccountId: ' + oldAolOpp[0].AccountId);
            system.debug('@opportunityDeferral > interest.businessUnit: ' + interest.businessUnit);
            system.debug('@opportunityDeferral > academicYear: ' + academicYear);*/

            //Get existing main opp 
            List<Opportunity> nonAolMainOpp = CORE_AOL_QueryProvider.getExistingMainOpps(oldAolOpp[0].AccountId, interest.businessUnit, academicYear);

            Opportunity newAolOpp = (nonAolMainOpp.size() > 0) ? nonAolMainOpp[0] :oldAolOpp[0].clone(false, true, false, false);
            newAolOpp.CORE_AOL_Deferral__c = true;

            if(nonAolMainOpp.size() > 0){//Main non AOL opportunity with same BU and Academic Year exists
                
                //is Short course process => create new Opp from Main
                if(CORE_AOL_Constants.PKL_PRODUCT_TYPE == newAolOpp.CORE_Main_Product_Interest__r.CORE_Product_Type__c){
                    CORE_AOL_Opportunity.overrideOwnerId = newAolOpp.OwnerId;
                    interest.isMain = true;
                    interest.personAccountId = newAolOpp.AccountId;

                    newAolOpp = CORE_AOL_Opportunity.createOpportunityProcess(newAolExternalId, interest, aolProgressionFields);
                }
                else{

                    newAolOpp = deferalProcess(
                        newAolOpp,
                        oldAolOpp[0],
                        newAolExternalId,
                        intake,
                        interest,
                        aolProgressionFields,
                        true
                    );
                }   
            }
            else{

                newAolOpp = CORE_AOL_GlobalWorker.removeUniqueFields(newAolOpp);
                newAolOpp.OwnerId = oldAolOpp[0].OwnerId;

                newAolOpp = deferalProcess(
                    newAolOpp,
                    oldAolOpp[0],
                    newAolExternalId,
                    intake,
                    interest,
                    aolProgressionFields,
                    true
                );
            }

            CORE_AOL_Wrapper.OpportunityWrapper opp = new CORE_AOL_Wrapper.OpportunityWrapper();

            result = new CORE_AOL_Wrapper.ResultWrapper(CORE_AOL_Constants.SUCCESS_STATUS, '', opp);

            //result.statut = 'OK';
            result.opportunity.Id = newAolOpp.Id;
            result.opportunity.applicationOnlineId = newAolOpp?.CORE_OPP_Application_Online_ID__c;
            result.opportunity.sisOpportunityId = newAolOpp?.CORE_OPP_SIS_Opportunity_Id__c;
            result.opportunity.sisStudentAcademicRecordId = newAolOpp?.CORE_OPP_SIS_Student_Academic_Record_ID__c;
            result.opportunity.accountId = newAolOpp.AccountId;
            res.responseBody = Blob.valueOf(JSON.serialize(result, false));
            res.statusCode = 200;

            system.debug('result: ' + result);
            
        }
        catch(Exception e){
            result = new CORE_AOL_Wrapper.ResultWrapper(CORE_AOL_Constants.ERROR_STATUS, e.getMessage() + ' at ' + e.getStackTraceString(), new CORE_AOL_Wrapper.OpportunityWrapper());
            system.debug('@Error Exception: ' + e.getMessage() + ' at ' + e.getStackTraceString());

            Database.rollback(sp);
        } finally{
            dt2 = DateTime.now().getTime();
            System.debug('CORE_AOL_AcademicDiplomaHistory(urlMapping=\'/aol/academic-diploma-history\') : T0TAL TIME OF EXECUTION = ' + (dt2-dt1) + ' ms');
            // creating log
            log.CORE_Processing_Duration__c =(dt2-dt1);
            log.CORE_HTTP_Status_Code__c = res.statusCode;
            log.CORE_Sent_Body__c = JSON.serialize(result);
            String reponseJSON = '{"formerAolExternalId" : "' + formerAolExternalId + '"newAolExternalId" : "' + newAolExternalId + '",';
            reponseJSON += '"interest" : ' + JSON.serialize(interest) + ',"aolProgressionFields" : ' + JSON.serialize(aolProgressionFields) + '}';
            log.CORE_Received_Body__c =  reponseJSON;
            
            if(!logSettings.isNone()){
                if(logSettings.isAllLogged()){ // is All logged
                    CORE_LogManager.createLog(log);
                }else if(logSettings.isErrorLogged() && isErrorLog){ // is Error
                    CORE_LogManager.createLog(log);
                }
            }
        }
    
        return result;
    }

    public static sObject getRelatedObj(String objAPI, String fields, String externalField, String externalID){
        List<SObject> result = database.query('SELECT ' + fields + ' FROM ' + objAPI + ' WHERE ' + externalField + ' = :externalID LIMIT 1');
        if(result.isEmpty()){
            return null;
        } else {
            return result.get(0);
        }
        //return database.query('SELECT ' + fields + ' FROM ' + objAPI + ' WHERE ' + externalField + ' = :externalID LIMIT 1');
    }

    public static Opportunity deferalProcess(Opportunity opp,
                                             Opportunity oldAolOpp, 
                                             String newAolExternalId, 
                                             CORE_Intake__c intake,
                                             CORE_AOL_Wrapper.InterestWrapper interest, 
                                             CORE_AOL_Wrapper.AOLProgressionFields aolProgressionFields,
                                             Boolean hasMainOpp){

        //Get new BU
        CORE_Business_Unit__c bu = (CORE_Business_Unit__c)getRelatedObj(
            'CORE_Business_Unit__c', 
            'Id',
            'CORE_Business_Unit_ExternalId__c', 
            interest.businessUnit
        );

        system.debug('@deferalProcess > intake: ' + JSON.serializePretty(intake));

        CORE_Study_Area__c studyArea = (CORE_Study_Area__c)getRelatedObj('CORE_Study_Area__c', 'Id', 'CORE_Study_Area_ExternalId__c', interest.studyArea);
        system.debug('@deferalProcess > interest.studyArea: ' + interest.studyArea);
		system.debug('@deferalProcess > studyArea: ' + JSON.serializePretty(studyArea));
                                                 
        CORE_Curriculum__c curriculum = (CORE_Curriculum__c)getRelatedObj('CORE_Curriculum__c', 'Id', 'CORE_Curriculum_ExternalId__c', interest.curriculum);
        CORE_Level__c level = (CORE_Level__c)getRelatedObj('CORE_Level__c', 'Id', 'CORE_Level_ExternalId__c', interest.level);

        opp.CORE_OPP_Academic_Year__c = intake.CORE_Academic_Year__c;

        List<CORE_PriceBook_BU__c> pbBUs = [SELECT Id, CORE_Price_Book__c, CORE_Business_Unit__r.CORE_BU_Technical_User__c
                                    FROM CORE_PriceBook_BU__c 
                                    WHERE CORE_Academic_Year__c = :intake.CORE_Academic_Year__c
                                    AND CORE_Business_Unit__c = :bu.Id LIMIT 1];

        CORE_PriceBook_BU__c pbBU = pbBUs.isEmpty() ? null : pbBUs.get(0);
        //Update Intake & Pricebook from existing Opportunity
        opp.CORE_OPP_Intake__c = intake.Id;
        opp.Pricebook2Id = pbBU?.CORE_Price_Book__c;

        CORE_OpportunityDataMaker dataMaker = new CORE_OpportunityDataMaker();
        
        CORE_CatalogHierarchyModel catalogHierarchy = new CORE_CatalogHierarchyModel(
            null, 
            opp.CORE_OPP_Business_Unit__c, 
            opp.CORE_OPP_Study_Area__c,//studyAreaId, 
            opp.CORE_OPP_Curriculum__c,//curriculumId, 
            opp.CORE_OPP_Level__c,//levelId, 
            opp.CORE_OPP_Intake__c,//intakeId, 
            null//productId
        );

        catalogHierarchy.priceBook2Id = opp.Pricebook2Id;

        catalogHierarchy = CORE_OpportunityDataMaker.getCatalogHierarchy(catalogHierarchy, false);

        Opportunity init_opp = dataMaker.initOpportunity(new Account(Id = opp.AccountId), opp.CORE_OPP_Academic_Year__c, catalogHierarchy, hasMainOpp);

        opp.CORE_OPP_Intake__c = intake.Id;
        opp.CORE_Main_Product_Interest__c = init_opp.CORE_Main_Product_Interest__c;
        opp.CORE_OPP_AOL_Opportunity_Id__c = newAolExternalId;
        opp.StageName = init_opp.StageName;
        opp.CORE_OPP_Sub_Status__c = init_opp.CORE_OPP_Sub_Status__c;                                        

        if(hasMainOpp){

            opp.CORE_OPP_Business_Unit__c = bu.Id;
            opp.CORE_OPP_Study_Area__c = studyArea.Id;
            opp.CORE_OPP_Curriculum__c = curriculum.Id;
            opp.CORE_OPP_Level__c = level.Id;

            opp.CORE_Is_AOL_Retail_Register__c = true;
            opp.CORE_OPP_Application_started__c = date.today();

        }

        //AOL Progression
        if(aolProgressionFields != null){                                 
            opp.CORE_Document_progression__c = aolProgressionFields.documentProgression;
            opp.CORE_First_document_upload_date__c = aolProgressionFields.firstDocumentUploadDate;
            opp.CORE_Last_AOL_event_date__c = aolProgressionFields.lastAolEventDate;
            opp.CORE_OPP_Application_Online_Advancement__c = aolProgressionFields.aolProgression;
		}
        opp.CORE_OPP_Application_Online_ID__c = newAolExternalId;
        opp.CORE_OPP_Application_Online__c = true;

        database.upsert(opp, true);

        //former Aol Opp is added as child to the new Aol Opp
        oldAolOpp.CORE_OPP_Parent_Opportunity__c = opp.Id;
        database.update(oldAolOpp, true);

        //Clear existing opp line items
        List<OpportunityLineItem> oliList = [SELECT Id FROM OpportunityLineItem WHERE OpportunityId = :opp.Id];

        if(oliList.size() > 0){
            database.delete(oliList, true);
        }

        if(!String.IsBlank(opp.CORE_Main_Product_Interest__c) 
            && !String.IsBlank(opp.Pricebook2Id)){

            PricebookEntry pricebookEntry = dataMaker.getProductPricebookEntryByPb(opp.Pricebook2Id, opp.CORE_Main_Product_Interest__c);
            //system.debug('pricebookEntry: ' + JSON.serializePretty(pricebookEntry));

            if(pricebookEntry != NULL){
                OpportunityLineItem oppLineItem = dataMaker.initOppLineItem(opp, pricebookEntry);

                //system.debug('oppLineItem: ' + JSON.serializePretty(oppLineItem));
                database.insert(oppLineItem, true);
            }
        }

        //system.debug('@deferalProcess > opp: ' + JSON.serializePretty(opp));

        return opp;
    }

    @TestVisible
    private static CORE_AOL_Wrapper.ResultWrapper checkMandatoryParameters(String formerAolExternalId,String newAolExternalId, CORE_AOL_Wrapper.InterestWrapper interest){
        List<String> missingParameters = new List<String>();
        Boolean isMissingFormerAolId = CORE_AOL_GlobalWorker.isAolExternalIdParameterMissing(formerAolExternalId);
        Boolean isMissingNewAolId = CORE_AOL_GlobalWorker.isAolExternalIdParameterMissing(newAolExternalId);

        if(isMissingFormerAolId){
            missingParameters.add('formerAolExternalId');
        }
        if(isMissingNewAolId){
            missingParameters.add('newAolExternalId');
        }
        
        missingParameters.addAll(CORE_AOL_ChangeInterest.getInterestMissingParameters(interest));

        CORE_AOL_Wrapper.ResultWrapperGlobal resultTmp = CORE_AOL_GlobalWorker.getMissingParameters(missingParameters);

        if(resultTmp == null){
            return null;
        } else {
            CORE_AOL_Wrapper.ResultWrapper result = new CORE_AOL_Wrapper.ResultWrapper(resultTmp.status, resultTmp.message, null);
            return result;
        }
    }
}