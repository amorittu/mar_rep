public class CORE_DeletedRecordMetadata {
    public CORE_Deleted_Record_Setting__mdt deletedRecordMetadata;

    public CORE_DeletedRecordMetadata() {
        getMetadata();
    }

    @TestVisible
    private void getMetadata(){
        if(Test.isRunningTest()){
            this.deletedRecordMetadata = new CORE_Deleted_Record_Setting__mdt(
                CORE_Delay__c = 14
           );
        }
        
        if(this.deletedRecordMetadata == null){
            this.deletedRecordMetadata = [SELECT CORE_Delay__c FROM CORE_Deleted_Record_Setting__mdt LIMIT 1];
        }
    }
}