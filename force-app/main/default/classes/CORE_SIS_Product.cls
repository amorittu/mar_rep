@RestResource(urlMapping='/sis/product')
global class CORE_SIS_Product {

    @HttpGet
    global static CORE_SIS_Wrapper.ResultWrapperProduct getProduct(){

        RestRequest	request = RestContext.request;

        // Initiating logs
        Long dt1 = DateTime.now().getTime();
        Long dt2;
        CORE_LogManager logSettings;
        CORE_LOG__c log = new CORE_LOG__c(CORE_API_Name__c = 'SIS');
        Boolean isErrorLog = false;
        logSettings = new CORE_LogManager('SIS');

        String oppProdId = request.params.get(CORE_SIS_Constants.PARAM_OPPPROD_ID);
        String additionalFields = request.params.get(CORE_SIS_Constants.PARAM_ADD_FIELDS);

        CORE_SIS_Wrapper.ResultWrapperProduct result = checkMandatoryParameters(oppProdId);

        try{
            if(result != null){
                return result;
            }
            List<OpportunityLineItem> productsList = CORE_SIS_QueryProvider.getProduct(oppProdId, additionalFields);

            result = new CORE_SIS_Wrapper.ResultWrapperProduct(productsList[0], CORE_SIS_Constants.SUCCESS_STATUS);

        } catch(Exception e){
            system.debug('@getProduct > exception error: ' + e.getMessage() + ' at ' + e.getStackTraceString());
            result = new CORE_SIS_Wrapper.ResultWrapperProduct(CORE_SIS_Constants.ERROR_STATUS, e.getMessage() + ' at ' + e.getStackTraceString());
        } finally{
            dt2 = DateTime.now().getTime();
            System.debug('CORE_AOL_AcademicDiplomaHistory(urlMapping=\'/aol/academic-diploma-history\') : T0TAL TIME OF EXECUTION = ' + (dt2-dt1) + ' ms');
            // creating log
            log.CORE_Processing_Duration__c =(dt2-dt1);
            log.CORE_HTTP_Status_Code__c = result.status == 'KO' ? 400 : result.status == 'OK' ? 200 : 500;
            log.CORE_Sent_Body__c = JSON.serialize(result);
            log.CORE_Received_Body__c =  request?.requestBody?.toString();
            
            if(!logSettings.isNone()){
                if(logSettings.isAllLogged()){ // is All logged
                    CORE_LogManager.createLog(log);
                }else if(logSettings.isErrorLogged() && isErrorLog){ // is Error
                    CORE_LogManager.createLog(log);
                }
            }
        }

        return result;
    }

    private static CORE_SIS_Wrapper.ResultWrapperProduct checkMandatoryParameters(String oppProdId){
        List<String> missingParameters = new List<String>();
        Boolean isMissingOppProdId = CORE_SIS_GlobalWorker.isParameterMissing(oppProdId);

        if(isMissingOppProdId){
            missingParameters.add(CORE_SIS_Constants.PARAM_OPPPROD_ID);
        }
        
        return getMissingParameters(missingParameters);
    }

    public static CORE_SIS_Wrapper.ResultWrapperProduct getMissingParameters(List<String> missingParameters){
        String errorMsg = CORE_SIS_Constants.MSG_MISSING_PARAMETERS;

        if(missingParameters.size() > 0){
            errorMsg += missingParameters;

            return new CORE_SIS_Wrapper.ResultWrapperProduct(CORE_SIS_Constants.ERROR_STATUS, errorMsg);
        }
        
        return null;
    }
}