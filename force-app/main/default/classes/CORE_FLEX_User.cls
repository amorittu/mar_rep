/**
 * @author Valentin Pitel
 * @create date 2022-04-13 17:22:13
 * @modify date 2022-04-13 17:22:13
 * @desc CORE_FLEX_User => call Twilio API on user creation and modification
 */
public with sharing class CORE_FLEX_User implements Queueable, Database.AllowsCallouts {
    @TestVisible
    private static final String CREATE_WORKER_PATH = '/sfdc/worker/create';
    @TestVisible
    private static final String UPDATE_WORKER_PATH = '/sfdc/worker/update';
    @TestVisible
    private static final String DELETE_WORKER_PATH = '/sfdc/worker/delete';
    @TestVisible
    private List<UserApiInfo> userApiInfos;
    private static final Integer MAX_USERS_BY_QUEUE = 50;
    private OmniChannelConfig__mdt omniConfig;
    @TestVisible
    private CORE_LogManager logSettings;

    
    public CORE_FLEX_User(List<UserApiInfo> userApiInfos, OmniChannelConfig__mdt omniConfig){
        this.userApiInfos = userApiInfos;
        this.omniConfig = omniConfig;
        if(test.isRunningTest()){
            this.logSettings = new CORE_LogManager();
            this.logSettings.customMetadataType = new CORE_Log_Settings__mdt(
                CORE_Log_Type__c = 'NONE'
            );
        } else {
            this.logSettings = new CORE_LogManager('Twilio_Users');
        }
    }

    @TestVisible
    class UserApiInfo{
        public String apiPath {get;set;}
        public String apiBody {get;set;}
        public User oldUser {get;set;}
        public User newUser {get;set;}
        public HttpResponse httpResponse {get;set;}
        public Boolean isSuccess {get;set;}
        public String apexException {get;set;}

        public UserApiInfo(){}

        public UserApiInfo(User user, User oldUser){
            this.oldUser = oldUser;
            this.newUser = user;
        }
        public UserApiInfo(UserApiInfo instanceToCopy){
            this.apiPath = instanceToCopy.apiPath;
            this.apiBody = instanceToCopy.apiBody;
            this.oldUser = instanceToCopy.oldUser;
            this.newUser = instanceToCopy.newUser;
            this.httpResponse = instanceToCopy.httpResponse;
            this.isSuccess = instanceToCopy.isSuccess;
            this.apexException = instanceToCopy.apexException;
        }
    }

    @TestVisible
    class LogDatas {
        public Long processDuration {get;set;}
        public userApiInfo userApiInfo {get;set;}

        public LogDatas(Long processDuration, userApiInfo userApiInfo){
            this.processDuration = processDuration;
            this.userApiInfo = userApiInfo;
        }
    }

    public void execute(QueueableContext context) {
        System.debug('@CORE_FLEX_User > execute START');
        List<User> usersToUpdate = new List<User>();

        if(this.userApiInfos.size() > MAX_USERS_BY_QUEUE){
            List<UserApiInfo> userApiInfosTmp = new List<UserApiInfo>();
            for(Integer i = 0 ; i < this.userApiInfos.size() ; i++){
                userApiInfosTmp.add( this.userApiInfos.get(i));
                if(math.mod(i,MAX_USERS_BY_QUEUE) == 0){
                    Id jobID = System.enqueueJob(new CORE_FLEX_User(userApiInfosTmp, this.omniConfig));
                    System.debug('@CORE_FLEX_User > execute : Create subQueue ' + jobID);
                    userApiInfosTmp = new List<UserApiInfo>();
                }
            }
            if(!userApiInfosTmp.isEmpty()){
                Id jobID = System.enqueueJob(new CORE_FLEX_User(userApiInfosTmp, this.omniConfig));
                System.debug('@CORE_FLEX_User > execute : Create subQueue ' + jobID);
            }
        } else {
            List<LogDatas> logDatas = new List<LogDatas>();

            for(Integer i = 0 ; i < this.userApiInfos.size() ; i++){
                Long dt1 = DateTime.now().getTime();
                Long dt2;
                System.debug('@CORE_FLEX_User > this.userApiInfos.get(i) = ' + this.userApiInfos.get(i));

                userApiInfo result = sendHttpRequest(this.userApiInfos.get(i));
                if(result.isSuccess){
                    usersToUpdate.add(result.newUser);
                    System.debug('result.newUser.Id = ' + result.newUser.Id);
                    System.debug('result.newUser.CORE_FLEX_Twilio_SID__c = ' + result.newUser.CORE_FLEX_Twilio_SID__c);
                }
                //System.debug('final result = ' + result.httpResponse.getBody());
                dt2 = DateTime.now().getTime();
                Long processDuration = dt2-dt1;

                LogDatas logData = new LogDatas(processDuration, result);
                logDatas.add(logData);
            }

            if(!usersToUpdate.isEmpty()){
                CORE_TriggerHandler.bypass('CORE_UserTriggerHandler');
                Update usersToUpdate;
                CORE_TriggerHandler.clearBypass('CORE_UserTriggerHandler');
            }

            logProcess(logDatas, this.logSettings);
        }
        System.debug('@CORE_FLEX_User > execute END');
    }

    public static void logProcess(List<LogDatas> logDatas, CORE_LogManager logSettings){        
        if(!logSettings.isNone()){
            List<CORE_LOG__c> logs = new List<CORE_LOG__c>();
            for(LogDatas logData : logDatas){
                CORE_LOG__c log = new CORE_LOG__c(CORE_API_Name__c = 'Twilio_Users');
                log.CORE_Processing_Duration__c = logData.processDuration;
                log.CORE_HTTP_Status_Code__c = logData.userApiInfo?.httpResponse?.getStatusCode();
                log.CORE_Sent_Body__c = logData.userApiInfo.apiBody;
                log.CORE_Received_Body__c = logData.userApiInfo?.httpResponse?.getBody();
                log.CORE_ApexExceptionError__c = logData.userApiInfo.apexException;
                log.CORE_Outgoing_API__c = true;
                log.CORE_Endpoint__c = logData.userApiInfo.apiPath;
                if(logSettings.isAllLogged() || (logSettings.isErrorLogged() && !logData.userApiInfo.isSuccess)){ // is All logged
                    logs.add(log);
                }
            }
            if(!logs.isEmpty()){
                insert logs;
            }
        }
    }
    
    @TestVisible
    private static UserApiInfo getUserApiInfo(User user,User oldUser, OmniChannelConfig__mdt omniConfig){
        UserApiInfo userApiInfo;
        Boolean isCreation = false;
        if(oldUser == null){
            isCreation = true;
        }
        if(omniConfig.CORE_Active_User_Provisioning__c && (user.IsActive && user.CallCenterId != null)){
            userApiInfo = new userApiInfo(user,oldUser);
            System.debug('sid = '+ user.CORE_FLEX_Twilio_SID__c);
            System.debug('friendlyName = '+ getFriendlyName(user));
            System.debug('email = '+ user.Email);
            JSONGenerator gen = JSON.createGenerator(true);    
            gen.writeStartObject();
            if(!isCreation && !String.isEmpty(user.CORE_FLEX_Twilio_SID__c)){
                gen.writeStringField('sid', user.CORE_FLEX_Twilio_SID__c);
            }
            gen.writeStringField('friendlyName', getFriendlyName(user));
            gen.writeFieldName('attributes');
            gen.writeStartObject();
            gen.writeStringField('email', user.Email);
            gen.writeEndObject();          

            gen.writeEndObject();    
            String jsonS = gen.getAsString();
            if(!isCreation && !String.isEmpty(user.CORE_FLEX_Twilio_SID__c)){
                userApiInfo.apiPath = UPDATE_WORKER_PATH;
            } else {
                userApiInfo.apiPath = CREATE_WORKER_PATH;
            }
            userApiInfo.apiBody = jsonS;
            System.debug('userApiInfo.apiBody = '+ userApiInfo.apiBody);
        } else if(omniConfig.CORE_Active_User_Deprovisioning__c && 
            (oldUser != null && 
                (!String.isEmpty(oldUser.CORE_FLEX_Twilio_SID__c) 
                    && (user.CallCenterId == null || !user.IsActive || String.isEmpty(user.CORE_FLEX_Twilio_SID__c))
                )
            )
        ){
            System.debug('delete case');
            JSONGenerator gen = JSON.createGenerator(true);    
            gen.writeStartObject();
            gen.writeObjectField('sid', new List<String>{oldUser.CORE_FLEX_Twilio_SID__c});
            gen.writeEndObject();    
            String jsonS = gen.getAsString();

            userApiInfo = new userApiInfo(user,oldUser);
            userApiInfo.apiPath = DELETE_WORKER_PATH;
            userApiInfo.apiBody = jsonS;
        }

        return userApiInfo;
    }

    private static string getFriendlyName(User user){
        String friendlyName = '';
        if(!String.isEmpty(user.Name)){
            friendlyName = user.Name;
        } else {
            if(!String.isEmpty(user.FirstName)){
                friendlyName = user.FirstName + ' ';
            }
            friendlyName += user.LastName;
        }

        return friendlyName;
    }
    public static void executeWorkerProcess(List<User> users,Map<Id, User> oldUsers, OmniChannelConfig__mdt omniConfig){
        System.debug('@CORE_FLEX_User > updateWorker START');
        System.debug('@CORE_FLEX_User > users.size() = ' + users.size());

        List<UserApiInfo> userApiInfos = new List<UserApiInfo>();
        for(User user : users){
            UserApiInfo userApiInfo = getUserApiInfo(user,oldUsers == null ? null : oldUsers.get(user.Id),omniConfig);
            if(userApiInfo != null){
                userApiInfos.add(userApiInfo);
            }
        }
        System.debug('@CORE_FLEX_User > userApiInfos.size() = ' + userApiInfos.size());

        Id jobID = System.enqueueJob(new CORE_FLEX_User(userApiInfos, omniConfig));
    }

    @TestVisible
    private static UserApiInfo sendHttpRequest(userApiInfo userInfoApi){
        UserApiInfo result = new UserApiInfo(userInfoApi);
        System.debug('result.apiBody = '+ result.apiBody);
        System.debug('req.apiBody = '+ userInfoApi.apiBody);

        result.isSuccess = true;
        
        String additionalErrors = '';
        try {
            HttpRequest req = CORE_FLEX_Service.createRequestTwilio(userInfoApi.apiPath);
            result.apiPath = req.getEndpoint();
            req.setBody(userInfoApi.apiBody);
            System.debug('req.getBody = '+ req.getBody());

            Http http = new Http();
            HTTPResponse res = http.send(req);
            result.httpResponse = res;
            String resultBody = res.getBody();
            system.debug('Result : '+resultBody+', status:'+res.getStatusCode());
            if(res.getStatusCode() == 200) {
                if(userInfoApi.apiPath == CREATE_WORKER_PATH){
                    Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(resultBody);
                    result.newUser = new user(
                        Id = userInfoApi.newUser.Id,
                        CORE_FLEX_Twilio_SID__c = (String) results.get('sid')
                    );
                } else if(userInfoApi.apiPath == DELETE_WORKER_PATH){
                    Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(resultBody);
                    if((String) results.get('status') == 'deleted'){
                        result.newUser = new user(
                            Id = userInfoApi.newUser.Id,
                            CORE_FLEX_Twilio_SID__c = null,
                            CallCenterId = null
                        );
                    }
                }
            } else {
                result.isSuccess = false;
            }
            return result;
        } catch(Exception e) {
            result.isSuccess = false;
            String errroMsg = 'An unexpected error occured while calling twilio api. exception:'+e.getStackTraceString();
            result.apexException = errroMsg;
            System.debug(errroMsg);
            return result;
        }
    }
}