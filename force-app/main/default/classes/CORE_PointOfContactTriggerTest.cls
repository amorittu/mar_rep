@IsTest
public class CORE_PointOfContactTriggerTest {
    
    @TestSetup
    public static void makeData(){
        CORE_DataFaker_CatalogHierarchy catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test1');
        Account account = CORE_DataFaker_Account.getStudentAccount('test1');
        Opportunity opportunity = CORE_DataFaker_Opportunity.getOpportunity(catalogHierarchy.division.Id, 
            catalogHierarchy.businessUnit.Id, 
            catalogHierarchy.studyArea.Id,
            catalogHierarchy.curriculum.Id,
            catalogHierarchy.level.Id,
            catalogHierarchy.intake.Id, 
            account.Id, 
            'Lead', 
            'Lead - New'
        );
    }
    
    
    @IsTest
    public static void insert_POC_update_first_poc_Opportunity_once() {
        Account account = [Select Id FROM Account LIMIT 1];
        Opportunity opportunity = [Select Id, CORE_Insertion_Date__c FROM Opportunity LIMIT 1];
        
        //Opp Insertion Date doit être null initialement
        System.assertEquals(null, opportunity.CORE_Insertion_Date__c);

        Test.startTest();
        //Creer et insert un nouveau POC
        CORE_DataFaker_PointOfContact.getPointOfContact(opportunity.id, account.id, 'POCExternalIDTest');
        Test.stopTest();
        
        //On récupère l'opp mise à jour
        Opportunity updatedOpportunity = [Select Id, CORE_Insertion_Date__c FROM Opportunity WHERE Id = :opportunity.Id];
        
        System.assertNotEquals(null, UpdatedOpportunity.CORE_Insertion_Date__c);
    }

}