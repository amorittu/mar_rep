public without sharing class CORE_EmailMessageTriggerHandler {
    private static final String SENT_STATUS = '3';
    @TestVisible
    private static final String TASK_STATUS_CLOSED = 'CORE_PKL_Completed';
    @TestVisible
    private static final String TASK_RESULT_ANSWERED = 'CORE_PKL_Answered';
    @TestVisible
    private static final String TASK_TYPE = 'CORE_PKL_ContactEmail';
    private static final String DELETE_PERMISSION_API_NAME = 'CORE_Delete_task_and_email';

    public static void handleTrigger(List<EmailMessage> oldRecords, List<EmailMessage> newRecords, System.TriggerOperation triggerEvent) {
        switch on triggerEvent {
            when AFTER_INSERT {
                System.debug('CORE_EmailMessageTriggerHandler.afterInsert : START');
                afterInsert(newRecords);
                System.debug('CORE_EmailMessageTriggerHandler.afterInsert : END');
            }
            when BEFORE_DELETE {
                System.debug('CORE_EmailMessageTriggerHandler.beforeDelete : START');
                beforeDelete(oldRecords);
                System.debug('CORE_EmailMessageTriggerHandler.beforeDelete : END');
            }
        }
    }

    public static void beforeDelete(List<EmailMessage> oldRecords){
        boolean userHaveDeletePermission = FeatureManagement.checkPermission(DELETE_PERMISSION_API_NAME);

        if(!userHaveDeletePermission){
            for(EmailMessage emailMessage : oldRecords){
                emailMessage.addError(Label.CORE_EmailMessage_DeleteError);
            }
        }
    }

    private static void afterInsert(List<EmailMessage> newRecords) {
        //process only for unique outbound email (manual replies)
        if(newRecords == null || newRecords.isEmpty() || newRecords.size() > 1){
            return;
        }

        EmailMessage emailMessage = newRecords.get(0);
        boolean closeRelativeTask = false;
        
        if(emailMessage.Status == SENT_STATUS && emailMessage.Incoming == false){
            closeRelativeTask = true;
        }
        

        if(closeRelativeTask){
            closeRelatedTask(emailMessage);
        }
        
    }

    private static void closeRelatedTask(EmailMessage emailMessage){
        String threadIdentifier = emailMessage.ThreadIdentifier;
        System.debug('threadIdentifier = ' + threadIdentifier);
        
        List<EmailMessage> relatedEmailMessages = [SELECT Id, ActivityId, CORE_TECH_TaskId__c 
            FROM EmailMessage 
            WHERE ThreadIdentifier = :threadIdentifier AND Incoming = true AND Id != :emailMessage.Id];

        System.debug('CORE_EmailMessageTriggerHandler.afterInsert relatedEmailMessagesTmp = ' + relatedEmailMessages);

        // no related email messages => the new email message is not a reply to another email, we can stop here
        if(relatedEmailMessages.isEmpty()){
            return;
        }

        Set<Id> taskIds = new Set<Id>();

        //get all tasks related to the email flow 
        for(EmailMessage relatedEmailMessage : relatedEmailMessages){
            Id taskId = relatedEmailMessage.ActivityId == null ? relatedEmailMessage.CORE_TECH_TaskId__c : relatedEmailMessage.ActivityId;

            taskIds.add(taskId);
        }
        
        System.debug('CORE_EmailMessageTriggerHandler.afterInsert taskIds = ' + taskIds);

        //keep only open tasks
        List<Task> tasks = [SELECT Id, LastModifiedDate 
            FROM Task where Id IN :taskIds AND IsClosed = false 
            ORDER BY LastModifiedDate DESC];
            System.debug('CORE_EmailMessageTriggerHandler.afterInsert tasks = ' + tasks);

        Task taskToClose;
        //close only the last open task
        if(!tasks.isEmpty()){
            taskToClose = tasks.get(0);
            taskToClose.Status = TASK_STATUS_CLOSED;
            taskToClose.Core_Task_Result__c = TASK_RESULT_ANSWERED;
            taskToClose.CORE_TECH_CallTaskType__c = TASK_TYPE;
        }

        
        System.debug('CORE_EmailMessageTriggerHandler.afterInsert tasksToClose = ' + taskToClose);

        if(taskToClose != null){
            Update taskToClose;
        }
    }
}