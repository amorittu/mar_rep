/**
 * Created by Gabriele Vitanza on 11/01/2022.
 */

@IsTest
private class IT_QueueAssignOwnerStartFlowTest {
    @TestSetup
    static void testSetup() {
        CORE_DataFaker_CatalogHierarchy catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test1');

        Account account = CORE_DataFaker_Account.getStudentAccount('test1');
        Opportunity opportunity = CORE_DataFaker_Opportunity.getOpportunity(
                catalogHierarchy.division.Id,
                catalogHierarchy.businessUnit.Id,
                account.Id,
                'Applicant',
                'Applicant - Application Submitted / New'
        );

        opportunity.Pricebook2Id = Test.getStandardPricebookId();
        opportunity.CORE_OPP_Intake__c = catalogHierarchy.intake.Id;
        opportunity.CORE_OPP_Level__c = catalogHierarchy.level.Id;
        opportunity.CORE_OPP_Curriculum__c = catalogHierarchy.curriculum.Id;
        opportunity.CORE_OPP_Study_Area__c = catalogHierarchy.studyArea.Id;
        opportunity.CORE_Capture_Channel__c = 'CORE_PKL_Web_form';

        update opportunity;

        Opportunity opportunity2 = CORE_DataFaker_Opportunity.getOpportunity(
                catalogHierarchy.division.Id,
                catalogHierarchy.businessUnit.Id,
                account.Id,
                'Applicant',
                'Applicant - Application Submitted / New'
        );

        IT_ExtendedInfo__c extendedInfo = IT_DataFaker_Objects.createExtendedInfo(account.id, opportunity.IT_Brand__c, false);
        extendedInfo.IT_LastContactInformationDate__c = Datetime.newInstance(2022, 1, 2, 0, 0 , 0);
        extendedInfo.IT_LastContactAdmissionDate__c = Datetime.newInstance(2022, 1, 1, 0, 0 , 0);
        System.debug('extendedInfo.IT_LastContactInformationDate__c:: ' + extendedInfo.IT_LastContactInformationDate__c);
        System.debug('extendedInfo.IT_LastContactAdmissionDate__c:: ' + extendedInfo.IT_LastContactAdmissionDate__c);
        insert extendedInfo;

        System.debug('extendedInfo inserted:: ' + [SELECT Id, IT_LastContactInformationDate__c, IT_LastContactAdmissionDate__c FROM IT_ExtendedInfo__c]);

        IT_OpportunityAssignmentRule__c oar = new IT_OpportunityAssignmentRule__c();
        oar.IT_BusinessUnit__c = opportunity.CORE_OPP_Business_Unit__c;
        oar.IT_MainCountry__c = opportunity.Account.PersonMailingCountry;
        oar.IT_CaptureChannel__c = opportunity.CORE_Capture_Channel__c;
        insert oar;

        IT_CountryRelevance__c cr = new IT_CountryRelevance__c();
        cr.IT_BusinessUnit__c = opportunity.CORE_OPP_Business_Unit__c;
        cr.IT_CountryISO__c = opportunity.Account.PersonMailingCountry;
        insert cr;

        List<Task> task2Insert = new List<Task>();
        Task t = new Task();
        t.WhatId = opportunity.Id;
        task2Insert.add(t);

        Task t2 = new Task();
        t2.WhatId = opportunity2.Id;
        task2Insert.add(t2);

        insert task2Insert;

    }

    @IsTest
    static void testBehavior(){
        Opportunity opty = [SELECT Id FROM Opportunity LIMIT 1];
        Set<Id> optyIds = new Set<Id>{opty.Id};

        Test.startTest();
        System.enqueueJob(new IT_QueueAssignOwnerStartFlow(optyIds));
        Test.stopTest();
    }

    /*@IsTest
    static void testBehavior2(){
        // PE - START (To cover Flow IT_TriggeredThankYouEmail)
        Test.setMock(HttpCalloutMock.class, new IT_MockMarketingCloudIntegration('token'));
        IT_EmailPlatformEvent__e PE = new IT_EmailPlatformEvent__e();

        PE.IT_BusinessUnitExternalId__c             = 'NB.MI0';
        PE.IT_AccountLanguagePC__c                  = 'English';
        PE.IT_AccountLanguageWebsite__c             = 'English';
        PE.IT_AccountLanguageOfRelationship__c      = 'English';
        PE.IT_CountryRelevance__c                   = 'A';
        PE.IT_StudentFirstName__c                   = 'TestN1';
        PE.IT_StudentLastName__c                    = 'TestS1';
        PE.IT_StudentEmail__c                       = 'TestN1TestS1@null.aa';
        PE.IT_TaskId__c                             = '123456789012345678';
        PE.IT_SubscriberKey__c                      = '923456789012345678';
        PE.IT_EventDefinitionKey__c                 = 'APIEvent-f82bb383-b351-d6c3-b969-a3dda66cbcd2';
        PE.IT_COAProcess__c                         = 'IT_PKL_LeadNew';
        PE.IT_DivisionExternalId__c                 = 'NB';
        PE.IT_EmailTypology__c                      = 'TY';

        List<IT_EmailPlatformEvent__e> PEList = new List<IT_EmailPlatformEvent__e>();
        PEList.add(PE);
        Database.SaveResult[] sr = EventBus.publish(PEList);
        // PE - END
        CORE_DataFaker_CatalogHierarchy catalogHierarchy2 = new CORE_DataFaker_CatalogHierarchy('test2');
        Opportunity opportunity3 = CORE_DataFaker_Opportunity.getOpportunity(
                catalogHierarchy2.division.Id,
                catalogHierarchy2.businessUnit.Id,
                [SELECT Id FROM Account LIMIT 1].Id,
                'Applicant',
                'Applicant - Application Submitted / New'
        );

        opportunity3.CORE_OPP_Intake__c = catalogHierarchy2.intake.Id;
        opportunity3.CORE_OPP_Level__c = catalogHierarchy2.level.Id;
        opportunity3.CORE_OPP_Curriculum__c = catalogHierarchy2.curriculum.Id;
        opportunity3.CORE_OPP_Study_Area__c = catalogHierarchy2.studyArea.Id;
        opportunity3.CORE_Capture_Channel__c = 'CORE_PKL_Web_form';
        update opportunity3;

        CORE_Business_Unit__c bu = new CORE_Business_Unit__c(Id = catalogHierarchy2.businessUnit.Id);
        bu.CORE_Brand__c = 'CORE_PKL_Istituto_Marangoni';
        update bu;

        IT_OpportunityAssignmentRule__c oar = new IT_OpportunityAssignmentRule__c();
        oar.IT_BusinessUnit__c = opportunity3.CORE_OPP_Business_Unit__c;
        oar.IT_MainCountry__c = opportunity3.Account.PersonMailingCountry;
        oar.IT_CaptureChannel__c = opportunity3.CORE_Capture_Channel__c;
        oar.IT_InformationOfficer__c = UserInfo.getUserId();
        oar.IT_COANotApplied__c = true;
        insert oar;

        Task t = new Task();
        t.WhatId = opportunity3.Id;
        insert t;

        Map<Opportunity, IT_OpportunityAssignmentRule__c> optyOARMap = new Map<Opportunity, IT_OpportunityAssignmentRule__c>{opportunity3 => oar};
        List<Opportunity> optyList = new List<Opportunity>{opportunity3};
        //IT_QueueAssignOwnerStartFlow.checkCOAnotApplied(optyList, optyOARMap);

        Set<Id> optyIds = new Set<Id>{opportunity3.Id};

        Test.startTest();
        System.enqueueJob(new IT_QueueAssignOwnerStartFlow(optyIds));
        Test.stopTest();
    }*/
}