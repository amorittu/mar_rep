@IsTest
public class CORE_LeadFormAcquisitionUtilsTest {
    @IsTest
    public static void getAccountFromId_Should_return_account_from_id() {
        Account account = CORE_DataFaker_Account.getStudentAccount('test');

        Test.startTest();
        Account result = CORE_LeadFormAcquisitionUtils.getAccountFromId(account.Id);
        Test.stopTest();

        System.assertEquals(account.Id, result.Id);
        System.assertEquals(account.FirstName, result.FirstName);
        System.assertEquals(account.LastName, result.LastName);
        System.assertEquals(account.PersonEmail, result.PersonEmail);
    }

    @IsTest
    public static void getExestingAccounts_Should_return_matching_account_based_on_email() {
        Account account = CORE_DataFaker_Account.getStudentAccount('test');
        account.CORE_Email_2__pc = 'email2@test.com';
        account.CORE_Email_3__pc = 'email3@test.com';
        account.CORE_Student_Email__pc = 'studentEmail@test.com';
        update account;

        
        Account account2 = CORE_DataFaker_Account.getStudentAccount('test2');

        CORE_LeadFormAcquisitionWrapper.PersonAccountWrapper personAccountWrapper = CORE_DataFaker_LeadFormAcquWrapper.getPersonAccountWrapper();
        personAccountWrapper.emailAddress = null;
        personAccountWrapper.phone = null;
		personAccountWrapper.mobilePhone = null;

        Test.startTest();

        List<Account> results = CORE_LeadFormAcquisitionUtils.getExestingAccounts(personAccountWrapper);
        System.assertEquals(0, results.size(), '0 account must be found when no emails and phone are provided');

        personAccountWrapper.emailAddress = 'test@gmail.com';
        results = CORE_LeadFormAcquisitionUtils.getExestingAccounts(personAccountWrapper);
        System.assertEquals(0, results.size(), '0 account must be found when no phone are provided and email do not exist');

        personAccountWrapper.emailAddress = account.PersonEmail;
        results = CORE_LeadFormAcquisitionUtils.getExestingAccounts(personAccountWrapper);
        System.assertEquals(1, results.size(), '1 account must be found when no phone are provided and email exist in one account');

        personAccountWrapper.emailAddress = account.CORE_Email_2__pc;
        results = CORE_LeadFormAcquisitionUtils.getExestingAccounts(personAccountWrapper);
        System.assertEquals(1, results.size(), '1 account must be found when no phone are provided and email exist in one account');

        personAccountWrapper.emailAddress = account.CORE_Email_3__pc;
        results = CORE_LeadFormAcquisitionUtils.getExestingAccounts(personAccountWrapper);
        System.assertEquals(1, results.size(), '1 account must be found when no phone are provided and email exist in one account');
        
        personAccountWrapper.emailAddress = account.CORE_Student_Email__pc;
        results = CORE_LeadFormAcquisitionUtils.getExestingAccounts(personAccountWrapper);
        System.assertEquals(1, results.size(), '1 account must be found when no phone are provided and email exist in one account');

        personAccountWrapper.emailAddress = account.PersonEmail;
        account2.PersonEmail = account.PersonEmail;
        Database.DMLOptions dml = new Database.DMLOptions();
        dml.DuplicateRuleHeader.AllowSave = true; 
        Database.SaveResult sr = Database.update(account2, dml); 
        
        results = CORE_LeadFormAcquisitionUtils.getExestingAccounts(personAccountWrapper);
        System.debug('account2 = '+ account2);
        System.debug('results = '+ results);
        List<Account> accounts = [SELECT Id, PersonContactId, CreatedDate, Salutation, LastName, FirstName, Phone, CORE_Phone2__pc,
                PersonMobilePhone, CORE_Mobile_2__pc, CORE_Mobile_3__pc, PersonEmail,CORE_Email_2__pc,CORE_Email_3__pc, CORE_Student_Email__pc,
                PersonMailingStreet, PersonMailingPostalCode, PersonMailingCity, CORE_Language_website__pc, PersonMailingCountry, PersonMailingState, OwnerId,CORE_GA_User_ID__c
                FROM Account
                WHERE PersonEmail = :account.PersonEmail OR CORE_Email_2__pc = :account.PersonEmail OR CORE_Email_3__pc = :account.PersonEmail OR CORE_Student_Email__pc = :account.PersonEmail];
        
        System.debug('accounts = '+ accounts);

        System.assertEquals(2, results.size(), '2 account must be found when no phone are provided and email exist in 2 account');
        
        Test.stopTest();
    }

    @IsTest
    public static void getExestingAccounts_Should_return_matching_account_based_on_phone() {
        Account account = CORE_DataFaker_Account.getStudentAccount('test');
        account.Phone = '+33200000000';
        account.CORE_Mobile_2__pc = '33600000001';
        account.CORE_Mobile_3__pc = '33600000002';
        account.PersonMobilePhone = '33600000002';
        update account;

        
        Account account2 = CORE_DataFaker_Account.getStudentAccount('test2');

        CORE_LeadFormAcquisitionWrapper.PersonAccountWrapper personAccountWrapper = CORE_DataFaker_LeadFormAcquWrapper.getPersonAccountWrapper();
        personAccountWrapper.emailAddress = null;
        personAccountWrapper.phone = null;
		personAccountWrapper.mobilePhone = null;

        Test.startTest();

        List<Account> results = CORE_LeadFormAcquisitionUtils.getExestingAccounts(personAccountWrapper);
        System.assertEquals(0, results.size(), '0 account must be found when no emails and phone are provided');

        personAccountWrapper.phone = '+33200000001';
        results = CORE_LeadFormAcquisitionUtils.getExestingAccounts(personAccountWrapper);
        System.assertEquals(0, results.size(), '0 account must be found when no phone are provided and email do not exist');

        Id [] fixedSearchResults= new List<Id>{account.Id};
        Test.setFixedSearchResults(fixedSearchResults);
        personAccountWrapper.phone = account.Phone;
        results = CORE_LeadFormAcquisitionUtils.getExestingAccounts(personAccountWrapper);
        System.assertEquals(1, results.size(), '1 account must be found when no phone are provided and email exist in one account');

        personAccountWrapper.mobilePhone = account.CORE_Mobile_2__pc;
        results = CORE_LeadFormAcquisitionUtils.getExestingAccounts(personAccountWrapper);
        System.assertEquals(1, results.size(), '1 account must be found when no phone are provided and email exist in one account');

        personAccountWrapper.mobilePhone = account.CORE_Mobile_3__pc;
        results = CORE_LeadFormAcquisitionUtils.getExestingAccounts(personAccountWrapper);
        System.assertEquals(1, results.size(), '1 account must be found when no phone are provided and email exist in one account');
        
        personAccountWrapper.mobilePhone = account.PersonMobilePhone;
        results = CORE_LeadFormAcquisitionUtils.getExestingAccounts(personAccountWrapper);
        System.assertEquals(1, results.size(), '1 account must be found when no phone are provided and email exist in one account');

        account2.Phone = account.Phone;
        Database.DMLOptions dml = new Database.DMLOptions();
        dml.DuplicateRuleHeader.AllowSave = true; 
        Database.SaveResult sr = Database.update(account2, dml); 

        fixedSearchResults.add(account2.Id);
        Test.setFixedSearchResults(fixedSearchResults);
        results = CORE_LeadFormAcquisitionUtils.getExestingAccounts(personAccountWrapper);
        System.assertEquals(2, results.size(), '2 account must be found when no phone are provided and email exist in 2 account');
        
        Test.stopTest();
    }

    @IsTest
    public static void getDivisionsFromExternalIds_Should_return_division_list_based_on_external_ids() {
        CORE_DataFaker_CatalogHierarchy catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test');
        CORE_LeadFormAcquisitionWrapper.GlobalWrapper globalWrapper = CORE_DataFaker_LeadFormAcquWrapper.getGlobalWrapper();
        globalWrapper.mainInterest.division = catalogHierarchy.division.CORE_Division_ExternalId__c;
        globalWrapper.secondInterest.division = catalogHierarchy.division.CORE_Division_ExternalId__c;
        globalWrapper.thirdInterest.division = catalogHierarchy.division.CORE_Division_ExternalId__c;

        Test.startTest();
        List<CORE_Division__c> results = CORE_LeadFormAcquisitionUtils.getDivisionsFromExternalIds(globalWrapper);

        System.assertNotEquals(null,results);
        System.assertEquals(1,results.size());
        System.assertEquals(globalWrapper.mainInterest.division, results.get(0).CORE_Division_ExternalId__c);
        System.assertEquals(catalogHierarchy.division.Id, results.get(0).Id);

        globalWrapper.mainInterest.division = 'testNotExist';
        globalWrapper.secondInterest.division = null;
        globalWrapper.thirdInterest.division = '';

        results = CORE_LeadFormAcquisitionUtils.getDivisionsFromExternalIds(globalWrapper);
        System.assertNotEquals(null,results);
        System.assertEquals(0,results.size());

        Test.stopTest();
    }

    @IsTest
    public static void getBusinessUnitsFromExternalIds_Should_return_business_unit_list_based_on_external_ids() {
        CORE_DataFaker_CatalogHierarchy catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test');
        CORE_LeadFormAcquisitionWrapper.GlobalWrapper globalWrapper = CORE_DataFaker_LeadFormAcquWrapper.getGlobalWrapper();
        globalWrapper.mainInterest.businessUnit = catalogHierarchy.businessUnit.CORE_Business_Unit_ExternalId__c;
        globalWrapper.secondInterest.businessUnit = catalogHierarchy.businessUnit.CORE_Business_Unit_ExternalId__c;
        globalWrapper.thirdInterest.businessUnit = catalogHierarchy.businessUnit.CORE_Business_Unit_ExternalId__c;

        Test.startTest();
        List<CORE_Business_Unit__c> results = CORE_LeadFormAcquisitionUtils.getBusinessUnitsFromExternalIds(globalWrapper);

        System.assertNotEquals(null,results);
        System.assertEquals(1,results.size());
        System.assertEquals(globalWrapper.mainInterest.businessUnit, results.get(0).CORE_Business_Unit_ExternalId__c);
        System.assertEquals(catalogHierarchy.businessUnit.Id, results.get(0).Id);

        globalWrapper.mainInterest.businessUnit = 'testNotExist';
        globalWrapper.secondInterest.businessUnit = null;
        globalWrapper.thirdInterest.businessUnit = '';

        results = CORE_LeadFormAcquisitionUtils.getBusinessUnitsFromExternalIds(globalWrapper);
        System.assertNotEquals(null,results);
        System.assertEquals(0,results.size());

        Test.stopTest();
    }

    @IsTest
    public static void getStudyAreasFromExternalIds_Should_return_business_unit_list_based_on_external_ids() {
        CORE_DataFaker_CatalogHierarchy catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test');
        CORE_LeadFormAcquisitionWrapper.GlobalWrapper globalWrapper = CORE_DataFaker_LeadFormAcquWrapper.getGlobalWrapper();
        globalWrapper.mainInterest.studyArea = catalogHierarchy.studyArea.CORE_Study_Area_ExternalId__c;
        globalWrapper.secondInterest.studyArea = catalogHierarchy.studyArea.CORE_Study_Area_ExternalId__c;
        globalWrapper.thirdInterest.studyArea = catalogHierarchy.studyArea.CORE_Study_Area_ExternalId__c;

        Test.startTest();
        List<CORE_Study_Area__c> results = CORE_LeadFormAcquisitionUtils.getStudyAreasFromExternalIds(globalWrapper);

        System.assertNotEquals(null,results);
        System.assertEquals(1,results.size());
        System.assertEquals(globalWrapper.mainInterest.studyArea, results.get(0).CORE_Study_Area_ExternalId__c);
        System.assertEquals(catalogHierarchy.studyArea.Id, results.get(0).Id);

        globalWrapper.mainInterest.studyArea = 'testNotExist';
        globalWrapper.secondInterest.studyArea = null;
        globalWrapper.thirdInterest.studyArea = '';

        results = CORE_LeadFormAcquisitionUtils.getStudyAreasFromExternalIds(globalWrapper);
        System.assertNotEquals(null,results);
        System.assertEquals(0,results.size());

        Test.stopTest();
    }

    @IsTest
    public static void getCurriculumsFromExternalIds_Should_return_business_unit_list_based_on_external_ids() {
        CORE_DataFaker_CatalogHierarchy catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test');
        CORE_LeadFormAcquisitionWrapper.GlobalWrapper globalWrapper = CORE_DataFaker_LeadFormAcquWrapper.getGlobalWrapper();
        globalWrapper.mainInterest.curriculum = catalogHierarchy.curriculum.CORE_Curriculum_ExternalId__c;
        globalWrapper.secondInterest.curriculum = catalogHierarchy.curriculum.CORE_Curriculum_ExternalId__c;
        globalWrapper.thirdInterest.curriculum = catalogHierarchy.curriculum.CORE_Curriculum_ExternalId__c;

        Test.startTest();
        List<CORE_Curriculum__c> results = CORE_LeadFormAcquisitionUtils.getCurriculumsFromExternalIds(globalWrapper);

        System.assertNotEquals(null,results);
        System.assertEquals(1,results.size());
        System.assertEquals(globalWrapper.mainInterest.curriculum, results.get(0).CORE_Curriculum_ExternalId__c);
        System.assertEquals(catalogHierarchy.curriculum.Id, results.get(0).Id);

        globalWrapper.mainInterest.curriculum = 'testNotExist';
        globalWrapper.secondInterest.curriculum = null;
        globalWrapper.thirdInterest.curriculum = '';

        results = CORE_LeadFormAcquisitionUtils.getCurriculumsFromExternalIds(globalWrapper);
        System.assertNotEquals(null,results);
        System.assertEquals(0,results.size());

        Test.stopTest();
    }

    @IsTest
    public static void getLevelsFromExternalIds_Should_return_business_unit_list_based_on_external_ids() {
        CORE_DataFaker_CatalogHierarchy catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test');
        CORE_LeadFormAcquisitionWrapper.GlobalWrapper globalWrapper = CORE_DataFaker_LeadFormAcquWrapper.getGlobalWrapper();
        globalWrapper.mainInterest.level = catalogHierarchy.level.CORE_Level_ExternalId__c;
        globalWrapper.secondInterest.level = catalogHierarchy.level.CORE_Level_ExternalId__c;
        globalWrapper.thirdInterest.level = catalogHierarchy.level.CORE_Level_ExternalId__c;

        Test.startTest();
        List<CORE_Level__c> results = CORE_LeadFormAcquisitionUtils.getLevelsFromExternalIds(globalWrapper);

        System.assertNotEquals(null,results);
        System.assertEquals(1,results.size());
        System.assertEquals(globalWrapper.mainInterest.level, results.get(0).CORE_Level_ExternalId__c);
        System.assertEquals(catalogHierarchy.level.Id, results.get(0).Id);

        globalWrapper.mainInterest.level = 'testNotExist';
        globalWrapper.secondInterest.level = null;
        globalWrapper.thirdInterest.level = '';

        results = CORE_LeadFormAcquisitionUtils.getLevelsFromExternalIds(globalWrapper);
        System.assertNotEquals(null,results);
        System.assertEquals(0,results.size());

        Test.stopTest();
    }

    @IsTest
    public static void getIntakesFromExternalIds_Should_return_business_unit_list_based_on_external_ids() {
        CORE_DataFaker_CatalogHierarchy catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test');
        CORE_LeadFormAcquisitionWrapper.GlobalWrapper globalWrapper = CORE_DataFaker_LeadFormAcquWrapper.getGlobalWrapper();
        globalWrapper.mainInterest.intake = catalogHierarchy.intake.CORE_Intake_ExternalId__c;
        globalWrapper.secondInterest.intake = catalogHierarchy.intake.CORE_Intake_ExternalId__c;
        globalWrapper.thirdInterest.intake = catalogHierarchy.intake.CORE_Intake_ExternalId__c;

        Test.startTest();
        List<CORE_Intake__c> results = CORE_LeadFormAcquisitionUtils.getIntakesFromExternalIds(globalWrapper);

        System.assertNotEquals(null,results);
        System.assertEquals(1,results.size());
        System.assertEquals(globalWrapper.mainInterest.intake, results.get(0).CORE_Intake_ExternalId__c);
        System.assertEquals(catalogHierarchy.intake.Id, results.get(0).Id);

        globalWrapper.mainInterest.intake = 'testNotExist';
        globalWrapper.secondInterest.intake = null;
        globalWrapper.thirdInterest.intake = '';

        results = CORE_LeadFormAcquisitionUtils.getIntakesFromExternalIds(globalWrapper);
        System.assertNotEquals(null,results);
        System.assertEquals(0,results.size());

        Test.stopTest();
    }
}