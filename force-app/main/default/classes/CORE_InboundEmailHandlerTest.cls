@IsTest
public class CORE_InboundEmailHandlerTest {
    @TestSetup
    static void makeData(){
        Group queue = new Group(
            Email = 'toAddress@test.com',
            Type = 'Queue',
            Name = 'TestQueue'
        );
        insert queue;
        QueuesObject q1 = new QueueSObject(QueueID = queue.id, SobjectType = 'Task');
        insert q1;
    }
    
    @IsTest
    public static void handleInboundEmail_Should_Create_EmailMessage_and_personaccount_when_email_not_found(){
        CORE_InboundEmailHandler inboundEmail = new CORE_InboundEmailHandler();
        List<Group> queues = [SELECT Id, Email FROM Group WHERE Name = 'TestQueue' AND Type = 'Queue'];
        System.assertEquals(1, queues.size());

        Messaging.InboundEmail email = new Messaging.InboundEmail();
        email.fromAddress ='test@test.fr';
        email.fromName = 'Testa Testb';
        email.plainTextBody = 'text body test';
        email.htmlBody = 'text body test html';
        email.messageId = '1235messageId';
        email.subject = 'subject';
        email.toAddresses = new List<String> {queues.get(0).Email};

        List<Account> accounts = [SELECT Id FROM Account WHERE name LIKE 'Test%'];
        System.assertEquals(0, accounts.size());

        Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();
        envelope.fromAddress = email.fromAddress;
        envelope.toAddress = email.toAddresses.get(0);
        Test.startTest();
        inboundEmail.handleInboundEmail(email, envelope);
        Test.stopTest();
        accounts = [SELECT Id FROM Account WHERE name LIKE 'Test%'];

        EmailMessage emailMessage = [SELECT Id,MessageIdentifier , Incoming, Status, HtmlBody, 
            TextBody, Subject, ToAddress, CcAddress, FromAddress, FromName, headers, RelatedToId,
            CORE_TECH_TaskOwnerId__c
            FROM EmailMessage WHERE MessageIdentifier = :email.messageId];

        System.assertEquals(1, accounts.size());
        System.assertEquals('0', emailMessage.Status);
        System.assertEquals(true, emailMessage.Incoming);
        System.assertEquals(email.htmlBody, emailMessage.HtmlBody);
        System.assertEquals(email.plainTextBody, emailMessage.TextBody);
        System.assertEquals(email.subject, emailMessage.Subject);
        System.assertEquals(envelope.toAddress , emailMessage.ToAddress);
        System.assertEquals(envelope.fromAddress, emailMessage.FromAddress);
        System.assertEquals(email.fromName , emailMessage.FromName);
        System.assertEquals(accounts.get(0).Id , emailMessage.RelatedToId);
        System.assertEquals(queues.get(0).Id , emailMessage.CORE_TECH_TaskOwnerId__c);
    }

    @IsTest
    public static void createTaskForInternalDomains_should_create_and_return_task(){
        EmailMessage emailMessage = new EmailMessage(Subject = 'test', TextBody = 'Description');
        CORE_InboundEmailHandler inboundEmail = new CORE_InboundEmailHandler();
        Task task = inboundEmail.createTaskForInternalDomains(emailMessage);

        System.assertNotEquals(null, task.Id);
        Task task2 = [SELECT id,Subject,Description FROM task WHERE Id = :task.Id];
        System.assertEquals(task2.Description, emailMessage.TextBody);
        System.assert(task2.Subject.contains(emailMessage.Subject));
    }

    @IsTest
    public static void selectOpportunityInManyOppCase_Should_Return_null_if_many_opp_meet_the_criteria (){
        CORE_DataFaker_CatalogHierarchy catalogHierarchy1 = new CORE_DataFaker_CatalogHierarchy('test1');
        Account account = CORE_DataFaker_Account.getStudentAccount('test1');
        Opportunity opportunity1 = CORE_DataFaker_Opportunity.getOpportunity(catalogHierarchy1.division.Id, 
            catalogHierarchy1.businessUnit.Id, 
            catalogHierarchy1.studyArea.Id,
            catalogHierarchy1.curriculum.Id,
            catalogHierarchy1.level.Id,
            catalogHierarchy1.intake.Id, 
            account.Id, 
            'Lead', 
            'Lead - New'
        ); 
        CORE_DataFaker_CatalogHierarchy catalogHierarchy2 = new CORE_DataFaker_CatalogHierarchy('test2');
        Opportunity opportunity2 = CORE_DataFaker_Opportunity.getOpportunity(catalogHierarchy2.division.Id, 
            catalogHierarchy2.businessUnit.Id, 
            catalogHierarchy2.studyArea.Id,
            catalogHierarchy2.curriculum.Id,
            catalogHierarchy2.level.Id,
            catalogHierarchy2.intake.Id, 
            account.Id, 
            'Lead', 
            'Lead - New'
        ); 

        String email1 = 'test1@test.com';
        CORE_Business_Unit__c bu1 = catalogHierarchy1.businessUnit;
        bu1.CORE_Generic_contact_email__c = email1;
        CORE_Business_Unit__c bu2 = catalogHierarchy2.businessUnit;
        bu2.CORE_Generic_contact_email__c = email1;
        
        update new List<CORE_Business_Unit__c>{bu1,bu2};
        
        List<Opportunity> oppsFound = new List<Opportunity>{opportunity1, opportunity2};
        
        List<String> recipientAdresses = new List<String> {email1};

        CORE_InboundEmailHandler emailHandler = new CORE_InboundEmailHandler();
        Opportunity result = emailHandler.selectOpportunityInManyOppCase(oppsFound,recipientAdresses);
        System.assertEquals(null, result);
    }

    @IsTest
    public static void selectOpportunityInManyOppCase_Should_Return_null_if_no_opp_meet_the_criteria (){
        CORE_DataFaker_CatalogHierarchy catalogHierarchy1 = new CORE_DataFaker_CatalogHierarchy('test1');
        Account account = CORE_DataFaker_Account.getStudentAccount('test1');
        Opportunity opportunity1 = CORE_DataFaker_Opportunity.getOpportunity(catalogHierarchy1.division.Id, 
            catalogHierarchy1.businessUnit.Id, 
            catalogHierarchy1.studyArea.Id,
            catalogHierarchy1.curriculum.Id,
            catalogHierarchy1.level.Id,
            catalogHierarchy1.intake.Id, 
            account.Id, 
            'Lead', 
            'Lead - New'
        ); 
        CORE_DataFaker_CatalogHierarchy catalogHierarchy2 = new CORE_DataFaker_CatalogHierarchy('test2');
        Opportunity opportunity2 = CORE_DataFaker_Opportunity.getOpportunity(catalogHierarchy2.division.Id, 
            catalogHierarchy2.businessUnit.Id, 
            catalogHierarchy2.studyArea.Id,
            catalogHierarchy2.curriculum.Id,
            catalogHierarchy2.level.Id,
            catalogHierarchy2.intake.Id, 
            account.Id, 
            'Lead', 
            'Lead - New'
        ); 

        String email1 = 'test1@test.com';
        String email2 = 'test2@test.com';
        CORE_Business_Unit__c bu1 = catalogHierarchy1.businessUnit;
        bu1.CORE_Generic_contact_email__c = email1;
        CORE_Business_Unit__c bu2 = catalogHierarchy2.businessUnit;
        bu2.CORE_Generic_contact_email__c = email1;
        
        update new List<CORE_Business_Unit__c>{bu1,bu2};
        
        List<Opportunity> oppsFound = new List<Opportunity>{opportunity1, opportunity2};
        
        List<String> recipientAdresses = new List<String> {email2};

        CORE_InboundEmailHandler emailHandler = new CORE_InboundEmailHandler();
        Opportunity result = emailHandler.selectOpportunityInManyOppCase(oppsFound,recipientAdresses);
        System.assertEquals(null, result);
    }

    @IsTest
    public static void selectOpportunityInManyOppCase_Should_Return_the_only_opp_which_meet_the_criteria (){
        CORE_DataFaker_CatalogHierarchy catalogHierarchy1 = new CORE_DataFaker_CatalogHierarchy('test1');
        Account account = CORE_DataFaker_Account.getStudentAccount('test1');
        Opportunity opportunity1 = CORE_DataFaker_Opportunity.getOpportunity(catalogHierarchy1.division.Id, 
            catalogHierarchy1.businessUnit.Id, 
            catalogHierarchy1.studyArea.Id,
            catalogHierarchy1.curriculum.Id,
            catalogHierarchy1.level.Id,
            catalogHierarchy1.intake.Id, 
            account.Id, 
            'Lead', 
            'Lead - New'
        ); 
        CORE_DataFaker_CatalogHierarchy catalogHierarchy2 = new CORE_DataFaker_CatalogHierarchy('test2');
        Opportunity opportunity2 = CORE_DataFaker_Opportunity.getOpportunity(catalogHierarchy2.division.Id, 
            catalogHierarchy2.businessUnit.Id, 
            catalogHierarchy2.studyArea.Id,
            catalogHierarchy2.curriculum.Id,
            catalogHierarchy2.level.Id,
            catalogHierarchy2.intake.Id, 
            account.Id, 
            'Lead', 
            'Lead - New'
        ); 

        String email1 = 'test1@test.com';
        String email2 = 'test2@test.com';
        CORE_Business_Unit__c bu1 = catalogHierarchy1.businessUnit;
        bu1.CORE_Generic_contact_email__c = email1;
        CORE_Business_Unit__c bu2 = catalogHierarchy2.businessUnit;
        bu2.CORE_Generic_Area_contact_email__c = 'test@test.com;'+email2;
        
        update new List<CORE_Business_Unit__c>{bu1,bu2};
        
        List<Opportunity> oppsFound = new List<Opportunity>{opportunity1, opportunity2};
        
        List<String> recipientAdresses = new List<String> {email1};

        CORE_InboundEmailHandler emailHandler = new CORE_InboundEmailHandler();
        Opportunity result = emailHandler.selectOpportunityInManyOppCase(oppsFound,recipientAdresses);
        System.assertNotEquals(null, result);
        System.assertEquals(opportunity1.Id, result.Id);

        recipientAdresses = new List<String> {'test2@test2.com',email2};
        result = emailHandler.selectOpportunityInManyOppCase(oppsFound,recipientAdresses);
        System.assertNotEquals(null, result);
        System.assertEquals(opportunity2.Id, result.Id);
    }

    @IsTest
    public static void handleInboundEmail_Should_Create_EmailMessage_and_use_existing_account_when_it_is_possible(){
        Account account = CORE_DataFaker_Account.getStudentAccount('pkey');
        CORE_InboundEmailHandler inboundEmail = new CORE_InboundEmailHandler();
        List<Group> queues = [SELECT Id, Email FROM Group WHERE Name = 'TestQueue' AND Type = 'Queue'];
        System.assertEquals(1, queues.size());

        Messaging.InboundEmail email = new Messaging.InboundEmail();
        email.fromAddress = Account.PersonEmail;
        email.fromName = 'Testa Testb';
        email.plainTextBody = 'text body test';
        email.htmlBody = 'text body test html';
        email.messageId = '1235messageId';
        email.subject = 'subject';
        email.toAddresses = new List<String> {queues.get(0).Email};


        Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();
        envelope.fromAddress = email.fromAddress;
        envelope.toAddress = email.toAddresses.get(0);
        Test.startTest();
        inboundEmail.handleInboundEmail(email, envelope);
        Test.stopTest();

        EmailMessage emailMessage = [SELECT Id,MessageIdentifier , Incoming, Status, HtmlBody, 
            TextBody, Subject, ToAddress, CcAddress, FromAddress, FromName, headers, RelatedToId, 
            CORE_TECH_TaskOwnerId__c
            FROM EmailMessage WHERE MessageIdentifier = :email.messageId];
        
        System.assertEquals(1, queues.size());

        System.assertEquals('0', emailMessage.Status);
        System.assertEquals(true, emailMessage.Incoming);
        System.assertEquals(email.htmlBody, emailMessage.HtmlBody);
        System.assertEquals(email.plainTextBody, emailMessage.TextBody);
        System.assertEquals(email.subject, emailMessage.Subject);
        System.assertEquals(envelope.toAddress , emailMessage.ToAddress);
        System.assertEquals(envelope.fromAddress, emailMessage.FromAddress);
        System.assertEquals(email.fromName , emailMessage.FromName);
        System.assertEquals(account.Id , emailMessage.RelatedToId);
        System.assertEquals(queues.get(0).Id , emailMessage.CORE_TECH_TaskOwnerId__c);
    }

    @IsTest
    public static void handleInboundEmail_Should_avoid_to_Create_duplicate_EmailMessage(){
        Account account = CORE_DataFaker_Account.getStudentAccount('pkey');
        CORE_InboundEmailHandler inboundEmail = new CORE_InboundEmailHandler();
        List<Group> queues = [SELECT Id, Email FROM Group WHERE Name = 'TestQueue' AND Type = 'Queue'];
        System.assertEquals(1, queues.size());

        Messaging.InboundEmail email = new Messaging.InboundEmail();
        email.fromAddress = Account.PersonEmail;
        email.fromName = 'Testa Testb';
        email.plainTextBody = 'text body test';
        email.htmlBody = 'text body test html';
        email.messageId = '1235messageId';
        email.subject = 'subject';
        email.toAddresses = new List<String> {queues.get(0).Email};
        Messaging.InboundEmail.Header header1 = new Messaging.InboundEmail.Header();
        Messaging.InboundEmail.Header header2 = new Messaging.InboundEmail.Header();
        Messaging.InboundEmail.Header header3 = new Messaging.InboundEmail.Header();
        Date dateTmp = System.today();
        Integer yearTmp = dateTmp.year();
        String dayTmp = '00' + String.valueOf(dateTmp.day());
        dayTmp = dayTmp.right(2);

        Map<String,String> monthNumberByPrefix = new Map<String,String>();
        monthNumberByPrefix.put('1','Jan');
        monthNumberByPrefix.put('2','Feb');
        monthNumberByPrefix.put('3','Mar');
        monthNumberByPrefix.put('4','Apr');
        monthNumberByPrefix.put('5','May');
        monthNumberByPrefix.put('6','Jun');
        monthNumberByPrefix.put('7','Jul');
        monthNumberByPrefix.put('8','Aug');
        monthNumberByPrefix.put('9','Sep');
        monthNumberByPrefix.put('10','Oct');
        monthNumberByPrefix.put('11','Nov');
        monthNumberByPrefix.put('12','Dec');
        String monthTmp = monthNumberByPrefix.get(String.valueOf(dateTmp.month()));
        
        
        header1.name= 'Date';
        header1.value= 'Fri, '+ dayTmp +' '+ monthTmp +' '+ yearTmp +' 13:53:44 +0000';
        header2.name= 'Date';
        header2.value= 'Fri, '+ dayTmp +' '+ monthTmp +' '+ yearTmp +' 13:54:44 +0000';
        header3.name= 'Date';
        header3.value= 'Fri, '+ dayTmp +' '+ monthTmp +' '+ yearTmp +' 13:58:44 +0000';
        email.headers = new List<Messaging.InboundEmail.Header> {header1};
        Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();
        envelope.fromAddress = email.fromAddress;
        envelope.toAddress = email.toAddresses.get(0);

        inboundEmail.handleInboundEmail(email, envelope);
        Test.startTest();
        Messaging.InboundEmail email2 = email;
        email2.messageId = '1235messageId2';
        email2.headers = new List<Messaging.InboundEmail.Header> {header2};
        inboundEmail = new CORE_InboundEmailHandler();
        inboundEmail.handleInboundEmail(email2, envelope);
        Messaging.InboundEmail email3 = email;
        email3.messageId = '1235messageId3';
        email3.headers = new List<Messaging.InboundEmail.Header> {header3};
        inboundEmail = new CORE_InboundEmailHandler();
        inboundEmail.handleInboundEmail(email3, envelope);
        Test.stopTest();

        List<String> emailMessagesIds = new List<String>{email.messageId,email2.messageId,email3.messageId};
        List<EmailMessage> emailMessages = [SELECT Id,MessageIdentifier , Incoming, Status, HtmlBody, 
            TextBody, Subject, ToAddress, CcAddress, FromAddress, FromName, headers, RelatedToId, MessageDate,
            CORE_TECH_TaskOwnerId__c
            FROM EmailMessage];
        System.debug('emailMessages.get(0).MessageDate = ' + emailMessages.get(0).MessageDate);  
        System.assertEquals(2 , emailMessages.size());
    }

    @IsTest
    public static void createNewPointOfContact_Should_Insert_Poc(){
        Account account = CORE_DataFaker_Account.getStudentAccount('pkey');
        CORE_DataFaker_CatalogHierarchy catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test');
        Opportunity opportunity = CORE_DataFaker_Opportunity.getOpportunity(
            catalogHierarchy.division.Id, 
            catalogHierarchy.businessUnit.Id, 
            catalogHierarchy.studyArea.Id,
            catalogHierarchy.curriculum.Id,
            catalogHierarchy.level.Id,
            catalogHierarchy.intake.Id,
            account.Id, 
            'Lead', 
            'Lead - New'
        );
        CORE_InboundEmailHandler.createNewPointOfContact(opportunity.Id, account.Id);

        List<CORE_Point_of_Contact__c> pocs = [SELECT Id FROM CORE_Point_of_Contact__c];

        System.assertEquals(1,pocs.Size());
    }

    @IsTest
    public static void createNewOpportunity_Should_Insert_Opp_and_return_it(){
        Account account = CORE_DataFaker_Account.getStudentAccount('pkey');
        CORE_DataFaker_CatalogHierarchy catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test');

        CORE_InboundEmailHandler inboundEmail = new CORE_InboundEmailHandler();
        Opportunity opp = inboundEmail.createNewOpportunity(account.Id, catalogHierarchy.businessUnit.Id, CORE_AcademicYearProcess.getAcademicYear());

        List<Opportunity> opps = [SELECT Id FROM Opportunity];

        System.assertEquals(1,opps.Size());
        System.assertEquals(opp.Id,opps.get(0).Id);
    }


    @IsTest
    public static void handleInboundEmail_Should_Create_EmailMessage_related_to_existing_opportunity_when_existing_email(){
        Account account = CORE_DataFaker_Account.getStudentAccount('pkey');
        CORE_DataFaker_CatalogHierarchy catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test');
        Opportunity opportunity = CORE_DataFaker_Opportunity.getOpportunity(
            catalogHierarchy.division.Id, 
            catalogHierarchy.businessUnit.Id, 
            catalogHierarchy.studyArea.Id,
            catalogHierarchy.curriculum.Id,
            catalogHierarchy.level.Id,
            catalogHierarchy.intake.Id,
            account.Id, 
            'Lead', 
            'Lead - New'
        );

        List<Group> queues = [Select Id, Email from Group where Name = 'TestQueue' and Type = 'Queue'];
        System.assertEquals(1, queues.size());


        EmailMessage emailMessage = new EmailMessage();
        emailMessage.MessageIdentifier = 'MessageId';
        emailMessage.Incoming = false;
        emailMessage.Status = '3'; // = new 
        emailMessage.HtmlBody = 'bodyTest html';
        emailMessage.TextBody = 'bodyTest';
        
        emailMessage.Subject = 'outboundSubject';

        emailMessage.ToAddress = Account.PersonEmail;
        
        emailMessage.FromAddress = queues.get(0).Email;
        emailMessage.FromName = 'generic email';
        emailMessage.RelatedToId = opportunity.Id;
        insert emailMessage;

        CORE_InboundEmailHandler inboundEmail = new CORE_InboundEmailHandler();

        Messaging.InboundEmail email = new Messaging.InboundEmail();
        email.fromAddress = Account.PersonEmail;
        email.fromName = 'Testa Testb';
        email.plainTextBody = 'text body test';
        email.htmlBody = 'text body test html';
        email.messageId = '1235messageId';
        email.subject = 'subject';
        email.toAddresses = new List<String> {emailMessage.FromAddress};
        email.inReplyTo = emailMessage.MessageIdentifier;

        Messaging.InboundEnvelope envelope = new Messaging.InboundEnvelope();
        envelope.fromAddress = email.fromAddress;
        envelope.toAddress = email.toAddresses.get(0);
        Test.startTest();
        inboundEmail.handleInboundEmail(email, envelope);
        Test.stopTest();

        EmailMessage emailMessageResult = [Select Id,MessageIdentifier , Incoming, Status, HtmlBody, 
            TextBody, Subject, ToAddress, CcAddress, FromAddress, FromName, headers, RelatedToId,
            CORE_TECH_TaskOwnerId__c
            FROM EmailMessage where subject = 'subject'];

        System.assertEquals('0', emailMessageResult.Status);
        System.assertEquals(true, emailMessageResult.Incoming);
        System.assertEquals(email.htmlBody, emailMessageResult.HtmlBody);
        System.assertEquals(email.plainTextBody, emailMessageResult.TextBody);
        System.assertEquals(email.subject, emailMessageResult.Subject);
        System.assertEquals(envelope.toAddress , emailMessageResult.ToAddress);
        System.assertEquals(envelope.fromAddress, emailMessageResult.FromAddress);
        System.assertEquals(email.fromName , emailMessageResult.FromName);
        System.assertEquals(opportunity.Id , emailMessageResult.RelatedToId);
        System.assertEquals(opportunity.ownerId , emailMessageResult.CORE_TECH_TaskOwnerId__c);
    }

    @IsTest
    public static void createContentDocumentLinks_Should_Create_attachement_related_to_emailMessage(){
        Account account = CORE_DataFaker_Account.getStudentAccount('pkey');
        List<Group> queues = [Select Id, Email from Group where Name = 'TestQueue' and Type = 'Queue'];

        EmailMessage emailMessage = new EmailMessage();
        emailMessage.MessageIdentifier = 'MessageId';
        emailMessage.Incoming = false;
        emailMessage.Status = '3'; // = new 
        emailMessage.HtmlBody = 'bodyTest html';
        emailMessage.TextBody = 'bodyTest';
        
        emailMessage.Subject = 'outboundSubject';

        emailMessage.ToAddress = Account.PersonEmail;
        
        emailMessage.FromAddress = queues.get(0).Email;
        emailMessage.FromName = 'generic email';
        emailMessage.RelatedToId = account.Id;
        insert emailMessage;

        Messaging.InboundEmail.BinaryAttachment attachment = new Messaging.InboundEmail.BinaryAttachment();
        attachment.body = blob.valueOf('my attachment text');
        attachment.fileName = 'textfileone.txt';
        attachment.mimeTypeSubType = 'text/plain';


        Test.startTest();
        CORE_InboundEmailHandler inboundEmail = new CORE_InboundEmailHandler();

        inboundEmail.createContentDocumentLinks(new Messaging.inboundEmail.BinaryAttachment[] { attachment },emailMessage.Id);
        Test.stopTest();

        List<ContentVersion> cvList = [Select Title, VersionData, PathOnClient, id, ContentDocumentId FROM ContentVersion where Title = :attachment.fileName ];
        System.assertEquals(1,cvList.size());
        
        List<ContentDocumentLink> cdlList = [Select ContentDocumentId,LinkedEntityId, ShareType, Visibility FROM ContentDocumentLink where LinkedEntityId = :emailMessage.Id];
        System.assertEquals(1,cdlList.size());

  

        System.assertEquals(attachment.fileName ,cvList.get(0).Title);
        System.assertEquals(EncodingUtil.base64Encode(attachment.body), EncodingUtil.base64Encode(cvList.get(0).VersionData));
        System.assertEquals('/' + attachment.fileName,cvList.get(0).PathOnClient);
        
        System.assertEquals(cvList.get(0).ContentDocumentId, cdlList.get(0).ContentDocumentId);
        System.assertEquals(emailMessage.Id, cdlList.get(0).LinkedEntityId);
        System.assertEquals('V', cdlList.get(0).ShareType);
        System.assertEquals('AllUsers', cdlList.get(0).Visibility);
    }

    @IsTest
    public static void getRelatedOpportunity_Should_Return_relatedOpportunity_based_on_account_and_businessUnit(){
        CORE_DataFaker_CatalogHierarchy catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test');
        CORE_Intake__c intake = [Select Id FROM CORE_Intake__c WHERE Id = :catalogHierarchy.intake.Id];
        intake.CORE_Academic_Year__c = CORE_AcademicYearProcess.getAcademicYear();
        update intake;

        Account account = CORE_DataFaker_Account.getStudentAccount('test');

        Opportunity opportunity = CORE_DataFaker_Opportunity.getOpportunity(
            catalogHierarchy.division.Id, 
            catalogHierarchy.businessUnit.Id, 
            catalogHierarchy.studyArea.Id,
            catalogHierarchy.curriculum.Id,
            catalogHierarchy.level.Id,
            catalogHierarchy.intake.Id,
            account.Id, 
            '', 
            ''
        );
        opportunity.CORE_OPP_Academic_Year__c = intake.CORE_Academic_Year__c;
        update opportunity;

        CORE_InboundEmailHandler inboundEmail = new CORE_InboundEmailHandler();
        CORE_Inbound_Email_Setting__mdt inboundEmailSettings = inboundEmail.getInboundEmailSettings();

        List<String> emails = new List<String>{catalogHierarchy.businessUnit.CORE_Generic_contact_email__c};
        Test.startTest();
        Opportunity result;

        result = inboundEmail.getRelatedOpportunity(account.Id, inboundEmailSettings,emails);
        System.assertNotEquals(null, result);
        System.assertEquals(opportunity.Id, result.Id);

        opportunity.StageName = 'Closed Won';
        opportunity.CORE_Change_Status__c = true;
        Update opportunity;

        result = inboundEmail.getRelatedOpportunity(account.Id, inboundEmailSettings,emails);
        //Opportunity must not be found because it is closed
        System.assertEquals(null, result);

        Test.stopTest();
    }

    @IsTest
    public static void getRelatedOpportunity_Should_Return_null_if_at_least_one_parameter_is_empty(){
        CORE_DataFaker_CatalogHierarchy catalogHierarchy1 = new CORE_DataFaker_CatalogHierarchy('test');
        CORE_Business_Unit__c bu1 = catalogHierarchy1.businessUnit;

        CORE_InboundEmailHandler inboundEmail = new CORE_InboundEmailHandler();
        CORE_Inbound_Email_Setting__mdt inboundEmailSettings = inboundEmail.getInboundEmailSettings();

        List<String> emails = new List<String>{bu1.CORE_Generic_contact_email__c};
        Test.startTest();
        Opportunity result;
        result = inboundEmail.getRelatedOpportunity(null, inboundEmailSettings, new List<String>{'false@test.com'});
        System.assertEquals(null, result);
        
        Account account = CORE_DataFaker_Account.getStudentAccount('test');
        result = inboundEmail.getRelatedOpportunity(account.Id, inboundEmailSettings, new List<String>{'false@test.com'});
        System.assertEquals(null, result);

        result = inboundEmail.getRelatedOpportunity(null, inboundEmailSettings, new List<String>{bu1.CORE_Generic_contact_email__c});
        System.assertEquals(null, result);

        Test.stopTest();
    }

    @IsTest
    public static void getRelatedBusinessUnits_Should_Return_ids_of_businessUnit_related_to_given_emailAddresses(){
        CORE_DataFaker_CatalogHierarchy catalogHierarchy1 = new CORE_DataFaker_CatalogHierarchy('test');
        CORE_DataFaker_CatalogHierarchy catalogHierarchy2 = new CORE_DataFaker_CatalogHierarchy('test2');
        CORE_DataFaker_CatalogHierarchy catalogHierarchy3 = new CORE_DataFaker_CatalogHierarchy('test3');
        CORE_Business_Unit__c bu1 = catalogHierarchy1.businessUnit;
        CORE_Business_Unit__c bu2 = catalogHierarchy2.businessUnit;
        CORE_Business_Unit__c bu3 = catalogHierarchy3.businessUnit;
        bu1.CORE_Generic_contact_email__c = 'test@test.fr';
        bu2.CORE_Generic_contact_email__c = 'test@test.fr';
        bu3.CORE_Generic_Admission_contact_email__c = 'test3@test.fr';
        update new List<CORE_Business_Unit__c> {bu1,bu2, bu3};

        CORE_InboundEmailHandler inboundEmail = new CORE_InboundEmailHandler();

        List<String> emails = new List<String>{bu1.CORE_Generic_contact_email__c};
        Test.startTest();
        List<CORE_Business_Unit__c> bus;
        bus = inboundEmail.getRelatedBusinessUnits(emails, CORE_InboundEmailHandler.RelatedBuEmailType.GENERIC);
        Set<Id> results = new Map<Id,CORE_Business_Unit__c>(bus).keySet();
        System.assert(results.contains(bu1.Id));
        System.assert(results.contains(bu2.Id));
        System.assert(!results.contains(bu3.Id));

        emails = new List<String>{ bu3.CORE_Generic_Admission_contact_email__c};

        bus = inboundEmail.getRelatedBusinessUnits(emails, CORE_InboundEmailHandler.RelatedBuEmailType.ADMISSION);
        results = new Map<Id,CORE_Business_Unit__c>(bus).keySet();
        System.assert(!results.contains(bu1.Id));
        System.assert(!results.contains(bu2.Id));
        System.assert(results.contains(bu3.Id));
        Test.stopTest();
    }

    @IsTest
    public static void validatedAddresses_Should_Return_List_of_validated_emailAddresses(){
        List<String> emailAddresses = new List<String>();
        emailAddresses.add('test@test.fr');
        emailAddresses.add('testtest.fr');
        emailAddresses.add('test2@test.fr');

        CORE_InboundEmailHandler inboundEmail = new CORE_InboundEmailHandler();

        Test.startTest();
        List<String> result;

        result = inboundEmail.validatedAddresses(emailAddresses);

        System.assert(result.contains(emailAddresses.get(0)));
        System.assert(!result.contains(emailAddresses.get(1)));
        System.assert(result.contains(emailAddresses.get(2)));

        Test.stopTest();
    }


    @IsTest
    public static void parseAddress_Should_Return_String_of_emailAddresses(){
        List<String> emailAddresses = new List<String>();
        emailAddresses.add('test@test.fr');
        CORE_InboundEmailHandler inboundEmail = new CORE_InboundEmailHandler();

        Test.startTest();
        String result;
        result = inboundEmail.parseAddress(emailAddresses);

        System.assertEquals(result,emailAddresses.get(0));

        emailAddresses.add('test2@test.fr');
        result = inboundEmail.parseAddress(emailAddresses);
        System.assertEquals(result,emailAddresses.get(0)+';'+emailAddresses.get(1));

        for(Integer i = 0 ; i < 4000 ; i++){
            emailAddresses.add('test'+i+'@test.fr');
        }
        result = inboundEmail.parseAddress(emailAddresses);

        System.assert(result.length() < 4000);
        System.assert(result.endsWith('[...]'));
        Test.stopTest();

    }

    @IsTest
    public static void getRelatedAccount_Should_Return_prefill_account_if_account_not_found_and_is_not_internal_domain(){
        EmailMessage emailMessage = new EmailMessage();
        emailMessage.MessageIdentifier = 'MessageId';
        emailMessage.Incoming = false;
        emailMessage.Status = '3'; // = new 
        emailMessage.HtmlBody = 'bodyTest html';
        emailMessage.TextBody = 'bodyTest';
        
        emailMessage.Subject = 'outboundSubject';

        emailMessage.ToAddress = 'test@test.fr';
        
        emailMessage.FromAddress = 'test@test.fr';
        emailMessage.Fromname = 'test';
        insert emailMessage;

        CORE_InboundEmailHandler inboundEmail = new CORE_InboundEmailHandler();
        Id studentRecordType = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get(CORE_InboundEmailHandler.STUDENT_RECORDTYPE_DEVNAME).getRecordTypeId();

        Test.startTest();
        Account result;
        result = inboundEmail.getRelatedAccount(emailMessage, false);

        System.assertNotEquals(null,result);
        System.assertEquals(emailMessage.Fromname,result.LastName);
        System.assertEquals(emailMessage.FromAddress,result.PersonEmail);
        System.assertEquals(studentRecordType,result.RecordTypeId);

        emailMessage.Fromname = 'test testa';
        result = inboundEmail.getRelatedAccount(emailMessage, false);
        System.debug('result.LastName = -----' + result.LastName + '------');
        System.assertEquals('test', result.FirstName);
        System.assertEquals('testa', result.LastName);

        emailMessage.Fromname = null;
        result = inboundEmail.getRelatedAccount(emailMessage, false);
        System.assertEquals('UNKNOWN', result.LastName);

        Test.stopTest();
    }

    @IsTest
    public static void getRelatedAccount_Should_Return_null_if_account_not_found_and_is_internal_domain(){
        EmailMessage emailMessage = new EmailMessage();
        emailMessage.MessageIdentifier = 'MessageId';
        emailMessage.Incoming = false;
        emailMessage.Status = '3'; // = new 
        emailMessage.HtmlBody = 'bodyTest html';
        emailMessage.TextBody = 'bodyTest';
        
        emailMessage.Subject = 'outboundSubject';

        emailMessage.ToAddress = 'test@test.fr';
        
        emailMessage.FromAddress = 'test@test.fr';
        emailMessage.Fromname = 'test';
        insert emailMessage;

        CORE_InboundEmailHandler inboundEmail = new CORE_InboundEmailHandler();
                
        Test.startTest();
        Account result;
        result = inboundEmail.getRelatedAccount(emailMessage, true);

        System.assertEquals(null,result);
        
        Test.stopTest();
    }

    @IsTest
    public static void validateEmail_Should_Return_true_if_email_is_valid(){
        CORE_InboundEmailHandler inboundEmail = new CORE_InboundEmailHandler();
                
        Test.startTest();
        Boolean result;
        result = inboundEmail.validateEmail('test.tes@test.fr');

        System.assertEquals(true,result);
        
        Test.stopTest();
    }

    @IsTest
    public static void validateEmail_Should_Return_false_if_email_is_invalid(){
        CORE_InboundEmailHandler inboundEmail = new CORE_InboundEmailHandler();
                
        Test.startTest();
        Boolean result;
        result = inboundEmail.validateEmail('test.tes');

        System.assertEquals(false,result);
        Test.stopTest();
    }
    
    @IsTest
    public static void getInternalDomains_Should_Return_list_of_domains_available_in_inboundEmailSettings(){
        CORE_InboundEmailHandler inboundEmail = new CORE_InboundEmailHandler();
        
        CORE_Inbound_Email_Setting__mdt emailSettings = inboundEmail.getInboundEmailSettings();
        
        Test.startTest();
        List<String> results = new List<String>();

        results = inboundEmail.getInternalDomains(emailSettings);
        System.assertEquals(0,results.Size());
        emailSettings.CORE_Internal_domains__c = 'pfh.de,google.fr';

        results = inboundEmail.getInternalDomains(emailSettings);
        System.assertNotEquals(0,results.Size());
        System.assertEquals('pfh.de',results.get(0));
        System.assertEquals('google.fr',results.get(1));

        Test.stopTest();
    }

    @IsTest
    public static void getInboundEmailSettings_Should_Return_inboundEmailSettings(){
        CORE_InboundEmailHandler inboundEmail = new CORE_InboundEmailHandler();
        
        Test.startTest();
        CORE_Inbound_Email_Setting__mdt result = inboundEmail.getInboundEmailSettings();
        Test.stopTest();
        
        System.assertNotEquals(null,result);
        System.assertNotEquals(null,result.CORE_AssignToOppOwner__c);
    }
    @IsTest
    public static void testHandleIncomingEmailFromExternalThirdParty(){
        Id B2BExternalId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('CORE_B2B_External_Partner').getRecordTypeId();
        Id BusinessContactId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Business_Contact').getRecordTypeId();
        EmailMessage emailMessage = new EmailMessage();
        emailMessage.MessageIdentifier = 'MessageId';
        emailMessage.Incoming = false;
        emailMessage.Status = '3'; // = new 
        emailMessage.HtmlBody = 'bodyTest html';
        emailMessage.TextBody = 'bodyTest';
        
        emailMessage.Subject = 'outboundSubject';

        emailMessage.ToAddress = 'test@test.fr';
        
        emailMessage.FromAddress = 'test@test.fr';
        emailMessage.Fromname = 'test';
        Account acct = new account( Name='F Account', RecordTypeId=B2BExternalId);
        insert acct;
        Contact ctc = new Contact(Email ='test@test.fr', LastName='FTest',RecordTypeId=BusinessContactId,AccountId=acct.Id);
        insert ctc;
        CORE_InboundEmailHandler inboundEmail = new CORE_InboundEmailHandler();
        Test.startTest();
        EmailMessage em = inboundEmail.handleIncomingEmailFromExternalThirdParty(emailMessage);
        Test.stopTest();
        System.assertEquals(em.CORE_TECH_WhoId__c, ctc.Id);

    }
    @IsTest
    public static void testHandleIncomingEmailFromExternalThirdPartyWithWebsiteDomain(){
        Id B2BExternalId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('CORE_B2B_External_Partner').getRecordTypeId();
        Id BusinessContactId = Schema.SObjectType.Contact.getRecordTypeInfosByDeveloperName().get('Business_Contact').getRecordTypeId();
        EmailMessage emailMessage = new EmailMessage();
        emailMessage.MessageIdentifier = 'MessageId';
        emailMessage.Incoming = false;
        emailMessage.Status = '3'; // = new 
        emailMessage.HtmlBody = 'bodyTest html';
        emailMessage.TextBody = 'bodyTest';
        
        emailMessage.Subject = 'outboundSubject';

        emailMessage.ToAddress = 'test@test.fr';
        
        emailMessage.FromAddress = 'test@test.fr';
        emailMessage.Fromname = 'test';
        Account acct = new account( Name='F Account', RecordTypeId=B2BExternalId,WebSite='www.test.fr');
        insert acct;
        Contact ctc = new Contact(Email ='marcus@marcus.fr', LastName='FTest',RecordTypeId=BusinessContactId,AccountId=acct.Id);
        insert ctc;
        CORE_InboundEmailHandler inboundEmail = new CORE_InboundEmailHandler();
        Test.startTest();
        inboundEmail.handleIncomingEmailFromExternalThirdParty(emailMessage);
        Test.stopTest();
        System.assertEquals(1, [SELECT ID FROM COntact where Email = 'test@test.fr'].size());
    }
}