@IsTest
public with sharing class CORE_DataFaker_ServiceAppointment {
    public static ServiceAppointment getServiceAppointment(Id parentId, DateTime earliestStartTime) {
        ServiceAppointment sa = getServiceAppointmentWithoutInsert(parentId, earliestStartTime);

        Insert sa;
        return sa;
    }
    public static ServiceAppointment getServiceAppointment(Id parentId, Id worktypeId, DateTime earliestStartTime) {
        ServiceAppointment sa = getServiceAppointmentWithoutInsert(parentId, earliestStartTime);
		sa.worktypeId = worktypeId;
        Insert sa;
        return sa;
    }

    public static ServiceAppointment getServiceAppointmentWithoutInsert(Id parentId, DateTime earliestStartTime) {
        ServiceAppointment sa = new ServiceAppointment(
            ParentRecordId = parentId,
            DueDate = earliestStartTime.addDays(1),
            EarliestStartTime = earliestStartTime,
            SchedEndTime = earliestStartTime.addHours(1),
            SchedStartTime = earliestStartTime,
            Status = 'Scheduled',
            AppointmentType = 'Call'
        );

        return sa;
    }

    public static AssignedResource getAssignedResource(Id serviceAppointmentId, Id serviceResourceId) {
        AssignedResource assignedResource = getAssignedResourceWithoutInsert(serviceAppointmentId, serviceResourceId);

        Insert assignedResource;
        return assignedResource;
    }

    public static AssignedResource getAssignedResourceWithoutInsert(Id serviceAppointmentId, Id serviceResourceId) {
        AssignedResource assignedResource = new assignedResource(
            IsPrimaryResource = true,
            IsRequiredResource = true,
            ServiceAppointmentId = serviceAppointmentId,
            ServiceResourceId = serviceResourceId
        );

        return assignedResource;
    }

    public static ServiceResource getServiceResource(Id userId, String name) {
        ServiceResource serviceResource = getServiceResourceWithoutInsert(userId, name);

        Insert serviceResource;
        return serviceResource;
    }

    public static ServiceResource getServiceResourceWithoutInsert(Id userId, String name) {
        ServiceResource serviceResource = new ServiceResource(
            Name = name,
            Description = 'test sr',
            IsActive = true,
            RelatedRecordId = userId,
            ResourceType = 'T'
        );

        return serviceResource;
    }
    
}