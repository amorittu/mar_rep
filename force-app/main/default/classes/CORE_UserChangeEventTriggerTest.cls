@IsTest
public class CORE_UserChangeEventTriggerTest {
    @IsTest
    public static void insert_userChangeEvent_should_not_call_api_when_provisioning_is_not_enable() {
        
        User u = CORE_DataFaker_User.getUser('testCORE_UserTriggerTest1');

        Test.enableChangeDataCapture();
        u.email = 'updatedEmail@gmail.com';
        update u;
        Test.getEventBus().deliver();

        System.assertEquals(null, [Select Id,CORE_FLEX_Twilio_SID__c from user where Id = :u.Id].CORE_FLEX_Twilio_SID__c);
    }
}