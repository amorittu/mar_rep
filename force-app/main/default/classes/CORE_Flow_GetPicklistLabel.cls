global class CORE_Flow_GetPicklistLabel {

    @InvocableMethod
    global static List<String> getPicklistLabel(List<PicklistParams> params) {
        List<String> pickListLabelList = new List<String>();
        Schema.SObjectType currentObj = Schema.getGlobalDescribe().get(params[0].objApiName);
        Schema.DescribeSObjectResult res = currentObj.getDescribe();
        Schema.DescribeFieldResult fieldResult = res.fields.getMap().get(params[0].fieldApiName).getDescribe();

        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();

        for( Schema.PicklistEntry pl : ple){
            if(pl.getValue() == params[0].fieldValue){
                pickListLabelList.add(pl.getLabel());
                break;
            }
        }

        return pickListLabelList;
    }

    global class PicklistParams {

        @InvocableVariable(required=true)
        global String objApiName;

        @InvocableVariable(required=true)
        global String fieldApiName;

        @InvocableVariable(required=true)
        global String fieldValue;

    }
}