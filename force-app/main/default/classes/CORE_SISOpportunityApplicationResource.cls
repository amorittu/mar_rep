@RestResource(urlMapping='/opportunity/applicant/*')
global class CORE_SISOpportunityApplicationResource {
    @HttpPost
    global static void postOpportunityApplicant(){
        RestRequest req = RestContext.request;
        RestResponse res = RestContext.response;

        Map<String,Object> reqBody = (Map<String,Object>) JSON.deserializeUntyped(req.requestBody.toString());
        CORE_SISOpportunityApplicationWrapper.GlobalWrapper reqWrapper = new CORE_SISOpportunityApplicationWrapper.GlobalWrapper();
        reqWrapper.acquisitionPointOfContact = (CORE_SISOpportunityApplicationWrapper.AcquisitionPointOfContactWrapper) JSON.deserialize(JSON.serialize(reqBody.get('acquisitionPointOfContact')), CORE_SISOpportunityApplicationWrapper.AcquisitionPointOfContactWrapper.class);
        reqWrapper.Consents = (List<Object>) reqBody.get('consents');
        reqWrapper.opportunity = (Map<String,Object>) reqBody.get('opportunity');
        reqWrapper.mainInterest = (CORE_SISOpportunityApplicationWrapper.MainInterestWrapper) JSON.deserialize(JSON.serialize(reqBody.get('mainInterest')), CORE_SISOpportunityApplicationWrapper.MainInterestWrapper.class);
        reqWrapper.personAccount = (Map<String,Object>) reqBody.get('personAccount');
        
        DateTime apiCallDate = Datetime.now();

        Account existedPersonAccount;
        Savepoint sp = Database.setSavepoint();
        try{
        
            CORE_SISOpportunityApplicationProcess process = new CORE_SISOpportunityApplicationProcess(apiCallDate,reqWrapper);
            process.execute();
            if(process.errors.size()>0){
                res.responseBody = Blob.valueOf(JSON.serializePretty(process.errors, false));
                res.statusCode=400;
            }
            else {
                CORE_SISOpportunityApplicationWrapper.ResultWrapper resultWrapper = new CORE_SISOpportunityApplicationWrapper.ResultWrapper();
                CORE_SISOpportunityApplicationWrapper.ResultAccountWrapper accountWrapper= new CORE_SISOpportunityApplicationWrapper.ResultAccountWrapper(); 
                CORE_SISOpportunityApplicationWrapper.ResultOpportunityWrapper opportunityWrapper= new CORE_SISOpportunityApplicationWrapper.ResultOpportunityWrapper(); 
                accountWrapper.id_Salesforce_PersonAccount = process.selectedPA.Id;
                accountWrapper.id_SIS_PersonAccount = process.selectedPA.CORE_SISStudentID__c;
                opportunityWrapper.id_Salesforce_Opportunity = process.selectedOpportunity.Id;
                opportunityWrapper.id_SIS_Opportunity = process.selectedOpportunity.CORE_OPP_SIS_Opportunity_Id__c;
                resultWrapper.Account = accountWrapper;
                resultWrapper.Opportunity = opportunityWrapper;
                res.responseBody = Blob.valueOf(JSON.serializePretty(resultWrapper));
                
                res.statusCode=200;
            }
        }catch(Exception e ){
            Database.rollback(sp);
            res.statusCode=500;
            CORE_SISOpportunityApplicationWrapper.ErrorWrapper unexpectedError = new CORE_SISOpportunityApplicationWrapper.ErrorWrapper(
                    CORE_LeadFormAcquistionInputValidator.UNEXPECTED_MSG + ': ' + e.getMessage(),
                    CORE_LeadFormAcquistionInputValidator.UNEXPECTED_CODE
                    );
                    
        res.responseBody = Blob.valueOf(JSON.serializePretty(unexpectedError, false));
        }
    }
}