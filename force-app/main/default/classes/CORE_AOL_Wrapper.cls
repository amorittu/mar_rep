global class CORE_AOL_Wrapper {
    global class ResultWrapperGlobal {
		global String status;
        global String message;
        global ResultWrapperGlobal(){
		}
        global ResultWrapperGlobal(String status, String message){
			this.status = status;
			this.message = message;
		}
	}

    global class ResultWrapperOpportuntiyExist {
        global String status;
        global String message;
		global Opportunity opportunity;
        
        global ResultWrapperOpportuntiyExist(){
		}
		global ResultWrapperOpportuntiyExist(String status, String message){
            this.status= status;
            this.message = message;
		}

		global ResultWrapperOpportuntiyExist(String status, Opportunity opportunity){
            this.status= status;
            this.opportunity = opportunity;
		}
	}


    global class ResultWrapper{
		global String statut;
		global String message;
        global OpportunityWrapper opportunity;

        global ResultWrapper(){
		}
        global ResultWrapper(String statut, String message, OpportunityWrapper opportunity){
			this.statut = statut;
			this.message = message;
            this.opportunity = opportunity;
		}
    }

    global class ErrorWrapper{
		global String message;
		global String errorCode;

        global ErrorWrapper(String message, String errorCode){
			this.message = message;
			this.errorCode = errorCode;
		}
    }

    global class OpportunityWrapper extends CORE_ObjectWrapper {
		global String Id;
		global String applicationOnlineId;
		global String sisOpportunityId;
		global String sisStudentAcademicRecordId;
        global String accountId;
        global OpportunityWrapper(){
        }
        global OpportunityWrapper(Opportunity opportunity){
            this.Id = opportunity.Id;
            this.applicationOnlineId = opportunity?.CORE_OPP_Application_Online_ID__c;
            this.sisOpportunityId = opportunity?.CORE_OPP_SIS_Opportunity_Id__c;
            this.sisStudentAcademicRecordId = opportunity?.CORE_OPP_SIS_Student_Academic_Record_ID__c;
            this.accountId = opportunity.AccountId;
		}
	}

    global class AccountWrapper extends CORE_ObjectWrapper {
		global String salutation;
		global String firstname;
		global String lastname;
		global Date birthdate;
        global String mainNationality;
        global String personMailingStreet;
        global String personMailingPostalCode;
        global String personMailingCity;
        global String personMailingState;
        global String personMailingCountry;
        global Boolean updateIfNotNull;
        global String cityOfBirth;
        global String countryOfBirth;
        global String deptOfBirth;
        global String gender;
        global String billingPermanentRegion;
        global String billingStreet;
        global String billingCity;
        global String billingPostalCode;
        global String billingState;
        global String billingCountry;
        global String personMailingCurrentRegion;
        global String socialSecNumberFiscalCode;
	}

    global class InterestWrapper extends CORE_ObjectWrapper {
        global String businessUnit;
		global String studyArea;
		global String curriculum;
		global String level;
        global String intake;   
        global Boolean isMain;
        global Boolean isAolRetailRegistered;
        global String personAccountId;
    } 

    global class AOLProgressionFields{
        global Double aolProgression;
		global String documentProgression;
		global Date firstDocumentUploadDate;
		global DateTime lastAolEventDate;
        global DateTime lastDeaAolLoginDate;
        global Boolean isAol;   
        global Boolean isAolOnlineRegistered;
        global Boolean isAolRetailRegistered;
    }
    
    global class DocumentWrapper extends CORE_ObjectWrapper {
        global String externalId;
		global String name;
		global String externalUrl;
		global String type;
        global Date createdDate;   
        global Date approvedDate;
        global Date rejectedDate;
    }

    global class ApplicationFeesPaid extends CORE_ObjectWrapper{
        global String paymentMethod;
		global String transactionId;
		global DateTime receivedDate;
		global DateTime acceptanceDate;
        global Boolean received;   
        global String bank;
        global String sender;
        global Double amount;
    }

    global class DocumentSendingFields extends CORE_ObjectWrapper{
        global String documentSendingChoice;
    }

    global class SelectionTest extends CORE_ObjectWrapper{
        global Date selectionTestDate;
        global String selectionTestShowNoShow;   
        global Double selectionTestNoShowCounter;
        global String selectionTestGrade;
        global String selectionTestResult;
		global String selectionTestTeacher;
        global String selectionStatus;
        global Date skillTestStartDate;
        global String selectionProcessStatus;
        global Date selectionProcessStartDate;
        global Date selectionProcessEndDate;
        global String selectionProcessProgramLeader;
        global String selectionProcessGrade;
        global String selectionProcessResult;
        global Boolean selectionTestNeeded;
        global Boolean selectionProcessNeeded;
    }

    global class ForeignLanguages extends CORE_ObjectWrapper{
        global String language1;
        global String language1Level;
        global String language2;
        global String language2Level;
    }

    global class AcademicDiplomaHistory extends CORE_ObjectWrapper{
        global String diplomaExternalId;
        global String diplomaName;
        global String diplomaLevel;
        global String diplomaSchool;
        global String diplomaCity;
        global String diplomaCountry;
        global String diplomaStatus;
        global String diplomaStudyArea;
        global Date diplomaExpectedGraduationDate;
        global Date diplomaModificationDate;
        global Decimal diplomaGrade;
        global Date diplomaDate;
        global String diplomaRecordType;
    }

    global class EnrollmentFeesPaid extends CORE_ObjectWrapper{
        global Boolean received;
        global String paymentMethod;
        global DateTime receivedDate;
        global DateTime acceptanceDate;
    }

    global class TuitionFeesPaid extends CORE_ObjectWrapper{
        global String paymentMethod;
		global String transactionId;
		global DateTime receivedDate;
		global DateTime acceptanceDate;
        global Boolean received;   
        global String bank;
        global String sender;
        global String paymentType;
        global Double amount;
    }
    
    global class Scholarship extends CORE_ObjectWrapper{
        global String level;
        global Boolean requested;
        global String type;
    }

    global class ExternalThirdParty extends CORE_ObjectWrapper{
        global String retailAgency;
        global String retailAgencyBusinessContact;
    }
}