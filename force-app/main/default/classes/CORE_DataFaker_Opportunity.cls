@IsTest
public class CORE_DataFaker_Opportunity {
    private static final String PKL_ACADEMIC_YEAR = 'CORE_PKL_2023-2024';

    public static Opportunity getAOLOpportunity(Id divisionId, Id businessUnitId, Id accountId, String stageName,String subStatus, String aolExternalId){
        Opportunity opportunity = new Opportunity(
            Name ='test',
            StageName = (String.isBlank(stageName) ? 'Lead' : stageName),
            CORE_OPP_Sub_Status__c = (String.isBlank(subStatus) ? 'Lead - New' : subStatus),
            CORE_OPP_Division__c = divisionId,
            CORE_OPP_Business_Unit__c = businessUnitId,
            CORE_OPP_Academic_Year__c = PKL_ACADEMIC_YEAR,
            AccountId = accountId,
            CloseDate = Date.Today().addDays(2),
            OwnerId = UserInfo.getUserId(),
            CORE_OPP_Main_Opportunity__c = true,
            CORE_OPP_Application_Online_ID__c = aolExternalId
        );

        Insert opportunity;

        return opportunity;
    }

    public static Opportunity getAOLOpportunity(Id divisionId, Id businessUnitId, Id studyAreaId, Id curriculumId, Id levelId, Id intakeId, Id accountId, String stageName,String subStatus, String aolExternalId){
        Opportunity opportunity = new Opportunity(
            Name ='test',
            StageName = (String.isBlank(stageName) ? 'Lead' : stageName),
            CORE_OPP_Sub_Status__c = (String.isBlank(subStatus) ? 'Lead - New' : subStatus),
            CORE_OPP_Division__c = divisionId,
            CORE_OPP_Business_Unit__c = businessUnitId,
            CORE_OPP_Study_Area__c = studyAreaId,
            CORE_OPP_Curriculum__c = curriculumId,
            CORE_OPP_Level__c = levelId,
            CORE_OPP_Intake__c = intakeId,
            CORE_OPP_Academic_Year__c = PKL_ACADEMIC_YEAR,
            AccountId = accountId,
            CloseDate = Date.Today().addDays(2),
            OwnerId = UserInfo.getUserId(),
            CORE_OPP_Main_Opportunity__c = true,
            CORE_OPP_Application_Online_ID__c = aolExternalId
        );

        Insert opportunity;

        return opportunity;
    }

    public static Opportunity getOpportunity(Id divisionId, Id businessUnitId, Id accountId, String stageName,String subStatus){
        Opportunity opportunity = new Opportunity(
            Name ='test',
            StageName = (String.isBlank(stageName) ? 'Lead' : stageName),
            CORE_OPP_Sub_Status__c = (String.isBlank(subStatus) ? 'Lead - New' : subStatus),
            CORE_OPP_Division__c = divisionId,
            CORE_OPP_Business_Unit__c = businessUnitId,
            CORE_OPP_Academic_Year__c = PKL_ACADEMIC_YEAR,
            AccountId = accountId,
            CloseDate = Date.Today().addDays(2),
            OwnerId = UserInfo.getUserId(),
            CORE_OPP_Main_Opportunity__c = true
        );

        Insert opportunity;

        return opportunity;
    }

    public static Opportunity getOpportunity(Id divisionId, Id businessUnitId, Id studyAreaId, Id curriculumId, Id levelId, Id intakeId, Id accountId, String stageName,String subStatus){
        Opportunity opportunity = new Opportunity(
            Name ='test',
            StageName = (String.isBlank(stageName) ? 'Lead' : stageName),
            CORE_OPP_Sub_Status__c = (String.isBlank(subStatus) ? 'Lead - New' : subStatus),
            CORE_OPP_Division__c = divisionId,
            CORE_OPP_Business_Unit__c = businessUnitId,
            CORE_OPP_Study_Area__c = studyAreaId,
            CORE_OPP_Curriculum__c = curriculumId,
            CORE_OPP_Level__c = levelId,
            CORE_OPP_Intake__c = intakeId,
            CORE_OPP_Academic_Year__c = PKL_ACADEMIC_YEAR,
            AccountId = accountId,
            CloseDate = Date.Today().addDays(2),
            OwnerId = UserInfo.getUserId(),
            CORE_OPP_Main_Opportunity__c = true
        );

        Insert opportunity;

        return opportunity;
    }
	 public static Opportunity getSISOpportunity(Id divisionId, Id businessUnitId, Id studyAreaId, Id curriculumId, Id levelId, Id intakeId, Id accountId, String stageName,String subStatus, String sisOpportunityId){
        Opportunity opportunity = new Opportunity(
            Name ='test',
            StageName = (String.isBlank(stageName) ? 'Lead' : stageName),
            CORE_OPP_Sub_Status__c = (String.isBlank(subStatus) ? 'Lead - New' : subStatus),
            CORE_OPP_Division__c = divisionId,
            CORE_OPP_Business_Unit__c = businessUnitId,
            CORE_OPP_Study_Area__c = studyAreaId,
            CORE_OPP_Curriculum__c = curriculumId,
            CORE_OPP_Level__c = levelId,
            CORE_OPP_Intake__c = intakeId,
            CORE_OPP_Academic_Year__c = PKL_ACADEMIC_YEAR,
            AccountId = accountId,
            CloseDate = Date.Today().addDays(2),
            OwnerId = UserInfo.getUserId(),
            CORE_OPP_Main_Opportunity__c = true,
            CORE_OPP_SIS_Opportunity_Id__c= sisOpportunityId
        );

        Insert opportunity;

        return opportunity;
    }

    public static Opportunity getOpportunity(Id divisionId, Id businessUnitId, Id accountId, String stageName,String subStatus, Datetime createdDate, Datetime lastModifiedDate){
        Opportunity opportunity = new Opportunity(
            Name ='test',
            StageName = (String.isBlank(stageName) ? 'Lead' : stageName),
            CORE_OPP_Sub_Status__c = (String.isBlank(subStatus) ? 'Lead - New' : subStatus),
            CORE_OPP_Division__c = divisionId,
            CORE_OPP_Business_Unit__c = businessUnitId,
            CORE_OPP_Academic_Year__c = PKL_ACADEMIC_YEAR,
            AccountId = accountId,
            CloseDate = Date.Today().addDays(2),
            OwnerId = UserInfo.getUserId(),
            CreatedDate = createdDate,
            LastModifiedDate = lastModifiedDate,
            CORE_OPP_Main_Opportunity__c = true
        );

        Insert opportunity;

        return opportunity;
    }
    
    public static OpportunityLineItem getOpportunityLineItem(Id opportunityId, Id pricebookEntryId, String externalId){
        OpportunityLineItem lineItem = new OpportunityLineItem(
            OpportunityId = opportunityId,
            Quantity = 1,
            CORE_External_Id__c = externalId,
            PricebookEntryId = pricebookEntryId,
            TotalPrice = 1
        );

        insert lineItem;
        return lineItem;
    }

    public static CORE_Status_Timestamp_History__c getStatusHistory(Id opportunityId, String status, String externalId){
        CORE_Status_Timestamp_History__c statusHistory = new CORE_Status_Timestamp_History__c(
            CORE_External_Id__c = externalId,
            CORE_Modification_Status_Date_time__c = System.now(),
            CORE_Opportunity__c = opportunityId,
            CORE_Status__c = status
        );

        insert statusHistory;
        return statusHistory;
    }

    public static CORE_Sub_Status_Timestamp_History__c getSubStatusHistory(Id opportunityId, String status, String subStatus, String externalId){
        CORE_Sub_Status_Timestamp_History__c subStatusHistory = new CORE_Sub_Status_Timestamp_History__c(
            CORE_External_Id__c = externalId,
            CORE_Modification_Sub_status_Date_time__c = System.now(),
            CORE_Opportunity__c = opportunityId,
            CORE_Status__c = status,
            CORE_Sub_status__c = subStatus
        );

        insert subStatusHistory;
        return subStatusHistory;
    }
}