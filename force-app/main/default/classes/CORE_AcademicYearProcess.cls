global class CORE_AcademicYearProcess {
    @InvocableMethod
    global static List<String> getCurrentAcademicYear(){
        String currentAcademicYear = getAcademicYear();

        System.debug('CurrentAcademicYear = ' + currentAcademicYear);
        return new List<String> {currentAcademicYear};
    }

    public static String getAcademicYear(){
        CORE_Opportunity_creation_setting__mdt oppCreationSettings = CORE_Opportunity_creation_setting__mdt.getInstance('Default_values');
        String defaultAcademicYear = oppCreationSettings?.CORE_Default_AY_for_Manual_OPP_Creation__c;
        if(!String.isEmpty(defaultAcademicYear) && !test.isRunningTest()){
            return defaultAcademicYear;
        }

        List<String> allAcademicYears = CORE_PicklistUtils.getPicklistValues('Opportunity', 'CORE_OPP_Academic_Year__c');
        String currentAcademicYear = CORE_OpportunityUtils.getAcademicYear(
            defaultAcademicYear,
            null, 
            null, 
            System.now(), 
            allAcademicYears
        );

        return currentAcademicYear;
    }
}