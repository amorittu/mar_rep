public without sharing class CORE_TaskTriggerHandler {
    private static final String EMAIL_TYPE = 'CORE_PKL_Email';
    private static final String DELETE_PERMISSION_API_NAME = 'CORE_Delete_task_and_email';

    public static void handleTrigger(List<Task> oldRecords, List<Task> newRecords, System.TriggerOperation triggerEvent) {
        switch on triggerEvent {
            when BEFORE_INSERT {
                System.debug('CORE_TaskTriggerHandler.beforeInsert : START');
                beforeInsert(newRecords);
                System.debug('CORE_TaskTriggerHandler.beforeInsert : END');
            }
            when BEFORE_UPDATE {
                System.debug('CORE_TaskTriggerHandler.beforeUpdate : START');
                beforeUpdate(oldRecords, newRecords);
                System.debug('CORE_TaskTriggerHandler.beforeUpdate : END');
            }
            when AFTER_UPDATE {
                System.debug('CORE_TaskTriggerHandler.afterUpdate : START');
                afterUpdate(oldRecords, newRecords);
                System.debug('CORE_TaskTriggerHandler.afterUpdate : END');
            }
            when BEFORE_DELETE {
                System.debug('CORE_TaskTriggerHandler.beforeDelete : START');
                beforeDelete(oldRecords);
                System.debug('CORE_TaskTriggerHandler.beforeDelete : END');
            }
        }
    }

    public static void beforeInsert(List<Task> newRecords){
        for(Task task : newRecords){
            if(initActivityFields(null,task)){
                task.CORE_Activity_Type__c = task.Type;
                task.CORE_Activity_Task_Subtype__c = task.TaskSubtype;
            }
            task.CORE_TECH_RecordTypeDevName__c = task.RecordType.DeveloperName;
        }
    }

    public static void beforeUpdate(List<Task> oldRecord, List<Task> newRecord){
        Map<Id,Task> oldTasks = new Map<Id, Task> (oldRecord);

        for(Task task : newRecord){
            if(initActivityFields(oldTasks.get(task.Id),task)){
                task.CORE_Activity_Type__c = task.Type;
                task.CORE_Activity_Task_Subtype__c = task.TaskSubtype;
            }
        }
    }

    private static boolean initActivityFields(Task oldRecord, Task newRecord){
        if(oldRecord == null){
            return true;
        }
        if(String.isBlank(oldRecord.CORE_Activity_Type__c) || String.isBlank(oldRecord.CORE_Activity_Task_Subtype__c)){
            return true;
        }
        if(oldRecord.Type != newRecord.Type || oldRecord.TaskSubtype != newRecord.TaskSubtype){
            return true;
        }

        return false;
    }


    public static void beforeDelete(List<Task> oldRecords){
        boolean userHaveDeletePermission = FeatureManagement.checkPermission(DELETE_PERMISSION_API_NAME);

        if(!userHaveDeletePermission){
            for(Task task : oldRecords){
                task.addError(Label.CORE_Task_DeleteError);
            }
        }
    }
    public static void afterUpdate(List<Task> oldRecords, List<Task> newRecords) {
        //BusinessHours businessHours = [SELECT Id FROM BusinessHours WHERE IsDefault=true];
        List<Opportunity> oppList = new List<Opportunity>();
        List<Task> taskList = new List<Task>();
        Map<Id,Task> oldMapTask= new Map<Id,Task>(oldRecords);
        Map<Id,DateTime> linkOppAndUpdate = new Map<Id,DateTime>();
        for(Task currentRecord : newRecords){
            Task oldRecord = oldMapTask.get(currentRecord.Id);
            if(currentRecord.CORE_Is_Incoming__c==false && currentRecord.Type!='CORE_PKL_Task' && currentRecord.Type!='CORE_PKL_Other' && oldRecord.CompletedDateTime != currentRecord.CompletedDateTime){
                if(currentRecord.WhatId != null){
                    Schema.SObjectType typeObject = currentRecord.WhatId.getsobjecttype();
                    if(typeObject==Schema.Opportunity.getSObjectType()){
                        Opportunity myOppToUpdate = new Opportunity();
                        myOppToUpdate.Id = currentRecord.WhatId;
                        linkOppAndUpdate.put(currentRecord.WhatId, currentRecord.CompletedDateTime);
                    }
                }
            }
            if(currentRecord.CORE_Business_Hours__c!=null){
                if(currentRecord.Status == 'CORE_PKL_Completed' && oldRecord.Status!=currentRecord.Status){
                    Long diffBetween =0;
                    if(currentRecord.CORE_Is_Incoming__c==true || (currentRecord.CORE_Is_Incoming__c==false && currentRecord.CORE_Start_Date__c==null)){
                        diffBetween = System.BusinessHours.diff(currentRecord.CORE_Business_Hours__c, currentRecord.CreatedDate, currentRecord.CompletedDateTime);
                    }else{
                        diffBetween = System.BusinessHours.diff(currentRecord.CORE_Business_Hours__c, currentRecord.CORE_Start_Date__c, currentRecord.CompletedDateTime);
                    }
                    System.debug('diffBetween '+diffBetween);
                    Integer seconds = (Integer)diffBetween / 1000;
                    Integer minutes = seconds / 60;
                    Task myTask = new Task();
                    myTask.Id = currentRecord.Id;
                    myTask.CORE_Resolution_Time__c = minutes;
                    taskList.add(myTask);
                }
            }
        }
        update taskList;
        List<Opportunity> oppListGet=[SELECT Id,CORE_OPP_First_Outbound_Contact_Date__c, CORE_OPP_Division__c, CORE_OPP_Business_Unit__c, CORE_OPP_Study_Area__c, CORE_OPP_Curriculum__c, CORE_OPP_Level__c FROM Opportunity WHERE Id IN :linkOppAndUpdate.keySet() AND CORE_OPP_First_Outbound_Contact_Date__c=null];
        System.debug('oppListGet '+oppListGet);
        for (Opportunity myOpp : oppListGet){
            myOpp.CORE_OPP_First_Outbound_Contact_Date__c = linkOppAndUpdate.get(myOpp.Id);
            oppList.add(myOpp);
        }
        Integer test = oppList.size();
        if (test > 0){
            Opportunity test2 = oppList.get(0);
        }
        System.debug('oppList '+oppList);
        upsert oppList;
    }
}