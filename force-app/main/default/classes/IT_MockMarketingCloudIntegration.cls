@isTest
public class IT_MockMarketingCloudIntegration implements HttpCalloutMock {
    private String method;

    public IT_MockMarketingCloudIntegration(String method){
        this.method = method;
    }

    public HttpResponse respond(HttpRequest param1) {
        HttpResponse response = new HttpResponse();
        if (this.method == 'token') {
            response.setBody('{"access_token":"eyJhbGciOiJIUzI1NiIsImtpZCI6IjQiLCJ2ZXIiOiIxIiwidHlwIjoiSldUIn0.eyJhY2Nlc3NfdG9rZW4iOiJYTHNnZGdPaXNBb3JmWWVldGhOS1FKVlMiLCJjbGllbnRfaWQiOiI5YnJvZ2xiY2h3ajJqa2xxcXNvcHFidHYiLCJlaWQiOjUxMDAwMzQyNCwic3RhY2tfa2V5IjoiUzUwIiwicGxhdGZvcm1fdmVyc2lvbiI6MiwiY2xpZW50X3R5cGUiOiJTZXJ2ZXJUb1NlcnZlciJ9.NjYQv5wrXDjeZbnYxLqjctEce4SRlfcjOIn1NxjykfI.cRj3OunLulLlZndu0E0CD4orLcCIZnk7ebXfZIzX-lY2OpJ2qdJut8jd0799MpcWL7xXgYN00gueu8Az61XFDmwgOJW7i2a4vHb4q5O6vUceyDXPf3jTwWiH1-0RO8tyF8VTXfoCW6NWn8UNfCd45P15FSIf9o7zTXYsM7N4poGqhpPOZ2B","token_type":"Bearer","expires_in":1079,"scope":"offline documents_and_images_read documents_and_images_write saved_content_read saved_content_write","soap_instance_url":"https://mc88sqhfpt23tcpqx9wrq7w01bp0.soap.marketingcloudapis.com/","rest_instance_url":"https://mc88sqhfpt23tcpqx9wrq7w01bp0.rest.marketingcloudapis.com/"}');
            response.setStatusCode(200);
            response.setStatus('SUCCESS');
        } else if (this.method == '/interaction/v1/events') {
            response.setBody('{"id-journey":"12345678abc"}');
            response.setStatusCode(200);
        }
        return response;
    }
}