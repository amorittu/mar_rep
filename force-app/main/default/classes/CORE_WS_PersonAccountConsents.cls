@RestResource(urlMapping='/core/person-account/consents')
global class CORE_WS_PersonAccountConsents {

    public static Integer limitPerPage = 2000;

    @HttpGet
    global static CORE_WS_Wrapper.ResultWrapperConsents getPAConsents(){

        String urlCallback = '';
        RestRequest	request = RestContext.request;
        String paId = request.params.get(CORE_WS_Constants.PARAM_PA_ID);
        String lastId = request.params.get(CORE_WS_Constants.PARAM_LAST_ID);

        CORE_WS_Wrapper.ResultWrapperConsents result = checkMandatoryParameters(paId);

        if(result != null){
            return result;
        }

        try{
            List<CORE_Consent__c> consentsList = CORE_WS_QueryProvider.getConsentsListByPersonAccountId(paId, lastId, limitPerPage);
            system.debug('@updateOpportunity > consentsList: ' + JSON.serializePretty(consentsList));

            if(consentsList.size() == limitPerPage){
                urlCallback = URL.getOrgDomainUrl()+'/services/apexrest/sis/person-account/consents?'
                        + CORE_WS_Constants.PARAM_PA_ID + '=' + paId
                        + '&' + CORE_WS_Constants.PARAM_LAST_ID + '=' + consentsList.get(consentsList.size() - 1).Id;
            }

            result = new CORE_WS_Wrapper.ResultWrapperConsents(consentsList.size(), urlCallback, consentsList, CORE_WS_Constants.SUCCESS_STATUS);

        } catch(Exception e){
            result = new CORE_WS_Wrapper.ResultWrapperConsents(CORE_WS_Constants.ERROR_STATUS, e.getMessage() + ' at ' + e.getStackTraceString());
        }

        return result;
    }

    private static CORE_WS_Wrapper.ResultWrapperConsents checkMandatoryParameters(String paId){
        List<String> missingParameters = new List<String>();
        Boolean isMissingPersonAccountId = CORE_SIS_GlobalWorker.isParameterMissing(paId);

        if(isMissingPersonAccountId){
            missingParameters.add(CORE_WS_Constants.PARAM_PA_ID);
        }

        return getMissingParameters(missingParameters);
    }

    public static CORE_WS_Wrapper.ResultWrapperConsents getMissingParameters(List<String> missingParameters){
        String errorMsg = CORE_WS_Constants.MSG_MISSING_PARAMETERS;

        if(missingParameters.size() > 0){
            errorMsg += missingParameters;

            return new CORE_WS_Wrapper.ResultWrapperConsents(CORE_WS_Constants.ERROR_STATUS, errorMsg);
        }

        return null;
    }
}