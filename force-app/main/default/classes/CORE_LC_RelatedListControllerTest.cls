@IsTest
public class CORE_LC_RelatedListControllerTest {
    private static String UNIQUE_KEY = 'makeData';
    @TestSetup
    static void makeData(){
        Account account = CORE_DataFaker_Account.getStudentAccount('UNIQUE_KEY');
        Account account2 = CORE_DataFaker_Account.getStudentAccount('makeData1');
        CORE_CatalogHierarchyModel catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('UNIQUE_KEY').catalogHierarchy;

        Opportunity opportunity =  CORE_DataFaker_Opportunity.getOpportunity(
            catalogHierarchy.division.Id, 
            catalogHierarchy.businessUnit.Id, 
            catalogHierarchy.studyArea.Id,
            catalogHierarchy.curriculum.Id,
            catalogHierarchy.level.Id,
            catalogHierarchy.intake.Id,
            account2.Id, 'Lead',
            'Lead - New'
        );
        Task task =  CORE_DataFaker_Task.getMasterTask('makeData', 'CORE_PKL_Completed', account.Id);
        Task task2 =  CORE_DataFaker_Task.getMasterTask('makeData', 'CORE_PKL_Completed', opportunity.Id);
    }

    @IsTest
    public static void fetchRecords_should_return_an_error_if_there_is_missing_parameters() {
        try{
            CORE_LC_RelatedListController.fetchRecords(null,'test', null, null, null, null,false);
        } catch(Exception e){
            System.assertEquals(e.getMessage(),CORE_LC_RelatedListController.MISSING_RECORDID_MSG);
        }

        try{
            CORE_LC_RelatedListController.fetchRecords('test',null, null, null, null, null,false);
        } catch(Exception e){
            System.assertEquals(e.getMessage(),CORE_LC_RelatedListController.MISSING_OBJECTNAME_MSG);
        }
    }

    @IsTest
    public static void fetchRecords_should_order_by_default_by_completedDateTime() {
        String uniqueKey = '%' + UNIQUE_KEY + '%';
        Account account = [SELECT Id FROM Account WHERE PersonEmail like :uniqueKey];

        CORE_LC_RelatedListController.RelatedListWrapper wrapper = CORE_LC_RelatedListController.fetchRecords(account.Id,'Account', null, null, null, null,false);

        System.assertNotEquals(null, wrapper);
        System.assertEquals('1', wrapper.recordCount);
        System.assertEquals(1, wrapper.listRecords.size());
    }

    @IsTest
    public static void fetchRecords_should_order_by_specific_field_if_it_is_specified() {
        String uniqueKey = '%' + UNIQUE_KEY + '%';
        Account account = [SELECT Id FROM Account WHERE PersonEmail like :uniqueKey];
        String sortField = 'Id';
        String sortOrder = 'asc';
        String search;
        Integer maxNumberOfRecord;
        CORE_LC_RelatedListController.RelatedListWrapper wrapper = CORE_LC_RelatedListController.fetchRecords(account.Id,'Account', maxNumberOfRecord, sortField, sortOrder, search,false);

        System.assertNotEquals(null, wrapper);
        System.assertEquals('1', wrapper.recordCount);
        System.assertEquals(1, wrapper.listRecords.size());
    }

    @IsTest
    public static void fetchRecords_should_order_by_specific_field_if_it_is_specified_and_restrict_result_if_search_is_provided() {
        String uniqueKey = '%' + UNIQUE_KEY + '%';
        Account account = [SELECT Id FROM Account WHERE PersonEmail like :uniqueKey];
        Opportunity opportunity = [SELECT Id FROM Opportunity LIMIT 1];
        String sortField = 'Id';
        String sortOrder = 'asc';
        String search;
        Integer maxNumberOfRecord;
        Task task =  CORE_DataFaker_Task.getMasterTask('fetchRecordstest', 'CORE_PKL_Completed', account.Id);
        Task task2 =  CORE_DataFaker_Task.getMasterTask('fetchRecordstest2', 'CORE_PKL_Cancelled', account.Id);

        CORE_LC_RelatedListController.RelatedListWrapper wrapper = CORE_LC_RelatedListController.fetchRecords(account.Id,'Account', maxNumberOfRecord, sortField, sortOrder, search,false);

        System.assertNotEquals(null, wrapper);
        System.assertEquals('3', wrapper.recordCount);
        System.assertEquals(3, wrapper.listRecords.size());
        
        wrapper = CORE_LC_RelatedListController.fetchRecords(account.Id,'Account', maxNumberOfRecord, sortField, sortOrder, search,true);

        System.assertNotEquals(null, wrapper);
        System.assertEquals('2', wrapper.recordCount);
        System.assertEquals(2, wrapper.listRecords.size());

        search = 'fetchRecordstest';
        wrapper = CORE_LC_RelatedListController.fetchRecords(account.Id,'Account', maxNumberOfRecord, sortField, sortOrder, search,false);

        System.assertNotEquals(null, wrapper);
        System.assertEquals('2', wrapper.recordCount);
        System.assertEquals(2, wrapper.listRecords.size());

        wrapper = CORE_LC_RelatedListController.fetchRecords(account.Id,'Account', maxNumberOfRecord, sortField, sortOrder, search,true);

        System.assertNotEquals(null, wrapper);
        System.assertEquals('1', wrapper.recordCount);
        System.assertEquals(1, wrapper.listRecords.size());

        search = 'makeData';
        wrapper = CORE_LC_RelatedListController.fetchRecords(account.Id,'Account', maxNumberOfRecord, sortField, sortOrder, search,false);

        System.assertNotEquals(null, wrapper);
        System.assertEquals('1', wrapper.recordCount);
        System.assertEquals(1, wrapper.listRecords.size());

        search = null;
        maxNumberOfRecord = 1;
        wrapper = CORE_LC_RelatedListController.fetchRecords(account.Id,'Account', maxNumberOfRecord, sortField, sortOrder, search,false);

        System.assertNotEquals(null, wrapper);
        System.assertEquals('1+', wrapper.recordCount);
        System.assertEquals(1, wrapper.listRecords.size());

        maxNumberOfRecord = null;
        wrapper = CORE_LC_RelatedListController.fetchRecords(opportunity.Id,'Opportunity', maxNumberOfRecord, sortField, sortOrder, search,false);

        System.assertNotEquals(null, wrapper);
        System.assertEquals('1', wrapper.recordCount);
        System.assertEquals(1, wrapper.listRecords.size());

    }
}