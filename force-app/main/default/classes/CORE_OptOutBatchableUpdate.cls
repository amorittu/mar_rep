public without sharing class CORE_OptOutBatchableUpdate implements Database.batchable<sObject> {
    public String consentScope; 
    public CORE_OptOutBatchableUpdate(){
        CORE_Consent_Setting__mdt consentMdt;
        consentMdt = [SELECT CORE_Scope__c
                FROM CORE_Consent_Setting__mdt LIMIT 1];
            if(consentMdt != null && !String.isEmpty(consentMdt.CORE_Scope__c)){
                consentScope = consentMdt.CORE_Scope__c;
            }
       // consentScope = CORE_Consent_Setting__mdt.getInstance('CORE_Scope__c');
    }  
    public Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator('SELECT Id,CORE_Person_Account__c,CORE_Scope__c,CORE_Consent_channel__c,CORE_Opt_in__c,CORE_Opt_out__c,CORE_Division__c,CORE_Business_Unit__c,CORE_Brand__c,CORE_Consent_type__c FROM CORE_Consent__c  WHERE (CORE_Consent__c.CreatedDate >= LAST_N_DAYS:1 OR CORE_Consent__c.LastModifiedDate >= LAST_N_DAYS:1) AND  CORE_Consent__c.CORE_Scope__c =\''+ consentScope + '\' AND (CORE_Consent__c.CORE_Consent_type__c = \'CORE_PKL_Authorisation_to_be_contacted_by_channel\' OR CORE_Consent__c.CORE_Consent_type__c = \'CORE_PKL_Global_consent\' OR CORE_Consent__c.CORE_Consent_type__c = \'CORE_PKL_Authorisation_to_be_contacted_by_channel_marketing\')');
        }
        
    public void execute(Database.BatchableContext bc, List<CORE_Consent__c> records){
        System.debug('records '+ records);
        Set<Id> accountIds = new Set<Id>();
        Set<String> scope = new Set<String>();
        Map<CORE_Consent__c,String> mymap = new Map <CORE_Consent__c,String>();
        String oppscope;
        for(CORE_Consent__c consent : records){
                String consentsc;
                accountIds.add(consent.CORE_Person_Account__c);
                switch on consentScope {
                    when 'CORE_PKL_Division' {
                        consentsc = consent.CORE_Division__c;
                        scope.add(consentsc);
                        oppscope = 'CORE_OPP_Division__c';
                     }
                    when 'CORE_PKL_BusinessUnit' {
                        consentsc = consent.CORE_Business_Unit__c;
                        scope.add(consentsc);
                        oppscope = 'CORE_OPP_Business_Unit__c';
                    }
                    when 'CORE_PKL_Brand' {
                        consentsc = consent.CORE_Brand__c;
                        scope.add(consentsc);
                        oppscope = 'CORE_BU_Brand__c';
                    }
                }
                mymap.put(consent,consentsc);
            }
            
        String query= 'SELECT Id,AccountId,CORE_OPP_Business_Unit__c,CORE_BU_Brand__c,CORE_OPP_Division__c,CORE_Opt_out_channels__c,CORE_Opt_out_channels_Marketing__c FROM Opportunity WHERE AccountId IN :accountIds  AND '
        + oppscope + ' IN :scope';
        System.debug(query);
        List<Opportunity> opportunities = Database.query(query);
        System.debug('query passed '+ opportunities);
        Map<String,List<CORE_Consent__c>> consAccAndScope = new Map<String,List<CORE_Consent__c>>();
        System.debug('opp size '+opportunities.size());
        List<Opportunity> oppToUpd = new List<Opportunity>();
        for(CORE_Consent__c cons:records){
            String key = cons.CORE_Person_Account__c + mymap.get(cons);
            if(consAccAndScope.containsKey(key)){ 
                 consAccAndScope.get(key).add(cons);
            }
            else{ 
                List<CORE_Consent__c> newlistcons = new List<CORE_Consent__c>();
                newlistcons.add(cons);
                consAccAndScope.put(key,newlistcons);
            }
        }
        for(Opportunity opp : opportunities){
            String acc = opp.AccountId;
            String sc;
            switch on consentScope {
                when 'CORE_PKL_Division' {
                    sc = opp.CORE_OPP_Division__c;
                 }
                when 'CORE_PKL_BusinessUnit' {
                    sc = opp.CORE_OPP_Business_Unit__c;
                }
                when 'CORE_PKL_Brand' {
                    sc = opp.CORE_BU_Brand__c;
                }
            }
            List<CORE_Consent__c> associatedconsent = new List<CORE_Consent__c>(consAccAndScope.get(acc+sc));
            System.debug('assoc cons ' + associatedconsent);
             for(CORE_Consent__c consent:associatedconsent){
                if(consent.CORE_Opt_out__c == true){
                    if (consent.CORE_Consent_type__c == 'CORE_PKL_Authorisation_to_be_contacted_by_channel' || consent.CORE_Consent_type__c == 'CORE_PKL_Global_consent'){
                        opp.CORE_Opt_out_channels__c = String.isBlank(opp.CORE_Opt_out_channels__c) ? consent.CORE_Consent_channel__c : opp.CORE_Opt_out_channels__c + ';' +consent.CORE_Consent_channel__c; }
                    if (consent.CORE_Consent_type__c == 'CORE_PKL_Authorisation_to_be_contacted_by_channel_marketing' || consent.CORE_Consent_type__c == 'CORE_PKL_Global_consent'){    
                        opp.CORE_Opt_out_channels_Marketing__c = String.isBlank(opp.CORE_Opt_out_channels_Marketing__c) ? consent.CORE_Consent_channel__c : opp.CORE_Opt_out_channels_Marketing__c + ';' +consent.CORE_Consent_channel__c;
                    }
                }
                else {
                    if(opp.CORE_Opt_out_channels__c !=null){
                        if (consent.CORE_Consent_type__c == 'CORE_PKL_Authorisation_to_be_contacted_by_channel' || consent.CORE_Consent_type__c == 'CORE_PKL_Global_consent'){
                            if(opp.CORE_Opt_out_channels__c.Contains(consent.CORE_Consent_channel__c)){
                                opp.CORE_Opt_out_channels__c = opp.CORE_Opt_out_channels__c.replace(consent.CORE_Consent_channel__c, '');
                            }
                        }
                        if (consent.CORE_Consent_type__c == 'CORE_PKL_Authorisation_to_be_contacted_by_channel_marketing' || consent.CORE_Consent_type__c == 'CORE_PKL_Global_consent'){    
                            if(opp.CORE_Opt_out_channels_Marketing__c.Contains(consent.CORE_Consent_channel__c)) {  
                                opp.CORE_Opt_out_channels_Marketing__c = opp.CORE_Opt_out_channels_Marketing__c.replace(consent.CORE_Consent_channel__c, '');
                            }
                        }
                    }    
                }
            }  
            oppToUpd.add(opp);
        }

    
           
        update oppToUpd ;
    }
            
    

    public void finish(Database.BatchableContext bc){
        // execute any post-processing operations
    }


}