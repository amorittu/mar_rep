@isTest
public class CORE_Flow_GetPicklistLabelTest {
	@isTest
    public static void testGetPicklistLabel(){
        CORE_Flow_GetPicklistLabel.PicklistParams pwrapper = new CORE_Flow_GetPicklistLabel.PicklistParams();
        List<Schema.PicklistEntry> entries = Opportunity.stageName.getDescribe().getPicklistValues();
        Test.startTest();
        String picklistLabel = entries[0].getLabel();
        String apiNamePicklist = entries[0].getValue();
        pwrapper.fieldApiName ='stageName';
        pwrapper.fieldValue = apiNamePicklist;
        pwrapper.objApiName ='Opportunity';
        List<String> sts = CORE_Flow_GetPicklistLabel.getPicklistLabel(new List<CORE_Flow_GetPicklistLabel.PicklistParams>{pwrapper});
        Test.stopTest();
        System.assertEquals(picklistLabel,sts[0]);
    }
}