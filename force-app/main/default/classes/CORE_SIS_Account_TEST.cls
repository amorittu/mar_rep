@isTest
private class CORE_SIS_Account_TEST {

    private static final String INTEREST_EXTERNALID = 'setup';
    private static final String ACADEMIC_YEAR = 'CORE_PKL_2023-2024';

    @testSetup 
    static void createData(){
        Id profileId = [SELECT Id FROM Profile WHERE Name = 'CORE_API_Profile' LIMIT 1].Id;

        User usr = new User(
            Username = 'usertest@aoltest.com',
            LastName = 'TEST_USER',
            Email = 'testemail@yopmail.com',
            Alias = 'T', 
            CommunityNickname = 'T',
            TimeZoneSidKey = 'Europe/Paris',
            LocaleSidKey = 'fr_FR_EURO',
            EmailEncodingKey = 'ISO-8859-1', 
            ProfileId = profileId,
            LanguageLocaleKey = 'fr'      
        );
        database.insert(usr, true);

        Account account = CORE_DataFaker_Account.getStudentAccount(INTEREST_EXTERNALID);

    }

    @isTest
    static void test_HasOpps(){
        User usr = [SELECT Id FROM User WHERE Username = 'usertest@aoltest.com' LIMIT 1];

        List<PermissionSetAssignment> psaList = new List<PermissionSetAssignment>();

        for(PermissionSet ps : [SELECT Id FROM PermissionSet WHERE Name = 'CORE_API_SIS' OR Name = 'CORE_LeadFormAcquisition']){
            psaList.add(
                new PermissionSetAssignment(AssigneeId = usr.id, PermissionSetId = ps.Id)
            );
        }
        
        database.insert(psaList, true);

        System.runAs(usr){

            Account acc = [SELECT Id FROM Account LIMIT 1];

            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
            req.requestURI = '/services/apexrest/sis/account';

            req.httpMethod = 'GET';
            req.params.put(CORE_SIS_Constants.PARAM_SF_ID, acc.Id);

            RestContext.request = req;
            RestContext.response= res;

            test.startTest();
            CORE_SIS_Wrapper.ResultWrapperAccount results = CORE_SIS_Account.accountList();
            test.stopTest();

            System.assertEquals(acc.Id, results.account.Id);
        }
    }

    @isTest
    static void test_MissingParams(){
        User usr = [SELECT Id FROM User WHERE Username = 'usertest@aoltest.com' LIMIT 1];

        List<PermissionSetAssignment> psaList = new List<PermissionSetAssignment>();

        for(PermissionSet ps : [SELECT Id FROM PermissionSet WHERE Name = 'CORE_API_SIS' OR Name = 'CORE_LeadFormAcquisition']){
            psaList.add(
                new PermissionSetAssignment(AssigneeId = usr.id, PermissionSetId = ps.Id)
            );
        }
        
        database.insert(psaList, true);

        System.runAs(usr){

            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
            req.requestURI = '/services/apexrest/sis/account';

            req.httpMethod = 'GET';

            RestContext.request = req;
            RestContext.response= res;

            test.startTest();
            CORE_SIS_Wrapper.ResultWrapperAccount results = CORE_SIS_Account.accountList();
            test.stopTest();

            System.assertEquals(CORE_SIS_Constants.ERROR_STATUS, results.status);
        }
    }
}