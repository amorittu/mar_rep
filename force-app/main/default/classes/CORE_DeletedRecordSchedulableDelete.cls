public without sharing class CORE_DeletedRecordSchedulableDelete implements Schedulable {
    public void execute(SchedulableContext sC) {
        CORE_DeletedRecordBatchableDelete deletedRecordBatchableDelete = new CORE_DeletedRecordBatchableDelete();
        database.executebatch(deletedRecordBatchableDelete);
     }
}