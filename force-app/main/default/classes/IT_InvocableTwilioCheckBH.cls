public class IT_InvocableTwilioCheckBH {
    
    @InvocableMethod
    public static List<FlowOutputs> checkIncomingDate(List<FlowInputs> request){
        // Get the default business hours
        
        Boolean isOpen = BusinessHours.isWithin(request[0].incomingBhId, request[0].targetDate);
        FlowOutputs flOutput= new FlowOutputs();
        flOutput.isOpen = isOpen;

        List<FlowOutputs> returnList = new List<FlowOutputs>();
        returnList.add(flOutput);

        return returnList;

    }

    public class FlowInputs{

        @InvocableVariable (required=true)
        public String incomingBhId;

        @InvocableVariable (required=true)
        public DateTime targetDate;
    }

    public class FlowOutputs{

        @InvocableVariable
        public Boolean isOpen;

    }

}