/**
 * Created by szocchi on 2022-03-22.
 *
 * Test class: IT_BatchExtendedInfoOppTest
 */

public with sharing class IT_BatchExtendedInfoOpportunityLastMod implements Database.Batchable<SObject>{
    Boolean runNext = false;

    public IT_UtilSetting__c utilSetting = IT_UtilSetting__c.getInstance();
    public String[] accountIds = new String[]{};
    public IT_BatchExtendedInfoOpportunityLastMod() {
    }
    public IT_BatchExtendedInfoOpportunityLastMod(Boolean runNextBatch, String[] accountIds) {
        this.accountIds = accountIds;
        this.runNext = runNextBatch;
    }

    public IT_BatchExtendedInfoOpportunityLastMod(Boolean runNextBatch) {
        this.runNext = runNextBatch;
    }
    public Database.QueryLocator start(Database.BatchableContext bc) {

        Integer daysSystemMode = utilSetting.IT_DaysSystemModeBatch__c != null ? Integer.valueOf(utilSetting.IT_DaysSystemModeBatch__c) : 365;

        if ( ! accountIds.isEmpty() ) {
            return Database.getQueryLocator(
                'SELECT Id, OwnerId,AccountId, CORE_OPP_Business_Unit__c, CORE_Main_Product_Interest__c, CORE_OPP_Agent_Perception__c, ' +
                        'CORE_OPP_Intake__c, CORE_OPP_Business_Unit__r.CORE_Brand__c, IT_OpportunityAssigned__c, IT_InformationOfficer__c, ' +
                        'IT_AdmissionOfficer__c, CORE_OPP_Study_Area__c, CORE_OPP_Level__c, CORE_Origin__c, CORE_Nature__c, CORE_Capture_Channel__c, ' +
                        'StageName, CORE_OPP_Sub_Status__c, IT_LastAPOCOrigin__c ' +
                        'FROM Opportunity ' +
                        'WHERE AccountId IN: accountIds ' + 
                        'ORDER BY AccountId, LastModifiedDate ASC'
            );
        }

        return Database.getQueryLocator(
                'SELECT Id, OwnerId,AccountId, CORE_OPP_Business_Unit__c, CORE_Main_Product_Interest__c, CORE_OPP_Agent_Perception__c, ' +
                        'CORE_OPP_Intake__c, CORE_OPP_Business_Unit__r.CORE_Brand__c, IT_OpportunityAssigned__c, IT_InformationOfficer__c, ' +
                        'IT_AdmissionOfficer__c, CORE_OPP_Study_Area__c, CORE_OPP_Level__c, CORE_Origin__c, CORE_Nature__c, CORE_Capture_Channel__c, ' +
                        'StageName, CORE_OPP_Sub_Status__c, IT_LastAPOCOrigin__c ' +
                        'FROM Opportunity ' +
                        'WHERE SystemModstamp = LAST_N_DAYS:' + daysSystemMode + ' ' +
                        'ORDER BY AccountId, LastModifiedDate ASC'
        );
    }
    public void execute(Database.BatchableContext bc, List<Opportunity> records) {
        System.debug('records.size(): ' + records.size());
        IT_OpportunityTriggerHandler.assignExtendedInfo(records, null, true);
    }
    public void finish(Database.BatchableContext bc) {
        if(!Test.isRunningTest() && this.runNext){
            if ( accountIds.isEmpty() ) {
                Database.executeBatch(new IT_BatchExtendedInfoTaskCreated(true), 1000);
            } else {
                Database.executeBatch(new IT_BatchExtendedInfoTaskCreated(true, accountIds), 1000);
            }
        }
    }
}