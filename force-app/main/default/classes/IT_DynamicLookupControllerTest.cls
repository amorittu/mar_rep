@isTest
public class IT_DynamicLookupControllerTest {
    @TestSetup
    public static void makeData(){
        Account a = new Account();
        a.FirstName = 'AtlFakeAccount';
        a.LastName = 'testing';
        a.PersonEmail = 'test@test.com';
        a.CORE_Language__pc = 'Ita';
        insert a;

        Task t = new Task();
        t.Subject = 'SpamTask';
        t.WhatId= a.Id;    
        //t.AccountId = a.Id;
        t.Status = 'CORE_PKL_Open';
        insert t;
    }

    @isTest 
    public static void testMethodWithoutSharing() {
        
        Test.startTest();
            IT_DynamicLookupController.fetchLookupData('AtlFakeAcco' , 'Account', false);
        Test.stopTest();
    }

    @isTest 
    public static void testMethodWithSharing() {
        
        Test.startTest();
            IT_DynamicLookupController.fetchLookupData('AtlFakeAcco' , 'Account', true);
        Test.stopTest();
    }

    @isTest 
    public static void testFetchDefaultRecordWithSharing() {
        Account[] acc = [SELECT Id FROM Account LIMIT 1];
        Test.startTest();
            IT_DynamicLookupController.fetchDefaultRecord(acc[0].Id , 'Account', false);
        Test.stopTest();
    }

    @isTest 
    public static void testFetchDefaultRecordWithoutSharing() {
        Account[] acc = [SELECT Id FROM Account LIMIT 1];
        Test.startTest();
            IT_DynamicLookupController.fetchDefaultRecord(acc[0].Id , 'Account', true);
        Test.stopTest();
    }
}