@RestResource(urlMapping='/sales-and-admission-incoming-email')
global class CORE_SalesAdmissionInboundEmail  {

	private static final String PARAM_INBOUNDEMAIL = 'inboundEmail';
	private static final String SUCCESS_STATUS = 'OK';
	private static final String ERROR_STATUS = 'KO';
	private static final String MISSING_PARAMS = 'Missing mandatory parameter : ';

	@HttpPost
    global static CORE_SalesAdmissionInboundEmail_Wrapper.ResultWrapper inboundEmail(){

        RestRequest	request = RestContext.request;
		CORE_SalesAdmissionInboundEmail_Wrapper.ResultWrapper result = null;

        try{

            //Map<String, Object> paramsMap = (Map<String, Object>)JSON.deserializeUntyped(request.requestBody.toString());

            CORE_SalesAdmissionInboundEmail_Wrapper.EmailWrapper emailInbound = (CORE_SalesAdmissionInboundEmail_Wrapper.EmailWrapper)JSON.deserialize(
				request.requestBody.toString(), CORE_SalesAdmissionInboundEmail_Wrapper.EmailWrapper.class
			);

			system.debug('@inboundEmail: ' + JSON.serializePretty(emailInbound)); 

            result = checkMandatoryParameters(emailInbound);

			if(result != null){
                return result;
            }

			Messaging.InboundEmail.Header[] headersList = new Messaging.InboundEmail.Header[4];

			if(emailInbound.headers != null){
				for(Integer i = 0; i < emailInbound.headers.size(); i++){
					Messaging.InboundEmail.Header newHeader = new Messaging.InboundEmail.Header();

					if(i == 0){
						newHeader.name = 'Received from';
					}
					else if(i == 1){
						newHeader.name = 'Custom headers';
					}
					else if(i == 2){
						newHeader.name = 'Message-ID';
					}
					else if(i == 3){
						newHeader.name = 'Date';
					}

					newHeader.value = emailInbound.headers[i];
					headersList.add(newHeader);
				}
			}

			Messaging.inboundEmail email = new Messaging.inboundEmail();
			email.fromName = emailInbound.fromName;
			email.fromAddress = emailInbound.fromEmail;
			email.toAddresses = emailInbound.toEmail;
			email.ccAddresses = emailInbound.ccEmails;
			email.subject = emailInbound.subject;
			email.htmlBody = emailInbound.htmlBody;
			email.plainTextBody = emailInbound.plainTextBody;
			email.headers = (headersList.size() > 0) ? headersList :null;
			email.messageId = emailInbound.messageId;
			email.references = emailInbound.inReplyTo;

			String attachments = '';
			Datetime emailDateTime = (emailInbound.emailDatetime != null) ? Datetime.valueOf(emailInbound.emailDatetime.replace('T', ' ')) :null;

			if(emailInbound.attachments != null){
				for(String attch : emailInbound.attachments){
					attachments += attch + '<br/>';
				}
			}

            CORE_InboundEmailHandler inboundEmailHandler = new CORE_InboundEmailHandler(attachments, emailDateTime);
			inboundEmailHandler.handleInboundEmail(email, null);

			result = new CORE_SalesAdmissionInboundEmail_Wrapper.ResultWrapper(SUCCESS_STATUS, '');

		} catch(Exception e){
            system.debug('@inboundEmail > exception error: ' + e.getMessage() + ' at ' + e.getStackTraceString());
            result = new CORE_SalesAdmissionInboundEmail_Wrapper.ResultWrapper(ERROR_STATUS, e.getMessage() + ' at ' + e.getStackTraceString());
        }

        return result;
	}

	private static CORE_SalesAdmissionInboundEmail_Wrapper.ResultWrapper checkMandatoryParameters(CORE_SalesAdmissionInboundEmail_Wrapper.EmailWrapper emailInbound){
        String errMsg = '';
		Set<String> missingParamsSet = new Set<String>();

		if(emailInbound.fromEmail == null){
			missingParamsSet.add('fromEmail');
		}

		if(emailInbound.toEmail == null){
			missingParamsSet.add('toEmail');
		}

		if(emailInbound.htmlBody == null){
			missingParamsSet.add('htmlBody');
		}

		if(missingParamsSet.size() > 0){
			for(String param : missingParamsSet){
				errMsg += param + ';';
			}

			return new CORE_SalesAdmissionInboundEmail_Wrapper.ResultWrapper(ERROR_STATUS, MISSING_PARAMS + errMsg);
		}

		return null;
    }
}