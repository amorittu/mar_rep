@IsTest
public class CORE_ObjectWrapperTest {

    private class testObj extends CORE_ObjectWrapper {
        public Integer integerValue;
        public String stringValue;
        public Datetime datetimeValue;
        public Boolean booleanValue;
    }

    @IsTest
    public static void attribute_values_of_object_extanded_of_CORE_ObjectWrapper_should_be_retrievable_using_get_method() {
        testObj obj = new testObj();
        obj.integerValue = 1;
        obj.stringValue = 'stringTest';
        obj.datetimeValue = System.now();
        obj.booleanValue = true;

        try{
            System.AssertEquals(obj.integerValue, (Integer) obj.get('integerValue'));
            System.AssertEquals(obj.stringValue, (String) obj.get('stringValue'));
            System.AssertEquals(obj.booleanValue, (Boolean) obj.get('booleanValue'));

            String strDatetime = (String) obj.get('datetimeValue');
            strDatetime = strDatetime.replace('T', ' ').replace(strDatetime.substring(strDatetime.indexOf('.')), '');
            Datetime resultDateTime = Datetime.valueOfGmt(strDatetime);
            System.AssertEquals(obj.datetimeValue, resultDateTime);
        }catch(Exception e){
            System.debug(e.getStackTraceString());
            System.assert(false, 'get method should be usable');
        }
    }
}