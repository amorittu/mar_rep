@IsTest
public with sharing class CORE_DataFaker_AOL_Wrapper {
    private static final String PKL_DOCUMENT_PROGRESSION = 'Started';
    private static final String PKL_SALUTATION = 'Mr.';

    public static CORE_AOL_Wrapper.AOLProgressionFields getAolProgressionFieldsWrapper(){
        CORE_AOL_Wrapper.AOLProgressionFields aolProgressionFields= new CORE_AOL_Wrapper.AOLProgressionFields();
        aolProgressionFields.aolProgression = 50.54;
        aolProgressionFields.documentProgression = PKL_DOCUMENT_PROGRESSION;
        aolProgressionFields.firstDocumentUploadDate = System.today();
        aolProgressionFields.lastAolEventDate = System.now();
        aolProgressionFields.lastDeaAolLoginDate = System.now();
        aolProgressionFields.isAol = true;
        aolProgressionFields.isAolOnlineRegistered = true;
        aolProgressionFields.isAolRetailRegistered = true;

        return aolProgressionFields;
    }

    public static CORE_AOL_Wrapper.AccountWrapper getAccountWrapper(){
		CORE_AOL_Wrapper.AccountWrapper accountWrapper = new CORE_AOL_Wrapper.AccountWrapper();
        accountWrapper.salutation = PKL_SALUTATION;
		accountWrapper.firstname = 'firstWrapper';
		accountWrapper.lastname = 'lastWrapper';
		accountWrapper.birthdate = Date.newInstance(2022, 2, 1);
        accountWrapper.mainNationality = 'FRA';
        accountWrapper.personMailingStreet = 'streetWrapper';
        accountWrapper.personMailingPostalCode = '75010';
        accountWrapper.personMailingCity = 'Paris';
        accountWrapper.personMailingState = null;
        accountWrapper.personMailingCountry = 'FR';
        accountWrapper.updateIfNotNull = false;

        accountWrapper.cityOfBirth = 'Paris';
        //accountWrapper.countryOfBirth = 'FRA';
        accountWrapper.deptOfBirth = 'test';
        accountWrapper.gender = 'CORE_PKL_Male';
        accountWrapper.billingPermanentRegion = 'testRegion';
        accountWrapper.billingStreet = 'streetWrapper';
        accountWrapper.billingCity = 'Paris';
        accountWrapper.billingPostalCode = '75010';
        accountWrapper.billingState = null;
        accountWrapper.billingCountry = 'FR';
        accountWrapper.personMailingCurrentRegion = 'testMailingRegion';
        accountWrapper.socialSecNumberFiscalCode = '12345';

        return accountWrapper;
	}

    
    public static CORE_AOL_Wrapper.ForeignLanguages getAccountLanguagesWrapper(){
		CORE_AOL_Wrapper.ForeignLanguages foreignLanguages = new CORE_AOL_Wrapper.ForeignLanguages();
        foreignLanguages.language1 = 'FRA';
        foreignLanguages.language1Level ='B1-Intermediate|Medium';
        foreignLanguages.language2 ='DEU';
        foreignLanguages.language2Level ='A1-Elementary|Low';

        return foreignLanguages;
	}

    public static CORE_AOL_Wrapper.DocumentWrapper getDocumentWrapper(Integer iteration) {
        CORE_AOL_Wrapper.DocumentWrapper documentWrapper = new CORE_AOL_Wrapper.DocumentWrapper();
        documentWrapper.externalId = 'testExtId' + iteration;
		documentWrapper.name = 'testName' + iteration;
		documentWrapper.externalUrl = 'testExtUrl' + iteration;
		documentWrapper.type = 'Visa';
        documentWrapper.createdDate = System.today();
        documentWrapper.approvedDate = System.today();
        documentWrapper.rejectedDate = System.today();

        return documentWrapper;
    } 

    public static CORE_AOL_Wrapper.SelectionTest getProcessSelectionWrapper() {
        CORE_AOL_Wrapper.SelectionTest selectionTest = new CORE_AOL_Wrapper.SelectionTest();
        selectionTest.selectionTestTeacher = UserInfo.getUserId();
		selectionTest.selectionTestResult = 'CORE_PKL_Failed';
		selectionTest.selectionProcessStatus = 'CORE_PKL_Booked';
        selectionTest.selectionStatus = 'CORE_PKL_Booked';
		selectionTest.selectionTestDate = System.today();
        selectionTest.selectionProcessEndDate = System.today();
        selectionTest.selectionProcessStartDate = System.today();
        selectionTest.skillTestStartDate = System.today();
        selectionTest.selectionTestShowNoShow =  'CORE_PKL_Cancelled';
        selectionTest.selectionTestNoShowCounter = 5;
        selectionTest.selectionTestGrade = '12,50';
        selectionTest.selectionProcessProgramLeader = 'Leader';
        selectionTest.selectionProcessGrade = '12';
        selectionTest.selectionProcessResult = 'CORE_Opp_Failed';
        selectionTest.selectionProcessNeeded = true;
        selectionTest.selectionTestNeeded = true ;

        return selectionTest;
    } 

    public static CORE_AOL_Wrapper.DocumentSendingFields getProcessDocumentWrapper() {
        CORE_AOL_Wrapper.DocumentSendingFields documentSendingFields = new CORE_AOL_Wrapper.DocumentSendingFields();
        documentSendingFields.documentSendingChoice = 'Email';
        return documentSendingFields;
    } 

    public static CORE_AOL_Wrapper.ApplicationFeesPaid getApplicationFeesPaidWrapper() {
        CORE_AOL_Wrapper.ApplicationFeesPaid applicationFeesPaid = new CORE_AOL_Wrapper.ApplicationFeesPaid();
        applicationFeesPaid.paymentMethod = 'CORE_PKL_Cash';
        applicationFeesPaid.transactionId = 'myTransactionId';
        applicationFeesPaid.receivedDate = System.today();
        applicationFeesPaid.acceptanceDate = System.today();
        applicationFeesPaid.received = true;
        applicationFeesPaid.bank = 'myBank';
        applicationFeesPaid.sender = 'mySender';
        applicationFeesPaid.amount = 10.50;
        return applicationFeesPaid;
    } 

    public static CORE_AOL_Wrapper.TuitionFeesPaid getTuitionFeesPaidWrapper() {
        CORE_AOL_Wrapper.TuitionFeesPaid tuitionFeesPaid = new CORE_AOL_Wrapper.TuitionFeesPaid();
        tuitionFeesPaid.paymentMethod = 'CORE_PKL_Cash';
        tuitionFeesPaid.paymentType = 'CORE_PKL_Partially_Paid';
        tuitionFeesPaid.transactionId = 'myTransactionId';
        tuitionFeesPaid.receivedDate = System.today();
        tuitionFeesPaid.acceptanceDate = System.today();
        tuitionFeesPaid.received = true;
        tuitionFeesPaid.bank = 'myBank';
        tuitionFeesPaid.sender = 'mySender';
        tuitionFeesPaid.amount = 10.50;
        return tuitionFeesPaid;
    } 

    public static CORE_AOL_Wrapper.enrollmentFeesPaid getEnrollmentFeesPaidWrapper() {
        CORE_AOL_Wrapper.enrollmentFeesPaid enrollmentFeesPaid = new CORE_AOL_Wrapper.enrollmentFeesPaid();
        enrollmentFeesPaid.paymentMethod = 'CORE_PKL_Cash';
        enrollmentFeesPaid.receivedDate = System.today();
        enrollmentFeesPaid.acceptanceDate = System.today();
        enrollmentFeesPaid.received = true;
        return enrollmentFeesPaid;
    } 

    public static CORE_AOL_Wrapper.InterestWrapper geInterestWrapper(CORE_CatalogHierarchyModel catalogHierarchy, Boolean isMain, Id personAccountId) {
        CORE_AOL_Wrapper.InterestWrapper interest = new CORE_AOL_Wrapper.InterestWrapper();
        interest.businessUnit = catalogHierarchy.businessUnit.CORE_Business_Unit_ExternalId__c;
		interest.studyArea = catalogHierarchy.studyArea?.CORE_Study_Area_ExternalId__c;
		interest.curriculum = catalogHierarchy.curriculum?.CORE_Curriculum_ExternalId__c;
		interest.level = catalogHierarchy.level?.CORE_Level_ExternalId__c;
        interest.intake = catalogHierarchy.intake?.CORE_Intake_ExternalId__c;  
        interest.isMain = isMain;
        interest.isAolRetailRegistered = true;
        interest.personAccountId = personAccountId;

        return interest;
    } 

    public static CORE_AOL_Wrapper.AcademicDiplomaHistory getAcademicDiplomaHistoryWrapper(Integer iteration) {
        CORE_AOL_Wrapper.AcademicDiplomaHistory academicDiplomaHistory = new CORE_AOL_Wrapper.AcademicDiplomaHistory();
        academicDiplomaHistory.diplomaExternalId = 'testExtId' + iteration;
		academicDiplomaHistory.diplomaName = 'testName' + iteration;
		academicDiplomaHistory.diplomaLevel = 'CORE_PKL_Apprenticeship';
		academicDiplomaHistory.diplomaSchool = 'School';
        academicDiplomaHistory.diplomaCity = 'Paris';
        academicDiplomaHistory.diplomaCountry = 'FRA';
        academicDiplomaHistory.diplomaStatus = 'CORE_PKL_In_Progress';
        academicDiplomaHistory.diplomaExpectedGraduationDate = System.today();
        academicDiplomaHistory.diplomaModificationDate = System.today();
        academicDiplomaHistory.diplomaDate = System.today();
        
        return academicDiplomaHistory;
    } 
}