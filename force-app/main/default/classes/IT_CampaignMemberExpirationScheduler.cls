public without sharing class IT_CampaignMemberExpirationScheduler implements Schedulable {

    public void execute(SchedulableContext sc) {
        IT_CampaignMemberExpirationChecker batch = new IT_CampaignMemberExpirationChecker(); //ur batch class
        Database.executebatch(batch, 10);
    }
}