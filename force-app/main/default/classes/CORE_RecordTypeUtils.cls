public without sharing class CORE_RecordTypeUtils {
    public static Map<Id, String> getDeveloperNameByIds (String objectName, Set<Id> recordTypeIds) {
        Map<Id, String> developerNameByIds = new Map<Id, String>();

        Schema.SObjectType sobjectType = Schema.getGlobalDescribe().get(objectName) ;
        Schema.DescribeSObjectResult sobjectResult = sobjectType.getDescribe() ;

        for(Id recordTypeId : recordTypeIds){
            if(recordTypeId != null){
                String developerName = sobjectResult.getRecordTypeInfosById().get(recordTypeId).getDeveloperName();
                developerNameByIds.put(recordTypeId,developerName);
            }
        }

        return developerNameByIds;
    }
}