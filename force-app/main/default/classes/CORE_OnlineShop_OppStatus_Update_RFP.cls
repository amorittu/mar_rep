@RestResource(urlMapping='/online-shop/opportunity/status/registered/registration-fee-fully-paid')
global class CORE_OnlineShop_OppStatus_Update_RFP {

    @HttpPut
    global static CORE_OnlineShop_Wrapper.ResultWrapperOpportunity updateOpportunityStatus(){

        RestRequest	request = RestContext.request;
        CORE_OnlineShop_Wrapper.ResultWrapperOpportunity result = null;

        try{

            Map<String, Object> paramsMap = (Map<String, Object>)JSON.deserializeUntyped(request.requestBody.toString());
            //system.debug('@updateOpportunity > paramsMap: ' + JSON.serializePretty(paramsMap));

            String onlineShopId = (String)paramsMap.get(CORE_OnlineShop_Constants.PARAM_ONLINESHOP_ID);
            system.debug('@updateOpportunityStatus > onlineShopId: ' + onlineShopId);

            result = checkMandatoryParameters(onlineShopId);

            if(result != null){
                return result;
            }

            Opportunity opportunity = CORE_OnlineShop_QueryProvider.getOpportunity(onlineShopId);
            //system.debug('@updateOpportunity > oppsList GET: ' + JSON.serializePretty(oppsList[0]));

            opportunity.StageName = CORE_OnlineShop_Constants.PKL_OPP_STAGENAME_REGISTERED;
            opportunity.CORE_OPP_Sub_Status__c = CORE_OnlineShop_Constants.PKL_OPP_SUBSTATUS_REGFEESFULLYPAID;

            database.update(opportunity, true);
            //system.debug('@updateOpportunity > oppsList after update: ' + JSON.serializePretty(oppsList[0]));

            result = new CORE_OnlineShop_Wrapper.ResultWrapperOpportunity(CORE_OnlineShop_Constants.SUCCESS_STATUS, CORE_OnlineShop_Constants.MSG_SUCCESSFUL);

        } catch(Exception e){
            result = new CORE_OnlineShop_Wrapper.ResultWrapperOpportunity(CORE_OnlineShop_Constants.ERROR_STATUS, e.getMessage() + ' at ' + e.getStackTraceString());
        }

        return result;
    }

    private static CORE_OnlineShop_Wrapper.ResultWrapperOpportunity checkMandatoryParameters(String onlineShopId){
        List<String> missingParameters = new List<String>();
        Boolean isMissingId = CORE_SIS_GlobalWorker.isParameterMissing(onlineShopId);

        if(isMissingId){
            missingParameters.add(CORE_OnlineShop_Constants.PARAM_ONLINESHOP_ID);
        }
        
        return getMissingParameters(missingParameters);
    }

    public static CORE_OnlineShop_Wrapper.ResultWrapperOpportunity getMissingParameters(List<String> missingParameters){
        String errorMsg = CORE_OnlineShop_Constants.MSG_MISSING_PARAMETERS;

        if(missingParameters.size() > 0){
            errorMsg += missingParameters;

            return new CORE_OnlineShop_Wrapper.ResultWrapperOpportunity(CORE_OnlineShop_Constants.ERROR_STATUS, errorMsg);
        }
        
        return null;
    }
}