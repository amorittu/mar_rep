/**
 * Created by ftrapani on 20/10/2021.
 * Test Class: IT_UtilityTest
 */

public without sharing class IT_Utility {

    public class MyPickListInfo
    {
        public String validFor;
    }

    //return picklist api / label values
    @testVisible
    public static Map<String,String> getPicklistLabel(String objectApi, String fieldApi) {
        Map<String,String> returnValue = new Map<String,String>();
        Schema.SObjectType objType = Schema.getGlobalDescribe().get(objectApi);
        Schema.DescribeSObjectResult describeResult = objType.getDescribe();
        Schema.DescribeFieldResult field = describeResult.fields.getMap().get(fieldApi).getDescribe();

        for (Schema.PicklistEntry f : field.getPicklistValues()){
            returnValue.put(f.getValue(),f.getLabel());
        }
        return returnValue;
    }
	
	//return Role Type records from Custom Metadata										   
	@testVisible
    public static Map<String,IT_Role_Type__mdt> getRoleTypeMDT2(){
        List<IT_Role_Type__mdt> roleTypeList = [SELECT Id, IT_Type__c, IT_RoleDeveloperName__c, IT_BrandMarangoni__c, IT_BrandNABA__c, IT_BrandDOMUS__c FROM IT_Role_Type__mdt];
        Map<String,IT_Role_Type__mdt> typeRoleMap = new Map<String,IT_Role_Type__mdt>();
        for(IT_Role_Type__mdt roleType : roleTypeList){
            typeRoleMap.put(roleType.IT_RoleDeveloperName__c,roleType);
        }
        return typeRoleMap;
    }

    //return default businesshours
    /*
    @testVisible
    public static BusinessHours getDefaultBusinessHour(){
        BusinessHours bh = [SELECT Id FROM BusinessHours WHERE IsDefault=true];
        return bh;
    }
     */

    //return all Assign Owner By Capture Channel records from Custom Metadata
    @testVisible
    public static Map<String,String> getCapturechannel4AssignOwner(){
        List<IT_AssignOwnerByCaptureChannel__mdt> captureChannelList = [SELECT Id, MasterLabel, DeveloperName FROM IT_AssignOwnerByCaptureChannel__mdt];
        Map<String,String> captureChannelMap = new Map<String,String>();
        for(IT_AssignOwnerByCaptureChannel__mdt captureChannel : captureChannelList){
            captureChannelMap.put(captureChannel.DeveloperName,captureChannel.DeveloperName);
        }
        return captureChannelMap;
    }

    //return all Sender Email records from Custom Metadata
    @testVisible
    public static Map<String,String> getSenderEmail(){
        List<IT_SenderEmail__mdt> senderEmailList = [SELECT Id, IT_EmailAddress__c, DeveloperName FROM IT_SenderEmail__mdt];
        Map<String,String> senderEmailMap = new Map<String,String>();
        for(IT_SenderEmail__mdt senderEmail : senderEmailList){
            senderEmailMap.put(senderEmail.DeveloperName,senderEmail.IT_EmailAddress__c);
        }
        return senderEmailMap;
    }

    //return all Opportunity Order Status records from Custom Metadata
    @testVisible
    public static Map<String,Decimal> getOptyOrderStatus(){
        List<IT_OpportunityStatusOrder__mdt> orderStatusList = [SELECT Label, IT_OrderNumber__c FROM IT_OpportunityStatusOrder__mdt ORDER BY IT_OrderNumber__c];
        Map<String,Decimal> orderStatusMap = new Map<String,Decimal>();
        for(IT_OpportunityStatusOrder__mdt orderStatus : orderStatusList){
            orderStatusMap.put(orderStatus.Label,orderStatus.IT_OrderNumber__c);
        }
        return orderStatusMap;
    }

    //return all Opportunity Order Status records from Custom Metadata
    @testVisible
    public static Map<Decimal,String> getOptyStatusOrder(){
        List<IT_OpportunityStatusOrder__mdt> orderStatusList = [SELECT Label, IT_OrderNumber__c FROM IT_OpportunityStatusOrder__mdt ORDER BY IT_OrderNumber__c];
        Map<Decimal,String> statusByOrderMap = new Map<Decimal,String>();
        for(IT_OpportunityStatusOrder__mdt orderStatus : orderStatusList){
            statusByOrderMap.put(orderStatus.IT_OrderNumber__c,orderStatus.Label);
        }
        return statusByOrderMap;
    }

    //return all Opportunity Order Sub Status records from Custom Metadata
    @testVisible
    public static Map<String,Decimal> getOptyOrderSubStatus(){
        List<IT_OpportunitySubStatusOrder__mdt> orderSubStatusList = [SELECT IT_SubStatus__c, IT_OrderNumber__c FROM IT_OpportunitySubStatusOrder__mdt];
        Map<String,Decimal> orderSubStatusMap = new Map<String,Decimal>();
        for(IT_OpportunitySubStatusOrder__mdt orderStatus : orderSubStatusList){
            orderSubStatusMap.put(orderStatus.IT_SubStatus__c,orderStatus.IT_OrderNumber__c);
        }
        return orderSubStatusMap;
    }

    @TestVisible
    public class SingleFieldSchema {
        @AuraEnabled public String fieldApiName;
        @AuraEnabled public List<PicklistEntitySchema> picklistOptions;
        public SingleFieldSchema() {
            picklistOptions = new List<PicklistEntitySchema>();
        }
    }
    
    @TestVisible
    public class PicklistEntitySchema {
        @AuraEnabled public String	value;
        @AuraEnabled public String 	label;
        @AuraEnabled public Boolean isDefault;
        @AuraEnabled public Boolean isActive;
    }
    
    @AuraEnabled
    @TestVisible
    public static SingleFieldSchema[] getPicklistOptions(String objectApiName, String[] fieldApiNames) {
	// declaring return entity of the method
	SingleFieldSchema[] toReturn = new SingleFieldSchema[] {};
	
	
	try {
		// get through apex reflection the object describe (at runtime)
		Schema.SObjectType objDescribe = Schema.getGlobalDescribe().get(objectApiName);
		// get through apex reflection the object describe (at runtime)
		Schema.DescribeSObjectResult objDescribeResult = objDescribe.getDescribe();
		
		Map<String,Schema.SObjectField> fieldsList = objDescribeResult.fields.getMap();
		
		// iterate through fields
		for(String fieldApiName : fieldApiNames) {
			//istanciate at every loop a new object
			SingleFieldSchema singleEntity = new SingleFieldSchema();
			
			Schema.DescribeFieldResult fieldResult = fieldsList.get(fieldApiName).getDescribe();
			List<Schema.PicklistEntry> picklistEntries = fieldResult.getPicklistValues();
			//iterate through every picklist the code found inside the object
			for (Schema.PicklistEntry pickListVal : picklistEntries) {
				
				//istanciate a new inner object
				PicklistEntitySchema pes = new PicklistEntitySchema();
				pes.value		= pickListVal.getValue(); 
				pes.label		= pickListVal.getLabel();
				pes.isDefault	= pickListVal.isDefaultValue();
				pes.isActive	= pickListVal.isActive();
				
				singleEntity.picklistOptions.add(pes);
				
			}
			// OUTPUT WILL BE A JSON LIKE "SomeCustomPicklist__c" : {"label":"some custom picklist", "value" }
			singleEntity.fieldApiName = fieldResult.getName();
			toReturn.add(singleEntity);
		}
	} catch (Exception ex) {
		throw new AuraHandledException(ex.getMessage());
	}
	System.debug('## return is : ' + JSON.serialize(toReturn) );
	return toReturn;
}


    public static Map<String, List<Schema.PicklistEntry>> getFieldDependencies(String objectName, String controllingField, String dependentField)
    {
        Map<String, List<Schema.PicklistEntry>> controllingInfo = new Map<String, List<Schema.PicklistEntry>>();

        Schema.SObjectType objType = Schema.getGlobalDescribe().get(objectName);

        Schema.DescribeSObjectResult describeResult = objType.getDescribe();
        Schema.DescribeFieldResult controllingFieldInfo = describeResult.fields.getMap().get(controllingField).getDescribe();
        Schema.DescribeFieldResult dependentFieldInfo = describeResult.fields.getMap().get(dependentField).getDescribe();

        List<Schema.PicklistEntry> controllingValues = controllingFieldInfo.getPicklistValues();
        List<Schema.PicklistEntry> dependentValues = dependentFieldInfo.getPicklistValues();

        for(Schema.PicklistEntry currControllingValue : controllingValues)
        {
            // System.debug('ControllingField: Label:' + currControllingValue.getLabel());
            controllingInfo.put(currControllingValue.getValue(), new List<Schema.PicklistEntry>());
        }

        for(Schema.PicklistEntry currDependentValue : dependentValues)
        {
            String jsonString = JSON.serialize(currDependentValue);

            MyPickListInfo info = (MyPickListInfo) JSON.deserialize(jsonString, MyPickListInfo.class);

            String hexString = EncodingUtil.convertToHex(EncodingUtil.base64Decode(info.validFor)).toUpperCase();

            // System.debug('DependentField: Label:' + currDependentValue.getLabel() + ' ValidForInHex:' + hexString + ' JsonString:' + jsonString);

            Integer baseCount = 0;

            for(Integer curr : hexString.getChars())
            {
                Integer val = 0;

                if(curr >= 65)
                {
                    val = curr - 65 + 10;
                }
                else
                {
                    val = curr - 48;
                }

                if((val & 8) == 8)
                {
                    // System.debug('Dependent Field: ' + currDependentValue.getLabel() + ' Partof ControllingField:' + controllingValues[baseCount + 0].getLabel());
                    controllingInfo.get(controllingValues[baseCount + 0].getValue()).add(currDependentValue);
                }
                if((val & 4) == 4)
                {
                    // System.debug('Dependent Field: ' + currDependentValue.getLabel() + ' Partof ControllingField:' + controllingValues[baseCount + 1].getLabel());
                    controllingInfo.get(controllingValues[baseCount + 1].getValue()).add(currDependentValue);
                }
                if((val & 2) == 2)
                {
                    // System.debug('Dependent Field: ' + currDependentValue.getLabel() + ' Partof ControllingField:' + controllingValues[baseCount + 2].getLabel());
                    controllingInfo.get(controllingValues[baseCount + 2].getValue()).add(currDependentValue);
                }
                if((val & 1) == 1)
                {
                    // System.debug('Dependent Field: ' + currDependentValue.getLabel() + ' Partof ControllingField:' + controllingValues[baseCount + 3].getLabel());
                    controllingInfo.get(controllingValues[baseCount + 3].getValue()).add(currDependentValue);
                }

                baseCount += 4;
            }
        }

        //System.debug('ControllingInfo: ' + controllingInfo);

        return controllingInfo;
    }

    public static List<String> getPicklistValuesByRecordType(Id recordTypeId, String objectApiName, String picklistApiName){
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setHeader('Content-Type','application/json');
        request.setHeader('Accept','application/json');
        request.setHeader('Authorization','Bearer ' + UserInfo.getSessionId());
        request.setEndpoint(Url.getSalesforceBaseUrl().toExternalForm() + '/services/data/v53.0/ui-api/object-info/'+ objectApiName + '/picklist-values/'+ recordTypeId +'/' + picklistApiName);
        request.setMethod('GET');
        HttpResponse response = http.send(request);
        //System.debug('response body:: ' + response.getBody());
        Map<String, Object> respBodyMap = (Map<String, Object>) JSON.deserializeUntyped(
                response.getBody()
        );

        List<Object> valuesObjList = (List<Object>) respBodyMap.get('values');
        List<String> pickValues = new List<String>();
        for (Object obj : valuesObjList) {
            Map<String, Object> valuesMap = (Map<String, Object>) obj;
            pickValues.add((String) valuesMap.get('value'));
        }
        System.debug('pvalues ' + pickValues);
        return pickValues;
    }

    public static Boolean isIntakeStarted(CORE_Intake__c intake, String academicYearStartMonth){
        Integer currentYear = Date.today().year();
        Integer currentMonth = Date.today().month();
        Map<String, Integer> numberByMonthMap = generateNumberByMonth();
        Integer sessionNumberMonth = numberByMonthMap.get(intake.CORE_Session__c);
        Integer academicYear;
        String bothYears = intake.CORE_Academic_Year__c.substringAfter('CORE_PKL_');
        List<String> bothYearsList = bothYears.split('-');

        System.debug('academicYearStartMonth:: ' + academicYearStartMonth);
        System.debug('sessionNumberMonth:: ' + sessionNumberMonth);
        if(Integer.valueOf(academicYearStartMonth) < sessionNumberMonth){
            academicYear = Integer.valueOf(bothYearsList[0]);
        }else{
            academicYear = Integer.valueOf(bothYearsList[1]);
        }

        return academicYear <= currentYear && sessionNumberMonth <= currentMonth;
    }

    public static Date courseStartDate(CORE_Intake__c intake, String academicYearStartMonth){
        System.debug('## IT_Utility.courseStartDate has been fired ');
        Map<String, Integer> numberByMonthMap = generateNumberByMonth();
        Integer sessionNumberMonth = numberByMonthMap.get(intake.CORE_Session__c);//mese dell'anno (october)
        Integer academicYear; //Anno (2021-2022)
        String bothYears = intake.CORE_Academic_Year__c.substringAfter('CORE_PKL_');
        List<String> bothYearsList = bothYears.split('-');

        if(Integer.valueOf(academicYearStartMonth) < sessionNumberMonth){
            academicYear = Integer.valueOf(bothYearsList[0]);
        }else{
            academicYear = Integer.valueOf(bothYearsList[1]);
        }

		System.debug('academicYear:: ' + academicYear);
        System.debug('sessionNumberMonth:: ' + sessionNumberMonth);														   
        return Date.newInstance(academicYear, sessionNumberMonth, 01);
    }

    public static Map<String, Integer> generateNumberByMonth(){
        Map<String, Integer> numberByMonthMap = new Map<String, Integer>();
        numberByMonthMap.put('January', 1);
        numberByMonthMap.put('February', 2);
        numberByMonthMap.put('March', 3);
        numberByMonthMap.put('April', 4);
        numberByMonthMap.put('May', 5);
        numberByMonthMap.put('June', 6);
        numberByMonthMap.put('July', 7);
        numberByMonthMap.put('August', 8);
        numberByMonthMap.put('September', 9);
        numberByMonthMap.put('October', 10);
        numberByMonthMap.put('November', 11);
        numberByMonthMap.put('December', 12);

        return numberByMonthMap;
    }

    public static Map<String,String> getLabelFromApiObject(String apiNameObject){
        Map<String,String> returnValue = new Map<String,String>();

        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Schema.SObjectType leadSchema = schemaMap.get(apiNameObject);
        Map<String, Schema.SObjectField> fieldMap = leadSchema.getDescribe().fields.getMap();


        for (String fieldName: fieldMap.keySet()) {


            returnValue.put(fieldName,fieldMap.get(fieldName).getDescribe().getLabel());



        }
        return returnValue;
    }

    /*********************************************************************************************************
     * @name			The name of your class or method
     * @author			Andrea Morittu <amorittu@atlantic-technologies.com>
     * @created			13 / 05 / 2022
     * @description		Description of your code
     * @param			String param : Explanation
     * @return			Explanation of the return value
    **********************************************************************************************************/
    public static String getSobjectNameFromId( Id input_id ) {
        if ( input_id != null ) {
            return String.valueOf(input_id.getSobjectType()) ;
        }
        return null;
    }
}