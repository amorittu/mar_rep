public without sharing class CORE_LC_RelatedListController { 
    @TestVisible
    private static final String MISSING_RECORDID_MSG = 'Missing parameters recordId';
    @TestVisible
    private static final String MISSING_OBJECTNAME_MSG = 'Missing parameters objectName';
    @TestVisible
    private static final String CANCEL_STATUS = 'CORE_PKL_Cancelled';

    @AuraEnabled( cacheable=true ) 
    public static RelatedListWrapper fetchRecords( String recordId, String objectName, Integer maxNumberOfRecord, String sortField, String sortOrder, String search, Boolean hideCancelledTask)  { 
        RelatedListWrapper objWrap = new RelatedListWrapper();

        if(String.isBlank(recordId)){
            throw new IllegalArgumentException(MISSING_RECORDID_MSG);
        }
        if(String.isBlank(objectName)){
            throw new IllegalArgumentException(MISSING_OBJECTNAME_MSG);
        }

        if(maxNumberOfRecord == null){
            maxNumberOfRecord = -1;
        }

        String order;
        if(sortField != null && sortOrder !=null){
            order = ' ORDER BY '+sortField+' '+sortOrder;
        } else {
            order = ' ORDER BY '+ 'CompletedDateTime'+' '+'DESC';
        }

        String strLimit = '';
        if(maxNumberOfRecord > 0){
            strLimit = ' LIMIT '+ maxNumberOfRecord;
        }

        String filter1 = '';
        if(!String.isBlank(search)){
            search = '%'+ search + '%';
            filter1 = ' WHERE Subject like :search';
        }
        if (hideCancelledTask != null && hideCancelledTask){
            if(String.isBlank(filter1)){
                filter1 += ' WHERE';
            } else {
                filter1 += ' AND';
            }
            filter1 += ' Status != :CANCEL_STATUS';
        }

        String strSOQL = 'SELECT Id, (SELECT Status,CompletedDateTime,CORE_TECH_RecordTypeDevName__c,Subject, CreatedDate, CORE_TECH_In_Out__c , CORE_Is_Automatic__c, CORE_TECH_Channel__c, CORE_Point_of_contact__c,CORE_TECH_PocName__c, CORE_Call_Output__c, CallDurationInSeconds, CORE_Origin__c, CORE_Source__c FROM ActivityHistories '+ filter1 + order + strLimit +') activityHistories';
        String strSOQLWithoutLimit = 'SELECT Id, (SELECT Id FROM ActivityHistories '+ filter1 +') activityHistories';
        strSOQL += ' FROM ' + objectName;
        strSOQLWithoutLimit += ' FROM ' + objectName;

        String filter2 = ' WHERE Id = :recordId';
        strSOQL += filter2;
        strSOQLWithoutLimit += filter2;
        System.debug('strSOQL = '+strSOQL);
        List < SObject > queryResult = Database.query( strSOQL );
        List < SObject > queryResultWithoutLimit = Database.query( strSOQLWithoutLimit );

        List < SObject > attemptResults = new List<SObject>();
        objWrap.recordCount = '0';
        if(!queryResult.isEmpty()){
            SObject recordTmp = queryResult.get(0);
            if(objectName.toLowerCase().equals('opportunity')){
                Opportunity record = (Opportunity) recordTmp;
                Opportunity recordWithoutLimit = (Opportunity) queryResultWithoutLimit.get(0);
                attemptResults = record.activityHistories;
                objWrap.recordCount = String.valueOf(attemptResults.size());
                if(recordWithoutLimit.activityHistories.size() > attemptResults.size()){
                    objWrap.recordCount += '+';
                }
            } else {
                Account record = (Account) recordTmp;
                Account recordWithoutLimit = (Account) queryResultWithoutLimit.get(0);
                attemptResults = record.activityHistories;
                objWrap.recordCount = String.valueOf(attemptResults.size());
                if(recordWithoutLimit.activityHistories.size() > attemptResults.size()){
                    objWrap.recordCount += '+';
                }
            }
        }
        
        objWrap.listRecords = attemptResults;
        System.debug(objWrap);
        return objWrap; 
         
    } 

    public class RelatedListWrapper {
        @AuraEnabled
        public List < SObject > listRecords;
        @AuraEnabled
        public String recordCount;
    }
         
}