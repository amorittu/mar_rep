@IsTest
public class CORE_LeadAcquisitionBufferTriggerTest {
    @IsTest
    public static void Insert_should_execute_async_methods() {
        CORE_CatalogHierarchyModel catalog = new CORE_DataFaker_CatalogHierarchy('test').catalogHierarchy;
        //TODO : Use dataFaker
        String uniqueKey = 'test';
        
        CORE_LeadAcquisitionBuffer__c buffer = CORE_DataFaker_LeadAcquisitionBuffer.getBufferWhithoutInsert('Test', '+33647459000', '+33247459000', 'email@Test.fr', catalog);
        
        Test.startTest();
        Insert buffer;
        Integer myAsyncCalls = System.Limits.getAsyncCalls();
        Test.stopTest();

        CORE_LeadAcquisitionBuffer__c bufferResult = [Select Id, CORE_Status__c,CORE_ErrorCode__c,CORE_ErrorDescription__c FROM CORE_LeadAcquisitionBuffer__c];


        System.debug('bufferResult = ' + bufferResult);
        System.assertEquals(1,myAsyncCalls);
    }
}