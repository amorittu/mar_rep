@isTest
private class CORE_Flow_CompareStatusWithBUStatusTEST {

    private static final String INTEREST_EXTERNALID = 'setup';
    private static final String ACADEMIC_YEAR = 'CORE_PKL_2023-2024';

    @testSetup
    static void createData() {
        Id profileId = [SELECT Id FROM Profile WHERE Name = 'system administrator' LIMIT 1].Id;

        User usr = new User(
            Username = 'usertest@testflow.com',
            LastName = 'TEST_USER',
            Email = 'testemail@yopmail.com',
            Alias = 'T',
            CommunityNickname = 'T',
            TimeZoneSidKey = 'Europe/Paris',
            LocaleSidKey = 'fr_FR_EURO',
            EmailEncodingKey = 'ISO-8859-1',
            ProfileId = profileId,
            LanguageLocaleKey = 'fr'
        );
        database.insert(usr, true);

    }

    @IsTest
    static void compareStatusLevel_FALSE_TEST(){
        User usr = [SELECT Id FROM User WHERE Username = 'usertest@testflow.com' LIMIT 1];

        System.runAs(usr){

            List<CORE_Flow_CompareOppStatusWithBUStatus.OppParameters> params = new List<CORE_Flow_CompareOppStatusWithBUStatus.OppParameters>();

            CORE_Flow_CompareOppStatusWithBUStatus.OppParameters param = new CORE_Flow_CompareOppStatusWithBUStatus.OppParameters();
            param.oppStatus = 'Lead';
            param.buStatus = 'Applicant';
            params.add(param);

            CORE_Flow_CompareOppStatusWithBUStatus.compareStatusLevel(params);
        }
    }

    @IsTest
    static void compareStatusLevel_TRUE_TEST(){
        User usr = [SELECT Id FROM User WHERE Username = 'usertest@testflow.com' LIMIT 1];

        System.runAs(usr){

            List<CORE_Flow_CompareOppStatusWithBUStatus.OppParameters> params = new List<CORE_Flow_CompareOppStatusWithBUStatus.OppParameters>();

            CORE_Flow_CompareOppStatusWithBUStatus.OppParameters param = new CORE_Flow_CompareOppStatusWithBUStatus.OppParameters();
            param.oppStatus = 'Applicant';
            param.buStatus = 'Lead';
            params.add(param);

            CORE_Flow_CompareOppStatusWithBUStatus.compareStatusLevel(params);
        }
    }
}