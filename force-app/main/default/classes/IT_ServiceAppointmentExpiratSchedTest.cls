@isTest 
public class IT_ServiceAppointmentExpiratSchedTest {
    @TestSetup
    public static void makeData() {
        
        IT_MainObjectExpirationChecker__mdt mdt = new IT_MainObjectExpirationChecker__mdt();
        mdt.IT_DaysNumber__c         = 1;
        mdt.IT_FieldChecker__c       = 'Division';
        mdt.IT_ObjectIdentifier__c   = 'CampaignMember';
        mdt.DeveloperName            = 'NB';
       
        
        IT_ObjectExpirationCheckerBU__mdt mdt2 = new IT_ObjectExpirationCheckerBU__mdt();
        mdt2.IT_BusinessUnitExternalId__c   = 'NB.2';
        mdt2.IT_MainObjectExpirationChecker__c = mdt.Id;
        mdt2.IT_DaysNumber__c               = 2;

        CORE_DataFaker_CatalogHierarchy catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('NB');
        Account account = CORE_DataFaker_Account.getStudentAccount('test1');
        Opportunity opportunity = CORE_DataFaker_Opportunity.getOpportunity(
                catalogHierarchy.division.Id,
                catalogHierarchy.businessUnit.Id,
                account.Id,
                'Applicant',
                'Applicant - Application Submitted / New'
        );

        opportunity.CORE_OPP_Intake__c = catalogHierarchy.intake.Id;
        opportunity.CORE_OPP_Curriculum__c = catalogHierarchy.curriculum.Id;
        opportunity.CORE_OPP_Study_Area__c = catalogHierarchy.studyArea.Id;
        opportunity.CORE_OPP_Level__c = catalogHierarchy.level.Id;
        opportunity.IT_AdmissionOfficer__c = UserInfo.getUserId();
        update opportunity;

        CORE_Business_Unit__c businessUnit = catalogHierarchy.businessUnit;
        businessUnit.CORE_Business_Unit_ExternalId__c = 'NB.2';
        update businessUnit;
        
        ServiceAppointment sa = new ServiceAppointment();
		sa.ParentRecordId = account.Id;
		sa.DueDate = DateTime.Now().addDays(1);
		sa.EarliestStartTime = DateTime.Now();
		sa.SchedEndTime = DateTime.Now().addHours(1);
		sa.SchedStartTime = DateTime.Now();
		sa.Status = 'Scheduled';
		sa.AppointmentType = 'Call';
		sa.CORE_TECH_Relatd_Opportunity__c = opportunity.Id;
		insert sa;
    }

    @isTest 
    public static void testBehavior() {
        ServiceAppointment sa = [SELECT Id FROM ServiceAppointment ];
        
        Test.startTest();

        IT_ServiceAppointmentExpirationScheduler scheduler = new IT_ServiceAppointmentExpirationScheduler();
            String chron = '0 0 23 * * ?';        
            System.schedule('Test Sched', chron, scheduler);
        Test.stopTest();
    }
}