@IsTest
public class CORE_DataFaker_LeadAcquisitionBuffer {
    public static final String CONSENT_TYPE = 'CORE_Optin_Optout';
    public static final String CONSENT_CHANNEL = 'CORE_PKL_Email';
    public static final String SALUTATION = 'Mr.';
    public static final String LANGUAGE = 'FRA';
    public static final String ACADEMIC_YEAR = 'CORE_PKL_2022-2023';
    public static final String LEARNING_MATERIAL = 'CORE_PKL_iPad';
    public static final String POC_NATURE = 'CORE_PKL_Offline';
    public static final String POC_DEVICE = 'CORE_PKL_Computer';
    public static final String POC_CAPTURE_CHANNEL = 'CORE_PKL_Phone_call';
    public static final String POC_CAMPAIGN_MEDIUM = 'CORE_PKL_Site';
    public static final String ADH_LEVEL = 'CORE_PKL_Certificates';
    
    public static CORE_LeadAcquisitionBuffer__c getBuffer(String uniqueKey,String mobile, String phone, String email, CORE_CatalogHierarchyModel catalog){
        CORE_LeadAcquisitionBuffer__c buffer = getBufferWhithoutInsert(uniqueKey, mobile, phone, email, catalog);
        
        Insert buffer;
        return buffer;
    }

    public static CORE_LeadAcquisitionBuffer__c getBufferWhithoutInsert(String uniqueKey, String mobile, String phone, String email, CORE_CatalogHierarchyModel catalog){
        CORE_LeadAcquisitionBuffer__c buffer = new CORE_LeadAcquisitionBuffer__c(CORE_Comments__c = 'Comment '+ uniqueKey );
        buffer = getBufferWithPersonAccountDatas(uniqueKey, mobile, phone, email, buffer);
        buffer = getBufferWithPointOfContact(buffer);
        buffer = getBufferWithConsentDatas(catalog, 1, buffer);
        buffer = getBufferWithConsentDatas(catalog, 2, buffer);
        buffer = getBufferWithConsentDatas(catalog, 3, buffer);
        buffer = getBufferWithConsentDatas(catalog, 4, buffer);
        buffer = getBufferWithConsentDatas(catalog, 5, buffer);
        buffer = getBufferWithInterest(uniqueKey, catalog, 1, buffer);
        buffer = getBufferWithInterest(uniqueKey, catalog, 2, buffer);
        buffer = getBufferWithInterest(uniqueKey, catalog, 3, buffer);
        buffer = getBufferWithAcademicDiplomaHistoryDatas(buffer);

        return buffer;
    }
    public static CORE_LeadAcquisitionBuffer__c getBufferWithCampaignMember(CORE_LeadAcquisitionBuffer__c currentBuffer, Id campaignId){
        if(currentBuffer == null){
            currentBuffer = new CORE_LeadAcquisitionBuffer__c();
        }

        currentBuffer.CORE_CS_CampaignId__c = campaignId;

        return currentBuffer;
    }
    public static CORE_LeadAcquisitionBuffer__c getBufferWithInterest(String uniqueKey, CORE_CatalogHierarchyModel catalog, Integer interestNumber, CORE_LeadAcquisitionBuffer__c currentBuffer){

        if(currentBuffer == null){
            currentBuffer = new CORE_LeadAcquisitionBuffer__c();
        }

        switch on interestNumber {
            when 1 {
                currentBuffer.CORE_MI_OPPAcademicYear__c = ACADEMIC_YEAR;
                currentBuffer.CORE_MI_OPPTDivision__c = catalog.division.Id;
                currentBuffer.CORE_MI_OPPTBusiness_Unit__c = catalog.businessUnit.Id;
                currentBuffer.CORE_MI_OPPTStudy_Area__c = catalog.studyArea.Id;
                currentBuffer.CORE_MI_OPPTCurriculum__c = catalog.curriculum.Id;
                currentBuffer.CORE_MI_OPPTLevel__c = catalog.level.Id;
                currentBuffer.CORE_MI_OPPTIntake__c = catalog.intake.Id;
                currentBuffer.CORE_MI_ApplicationShippedByMail__c = true;
                currentBuffer.CORE_MI_BrochureShippedByMail__c = false;
                currentBuffer.CORE_MI_IsSuspect__c = false;
                currentBuffer.CORE_MI_LeadSourceExternalId__c = uniqueKey + '1';
                currentBuffer.CORE_MI_LearningMaterial__c = LEARNING_MATERIAL;
                currentBuffer.CORE_MI_AOL_ExternalId__c  = null;
                //Account acc = CORE_DataFaker_Account.getStudentAccount('test1');
                //System.debug('size '+ acc.CORE_LegacyCRMID__c);
            }
            when 2 {
                currentBuffer.CORE_SI_OPPAcademicYear__c = ACADEMIC_YEAR;
                currentBuffer.CORE_SI_OPPTDivision__c = catalog.division.Id;
                currentBuffer.CORE_SI_OPPTBusiness_Unit__c = catalog.businessUnit.Id;
                currentBuffer.CORE_SI_OPPTStudy_Area__c = catalog.studyArea.Id;
                currentBuffer.CORE_SI_OPPTCurriculum__c = catalog.curriculum.Id;
                currentBuffer.CORE_SI_OPPTLevel__c = catalog.level.Id;
                currentBuffer.CORE_SI_OPPTIntake__c = catalog.intake.Id;
                currentBuffer.CORE_SI_ApplicationShippedByMail__c = true;
                currentBuffer.CORE_SI_BrochureShippedByMail__c = false;
                currentBuffer.CORE_SI_IsSuspect__c = false;
                currentBuffer.CORE_SI_LeadSourceExternalId__c = uniqueKey + '2';
                currentBuffer.CORE_SI_LearningMaterial__c = LEARNING_MATERIAL;
                currentBuffer.CORE_SI_AOL_ExternalId__c  = null;
            }
            when 3 {
                currentBuffer.CORE_TI_OPPAcademicYear__c = ACADEMIC_YEAR;
                currentBuffer.CORE_TI_OPPTDivision__c = catalog.division.Id;
                currentBuffer.CORE_TI_OPPTBusiness_Unit__c = catalog.businessUnit.Id;
                currentBuffer.CORE_TI_OPPTStudy_Area__c = catalog.studyArea.Id;
                currentBuffer.CORE_TI_OPPTCurriculum__c = catalog.curriculum.Id;
                currentBuffer.CORE_TI_OPPTLevel__c = catalog.level.Id;
                currentBuffer.CORE_TI_OPPTIntake__c = catalog.intake.Id;
                currentBuffer.CORE_TI_ApplicationShippedByMail__c = true;
                currentBuffer.CORE_TI_BrochureShippedByMail__c = false;
                currentBuffer.CORE_TI_IsSuspect__c = false;
                currentBuffer.CORE_TI_LeadSourceExternalId__c = uniqueKey + '3';
                currentBuffer.CORE_TI_LearningMaterial__c = LEARNING_MATERIAL;
                currentBuffer.CORE_TI_AOL_ExternalId__c  = null;
            }
        }
        

        return currentBuffer;
    }

    
    public static CORE_LeadAcquisitionBuffer__c getBufferWithPointOfContact(CORE_LeadAcquisitionBuffer__c currentBuffer){

        if(currentBuffer == null){
            currentBuffer = new CORE_LeadAcquisitionBuffer__c();
        }

        currentBuffer.CORE_POC_CreatedDate__c = System.now();
        currentBuffer.CORE_POC_SourceId__c = 'sourceTest';
        currentBuffer.CORE_POC_Nature__c = POC_NATURE;
        currentBuffer.CORE_POC_CaptureChannel__c = POC_CAPTURE_CHANNEL;
        currentBuffer.CORE_POC_Origin__c = 'originTest';
        currentBuffer.CORE_POC_SalesOrigin__c = 'salesOriginTest';
        currentBuffer.CORE_POC_CampaignMedium__c = POC_CAMPAIGN_MEDIUM;
        currentBuffer.CORE_POC_CampaignSource__c = 'campaignSourceTest';
        currentBuffer.CORE_POC_CampaignTerm__c = 'campaignTermTest';
        currentBuffer.CORE_POC_CampaignContent__c = 'campaignConsentTest';
        currentBuffer.CORE_POC_CampaignName__c = 'campaignNameTest';
        currentBuffer.CORE_POC_Device__c = POC_DEVICE;
        currentBuffer.CORE_POC_FirstVisit__c = System.today();
        currentBuffer.CORE_POC_FirstPage__c = 'http://www.test.com';
        currentBuffer.CORE_POC_IPAddress__c = '192.168.1.1';
        currentBuffer.CORE_POC_Latitude__c = '48.856614';
        currentBuffer.CORE_POC_Longitude__c = '2.3522219';

        return currentBuffer;
    }
    public static CORE_LeadAcquisitionBuffer__c getBufferWithConsentDatas(CORE_CatalogHierarchyModel catalog, Integer consentNumber, CORE_LeadAcquisitionBuffer__c currentBuffer){
        if(currentBuffer == null){
            currentBuffer = new CORE_LeadAcquisitionBuffer__c();
        }

        switch on consentNumber {
            when 2 {
                currentBuffer.CORE_CO2_ConsentType__c = CONSENT_TYPE; 
                currentBuffer.CORE_CO2_ConsentChannel__c = CONSENT_CHANNEL; 
                currentBuffer.CORE_CO2_ConsentDescription__c = 'test';
                currentBuffer.CORE_CO2_OptIn__c = true;
                currentBuffer.CORE_CO2_OptInDate__c = System.now();
                currentBuffer.CORE_CO2_OptOut__c = false;
                currentBuffer.CORE_CO2_OptOutDate__c = System.now();
                currentBuffer.CORE_CO2_Division__c = catalog.division.Id;
                currentBuffer.CORE_CO2_BusinessUnit__c = catalog.businessUnit.Id;
            }
            when 3 {
                currentBuffer.CORE_CO3_ConsentType__c = CONSENT_TYPE; 
                currentBuffer.CORE_CO3_ConsentChannel__c = CONSENT_CHANNEL; 
                currentBuffer.CORE_CO3_ConsentDescription__c = 'test';
                currentBuffer.CORE_CO3_OptIn__c = true;
                currentBuffer.CORE_CO3_OptInDate__c = System.now();
                currentBuffer.CORE_CO3_OptOut__c = false;
                currentBuffer.CORE_CO3_OptOutDate__c = System.now();
                currentBuffer.CORE_CO3_Division__c = catalog.division.Id;
                currentBuffer.CORE_CO3_BusinessUnit__c = catalog.businessUnit.Id;
            }
            when 4 {
                currentBuffer.CORE_CO4_ConsentType__c = CONSENT_TYPE; 
                currentBuffer.CORE_CO4_ConsentChannel__c = CONSENT_CHANNEL; 
                currentBuffer.CORE_CO4_ConsentDescription__c = 'test';
                currentBuffer.CORE_CO4_OptIn__c = true;
                currentBuffer.CORE_CO4_OptInDate__c = System.now();
                currentBuffer.CORE_CO4_OptOut__c = false;
                currentBuffer.CORE_CO4_OptOutDate__c = System.now();
                currentBuffer.CORE_CO4_Division__c = catalog.division.Id;
                currentBuffer.CORE_CO4_BusinessUnit__c = catalog.businessUnit.Id;
            }
            when 5 {
                currentBuffer.CORE_CO5_ConsentType__c = CONSENT_TYPE; 
                currentBuffer.CORE_CO5_ConsentChannel__c = CONSENT_CHANNEL; 
                currentBuffer.CORE_CO5_ConsentDescription__c = 'test';
                currentBuffer.CORE_CO5_OptIn__c = true;
                currentBuffer.CORE_CO5_OptInDate__c = System.now();
                currentBuffer.CORE_CO5_OptOut__c = false;
                currentBuffer.CORE_CO5_OptOutDate__c = System.now();
                currentBuffer.CORE_CO5_Division__c = catalog.division.Id;
                currentBuffer.CORE_CO5_BusinessUnit__c = catalog.businessUnit.Id;
            }
            when else {
                currentBuffer.CORE_CO1_ConsentType__c = CONSENT_TYPE; 
                currentBuffer.CORE_CO1_ConsentChannel__c = CONSENT_CHANNEL; 
                currentBuffer.CORE_CO1_ConsentDescription__c = 'test';
                currentBuffer.CORE_CO1_OptIn__c = true;
                currentBuffer.CORE_CO1_OptInDate__c = System.now();
                currentBuffer.CORE_CO1_OptOut__c = false;
                currentBuffer.CORE_CO1_OptOutDate__c = System.now();
                currentBuffer.CORE_CO1_Division__c = catalog.division.Id;
                currentBuffer.CORE_CO1_BusinessUnit__c = catalog.businessUnit.Id;
            }
        }

        return currentBuffer;
    }

    public static CORE_LeadAcquisitionBuffer__c getBufferWithPersonAccountDatas(String uniqueKey, String mobile, String phone, String email, CORE_LeadAcquisitionBuffer__c currentBuffer){
        String lastName = 'lastName-' + uniqueKey;
        String firstName = 'firstName-' + uniqueKey;

        Boolean countryPicklistIsActivated = CORE_SobjectUtils.doesFieldExist('User', 'Countrycode');
        if(currentBuffer == null){
            currentBuffer = new CORE_LeadAcquisitionBuffer__c();
        }

        currentBuffer.CORE_PA_Salutation__c = SALUTATION;
        currentBuffer.CORE_PA_CreatedDate__c = System.now();
        currentBuffer.CORE_PA_LastName__c = lastName;
        currentBuffer.CORE_PA_FirstName__c = firstName;
        currentBuffer.CORE_PA_Phone__c = phone;
        currentBuffer.CORE_PA_Mobile__c = mobile;
        currentBuffer.CORE_PA_Email__c = email;
        currentBuffer.CORE_PA_MailingStreet__c = 'street-' + uniqueKey;
        currentBuffer.CORE_PA_MailingPostalCode__c = '75010';
        currentBuffer.CORE_PA_MailingCity__c = 'city-' + uniqueKey;
        //currentBuffer.CORE_PA_MailingState__c = 'state-' + uniqueKey;
        if(countryPicklistIsActivated){
            currentBuffer.CORE_PA_MailingCountryCode__c = 'FR';
        } else{
            currentBuffer.CORE_PA_MailingCountryCode__c = 'France';
        }
        currentBuffer.CORE_PA_LanguageWebsite__c = LANGUAGE;
        currentBuffer.CORE_PA_GAUserId__c = 'gaUserId-' + uniqueKey;
        currentBuffer.CORE_PA_updateIfNotNull__c = false;
        currentBuffer.CORE_PA_MainNationality__c = 'FRA';
        return currentBuffer;
    }

    public static CORE_LeadAcquisitionBuffer__c getBufferWithAcademicDiplomaHistoryDatas(CORE_LeadAcquisitionBuffer__c currentBuffer){
        if(currentBuffer == null){
            currentBuffer = new CORE_LeadAcquisitionBuffer__c();
        }

        currentBuffer.CORE_ADH_Name__c = 'nameTest';
        currentBuffer.CORE_ADH_Level__c = ADH_LEVEL;

        return currentBuffer;
    }
}