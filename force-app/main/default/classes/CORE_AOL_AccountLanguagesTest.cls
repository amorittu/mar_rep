@IsTest
public with sharing class CORE_AOL_AccountLanguagesTest {
    private static final String AOL_EXTERNALID = 'setupAol';

    private static final String REQ_BODY = '{'
    + '"aolExternalId": "' + AOL_EXTERNALID + '",'
    + '"foreignLanguages": {'
    + '"language1": "FRA",'
    + '"language1Level": "B1-Intermediate|Medium",'
    + '"language2": "DEU",'
    + '"language2Level": "A1-Elementary|Low"'
    + '},'
    + '"additionalFields": {'
    + '"CORE_Commercial_Name__c": "test value"'
    + '},'
    + '"aolProgressionFields": {'
    + '"aolProgression": 50.09,'
    + '"documentProgression": "Started",'
    + '"firstDocumentUploadDate": "2022-03-29",'
    + '"lastAolEventDate": "2022-03-29T08:47:39.689Z",'
    + '"lastDeaAolLoginDate": "2022-03-29T08:47:39.689Z",'
    + '"isAol": true,'
    + '"isAolOnlineRegistered": true,'
    + '"isAolRetailRegistered": true'
    + '}'
    + '}';

    private static final String REQ_BODY_ERROR_NO_ID = '{'
    + '"aolExternalId": "",'
    + '"foreignLanguages": {'
    + '"language1": "FRA",'
    + '"language1Level": "B1-Intermediate|Medium",'
    + '"language2": "DEU",'
    + '"language2Level": "A1-Elementary|Low"'
    + '},'
    + '"additionalFields": {'
    + '"CORE_Commercial_Name__c": "test value"'
    + '},'
    + '"aolProgressionFields": {'
    + '"aolProgression": 50.09,'
    + '"documentProgression": "Started",'
    + '"firstDocumentUploadDate": "2022-03-29",'
    + '"lastAolEventDate": "2022-03-29T08:47:39.689Z",'
    + '"lastDeaAolLoginDate": "2022-03-29T08:47:39.689Z",'
    + '"isAol": true,'
    + '"isAolOnlineRegistered": true,'
    + '"isAolRetailRegistered": true'
    + '}'
    + '}';

    private static final String REQ_BODY_ERROR_INVALID_ID = '{'
    + '"aolExternalId": "invalidID",'
    + '"foreignLanguages": {'
    + '"language1": "FRA",'
    + '"language1Level": "B1-Intermediate|Medium",'
    + '"language2": "DEU",'
    + '"language2Level": "A1-Elementary|Low"'
    + '},'
    + '"additionalFields": {'
    + '"CORE_Commercial_Name__c": "test value"'
    + '},'
    + '"aolProgressionFields": {'
    + '"aolProgression": 50.09,'
    + '"documentProgression": "Started",'
    + '"firstDocumentUploadDate": "2022-03-29",'
    + '"lastAolEventDate": "2022-03-29T08:47:39.689Z",'
    + '"lastDeaAolLoginDate": "2022-03-29T08:47:39.689Z",'
    + '"isAol": true,'
    + '"isAolOnlineRegistered": true,'
    + '"isAolRetailRegistered": true'
    + '}'
    + '}';

    private static final String REQ_BODY_ERROR = '{'
    + '"aolExternalId": "' + AOL_EXTERNALID + '",'
    + '"foreignLanguages": {'
    + '"language1": "FRA",'
    + '"language1Level": "invalid_value",'
    + '"language2": "DEU",'
    + '"language2Level": "A1-Elementary|Low"'
    + '},'
    + '"additionalFields": {'
    + '"CORE_Commercial_Name__c": "test value"'
    + '},'
    + '"aolProgressionFields": {'
    + '"aolProgression": 50.09,'
    + '"documentProgression": "Started",'
    + '"firstDocumentUploadDate": "2022-03-29",'
    + '"lastAolEventDate": "2022-03-29T08:47:39.689Z",'
    + '"lastDeaAolLoginDate": "2022-03-29T08:47:39.689Z",'
    + '"isAol": true,'
    + '"isAolOnlineRegistered": true,'
    + '"isAolRetailRegistered": true'
    + '}'
    + '}';

    @TestSetup
    static void makeData(){
        CORE_CatalogHierarchyModel catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test').catalogHierarchy;
        Account account = CORE_DataFaker_Account.getStudentAccount('test');
        Opportunity  opportunity = CORE_DataFaker_Opportunity.getAOLOpportunity(catalogHierarchy.division.Id, 
            catalogHierarchy.businessUnit.Id, 
            account.Id, 
            CORE_AOL_Constants.PKL_OPP_STAGENAME_PROSPECT, 
            CORE_AOL_Constants.PKL_OPP_SUBSTATUS_PROSPECTAPPSTARTED, 
            AOL_EXTERNALID
        );
    }
    
    @IsTest
    public static void accountLanguagesProcess_Should_Update_Account_and_return_it() {
        CORE_AOL_Wrapper.AOLProgressionFields aolProgressionFields= CORE_DataFaker_AOL_Wrapper.getAolProgressionFieldsWrapper();
        CORE_AOL_Wrapper.ForeignLanguages foreignLanguages = CORE_DataFaker_AOL_Wrapper.getAccountLanguagesWrapper();

        CORE_AOL_GlobalWorker globalWorker = new CORE_AOL_GlobalWorker(aolProgressionFields,AOL_EXTERNALID);

        Account account = CORE_AOL_AccountLanguages.languagesProcess(globalWorker, foreignLanguages);
        database.update(account, true);

        account = [SELECT Id,CORE_Foreign_language_1__c,CORE_Foreign_language_1_Level__c,CORE_Foreign_language_2__c,CORE_Foreign_language_2_Level__c
        FROM Account];

        System.assertEquals(foreignLanguages.language1, account.CORE_Foreign_language_1__c);
        System.assertEquals(foreignLanguages.language1Level, account.CORE_Foreign_language_1_Level__c);
        System.assertEquals(foreignLanguages.language2, account.CORE_Foreign_language_2__c);
        System.assertEquals(foreignLanguages.language2Level, account.CORE_Foreign_language_2_Level__c);
    }

    @IsTest
    private static void execute_should_return_an_error_if_madatory_missing(){
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/aol/account/languages';

        req.httpMethod = 'PUT';
        req.requestBody = Blob.valueOf(REQ_BODY_ERROR_NO_ID);

        RestContext.request = req;
        RestContext.response= res;

        CORE_AOL_Wrapper.ResultWrapperGlobal result = CORE_AOL_AccountLanguages.execute();

        System.assertEquals('KO', result.status);
    }

    @IsTest
    private static void execute_should_return_an_error_if_invalid_externalID(){
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/aol/account/languages';

        req.httpMethod = 'PUT';
        req.requestBody = Blob.valueOf(REQ_BODY_ERROR_INVALID_ID);

        RestContext.request = req;
        RestContext.response= res;

        CORE_AOL_Wrapper.ResultWrapperGlobal result = CORE_AOL_AccountLanguages.execute();

        System.assertEquals('KO', result.status);
    }

    @IsTest
    private static void execute_should_return_an_error_if_something_goes_wrong(){
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/aol/account/languages';

        req.httpMethod = 'PUT';
        req.requestBody = Blob.valueOf(REQ_BODY_ERROR);

        RestContext.request = req;
        RestContext.response= res;

        CORE_AOL_Wrapper.ResultWrapperGlobal result = CORE_AOL_AccountLanguages.execute();

        System.assertEquals('KO', result.status);
    }

    @IsTest
    private static void execute_should_return_a_success_if_all_updates_are_ok(){
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/aol/account/languages';

        req.httpMethod = 'PUT';
        req.requestBody = Blob.valueOf(REQ_BODY);

        RestContext.request = req;
        RestContext.response= res;

        CORE_AOL_Wrapper.ResultWrapperGlobal result = CORE_AOL_AccountLanguages.execute();

        System.assertEquals('OK', result.status);
    }
}