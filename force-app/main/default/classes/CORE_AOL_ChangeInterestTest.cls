@IsTest
public without sharing class CORE_AOL_ChangeInterestTest {
    private static final String AOL_EXTERNALID = 'setupAol';

    @TestSetup
    static void makeData(){
        CORE_CatalogHierarchyModel catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('setup').catalogHierarchy;
        Account account = CORE_DataFaker_Account.getStudentAccount('setup');

        Opportunity  opportunity = CORE_DataFaker_Opportunity.getAOLOpportunity(
            catalogHierarchy.division.Id, 
            catalogHierarchy.businessUnit.Id,
            catalogHierarchy.studyArea.Id, 
            catalogHierarchy.curriculum.Id, 
            catalogHierarchy.level.Id,
            catalogHierarchy.intake.Id,
            account.Id, 
            CORE_AOL_Constants.PKL_OPP_STAGENAME_PROSPECT, 
            CORE_AOL_Constants.PKL_OPP_SUBSTATUS_PROSPECTAPPSTARTED, 
            AOL_EXTERNALID
        );
    }

    @IsTest
    private static void execute_should_return_a_success_if_all_upsert_are_ok(){
        String aolExternalId = AOL_EXTERNALID;
        CORE_CatalogHierarchyModel catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test1').catalogHierarchy;
        Account account = [SELECT Id FROM Account];

        Opportunity opp = [SELECT Id, CORE_OPP_Business_Unit__r.CORE_Business_Unit_ExternalId__c, CORE_OPP_Study_Area__r.CORE_Study_Area_ExternalId__c,
            CORE_OPP_Curriculum__r.CORE_Curriculum_ExternalId__c , 
            CORE_OPP_Level__r.CORE_Level_ExternalId__c, CORE_OPP_Intake__r.CORE_Intake_ExternalId__c 
            FROM Opportunity];

        CORE_AOL_Wrapper.AOLProgressionFields aolProgressionFields = CORE_DataFaker_AOL_Wrapper.getAolProgressionFieldsWrapper();
        CORE_AOL_GlobalWorker aolWorker = new CORE_AOL_GlobalWorker(aolProgressionFields, aolExternalId);

        CORE_AOL_Wrapper.InterestWrapper interest = new CORE_AOL_Wrapper.InterestWrapper();
        interest.businessUnit = opp.CORE_OPP_Business_Unit__r.CORE_Business_Unit_ExternalId__c;
		interest.studyArea = opp.CORE_OPP_Study_Area__r.CORE_Study_Area_ExternalId__c;
		interest.curriculum = opp.CORE_OPP_Curriculum__r.CORE_Curriculum_ExternalId__c;
		interest.level = opp.CORE_OPP_Level__r.CORE_Level_ExternalId__c;
        interest.intake = opp.CORE_OPP_Intake__r.CORE_Intake_ExternalId__c;  

        CORE_AOL_Wrapper.ResultWrapper result = CORE_AOL_ChangeInterest.execute(
            aolExternalId, 
            interest,
            aolProgressionFields
        );

        System.debug('result.message = ' + result.message);
        System.assertEquals('OK', result.statut);
    }

    @IsTest
    private static void execute_should_return_an_error_if_all_upsert_are_not_ok(){
        String aolExternalId = 'test1';
        CORE_CatalogHierarchyModel catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test1').catalogHierarchy;
        Account account = [SELECT Id FROM Account];
        CORE_AOL_Wrapper.AOLProgressionFields aolProgressionFields = CORE_DataFaker_AOL_Wrapper.getAolProgressionFieldsWrapper();
        CORE_AOL_GlobalWorker aolWorker = new CORE_AOL_GlobalWorker(aolProgressionFields, aolExternalId);

        CORE_AOL_Wrapper.InterestWrapper interest = CORE_DataFaker_AOL_Wrapper.geInterestWrapper(catalogHierarchy, true, account.Id);

        CORE_AOL_Wrapper.ResultWrapper result = CORE_AOL_ChangeInterest.execute(
            aolExternalId, 
            interest,
            aolProgressionFields
        );

        Opportunity opp = [SELECT Id, CORE_OPP_Business_Unit__r.CORE_Business_Unit_ExternalId__c, CORE_OPP_Study_Area__r.CORE_Study_Area_ExternalId__c,
            CORE_OPP_Curriculum__r.CORE_Curriculum_ExternalId__c , 
            CORE_OPP_Level__r.CORE_Level_ExternalId__c, CORE_OPP_Intake__r.CORE_Intake_ExternalId__c 
            FROM Opportunity];

        interest = new CORE_AOL_Wrapper.InterestWrapper();
        //interest.businessUnit = opp.CORE_OPP_Business_Unit__r.CORE_Business_Unit_ExternalId__c;
		interest.studyArea = opp.CORE_OPP_Study_Area__r.CORE_Study_Area_ExternalId__c;
		interest.curriculum = opp.CORE_OPP_Curriculum__r.CORE_Curriculum_ExternalId__c;
		interest.level = opp.CORE_OPP_Level__r.CORE_Level_ExternalId__c;
        interest.intake = opp.CORE_OPP_Intake__r.CORE_Intake_ExternalId__c;  

        result = CORE_AOL_ChangeInterest.execute(
            aolExternalId, 
            interest,
            aolProgressionFields
        );
        
        System.assertEquals('KO', result.statut);

        
        interest = new CORE_AOL_Wrapper.InterestWrapper();
        //interest.businessUnit = opp.CORE_OPP_Business_Unit__r.CORE_Business_Unit_ExternalId__c;
		interest.studyArea = opp.CORE_OPP_Study_Area__r.CORE_Study_Area_ExternalId__c;
		interest.curriculum = opp.CORE_OPP_Curriculum__r.CORE_Curriculum_ExternalId__c;
		interest.level = opp.CORE_OPP_Level__r.CORE_Level_ExternalId__c;
        //interest.intake = opp.CORE_OPP_Intake__r.CORE_Intake_ExternalId__c;  

        result = CORE_AOL_ChangeInterest.execute(
            aolExternalId, 
            interest,
            aolProgressionFields
        );

        System.assertEquals('KO', result.statut);

    }


    @IsTest
    private static void getOppLineItem_should_insert_opp_line_items(){
        String aolExternalId = AOL_EXTERNALID;

        Product2 product = [Select Id from product2];
        CORE_Business_Unit__c bu = [Select Id from CORE_Business_Unit__c];

        CORE_CatalogHierarchyModel catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test1').catalogHierarchy;
        Account account = [SELECT Id FROM Account];
        CORE_AOL_Wrapper.AOLProgressionFields aolProgressionFields = CORE_DataFaker_AOL_Wrapper.getAolProgressionFieldsWrapper();
        CORE_AOL_GlobalWorker aolWorker = new CORE_AOL_GlobalWorker(aolProgressionFields, aolExternalId);

        CORE_AOL_Wrapper.InterestWrapper interest = CORE_DataFaker_AOL_Wrapper.geInterestWrapper(catalogHierarchy, true, account.Id);
        CORE_AOL_ChangeInterest changeInterestProcess = new CORE_AOL_ChangeInterest(aolWorker, aolExternalId, interest);

        OpportunityLineItem result = CORE_AOL_ChangeInterest.getOppLineItem(changeInterestProcess.dataMaker, changeInterestProcess.aolWorker.aolOpportunity, true);
        System.assertEquals(null, result);

        Opportunity opp = [Select Id, CORE_OPP_Business_Unit__c,CORE_Main_Product_Interest__c  from Opportunity];
        opp.CORE_Main_Product_Interest__c = product.Id;
        Id pricebookId = CORE_DataFaker_PriceBook.getStandardPricebookId();
        CORE_PriceBook_BU__c pbBu = CORE_DataFaker_PriceBook.getPriceBookBu(pricebookId, bu.Id , AOL_EXTERNALID);
        PricebookEntry pbEntry = CORE_DataFaker_PriceBook.getPriceBookEntry(product.Id, pricebookId, 10);

        result = CORE_AOL_ChangeInterest.getOppLineItem(changeInterestProcess.dataMaker, opp, true);
        System.assertEquals(pbEntry.Id, result.PricebookEntryId);

        result = CORE_AOL_ChangeInterest.getOppLineItem(changeInterestProcess.dataMaker, opp, true);
        System.assertEquals(null, result);
    }

    @IsTest
    private static void getCatalogHierarchy_should_return_a_catalog_with_ids(){
        CORE_CatalogHierarchyModel catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test1').catalogHierarchy;
        Account account = [SELECT Id FROM Account];

        CORE_AOL_Wrapper.InterestWrapper interest = CORE_DataFaker_AOL_Wrapper.geInterestWrapper(catalogHierarchy, true, account.Id);

        CORE_CatalogHierarchyModel result = CORE_AOL_ChangeInterest.getCatalogHierarchy(interest);

        System.assertEquals(catalogHierarchy.division.Id, result.division.Id);
        System.assertEquals(catalogHierarchy.businessUnit.Id, result.businessUnit.Id);
        System.assertEquals(catalogHierarchy.curriculum.Id, result.curriculum.Id);
        System.assertEquals(catalogHierarchy.level.Id, result.level.Id);
        System.assertEquals(catalogHierarchy.intake.Id, result.intake.Id);
    }

    @IsTest
    private static void getInterestMissingParameters_should_return_empty_list_if_there_is_no_missing_interest_parameters(){
        String formerAolExternalId = 'test1';
        String newAolExternalId = 'test2';
        CORE_CatalogHierarchyModel catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test1').catalogHierarchy;
        Account account = [SELECT Id FROM Account];

        CORE_AOL_Wrapper.InterestWrapper interest = CORE_DataFaker_AOL_Wrapper.geInterestWrapper(catalogHierarchy, true, account.Id);
        List<String> results = CORE_AOL_ChangeInterest.getInterestMissingParameters(interest);

        System.assertEquals(true, results.isEmpty());
    }

    @IsTest
    private static void getInterestMissingParameters_should_return_list_of_missing_paramters_when_there_is_missing_interest_parameters(){
        CORE_CatalogHierarchyModel catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test1').catalogHierarchy;
        Account account = [SELECT Id FROM Account];
        String wrapperName = 'interest';
        CORE_AOL_Wrapper.InterestWrapper interest = CORE_DataFaker_AOL_Wrapper.geInterestWrapper(catalogHierarchy, true, account.Id);
        List<String> results = CORE_AOL_ChangeInterest.getInterestMissingParameters(null);

        System.assertEquals(1, results.size());
        System.assert(results.contains(wrapperName));

        //'businessUnit', 'studyArea', 'curriculum', 'level', 'intake'
        interest.businessUnit = null;
        results = CORE_AOL_ChangeInterest.getInterestMissingParameters(interest);

        System.assertEquals(1, results.size());
        System.assert(results.contains(wrapperName + '.businessUnit'));

        interest.studyArea = null;
        results = CORE_AOL_ChangeInterest.getInterestMissingParameters(interest);

        System.assertEquals(2, results.size());
        System.assert(results.contains(wrapperName + '.businessUnit'));
        System.assert(results.contains(wrapperName + '.studyArea'));

        interest.curriculum = null;
        results = CORE_AOL_ChangeInterest.getInterestMissingParameters(interest);

        System.assertEquals(3, results.size());
        System.assert(results.contains(wrapperName + '.businessUnit'));
        System.assert(results.contains(wrapperName + '.studyArea'));
        System.assert(results.contains(wrapperName + '.curriculum'));

        interest.level = null;
        results = CORE_AOL_ChangeInterest.getInterestMissingParameters(interest);

        System.assertEquals(4, results.size());
        System.assert(results.contains(wrapperName + '.businessUnit'));
        System.assert(results.contains(wrapperName + '.studyArea'));
        System.assert(results.contains(wrapperName + '.curriculum'));
        System.assert(results.contains(wrapperName + '.level'));

        interest.intake = null;
        results = CORE_AOL_ChangeInterest.getInterestMissingParameters(interest);

        System.assertEquals(5, results.size());
        System.assert(results.contains(wrapperName + '.businessUnit'));
        System.assert(results.contains(wrapperName + '.studyArea'));
        System.assert(results.contains(wrapperName + '.curriculum'));
        System.assert(results.contains(wrapperName + '.level'));
        System.assert(results.contains(wrapperName + '.intake'));
    }
}