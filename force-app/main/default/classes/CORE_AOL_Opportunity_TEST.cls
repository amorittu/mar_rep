@isTest
private class CORE_AOL_Opportunity_TEST {

    @testSetup static void createData(){
        Id profileId = [SELECT Id FROM Profile WHERE Name = 'CORE_API_Profile' LIMIT 1].Id;

        User usr = new User(
            Username = 'usertest@aoltest.com',
            LastName = 'TEST_USER',
            Email = 'testemail@yopmail.com',
            Alias = 'T', 
            CommunityNickname = 'T',
            TimeZoneSidKey = 'Europe/Paris',
            LocaleSidKey = 'fr_FR_EURO',
            EmailEncodingKey = 'ISO-8859-1', 
            ProfileId = profileId,
            LanguageLocaleKey = 'fr'      
        );
        database.insert(usr, true);

        Account account = CORE_DataFaker_Account.getStudentAccount('test1');

        CORE_CatalogHierarchyModel catalog = new CORE_DataFaker_CatalogHierarchy('1234').catalogHierarchy;
        Id pricebookId = CORE_DataFaker_PriceBook.getStandardPricebookId();
        PricebookEntry pbe1 = CORE_DataFaker_PriceBook.getPriceBookEntry(catalog.product.Id, pricebookId, 100.0);
        //CORE_PriceBook_BU__c pbu1 = CORE_DataFaker_PriceBook.getPriceBookBu(pricebookId, catalog.businessUnit.Id, '1234');

        CORE_PriceBook_BU__c pricebookBu = CORE_DataFaker_PriceBook.getPriceBookBuWithoutInsert(pricebookId, catalog.businessUnit.Id, '1234');
        pricebookBu.CORE_Academic_Year__c =CORE_AcademicYearProcess.getAcademicYear();

        insert pricebookBu;
    }

    @isTest
    static void testPostFull(){
        
        User usr = [SELECT Id FROM User WHERE Username = 'usertest@aoltest.com' LIMIT 1];

        List<PermissionSetAssignment> psaList = new List<PermissionSetAssignment>();

        for(PermissionSet ps : [SELECT Id FROM PermissionSet WHERE Name = 'CORE_API_AOL' OR Name = 'CORE_LeadFormAcquisition']){
            psaList.add(
                new PermissionSetAssignment(AssigneeId = usr.id, PermissionSetId = ps.Id)
            );
        }
        
        database.insert(psaList, true);

        System.runAs(usr){
            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
            req.requestURI = '/services/apexrest/aol/opportunity';
            req.httpMethod = 'POST';

            RestContext.request = req;
            RestContext.response= res;

            Account account = [SELECT Id FROM Account LIMIT 1];

            CORE_AOL_Wrapper.OpportunityWrapper opp = new CORE_AOL_Wrapper.OpportunityWrapper();

            CORE_AOL_Wrapper.InterestWrapper interest = new CORE_AOL_Wrapper.InterestWrapper(); 
            interest.businessUnit = '1234';
            interest.studyArea = '1234';
            interest.curriculum = '1234';
            interest.level = '1234';
            interest.intake = '1234';   
            interest.isMain = true;
            interest.isAolRetailRegistered = false;
            interest.personAccountId = account.Id;

            CORE_AOL_Wrapper.AOLProgressionFields aolProgressionFields = new CORE_AOL_Wrapper.AOLProgressionFields();
            //aolProgressionFields.

            test.startTest();
            CORE_AOL_Opportunity.opportunity('12345', interest, aolProgressionFields);
            test.stopTest();
            List<Opportunity> opps = [SELECT Id FROM Opportunity];
            System.assertEquals(1,opps.size());
        }
    }

    @isTest
    static void testPostPartial(){
        User usr = [SELECT Id FROM User WHERE Username = 'usertest@aoltest.com' LIMIT 1];

        List<PermissionSetAssignment> psaList = new List<PermissionSetAssignment>();

        for(PermissionSet ps : [SELECT Id FROM PermissionSet WHERE Name = 'CORE_API_AOL' OR Name = 'CORE_LeadFormAcquisition']){
            psaList.add(
                new PermissionSetAssignment(AssigneeId = usr.id, PermissionSetId = ps.Id)
            );
        }
        
        database.insert(psaList, true);

        System.runAs(usr){
            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
            req.requestURI = '/services/apexrest/aol/opportunity';
            req.httpMethod = 'POST';

            RestContext.request = req;
            RestContext.response= res;

            Account account = [SELECT Id FROM Account LIMIT 1];

            CORE_AOL_Wrapper.OpportunityWrapper opp = new CORE_AOL_Wrapper.OpportunityWrapper();

            CORE_AOL_Wrapper.InterestWrapper interest = new CORE_AOL_Wrapper.InterestWrapper(); 
            interest.businessUnit = '1234';
            interest.studyArea = '1234';
            interest.curriculum = '1234';
            interest.level = '1234';
            //interest.intake = '1234';   
            interest.isMain = false;
            interest.isAolRetailRegistered = false;
            interest.personAccountId = account.Id;

            CORE_AOL_Wrapper.AOLProgressionFields aolProgressionFields = new CORE_AOL_Wrapper.AOLProgressionFields();
            //aolProgressionFields.

            test.startTest();
            CORE_AOL_Opportunity.opportunity('12345', interest, aolProgressionFields);
            test.stopTest();
            List<Opportunity> opps = [SELECT Id FROM Opportunity];
            System.assertEquals(1,opps.size());
        }
    }
}