/**
 * Created by ftrapani on 1/5/2022.
 */

public without sharing class IT_QueueManageSubStatusTSH implements Queueable {
    List<Opportunity> newList;
    Map<Id, Opportunity> oldMap;

    public IT_QueueManageSubStatusTSH(String newListString, String oldMapString) {
        newList = (List<Opportunity>) JSON.deserialize(newListString, List<Opportunity>.class);
        oldMap = (Map<Id, Opportunity>) JSON.deserialize(oldMapString, Map<Id, Opportunity>.class);
    }

    public void execute(QueueableContext context) {
        List<CORE_Sub_Status_Timestamp_History__c> subStatusHistoriesToInsert = new List<CORE_Sub_Status_Timestamp_History__c>();
        Id enrollmentRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('CORE_Enrollment').getRecordTypeId();
        Id reRegistrationRecordTypeId = Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get('CORE_Re_registration').getRecordTypeId();
        Map<Id, List<String>> picklistValuesByRecordTypeMap = new Map<Id, List<String>>();
        Map<String, IT_OpportunitySubStatusOrder__mdt> ordersByStatus = new Map<String, IT_OpportunitySubStatusOrder__mdt>();
        Map<String, List<IT_OpportunitySubStatusOrder__mdt>> subStatusByStatus = new Map<String, List<IT_OpportunitySubStatusOrder__mdt>>();
        Map<Decimal, String> statusByOrder = new Map<Decimal, String>();

        //populating map with sub-status values by recordTypeId
        if(Test.isRunningTest()){
            picklistValuesByRecordTypeMap.put(enrollmentRecordTypeId, new List<String>());
            picklistValuesByRecordTypeMap.put(reRegistrationRecordTypeId, new List<String>());
        }else {
            picklistValuesByRecordTypeMap.put(enrollmentRecordTypeId, IT_Utility.getPicklistValuesByRecordType(enrollmentRecordTypeId, 'Opportunity', 'CORE_OPP_Sub_Status__c'));
            picklistValuesByRecordTypeMap.put(reRegistrationRecordTypeId, IT_Utility.getPicklistValuesByRecordType(reRegistrationRecordTypeId, 'Opportunity', 'CORE_OPP_Sub_Status__c'));
        }
        List<IT_OpportunitySubStatusOrder__mdt> mdtList = new List<IT_OpportunitySubStatusOrder__mdt>();

        //querying through all the metadata records to know the order for both Status and Sub-Status picklists
        mdtList = [
                SELECT Id, IT_SubStatus__c, IT_OrderNumber__c, IT_OpportunityStatusOrder__c,
                        IT_OpportunityStatusOrder__r.Label, IT_OpportunityStatusOrder__r.IT_OrderNumber__c
                FROM IT_OpportunitySubStatusOrder__mdt
                ORDER BY IT_OrderNumber__c ASC
        ];

        //populating all the maps required to execute the logic
        for (IT_OpportunitySubStatusOrder__mdt subStatusOrder : mdtList) {
            ordersByStatus.put(subStatusOrder.IT_SubStatus__c, subStatusOrder);
            statusByOrder.put(subStatusOrder.IT_OpportunityStatusOrder__r.IT_OrderNumber__c, subStatusOrder.IT_OpportunityStatusOrder__r.Label);
            List<IT_OpportunitySubStatusOrder__mdt> subStatusList = subStatusByStatus.get(subStatusOrder.IT_OpportunityStatusOrder__r.Label) != null ? subStatusByStatus.get(subStatusOrder.IT_OpportunityStatusOrder__r.Label) : new List<IT_OpportunitySubStatusOrder__mdt>();
            subStatusList.add(subStatusOrder);
            subStatusByStatus.put(subStatusOrder.IT_OpportunityStatusOrder__r.Label, subStatusList);
        }

        //System.debug(CLASS_NAME + 'manageSubStatusTimestampHistoryAsync::statusByOrder ' + statusByOrder);
        for (Opportunity opty : newList) {
            Opportunity oldOpty = oldMap?.get(opty.Id);
            if (oldOpty != null) {
                List<String> picklistValues = picklistValuesByRecordTypeMap.get(opty.RecordTypeId);

                IT_OpportunitySubStatusOrder__mdt oldStatusSubStatusOrder = ordersByStatus.get(oldOpty.CORE_OPP_Sub_Status__c);
                IT_OpportunitySubStatusOrder__mdt statusSubStatusOrder = ordersByStatus.get(opty.CORE_OPP_Sub_Status__c);
                Decimal diffStatus = statusSubStatusOrder.IT_OpportunityStatusOrder__r.IT_OrderNumber__c - oldStatusSubStatusOrder.IT_OpportunityStatusOrder__r.IT_OrderNumber__c;
                if(diffStatus == 0){
                    //this logic is executed only if the Sub-Status change happen in the same Status
                    Decimal oldOrderSubStatus = oldStatusSubStatusOrder.IT_OrderNumber__c;
                    Decimal orderSubStatus = statusSubStatusOrder.IT_OrderNumber__c;
                    Map<Decimal, String> subStatusByOrderMap = generateSubStatusByOrderMap(
                            subStatusByStatus,
                            opty.StageName
                    );

                    System.debug('test');
                    subStatusHistoriesToInsert.addAll(createSubStatusHistoriesRecord(
                            orderSubStatus,
                            oldOrderSubStatus,
                            subStatusByOrderMap,
                            picklistValues,
                            opty,
                            oldOpty,
                            null
                    ));
                }else{
                    //this logic is executed only if the Status change
                    for(Integer i = 0; i <= diffStatus; i++){
                        //tmpStatus contains the Status (from the old selected to the new one)
                        String tmpStatus = statusByOrder.get(oldStatusSubStatusOrder.IT_OpportunityStatusOrder__r.IT_OrderNumber__c + i);

                        //System.debug(CLASS_NAME + 'manageSubStatusTimestampHistoryAsync::key - tmpStatus ' + (oldStatusSubStatusOrder.IT_OpportunityStatusOrder__r.IT_OrderNumber__c + i) + ' - ' + tmpStatus);
                        if(tmpStatus == null || (tmpStatus != opty.StageName && tmpStatus != oldOpty.StageName && tmpStatus == 'Closed Won')){
                            continue;
                        }

                        Map<Decimal, String> subStatusByOrderMap = generateSubStatusByOrderMap(
                                subStatusByStatus,
                                tmpStatus
                        );

                        List<Decimal> orderSubStatusKey = new List<Decimal>(subStatusByOrderMap.keySet());
                        Integer orderSubStatusKeySize = orderSubStatusKey.size();
                        Decimal oldOrderSubStatus = i == 0 ? oldStatusSubStatusOrder.IT_OrderNumber__c : orderSubStatusKey[0];
                        Decimal orderSubStatus = i == diffStatus ? statusSubStatusOrder.IT_OrderNumber__c : orderSubStatusKey.get(orderSubStatusKeySize - 1);

                        subStatusHistoriesToInsert.addAll(createSubStatusHistoriesRecord(
                                orderSubStatus,
                                oldOrderSubStatus,
                                subStatusByOrderMap,
                                picklistValues,
                                opty,
                                oldOpty,
                                tmpStatus
                        ));
                    }
                }

            } else {
                CORE_Sub_Status_Timestamp_History__c subStatusTimestampHistory = new CORE_Sub_Status_Timestamp_History__c();
                subStatusTimestampHistory.CORE_Opportunity__c = opty.Id;
                subStatusTimestampHistory.CORE_Status__c = opty.StageName;
                subStatusTimestampHistory.CORE_Sub_status__c = opty.CORE_OPP_Sub_Status__c;
                subStatusTimestampHistory.CORE_Modification_Sub_status_Date_time__c = Datetime.now();
                subStatusHistoriesToInsert.add(subStatusTimestampHistory);
            }
        }
        //System.debug(CLASS_NAME + '::manageSubStatusTimestampHistoryAsync::subStatusHistoriesToInsert:: + subStatusHistoriesToInsert);
        insert subStatusHistoriesToInsert;

    }

    public static Map<Decimal, String> generateSubStatusByOrderMap(Map<String, List<IT_OpportunitySubStatusOrder__mdt>> subStatusByStatus, String status){
        Map<Decimal, String> subStatusByOrderMap = new Map<Decimal, String>();
        for (IT_OpportunitySubStatusOrder__mdt itOpportunitySubStatusOrder : subStatusByStatus.get(status)) {
            subStatusByOrderMap.put(itOpportunitySubStatusOrder.IT_OrderNumber__c, itOpportunitySubStatusOrder.IT_SubStatus__c);
        }

        return subStatusByOrderMap;
    }

    public static List<CORE_Sub_Status_Timestamp_History__c> createSubStatusHistoriesRecord(
            Decimal orderNumberSubStatus,
            Decimal oldOrderNumberSubStatus,
            Map<Decimal, String> subStatusByOrderMap,
            List<String> picklistValues,
            Opportunity opty,
            Opportunity oldOpty,
            String betweenStatus
    ) {
        List<CORE_Sub_Status_Timestamp_History__c> subStatusHistoriesToInsert = new List<CORE_Sub_Status_Timestamp_History__c>();
        Decimal diffSubStatus = orderNumberSubStatus - oldOrderNumberSubStatus;
        for (Integer i = 1; i <= diffSubStatus; i++) {
            String betweenSubStatus = subStatusByOrderMap.get(oldOrderNumberSubStatus + i);
            if (betweenSubStatus == null) {
                System.debug('betweenSubStatus null');
                continue;
            }
            if (!Test.isRunningTest() && !picklistValues.contains(betweenSubStatus)) {
                System.debug('!picklistValues.contains(betweenSubStatus)');
                continue;
            }
            CORE_Sub_Status_Timestamp_History__c subStatusTimestampHistory = new CORE_Sub_Status_Timestamp_History__c();
            subStatusTimestampHistory.CORE_Opportunity__c = opty.Id;
            subStatusTimestampHistory.CORE_Status__c = betweenStatus != null ? betweenStatus : opty.StageName;
            subStatusTimestampHistory.CORE_Sub_status__c = betweenSubStatus;
            subStatusTimestampHistory.CORE_Modification_Sub_status_Date_time__c = Datetime.now();
            subStatusTimestampHistory.IT_NotSelected__c = betweenSubStatus != oldOpty.CORE_OPP_Sub_Status__c && betweenSubStatus != opty.CORE_OPP_Sub_Status__c;
            subStatusHistoriesToInsert.add(subStatusTimestampHistory);
        }

        return subStatusHistoriesToInsert;
    }

}