@RestResource(urlMapping='/aol/academic-diploma-history')
global with sharing class CORE_AOL_AcademicDiplomaHistory {
    private static final String SUCCESS_MESSAGE = 'Academic diploma history & progression fields updated with success';

    @HttpPost
    global static CORE_AOL_Wrapper.ResultWrapperGlobal execute() {

        RestRequest	request = RestContext.request;

        // Initiating logs
        Long dt1 = DateTime.now().getTime();
        Long dt2;
        CORE_LogManager logSettings;
        CORE_LOG__c log = new CORE_LOG__c(CORE_API_Name__c = 'AOL');
        Boolean isErrorLog = false;
        logSettings = new CORE_LogManager('AOL');

        Map<String, Object> paramsMap = (Map<String, Object>)JSON.deserializeUntyped(request.requestBody.toString());
        String aolExternalId = (String)paramsMap.get(CORE_AOL_Constants.PARAM_AOL_EXTERNALID);
        List<CORE_AOL_Wrapper.AcademicDiplomaHistory> academicDiplomaHistory = (List<CORE_AOL_Wrapper.AcademicDiplomaHistory>)JSON.deserialize((String)JSON.serialize(paramsMap.get(CORE_AOL_Constants.PARAM_ACADEMIC_DIPLOMA_HISTORY)), List<CORE_AOL_Wrapper.AcademicDiplomaHistory>.class);
        CORE_AOL_Wrapper.AOLProgressionFields aolProgressionFields = (CORE_AOL_Wrapper.AOLProgressionFields)JSON.deserialize((String)JSON.serialize(paramsMap.get(CORE_AOL_Constants.PARAM_AOL_PROG_FIELDS)), CORE_AOL_Wrapper.AOLProgressionFields.class);

        CORE_AOL_Wrapper.ResultWrapperGlobal result = checkMandatoryParameters(aolExternalId,academicDiplomaHistory);
        
        try{
            if(result != null){  //section moved inside Try 02/06/2022 by GH
                //missing parameter(s)
                return result;
            }
            CORE_AOL_GlobalWorker aolWorker = new CORE_AOL_GlobalWorker(aolProgressionFields,aolExternalId, paramsMap);
            Opportunity opportunity = aolWorker.aolOpportunity;

            if(opportunity == null){
                result = new CORE_AOL_Wrapper.ResultWrapperGlobal(
                    CORE_AOL_Constants.ERROR_STATUS, 
                    String.format(CORE_AOL_Constants.MSG_OPPORTUNITY_NOT_FOUND, new List<String>{aolExternalId})
                );
            } else {
                academicDiplomaHistoryProcess(aolWorker,academicDiplomaHistory);

                aolWorker.updateOppIsNeeded = true;
                aolWorker.updateAolProgressionFields();

                result = new CORE_AOL_Wrapper.ResultWrapperGlobal(CORE_AOL_Constants.SUCCESS_STATUS, SUCCESS_MESSAGE);
            }
            
        } catch(Exception e){
            result = new CORE_AOL_Wrapper.ResultWrapperGlobal(CORE_AOL_Constants.ERROR_STATUS, e.getMessage() + ' at ' + e.getStackTraceString());
        } finally{
            dt2 = DateTime.now().getTime();
            System.debug('CORE_AOL_AcademicDiplomaHistory(urlMapping=\'/aol/academic-diploma-history\') : T0TAL TIME OF EXECUTION = ' + (dt2-dt1) + ' ms');
            // creating log
            log.CORE_Processing_Duration__c =(dt2-dt1);
            log.CORE_HTTP_Status_Code__c = result.status == 'KO' ? 400 : result.status == 'OK' ? 200 : 500;
            log.CORE_Sent_Body__c = JSON.serialize(result);
            log.CORE_Received_Body__c =  request.requestBody.toString();
            
            if(!logSettings.isNone()){
                if(logSettings.isAllLogged()){ // is All logged
                    CORE_LogManager.createLog(log);
                }else if(logSettings.isErrorLogged() && isErrorLog){ // is Error
                    CORE_LogManager.createLog(log);
                }
            }
        }

        return result;
    }

    public static void academicDiplomaHistoryProcess(CORE_AOL_GlobalWorker aolWorker, List<CORE_AOL_Wrapper.AcademicDiplomaHistory> academicDiplomaHistory){

        List<CORE_Academic_Diploma_History__c> academicDiplomaHistoryToUpsert = new List<CORE_Academic_Diploma_History__c>();
        Map<String, Object> diplomaAdditionFieldsMap = new Map<String, Object>();

        if(aolWorker.paramsMap != null
            && aolWorker.paramsMap.containsKey(CORE_AOL_Constants.PARAM_ACADEMIC_DIPLOMA_HISTORY)){
            List<Object> aolDiplomaList = (List<Object>)JSON.deserializeUntyped((String)JSON.serialize(aolWorker.paramsMap.get(CORE_AOL_Constants.PARAM_ACADEMIC_DIPLOMA_HISTORY)));

            for(Object diploma : aolDiplomaList){
                Map<String, Object> diplomaMap = (Map<String, Object>)JSON.deserializeUntyped((String)JSON.serialize(diploma));

                if(diplomaMap.get('diplomaExternalId') != null
                    && diplomaMap.containsKey(CORE_AOL_Constants.PARAM_ADDITIONAL_FIELDS)){

                    diplomaAdditionFieldsMap.put((String)diplomaMap.get('diplomaExternalId'), diplomaMap.get(CORE_AOL_Constants.PARAM_ADDITIONAL_FIELDS));
                }
            }
        }
		/* getting the recordTypes*/
        Set<String> recordTypeNames = new Set<String>();
        for(CORE_AOL_Wrapper.AcademicDiplomaHistory myAcademicDiplomaHistory : academicDiplomaHistory){
            if (!recordTypeNames.contains(myAcademicDiplomaHistory.diplomaRecordType)){
                recordTypeNames.add(myAcademicDiplomaHistory.diplomaRecordType);
            }  
        }
        List<RecordType> recordTypes =  [SELECT Id,Name,DeveloperName FROM RecordType where DeveloperName in :recordTypeNames AND SobjectType = 'CORE_Academic_Diploma_History__c' ];
        Map<String,Id> mapDeveloperNameToId =  new Map<String,Id>();
        for (RecordType rt : recordTypes){
            mapDeveloperNameToId.put(rt.DeveloperName,rt.Id);
        }

        for(CORE_AOL_Wrapper.AcademicDiplomaHistory myAcademicDiplomaHistory : academicDiplomaHistory){
            CORE_Academic_Diploma_History__c currentAcademicDiplomaHistory = new CORE_Academic_Diploma_History__c();
            currentAcademicDiplomaHistory.CORE_Diploma_ExternalId__c = myAcademicDiplomaHistory.diplomaExternalId;
            currentAcademicDiplomaHistory.Name = myAcademicDiplomaHistory.diplomaName;
            currentAcademicDiplomaHistory.CORE_Diploma_level__c = myAcademicDiplomaHistory.diplomaLevel;
            currentAcademicDiplomaHistory.CORE_Diploma_school__c = myAcademicDiplomaHistory.diplomaSchool;
            currentAcademicDiplomaHistory.CORE_Diploma_city__c = myAcademicDiplomaHistory.diplomaCity;
            currentAcademicDiplomaHistory.CORE_Diploma_country__c = myAcademicDiplomaHistory.diplomaCountry;
            currentAcademicDiplomaHistory.CORE_Diploma_status__c = myAcademicDiplomaHistory.diplomaStatus;
            currentAcademicDiplomaHistory.CORE_Expected_graduation_Date__c = myAcademicDiplomaHistory.diplomaExpectedGraduationDate;
            currentAcademicDiplomaHistory.CORE_Last_Modification_date_from_AOL__c = myAcademicDiplomaHistory.diplomaModificationDate;
            currentAcademicDiplomaHistory.CORE_Person_Account_Id__c = aolWorker.getRelatedAccount().Id;
            currentAcademicDiplomaHistory.CORE_Diploma_grade__c = myAcademicDiplomaHistory.diplomaGrade;
            currentAcademicDiplomaHistory.CORE_Diploma_date__c = myAcademicDiplomaHistory.diplomaDate;
            currentAcademicDiplomaHistory.CORE_Diploma_study_area__c  = myAcademicDiplomaHistory.diplomaStudyArea;

            String rtype = myAcademicDiplomaHistory.diplomaRecordType;
			if(rtype != null && rtype != '' && mapDeveloperNameToId.containsKey(rtype)) currentAcademicDiplomaHistory.RecordTypeId =  mapDeveloperNameToId.get(rtype);
            //set Additional fields to update
            if(diplomaAdditionFieldsMap.containsKey(myAcademicDiplomaHistory.diplomaExternalId)){
                JSONGenerator jsonReq = JSON.createGenerator(true);
                jsonReq.writeStartObject();

                jsonReq.writeObjectField(CORE_AOL_Constants.PARAM_ADDITIONAL_FIELDS, diplomaAdditionFieldsMap.get(myAcademicDiplomaHistory.diplomaExternalId));

                jsonReq.writeEndObject();

                currentAcademicDiplomaHistory = (CORE_Academic_Diploma_History__c)CORE_AOL_GlobalWorker.setAdditionalFields(currentAcademicDiplomaHistory, jsonReq.getAsString());
            }

            academicDiplomaHistoryToUpsert.add(currentAcademicDiplomaHistory);
        }

        if(academicDiplomaHistoryToUpsert.size() > 0){
            upsert academicDiplomaHistoryToUpsert CORE_Diploma_ExternalId__c;
        }
    }


    @TestVisible
    private static CORE_AOL_Wrapper.ResultWrapperGlobal checkMandatoryParameters(String aolExternalId, List<CORE_AOL_Wrapper.AcademicDiplomaHistory> academicDiplomaHistory){
        List<String> missingParameters = new List<String>();
        Boolean isMissingAolId = CORE_AOL_GlobalWorker.isAolExternalIdParameterMissing(aolExternalId);

        if(isMissingAolId){
            missingParameters.add(CORE_AOL_Constants.PARAM_AOL_EXTERNALID);
        }
        
        for(Integer i = 0 ; i < academicDiplomaHistory.size() ; i++){
            missingParameters.addAll(getAcademicDiplomaHistoryMissingParameters(academicDiplomaHistory.get(i),i));
        }

        return CORE_AOL_GlobalWorker.getMissingParameters(missingParameters);
    }

    private static List<String> getAcademicDiplomaHistoryMissingParameters(CORE_AOL_Wrapper.AcademicDiplomaHistory academicDiplomaHistory, Integer iteration){
        List<String> mandatoryParameters = new List<String> {'diplomaExternalId', 'diplomaName'};
        
        List<String> missingParameters = new List<String>();

        for(String mandatoryParameter : mandatoryParameters){
            if(String.IsBlank((String) academicDiplomaHistory.get(mandatoryParameter))){
                missingParameters.add(String.format('{0}[{1}].{2}', new List<Object>{CORE_AOL_Constants.PARAM_ACADEMIC_DIPLOMA_HISTORY, iteration, mandatoryParameter}));
            }
        }
        
        return missingParameters;
    }
}