@RestResource(urlMapping='/sis/deletedRecords')
global class CORE_SIS_DeletedRecords {

    public static Integer limitPerPage = 2000;

    @HttpGet
    global static CORE_SIS_Wrapper.ResultWrapperDeletedRecords getDeletedRecords(){

        String urlCallback = '';
        RestRequest	request = RestContext.request;

        // Initiating logs
        Long dt1 = DateTime.now().getTime();
        Long dt2;
        CORE_LogManager logSettings;
        CORE_LOG__c log = new CORE_LOG__c(CORE_API_Name__c = 'SIS');
        Boolean isErrorLog = false;
        logSettings = new CORE_LogManager('SIS');

        String lastModifiedDate = request.params.get(CORE_SIS_Constants.PARAM_LASTMODIFIEDDATE);

        String recTypeName = request.params.get(CORE_SIS_Constants.PARAM_RECTYPE);
        String lastId = request.params.get(CORE_SIS_Constants.PARAM_LAST_ID);

        CORE_SIS_Wrapper.ResultWrapperDeletedRecords result = checkMandatoryParameters(lastModifiedDate, recTypeName);

        try{
            if(result != null){
                return result;
            }
            List<CORE_Deleted_record__c> deletedRecordsList = CORE_SIS_QueryProvider.getDeletedRecords(lastModifiedDate, recTypeName, lastId, limitPerPage);

            if(deletedRecordsList.size() == limitPerPage){
                urlCallback = URL.getOrgDomainUrl()+'/services/apexrest/sis/deletedRecords?'
                            + CORE_SIS_Constants.PARAM_LASTMODIFIEDDATE + '=' + lastModifiedDate 
                            + '&' + CORE_SIS_Constants.PARAM_RECTYPE + '=' + recTypeName 
                            + '&' + CORE_SIS_Constants.PARAM_LAST_ID + '=' + deletedRecordsList.get(deletedRecordsList.size() - 1).Id;
            }

            result = new CORE_SIS_Wrapper.ResultWrapperDeletedRecords(deletedRecordsList.size(), urlCallback, deletedRecordsList, CORE_SIS_Constants.SUCCESS_STATUS);

        } catch(Exception e){
            system.debug('@getOpportunities > exception error: ' + e.getMessage() + ' at ' + e.getStackTraceString());
            result = new CORE_SIS_Wrapper.ResultWrapperDeletedRecords(CORE_SIS_Constants.ERROR_STATUS, e.getMessage() + ' at ' + e.getStackTraceString());
        } finally{
            dt2 = DateTime.now().getTime();
            System.debug('CORE_AOL_AcademicDiplomaHistory(urlMapping=\'/aol/academic-diploma-history\') : T0TAL TIME OF EXECUTION = ' + (dt2-dt1) + ' ms');
            // creating log
            log.CORE_Processing_Duration__c =(dt2-dt1);
            log.CORE_HTTP_Status_Code__c = result.status == 'KO' ? 400 : result.status == 'OK' ? 200 : 500;
            log.CORE_Sent_Body__c = JSON.serialize(result);
            log.CORE_Received_Body__c =  'params : ' + request.params.toString();
            
            if(!logSettings.isNone()){
                if(logSettings.isAllLogged()){ // is All logged
                    CORE_LogManager.createLog(log);
                }else if(logSettings.isErrorLogged() && isErrorLog){ // is Error
                    CORE_LogManager.createLog(log);
                }
            }
        }

        return result;
    }

    private static CORE_SIS_Wrapper.ResultWrapperDeletedRecords checkMandatoryParameters(String lastModifiedDate, String recordType){
        List<String> missingParameters = new List<String>();
        Boolean isMissingLastModifiedDate = CORE_SIS_GlobalWorker.isParameterMissing(lastModifiedDate);
        Boolean isMissingRecordType = CORE_SIS_GlobalWorker.isParameterMissing(recordType);

        if(isMissingLastModifiedDate){
            missingParameters.add(CORE_SIS_Constants.PARAM_LASTMODIFIEDDATE);
        }

        if(isMissingRecordType){
            missingParameters.add(CORE_SIS_Constants.PARAM_RECTYPE);
        }
        
        return getMissingParameters(missingParameters);
    }

    public static CORE_SIS_Wrapper.ResultWrapperDeletedRecords getMissingParameters(List<String> missingParameters){
        String errorMsg = CORE_SIS_Constants.MSG_MISSING_PARAMETERS;

        if(missingParameters.size() > 0){
            errorMsg += missingParameters;

            return new CORE_SIS_Wrapper.ResultWrapperDeletedRecords(CORE_AOL_Constants.ERROR_STATUS, errorMsg);
        }
        
        return null;
    }
}