@IsTest
public with sharing class CORE_AsyncLFABatchTest {
    @IsTest
    public static void CORE_AsyncLFABatch_Should_execute_async_process() {
        List<CORE_CatalogHierarchyModel> catalogs = new List<CORE_CatalogHierarchyModel>();

        List<CORE_DataFaker_CatalogHierarchy> fakeCatalogs =  CORE_DataFaker_CatalogHierarchy.createMultipleCatalogs('uniqueKey', 50);
        for(CORE_DataFaker_CatalogHierarchy catalog : fakeCatalogs){
            catalogs.add(catalog.catalogHierarchy);
        }

        CORE_TriggerUtils.setBypassTrigger();

        List<CORE_LeadAcquisitionBuffer__c> buffers = new List<CORE_LeadAcquisitionBuffer__c>();
        for(Integer i = 0 ; i <10 ; i++){ //EScudo, 06/09/2022, reduced to 10
            String uniqueKey = 'uniqueKey' + i;
            String num = String.valueOf(i).leftPad(3, '0');
            String mobile = '+33600000' + num;
            String phone = '+33200000' + num;
            String email = 'testEmail' + i + '@test.com';
            Integer randomNumber = Integer.valueof((Math.random() * catalogs.size()));
            CORE_LeadAcquisitionBuffer__c buffer = CORE_DataFaker_LeadAcquisitionBuffer.getBufferWhithoutInsert(uniqueKey, mobile, phone, email, catalogs.get(randomNumber));
            buffers.add(buffer);
        }

        insert buffers;
        Set<Id> bufferIds = (new Map<Id, CORE_LeadAcquisitionBuffer__c> (buffers)).keySet();
        CORE_TriggerUtils.setBypassTrigger();

        List<Account> accounts = [Select Id from Account];
        List<Opportunity> opportunities = [Select Id from Opportunity];

        System.assertEquals(0, accounts.size());
        System.assertEquals(0, opportunities.size());

        Test.startTest();
        CORE_AsyncLFABatch lfaProcessBatch = new CORE_AsyncLFABatch(bufferIds,CORE_AsyncLFABatch.BUFFER_TYPE.STANDARD);
        Database.executeBatch(lfaProcessBatch,10);//EScudo, 06/09/2022, reduced to 10
        Test.stopTest();

        accounts = [Select Id from Account];
        opportunities = [Select Id from Opportunity];
        List<CORE_LeadAcquisitionBuffer__c> buffersAfterUpdate = [Select Id, CORE_Status__c, CORE_ErrorDescription__c FROM CORE_LeadAcquisitionBuffer__c LIMIT 5];
        System.assertEquals(bufferIds.size(), accounts.size());
        System.assertEquals(bufferIds.size(), opportunities.size());
    }
}