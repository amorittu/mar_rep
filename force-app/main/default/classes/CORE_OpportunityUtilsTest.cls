@IsTest
public class CORE_OpportunityUtilsTest {
    @IsTest
    public static void getShortQuestionForOpportunity_should_trunc_question_on_opportunity_if_length_is_greater_than_255() {
        String question = '255charaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa';
        String questionTooLong = '256charaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa';
        String result = CORE_OpportunityUtils.getShortQuestionForOpportunity(question);
        System.assertEquals(question.length(), result.length(), 'Question should not be trunc');
        result = CORE_OpportunityUtils.getShortQuestionForOpportunity(questionTooLong);
        system.assertNotEquals(questionTooLong.length(), result.length(), 'Question should be trunc');
        system.assert(result.contains(Label.CORE_LFA_TooLongQuestionMsg), 'Question should contain additional test');
    }

    @IsTest
    public static void getCreationStatus_Should_return_Full_status_When_hierarchy_is_full() {
        CORE_OpportunityMetadata metadata = new CORE_OpportunityMetadata();
        Test.startTest();
        boolean isFullHierarchy = true;
        boolean isSuspect = false;
        String result = CORE_OpportunityUtils.getCreationStatus(isFullHierarchy, isSuspect, metadata, false);
        Test.stopTest();

        String expectedResult = metadata.statusFull;
        System.assertEquals(expectedResult, result, 'Status returned should be full status');
    }

    @IsTest
    public static void getCreationStatus_Should_return_Partial_status_When_hierarchy_is_partial() {
        CORE_OpportunityMetadata metadata = new CORE_OpportunityMetadata();
        Test.startTest();
        boolean isFullHierarchy = false;
        boolean isSuspect = false;
        String result = CORE_OpportunityUtils.getCreationStatus(isFullHierarchy, isSuspect, metadata, false);
        Test.stopTest();

        String expectedResult = metadata.statusPartial;
        System.assertEquals(expectedResult, result, 'Status returned should be partial status');
    }

    @IsTest
    public static void getCreationStatus_Should_return_Suspect_status_When_hierarchy_is_suspect() {
        CORE_OpportunityMetadata metadata = new CORE_OpportunityMetadata();
        Test.startTest();
        boolean isFullHierarchy = false;
        boolean isSuspect = true;
        String result = CORE_OpportunityUtils.getCreationStatus(isFullHierarchy, isSuspect, metadata, false);
        Test.stopTest();

        String expectedResult = metadata.statusSuspect;
        System.assertEquals(expectedResult, result, 'Status returned should be suspect status');
    }

    @IsTest
    public static void getCreationSubStatus_Should_return_Full_subStatus_When_hierarchy_is_full() {
        CORE_OpportunityMetadata metadata = new CORE_OpportunityMetadata();
        Test.startTest();
        boolean isFullHierarchy = true;
        boolean isSuspect = false;
        String result = CORE_OpportunityUtils.getCreationSubStatus(isFullHierarchy, isSuspect, metadata, false);
        Test.stopTest();

        String expectedResult = metadata.subStatusFull;
        System.assertEquals(expectedResult, result, 'SubStatus returned should be full status');
    }

    @IsTest
    public static void getCreationSubStatus_Should_return_Partial_subStatus_When_hierarchy_is_partial() {
        CORE_OpportunityMetadata metadata = new CORE_OpportunityMetadata();
        Test.startTest();
        boolean isFullHierarchy = false;
        boolean isSuspect = false;
        String result = CORE_OpportunityUtils.getCreationSubStatus(isFullHierarchy, isSuspect, metadata, false);
        Test.stopTest();

        String expectedResult = metadata.subStatusPartial;
        System.assertEquals(expectedResult, result, 'SubStatus returned should be partial status');
    }

    @IsTest
    public static void getCreationSubStatus_Should_return_Suspect_subStatus_When_hierarchy_is_suspect() {
        CORE_OpportunityMetadata metadata = new CORE_OpportunityMetadata();
        Test.startTest();
        boolean isFullHierarchy = false;
        boolean isSuspect = true;
        String result = CORE_OpportunityUtils.getCreationSubStatus(isFullHierarchy, isSuspect, metadata, false);
        Test.stopTest();

        String expectedResult = metadata.subStatusSuspect;
        System.assertEquals(expectedResult, result, 'SubStatus returned should be suspect status');
    }

    @IsTest
    public static void isExistingMainOpportunity_Should_return_true_when_main_opportunity_exist_on_same_business_unit() {
        String uniqueKey = 'test1';
        CORE_DataFaker_CatalogHierarchy catalogHierarchy = new CORE_DataFaker_CatalogHierarchy(uniqueKey);
        Account account = CORE_DataFaker_Account.getStudentAccount(uniqueKey);

        Opportunity opportunity = CORE_DataFaker_Opportunity.getOpportunity(catalogHierarchy.division.Id, 
            catalogHierarchy.businessUnit.Id, 
            catalogHierarchy.studyArea.Id,
            catalogHierarchy.curriculum.Id,
            catalogHierarchy.level.Id,
            catalogHierarchy.intake.Id, 
            account.Id, 
            'Lead', 
            'Lead - New'
        );

        Test.startTest();
        Boolean result = CORE_OpportunityUtils.isExistingMainOpportunity(catalogHierarchy.businessUnit.Id,account.Id, opportunity.CORE_OPP_Academic_Year__c);
        Test.stopTest();

        Boolean expectedResult = true;
        System.assertEquals(expectedResult, result, 'Boolean value should be true');
    }

    @IsTest
    public static void isExistingMainOpportunity_Should_return_false_when_there_is_no_existing_opportunity_on_same_business_unit() {
        String uniqueKey = 'test1';
        CORE_DataFaker_CatalogHierarchy catalogHierarchy = new CORE_DataFaker_CatalogHierarchy(uniqueKey);
        CORE_DataFaker_CatalogHierarchy catalogHierarchy2 = new CORE_DataFaker_CatalogHierarchy('test2');
        Account account = CORE_DataFaker_Account.getStudentAccount(uniqueKey);

        Opportunity opportunity = CORE_DataFaker_Opportunity.getOpportunity(catalogHierarchy.division.Id, 
            catalogHierarchy.businessUnit.Id, 
            catalogHierarchy.studyArea.Id,
            catalogHierarchy.curriculum.Id,
            catalogHierarchy.level.Id,
            catalogHierarchy.intake.Id, 
            account.Id, 
            'Lead', 
            'Lead - New'
        );
        
        Test.startTest();
        Boolean result = CORE_OpportunityUtils.isExistingMainOpportunity(catalogHierarchy2.businessUnit.Id,account.Id, opportunity.CORE_OPP_Academic_Year__c);
        Test.stopTest();

        Boolean expectedResult = false;
        System.assertEquals(expectedResult, result, 'Boolean value should be false');
    }

    @IsTest
    public static void isExistingMainOpportunity_Should_return_false_when_there_is_no_existing_opportunity_on_same_account() {
        String uniqueKey = 'test1';
        CORE_DataFaker_CatalogHierarchy catalogHierarchy = new CORE_DataFaker_CatalogHierarchy(uniqueKey);
        Account account = CORE_DataFaker_Account.getStudentAccount(uniqueKey);

        Test.startTest();
        Boolean result = CORE_OpportunityUtils.isExistingMainOpportunity(catalogHierarchy.businessUnit.Id,account.Id, 'CORE_PKL_2023-2024');
        Test.stopTest();

        Boolean expectedResult = false;
        System.assertEquals(expectedResult, result, 'Boolean value should be false');
    }



    @IsTest
    public static void getAcademicYear_Should_return_academicYear_when_academicYear_parameter_is_not_blank() {
        List<String> academicYears = CORE_PicklistUtils.getPicklistValues('Opportunity', 'CORE_OPP_Academic_Year__c');
        Datetime dtNow = System.now();
        String result = CORE_OpportunityUtils.getAcademicYear(null,'test',null,dtNow,academicYears);
        System.assertEquals('test', result);
    }

    @IsTest
    public static void getAcademicYear_Should_return_intake_academicYear_when_exist() {
        List<String> academicYears = CORE_PicklistUtils.getPicklistValues('Opportunity', 'CORE_OPP_Academic_Year__c');
        Datetime dtNow = System.now();
        CORE_DataFaker_CatalogHierarchy catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test');
        CORE_CatalogHierarchyModel catalogHierarchyModel = catalogHierarchy.catalogHierarchy;
        String intakeAcademicYear =  catalogHierarchyModel.intake.CORE_Academic_Year__c;

        String result = CORE_OpportunityUtils.getAcademicYear(null,'test',catalogHierarchyModel,dtNow,academicYears);
        
        System.assertEquals(intakeAcademicYear, result);
    }

    @IsTest
    public static void getAcademicYear_Should_return_current_academicYear_when_intake_and_division_and_input_academic_year_and_default_academicYear_are_not_provided() {
        List<String> academicYears = CORE_PicklistUtils.getPicklistValues('Opportunity', 'CORE_OPP_Academic_Year__c');
        Datetime dtNow = System.now();
        CORE_DataFaker_CatalogHierarchy catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test');
        CORE_CatalogHierarchyModel catalogHierarchyModel = catalogHierarchy.catalogHierarchy;

        catalogHierarchyModel.intake.CORE_Academic_Year__c = null;
        catalogHierarchyModel.division.CORE_Default_Academic_Year__c = null;
        String result = CORE_OpportunityUtils.getAcademicYear(null,null,catalogHierarchyModel,dtNow,academicYears);
        System.assert(result.contains(String.valueOf(dtNow.year())));
    }

    @IsTest
    public static void getAcademicYear_Should_return_default_academicYear_when_intake_and_division_and_input_academic_year_are_not_provided() {
        List<String> academicYears = CORE_PicklistUtils.getPicklistValues('Opportunity', 'CORE_OPP_Academic_Year__c');
        Datetime dtNow = System.now();
        CORE_DataFaker_CatalogHierarchy catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test');
        CORE_CatalogHierarchyModel catalogHierarchyModel = catalogHierarchy.catalogHierarchy;

        catalogHierarchyModel.intake.CORE_Academic_Year__c = null;
        catalogHierarchyModel.division.CORE_Default_Academic_Year__c = null;
        String result = CORE_OpportunityUtils.getAcademicYear('test',null,catalogHierarchyModel,dtNow,academicYears);
        System.assertEquals('test', result);
    }
    @IsTest
    public static void getAcademicYear_Should_return_division_academic_year_when_exist() {
        List<String> academicYears = CORE_PicklistUtils.getPicklistValues('Opportunity', 'CORE_OPP_Academic_Year__c');
        Datetime dtNow = System.now();
        CORE_DataFaker_CatalogHierarchy catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test');
        CORE_CatalogHierarchyModel catalogHierarchyModel = catalogHierarchy.catalogHierarchy;

        catalogHierarchyModel.intake.CORE_Academic_Year__c = null;
        String divisionAcademicYear =  catalogHierarchyModel.division.CORE_Default_Academic_Year__c;

        String result = CORE_OpportunityUtils.getAcademicYear(null,'test',catalogHierarchyModel,dtNow,academicYears);
        System.assertEquals(divisionAcademicYear, result);
    }

    @IsTest
    public static void getExistingMainOpportunitiesByExternalIds_Should_Return_List_of_main_opportunity_related_to_account_when_exist() {
        String uniqueKey = 'test';
        CORE_DataFaker_CatalogHierarchy catalogHierarchy = new CORE_DataFaker_CatalogHierarchy(uniqueKey);
        Account account = CORE_DataFaker_Account.getStudentAccount(uniqueKey);
        Set<String> businessUnitExternalIds = new Set<String>{catalogHierarchy.businessUnit.CORE_Business_Unit_ExternalId__c};

        Opportunity opp = CORE_DataFaker_Opportunity.getOpportunity(catalogHierarchy.division.Id, 
            catalogHierarchy.businessUnit.Id, 
            catalogHierarchy.studyArea.Id,
            catalogHierarchy.curriculum.Id,
            catalogHierarchy.level.Id,
            catalogHierarchy.intake.Id, 
            account.Id, 
            'Lead', 
            'Lead - New'
        );
        //==> this opp is main
        
        List<Opportunity> mainOpps = CORE_OpportunityUtils.getExistingMainOpportunitiesByExternalIds(businessUnitExternalIds,account.Id);
        
        System.assertEquals(1,mainOpps.size());
        Opportunity opp2 = CORE_DataFaker_Opportunity.getOpportunity(catalogHierarchy.division.Id, 
            catalogHierarchy.businessUnit.Id, 
            catalogHierarchy.studyArea.Id,
            catalogHierarchy.curriculum.Id,
            catalogHierarchy.level.Id,
            catalogHierarchy.intake.Id, 
            account.Id, 
            'Lead', 
            'Lead - New'
        );
        //==> this opp is main too
        mainOpps = CORE_OpportunityUtils.getExistingMainOpportunitiesByExternalIds(businessUnitExternalIds,account.Id);
        System.assertEquals(2,mainOpps.size());

        businessUnitExternalIds = new Set<String>{'test123123'};
        mainOpps = CORE_OpportunityUtils.getExistingMainOpportunitiesByExternalIds(businessUnitExternalIds,account.Id);
        //no main opportunities on this bu
        System.assertEquals(0,mainOpps.size());

    }

    @IsTest
    public static void getExistingMainOpportunitiesByBuIds_Should_Return_List_of_main_opportunity_related_to_account_and_bu_when_exist() {
        String uniqueKey = 'test';
        CORE_DataFaker_CatalogHierarchy catalogHierarchy = new CORE_DataFaker_CatalogHierarchy(uniqueKey);
        CORE_DataFaker_CatalogHierarchy catalogHierarchy2 = new CORE_DataFaker_CatalogHierarchy(uniqueKey + '2');
        Account account = CORE_DataFaker_Account.getStudentAccount(uniqueKey);
        Set<Id> businessUnitIds = new Set<Id>{catalogHierarchy.businessUnit.Id};

        Opportunity opp = CORE_DataFaker_Opportunity.getOpportunity(catalogHierarchy.division.Id, 
            catalogHierarchy.businessUnit.Id, 
            catalogHierarchy.studyArea.Id,
            catalogHierarchy.curriculum.Id,
            catalogHierarchy.level.Id,
            catalogHierarchy.intake.Id, 
            account.Id, 
            'Lead', 
            'Lead - New'
        );
        //==> this opp is main
        Set<Id> accountIds = new Set<Id> {account.Id};
        List<Opportunity> mainOpps = CORE_OpportunityUtils.getExistingMainOpportunitiesByBuIds(businessUnitIds,accountIds);
        
        System.assertEquals(1,mainOpps.size());
        Opportunity opp2 = CORE_DataFaker_Opportunity.getOpportunity(catalogHierarchy.division.Id, 
            catalogHierarchy.businessUnit.Id, 
            catalogHierarchy.studyArea.Id,
            catalogHierarchy.curriculum.Id,
            catalogHierarchy.level.Id,
            catalogHierarchy.intake.Id, 
            account.Id, 
            'Lead', 
            'Lead - New'
        );
        //==> this opp is main too
        mainOpps = CORE_OpportunityUtils.getExistingMainOpportunitiesByBuIds(businessUnitIds,accountIds);
        System.assertEquals(2,mainOpps.size());

        businessUnitIds = new Set<Id>{catalogHierarchy2.businessUnit.Id};
        mainOpps = CORE_OpportunityUtils.getExistingMainOpportunitiesByBuIds(businessUnitIds,accountIds);
        //no main opportunities on this bu
        System.assertEquals(0,mainOpps.size());

    }

    @IsTest
    public static void getExistingAolOpportunities_Should_Return_List_of_opportunity_related_to_aol_ids() {
        String uniqueKey = 'test';
        CORE_DataFaker_CatalogHierarchy catalogHierarchy = new CORE_DataFaker_CatalogHierarchy(uniqueKey);
        Account account = CORE_DataFaker_Account.getStudentAccount(uniqueKey);

        Opportunity opp = CORE_DataFaker_Opportunity.getAOLOpportunity(catalogHierarchy.division.Id, 
            catalogHierarchy.businessUnit.Id, 
            catalogHierarchy.studyArea.Id,
            catalogHierarchy.curriculum.Id,
            catalogHierarchy.level.Id,
            catalogHierarchy.intake.Id, 
            account.Id, 
            'Lead', 
            'Lead - New',
            'testAolId'
        );
        //==> this opp is main
        Set<String> ids = new Set<String>{opp.CORE_OPP_Application_Online_ID__c};

        List<Opportunity> mainOpps = CORE_OpportunityUtils.getExistingAolOpportunities(ids);
        
        System.assertEquals(1,mainOpps.size());

        ids = new Set<String>{'unexistingAolId'};
        mainOpps = CORE_OpportunityUtils.getExistingAolOpportunities(ids);
        //no main opportunities on this bu
        System.assertEquals(0,mainOpps.size());

    }

    @IsTest
    public static void getExistingOnlineShopOpportunities_Should_Return_List_of_opportunity_related_to_onlineShop_ids() {
        String uniqueKey = 'test';
        CORE_DataFaker_CatalogHierarchy catalogHierarchy = new CORE_DataFaker_CatalogHierarchy(uniqueKey);
        Account account = CORE_DataFaker_Account.getStudentAccount(uniqueKey);

        Opportunity opp = CORE_DataFaker_Opportunity.getOpportunity(catalogHierarchy.division.Id, 
            catalogHierarchy.businessUnit.Id, 
            catalogHierarchy.studyArea.Id,
            catalogHierarchy.curriculum.Id,
            catalogHierarchy.level.Id,
            catalogHierarchy.intake.Id, 
            account.Id, 
            'Lead', 
            'Lead - New'
        );
        opp.CORE_OPP_OnlineShop_Id__c = 'testOSid';
        update opp;
        //==> this opp is main
        
        Set<String> ids = new Set<String>{opp.CORE_OPP_OnlineShop_Id__c};
        List<Opportunity> mainOpps = CORE_OpportunityUtils.getExistingOnlineShopOpportunities(ids);
        
        System.assertEquals(1,mainOpps.size());

        ids = new Set<String>{'unexistingOSId'};
        mainOpps = CORE_OpportunityUtils.getExistingOnlineShopOpportunities(ids);
        //no main opportunities on this bu
        System.assertEquals(0,mainOpps.size());
    }

    @IsTest
    public static void getInterestTask_should_return_task() {
        String uniqueKey = 'test';
        CORE_DataFaker_CatalogHierarchy catalogHierarchy = new CORE_DataFaker_CatalogHierarchy(uniqueKey);
        Account account = CORE_DataFaker_Account.getStudentAccount(uniqueKey);

        Opportunity opp = CORE_DataFaker_Opportunity.getOpportunity(catalogHierarchy.division.Id, 
            catalogHierarchy.businessUnit.Id, 
            catalogHierarchy.studyArea.Id,
            catalogHierarchy.curriculum.Id,
            catalogHierarchy.level.Id,
            catalogHierarchy.intake.Id, 
            account.Id, 
            'Lead', 
            'Lead - New'
        );
        //==> this opp is main
        
        Task result = CORE_OpportunityUtils.getInterestTask(null, null, opp, account);
        
        System.assertNotEquals(null,result); 
        
        result = CORE_OpportunityUtils.getInterestTask(null, 'test', opp, account);
        
        System.assertNotEquals(null,result);

        opp.CORE_OPP_OnlineShop_Id__c = 'testOSId';
        result = CORE_OpportunityUtils.getInterestTask(null, 'test', opp, account);
        
        System.assertNotEquals(null,result);
    }

    @IsTest
    public static void getReturningTask_should_return_task() {
        String uniqueKey = 'test';
        CORE_DataFaker_CatalogHierarchy catalogHierarchy = new CORE_DataFaker_CatalogHierarchy(uniqueKey);
        Account account = CORE_DataFaker_Account.getStudentAccount(uniqueKey);

        Opportunity opp = CORE_DataFaker_Opportunity.getOpportunity(catalogHierarchy.division.Id, 
            catalogHierarchy.businessUnit.Id, 
            catalogHierarchy.studyArea.Id,
            catalogHierarchy.curriculum.Id,
            catalogHierarchy.level.Id,
            catalogHierarchy.intake.Id, 
            account.Id, 
            'Lead', 
            'Lead - New'
        );
        //==> this opp is main
        catalogHierarchy.catalogHierarchy.interest = new CORE_LeadFormAcquisitionWrapper.OpportunityWrapper();
        Task result = CORE_OpportunityUtils.getReturningTask(catalogHierarchy.catalogHierarchy, null, opp, account);
        
        System.assertNotEquals(null,result); 
        
        result = CORE_OpportunityUtils.getReturningTask(catalogHierarchy.catalogHierarchy, 'test', opp, account);
        
        System.assertNotEquals(null,result);

        catalogHierarchy.catalogHierarchy.interest.onlineShopId = 'testOSId';
        result = CORE_OpportunityUtils.getReturningTask(catalogHierarchy.catalogHierarchy, 'test', opp, account);
        
        System.assertNotEquals(null,result);

        catalogHierarchy.catalogHierarchy.interest.onlineShopId = null;
        catalogHierarchy.catalogHierarchy.interest.applicationOnlineId = 'testaol';
        result = CORE_OpportunityUtils.getReturningTask(catalogHierarchy.catalogHierarchy, 'test', opp, account);
        
        System.assertNotEquals(null,result);
    }


    @IsTest
    public static void getDuplicatedAttachments_Should_duplicated_attachments() {
        String uniqueKey = 'test';
        CORE_DataFaker_CatalogHierarchy catalogHierarchy = new CORE_DataFaker_CatalogHierarchy(uniqueKey);
        Account account = CORE_DataFaker_Account.getStudentAccount(uniqueKey);

        Opportunity opp = CORE_DataFaker_Opportunity.getOpportunity(catalogHierarchy.division.Id, 
            catalogHierarchy.businessUnit.Id, 
            catalogHierarchy.studyArea.Id,
            catalogHierarchy.curriculum.Id,
            catalogHierarchy.level.Id,
            catalogHierarchy.intake.Id, 
            account.Id, 
            'Lead', 
            'Lead - New'
        );

        Opportunity opp2 = CORE_DataFaker_Opportunity.getOpportunity(catalogHierarchy.division.Id, 
            catalogHierarchy.businessUnit.Id, 
            catalogHierarchy.studyArea.Id,
            catalogHierarchy.curriculum.Id,
            catalogHierarchy.level.Id,
            catalogHierarchy.intake.Id, 
            account.Id, 
            'Lead', 
            'Lead - New'
        );
        //==> this opp is main

        ContentVersion contentVersion = new ContentVersion(
                    Title          = 'a picture',
                    PathOnClient   = 'Pic.jpg',
                    VersionData    = Blob.valueOf('Test Content'),
                    IsMajorVersion = true);
            insert contentVersion;

            List<ContentDocument> documents = [SELECT Id, Title, LatestPublishedVersionId FROM ContentDocument];

        //create ContentDocumentLink  record
        ContentDocumentLink doc = new ContentDocumentLink();
        doc.LinkedEntityId = opp.Id;
        doc.ContentDocumentId = documents[0].Id;
        doc.ShareType = 'V';
        doc.Visibility = 'AllUsers';
        insert doc;

        List<ContentDocumentLink> docs = [Select Id from ContentDocumentLink where LinkedEntityId = :opp.Id];
        System.assertEquals(1, docs.size());

        List<ContentDocumentLink> results = CORE_OpportunityUtils.getDuplicatedAttachments(new List<ContentDocumentLink>{doc}, opp2.Id);
        System.assertEquals(1,results.size());

        docs = [Select Id from ContentDocumentLink where LinkedEntityId = :opp2.Id];
        System.assertEquals(1, docs.size());
    }

    @IsTest
    public static void getDuplicatedContactRoles_Should_duplicated_roles() {
        String uniqueKey = 'test';
        CORE_DataFaker_CatalogHierarchy catalogHierarchy = new CORE_DataFaker_CatalogHierarchy(uniqueKey);
        Account account = CORE_DataFaker_Account.getStudentAccount(uniqueKey);
        account = [Select Id, personContactID from Account where Id = :account.Id];
        Opportunity opp = CORE_DataFaker_Opportunity.getOpportunity(catalogHierarchy.division.Id, 
            catalogHierarchy.businessUnit.Id, 
            catalogHierarchy.studyArea.Id,
            catalogHierarchy.curriculum.Id,
            catalogHierarchy.level.Id,
            catalogHierarchy.intake.Id, 
            account.Id, 
            'Lead', 
            'Lead - New'
        );

        Opportunity opp2 = CORE_DataFaker_Opportunity.getOpportunity(catalogHierarchy.division.Id, 
            catalogHierarchy.businessUnit.Id, 
            catalogHierarchy.studyArea.Id,
            catalogHierarchy.curriculum.Id,
            catalogHierarchy.level.Id,
            catalogHierarchy.intake.Id, 
            account.Id, 
            'Lead', 
            'Lead - New'
        );
        //==> this opp is main
        OpportunityContactRole role = new OpportunityContactRole(
            OpportunityId = opp.ID,
            ContactId = account.personContactId
        );

        insert role;
        List<OpportunityContactRole> roles = [Select Id from OpportunityContactRole];
        System.assertEquals(1, roles.size());

        List<OpportunityContactRole> results = CORE_OpportunityUtils.getDuplicatedContactRoles(new List<OpportunityContactRole>{role}, opp2.Id);
        System.assertEquals(1,results.size());

        roles = [Select Id from OpportunityContactRole];
        System.assertEquals(2, roles.size());
    }

    @IsTest
    public static void getDuplicatedSubStatusTimeStamps_Should_duplicated_subStatus() {
        String uniqueKey = 'test';
        CORE_DataFaker_CatalogHierarchy catalogHierarchy = new CORE_DataFaker_CatalogHierarchy(uniqueKey);
        Account account = CORE_DataFaker_Account.getStudentAccount(uniqueKey);

        Opportunity opp = CORE_DataFaker_Opportunity.getOpportunity(catalogHierarchy.division.Id, 
            catalogHierarchy.businessUnit.Id, 
            catalogHierarchy.studyArea.Id,
            catalogHierarchy.curriculum.Id,
            catalogHierarchy.level.Id,
            catalogHierarchy.intake.Id, 
            account.Id, 
            'Lead', 
            'Lead - New'
        );

        Opportunity opp2 = CORE_DataFaker_Opportunity.getOpportunity(catalogHierarchy.division.Id, 
            catalogHierarchy.businessUnit.Id, 
            catalogHierarchy.studyArea.Id,
            catalogHierarchy.curriculum.Id,
            catalogHierarchy.level.Id,
            catalogHierarchy.intake.Id, 
            account.Id, 
            'Lead', 
            'Lead - New'
        );
        //==> this opp is main
        List<CORE_Sub_Status_Timestamp_History__c> subStatuses = [Select Id, CORE_Opportunity__c, CORE_Status__c, CORE_External_Id__c, Name, CORE_Sub_status__c from CORE_Sub_Status_Timestamp_History__c];
        //System.assertNotEquals(0, subStatuses.size());
        Integer nbSubstatus = subStatuses.size();

        List<CORE_Sub_Status_Timestamp_History__c> results = CORE_OpportunityUtils.getDuplicatedSubStatusTimeStamps(subStatuses, opp2.Id);
        //System.assertEquals(nbSubstatus,results.size());

        subStatuses = [Select Id, CORE_Opportunity__c, CORE_Status__c, CORE_External_Id__c, Name, CORE_Sub_status__c from CORE_Sub_Status_Timestamp_History__c];
        //System.assertEquals(nbSubstatus*2, subStatuses.size());
    }

    @IsTest
    public static void getDuplicatedStatusTimeStamps_Should_duplicated_status() {
        String uniqueKey = 'test';
        CORE_DataFaker_CatalogHierarchy catalogHierarchy = new CORE_DataFaker_CatalogHierarchy(uniqueKey);
        Account account = CORE_DataFaker_Account.getStudentAccount(uniqueKey);

        Opportunity opp = CORE_DataFaker_Opportunity.getOpportunity(catalogHierarchy.division.Id, 
            catalogHierarchy.businessUnit.Id, 
            catalogHierarchy.studyArea.Id,
            catalogHierarchy.curriculum.Id,
            catalogHierarchy.level.Id,
            catalogHierarchy.intake.Id, 
            account.Id, 
            'Lead', 
            'Lead - New'
        );

        Opportunity opp2 = CORE_DataFaker_Opportunity.getOpportunity(catalogHierarchy.division.Id, 
            catalogHierarchy.businessUnit.Id, 
            catalogHierarchy.studyArea.Id,
            catalogHierarchy.curriculum.Id,
            catalogHierarchy.level.Id, 
            catalogHierarchy.intake.Id, 
            account.Id, 
            'Lead', 
            'Lead - New'
        );
        //==> this opp is main
        
        List<CORE_Status_Timestamp_History__c> status = [Select Id, CORE_Opportunity__c, CORE_Status__c, CORE_External_Id__c, Name from CORE_Status_Timestamp_History__c];
        System.assertNotEquals(0, status.size());
        Integer nbStatus = status.size();

        List<CORE_Status_Timestamp_History__c> results = CORE_OpportunityUtils.getDuplicatedStatusTimeStamps(status, opp2.Id);
        System.assertEquals(nbStatus,results.size());

        status = [Select Id, CORE_Opportunity__c, CORE_Status__c, CORE_External_Id__c, Name from CORE_Status_Timestamp_History__c];
        System.assertEquals(nbStatus*2, status.size());
    }

    @IsTest
    public static void getDuplicatedPointOfContacts_Should_duplicated_status() {
        String uniqueKey = 'test';
        CORE_DataFaker_CatalogHierarchy catalogHierarchy = new CORE_DataFaker_CatalogHierarchy(uniqueKey);
        Account account = CORE_DataFaker_Account.getStudentAccount(uniqueKey);

        Opportunity opp = CORE_DataFaker_Opportunity.getOpportunity(catalogHierarchy.division.Id, 
            catalogHierarchy.businessUnit.Id, 
            catalogHierarchy.studyArea.Id,
            catalogHierarchy.curriculum.Id,
            catalogHierarchy.level.Id,
            catalogHierarchy.intake.Id, 
            account.Id, 
            'Lead', 
            'Lead - New'
        );

        Opportunity opp2 = CORE_DataFaker_Opportunity.getOpportunity(catalogHierarchy.division.Id, 
            catalogHierarchy.businessUnit.Id, 
            catalogHierarchy.studyArea.Id,
            catalogHierarchy.curriculum.Id,
            catalogHierarchy.level.Id,
            catalogHierarchy.intake.Id, 
            account.Id, 
            'Lead', 
            'Lead - New'
        );
        //==> this opp is main
        CORE_Point_of_Contact__c poc = CORE_DataFaker_PointOfContact.getPointOfContact(opp.Id, account.Id, 'test1');
        List<CORE_Point_of_Contact__c> pocs = [SELECT Id FROM CORE_Point_of_Contact__c];
        System.assertNotEquals(0, pocs.size());
        Integer nbPpocs= pocs.size();

        List<CORE_Point_of_Contact__c> results = CORE_OpportunityUtils.getDuplicatedPointOfContacts(new List<CORE_Point_of_Contact__c> {poc}, opp2.Id);
        System.assertNotEquals(0,results.size());

        pocs = [SELECT Id FROM CORE_Point_of_Contact__c];
        System.assertEquals(nbPpocs*2, pocs.size());
    }
}