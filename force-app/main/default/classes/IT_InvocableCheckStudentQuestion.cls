public without sharing class IT_InvocableCheckStudentQuestion {
   
    @InvocableMethod
    public static List<FlowOutputs> getOutputs(List<FlowInputs> request){
        Boolean hasValue = false;
        String questionValue = null;

        Task incomingTask   = request[0].incomingTask;
        String comments     = incomingTask.Description;
        if (String.isNotBlank(comments) ) {
            String[] eachLine = comments.split('\n');            

            for (String line :eachLine ) {
                if (line.containsIgnoreCase('Student Question :' ) ){
                    questionValue = line.split(':')[1].trim();
                }
            }
        }
        hasValue = String.isNotBlank(questionValue) && questionValue != 'null' && questionValue != null ; //  <questionValue != 'null'> because of middleware. It inserts tasks with student question: 'null' - 27/06/22
        questionValue = hasValue ? questionValue : null;

        FlowOutputs flOutput= new FlowOutputs();
        flOutput.hasValue       = hasValue;
        flOutput.questionValue  = questionValue;

        List<FlowOutputs> returnList = new List<FlowOutputs>();
        returnList.add(flOutput);
        return returnList;

    }

    @TestVisible
    public class FlowInputs{

        @InvocableVariable (required=true)
        public Task incomingTask;

    }

    @TestVisible
    public class FlowOutputs{

        @InvocableVariable
        public Boolean hasValue;

        @InvocableVariable
        public String questionValue;

    }

   
}