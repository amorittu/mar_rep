@isTest
public with sharing class IT_EmailMessageTriggerHandlerTest {
    
    @TestSetup
    public static void makeData(){
        CORE_DataFaker_CatalogHierarchy catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test1');
        Account account = CORE_DataFaker_Account.getStudentAccount('test1');
        Opportunity opportunity = CORE_DataFaker_Opportunity.getOpportunity(
                catalogHierarchy.division.Id,
                catalogHierarchy.businessUnit.Id,
                account.Id,
                'Applicant',
                'Applicant - Application Submitted / New'
        );

        opportunity.CORE_OPP_Intake__c = catalogHierarchy.intake.Id;
        opportunity.CORE_OPP_Curriculum__c = catalogHierarchy.curriculum.Id;
        opportunity.CORE_OPP_Study_Area__c = catalogHierarchy.studyArea.Id;
        opportunity.CORE_OPP_Level__c = catalogHierarchy.level.Id;
        opportunity.IT_AdmissionOfficer__c = UserInfo.getUserId();
        update opportunity;
    }

    @isTest
    public static void afterUpdateTest(){
        Opportunity opportunity = [SELECT Id FROM Opportunity LIMIT 1 ];
        EmailMessage emailMessage = new EmailMessage();
        emailMessage.HtmlBody = 'test';
        emailMessage.Incoming = true; 
        emailMessage.relatedToId = opportunity.Id;
        insert emailMessage;
    }
}