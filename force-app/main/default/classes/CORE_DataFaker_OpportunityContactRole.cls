@IsTest
public class CORE_DataFaker_OpportunityContactRole {
    private static final String CONTACT_ROLE = 'CORE_PKL_Student';

    public static OpportunityContactRole getOpportunityContactRole(Id opportunityId, Id contactId){
        OpportunityContactRole contactRole = new OpportunityContactRole(
            ContactId = contactId,
            OpportunityId = opportunityId,
            IsPrimary = true,
            Role = CONTACT_ROLE
        );
       
        insert contactRole;
        
        return contactRole;
    }
}