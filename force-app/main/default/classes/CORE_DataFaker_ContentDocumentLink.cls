@IsTest
public class CORE_DataFaker_ContentDocumentLink {
    public static ContentDocumentLink getContentDocumentLink(Id linkedEntityId){
        ContentVersion contentVersion = getContentVersion();
        Id contentdocumentid = [select contentdocumentid from contentversion where id =: contentVersion.id].contentdocumentid;
        
        ContentDocumentLink contentlink=new ContentDocumentLink(
            LinkedEntityId = linkedEntityId,
            contentdocumentid = contentdocumentid,
            ShareType = 'I',
            Visibility = 'AllUsers'
        );

        insert contentlink;

        return contentlink;
    }

    private static ContentVersion getContentVersion(){
        Blob bodyBlob = Blob.valueOf('Unit Test ContentVersion Body'); 
        String title = 'Header_Picture1';
        ContentVersion content=new ContentVersion(
            Title = title,
            PathOnClient='/' + title + '.jpg',
            VersionData=bodyBlob,
            origin = 'H'
        );

        insert content;

        return content;
    }
}