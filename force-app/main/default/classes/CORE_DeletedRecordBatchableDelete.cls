public without sharing class CORE_DeletedRecordBatchableDelete implements Database.batchable<SObject> {
    public Database.QueryLocator start(Database.BatchableContext info){
        System.debug('CORE_DeletedRecordBatchableDelete.start() : START');
        CORE_DeletedRecordMetadata delMetadata = new CORE_DeletedRecordMetadata();
        DateTime dateTimeMinimum = System.now().addDays(Integer.valueOf(delMetadata.deletedRecordMetadata.CORE_Delay__c * -1));
        return Database.getQueryLocator([Select Id FROM CORE_Deleted_record__c WHERE CORE_Deletion_Date__c < :dateTimeMinimum]); 
    }

    public void execute(Database.BatchableContext info, List<CORE_Deleted_record__c> scope){
        System.debug('CORE_DeletedRecordBatchableDelete.execute() : START');
        delete scope;
        System.debug('CORE_DeletedRecordBatchableDelete.execute() : END');
    }

    public void finish(Database.BatchableContext info){
        System.debug('CORE_DeletedRecordBatchableDelete.finish() : FINISHED');
    } 
}