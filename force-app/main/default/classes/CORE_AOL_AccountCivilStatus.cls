@RestResource(urlMapping='/aol/account/civil-status')
global without sharing class CORE_AOL_AccountCivilStatus {  
    private static final String SUCCESS_MESSAGE = 'Civil status & progression fiedls updated with success';

    @HttpPut
    global static CORE_AOL_Wrapper.ResultWrapperGlobal execute() {

        RestRequest	request = RestContext.request;

        // Initiating logs
        Long dt1 = DateTime.now().getTime();
        Long dt2;
        CORE_LogManager logSettings;
        CORE_LOG__c log = new CORE_LOG__c(CORE_API_Name__c = 'AOL');
        Boolean isErrorLog = false;
        logSettings = new CORE_LogManager('AOL');

        Map<String, Object> paramsMap = (Map<String, Object>)JSON.deserializeUntyped(request.requestBody.toString());
        String aolExternalId = (String)paramsMap.get(CORE_AOL_Constants.PARAM_AOL_EXTERNALID);
        CORE_AOL_Wrapper.AccountWrapper account = (CORE_AOL_Wrapper.AccountWrapper)JSON.deserialize((String)JSON.serialize(paramsMap.get(CORE_AOL_Constants.PARAM_AOL_ACCOUNT_CIVIL_STATUS)), CORE_AOL_Wrapper.AccountWrapper.class);
        CORE_AOL_Wrapper.AOLProgressionFields aolProgressionFields = (CORE_AOL_Wrapper.AOLProgressionFields)JSON.deserialize((String)JSON.serialize(paramsMap.get(CORE_AOL_Constants.PARAM_AOL_PROG_FIELDS)), CORE_AOL_Wrapper.AOLProgressionFields.class);

        CORE_AOL_Wrapper.ResultWrapperGlobal result = checkMandatoryParameters(aolExternalId);

        try{
            
            if(result != null){
                //missing parameter(s)
                return result;
            }
            CORE_AOL_GlobalWorker aolWorker = new CORE_AOL_GlobalWorker(aolProgressionFields,aolExternalId);
            Opportunity opportunity = aolWorker.aolOpportunity;

            if(opportunity == null){
                result = new CORE_AOL_Wrapper.ResultWrapperGlobal(
                    CORE_AOL_Constants.ERROR_STATUS, 
                    String.format(CORE_AOL_Constants.MSG_OPPORTUNITY_NOT_FOUND, new List<String>{aolExternalId})
                );
            } else {
                Account acc = civilStatusProcess(aolWorker,account);

                //set Additional fields to update
                acc = (Account)CORE_AOL_GlobalWorker.setAdditionalFields(acc, request.requestBody.toString());
                database.update(acc, true);

                aolWorker.updateOppIsNeeded = true;
                aolWorker.updateAolProgressionFields();

                result = new CORE_AOL_Wrapper.ResultWrapperGlobal(CORE_AOL_Constants.SUCCESS_STATUS, SUCCESS_MESSAGE);
            }
            
        } catch(Exception e){
            result = new CORE_AOL_Wrapper.ResultWrapperGlobal(CORE_AOL_Constants.ERROR_STATUS, e.getMessage() + ' at ' + e.getStackTraceString());
        } finally{
            dt2 = DateTime.now().getTime();
            System.debug('CORE_AOL_AcademicDiplomaHistory(urlMapping=\'/aol/academic-diploma-history\') : T0TAL TIME OF EXECUTION = ' + (dt2-dt1) + ' ms');
            // creating log
            log.CORE_Processing_Duration__c =(dt2-dt1);
            log.CORE_HTTP_Status_Code__c = result.status == 'KO' ? 400 : result.status == 'OK' ? 200 : 500;
            log.CORE_Sent_Body__c = JSON.serialize(result);
            log.CORE_Received_Body__c =  request.requestBody.toString();
            
            if(!logSettings.isNone()){
                if(logSettings.isAllLogged()){ // is All logged
                    CORE_LogManager.createLog(log);
                }else if(logSettings.isErrorLogged() && isErrorLog){ // is Error
                    CORE_LogManager.createLog(log);
                }
            }
        }

        return result;
    }

    public static Account civilStatusProcess(CORE_AOL_GlobalWorker aolWorker,
        CORE_AOL_Wrapper.AccountWrapper accountWrapper){

        boolean updateIfNotNull = accountWrapper.updateIfNotNull == null ? false : accountWrapper.updateIfNotNull;

        Account account = aolWorker.getRelatedAccount();

        if(!String.IsBlank(accountWrapper.salutation) && (account.Salutation == null || updateIfNotNull)){
            account.Salutation = accountWrapper.salutation;
        }

        if(!String.IsBlank(accountWrapper.firstname) && (account.FirstName == null || updateIfNotNull)){
            account.FirstName = accountWrapper.firstname;
        }

        if(!String.IsBlank(accountWrapper.lastName) && (account.LastName == null || updateIfNotNull)){
            account.LastName = accountWrapper.lastName;
        }

        if(accountWrapper.birthdate != null && (account.PersonBirthdate == null || updateIfNotNull)){
            account.PersonBirthdate = accountWrapper.birthdate;
        }

        if(!String.IsBlank(accountWrapper.mainNationality) && (account.CORE_Main_Nationality__pc == null || updateIfNotNull)){
            account.CORE_Main_Nationality__pc = accountWrapper.mainNationality;
        }

        if(!String.IsBlank(accountWrapper.personMailingStreet) && (account.PersonMailingStreet == null || updateIfNotNull)){
            account.PersonMailingStreet = accountWrapper.personMailingStreet;
        }

        if(!String.IsBlank(accountWrapper.personMailingPostalCode) && (account.PersonMailingPostalCode == null || updateIfNotNull)){
            account.PersonMailingPostalCode = accountWrapper.personMailingPostalCode;
        }

        if(!String.IsBlank(accountWrapper.personMailingCity) && (account.PersonMailingCity == null || updateIfNotNull)){
            account.PersonMailingCity = accountWrapper.personMailingCity;
        }
        
        if(!String.IsBlank(accountWrapper.personMailingState) && (account.PersonMailingStateCode == null || updateIfNotNull)){
            account.PersonMailingStateCode = accountWrapper.personMailingState;
        }

        if(!String.IsBlank(accountWrapper.personMailingCountry) && (account.PersonMailingCountryCode == null || updateIfNotNull)){
            account.PersonMailingCountryCode = accountWrapper.personMailingCountry;
        }

        account.CORE_City_of_birth__pc = (checkIfNotNull(accountWrapper.cityOfBirth, updateIfNotNull)) ? accountWrapper.cityOfBirth :account.CORE_City_of_birth__pc;
        account.CORE_Country_of_birth__pc = (checkIfNotNull(accountWrapper.countryOfBirth, updateIfNotNull)) ? accountWrapper.countryOfBirth :account.CORE_Country_of_birth__pc;
        account.CORE_Department_of_birth__pc = (checkIfNotNull(accountWrapper.deptOfBirth, updateIfNotNull)) ? accountWrapper.deptOfBirth :account.CORE_Department_of_birth__pc;
        account.CORE_Gender__pc = (checkIfNotNull(accountWrapper.gender, updateIfNotNull)) ? accountWrapper.gender :account.CORE_Gender__pc;
        account.CORE_BillingPermanentRegion__c = (checkIfNotNull(accountWrapper.billingPermanentRegion, updateIfNotNull)) ? accountWrapper.billingPermanentRegion :account.CORE_BillingPermanentRegion__c;
        account.BillingStreet = (checkIfNotNull(accountWrapper.billingStreet, updateIfNotNull)) ? accountWrapper.billingStreet :account.BillingStreet;
        account.BillingCity = (checkIfNotNull(accountWrapper.billingCity, updateIfNotNull)) ? accountWrapper.billingCity :account.BillingCity;
        account.BillingPostalCode = (checkIfNotNull(accountWrapper.billingPostalCode, updateIfNotNull)) ? accountWrapper.billingPostalCode :account.BillingPostalCode;
        account.BillingStateCode = (checkIfNotNull(accountWrapper.billingState, updateIfNotNull)) ? accountWrapper.billingState :account.BillingStateCode;
        account.BillingCountryCode = (checkIfNotNull(accountWrapper.billingCountry, updateIfNotNull)) ? accountWrapper.billingCountry :account.BillingCountryCode;
        account.CORE_PersonMailingCurrentRegion__c = (checkIfNotNull(accountWrapper.personMailingCurrentRegion, updateIfNotNull)) ? accountWrapper.personMailingCurrentRegion :account.CORE_PersonMailingCurrentRegion__c;
        account.CORE_Social_Security_Number_Fiscal_Code__pc = (checkIfNotNull(accountWrapper.socialSecNumberFiscalCode, updateIfNotNull)) ? accountWrapper.socialSecNumberFiscalCode :account.CORE_Social_Security_Number_Fiscal_Code__pc;

        return account;
    }

    private static CORE_AOL_Wrapper.ResultWrapperGlobal checkMandatoryParameters(String aolExternalId){
        List<String> missingParameters = new List<String>();
        Boolean isMissingAolId = CORE_AOL_GlobalWorker.isAolExternalIdParameterMissing(aolExternalId);

        if(isMissingAolId){
            missingParameters.add(CORE_AOL_Constants.PARAM_AOL_EXTERNALID);
        }
        
        return CORE_AOL_GlobalWorker.getMissingParameters(missingParameters);
    }

    private static Boolean checkIfNotNull(String param, Boolean updateIfNotNull){

        if(param != null
           || (param == null
                && updateIfNotNull)){

            return true;
        }

        return false;
    }
}