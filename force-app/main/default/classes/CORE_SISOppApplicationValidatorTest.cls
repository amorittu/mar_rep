/*
Created By Fodil:  25-04-2022
Test Class for CORE_SISOpportunityApplicationValidator
*/
@isTest
public without sharing class CORE_SISOppApplicationValidatorTest {

    @IsTest
    static void testValidator(){
        
        CORE_SISOpportunityApplicationWrapper.GlobalWrapper body = new CORE_SISOpportunityApplicationWrapper.GlobalWrapper();
        CORE_SISOpportunityApplicationWrapper.MainInterestWrapper interestwrapper = new CORE_SISOpportunityApplicationWrapper.MainInterestWrapper();
        interestwrapper.SIS_OpportunityId = 'SIS_EXT_ID';
        interestwrapper.academicYear = 'CORE_PKL_2021-2022';
        interestwrapper.businessUnit = 'BU';
        interestwrapper.studyArea = 'SA';
        interestwrapper.curriculum = 'CU';
        interestwrapper.level = 'LE';
        interestwrapper.intake = 'IN';
        body.mainInterest = interestwrapper;
        body.personAccount = new Map<String,Object>{'CORE_SISStudentID__c'=>'SIS-Student-ID'};
        Test.startTest();
         CORE_SISOpportunityApplicationValidator validator= new CORE_SISOpportunityApplicationValidator(body,DateTime.now());        
         List<CORE_SISOpportunityApplicationWrapper.ErrorWrapper> errors = validator.getFieldsErrors();
        System.assertEquals(0, errors.size());
        Test.stopTest();
        
    }

    @IsTest
    static void testValidatorExceptionNull(){
        
        CORE_SISOpportunityApplicationWrapper.GlobalWrapper body = new CORE_SISOpportunityApplicationWrapper.GlobalWrapper();
        CORE_SISOpportunityApplicationWrapper.MainInterestWrapper interestwrapper = new CORE_SISOpportunityApplicationWrapper.MainInterestWrapper();
        interestwrapper.SIS_OpportunityId = 'SIS_EXT_ID';
        interestwrapper.academicYear = 'CORE_PKL_2021-2022';
        interestwrapper.businessUnit = null;//null error
        interestwrapper.studyArea = '';// empty string error
        interestwrapper.curriculum = 'CU';
        interestwrapper.level = 'LE';
        interestwrapper.intake = 'IN';
        body.mainInterest = interestwrapper;
        body.personAccount = new Map<String,Object>{'CORE_SISStudentID__c'=>'SIS-Student-ID'};
        Test.startTest();
        CORE_SISOpportunityApplicationValidator validator= new CORE_SISOpportunityApplicationValidator(body,DateTime.now());        
        List<CORE_SISOpportunityApplicationWrapper.ErrorWrapper> errors = validator.getFieldsErrors();
        System.assertEquals(1, errors.size());
        Test.stopTest();
        
    }

    @IsTest
    static void testValidatorExceptionEmptyString(){
        
        CORE_SISOpportunityApplicationWrapper.GlobalWrapper body = new CORE_SISOpportunityApplicationWrapper.GlobalWrapper();
        CORE_SISOpportunityApplicationWrapper.MainInterestWrapper interestwrapper = new CORE_SISOpportunityApplicationWrapper.MainInterestWrapper();
        interestwrapper.SIS_OpportunityId = 'SIS_EXT_ID';
        interestwrapper.academicYear = 'CORE_PKL_2021-2022';
        interestwrapper.businessUnit = 'BU';
        interestwrapper.studyArea = '';// empty string error
        interestwrapper.curriculum = 'CU';
        interestwrapper.level = 'LE';
        interestwrapper.intake = 'IN';
        body.mainInterest = interestwrapper;
        body.personAccount = new Map<String,Object>{'CORE_SISStudentID__c'=>'SIS-Student-ID'};
        Test.startTest();
        CORE_SISOpportunityApplicationValidator validator= new CORE_SISOpportunityApplicationValidator(body,DateTime.now());        
        List<CORE_SISOpportunityApplicationWrapper.ErrorWrapper> errors = validator.getFieldsErrors();
        System.assertEquals(1, errors.size());
        Test.stopTest();
        
    }

    @IsTest
    static void testValidatorExceptionNoKey(){
        
        CORE_SISOpportunityApplicationWrapper.GlobalWrapper body = new CORE_SISOpportunityApplicationWrapper.GlobalWrapper();
        CORE_SISOpportunityApplicationWrapper.MainInterestWrapper interestwrapper = new CORE_SISOpportunityApplicationWrapper.MainInterestWrapper();
        interestwrapper.SIS_OpportunityId = 'SIS_EXT_ID';
        interestwrapper.academicYear = 'CORE_PKL_2021-2022';
        interestwrapper.businessUnit = 'BU';
        interestwrapper.studyArea = 'SA';// empty string error
        interestwrapper.curriculum = 'CU';
        interestwrapper.level = 'LE';
        interestwrapper.intake = 'IN';
        body.mainInterest = interestwrapper;
        body.personAccount = new Map<String,Object>();
        Test.startTest();
        CORE_SISOpportunityApplicationValidator validator= new CORE_SISOpportunityApplicationValidator(body,DateTime.now());        
        List<CORE_SISOpportunityApplicationWrapper.ErrorWrapper> errors = validator.getFieldsErrors();
        System.assertEquals(1, errors.size());
        Test.stopTest();
        
    }
}