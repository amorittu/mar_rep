/**
 * Created by dgagliardi on 19/05/2022.
 */

@IsTest
private class IT_BatchExtendedInfoOppTest {

	@TestSetup
	static void testSetup() {
		CORE_DataFaker_CatalogHierarchy catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test1');
		Account account = CORE_DataFaker_Account.getStudentAccount('test1');
		Opportunity opportunity = CORE_DataFaker_Opportunity.getOpportunity(
				catalogHierarchy.division.Id,
				catalogHierarchy.businessUnit.Id,
				account.Id,
				'Applicant',
				'Applicant - Application Submitted / New'
		);

		opportunity.CORE_OPP_Intake__c = catalogHierarchy.intake.Id;
		opportunity.CORE_OPP_Curriculum__c = catalogHierarchy.curriculum.Id;
		opportunity.CORE_OPP_Study_Area__c = catalogHierarchy.studyArea.Id;
		opportunity.CORE_OPP_Level__c = catalogHierarchy.level.Id;
		opportunity.IT_AdmissionOfficer__c = UserInfo.getUserId();
		update opportunity;

		Id rtId = Schema.SObjectType.Task.getRecordTypeInfosByDeveloperName().get('CORE_Continuum_of_action').getRecordTypeId();

		Task t = new Task();
		t.WhatId = opportunity.Id;
		t.RecordTypeId = rtId;
		t.Status = 'CORE_PKL_Open';
		insert t;

	}

	@IsTest
	static void testBehaviorRunNext() {

		Test.startTest();
            IT_BatchExtendedInfoOpportunityCreated b = new IT_BatchExtendedInfoOpportunityCreated(true);
			Database.executeBatch(b);
		Test.stopTest();

	}
	@IsTest
	static void testBehavior() {

		Test.startTest();
            IT_BatchExtendedInfoOpportunityCreated b = new IT_BatchExtendedInfoOpportunityCreated();
            Database.executeBatch(b);
		Test.stopTest();

	}
    
    @IsTest
	static void testBehaviorRunBatchAndAccount() {
		
        Account[] account = [SELECT id FROM Account LIMIT 1];
        
		Test.startTest();
        IT_BatchExtendedInfoOpportunityCreated b = new IT_BatchExtendedInfoOpportunityCreated(true, new String[] {account.get(0).Id} );
            Database.executeBatch(b); 
		Test.stopTest();

	}
}