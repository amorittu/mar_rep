global with sharing class CORE_CUBE_UpdateMasterLog  implements Database.Batchable<sObject> {


    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        String query = 'SELECT Id,ErrorOn__c,Last_Success__c,Last_Try__c,IsSuccess__c FROM CORE_CUBE_Master_Log__c';
        return Database.getQueryLocator(query);
    }
 
    global void execute(Database.BatchableContext BC, List<CORE_CUBE_Master_Log__c> myMasterLog)
    {
        System.debug('myMasterLog '+myMasterLog[0]);
        updateMasterLog(myMasterLog[0]);
    }  
    global void finish(Database.BatchableContext BC)
    {
    }

    /*global Database.QueryLocator start(Database.BatchableContext BC)
    {
        String query = 'SELECT Id,ErrorOn__c,Last_Success__c,Last_Try__c,IsSuccess__c FROM CORE_CUBE_Master_Log__c';
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC,List<myMasterLog> myMasterLog) {
        updateMasterLog();
    }

    global void finish(Database.BatchableContext BC)
    {

    }*/


    public static void updateMasterLog(CORE_CUBE_Master_Log__c myMasterLog){
        DateTime todaysDate = Date.today();
        DateTime todaysTime = DateTime.now();
        String todaysDateStr = todaysDate.format('_yyyy_MM_dd');
        DateTime yesterday = todaysDate.AddDays(-1);

        Timezone tz = UserInfo.getTimeZone();//.Timezone.getTimeZone(city);
        Integer offset = tz.getOffset(todaysDate);
        DateTime localDate = todaysDate.addSeconds(-(offset/1000));


        String tmp='';
        Boolean isError=false;

        //today dont exist already
        //CORE_CUBE_Master_Log__c myMasterLog = [SELECT Id,ErrorOn__c,Last_Success__c,Last_Try__c,IsSuccess__c FROM CORE_CUBE_Master_Log__c];
        List<CORE_CUBE_Log__c> myListLog = [SELECT Id,ObjectName__c,isPassed__c FROM CORE_CUBE_Log__c WHERE CreatedDate = TODAY];
        for(CORE_CUBE_Log__c myLog : myListLog){
            if(myLog.isPassed__c==false){
                tmp=tmp+myLog.ObjectName__c+ ', ';
                isError=true;
            }
        }
        if(isError==false){
            myMasterLog.IsSuccess__c=true;
            myMasterLog.ErrorOn__c='';
            myMasterLog.Last_Success__c=localDate;

        }else{
            myMasterLog.IsSuccess__c=false;
            myMasterLog.ErrorOn__c=tmp;
        }

        myMasterLog.Last_Try__c = todaysTime;
        upsert myMasterLog;
    }
}