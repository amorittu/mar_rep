@isTest
private class CORE_WS_Consents_TEST {

    private static final String INTEREST_EXTERNALID = 'setup';

    @testSetup
    static void createData() {
        Id profileId = [SELECT Id FROM Profile WHERE Name = 'CORE_API_Profile' LIMIT 1].Id;

        User usr = new User(
            Username = 'usertest@aoltest.com',
            LastName = 'TEST_USER',
            Email = 'testemail@yopmail.com',
            Alias = 'T',
            CommunityNickname = 'T',
            TimeZoneSidKey = 'Europe/Paris',
            LocaleSidKey = 'fr_FR_EURO',
            EmailEncodingKey = 'ISO-8859-1',
            ProfileId = profileId,
            LanguageLocaleKey = 'fr'
        );
        database.insert(usr, true);

        CORE_CatalogHierarchyModel catalogHierarchy = new CORE_DataFaker_CatalogHierarchy(INTEREST_EXTERNALID).catalogHierarchy;
        Account account = CORE_DataFaker_Account.getStudentAccount(INTEREST_EXTERNALID);

        CORE_Consent__c consent1 = new CORE_Consent__c();
        consent1.CORE_Person_Account__c = account.Id;
        consent1.CORE_Business_Unit__c = catalogHierarchy.businessUnit.Id;
        consent1.CORE_Consent_type__c = 'CORE_Optin_Optout';
        consent1.CORE_Consent_channel__c = 'CORE_PKL_Email';
        consent1.CORE_Scope__c = 'CORE_PKL_BusinessUnit';
        consent1.CORE_Opt_in__c = true;
        consent1.CORE_Double_Opt_in_date__c = datetime.now();
        consent1.CORE_Opt_in_date__c = datetime.now();

        CORE_Consent__c consent2 = new CORE_Consent__c();
        consent2.CORE_Person_Account__c = account.Id;
        consent2.CORE_Business_Unit__c = catalogHierarchy.businessUnit.Id;
        consent2.CORE_Consent_type__c = 'CORE_PKL_Global_consent';
        consent2.CORE_Consent_channel__c = 'CORE_PKL_Fax';
        consent2.CORE_Scope__c = 'CORE_PKL_Brand';
        consent2.CORE_Opt_in__c = true;
        consent2.CORE_Opt_in_date__c = datetime.now();
        List<CORE_Consent__c> consentsList = new List<CORE_Consent__c> { consent1, consent2 };

        database.insert(consentsList, true);

        Test.setCreatedDate(consentsList[0].Id, datetime.now().addDays(1));
        Test.setCreatedDate(consentsList[1].Id, datetime.now().addDays(1));
    }

    @isTest
    static void test_getConsents(){
        User usr = [SELECT Id FROM User WHERE Username = 'usertest@aoltest.com' LIMIT 1];

        List<PermissionSetAssignment> psaList = new List<PermissionSetAssignment>();

        for(PermissionSet ps : [SELECT Id FROM PermissionSet WHERE Name = 'CORE_API_SIS' OR Name = 'CORE_LeadFormAcquisition']){
            psaList.add(
                new PermissionSetAssignment(AssigneeId = usr.id, PermissionSetId = ps.Id)
            );
        }

        database.insert(psaList, true);

        System.runAs(usr){

            CORE_Consent__c consent = [SELECT Id, LastModifiedDate, CreatedDate FROM CORE_Consent__c LIMIT 1];
            system.debug('consent: ' + consent);

            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
            req.requestURI = '/services/apexrest/core/consents';
            req.params.put(CORE_WS_Constants.PARAM_LASTDATE, '2022-04-25T11:16:00Z');
            req.httpMethod = 'GET';

            RestContext.request = req;
            RestContext.response= res;

            test.startTest();
            CORE_WS_Wrapper.ResultWrapperConsents results = CORE_WS_Consents.getConsents();
            test.stopTest();
            System.assertEquals(2, results.consentsList.size());
        }
    }

    @isTest
    static void test_getConsents_next_page(){
        User usr = [SELECT Id FROM User WHERE Username = 'usertest@aoltest.com' LIMIT 1];

        List<PermissionSetAssignment> psaList = new List<PermissionSetAssignment>();

        for(PermissionSet ps : [SELECT Id FROM PermissionSet WHERE Name = 'CORE_API_SIS' OR Name = 'CORE_LeadFormAcquisition']){
            psaList.add(
                new PermissionSetAssignment(AssigneeId = usr.id, PermissionSetId = ps.Id)
            );
        }

        database.insert(psaList, true);

        System.runAs(usr){

            CORE_WS_Consents.limitPerPage = 1;

            List<CORE_Consent__c> consents = [SELECT Id, LastModifiedDate, CreatedDate FROM CORE_Consent__c];

            RestRequest req = new RestRequest();
            RestResponse res = new RestResponse();
            req.requestURI = '/services/apexrest/core/consents';
            req.params.put(CORE_WS_Constants.PARAM_LASTDATE, '2022-04-25T11:16:00Z');
            req.params.put(CORE_WS_Constants.PARAM_LAST_ID, consents[0].Id);
            req.httpMethod = 'GET';

            RestContext.request = req;
            RestContext.response= res;

            test.startTest();
            CORE_WS_Wrapper.ResultWrapperConsents results = CORE_WS_Consents.getConsents();
            test.stopTest();

            System.assertEquals(1, results.consentsList.size());
        }
    }
}