@IsTest
public with sharing class CORE_AOL_OpportunityFeesPaidTuitionTest {
    private static final String AOL_EXTERNALID = 'setupAol';

    private static final String REQ_BODY = '{'
    + '"aolExternalId": "' + AOL_EXTERNALID + '",'
    + '"tuitionFeesPaid": {'
    + '"received": true,'
    + '"paymentType": "CORE_PKL_Partially_Paid",'
    + '"paymentMethod": "CORE_PKL_Cash",'
    + '"receivedDate": "2022-04-05T15:45:14.191Z",'
    + '"acceptanceDate": "2022-04-05T15:45:14.191Z",'
    + '"amount": 0.01,'
    + '"transactionId": "myTransactionId",'
    + '"bank": "myBank",'
    + '"sender": "mySender"'
    + '},'
    + '"additionalFields": {'
    + '"CORE_Campaign_Name__c": "test value"'
    + '},'
    + '"aolProgressionFields": {'
    + '"aolProgression": 50.09,'
    + '"documentProgression": "Started",'
    + '"firstDocumentUploadDate": "2022-03-29",'
    + '"lastAolEventDate": "2022-03-29T08:47:39.689Z",'
    + '"lastDeaAolLoginDate": "2022-03-29T08:47:39.689Z",'
    + '"isAol": true,'
    + '"isAolOnlineRegistered": true,'
    + '"isAolRetailRegistered": true'
    + '}'
    + '}';

    private static final String REQ_BODY_ERROR_NO_ID = '{'
    + '"aolExternalId": "",'
    + '"tuitionFeesPaid": {'
    + '"received": true,'
    + '"paymentType": "CORE_PKL_Partially_Paid",'
    + '"paymentMethod": "CORE_PKL_Cash",'
    + '"receivedDate": "2022-04-05T15:45:14.191Z",'
    + '"acceptanceDate": "2022-04-05T15:45:14.191Z",'
    + '"amount": 0.01,'
    + '"transactionId": "myTransactionId",'
    + '"bank": "myBank",'
    + '"sender": "mySender"'
    + '},'
    + '"additionalFields": {'
    + '"CORE_Campaign_Name__c": "test value"'
    + '},'
    + '"aolProgressionFields": {'
    + '"aolProgression": 50.09,'
    + '"documentProgression": "Started",'
    + '"firstDocumentUploadDate": "2022-03-29",'
    + '"lastAolEventDate": "2022-03-29T08:47:39.689Z",'
    + '"lastDeaAolLoginDate": "2022-03-29T08:47:39.689Z",'
    + '"isAol": true,'
    + '"isAolOnlineRegistered": true,'
    + '"isAolRetailRegistered": true'
    + '}'
    + '}';

    private static final String REQ_BODY_ERROR_INVALID_ID = '{'
    + '"aolExternalId": "invalidID",'
    + '"tuitionFeesPaid": {'
    + '"received": true,'
    + '"paymentType": "CORE_PKL_Partially_Paid",'
    + '"paymentMethod": "CORE_PKL_Cash",'
    + '"receivedDate": "2022-04-05T15:45:14.191Z",'
    + '"acceptanceDate": "2022-04-05T15:45:14.191Z",'
    + '"amount": 0.01,'
    + '"transactionId": "myTransactionId",'
    + '"bank": "myBank",'
    + '"sender": "mySender"'
    + '},'
    + '"additionalFields": {'
    + '"CORE_Campaign_Name__c": "test value"'
    + '},'
    + '"aolProgressionFields": {'
    + '"aolProgression": 50.09,'
    + '"documentProgression": "Started",'
    + '"firstDocumentUploadDate": "2022-03-29",'
    + '"lastAolEventDate": "2022-03-29T08:47:39.689Z",'
    + '"lastDeaAolLoginDate": "2022-03-29T08:47:39.689Z",'
    + '"isAol": true,'
    + '"isAolOnlineRegistered": true,'
    + '"isAolRetailRegistered": true'
    + '}'
    + '}';

    private static final String REQ_BODY_ERROR = '{'
    + '"aolExternalId": "' + AOL_EXTERNALID + '",'
    + '"tuitionFeesPaid": {'
    + '"received": true,'
    + '"paymentType": "CORE_PKL_Partially_Paid",'
    + '"paymentMethod": "invalid_value",'
    + '"receivedDate": "2022-04-05T15:45:14.191Z",'
    + '"acceptanceDate": "2022-04-05T15:45:14.191Z",'
    + '"amount": 0.01,'
    + '"transactionId": "myTransactionId",'
    + '"bank": "myBank",'
    + '"sender": "mySender"'
    + '},'
    + '"additionalFields": {'
    + '"CORE_Campaign_Name__c": "test value"'
    + '},'
    + '"aolProgressionFields": {'
    + '"aolProgression": 50.09,'
    + '"documentProgression": "Started",'
    + '"firstDocumentUploadDate": "2022-03-29",'
    + '"lastAolEventDate": "2022-03-29T08:47:39.689Z",'
    + '"lastDeaAolLoginDate": "2022-03-29T08:47:39.689Z",'
    + '"isAol": true,'
    + '"isAolOnlineRegistered": true,'
    + '"isAolRetailRegistered": true'
    + '}'
    + '}';

    @TestSetup
    static void makeData(){
        CORE_CountrySpecificSettings__c countrySetting = new CORE_CountrySpecificSettings__c(
                CORE_FieldPrefix__c = CORE_AOL_Constants.PREFIX_CORE
        );
        database.insert(countrySetting, true);

        CORE_CatalogHierarchyModel catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test').catalogHierarchy;
        Account account = CORE_DataFaker_Account.getStudentAccount('test');
        Opportunity  opportunity = CORE_DataFaker_Opportunity.getAOLOpportunity(catalogHierarchy.division.Id, 
            catalogHierarchy.businessUnit.Id, 
            account.Id, 
            CORE_AOL_Constants.PKL_OPP_STAGENAME_PROSPECT, 
            CORE_AOL_Constants.PKL_OPP_SUBSTATUS_PROSPECTAPPSTARTED, 
            AOL_EXTERNALID
        );
    }

    @IsTest
    public static void opportunityFeesPaidTuition_Should_Update_Opportunity_and_return_it() {
        CORE_AOL_Wrapper.AOLProgressionFields aolProgressionFields= CORE_DataFaker_AOL_Wrapper.getAolProgressionFieldsWrapper();
        CORE_AOL_Wrapper.TuitionFeesPaid tuitionFeesPaid = CORE_DataFaker_AOL_Wrapper.getTuitionFeesPaidWrapper();

        CORE_AOL_GlobalWorker globalWorker = new CORE_AOL_GlobalWorker(aolProgressionFields,AOL_EXTERNALID);

        Opportunity opportunity = CORE_AOL_OpportunityFeesPaidTuition.opportunityFeesPaidTuitionProcess(globalWorker, tuitionFeesPaid);
        database.update(opportunity, true);

        opportunity = [SELECT Id,CORE_OPP_Tuition_Fee_Received__c,CORE_OPP_Tuition_Fee_Payment_Method__c,CORE_OPP_Tuition_Fee_Received_Date__c,CORE_Tuition_Fee_Acceptance_Date__c,CORE_Tuition_fee_Transaction_ID__c,CORE_Tuition_Fee_Bank__c,CORE_Tuition_Fee_Sender__c,CORE_Tuition_fee_amount__c,CORE_OPP_Tuition_Fee_Payment_Type__c
        FROM Opportunity];

        System.assertEquals(tuitionFeesPaid.amount, opportunity.CORE_Tuition_fee_amount__c);
        System.assertEquals(tuitionFeesPaid.bank, opportunity.CORE_Tuition_Fee_Bank__c);
        System.assertEquals(tuitionFeesPaid.sender, opportunity.CORE_Tuition_Fee_Sender__c);
        System.assertEquals(tuitionFeesPaid.transactionId, opportunity.CORE_Tuition_fee_Transaction_ID__c);
        System.assertEquals(tuitionFeesPaid.paymentMethod, opportunity.CORE_OPP_Tuition_Fee_Payment_Method__c);
        System.assertEquals(tuitionFeesPaid.received, opportunity.CORE_OPP_Tuition_Fee_Received__c);
        System.assertEquals(tuitionFeesPaid.receivedDate, opportunity.CORE_OPP_Tuition_Fee_Received_Date__c);
        System.assertEquals(tuitionFeesPaid.acceptanceDate, opportunity.CORE_Tuition_Fee_Acceptance_Date__c);
        System.assertEquals(tuitionFeesPaid.paymentType, opportunity.CORE_OPP_Tuition_Fee_Payment_Type__c);
    }

    @IsTest
    private static void execute_should_return_an_error_if_mandatory_missing(){
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/aol/opportunity/fees-paid/tuition';

        req.httpMethod = 'PUT';
        req.requestBody = Blob.valueOf(REQ_BODY_ERROR_NO_ID);

        RestContext.request = req;
        RestContext.response= res;

        CORE_AOL_Wrapper.ResultWrapperGlobal result = CORE_AOL_OpportunityFeesPaidTuition.execute();

        System.assertEquals('KO', result.status);

    }

    @IsTest
    private static void execute_should_return_an_error_if_invalid_externalID(){
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/aol/opportunity/fees-paid/tuition';

        req.httpMethod = 'PUT';
        req.requestBody = Blob.valueOf(REQ_BODY_ERROR_INVALID_ID);

        RestContext.request = req;
        RestContext.response= res;

        CORE_AOL_Wrapper.ResultWrapperGlobal result = CORE_AOL_OpportunityFeesPaidTuition.execute();

        System.assertEquals('KO', result.status);

    }

    @IsTest
    private static void execute_should_return_an_error_if_something_goes_wrong(){
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/aol/opportunity/fees-paid/tuition';

        req.httpMethod = 'PUT';
        req.requestBody = Blob.valueOf(REQ_BODY_ERROR);

        RestContext.request = req;
        RestContext.response= res;

        CORE_AOL_Wrapper.ResultWrapperGlobal result = CORE_AOL_OpportunityFeesPaidTuition.execute();

        System.assertEquals('KO', result.status);

    }

    @IsTest
    private static void execute_should_return_a_success_if_all_updates_are_ok(){
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        req.requestURI = '/services/apexrest/aol/opportunity/fees-paid/tuition';

        req.httpMethod = 'PUT';
        req.requestBody = Blob.valueOf(REQ_BODY);

        RestContext.request = req;
        RestContext.response= res;

        CORE_AOL_Wrapper.ResultWrapperGlobal result = CORE_AOL_OpportunityFeesPaidTuition.execute();

        System.assertEquals('OK', result.status);
    }
}