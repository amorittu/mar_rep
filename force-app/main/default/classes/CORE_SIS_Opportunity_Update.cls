@RestResource(urlMapping='/sis/opportunity/*')
global class CORE_SIS_Opportunity_Update {

    @HttpPut
    global static CORE_SIS_Wrapper.ResultWrapperOpportunity updateOpportunity(){

        RestRequest	request = RestContext.request;

        // Initiating logs
        Long dt1 = DateTime.now().getTime();
        Long dt2;
        CORE_LogManager logSettings;
        CORE_LOG__c log = new CORE_LOG__c(CORE_API_Name__c = 'SIS');
        Boolean isErrorLog = false;
        logSettings = new CORE_LogManager('SIS');
        CORE_SIS_Wrapper.ResultWrapperOpportunity result = null;

        try{
            CORE_CountrySpecificSettings__c countrySetting = CORE_CountrySpecificSettings__c.getOrgDefaults();

            String paramId = request.requestURI.substringAfterLast('/');

            if(paramId == ''
               || paramId == null){

                result = new CORE_SIS_Wrapper.ResultWrapperOpportunity(CORE_SIS_Constants.ERROR_STATUS, CORE_SIS_Constants.MSG_MISSING_PARAMETERS + CORE_SIS_Constants.PARAM_SF_ID);
                return result;
            }

            Map<String, Object> paramsMap = (Map<String, Object>)JSON.deserializeUntyped(request.requestBody.toString());
            //system.debug('@updateOpportunity > paramsMap: ' + JSON.serializePretty(paramsMap));

            List<Opportunity> oppsList = CORE_SIS_QueryProvider.getOpportunity(paramId, null, null);
            //system.debug('@updateOpportunity > oppsList GET: ' + JSON.serializePretty(oppsList[0]));

            for(String param : paramsMap.keySet()){
                //system.debug('@updateOpportunity > param: ' + param);

                if(CORE_SIS_QueryProvider.oppsPutFieldsSet.contains(param)
                   || (countrySetting != null
                       && countrySetting.CORE_FieldPrefix__c != null
                       && countrySetting.CORE_FieldPrefix__c != CORE_SIS_Constants.PREFIX_CORE
                       && param.startsWith(countrySetting.CORE_FieldPrefix__c))){

                    oppsList[0] = (Opportunity)CORE_SIS_GlobalWorker.assignValue(oppsList[0], param, paramsMap.get(param));
                    
                }
                else{
                    result = new CORE_SIS_Wrapper.ResultWrapperOpportunity(CORE_SIS_Constants.ERROR_STATUS, 'INVALID FIELD IN REQUEST : ' + param);
                    return result;
                }
            }
            system.debug('@updateOpportunity > oppsList PUT: ' + JSON.serializePretty(oppsList[0]));

            database.update(oppsList[0], true);
            //system.debug('@updateOpportunity > oppsList after update: ' + JSON.serializePretty(oppsList[0]));

            result = new CORE_SIS_Wrapper.ResultWrapperOpportunity(oppsList[0], CORE_SIS_Constants.SUCCESS_STATUS);

        } catch(Exception e){
            result = new CORE_SIS_Wrapper.ResultWrapperOpportunity(CORE_SIS_Constants.ERROR_STATUS, e.getMessage() + ' at ' + e.getStackTraceString());
        } finally{
            dt2 = DateTime.now().getTime();
            System.debug('CORE_AOL_AcademicDiplomaHistory(urlMapping=\'/aol/academic-diploma-history\') : T0TAL TIME OF EXECUTION = ' + (dt2-dt1) + ' ms');
            // creating log
            log.CORE_Processing_Duration__c =(dt2-dt1);
            log.CORE_HTTP_Status_Code__c = result.status == 'KO' ? 400 : result.status == 'OK' ? 200 : 500;
            log.CORE_Sent_Body__c = JSON.serialize(result);
            log.CORE_Received_Body__c =  request.requestBody.toString();
            
            if(!logSettings.isNone()){
                if(logSettings.isAllLogged()){ // is All logged
                    CORE_LogManager.createLog(log);
                }else if(logSettings.isErrorLogged() && isErrorLog){ // is Error
                    CORE_LogManager.createLog(log);
                }
            }
        }

        return result;
    }
}