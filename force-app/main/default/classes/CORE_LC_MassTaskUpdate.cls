public with sharing class CORE_LC_MassTaskUpdate {

    public static final String EMAIL_SETTING_NAME = 'Mass_Task_Update';
    public static final Integer defaultBatchSize = 200;

    public class SettingWrapper {
        @AuraEnabled public String settingId { get; set; }
        @AuraEnabled public Integer batchSize { get; set; }
        @AuraEnabled public Boolean jobInProgress { get; set; }
        @AuraEnabled public String listViewFilter { get; set; }
    }

    public class ListViewWrapper {
        @AuraEnabled public String viewId { get; set; }
        @AuraEnabled public String viewLabel { get; set; }
        @AuraEnabled public String viewName { get; set; }
        @AuraEnabled public String viewQuery { get; set; }
    }

    public class CurrentViewWrapper {
        @AuraEnabled public String viewId { get; set; }
        @AuraEnabled public String viewQuery { get; set; }
        @AuraEnabled public Integer viewRecordsSize { get; set; }
    }

    public class BUWrapper {
        @AuraEnabled public String buId { get; set; }
        @AuraEnabled public String buName { get; set; }
    }
    public class subtypeWrapper {
        @AuraEnabled public String apiname { get; set; }
        @AuraEnabled public String label { get; set; }
    }
    public class ayWrapper {
        @AuraEnabled public String ayapi { get; set; }
        @AuraEnabled public String aylabel { get; set; }
    }
    public class pklWrapper {
        @AuraEnabled public String apiname { get; set; }
        @AuraEnabled public String labelname { get; set; }
    }


    public class AsyncWrapper {
        public Set<Id> remTasksIdSet { get; set; }
        public List<Database.SaveResult> resultsList { get; set; }
    }

    public CORE_LC_MassTaskUpdate() {
    }

    @RemoteAction
    public static List<ListViewWrapper> getListViewsInfo(){
        SettingWrapper orgSetting = getOrgSetting();
        //system.debug('@getListViews > orgSetting: ' + orgSetting);
        //system.debug('@getListViews > userSetting: ' + userSetting);
        System.debug('orgSetting '+orgSetting.listViewFilter);
        List<ListViewWrapper> listViewsList = new List<ListViewWrapper>();
        String filter = '';
        String query = 'SELECT Id, Name, DeveloperName, SobjectType FROM ListView WHERE SobjectType = \'Task\'';

        if(orgSetting.listViewFilter != null){
            filter = '%' + orgSetting.listViewFilter + '%';
            query += ' AND DeveloperName LIKE :filter';
        }
        System.debug('query '+query);
        //Get list views of object
        for(ListView lv : database.query(query)){
            ListViewWrapper newLV = new ListViewWrapper();
            System.debug('list view '+ lv.Id);
            newLV.viewId = lv.Id;
            newLV.viewLabel = lv.Name;
            newLV.viewName = lv.DeveloperName;
            //here
            HttpRequest req = new HttpRequest();
            req.setEndpoint('callout:CORE_MassOpportunityEndpoint' + '/Task/listviews/' + lv.Id + '/describe');
            //req.setEndpoint('https://ggecs--mvpdev.my.salesforce.com/services/data/v50.0/sobjects/Opportunity/listviews/' + lv.Id + '/describe');
            req.setHeader('Authorization', 'Bearer ' + userinfo.getSessionId());
            req.setMethod('GET');
            Http http = new Http();
            HttpResponse res = http.send(req);

            if(res.getStatusCode() == 200){
                
                Map<String, Object> tokenRespMap = (Map<String, Object>)JSON.deserializeUntyped(res.getBody());

                newLV.viewQuery = (String)tokenRespMap.get('query');//.replaceAll('\'', '"');
            }

            listViewsList.add(newLV);
        }

        system.debug('@getTaskListViews > listViewsList: ' + JSON.serializePretty(listViewsList));

        return listViewsList;
    }

    @AuraEnabled (cacheable=true)
    public static SettingWrapper getOrgSetting(){

        SettingWrapper settingWrap = new SettingWrapper();

        Core_MassTasksUpdate__c orgsetting = Core_MassTasksUpdate__c.getOrgDefaults();

        if(orgsetting.Id == null){
            AuraHandledException e = new AuraHandledException(Label.CORE_MTU_MSG_NO_SETTINGS);
            e.setMessage(Label.CORE_MTU_MSG_NO_SETTINGS);
            throw e;
        }

        settingWrap.settingId = orgsetting.Id;
        settingWrap.batchSize = (orgsetting.CORE_BatchSize__c != null) ? orgsetting.CORE_BatchSize__c.intValue() :defaultBatchSize;
        settingWrap.listViewFilter = orgsetting.CORE_ListViewNameFilter__c;
        settingWrap.jobInProgress = orgsetting.CORE_JobInProgress__c;

        return settingWrap;
    }

    /*@AuraEnabled (cacheable=true)
    public static List<ListViewWrapper> getListViews(){
        try {

            SettingWrapper orgSetting = getOrgSetting();
            //system.debug('@getListViews > orgSetting: ' + orgSetting);
            //system.debug('@getListViews > userSetting: ' + userSetting);

            List<ListViewWrapper> listViewsList = new List<ListViewWrapper>();
            String filter = '';
            String query = 'SELECT Id, Name, DeveloperName, SobjectType FROM ListView WHERE SobjectType = \'Opportunity\'';

            if(orgSetting.listViewFilter != null){
                filter = '%' + orgSetting.listViewFilter + '%';
                query += ' AND DeveloperName LIKE :filter';
            }

            //Get list views of object
            for(ListView lv : database.query(query)){
                ListViewWrapper newLV = new ListViewWrapper();

                newLV.viewId = lv.Id;
                newLV.viewLabel = lv.Name;
                newLV.viewName = lv.DeveloperName;

                listViewsList.add(newLV);
            }

            //system.debug('@getOppListViews > listViewsList: ' + JSON.serializePretty(listViewsList));

            return listViewsList;

        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }*/

    @AuraEnabled (cacheable=true)
    public static CurrentViewWrapper getListViewData(Object view) {
        system.debug('@getListViewData > view: ' + view);

        try{
            Map<String, Object> viewsMap = (Map<String, Object>)JSON.deserializeUntyped(JSON.serialize(view));
            system.debug('@getListViewData > viewsMap: ' + viewsMap);

            CurrentViewWrapper newCurrentView = new CurrentViewWrapper();
            newCurrentView.viewId = (String)viewsMap.get('value');
            newCurrentView.viewQuery = ((String)viewsMap.get('query')).unescapeHtml4();
            newCurrentView.viewRecordsSize = database.query(newCurrentView.viewQuery).size();

            return newCurrentView;
        }
        catch(Exception e){
            throw new AuraHandledException(e.getMessage() + ' => ' + e.getStackTraceString());
        }
    }

    

    @AuraEnabled (cacheable=true)
    public static List<BUWrapper> getBUs(CurrentViewWrapper viewSelected) {
        system.debug('@getBUs > viewSelected: ' + viewSelected);

        List<BUWrapper> bulist = new List<BUWrapper>();
        Set<Id> oppIdSet = new Set<Id>();
        if(!viewSelected.viewQuery.containsIgnoreCase('WhatId')){
            viewSelected.viewQuery = viewSelected.viewQuery.replace('SELECT', 'SELECT WhatId,');
        }

        for(Task task : ((List<Task>)database.query(viewSelected.viewQuery.unescapeHtml4()))){
            if(task.WhatId != null){
                oppIdSet.add(task.WhatId);
            }
        }
        system.debug('@getBUs > oppIdSet: ' + oppIdSet);
        Set<Id> buAlreadyUsed = new Set<Id>();
        if(oppIdSet.size() > 0){
            for(Opportunity opp : [SELECT Id, CORE_OPP_Business_Unit__c, CORE_OPP_Business_Unit__r.Name  FROM Opportunity WHERE Id IN :oppIdSet]){
                if(buAlreadyUsed.contains(opp.CORE_OPP_Business_Unit__c)){
                    continue;
                }
                
                BUWrapper buWrap = new BUWrapper();

                buWrap.buId = opp.CORE_OPP_Business_Unit__c;
                buWrap.buName = opp.cORE_OPP_Business_Unit__r.Name;
                bulist.add(buWrap);
                buAlreadyUsed.add(opp.CORE_OPP_Business_Unit__c);
            }
        }
        System.debug('bulist'+bulist);
        return bulist;
    }
    @AuraEnabled (cacheable=true)
    public static List<AYWrapper> getAYs(CurrentViewWrapper viewSelected,String bu) {
        system.debug('@getBUs > viewSelected: ' + viewSelected);

        List<AYWrapper> aylist = new List<AYWrapper>();
        Set<Id> oppIdSet = new Set<Id>();
        if(!viewSelected.viewQuery.containsIgnoreCase('WhatId')){
            viewSelected.viewQuery = viewSelected.viewQuery.replace('SELECT', 'SELECT WhatId,');
        }

        for(Task task : ((List<Task>)database.query(viewSelected.viewQuery.unescapeHtml4()))){
            if(task.WhatId != null){
                oppIdSet.add(task.WhatId);
            }
        }
        
        Set<String> ayAlreadyUsed = new Set<String>();
        Map<String,String> MapValueLabel = new Map<String,String>();
        Schema.DescribeFieldResult fieldResult = Opportunity.CORE_Opp_Academic_Year__c.getDescribe();
        List<Schema.PicklistEntry> values = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry v : values) {
            MapValueLabel.put(v.getValue(),v.getLabel());        
        }
        if(oppIdSet.size() > 0){
            for(Opportunity opp : [SELECT Id, CORE_OPP_Academic_Year__c  FROM Opportunity WHERE Id IN :oppIdSet AND CORE_Opp_Business_Unit__c = :bu ORDER BY CORE_Opp_Academic_Year__c]){
                if(ayAlreadyUsed.contains(opp.CORE_OPP_Academic_Year__c)){
                    continue;
                }
                
                AYWrapper ayWrap = new AYWrapper();

                ayWrap.ayapi = opp.CORE_OPP_Academic_Year__c;
                ayWrap.aylabel = MapValueLabel.get(opp.CORE_OPP_Academic_Year__c);
                aylist.add(ayWrap);
                ayAlreadyUsed.add(opp.CORE_OPP_Academic_Year__c);
            }
        }
        System.debug('aylist'+aylist);
        return aylist;
    }
    
    @AuraEnabled (cacheable=true)
    public static List<SubTypeWrapper> getTaskSubtype(CurrentViewWrapper viewSelected,String bu,String ay) {
        system.debug('@getTaskSubtype > viewSelected: ' + viewSelected);
        List<SubTypeWrapper> subtypelist = new List<SubTypeWrapper>();
        Map<Id, Task> tasksMap = new Map<Id, Task>(
                ((List<Task>)database.query(viewSelected.viewQuery.unescapeHtml4()))
            );
        Set<Id> tasksListId = tasksMap.keySet();
        List<Opportunity> oppsId= [SELECT Id,CORE_OPP_Business_Unit__c,CORE_Opp_Academic_Year__c FROM Opportunity WHERE CORE_Opp_Business_Unit__c = :bu AND CORE_Opp_Academic_Year__c = :ay];
        /*if(!viewSelected.viewQuery.containsIgnoreCase('CORE_TECH_CallTaskType__c')){
            viewSelected.viewQuery = viewSelected.viewQuery.replace('SELECT', 'SELECT CORE_TECH_CallTaskType__c,');
        }*/
        Map<String,String> MapValueLabel = new Map<String,String>();
        Schema.DescribeFieldResult fieldResult = Task.CORE_TECH_CallTaskType__c.getDescribe();
        List<Schema.PicklistEntry> values = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry v : values) {
            MapValueLabel.put(v.getValue(),v.getLabel());        
        }
        Set<String> subtypeAlreadyUsed = new Set<String>();
        String query = 'SELECT Id, WhatId,CORE_TECH_CallTaskType__c'
                     + ' FROM Task'
                     + ' WHERE Id IN :tasksListId AND WhatId IN :oppsId';
        for(Task task : database.query(query)){
            if( subtypeAlreadyUsed.contains(task.CORE_TECH_CallTaskType__c)){
                continue;
            }
            SubTypeWrapper stWrap = new SubTypeWrapper();
            stWrap.apiname = task.CORE_TECH_CallTaskType__c;
            stWrap.label = MapValueLabel.get(task.CORE_TECH_CallTaskType__c);
            subtypelist.add(stWrap);
            subtypeAlreadyUsed.add(task.CORE_TECH_CallTaskType__c);
            }
        
        
        return subtypelist;
    }
    @AuraEnabled (cacheable=true)
    public static List<pklWrapper> getPicklistValues (String objectName, String fieldName) {
        List<pklWrapper> picklistValuesApiName = new  List<pklWrapper>();

        Schema.SObjectType s = Schema.getGlobalDescribe().get(objectName) ;
        Schema.DescribeSObjectResult r = s.getDescribe() ;
        Map<String,Schema.SObjectField> fields = r.fields.getMap() ;
        Schema.DescribeFieldResult fieldResult = fields.get(fieldName).getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();

        for(Schema.PicklistEntry pickListVal : ple){
            pklWrapper pkl = new pklWrapper();
            pkl.apiname = pickListVal.getValue();
            pkl.labelname = pickListVal.getLabel();
            picklistValuesApiName.add(pkl);
        }

        return picklistValuesApiName;
    }
    @AuraEnabled (cacheable=true)
    public static Integer getNumberOfTasks(CurrentViewWrapper viewSelected,String buId,String ay,String subtype){
        List<SubTypeWrapper> subtypelist = new List<SubTypeWrapper>();
        Map<Id, Task> tasksMap = new Map<Id, Task>(
                ((List<Task>)database.query(viewSelected.viewQuery.unescapeHtml4()))
            );
        Set<Id> tasksListId = tasksMap.keySet();
        List<Opportunity> oppsId= [SELECT Id,CORE_OPP_Business_Unit__c,CORE_Opp_Academic_Year__c FROM Opportunity WHERE CORE_Opp_Business_Unit__c = :buId AND CORE_Opp_Academic_Year__c = :ay];
        Integer i = [SELECT count() FROM Task WHERE Id IN :tasksListId AND WhatId IN :oppsId AND CORE_TECH_CallTaskType__c = :subtype];
        return i;
    }
    
    /*@AuraEnabled (cacheable=true)
    public static List<String> getSubtype(CurrentViewWrapper viewSelected) {
        List<String> subtypelist = new List<String>();
        if(!viewSelected.viewQuery.containsIgnoreCase('TaskSubtype')){
            viewSelected.viewQuery = viewSelected.viewQuery.replace('SELECT', 'SELECT TaskSubtype,');
        }
        for(Task task : ((List<Task>)database.query(viewSelected.viewQuery.unescapeHtml4()))){
            if(task.TaskSubtype != null){
                subtypelist.add(task.TaskSubtype);
            }
        }
        return subtypelist;
    }*/

    @AuraEnabled
    public static string runUpdateTasks(CurrentViewWrapper viewSelected, String buId, String academicYear, String subtype,String status,String result,String lossreason){
        buId = buId.replaceAll('"', '');
        try {

            SettingWrapper setting = getOrgSetting();
            Map<Id, Task> tasksMap = new Map<Id, Task>(
                ((List<Task>)database.query(viewSelected.viewQuery.unescapeHtml4()))
            );
            System.debug('tasks Map '+ tasksMap);

            List<String> resultsList = new List<String> {
                String.valueOf(tasksMap.size()), '0', '0', ''
            };
            System.debug('batch size '+setting.batchSize + '>= '+ viewSelected.viewRecordsSize );
            if(setting.batchSize >= viewSelected.viewRecordsSize){//Run sync. update
                updateTasks(tasksMap.keySet(), buId, academicYear,subtype,status,result,lossreason,null, resultsList,true);

                return null;
            }
            else{//Run async. update

                database.update(new Core_MassTasksUpdate__c(
                    Id = setting.settingId, CORE_JobInProgress__c = true
                ), true);
                System.debug('asynchronous job');
                System.enqueueJob(new CORE_QU_MassTaskUpdate(tasksMap.keySet(), buId, academicYear,subtype,status,result,lossreason, resultsList));

                return 'ASYNC';
            }
            
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }

    public static AsyncWrapper updateTasks(Set<Id> tasksListId, String buId, 
                                                  String academicYear,String subtype,String status,String result,String lossreason,Integer batchSize,
                                                  List<String> resultsList,Boolean sendEmail){
        System.debug('starting update Tasks '+ tasksListId);
        List<Task> updatetasksList = new List<Task>();
        Set<Id> remTasksIdSet = tasksListId;
        List<Opportunity> oppsId= [SELECT Id,CORE_OPP_Business_Unit__c,CORE_Opp_Academic_Year__c FROM Opportunity WHERE CORE_Opp_Business_Unit__c = :buId AND CORE_Opp_Academic_Year__c = :academicYear];
        System.debug('starting opportunities  '+ oppsId);
        String query = 'SELECT Id, WhatId,Status'
                     + ' FROM Task'
                     + ' WHERE Id IN :tasksListId AND WhatId IN :oppsId AND CORE_TECH_CallTaskType__c = :subtype';
        if(batchSize != null){
                        query += ' LIMIT :batchSize';
        }
        System.debug('query '+query);
        for(Task task : database.query(query)){
            Task tache = new Task( Id = task.Id);
            
            tache.Status = status;
            if(status == 'CORE_PKL_Completed'){
                tache.Core_Task_Result__c = result;
            }
            if((lossreason != null) && (!String.IsBlank(lossreason)) ){
                tache.CORE_Loss_Reason__c = lossreason;
            }
            updatetasksList.add(tache);
            remTasksIdSet.remove(task.Id);
        }
        List<Database.SaveResult> results = database.update(updatetasksList, false);
        List<Task> taches = [SELECT Id, WhatId,Status,Core_Task_Result__c  FROM Task];
        //System.debug('update done '+ taches);
        if(sendEmail){
            CORE_MassTaskUpdateHandler resultHandler = new CORE_MassTaskUpdateHandler();
            resultHandler.settingName = EMAIL_SETTING_NAME;
            resultHandler.resultsList = resultsList;
            resultHandler.sendMail();
            
        }
        
        
        
        AsyncWrapper asynWrap = new AsyncWrapper();
        asynWrap.remTasksIdSet = remTasksIdSet;
        asynWrap.resultsList = results;
        return asynWrap;
        
    }
}