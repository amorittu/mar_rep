/**
 * @author Valentin Pitel
 * @create date 2022-05-09 16:40:22
 * @modify date 2022-05-09 16:40:22
 * @desc Test class for CORE_Flow_UpsertRecords class.
 */
@IsTest
private class CORE_Flow_UpsertRecordsTest {
    private static final String EXTERNAL_FIELD = 'CORE_Data_Migration_External_ID__c';

    @IsTest
    static void initUpsertTest_Success() {
        Id rtId = Schema.SObjectType.Account.getRecordTypeInfosByDeveloperName().get('CORE_Business_Account').getRecordTypeId();
        Account acc = new Account(
            Name = 'TestAccount',
            recordTypeId = rtId
        );
        acc.put(EXTERNAL_FIELD, '007');

        Test.startTest();
        CORE_Flow_UpsertRecords.ActionOutput[] outputs = CORE_Flow_UpsertRecords.initUpsert(getActionInputs(new List<Account>{acc},'Account'));
        Test.stopTest();

        System.assertEquals(
            true, 
            outputs[0].isSuccess,
            'Upsert operation is expected to succeed.'
        );

        System.assertNotEquals(
            null, 
            outputs[0].records[0].Id,
            'Record is expected to be returned with his associated id'
        );
    }

    private static CORE_Flow_UpsertRecords.ActionInput[] getActionInputs(List<SObject> records, String objectApiName) {

        CORE_Flow_UpsertRecords.ActionInput input = new CORE_Flow_UpsertRecords.ActionInput();
        input.records = records;
        input.objectApiName = objectApiName;
        input.externalIdField = EXTERNAL_FIELD;

        return new CORE_Flow_UpsertRecords.ActionInput[]{input};
    }
}