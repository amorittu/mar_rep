@IsTest
public class CORE_AOL_OpportunityReorientTest {
    private static final String AOL_EXTERNALID = 'setupAol';

    @TestSetup
    static void makeData(){
        CORE_CatalogHierarchyModel catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('setup').catalogHierarchy;
        Account account = CORE_DataFaker_Account.getStudentAccount('setup');

        Opportunity  opportunity = CORE_DataFaker_Opportunity.getAOLOpportunity(
            catalogHierarchy.division.Id, 
            catalogHierarchy.businessUnit.Id,
            catalogHierarchy.studyArea.Id, 
            catalogHierarchy.curriculum.Id, 
            catalogHierarchy.level.Id,
            catalogHierarchy.intake.Id,
            account.Id, 
            CORE_AOL_Constants.PKL_OPP_STAGENAME_PROSPECT, 
            CORE_AOL_Constants.PKL_OPP_SUBSTATUS_PROSPECTAPPSTARTED, 
            AOL_EXTERNALID
        );
    }

    @IsTest
    private static void execute_should_return_a_success_if_all_upsert_are_ok(){
        String formerAolExternalId = AOL_EXTERNALID;
        String newAolExternalId = 'test2';
        CORE_CatalogHierarchyModel catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test1').catalogHierarchy;
        Account account = [SELECT Id FROM Account];
        CORE_AOL_Wrapper.AOLProgressionFields aolProgressionFields = CORE_DataFaker_AOL_Wrapper.getAolProgressionFieldsWrapper();
        CORE_AOL_GlobalWorker aolWorker = new CORE_AOL_GlobalWorker(aolProgressionFields, formerAolExternalId);

        CORE_AOL_Wrapper.InterestWrapper interest = CORE_DataFaker_AOL_Wrapper.geInterestWrapper(catalogHierarchy, true, account.Id);

        CORE_AOL_Wrapper.ResultWrapper result = CORE_AOL_OpportunityReorient.execute(
            formerAolExternalId, 
            newAolExternalId, 
            interest,
            aolProgressionFields
        );

        System.assertEquals('OK', result.statut);
    }

    @IsTest
    private static void execute_should_return_an_error_if_all_upsert_are_not_ok(){
        String formerAolExternalId = 'test1';
        String newAolExternalId = 'test2';
        CORE_CatalogHierarchyModel catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test1').catalogHierarchy;
        Account account = [SELECT Id FROM Account];
        CORE_AOL_Wrapper.AOLProgressionFields aolProgressionFields = CORE_DataFaker_AOL_Wrapper.getAolProgressionFieldsWrapper();
        CORE_AOL_GlobalWorker aolWorker = new CORE_AOL_GlobalWorker(aolProgressionFields, formerAolExternalId);

        CORE_AOL_Wrapper.InterestWrapper interest = CORE_DataFaker_AOL_Wrapper.geInterestWrapper(catalogHierarchy, true, account.Id);

        CORE_AOL_Wrapper.ResultWrapper result = CORE_AOL_OpportunityReorient.execute(
            formerAolExternalId, 
            newAolExternalId, 
            interest,
            aolProgressionFields
        );

        System.assertEquals('KO', result.statut);
    }

    @IsTest
    private static void construtor_Should_initialize_attributes() {
        String formerAolExternalId = 'test1';
        String newAolExternalId = 'test2';
        CORE_CatalogHierarchyModel catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test1').catalogHierarchy;
        Account account = [SELECT Id FROM Account];
        CORE_AOL_Wrapper.AOLProgressionFields aolProgressionFields = CORE_DataFaker_AOL_Wrapper.getAolProgressionFieldsWrapper();
        CORE_AOL_GlobalWorker aolWorker = new CORE_AOL_GlobalWorker(aolProgressionFields, formerAolExternalId);

        CORE_AOL_Wrapper.InterestWrapper interest = CORE_DataFaker_AOL_Wrapper.geInterestWrapper(catalogHierarchy, true, account.Id);
        CORE_AOL_OpportunityReorient reorientProcess = new CORE_AOL_OpportunityReorient(aolWorker, formerAolExternalId, newAolExternalId, interest);

        System.assertEquals(aolWorker, reorientProcess.aolWorker);
        System.assertNotEquals(null, reorientProcess.dataMaker);
        System.assertEquals(formerAolExternalId, reorientProcess.formerAolExternalId);
        System.assertEquals(newAolExternalId, reorientProcess.newAolExternalId);
        System.assertEquals(interest, reorientProcess.interest);
    }

    @IsTest
    private static void moveOpportunityToExitReoriented_should_update_substatus() {
        Opportunity opportunity = new Opportunity(
            StageName = CORE_AOL_Constants.PKL_OPP_STAGENAME_LEAD, 
            CORE_OPP_Sub_Status__c = CORE_AOL_Constants.PKL_OPP_SUBSTATUS_LEADAPPSTARTED
        );
        
        opportunity = CORE_AOL_OpportunityReorient.moveOpportunityToExitReoriented(opportunity);

        System.assertNotEquals(null, opportunity.CORE_OPP_Sub_Status__c);
        System.assertNotEquals(CORE_AOL_Constants.PKL_OPP_SUBSTATUS_LEADAPPSTARTED, opportunity.CORE_OPP_Sub_Status__c);
    }

    @IsTest
    private static void reorientInternally_should_return_opportunity_reoriented(){
        String formerAolExternalId = AOL_EXTERNALID;
        String newAolExternalId = 'test2';
        CORE_CatalogHierarchyModel catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test1').catalogHierarchy;
        Account account = [SELECT Id FROM Account];
        CORE_AOL_Wrapper.AOLProgressionFields aolProgressionFields = CORE_DataFaker_AOL_Wrapper.getAolProgressionFieldsWrapper();
        CORE_AOL_GlobalWorker aolWorker = new CORE_AOL_GlobalWorker(aolProgressionFields, formerAolExternalId);

        CORE_AOL_Wrapper.InterestWrapper interest = CORE_DataFaker_AOL_Wrapper.geInterestWrapper(catalogHierarchy, true, account.Id);
        CORE_AOL_OpportunityReorient reorientProcess = new CORE_AOL_OpportunityReorient(aolWorker, formerAolExternalId, newAolExternalId, interest);

        Opportunity result = reorientProcess.reorientInternally();

        System.assertEquals(true, result.CORE_OPP_Application_Online__c);
        System.assertEquals(newAolExternalId, result.CORE_OPP_Application_Online_ID__c);
        System.assertEquals(true, result.CORE_Is_AOL_Register__c);
        System.assertEquals(true, result.CORE_AOL_Re_Orientation__c);
        System.assertEquals(null, result.Id);
        System.assertEquals(false, reorientProcess.aolWorker.aolOpportunity.CORE_AOL_Re_Orientation__c);
        System.assertNotEquals(null, reorientProcess.aolWorker.aolOpportunity.Id);
    }

    @IsTest
    private static void reorientByUpdate_should_return_the_same_opp_updated(){
        String formerAolExternalId = AOL_EXTERNALID;
        String newAolExternalId = 'test2';
        CORE_CatalogHierarchyModel catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test1').catalogHierarchy;
        Account account = [SELECT Id FROM Account];
        CORE_AOL_Wrapper.AOLProgressionFields aolProgressionFields = CORE_DataFaker_AOL_Wrapper.getAolProgressionFieldsWrapper();
        CORE_AOL_GlobalWorker aolWorker = new CORE_AOL_GlobalWorker(aolProgressionFields, formerAolExternalId);

        CORE_AOL_Wrapper.InterestWrapper interest = CORE_DataFaker_AOL_Wrapper.geInterestWrapper(catalogHierarchy, true, account.Id);
        CORE_AOL_OpportunityReorient reorientProcess = new CORE_AOL_OpportunityReorient(aolWorker, formerAolExternalId, newAolExternalId, interest);

        Opportunity  newOpportunity = CORE_DataFaker_Opportunity.getAOLOpportunity(
            catalogHierarchy.division.Id, 
            catalogHierarchy.businessUnit.Id,
            catalogHierarchy.studyArea.Id, 
            catalogHierarchy.curriculum.Id, 
            catalogHierarchy.level.Id,
            catalogHierarchy.intake.Id,
            account.Id, 
            CORE_AOL_Constants.PKL_OPP_STAGENAME_PROSPECT, 
            CORE_AOL_Constants.PKL_OPP_SUBSTATUS_PROSPECTAPPSTARTED, 
            null
        );

        Opportunity result = reorientProcess.reorientByUpdate(newOpportunity);

        System.assertEquals(true, result.CORE_OPP_Application_Online__c);
        System.assertEquals(newAolExternalId, result.CORE_OPP_Application_Online_ID__c);
        System.assertEquals(true, result.CORE_Is_AOL_Register__c);
        System.assertEquals(true, result.CORE_AOL_Re_Orientation__c);
        System.assertEquals(interest.isAolRetailRegistered, result.CORE_Is_AOL_Retail_Register__c);
        System.assertEquals(newOpportunity.Id, result.Id);

        //System.assertEquals(null, result.Id);
    }

    @IsTest
    private static void checkMandatoryParameters_should_return_null_if_there_is_no_missing_parameters(){
        String formerAolExternalId = 'test1';
        String newAolExternalId = 'test2';
        CORE_CatalogHierarchyModel catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test1').catalogHierarchy;
        Account account = [SELECT Id FROM Account];

        CORE_AOL_Wrapper.InterestWrapper interest = CORE_DataFaker_AOL_Wrapper.geInterestWrapper(catalogHierarchy, true, account.Id);
        CORE_AOL_Wrapper.ResultWrapper result = CORE_AOL_OpportunityReorient.checkMandatoryParameters(formerAolExternalId, newAolExternalId, interest);

        System.assertEquals(null, result);
    }

    @IsTest
    private static void checkMandatoryParameters_should_return_a_resultWrapper_if_there_is_missing_parameters(){
        String formerAolExternalId = 'test1';
        String newAolExternalId = 'test2';
        CORE_CatalogHierarchyModel catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test1').catalogHierarchy;
        Account account = [SELECT Id FROM Account];

        CORE_AOL_Wrapper.InterestWrapper interest = CORE_DataFaker_AOL_Wrapper.geInterestWrapper(catalogHierarchy, true, account.Id);
        CORE_AOL_Wrapper.ResultWrapper result = CORE_AOL_OpportunityReorient.checkMandatoryParameters(formerAolExternalId, newAolExternalId, null);

        System.assertNotEquals(null, result);
        System.assertEquals(CORE_AOL_Constants.ERROR_STATUS, result.statut);
        System.assert(result.message.contains('interest'));

        result = CORE_AOL_OpportunityReorient.checkMandatoryParameters(null, newAolExternalId, null);
        System.assertNotEquals(null, result);
        System.assertEquals(CORE_AOL_Constants.ERROR_STATUS, result.statut);
        System.assert(result.message.contains('interest'));
        System.assert(result.message.contains('formerAolExternalId'));

        result = CORE_AOL_OpportunityReorient.checkMandatoryParameters(null, null, null);
        System.assertNotEquals(null, result);
        System.assertEquals(CORE_AOL_Constants.ERROR_STATUS, result.statut);
        System.assert(result.message.contains('interest'));
        System.assert(result.message.contains('formerAolExternalId'));
        System.assert(result.message.contains('newAolExternalId'));
    }

    @IsTest
    private static void duplicateRelatedDatas_should_duplicate_related_datas_from_the_former_opportunity_to_the_new_opportunity(){
        String formerAolExternalId = AOL_EXTERNALID;
        String newAolExternalId = 'test2';
        
        CORE_CatalogHierarchyModel catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test1').catalogHierarchy;

        Account account = [SELECT Id, PersonContactId FROM Account];

        Opportunity  newOpportunity = CORE_DataFaker_Opportunity.getAOLOpportunity(
            catalogHierarchy.division.Id, 
            catalogHierarchy.businessUnit.Id,
            catalogHierarchy.studyArea.Id, 
            catalogHierarchy.curriculum.Id, 
            catalogHierarchy.level.Id,
            catalogHierarchy.intake.Id,
            account.Id, 
            CORE_AOL_Constants.PKL_OPP_STAGENAME_PROSPECT, 
            CORE_AOL_Constants.PKL_OPP_SUBSTATUS_PROSPECTAPPSTARTED, 
            newAolExternalId
        );

        Delete [Select Id from CORE_Sub_Status_Timestamp_History__c where CORE_Opportunity__c =: newOpportunity.Id];
        Delete [Select Id from CORE_Status_Timestamp_History__c where CORE_Opportunity__c =: newOpportunity.Id];

        CORE_AOL_Wrapper.AOLProgressionFields aolProgressionFields = CORE_DataFaker_AOL_Wrapper.getAolProgressionFieldsWrapper();
        CORE_AOL_GlobalWorker aolWorker = new CORE_AOL_GlobalWorker(aolProgressionFields, formerAolExternalId);

        Opportunity oldOpportunity = aolWorker.aolOpportunity;

        CORE_DataFaker_Opportunity.getStatusHistory(oldOpportunity.Id, CORE_AOL_Constants.PKL_OPP_STAGENAME_PROSPECT, formerAolExternalId);
        CORE_DataFaker_Opportunity.getSubStatusHistory(oldOpportunity.Id, CORE_AOL_Constants.PKL_OPP_STAGENAME_PROSPECT, CORE_AOL_Constants.PKL_OPP_SUBSTATUS_PROSPECTAPPSTARTED, formerAolExternalId);
        CORE_DataFaker_ContentDocumentLink.getContentDocumentLink(oldOpportunity.Id);
        OpportunityContactRole contactRole = new OpportunityContactRole(
            IsPrimary = true,
            ContactId = account.PersonContactId,
            CORE_External_Id__c = formerAolExternalId,
            OpportunityId = oldOpportunity.Id,
            Role = 'CORE_PKL_Student'
        );
        Insert contactRole;


        CORE_AOL_Wrapper.InterestWrapper interest = CORE_DataFaker_AOL_Wrapper.geInterestWrapper(catalogHierarchy, true, account.Id);
        CORE_AOL_OpportunityReorient reorientProcess = new CORE_AOL_OpportunityReorient(aolWorker, formerAolExternalId, newAolExternalId, interest);

        test.startTest();
        CORE_AOL_OpportunityReorient.duplicateRelatedDatas(oldOpportunity.Id, newOpportunity.Id);
        test.stopTest();

        List<CORE_Sub_Status_Timestamp_History__c> newSubStatus = [Select Id from CORE_Sub_Status_Timestamp_History__c where CORE_Opportunity__c =: newOpportunity.Id];
        List<CORE_Status_Timestamp_History__c> newStatus =[Select Id from CORE_Status_Timestamp_History__c where CORE_Opportunity__c =: newOpportunity.Id];
        List<ContentDocumentLink> attachments =[Select Id from ContentDocumentLink where LinkedEntityId =: newOpportunity.Id];
        List<OpportunityContactRole> contactRoles =[Select Id from OpportunityContactRole where OpportunityId =: newOpportunity.Id];
        
        System.assertEquals(false, newSubStatus.isEmpty());
        System.assertEquals(false, newStatus.isEmpty());
        System.assertEquals(false, attachments.isEmpty());
        System.assertEquals(false, contactRoles.isEmpty());
    }
}