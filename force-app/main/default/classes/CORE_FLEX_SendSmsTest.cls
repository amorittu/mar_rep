@isTest
public class CORE_FLEX_SendSmsTest {
    @isTest static void testSendSms() {
        List<Integer> res = CORE_FLEX_SendSms.execute(null);
        System.assertEquals(-1, res.get(0));

        CORE_FLEX_SendSms.FlowInput input = new CORE_FLEX_SendSms.FlowInput();        
        res = CORE_FLEX_SendSms.execute(new List<CORE_FLEX_SendSms.FlowInput>{input});
        System.assertEquals(-1, res.get(0));

        input.phone = '0102030405';        
        res = CORE_FLEX_SendSms.execute(new List<CORE_FLEX_SendSms.FlowInput>{input});
        System.assertEquals(-1, res.get(0));

        input.phone = '0102030405';
        input.account = new Account();
        input.account.Name = 'name';
        res = CORE_FLEX_SendSms.execute(new List<CORE_FLEX_SendSms.FlowInput>{input});
        System.assertEquals(-1, res.get(0));

    }
}