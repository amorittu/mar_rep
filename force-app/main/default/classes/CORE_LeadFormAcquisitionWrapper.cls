global class CORE_LeadFormAcquisitionWrapper {
	global class GlobalWrapper{
		global PersonAccountWrapper personAccount;
		global List<ConsentWrapper> consents;
		global OpportunityWrapper mainInterest;
		global string prospectiveStudentComment;
		global OpportunityWrapper secondInterest;
		global OpportunityWrapper thirdInterest;
		global PointOfContactWrapper pointOfContact;
		global CampaignSubscriptionWrapper campaignSubscription;
		global AcademicDiplomaHistoryWrapper academicDiplomaHistory;

		global GlobalWrapper(){
			this.personAccount = new PersonAccountWrapper();
			this.consents = new List<ConsentWrapper>();
			this.mainInterest = new OpportunityWrapper();
			this.prospectiveStudentComment = '';
			this.secondInterest = new OpportunityWrapper();
			this.thirdInterest = new OpportunityWrapper();
			this.pointOfContact = new PointOfContactWrapper();
			this.campaignSubscription = new CampaignSubscriptionWrapper();
			this.academicDiplomaHistory = new AcademicDiplomaHistoryWrapper();
		}
	}

	global class ErrorWrapper{
		global List<String> fields;
		global String message;
		global String errorCode;
		global Datetime apiCallDate;

		global ErrorWrapper(List<String> fields, String message, String errorCode, Datetime apiCallDate){
			this.fields = fields;
			this.message = message;
			this.errorCode = errorCode;
			this.apiCallDate = apiCallDate;
		}

		global ErrorWrapper(String field, String message, String errorCode, Datetime apiCallDate){
			this.fields = new List<String> {field};
			this.message = message;
			this.errorCode = errorCode;
			this.apiCallDate = apiCallDate;
		}

		global ErrorWrapper(String message, String errorCode, Datetime apiCallDate){
			this.fields = null;
			this.message = message;
			this.errorCode = errorCode;
			this.apiCallDate = apiCallDate;
		}
	}

	global class ResultWrapper {
		global ResultAccountWrapper account;
		global List<ResultConsentWrapper> consents;
		global ResultOpportunitiesWrapper opportunities;
		global List<ResultPointOfContactWrapper> pointOfContacts;
		global ResultCampaignMemberWrapper campaignMember;

		global ResultWrapper(){
			this.account = new ResultAccountWrapper();
			this.consents = new List<ResultConsentWrapper>();
			this.opportunities = new ResultOpportunitiesWrapper();
			this.pointOfContacts = new List<ResultPointOfContactWrapper>();
			this.campaignMember = new ResultCampaignMemberWrapper();
		}
	}
	
	global class ResultAccountWrapper {
		global Id id;
		global String globalId;
	}

	global class ResultConsentWrapper {
		global Id id;
	}
	global class ResultOpportunitiesWrapper {
		global ResultOpportunityWrapper mainInterest;
		global ResultOpportunityWrapper secondInterest;
		global ResultOpportunityWrapper thirdInterest;

		global ResultOpportunitiesWrapper(){
			this.mainInterest = new ResultOpportunityWrapper();
			this.secondInterest = new ResultOpportunityWrapper();
			this.thirdInterest = new ResultOpportunityWrapper();
		}
	}
	global class ResultOpportunityWrapper {
		global Id id;
	}

	global class ResultPointOfContactWrapper {
		global Id id;
	}

	global class ResultCampaignMemberWrapper {
		global Id id;
	}

	global class PersonAccountWrapper extends CORE_ObjectWrapper {
		global string creationDate;
		global string salutation;
		global string gender;
		global string professionalSituation;
		global string lastName;
		global string firstName;
		global string phone;
		global string mobilePhone;
		global string emailAddress;
		global string street;
		global string postalCode;
		global string city;
		global string stateCode;
		global string countryCode;
		global string languageWebsite;
		global String gaUserId;
		global Boolean updateIfNotNull;
		global String mainNationality;
		global Integer birthYear;
		global Date birthDate;
	}
	global class ConsentWrapper extends CORE_ObjectWrapper {
		global string consentType;
		global string channel;
		global string description;
		global Boolean optIn;
		global string optInDate;
		global string doubleOptInDate;
		global Boolean optOut;
		global string optOutDate;
		global string division;
		global string businessUnit;
		global string brand;
	}

	global class OpportunityWrapper extends CORE_ObjectWrapper {
		global string academicYear;
		global string division;
		global string businessUnit;
		global string studyArea;
		global string curriculum;
		global string level;
		global string intake;
		global string product;
		global Double salesPrice;
		global Double quantity;
		global Boolean brochureShippedByMail;
		global Boolean applicationFormShippedByMail;
		global Boolean isSuspect;
		global string leadSourceExternalId;
		global string learningMaterial;
		global string applicationOnlineId;
		global String onlineShopId;
		global Boolean isAolRetailRegistered;
		global String funding;
		global String thirdPartyId;

	}
	
	global class PointOfContactWrapper extends CORE_ObjectWrapper {
		global string creationDate;
		global string sourceId;
		global string nature;
		global string captureChannel;
		global string origin;
		global string salesOrigin;
		global string campaignMedium;
		global string campaignSource;
		global string campaignTerm;
		global string campaignContent;
		global string campaignName;
		global string proactivePrompt;
		global string proactiveEngaged;
		global string chatWith;
		global string device;
		global Date firstVisitDate;
		global string firstPage;
		global string ipAddress;
		global string latitute;
		global string longitude;
		global Campaign crmCampaign;
		global string conversionPage;
		global string gclid;
		global string form;
		global string formArea;
		global String cityGeoloc;
		global String countryGeoloc;
		global String stateGeoloc;
		global string languageWebsite;
	}

	global class CampaignSubscriptionWrapper extends CORE_ObjectWrapper {
		global string campaignId;
	}

	global class AcademicDiplomaHistoryWrapper extends CORE_ObjectWrapper {
		global string name;
		global string level;
		global date diplomaDate;
	}
}