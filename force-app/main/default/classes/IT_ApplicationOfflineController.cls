/**
 * Created by Gabriele Vitanza on 03/02/2022.
 */

public with sharing class IT_ApplicationOfflineController {
    public static final String CLASS_NAME = IT_ApplicationOfflineController.class.getName();
    
    public class DocumentFileWrap{
        @AuraEnabled
        public IT_ApplicationOfflineDocument__mdt mdtAppDoc = new IT_ApplicationOfflineDocument__mdt();
        @AuraEnabled
        public CORE_AOL_Document__c createdDoc = new CORE_AOL_Document__c();
        @AuraEnabled
        public String id;
        
        @AuraEnabled
        public String urlRecord;
        
        @AuraEnabled
        public Boolean isReceived = false;
        @AuraEnabled
        public String note;
        @AuraEnabled
        public Decimal order;
    }

    @AuraEnabled
    public static List<String> getLabels() {
        List<String> labels = new List<String>();
        labels.add(Schema.SObjectType.CORE_AOL_Document__c.fields.Name.label);
        labels.add('Received');
        labels.add(Schema.SObjectType.CORE_AOL_Document__c.fields.CORE_Date_Created__c.label);
        labels.add(Schema.SObjectType.CORE_AOL_Document__c.fields.CORE_Date_Approved__c.label);
        labels.add(Schema.SObjectType.CORE_AOL_Document__c.fields.CORE_Date_Rejected__c.label);
        labels.add(Schema.SObjectType.CORE_AOL_Document__c.fields.CORE_Document_external_Url__c.label);
        labels.add(Schema.SObjectType.CORE_AOL_Document__c.fields.IT_Note__c.label);

        return labels;
    }

    @AuraEnabled
    public static Opportunity getOpty(Id recordId){
        Opportunity toReturn = new Opportunity();
        Opportunity[] opportunity = 
        [
            SELECT Id, CORE_OPP_Application_started__c, CORE_OPP_Application_completed__c, 
                IT_Interview__c, CORE_OPP_Selection_Process_Needed__c, CORE_Document_progression__c,  // AM - Added fields - 05/04/2022
                CORE_OPP_Division__r.CORE_Division_ExternalId__c, IT_EntryAssessmentTestType__c,
                CORE_OPP_Selection_Test_Date__c,
                IT_DateRegistrationStart__c,
                IT_DateRegistrationEnd__c,
                IT_DateCourseEnd__c,
                IT_DateAnagraphicalDataEnd__c,
                IT_DateSelectionDocumentsStart__c,
                IT_DateSelectionDocumentsEnd__c,
                CORE_OPP_AdmissionTest_Fee_Received_Date__c,
                IT_DateTestFeeEnd__c,
                IT_DateOtherDocumentStart__c,
                IT_DateOtherDocumentEnd__c,
                CORE_Selection_process_start_date__c,
                CORE_Selection_process_end_date__c,
                CORE_OPP_Selection_Test_Needed__c,
                IT_FromMigrationOLA__c,
                IT_BusinessUnitExternalId__c,
                CORE_First_document_upload_date__c,
                CORE_OPP_Admission_Test_Fee_Received__c
            FROM Opportunity 
            WHERE Id = :recordId];
        if ( ! opportunity.isEmpty() ) {
            toReturn = opportunity.get(0);
        }
        return toReturn;
    }

    /**
     * ----------------------------
     * @author Gabriele Vitanza
     * @description ?
     * @return ?
     * ----------------------------
     * Versions
     * 1.0 Creation
     * 1.1 - changed the logic including also IM division - Andrea Morittu
     */
    @AuraEnabled
    public static List<DocumentFileWrap> retrieveDocConfig(Id recordId, String division){
        Boolean isNABA = division == 'NB';
        Boolean isDA = division == 'DA';
        Boolean isMARANGONI = division == 'IM';

        List<IT_ApplicationOfflineDocument__mdt> documents = new List<IT_ApplicationOfflineDocument__mdt>();

        switch on division {
            when 'NB' {
                documents = [
                    SELECT Id, Label, IT_DocumentType__c,IT_Order__c
                    FROM IT_ApplicationOfflineDocument__mdt WHERE IT_NABA__c = TRUE ORDER BY IT_Order__c
                ];
            }
            when 'DA' {
                documents = [
                    SELECT Id, Label, IT_DocumentType__c,IT_Order__c
                    FROM IT_ApplicationOfflineDocument__mdt WHERE IT_DA__c = TRUE ORDER BY IT_Order__c
                ];
            }
            when 'IM' {
                documents = [
                    SELECT Id, Label, IT_DocumentType__c,IT_Order__c, IT_Istituto_Marangoni__c
                    FROM IT_ApplicationOfflineDocument__mdt WHERE IT_Istituto_Marangoni__c  = TRUE ORDER BY IT_Order__c
                ];
            }
        }

        Map<String, CORE_AOL_Document__c> createdDocumentsByName = new Map<String, CORE_AOL_Document__c>();
        List<CORE_AOL_Document__c> createdDocuments = [
                SELECT Id, Name, CORE_Date_Created__c, IT_Note__c, CORE_Document_external_Url__c,
                CORE_Date_Approved__c,CORE_Date_Rejected__c
                FROM CORE_AOL_Document__c
                WHERE CORE_Related_to_Opportunity__c = :recordId
        ];

        for (CORE_AOL_Document__c doc : createdDocuments) {
            createdDocumentsByName.put(doc.Name, doc);
        }

        List<DocumentFileWrap> documentFileWrapsToReturn = new List<DocumentFileWrap>();
        for (IT_ApplicationOfflineDocument__mdt aofDocument : documents) {
            DocumentFileWrap docFileWrap = new DocumentFileWrap();
            CORE_AOL_Document__c createdDoc = createdDocumentsByName.get(aofDocument.Label);
            docFileWrap.id = createdDoc != null ? createdDoc.Id : null;
            docFileWrap.urlRecord = String.isNotBlank(docFileWrap.id) ? URL.getSalesforceBaseUrl().toExternalForm() + '/' + docFileWrap.id : null;
            docFileWrap.mdtAppDoc = aofDocument;
            docFileWrap.isReceived = createdDoc != null;
            docFileWrap.createdDoc = createdDoc;
            docFileWrap.order = aofDocument.IT_Order__c;
            documentFileWrapsToReturn.add(docFileWrap);
        }
        
        return documentFileWrapsToReturn;
    }

    @AuraEnabled
    public static Map<String, String> getEntryAssessmentTestTypeValue(){
        Map<String, String> options = new Map<String, String>();

        Schema.DescribeFieldResult fieldResult = Opportunity.IT_EntryAssessmentTestType__c.getDescribe();

        List<Schema.PicklistEntry> pValues = fieldResult.getPicklistValues();
        for (Schema.PicklistEntry p: pValues) {

            options.put(p.getValue(), p.getLabel());
        }
        return options;
    }

    @AuraEnabled
    public static void saveOptyAndDocs(Opportunity opty, String docWrapList){
        System.debug('---saveOptyAndDocs');
        Boolean changeDocumentFirstUploadDate = true;

        Boolean tuitionFeeStatus = false;
        Boolean enrollmentFeeStatus = false;
        //Application Management change start
        Boolean isSH0 = false;
        System.debug('---opty.Id' + opty.Id);
        System.debug('---opty.IT_BusinessUnitExternalId__c' + opty.IT_BusinessUnitExternalId__c);
        if (opty != null && (opty.IT_BusinessUnitExternalId__c == 'IM.SH0' || opty.IT_BusinessUnitExternalId__c == 'IM.SHT')) {
            isSH0 = true;    
        }
        System.debug('---isSH0 ' + isSH0);
        //Application Management change finish
        Boolean isNB = opty.CORE_OPP_Division__r.CORE_Division_ExternalId__c == 'NB';
        Boolean isDA = opty.CORE_OPP_Division__r.CORE_Division_ExternalId__c == 'DA';
        Boolean isIM = opty.CORE_OPP_Division__r.CORE_Division_ExternalId__c == 'IM';
        Boolean fillFirstDocumentDate = true;

        Map<String, CORE_AOL_Document__c> createdDocumentsByName = new Map<String, CORE_AOL_Document__c>();
        List<DocumentFileWrap> docWrapListDes = (List<DocumentFileWrap>) JSON.deserialize(docWrapList, List<DocumentFileWrap>.class);
        List<CORE_AOL_Document__c> offlineDocuments = new List<CORE_AOL_Document__c>();
        Date firstFileDate;

        List<CORE_AOL_Document__c> createdDocuments = [
                SELECT Id, Name, CORE_Date_Created__c,IT_Note__c, CORE_Document_external_Url__c,
                CORE_Date_Approved__c, CORE_Date_Rejected__c
                FROM CORE_AOL_Document__c
                WHERE CORE_Related_to_Opportunity__c = :opty.Id
        ];
        System.debug('createdDocuments is : ' + JSON.serialize(createdDocuments));

        System.debug('createdDocuments is : ' + JSON.serialize(createdDocuments));
        System.debug('docWrapListDes is : ' + JSON.serialize(docWrapListDes));
        System.debug(docWrapListDes.size());

        for (CORE_AOL_Document__c doc : createdDocuments) {
            if(firstFileDate == null || doc.CORE_Date_Created__c<firstFileDate){
                firstFileDate = doc.CORE_Date_Created__c;
            }
            createdDocumentsByName.put(doc.Name, doc);
        }
        
        Integer counter = 0;
        for (DocumentFileWrap fileWrap : docWrapListDes) {
            System.debug(fileWrap);
            
            if ( fileWrap.isReceived == true ) {
                counter++;
            }
        }

        for (DocumentFileWrap fileWrap : docWrapListDes) {
            System.debug(fileWrap);

            System.debug(counter);
            System.debug(docWrapListDes.get(0).isReceived);
            System.debug(docWrapListDes.get(0).mdtAppDoc.IT_DocumentType__c);
            if (counter == 1 && docWrapListDes.get(0).isReceived == true && docWrapListDes.get(0).mdtAppDoc.IT_DocumentType__c == 'Application form' ) {
                fillFirstDocumentDate = false;
            }  
            //  else if(counter <= 2) { 11/05/2022 - 29/06/22 Commented according the https://gge-ita.lightning.force.com/lightning/r/SFDC_Bug__c/a1v09000000OW1SAAW/view and  https://ggeedu.atlassian.net/wiki/spaces/GSSGD/pages/2775023852/Application+Management+Offline
            if (fileWrap.mdtAppDoc.IT_DocumentType__c == 'Enrolment fee' || fileWrap.mdtAppDoc.IT_DocumentType__c == 'Tuition fee') {
                changeDocumentFirstUploadDate = false;

                System.debug('fileWrap.mdtAppDoc.IT_DocumentType__c is : ' + fileWrap.mdtAppDoc.IT_DocumentType__c);
                System.debug(' --------------------------------------------------------------------------------------  ');
                System.debug('fileWrap.fileWrap.isReceived is : ' + fileWrap.isReceived);
                if (fileWrap.mdtAppDoc.IT_DocumentType__c == 'Enrolment fee' && fileWrap.isReceived == true  ) {
                    enrollmentFeeStatus = true;
                }
                if(fileWrap.mdtAppDoc.IT_DocumentType__c == 'Tuition fee' &&  fileWrap.isReceived == true ) {
                    tuitionFeeStatus = true;
                }
            }

            
            

            if(fileWrap.isReceived && (createdDocuments == null || !createdDocumentsByName.containsKey(fileWrap.mdtAppDoc.Label))){
                System.debug('---metCondition');
                Date currentDate;
                CORE_AOL_Document__c offlineDoc = new CORE_AOL_Document__c();
                offlineDoc.Name = fileWrap.mdtAppDoc.Label;
                offlineDoc.CORE_Document_type__c = fileWrap.mdtAppDoc.IT_DocumentType__c;
                offlineDoc.CORE_Date_Created__c = fileWrap.createdDoc.CORE_Date_Created__c;
                offlineDoc.IT_Note__c = fileWrap.note;
                offlineDoc.CORE_Document_external_Url__c = fileWrap.createdDoc.CORE_Document_external_Url__c;
                offlineDoc.CORE_Related_to_Opportunity__c = opty.Id;
                offlineDoc.CORE_Date_Approved__c = fileWrap.createdDoc.CORE_Date_Approved__c;
                offlineDoc.CORE_Date_Rejected__c = fileWrap.createdDoc.CORE_Date_Rejected__c;
                
                offlineDocuments.add(offlineDoc);

                currentDate = offlineDoc.CORE_Date_Created__c;

                if(firstFileDate == null && opty.CORE_OPP_Application_started__c != null){
                    System.debug('if(firstFileDate == null && opty.CORE_OPP_Application_started__c != null){');
                    Date dt = Date.newInstance( (opty.CORE_OPP_Application_started__c).year(), (opty.CORE_OPP_Application_started__c).month(), (opty.CORE_OPP_Application_started__c).day() );
                    
                    firstFileDate = dt;
                } else if(firstFileDate == null && opty.CORE_OPP_Application_started__c == null){
                    System.debug('else if(firstFileDate == null && opty.CORE_OPP_Application_started__c == null){');

                    firstFileDate = offlineDoc.CORE_Date_Created__c;
                }

                if(currentDate < firstFileDate){
                    System.debug('if(currentDate < firstFileDate){');
                    firstFileDate = currentDate;
                }
                //Application Management change start
                if (isSH0 && offlineDoc.CORE_Date_Created__c != null) {
                    if (offlineDoc.CORE_Document_type__c == 'Visa') {
                        opty.CORE_OPP_Visa_Status__c = 'CORE_PKL_Visa_Accepted';
                    }
                    if (offlineDoc.CORE_Document_type__c == 'Enrolment fee') {
                        opty.IT_PaymentAdvancementStatus__c = 'IT_PKL_EnrolmentFeeCompleted';    
                    }
                    if (offlineDoc.CORE_Document_type__c == 'Tuition fee') {
                        opty.IT_PaymentAdvancementStatus__c = 'IT_PKL_TuitionFeeCompleted';    
                    }
                }
                //Application Management change finish
            } else if(createdDocumentsByName.containsKey(fileWrap.mdtAppDoc.Label)){
                System.debug('else if(createdDocumentsByName.containsKey(fileWrap.mdtAppDoc.Label)){');
                CORE_AOL_Document__c existingOfflineDoc = createdDocumentsByName.get(fileWrap.mdtAppDoc.Label);
                existingOfflineDoc.IT_Note__c = fileWrap.createdDoc.IT_Note__c;
                existingOfflineDoc.CORE_Document_external_Url__c = fileWrap.createdDoc.CORE_Document_external_Url__c;
                existingOfflineDoc.CORE_Date_Created__c = fileWrap.createdDoc.CORE_Date_Created__c;

                existingOfflineDoc.CORE_Date_Approved__c = fileWrap.createdDoc.CORE_Date_Approved__c;
                existingOfflineDoc.CORE_Date_Rejected__c = fileWrap.createdDoc.CORE_Date_Rejected__c;
                System.debug(CLASS_NAME + '. saveOptAndDocs - existingOfflineDoc.CORE_Date_Rejected__c : ' + existingOfflineDoc.CORE_Date_Rejected__c );
                System.debug(CLASS_NAME + '. saveOptAndDocs - existingOfflineDoc.CORE_Date_Rejected__c: ' + existingOfflineDoc.CORE_Date_Rejected__c );
                offlineDocuments.add(existingOfflineDoc);
                                System.debug(CLASS_NAME + '. saveOptAndDocs - existingOfflineDoc.CORE_Date_Rejected__c: ' + existingOfflineDoc.CORE_Date_Rejected__c );
                System.debug('existingOfflineDoc.CORE_Date_Created__c '  + existingOfflineDoc.CORE_Date_Created__c);
                System.debug('opty.CORE_OPP_Application_started__c ' + opty.CORE_OPP_Application_started__c);
                if(existingOfflineDoc.CORE_Date_Created__c != opty.CORE_OPP_Application_started__c){
                    firstFileDate = existingOfflineDoc.CORE_Date_Created__c;
                } 
                
            }

            
        }

        System.debug('offlineDocuments: ' + offlineDocuments);

        // changed in AND the conditions --> if there's already a value inside the opt field, we don't have to change it!
        if(opty.CORE_OPP_Application_started__c == null && firstFileDate != opty.CORE_OPP_Application_started__c){
            opty.CORE_OPP_Application_started__c = 
            DateTime.newInstance(
                firstFileDate.year()    , 
                firstFileDate.month()   , 
                firstFileDate.day()     , 
                System.now().hour()     ,
                System.now().minute()   ,
                System.now().second()  
            );

            System.debug(CLASS_NAME + '. fillFirstDocumentDate: ' + fillFirstDocumentDate );
            System.debug(CLASS_NAME + '. isDA: ' + isDA );
            System.debug(CLASS_NAME + '. isNB: ' + isNB );
        }

        if (fillFirstDocumentDate && (isNB || isDA) ) {
            System.debug(' firstFileDate: ' + firstFileDate );
            if (firstFileDate != null && opty.CORE_First_document_upload_date__c == null) {
                opty.CORE_First_document_upload_date__c = firstFileDate;
            }
        } else if (!fillFirstDocumentDate && (isNB || isDA) ) {
            System.debug(' !fillFirstDocumentDate && (isNB || isDA): ');
            opty.CORE_First_document_upload_date__c = null;
        }
        //Application Management change start
        System.debug('---isSH0 ' + isSH0);
        if (isSH0) {
            System.debug('---Id' + opty.Id);
            System.debug('---opty.CORE_First_document_upload_date__c' + opty.CORE_First_document_upload_date__c);
            if (opty.CORE_First_document_upload_date__c == null) {
                if (changeDocumentFirstUploadDate == true) {
                    opty.CORE_Document_progression__c = 'Started';    
                    opty.CORE_First_document_upload_date__c = Date.today();
                }
            }
            System.debug('enrollmentFeeStatus is : ' + enrollmentFeeStatus);
            if (enrollmentFeeStatus) {
                opty.IT_PaymentAdvancementStatus__c = 'IT_PKL_EnrolmentFeeCompleted';
            }
            System.debug('tuitionFeeStatus is : ' + tuitionFeeStatus);
            if (tuitionFeeStatus) {
                opty.IT_PaymentAdvancementStatus__c = 'IT_PKL_TuitionFeeCompleted';
            }
            
            if (opty.CORE_OPP_Selection_Process_Needed__c) {
                opty.CORE_Selection_process_status__c = 'CORE_PKL_Closed_success';
            }
        } else {
            if (isIM) {
                if (opty.CORE_First_document_upload_date__c == null) {
                    opty.CORE_First_document_upload_date__c = Date.today();
                }
                if (String.isBlank(opty.CORE_Document_progression__c) || opty.CORE_Document_progression__c == 'Not started') {
                    opty.CORE_Document_progression__c = 'Started';
                }
            }
                
        }
        //Application Management change finish

        update opty;
        upsert offlineDocuments;
    }

}