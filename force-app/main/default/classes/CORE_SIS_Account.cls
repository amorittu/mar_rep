@RestResource(urlMapping='/sis/account')
global without sharing class CORE_SIS_Account {

    public static Set<String> overrideFieldsSet;//used mainly by test class for code coverage

    @HttpGet
    global static CORE_SIS_Wrapper.ResultWrapperAccount accountList() {
        RestRequest	request = RestContext.request;

        // Initiating logs
        Long dt1 = DateTime.now().getTime();
        Long dt2;
        CORE_LogManager logSettings;
        CORE_LOG__c log = new CORE_LOG__c(CORE_API_Name__c = 'SIS');
        Boolean isErrorLog = false;
        logSettings = new CORE_LogManager('SIS');

        String salesforceId = request.params.get(CORE_SIS_Constants.PARAM_SF_ID);
        String sisStudentId = request.params.get(CORE_SIS_Constants.PARAM_SISSTUDENT_ID);
        String additionalFields = request.params.get(CORE_SIS_Constants.PARAM_ADD_FIELDS);
        
        List<String> additionalFieldsList = CORE_SIS_GlobalWorker.splitValues(additionalFields);

        CORE_SIS_Wrapper.ResultWrapperAccount result = checkMandatoryParameters(salesforceId, sisStudentId);

        try{
            if(result != null){
                return result;
            }
            Account account = CORE_SIS_QueryProvider.getAccount(salesforceId, sisStudentId, additionalFieldsList);
            result = new CORE_SIS_Wrapper.ResultWrapperAccount(account,CORE_SIS_Constants.SUCCESS_STATUS);
        } catch(Exception e){
            result = new CORE_SIS_Wrapper.ResultWrapperAccount(CORE_SIS_Constants.ERROR_STATUS, e.getMessage() + ' at ' + e.getStackTraceString());
        } finally{
            dt2 = DateTime.now().getTime();
            System.debug('CORE_AOL_AcademicDiplomaHistory(urlMapping=\'/aol/academic-diploma-history\') : T0TAL TIME OF EXECUTION = ' + (dt2-dt1) + ' ms');
            // creating log
            log.CORE_Processing_Duration__c =(dt2-dt1);
            log.CORE_HTTP_Status_Code__c = result.status == 'KO' ? 400 : result.status == 'OK' ? 200 : 500;
            log.CORE_Sent_Body__c = JSON.serialize(result);
            log.CORE_Received_Body__c =  'params : ' + request.params.toString();
            
            if(!logSettings.isNone()){
                if(logSettings.isAllLogged()){ // is All logged
                    CORE_LogManager.createLog(log);
                }else if(logSettings.isErrorLogged() && isErrorLog){ // is Error
                    CORE_LogManager.createLog(log);
                }
            }
        }

        return result;
    }

    private static CORE_SIS_Wrapper.ResultWrapperAccount checkMandatoryParameters(String salesforceId, String sisStudentId){
        Boolean isMissingSalesforceId= CORE_SIS_GlobalWorker.isParameterMissing(salesforceId);
        Boolean isMissingSisStudentId= CORE_SIS_GlobalWorker.isParameterMissing(sisStudentId);
        String errorMsg = CORE_SIS_Constants.MSG_MISSING_PARAMETERS;

        if(isMissingSalesforceId 
           && isMissingSisStudentId){
            return new CORE_SIS_Wrapper.ResultWrapperAccount(CORE_SIS_Constants.ERROR_STATUS, errorMsg + CORE_SIS_Constants.PARAM_SF_ID + ' or ' + CORE_SIS_Constants.PARAM_SISSTUDENT_ID);
        }else{
            return null;
        }
    }
}