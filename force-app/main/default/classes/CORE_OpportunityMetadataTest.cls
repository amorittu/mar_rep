@IsTest
public class CORE_OpportunityMetadataTest {
    @IsTest
    public static void CORE_OpportunityMetadata_constructor_should_initialize_attribute_from_custom_metadata() {
        Test.startTest();
        CORE_OpportunityMetadata oppMetadata = new CORE_OpportunityMetadata();
        Test.stopTest();

        CORE_Opportunity_creation_setting__mdt opportunityMetadata = oppMetadata.opportunityMetadata;

        System.assertNotEquals(NULL, opportunityMetadata);
        System.assertNotEquals(NULL, opportunityMetadata.CORE_OPP_Full_mode_Status__c);

        System.assertEquals(opportunityMetadata.CORE_OPP_Full_mode_Status__c , oppMetadata.statusFull);
        System.assertEquals(opportunityMetadata.CORE_OPP_Full_mode_SubStatus__c , oppMetadata.subStatusFull);
        System.assertEquals(opportunityMetadata.CORE_OPP_Partial_mode_Status__c , oppMetadata.statusPartial);
        System.assertEquals(opportunityMetadata.CORE_OPP_Partial_mode_SubStatus__c , oppMetadata.subStatusPartial);
        System.assertEquals(Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get(opportunityMetadata.CORE_OPP_RecordType__c).getRecordTypeId() , oppMetadata.oppRecordTypeId);
        
        Date closeDate = Date.today().addYears((Integer) opportunityMetadata.CORE_OPP_Close_date_nb_year__c);
        System.assertEquals(closeDate , oppMetadata.closeDate);
        System.assertEquals(opportunityMetadata.CORE_POC_Insertion_Mode__c , oppMetadata.pocInsertionMode);
    }

    @IsTest
    public static void getMetadata_Should_retrieve_and_store_custom_metadata_values() {
        CORE_OpportunityMetadata oppMetadata = new CORE_OpportunityMetadata();

        oppMetadata.opportunityMetadata = null;
        System.assertEquals(NULL,oppMetadata.opportunityMetadata);

        Test.startTest();
        oppMetadata.getMetadata();
        Test.stopTest();     

        System.assertNotEquals(null,oppMetadata.opportunityMetadata , 'CORE_Opportunity_creation_setting__mdt should be initialized');

    }

    @IsTest
    public static void initializeAttributes_Should_store_metadata_value_in_class_attributes() {
        Test.startTest();
        CORE_OpportunityMetadata oppMetadata = new CORE_OpportunityMetadata();
        Test.stopTest();
        //System.assertEquals(NULL,oppMetadata.opportunityMetadata);
        oppMetadata.statusFull = null;
        oppMetadata.subStatusFull= null;
        oppMetadata.statusPartial= null;
        oppMetadata.subStatusPartial= null;
        oppMetadata.oppRecordTypeId = null;
        oppMetadata.closeDate = null;

        oppMetadata.pocInsertionMode = null;

        oppMetadata.initializeAttributes();
       
        System.assertNotEquals(null, oppMetadata.statusFull, 'statusFull should be initialized');
        System.assertNotEquals(null, oppMetadata.subStatusFull, 'subStatusFull should be initialized');
        System.assertNotEquals(null, oppMetadata.statusPartial, 'statusPartial should be initialized');
        System.assertNotEquals(null, oppMetadata.subStatusPartial, 'subStatusPartial should be initialized');
        System.assertNotEquals(null, oppMetadata.oppRecordTypeId, 'oppRecordTypeId should be initialized');
        System.assertNotEquals(null, oppMetadata.closeDate , 'closeDate should be initialized');

        System.assertNotEquals(null, oppMetadata.pocInsertionMode, 'pocInsertionMode should be initialized');
    }
}