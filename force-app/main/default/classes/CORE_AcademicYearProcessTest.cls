@IsTest
public class CORE_AcademicYearProcessTest {
    @IsTest
    public static void getCurrentAcademicYear_Should_return_a_list_of_1_academicYear(){
        List<String> academicYears = CORE_AcademicYearProcess.getCurrentAcademicYear();
        System.assertEquals(1,academicYears.size(),'Sould be only one academic year');
    }

}