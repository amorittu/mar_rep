public class CORE_PicklistUtils {
    public static List<String> getPicklistValues (String objectName, String fieldName) {
        List<String> picklistValuesApiName = new List<String>();

        Schema.SObjectType s = Schema.getGlobalDescribe().get(objectName) ;
        Schema.DescribeSObjectResult r = s.getDescribe() ;
        Map<String,Schema.SObjectField> fields = r.fields.getMap() ;
        Schema.DescribeFieldResult fieldResult = fields.get(fieldName).getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();

        for(Schema.PicklistEntry pickListVal : ple){
            picklistValuesApiName.add(pickListVal.getValue());
        }

        return picklistValuesApiName;
    }
}