@RestResource(urlMapping='/twilio/CORE_FLEX_GetInfo')
global without sharing class CORE_FLEX_GetInfo {
    
    private String calledPhone;
    private String callerPhone;
    private String interactionType;
    private static final String MSG_MISSING_PARAMETERS = 'Input parameters are missing';
    private static final List<String> MANDATORY_PARAMETERS = new List<String>{'CalledPhone','CallerPhone'};
    private static final String API_PROFILE = 'CORE_API_Profile';

    public CORE_FLEX_GetInfo(CORE_FLEX_GetInfo.InputParams input){
        this.calledPhone = input.CalledPhone;
        this.callerPhone = input.CallerPhone;
        this.interactionType = input.interactionType;
    }

    @HttpPOST
    global static List<SuccessResult> execute(List<CORE_FLEX_GetInfo.InputParams> inputs) {
        if(generateMissingParametersError(inputs) != null){
            throw new IllegalArgumentException(MSG_MISSING_PARAMETERS);
        }
        CORE_FLEX_GetInfo process = new CORE_FLEX_GetInfo(inputs.get(0));

        Account account;
        CORE_Business_Unit__c businessUnit;
        Opportunity opportunity;
        Opportunity opportunityTmp;
        User user;

        System.debug('@CORE_FLEX_GetInfo>execute : process.calledPhone = ' + process.calledPhone);
        System.debug('@CORE_FLEX_GetInfo>execute : process.callerPhone = ' + process.callerPhone);
        System.debug('@CORE_FLEX_GetInfo>execute : process.interactionType = ' + process.interactionType);
        //FindAccount
        Account accountTmp = process.findAccount();

        if(accountTmp != null){
            account = new Account(
                Id = accountTmp.Id,
                FirstName = accountTmp?.FirstName,
                LastName = accountTmp?.LastName,
                CORE_Language__pc = accountTmp?.CORE_Language__pc
            );
        }
        System.debug('@CORE_FLEX_GetInfo>execute : account = ' + account);

        //FindBusinessUnit
        businessUnit = process.getBusinessUnit();
        System.debug('@CORE_FLEX_GetInfo>execute : businessUnit = ' + businessUnit);

        //GetOpportunity
        if(businessUnit != null && accountTmp != null){
            opportunityTmp = process.getOpportunity(accountTmp, businessUnit);
            opportunity = new Opportunity(
                Id = opportunityTmp.Id,
                OwnerId = opportunityTmp.OwnerId,
                StageName = opportunityTmp.StageName, 
                CORE_ProfileOwner__c = opportunityTmp.CORE_ProfileOwner__c,
                CORE_OPP_Business_Unit__c = opportunityTmp.CORE_OPP_Business_Unit__c
            );
        }
        System.debug('@CORE_FLEX_GetInfo>execute : opportunity = ' + opportunity);

        //GetUser
        if(opportunity != null){
            user = process.getUserInfos(opportunityTmp);
        }
        System.debug('@CORE_FLEX_GetInfo>execute : user = ' + user);

        SuccessResult result = new SuccessResult();
        result.actionName = 'CORE_FLEX_GetInfo';
        result.isSuccess = true;
        result.outputValues = new OutputValues();
        result.outputValues.Account = account;
        result.outputValues.Username = user?.UserName;
        result.outputValues.BusinessUnit = businessUnit;
        result.outputValues.UserId = user?.Id;
        result.outputValues.Opportunity = opportunity;
        result.outputValues.UserEmail = user?.Email;
        System.debug('@CORE_FLEX_GetInfo>execute : result = ' + result);

        return new List<SuccessResult> {result};

    }

    private Account findAccount(){
        String callerPhone = '%' + this.callerPhone + '%';

        List<Account> accounts = [Select Id, FirstName, LastName, CORE_Language__pc, 
                (Select Id, OwnerId,Owner.CallCenterId,Owner.Email,Owner.UserName, StageName, CORE_ProfileOwner__c, CORE_OPP_Business_Unit__c, IsClosed 
                FROM Opportunities 
                ORDER BY LastModifiedDate Desc, IsClosed Asc)
            FROM Account 
            WHERE CORE_TECH_All_Phones__c like :callerPhone LIMIT 1];

        if(accounts.isEmpty()){
            return null;
        } else {
            return accounts.get(0);
        }
    }

    private CORE_Business_Unit__c getBusinessUnit(){
        String calledPhone = '%' + this.calledPhone + '%';

        List<CORE_Business_Unit__c> businessUnit = [Select Id, Name, CORE_Business_Unit_code__c, CORE_Business_Unit_ExternalId__c
            FROM CORE_Business_Unit__c 
            WHERE CORE_Phone_numbers__c like :calledPhone LIMIT 1];

        if(businessUnit.isEmpty()){
            return null;
        } else {
            return businessUnit.get(0);
        }
    }

    private Opportunity getOpportunity(Account account, CORE_Business_Unit__c businessUnit){
        List<Opportunity> opportunities = account.Opportunities;

        if(opportunities == null || opportunities.isEmpty()){
            return null;
        }

        for(Opportunity opportunity : opportunities){
            if(opportunity.CORE_OPP_Business_Unit__c == businessUnit.Id){
                return opportunity;
            }
        }

        return null;
    }

    private User getUserInfos(Opportunity opportunity){
        User user = new User(
            Id = opportunity.OwnerId,
            UserName = opportunity.Owner.UserName
        );

        if(Test.isRunningTest()){
            user.Email = opportunity.Owner.Email;
        }
        
        if(opportunity.CORE_ProfileOwner__c != API_PROFILE && !String.isBlank(opportunity.Owner.CallCenterId)){
            user.Email = opportunity.Owner.Email;
        }

        return user;
    }

    @TestVisible
    private static ResultWrapper generateMissingParametersError(List<InputParams> inputs){
        List<String> missingParameters = new List<String>();
        
        missingParameters.addAll(getMissingParameters(inputs));

        String errorMsg = MSG_MISSING_PARAMETERS;

        if(missingParameters != null && !missingParameters.isEmpty()){
            errorMsg += missingParameters;

            return new ResultWrapper(errorMsg, '');
        }
        
        return null;
    }

    private static List<String> getMissingParameters(List<InputParams> inputs){
        String wrapperName = 'Inputs';

        List<String> missingParameters = new List<String>();

        if(inputs == null || inputs.isEmpty()){
            missingParameters.add(wrapperName);
            return missingParameters;
        }
        InputParams input = inputs.get(0);

        for(String mandatoryParameter : MANDATORY_PARAMETERS){
            if(String.IsBlank((String) input.get(mandatoryParameter))){
                missingParameters.add(String.format('{0}[0].{1}', new List<Object>{wrapperName, mandatoryParameter}));
            }
        }
        
        return missingParameters;
    }

    global class ResultWrapper {
        String message;
        String errorCode;

        public ResultWrapper (String message, String errorCode){
            this.message = message;
            this.errorCode = errorCode;
        }
    }

    global class InputParams extends CORE_ObjectWrapper {
        public String CalledPhone;
        public String CallerPhone;
        public String InteractionType;
    } 

    global class SuccessResult {
        public String actionName;
        public String errors;
        public Boolean isSuccess;
        public OutputValues outputValues;
    }

    global class OutputValues {
        public Account Account;
        public String Username;
        public CORE_Business_Unit__c BusinessUnit;
        public Id UserId;
        public Opportunity Opportunity;
        public String UserEmail;
    }
}