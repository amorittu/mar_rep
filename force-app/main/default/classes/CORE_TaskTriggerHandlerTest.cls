@isTest
public with sharing class CORE_TaskTriggerHandlerTest {
    @TestSetup
    public static void makeData(){
        CORE_DataFaker_CatalogHierarchy catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test1');
        Account account = CORE_DataFaker_Account.getStudentAccount('test1');
        Opportunity opportunity = CORE_DataFaker_Opportunity.getOpportunity(catalogHierarchy.division.Id, catalogHierarchy.businessUnit.Id, catalogHierarchy.studyArea.Id, catalogHierarchy.curriculum.Id, catalogHierarchy.level.Id, catalogHierarchy.intake.Id, account.Id, 'Lead', 'Lead - New');
    }

    @isTest
    public static void afterUpdate_Should_Update_Task(){
        //List<Task> tasks = [Select Id,CORE_Business_Hours__c,CORE_Start_Date__c,WhatId,Status,CompletedDateTime,Type,CORE_Is_Incoming__c,CORE_Resolution_Time__c,CreatedDate FROM Task];
        Opportunity opportunity = [Select Id from Opportunity LIMIT 1];
        Task newTask = new Task(
            subject = 'taskTest',
            CORE_Is_Incoming__c = false,
            Type = 'CORE_PKL_Call',
            CORE_Start_Date__c=null,
            whatId = opportunity.Id
        );
        insert newTask;
        CORE_TriggerUtils.setBypassTrigger();
        Task task = new Task(
            id = newTask.Id,
            CORE_Is_Incoming__c = false,
            Type = 'CORE_PKL_Call',
            Status = 'CORE_PKL_Completed',
            CORE_Start_Date__c=null
        );
        update task;
        CORE_TriggerUtils.setBypassTrigger();
        List<Task> newTasks = [Select Id,CORE_Business_Hours__c,WhatId,CORE_Start_Date__c,Status,CompletedDateTime,Type,CORE_Is_Incoming__c,CORE_Resolution_Time__c,CreatedDate FROM Task where Id = :newTask.Id];
       	//Map<Id,Task> newTasksMap = new Map<Id,Task> (newTasks);
        Test.startTest();
        
        CORE_TaskTriggerHandler.afterUpdate(new List<Task>{newTask}, newTasks);
        Test.stopTest();
        update newTask;

        Task myTaskUpdate = [SELECT Id,CORE_Resolution_Time__c FROM Task WHERE Id=:newTask.Id];
        System.assertNotEquals(null, myTaskUpdate.CORE_Resolution_Time__c);
    }

    @isTest
    public static void afterUpdate_Should_Update_Opportunity(){
        Opportunity opportunity = [Select Id from Opportunity LIMIT 1];
        Task task = new Task(
            subject = 'taskTest',
            CORE_Is_Incoming__c = false,
            Type = 'CORE_PKL_Call',
            CORE_Start_Date__c=null,
            whatId = opportunity.Id
        );
        insert task;
        
        CORE_TriggerUtils.setBypassTrigger();
        Task newTask = new Task(
            id = task.Id,
            CORE_Is_Incoming__c = false,
            Type = 'CORE_PKL_Call',
            Status = 'CORE_PKL_Completed',
            CORE_Start_Date__c=null
        );
        update newTask;
        CORE_TriggerUtils.setBypassTrigger();
        List<Task> newTasks = [Select Id,CORE_Business_Hours__c,WhatId,CORE_Start_Date__c,Status,CompletedDateTime,Type,CORE_Is_Incoming__c,CORE_Resolution_Time__c,CreatedDate FROM Task where Id = :task.Id];
        
        Test.startTest();
        CORE_TaskTriggerHandler.afterUpdate(new List<Task>{task}, newTasks);
        Test.stopTest();
        
        Opportunity myOpportunityUpdate = [SELECT Id,CORE_OPP_First_Outbound_Contact_Date__c FROM Opportunity WHERE Id=:task.WhatId];
        System.assertNotEquals(null, myOpportunityUpdate.CORE_OPP_First_Outbound_Contact_Date__c);
    }
    
    @isTest
    public static void afterUpdate_Should_Update_Task_using_trigger(){
        Opportunity opportunity = [Select Id from Opportunity LIMIT 1];
        Task task = new Task(
            subject = 'taskTest',
            CORE_Is_Incoming__c = false,
            Type = 'CORE_PKL_Call',
            CORE_Start_Date__c=null,
            whatId = opportunity.Id
        );
        insert task;
        
        Task newTask = new Task(
            id = task.Id,
            CORE_Is_Incoming__c = false,
            Type = 'CORE_PKL_Call',
            Status = 'CORE_PKL_Completed',
            CORE_Start_Date__c=null
        );
        
        Test.startTest();
        update newTask;
        
        Test.stopTest();

        Task myTaskUpdate = [SELECT Id,CORE_Resolution_Time__c FROM Task WHERE Id=:task.Id];
        System.assertNotEquals(null, myTaskUpdate.CORE_Resolution_Time__c);
    }

    @isTest
    public static void beforeDelete_Should_return_an_error_if_user_have_not_custom_permission(){
        Task newTask = new Task(
            CORE_Is_Incoming__c = false,
            Type = 'CORE_PKL_Call',
            Status = 'CORE_PKL_Completed',
            CORE_Start_Date__c=null,
            Subject = 'test'
        );
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Core Telesales'];
        User u =  CORE_DataFaker_User.getUserWithoutInsert('testUser', p.Id);

        System.runAs(u) {
            insert newTask;
            Boolean expectedExceptionThrown =  false;
            try{
                delete newTask;
            } catch(Exception e){
                expectedExceptionThrown =  e.getMessage().contains(Label.CORE_Task_DeleteError) ? true : false;
                System.debug('Exception !');
            }
            System.AssertEquals(expectedExceptionThrown, true);
        }
    }

    @isTest
    public static void beforeDelete_Should_not_return_an_error_if_user_have_custom_permission(){
        Task newTask = new Task(
            CORE_Is_Incoming__c = false,
            Type = 'CORE_PKL_Call',
            Status = 'CORE_PKL_Completed',
            CORE_Start_Date__c=null,
            Subject = 'test'
        );

        insert newTask;
        Boolean expectedExceptionThrown =  false;
        try{
            delete newTask;
        } catch(Exception e){
            expectedExceptionThrown =  true;
        }
        System.AssertEquals(expectedExceptionThrown, false);
    }
    
    @isTest
    public static void beforeInsert_Should_Init_Activity_Fields(){
        Task newTask = new Task(
            CORE_Is_Incoming__c = false,
            Type = 'CORE_PKL_Call',
            Status = 'CORE_PKL_Completed',
            CORE_Start_Date__c=null,
            Subject = 'test',
            TaskSubtype = 'Task'
        );
        
        Profile p = [SELECT Id FROM Profile WHERE Name='Core Telesales'];
        User u =  CORE_DataFaker_User.getUserWithoutInsert('testUser', p.Id);

        Test.startTest();
        System.runAs(u) {
            insert newTask;
        }
        Test.stopTest();
       
        Task task = [SELECT Id, Type, CORE_Activity_Task_Subtype__c, TaskSubtype, CORE_Activity_Type__c FROM Task WHERE ID = :newTask.Id];
        System.AssertEquals(newTask.Type, task.CORE_Activity_Type__c);
        System.AssertEquals(newTask.TaskSubtype, task.CORE_Activity_Task_Subtype__c);
    }

    @isTest
    public static void beforeUpdate_Should_Init_Activity_Fields(){
        Task oldTask = new Task(
            CORE_Is_Incoming__c = false,
            Type = 'CORE_PKL_Call',
            Status = 'CORE_PKL_Completed',
            CORE_Start_Date__c=null,
            Subject = 'test',
            TaskSubtype = 'Task'
        );
        insert oldTask;

        oldTask.CORE_Activity_Type__c = null;
        oldTask.CORE_Activity_Task_Subtype__c = null;

        Task newTask = oldTask.clone(true, true, true, true);
        

        Profile p = [SELECT Id FROM Profile WHERE Name='Core Telesales'];
        User u =  CORE_DataFaker_User.getUserWithoutInsert('testUser', p.Id);

        System.runAs(u) {
            CORE_TaskTriggerHandler.beforeUpdate(new List<Task> {oldTask}, new List<Task> {newTask});
            System.AssertEquals(newTask.Type, newTask.CORE_Activity_Type__c);
            System.AssertEquals(newTask.TaskSubtype, newTask.CORE_Activity_Task_Subtype__c);

            newTask.Type = 'CORE_PKL_Email';
            CORE_TaskTriggerHandler.beforeUpdate(new List<Task> {oldTask}, new List<Task> {newTask});
            System.AssertEquals(newTask.Type, newTask.CORE_Activity_Type__c);
            System.AssertEquals(newTask.TaskSubtype, newTask.CORE_Activity_Task_Subtype__c);
        }
    }
}