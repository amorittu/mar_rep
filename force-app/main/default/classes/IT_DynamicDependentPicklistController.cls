public without sharing class IT_DynamicDependentPicklistController {
    public static final String CLASS_NAME = IT_DynamicDependentPicklistController.class.getName();
    
    @AuraEnabled
    public static Map<String, List<IT_PicklistValuesController.PickListValues>> getWrapperToDisplay(String sObjectApiName, String controllingFieldApiName, String dependentFieldApiName){
        return IT_PicklistValuesController.getWrapperToDisplay(sObjectApiName, controllingFieldApiName, dependentFieldApiName);
    }

}