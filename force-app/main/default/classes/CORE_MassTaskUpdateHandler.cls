public with sharing class CORE_MassTaskUpdateHandler {
    public String settingName;
    public List<Database.SaveResult> results;
	public List<Database.UpsertResult> upsertResults;
    public List<sObject> recordsList;
    public List<String> resultsList;
    public Boolean sendEmail;
    public String exceptionError;

    public CORE_MassTaskUpdateHandler(){}

    public List<String> handleResults(){

        system.debug('@CORE_MassTaskUpdateHandler > handleResults > recordsList.size: ' + recordsList.size());
        system.debug('@CORE_MassTaskUpdateHandler > handleResults > resultsList start: ' + resultsList);

        CORE_MassTaskEmailSetting__mdt emailSetting = CORE_MassTaskEmailSetting__mdt.getInstance(settingName);

		if(results != null){//Insert or Update operation

			Integer countErrors = 0;
			resultsList = (resultsList == null || resultsList.size() == 0) ? new List<String> { '0', '0', '0', '' } :resultsList;

			for (Integer i = 0; i < recordsList.size(); i++) {
				Database.SaveResult sr = results[i];

				if(!sr.isSuccess()){
					countErrors++;

					if(emailSetting.CORE_SendErrorsList__c){
						resultsList[3] += (String)recordsList[i].get('Id') + ',' + JSON.serialize(sr.getErrors()) + '\n';
					}
				}
			}

			system.debug('@CORE_MassTaskUpdateHandler > handleResults INSERT/UPDATE > resultsList before: ' + resultsList);
			resultsList[0] = String.valueOf(Integer.valueOf(resultsList[0]) + Integer.valueOf(recordsList.size()));
			resultsList[1] = String.valueOf(Integer.valueOf(resultsList[1]) + (Integer.valueOf(recordsList.size()) - countErrors));
			resultsList[2] = String.valueOf(Integer.valueOf(resultsList[2]) + countErrors);

			system.debug('@CORE_MassTaskUpdateHandler > handleResults INSERT/UPDATE > resultsList after: ' + resultsList);
		}
        else if(upsertResults != null){//Upsert operation
			
			Integer countInsertErrors = 0;
			Integer countUpdateErrors = 0;
			resultsList = (resultsList == null || resultsList.size() == 0) ? new List<String> { '0', '0', '0', '0', '0', '' } :resultsList;
			List<SObject> insertRecordsList = new List<SObject>();
			List<SObject> updateRecordsList = new List<SObject>();

			for (Integer i = 0; i < recordsList.size(); i++) {
				Database.UpsertResult sr = upsertResults[i];

				if(sr.isCreated()){
					insertRecordsList.add(recordsList[i]);

					if(!sr.isSuccess()){
						countInsertErrors++;
					}
				}
				else{
					updateRecordsList.add(recordsList[i]);

					if(!sr.isSuccess()){
						countUpdateErrors++;
					}
				}

				if(!sr.isSuccess()){
					if(emailSetting.CORE_SendErrorsList__c){
						resultsList[5] += (String)recordsList[i].get('Id') + ',' + JSON.serialize(sr.getErrors()) + '\n';
					}
				}
			}

			system.debug('@CORE_MassTaskUpdateHandler > handleResults UPSERT > resultsList before: ' + resultsList);

			resultsList[0] = String.valueOf(Integer.valueOf(resultsList[0]) + Integer.valueOf(recordsList.size()));

			resultsList[1] = String.valueOf(Integer.valueOf(resultsList[1]) + (Integer.valueOf(insertRecordsList.size()) - countInsertErrors));
			resultsList[2] = String.valueOf(Integer.valueOf(resultsList[2]) + countInsertErrors);

			resultsList[3] = String.valueOf(Integer.valueOf(resultsList[3]) + (Integer.valueOf(updateRecordsList.size()) - countUpdateErrors));
			resultsList[4] = String.valueOf(Integer.valueOf(resultsList[4]) + countUpdateErrors);

			system.debug('@CORE_MassTaskUpdateHandler > handleResults UPSERT > resultsList after: ' + resultsList);
		}

        

        if(sendEmail){
            sendMail();
        }

        return resultsList;
    }

    public void sendMail(){
        system.debug('@CORE_MassTaskUpdateHandler > handleResults > settingName: ' + settingName);
        CORE_MassTaskEmailSetting__mdt emailSetting = CORE_MassTaskEmailSetting__mdt.getInstance(settingName);
        system.debug('@CORE_MassTaskUpdateHandler > handleResults > emailSetting: ' + emailSetting);

        if(emailSetting != null){
            
            String[] toAddresses = new String[] {};

            if(emailSetting.CORE_RecipientsEmailAddresses__c != null){
                for(String em : emailSetting.CORE_RecipientsEmailAddresses__c.split(';')){
                    toAddresses.add(em);
                }
            }

            if(emailSetting.CORE_RecipientsIncludeRunningUser__c){
                if(!toAddresses.contains(Userinfo.getUserEmail())){
                    toAddresses.add(Userinfo.getUserEmail());
                }
            }

            String body = '';

            if(exceptionError != null){
                body += exceptionError + '\n\n';
            }

            if(resultsList != null){
                body += String.format(emailSetting.CORE_MailBody__c, resultsList);
            }

            if(body != ''){
                Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                mail.setToAddresses(toAddresses);
                mail.setSubject(emailSetting.CORE_MailSubject__c);
                mail.setPlainTextBody(body);
                mail.setSaveAsActivity(false);
                
                if(emailSetting.CORE_SendErrorsList__c){
                    if(resultsList != null 
                       && resultsList.size() == 4
                       && resultsList[3] != ''){
                        Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
                        efa.setFileName('errors.csv');
                        efa.setBody(Blob.valueOf(resultsList[3]));
                        efa.setContentType('application/csv');

                        mail.setFileAttachments(new List<Messaging.EmailFileAttachment> { efa });
                    }
                }
                if(!Test.isRunningTest()){
                    Messaging.SendEmailResult[] emailResults = Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
                }
            }
        }
    }
}