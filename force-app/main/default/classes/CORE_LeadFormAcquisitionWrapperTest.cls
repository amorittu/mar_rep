@IsTest
public class CORE_LeadFormAcquisitionWrapperTest {
    @IsTest
    private static void GlobalWrapper_constructor_Should_initialize_attributes(){
        CORE_LeadFormAcquisitionWrapper.GlobalWrapper wrapper = new CORE_LeadFormAcquisitionWrapper.GlobalWrapper();

        System.assertNotEquals(null, wrapper.personAccount);
        System.assertNotEquals(null, wrapper.consents);
        System.assertNotEquals(null, wrapper.mainInterest);
        System.assertNotEquals(null, wrapper.prospectiveStudentComment);
        System.assertNotEquals(null, wrapper.secondInterest);
        System.assertNotEquals(null, wrapper.thirdInterest);
        System.assertNotEquals(null, wrapper.pointOfContact);
        System.assertNotEquals(null, wrapper.campaignSubscription);
    }

    @IsTest
    private static void ErrorWrapper_constructor_Should_initialize_attributes_with_list_fields_parameters(){
        List<String> fields = new List<String> {'field1', 'field2'};
        String message = 'message';
        String errorCode = 'errorCode';
        Datetime apiCallDate = System.now();

        CORE_LeadFormAcquisitionWrapper.ErrorWrapper wrapper = new CORE_LeadFormAcquisitionWrapper.ErrorWrapper(fields,message,errorCode,apiCallDate);

        System.assertEquals(fields.size(), wrapper.fields.size());
        for(Integer i = 0 ; i < fields.size() ; i++){
            System.assertEquals(fields.get(i), wrapper.fields.get(i));
        }
        System.assertEquals(message, wrapper.message);
        System.assertEquals(errorCode, wrapper.errorCode);
        System.assertEquals(apiCallDate, wrapper.apiCallDate);
    }
    
    @IsTest
    private static void ErrorWrapper_constructor_Should_initialize_attributes_with_one_field_parameters(){
        String field = 'field1';
        String message = 'message';
        String errorCode = 'errorCode';
        Datetime apiCallDate = System.now();

        CORE_LeadFormAcquisitionWrapper.ErrorWrapper wrapper = new CORE_LeadFormAcquisitionWrapper.ErrorWrapper(field,message,errorCode,apiCallDate);

        System.assertEquals(1 , wrapper.fields.size());
        System.assertEquals(field, wrapper.fields.get(0));
        System.assertEquals(message, wrapper.message);
        System.assertEquals(errorCode, wrapper.errorCode);
        System.assertEquals(apiCallDate, wrapper.apiCallDate);
    }

    @IsTest
    private static void ErrorWrapper_constructor_Should_initialize_attributes_without_field_parameters(){
        String message = 'message';
        String errorCode = 'errorCode';
        Datetime apiCallDate = System.now();

        CORE_LeadFormAcquisitionWrapper.ErrorWrapper wrapper = new CORE_LeadFormAcquisitionWrapper.ErrorWrapper(message,errorCode,apiCallDate);

        System.assertEquals(null , wrapper.fields);
        System.assertEquals(message, wrapper.message);
        System.assertEquals(errorCode, wrapper.errorCode);
        System.assertEquals(apiCallDate, wrapper.apiCallDate);
    }

    @IsTest
    private static void ResultWrapper_constructor_Should_initialize_attributess(){

        CORE_LeadFormAcquisitionWrapper.ResultWrapper wrapper = new CORE_LeadFormAcquisitionWrapper.ResultWrapper();

        System.assertNotEquals(null , wrapper.account);
        System.assertNotEquals(null , wrapper.consents);
        System.assertNotEquals(null , wrapper.opportunities);
        System.assertNotEquals(null , wrapper.pointOfContacts);
        System.assertNotEquals(null , wrapper.campaignMember);
    }
    
    @IsTest
    private static void ResultOpportunitiesWrapper_constructor_Should_initialize_attributess(){

        CORE_LeadFormAcquisitionWrapper.ResultOpportunitiesWrapper wrapper = new CORE_LeadFormAcquisitionWrapper.ResultOpportunitiesWrapper();

        System.assertNotEquals(null , wrapper.mainInterest);
        System.assertNotEquals(null , wrapper.secondInterest);
        System.assertNotEquals(null , wrapper.thirdInterest);
    }
}