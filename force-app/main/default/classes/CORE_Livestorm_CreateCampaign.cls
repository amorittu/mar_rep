public class CORE_Livestorm_CreateCampaign implements Database.Batchable<SObject>, Database.AllowsCallouts{
    Set<Id> campaignIds;

    public CORE_Livestorm_CreateCampaign(Set<Id> c){
        System.debug('CORE_Livestorm_CreateCampaign : START');
        campaignIds = c;
}


    public Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator([SELECT Id, Name, CORE_Campaign_Full_Name__c, CORE_Start_Date_and_hour__c, CORE_Timezone__c, Description, CORE_Online_event__c, CORE_Online_event_url__c, CORE_Business_Unit__c
        FROM Campaign where Id in :this.campaignIds]);
    }

    public void execute(Database.BatchableContext bc, List<Campaign> campaigns){
        System.debug('CORE_Livestorm_CreateCampaign.execute() : START');

        Map<Id,CORE_Business_Unit__c> orderBusinessUnit = CORE_Livestorm_Helper.getBuFromCampaign(campaigns);

        for(Campaign campaign : campaigns){
            System.debug('Insert new element in livestorm for : ' + CORE_Livestorm_Helper.getName(campaign));

            CORE_Livestorm__mdt livestormMetadata = CORE_Livestorm_Helper.GetMetadata(orderBusinessUnit, campaign);
            if (livestormMetadata.CORE_Is_Livestorm_Active__c){
                try {
                    CORE_Livestorm_HttpCreationResult newEvent = CreateEvent(campaign, livestormMetadata);
                    if (!newEvent.error){
                        System.debug('Event : ' + newEvent.id + ' created with link : '  + newEvent.link);
                        campaign.CORE_Online_event_url__c = newEvent.link;
                        campaign.CORE_Livestorm_Event_Id__c = newEvent.id;

                        try {
                            CORE_Livestorm_HttpCreationResult newSession = CreateSession(campaign, newEvent.id, livestormMetadata);
                            if(!newSession.error){
                                System.debug('Session : ' + newSession.id + ' created with link : '  + newSession.link);
                                campaign.CORE_Livestorm_Session_Id__c = newSession.id;
                                campaign.CORE_Livestorm_Session_Link__c = newSession.link;
                                campaign.CORE_Is_Up_to_Date_With_Livestorm__c = true;
                            }
                        } catch (Exception e) {
                            System.debug('The following exception has occurred during session creation : ' + e.getMessage());
                        }
                    } else{
                        System.debug('Cannot create session because event were not created');
                    }
                } catch (Exception e) {
                    System.debug('The following exception has occurred during event creation : ' + e.getMessage());
                }
            }
            else {
                System.debug('Not inserting event cause Livestorm is not active');
            }
        }
        Database.update(campaigns, false);
        System.debug('CORE_Livestorm_CreateCampaign.execute() : END');
    }

    public void finish(Database.BatchableContext bc){
        System.debug('CORE_Livestorm_CreateCampaign END.');
    }

    public CORE_Livestorm_HttpCreationResult CreateEvent(Campaign campaign, CORE_Livestorm__mdt livestormMetadata){
        Http http = new Http();
        HttpRequest request = new HttpRequest();

        string fullEndPoint = livestormMetadata.CORE_Default_End_Point__c + livestormMetadata.CORE_Create_Event_End_Point__c;

        request.setMethod('POST');
        request.setEndpoint(fullEndPoint);
        request.setHeader('Accept', 'application/vnd.api+json');
        request.setHeader('Authorization', livestormMetadata.CORE_Authorization__c);
        request.setHeader('Content-Type', 'application/vnd.api+json');
        

        request.setBody('{"data": {"type": "events","attributes": {"owner_id": "'+ livestormMetadata.CORE_Owner_Id__c +'","title": "' + CORE_Livestorm_Helper.getName(campaign) + '", "status": "published", "copy_from_event_id": "' + livestormMetadata.CORE_Copy_From_Event_Id__c +'", "description": "<p>' + (campaign.Description != null ? campaign.Description : '') + '</p> "}}}');
        
        Long dt1 = DateTime.now().getTime();
        DateTime dt1Date = DateTime.now();

        HttpResponse response = http.send(request);

        long dt2 = DateTime.now().getTime(); 
        DateTime dt2Date = DateTime.now();
        System.debug('http create Event execution time = ' + (dt2-dt1) + ' ms');

        CORE_Livestorm_HttpCreationResult createCampaignResult = new CORE_Livestorm_HttpCreationResult();
        if(response.getStatusCode() != 201) {
            System.debug('Event :The status code returned was not expected: ' + response.getStatusCode() + ' ' + response.getStatus());
            createCampaignResult.error = true;
        } else {
            System.debug(response.getBody());
            Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());
            Map<String, Object> data = (Map<String, Object>) results.get('data');
            Map<String, Object> attributes = (Map<String, Object>) data.get('attributes');

            createCampaignResult = new CORE_Livestorm_HttpCreationResult();

            createCampaignResult.id = (string) data.get('id');
            createCampaignResult.link = (string) attributes.get('registration_link');
            createCampaignResult.error = false;
        }

        return createCampaignResult;
    }

    public CORE_Livestorm_HttpCreationResult CreateSession(Campaign campaign, String eventId, CORE_Livestorm__mdt livestormMetadata){
        System.debug('Start creating Session for ' + CORE_Livestorm_Helper.getName(campaign) + ' with ls id  ' + eventId);

        Http http = new Http();
        HttpRequest request = new HttpRequest();
        string fullEndPoint = livestormMetadata.CORE_Default_End_Point__c + livestormMetadata.CORE_Create_Session_End_Point__c;
        fullEndPoint = String.format(fullEndPoint, new List<String>{eventId});

        request.setMethod('POST');
        request.setEndpoint(fullEndPoint);
        request.setHeader('Accept', 'application/vnd.api+json');
        request.setHeader('Authorization', livestormMetadata.CORE_Authorization__c);
        request.setHeader('Content-Type', 'application/vnd.api+json');

        request.setBody('{"data": {"type": "sessions","attributes": {"estimated_started_at": "' + CORE_Livestorm_Helper.GetSessionStartDate(campaign) + '","timezone": "' + CORE_Livestorm_Helper.GetTimezone(campaign) + '"}}}');
        
        Long dt1 = DateTime.now().getTime();
        DateTime dt1Date = DateTime.now();

        HttpResponse response = http.send(request);

        long dt2 = DateTime.now().getTime(); 
        DateTime dt2Date = DateTime.now();
        System.debug('http create session execution time = ' + (dt2-dt1) + ' ms');

        CORE_Livestorm_HttpCreationResult createCampaignResult = new CORE_Livestorm_HttpCreationResult();
        if(response.getStatusCode() != 201) {
            createCampaignResult.error = true;
            System.debug('Session : The status code returned was not expected: ' + response.getStatusCode() + ' ' + response.getStatus());
        } else {
            Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(response.getBody());
            Map<String, Object> data = (Map<String, Object>) results.get('data');
            Map<String, Object> attributes = (Map<String, Object>) data.get('attributes');

            createCampaignResult = new CORE_Livestorm_HttpCreationResult();

            createCampaignResult.id = (string) data.get('id');
            createCampaignResult.link = (string) attributes.get('room_link');
            createCampaignResult.error = false;
        }

        return createCampaignResult;
    }
}