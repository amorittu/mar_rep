@IsTest
public with sharing class CORE_SobjectUtilsTest {
    @TestSetup
    static void makeData(){
        String uniqueKey = 'test';
        Lead lead = CORE_DataFaker_Lead.getLead(uniqueKey);
        Account account = CORE_DataFaker_Account.getStudentAccount(uniqueKey);
        CORE_DataFaker_CatalogHierarchy catalogHierarchy = new CORE_DataFaker_CatalogHierarchy(uniqueKey);
        Opportunity opportunity = CORE_DataFaker_Opportunity.getOpportunity(catalogHierarchy.division.Id, 
            catalogHierarchy.businessUnit.Id, 
            catalogHierarchy.studyArea.Id,
            catalogHierarchy.curriculum.Id,
            catalogHierarchy.level.Id,
            catalogHierarchy.intake.Id, 
            account.Id, 
            'Lead', 
            'Lead - New'
        );       
        
        Task task = new Task(Subject = 'test');
        Insert task;
    }

    @IsTest
    public static void getSobjectType_Should_Return_sObject_type_detected_by_id() {
        Lead lead = [SELECT Id FROM Lead LIMIT 1];
        Account account = [SELECT Id FROM Account LIMIT 1];
        Opportunity opportunity = [SELECT Id FROM Opportunity LIMIT 1];
        Task task = [SELECT Id FROM Task LIMIT 1];
        CORE_Business_Unit__c bu = [SELECT Id FROM CORE_Business_Unit__c LIMIT 1];

        String result = CORE_SobjectUtils.getSobjectType(lead.Id);
        System.assertEquals(CORE_SobjectUtils.OBJ_LEAD, result);

        result = CORE_SobjectUtils.getSobjectType(account.Id);
        System.assertEquals(CORE_SobjectUtils.OBJ_ACCOUNT, result);

        result = CORE_SobjectUtils.getSobjectType(opportunity.Id);
        System.assertEquals(CORE_SobjectUtils.OBJ_OPPORTUNITY, result);

        result = CORE_SobjectUtils.getSobjectType(task.Id);
        System.assertEquals(CORE_SobjectUtils.OBJ_TASK, result);
        
        result = CORE_SobjectUtils.getSobjectType(bu.Id);
        System.assertEquals('CORE_BUSINESS_UNIT__C', result);
        
        result = CORE_SobjectUtils.getSobjectType(null);
        System.assertEquals(CORE_SobjectUtils.OBJ_UNKNOWN, result);
    }

    @IsTest
    public static void getSObjectField_should_return_an_sobjectField_type() {
        Schema.DescribeFieldResult dfr = Opportunity.StageName.getDescribe();
        Schema.sObjectField expectedResult = dfr.getSObjectField();

        Schema.sObjectField result = CORE_SobjectUtils.getSObjectField('Opportunity', 'StageName');
        System.assertEquals(expectedResult, result);
    }

    @IsTest
    public static void isOpportunityId_Should_Return_true_if_provided_id_is_an_opportunity() {
        Opportunity opportunity = [SELECT Id FROM Opportunity];
        Boolean result = CORE_SobjectUtils.isOpportunityId(opportunity.Id);
        System.assertEquals(true, result);
    }

    @IsTest
    public static void isOpportunityId_Should_Return_false_if_provided_id_is_not_an_opportunity() {
        Lead lead = [SELECT Id FROM Lead];
        Boolean result = CORE_SobjectUtils.isOpportunityId(lead.Id);
        System.assertEquals(false, result);
    }

    @IsTest
    public static void isLeadId_Should_Return_true_if_provided_id_is_a_lead() {
        Lead lead = [SELECT Id FROM Lead];
        Boolean result = CORE_SobjectUtils.isLeadId(lead.Id);
        System.assertEquals(true, result);
    }

    @IsTest
    public static void isLeadId_Should_Return_false_if_provided_id_is_not_a_lead() {
        Opportunity opportunity = [SELECT Id FROM Opportunity];
        Boolean result = CORE_SobjectUtils.isLeadId(opportunity.Id);
        System.assertEquals(false, result);
    }

    @IsTest
    public static void isAccountId_Should_Return_true_if_provided_id_is_an_account() {
        Account account = [SELECT Id FROM Account];
        Boolean result = CORE_SobjectUtils.isAccountId(account.Id);
        System.assertEquals(true, result);
    }

    @IsTest
    public static void isAccountId_Should_Return_true_if_provided_id_is_not_an_account() {
        Opportunity opportunity = [SELECT Id FROM Opportunity];
        Boolean result = CORE_SobjectUtils.isAccountId(opportunity.Id);
        System.assertEquals(false, result);
    }

    @IsTest
    public static void doesFieldExist_Should_Return_true_when_field_exist()
    {
         Boolean result = CORE_SobjectUtils.doesFieldExist('Opportunity', 'StageName');
         System.assertEquals(true, result);
    }

    @IsTest
    public static void doesFieldExist_Should_Return_false_when_field_does_not_exist()
    {
         Boolean result = CORE_SobjectUtils.doesFieldExist('Opportunity', 'zertzertzert');
         System.assertEquals(false, result);
    }

}