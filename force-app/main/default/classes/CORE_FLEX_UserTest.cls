@IsTest
public class CORE_FLEX_UserTest {
    private static final String USER_UNIQUE_KEY = 'testSetupCORE_UserTriggerHandlerTest';       
    private static final String SID_TEST = 'testSID';       

    private class DeleteMock implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            JSONGenerator gen = JSON.createGenerator(true);    
            gen.writeStartObject();
            gen.writeStringField('status','deleted');
            gen.writeEndObject();          
            String fullJson = gen.getAsString();


            HTTPResponse res = new HTTPResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setBody(fullJson);
            res.setStatusCode(200);
            return res;
        }
    }
    private class ErrorMock1 implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            JSONGenerator gen = JSON.createGenerator(true);    
            gen.writeStringField('status','deleted');
            gen.writeEndObject();          
            String fullJson = gen.getAsString();


            HTTPResponse res = new HTTPResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setBody(fullJson);
            res.setStatusCode(200);
            return res;
        }
    }

    private class ErrorMock2 implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {         
            String fullJson = 'errror';


            HTTPResponse res = new HTTPResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setBody(fullJson);
            res.setStatusCode(500);
            return res;
        }
    }
    private class UpsertMock implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();    
            gen.writeStringField('sid',SID_TEST);
            gen.writeEndObject();          
            String fullJson = gen.getAsString();

            HTTPResponse res = new HTTPResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setBody(fullJson);
            res.setStatusCode(200);
            return res;
        }
    }

    @TestSetup
    static void makeData(){
        User userTest = CORE_DataFaker_User.getUser(USER_UNIQUE_KEY);
        userTest.CallCenterId = [Select Id From CallCenter limit 1].Id;
        update userTest;

        CORE_FLEX_Settings__c cs = new CORE_FLEX_Settings__c();
		Blob cryptoKey = Crypto.generateAesKey(256);
		Blob data = Blob.valueOf('password12');
		Blob encryptedData = Crypto.encryptWithManagedIV('AES256', cryptoKey, data);

		cs.UrlFlex__c='https://project.twil.io12';
		cs.TokenFlex__c=EncodingUtil.base64Encode(encryptedData);
		cs.KeySecret__c= EncodingUtil.base64Encode(cryptoKey);
        cs.AccountSid__c='ACxx12';
        
		insert cs;
    }

    @IsTest
    public static void getUserApiInfo_should_return_a_userApiInfo_for_creation(){
        String lastnameFilter = '%' + USER_UNIQUE_KEY + '%';
    
        User userTest = [SELECT Id,IsActive, FirstName, LastName, Name, CallcenterId, Email, CORE_FLEX_Twilio_SID__c FROM user WHERE LastName LIKE :lastnameFilter ];
        User oldUser = userTest.clone(true, true, true, true);
        oldUser.IsActive = false;

        OmniChannelConfig__mdt omniConfig = new OmniChannelConfig__mdt(
            MasterLabel = 'UNIT_TEST',
            DeveloperName = 'UNIT_TEST',
            CORE_Active_User_Deprovisioning__c = false,
            CORE_Active_User_Provisioning__c = true
        );

        CORE_FLEX_User.UserApiInfo result = CORE_FLEX_User.getUserApiInfo(userTest,oldUser,omniConfig);

        System.assertNotEquals(null, result);
        System.assertNotEquals(null, result.apiBody);
        System.assertEquals(CORE_FLEX_User.CREATE_WORKER_PATH, result.apiPath);
    }

    @IsTest
    public static void getUserApiInfo_should_return_a_userApiInfo_for_update(){
        String lastnameFilter = '%' + USER_UNIQUE_KEY + '%';
    
        User userTest = [SELECT Id,IsActive, FirstName, LastName, Name, CallcenterId, Email, CORE_FLEX_Twilio_SID__c FROM user WHERE LastName LIKE :lastnameFilter ];
        userTest.CORE_FLEX_Twilio_SID__c = SID_TEST;
        User oldUser = userTest.clone(true, true, true, true);
        oldUser.IsActive = false;

        OmniChannelConfig__mdt omniConfig = new OmniChannelConfig__mdt(
            MasterLabel = 'UNIT_TEST',
            DeveloperName = 'UNIT_TEST',
            CORE_Active_User_Deprovisioning__c = false,
            CORE_Active_User_Provisioning__c = true
        );

        CORE_FLEX_User.UserApiInfo result = CORE_FLEX_User.getUserApiInfo(userTest,oldUser,omniConfig);

        System.assertNotEquals(null, result);
        System.assertNotEquals(null, result.apiBody);
        System.assertEquals(CORE_FLEX_User.UPDATE_WORKER_PATH, result.apiPath);
    }

    @IsTest
    public static void getUserApiInfo_should_return_a_userApiInfo_for_delete(){
        String lastnameFilter = '%' + USER_UNIQUE_KEY + '%';
    
        User userTest = [SELECT Id,IsActive, FirstName, LastName, Name, CallcenterId, Email, CORE_FLEX_Twilio_SID__c FROM user WHERE LastName LIKE :lastnameFilter ];
        userTest.CORE_FLEX_Twilio_SID__c = SID_TEST;
        userTest.IsActive = false;

        User oldUser = userTest.clone(true, true, true, true);
        oldUser.IsActive = true;

        OmniChannelConfig__mdt omniConfig = new OmniChannelConfig__mdt(
            MasterLabel = 'UNIT_TEST',
            DeveloperName = 'UNIT_TEST',
            CORE_Active_User_Deprovisioning__c = true,
            CORE_Active_User_Provisioning__c = false
        );

        CORE_FLEX_User.UserApiInfo result = CORE_FLEX_User.getUserApiInfo(userTest,oldUser,omniConfig);

        System.assertNotEquals(null, result);
        System.assertNotEquals(null, result.apiBody);
        System.assertEquals(CORE_FLEX_User.DELETE_WORKER_PATH, result.apiPath);
    }


    @IsTest
    public static void executeWorkerProcess_should_launch_a_queue(){
        String lastnameFilter = '%' + USER_UNIQUE_KEY + '%';
    
        User userTest = [SELECT Id,IsActive, FirstName, LastName, Name, CallcenterId, Email, CORE_FLEX_Twilio_SID__c FROM user WHERE LastName LIKE :lastnameFilter ];
        userTest.CORE_FLEX_Twilio_SID__c = SID_TEST;
        User oldUser = userTest.clone(true, true, true, true);
        oldUser.IsActive = false;

        OmniChannelConfig__mdt omniConfig = new OmniChannelConfig__mdt(
            MasterLabel = 'UNIT_TEST',
            DeveloperName = 'UNIT_TEST',
            CORE_Active_User_Deprovisioning__c = false,
            CORE_Active_User_Provisioning__c = true
        );

        Integer nbQueue = 0;
        test.startTest();
        CORE_FLEX_User.executeWorkerProcess(
            new List<User> {userTest}, 
            new Map<Id,User>{oldUser.Id => oldUser},
            omniConfig
        );
        nbQueue = Limits.getQueueableJobs();
        test.stopTest();

        System.assert(nbQueue == 1 || nbQueue == 2);
    }

    @IsTest
    public static void sendHttpRequest_should_return_userInfoApi_in_success_if_status_code_is_200_for_upsert(){
        String lastnameFilter = '%' + USER_UNIQUE_KEY + '%';
    
        User userTest = [SELECT Id,IsActive, FirstName, LastName, Name, CallcenterId, Email, CORE_FLEX_Twilio_SID__c FROM user WHERE LastName LIKE :lastnameFilter ];
        userTest.CORE_FLEX_Twilio_SID__c = null;
        User oldUser = userTest.clone(true, true, true, true);
        oldUser.IsActive = false;

        OmniChannelConfig__mdt omniConfig = new OmniChannelConfig__mdt(
            MasterLabel = 'UNIT_TEST',
            DeveloperName = 'UNIT_TEST',
            CORE_Active_User_Deprovisioning__c = false,
            CORE_Active_User_Provisioning__c = true
        );
        Test.setMock(HttpCalloutMock.class, new UpsertMock());

        test.startTest();
        CORE_FLEX_User.UserApiInfo init = CORE_FLEX_User.getUserApiInfo(userTest,oldUser,omniConfig);
        CORE_FLEX_User.UserApiInfo result = CORE_FLEX_User.sendHttpRequest(init);
        test.stopTest();
        
        System.assertNotEquals(null,result);
        System.assertEquals(true,result.isSuccess);
        System.assertNotEquals(null,result.newUser);
        System.assertEquals(init.newUser.Id,result.newUser.Id);
        System.assertEquals(SID_TEST,result.newUser.CORE_FLEX_Twilio_SID__c);
    }

    @IsTest
    public static void sendHttpRequest_should_return_userInfoApi_in_success_if_status_code_is_200_for_delete(){
        String lastnameFilter = '%' + USER_UNIQUE_KEY + '%';
    
        User userTest = [SELECT Id,IsActive, FirstName, LastName, Name, CallcenterId, Email, CORE_FLEX_Twilio_SID__c FROM user WHERE LastName LIKE :lastnameFilter ];
        userTest.CORE_FLEX_Twilio_SID__c = SID_TEST;
        userTest.IsActive = false;

        User oldUser = userTest.clone(true, true, true, true);
        oldUser.IsActive = true;

        OmniChannelConfig__mdt omniConfig = new OmniChannelConfig__mdt(
            MasterLabel = 'UNIT_TEST',
            DeveloperName = 'UNIT_TEST',
            CORE_Active_User_Deprovisioning__c = true,
            CORE_Active_User_Provisioning__c = false
        );
        Test.setMock(HttpCalloutMock.class, new DeleteMock());

        test.startTest();
        CORE_FLEX_User.UserApiInfo init = CORE_FLEX_User.getUserApiInfo(userTest,oldUser,omniConfig);
        CORE_FLEX_User.UserApiInfo result = CORE_FLEX_User.sendHttpRequest(init);
        test.stopTest();
        
        System.assertNotEquals(null,result);
        System.assertEquals(true,result.isSuccess);
        System.assertNotEquals(null,result.newUser);
        System.assertEquals(init.newUser.Id,result.newUser.Id);
        System.assertEquals(null,result.newUser.CORE_FLEX_Twilio_SID__c);
    }

    @IsTest
    public static void sendHttpRequest_should_return_userInfoApi_not_in_success_if_status_code_is_not_200(){
        String lastnameFilter = '%' + USER_UNIQUE_KEY + '%';
    
        User userTest = [SELECT Id,IsActive, FirstName, LastName, Name, CallcenterId, Email, CORE_FLEX_Twilio_SID__c FROM user WHERE LastName LIKE :lastnameFilter ];
        userTest.CORE_FLEX_Twilio_SID__c = SID_TEST;
        userTest.IsActive = false;

        User oldUser = userTest.clone(true, true, true, true);
        oldUser.IsActive = true;

        OmniChannelConfig__mdt omniConfig = new OmniChannelConfig__mdt(
            MasterLabel = 'UNIT_TEST',
            DeveloperName = 'UNIT_TEST',
            CORE_Active_User_Deprovisioning__c = true,
            CORE_Active_User_Provisioning__c = false
        );
        Test.setMock(HttpCalloutMock.class, new ErrorMock2());

        test.startTest();
        CORE_FLEX_User.UserApiInfo init = CORE_FLEX_User.getUserApiInfo(userTest,oldUser,omniConfig);
        CORE_FLEX_User.UserApiInfo result = CORE_FLEX_User.sendHttpRequest(init);
        test.stopTest();
        
        System.assertNotEquals(null,result);
        System.assertEquals(false,result.isSuccess);
    }

    @IsTest
    public static void sendHttpRequest_should_return_userInfoApi_not_in_success_if_there_is_apex_error(){
        String lastnameFilter = '%' + USER_UNIQUE_KEY + '%';
    
        User userTest = [SELECT Id,IsActive, FirstName, LastName, Name, CallcenterId, Email, CORE_FLEX_Twilio_SID__c FROM user WHERE LastName LIKE :lastnameFilter ];
        userTest.CORE_FLEX_Twilio_SID__c = SID_TEST;
        userTest.IsActive = false;

        User oldUser = userTest.clone(true, true, true, true);
        oldUser.IsActive = true;

        OmniChannelConfig__mdt omniConfig = new OmniChannelConfig__mdt(
            MasterLabel = 'UNIT_TEST',
            DeveloperName = 'UNIT_TEST',
            CORE_Active_User_Deprovisioning__c = true,
            CORE_Active_User_Provisioning__c = false
        );
        Test.setMock(HttpCalloutMock.class, new ErrorMock1());

        test.startTest();
        CORE_FLEX_User.UserApiInfo init = CORE_FLEX_User.getUserApiInfo(userTest,oldUser,omniConfig);
        CORE_FLEX_User.UserApiInfo result = CORE_FLEX_User.sendHttpRequest(init);
        test.stopTest();
        
        System.assertNotEquals(null,result);
        System.assertEquals(false,result.isSuccess);
        System.assertNotEquals(null,result.apexException);
    }

    @IsTest
    public static void logProcess_should_insert_log_if_log_is_activated(){
        String lastnameFilter = '%' + USER_UNIQUE_KEY + '%';
    
        User userTest = [SELECT Id,IsActive, FirstName, LastName, Name, CallcenterId, Email, CORE_FLEX_Twilio_SID__c FROM user WHERE LastName LIKE :lastnameFilter ];
        userTest.CORE_FLEX_Twilio_SID__c = SID_TEST;
        userTest.IsActive = false;

        User oldUser = userTest.clone(true, true, true, true);
        oldUser.IsActive = true;

        OmniChannelConfig__mdt omniConfig = new OmniChannelConfig__mdt(
            MasterLabel = 'UNIT_TEST',
            DeveloperName = 'UNIT_TEST',
            CORE_Active_User_Deprovisioning__c = true,
            CORE_Active_User_Provisioning__c = false
        );
        CORE_FLEX_User.UserApiInfo userApiInfo = CORE_FLEX_User.getUserApiInfo(userTest,oldUser,omniConfig);
        userApiInfo.apiBody = 'test';
        userApiInfo.httpResponse = new HttpResponse();
        userApiInfo.httpResponse.setBody('bodyTest');
        userApiInfo.httpResponse.setStatusCode(200);

        CORE_FLEX_User.LogDatas logData = new CORE_FLEX_User.LogDatas(
            15,
            userApiInfo
        ); 

        CORE_LogManager logSettings = new CORE_LogManager();
        logSettings.customMetadataType = new CORE_Log_Settings__mdt(
            CORE_Log_Type__c = 'ALL'
        );
        CORE_FLEX_User.logProcess(new List<CORE_FLEX_User.LogDatas>{logData}, logSettings);

        List<CORE_LOG__c> logs = [SELECT Id,CORE_Processing_Duration__c,CORE_HTTP_Status_Code__c,CORE_Sent_Body__c,CORE_Received_Body__c,CORE_ApexExceptionError__c,CORE_Outgoing_API__c,CORE_Endpoint__c FROM CORE_LOG__c];

        System.assertEquals(1, logs.size());
        System.assertEquals(logData.processDuration, logs.get(0).CORE_Processing_Duration__c);
        System.assertEquals(200, logs.get(0).CORE_HTTP_Status_Code__c);
        System.assertEquals('test', logs.get(0).CORE_Sent_Body__c);
        System.assertEquals('bodyTest', logs.get(0).CORE_Received_Body__c);
        System.assertEquals(null, logs.get(0).CORE_ApexExceptionError__c);
        System.assertEquals(true, logs.get(0).CORE_Outgoing_API__c);
        System.assertEquals(userApiInfo.apiPath, logs.get(0).CORE_Endpoint__c);
    }
}