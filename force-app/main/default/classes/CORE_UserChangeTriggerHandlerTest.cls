@IsTest
public class CORE_UserChangeTriggerHandlerTest {
    private static final String USER_UNIQUE_KEY = 'testSetupCORE_UserChangeTriggerHandlerTest';       
    private static final String SID_TEST = 'SID_TEST';       

    private class UpsertMock implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {
            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();    
            gen.writeStringField('sid',SID_TEST);
            gen.writeEndObject();          
            String fullJson = gen.getAsString();

            HTTPResponse res = new HTTPResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setBody(fullJson);
            res.setStatusCode(200);
            return res;
        }
    }

    @TestSetup
    static void makeData(){
        Test.enableChangeDataCapture();
        User userTest = CORE_DataFaker_User.getUser(USER_UNIQUE_KEY);
        userTest.CallCenterId = [Select Id From CallCenter limit 1].Id;
        update userTest;
        //Test.getEventBus().deliver();
        CORE_FLEX_Settings__c cs = new CORE_FLEX_Settings__c();
		Blob cryptoKey = Crypto.generateAesKey(256);
		Blob data = Blob.valueOf('password12');
		Blob encryptedData = Crypto.encryptWithManagedIV('AES256', cryptoKey, data);

		cs.UrlFlex__c='https://project.twil.io12';
		cs.TokenFlex__c=EncodingUtil.base64Encode(encryptedData);
		cs.KeySecret__c= EncodingUtil.base64Encode(cryptoKey);
        cs.AccountSid__c='ACxx12';
        
		insert cs;
    }

    @IsTest
    public static void constructor_should_init_metadata() {
        CORE_UserTriggerHandler handler = new CORE_UserTriggerHandler();
        System.assertNotEquals(null, handler.omniConfig);
        System.assertNotEquals(null, handler.oldMap);
        System.assertNotEquals(null, handler.newRecords);
    }

    @IsTest
    public static void afterInsert_should_not_execute_api_if_provisioning_is_not_activated(){
        String lastnameFilter = '%' + USER_UNIQUE_KEY + '%';

        User userTest = [SELECT Id,IsActive, FirstName, LastName, Name, CallcenterId, Email, CORE_FLEX_Twilio_SID__c FROM user WHERE LastName LIKE :lastnameFilter ];

        Integer nbQueue = 0;
        Test.startTest();
        Test.enableChangeDataCapture();
        userTest.email = 'updatedEmail@gmail.com';
        update userTest;
        Test.getEventBus().deliver();
        nbQueue = Limits.getQueueableJobs();
        Test.stopTest();

        System.assertEquals(null, [Select Id,CORE_FLEX_Twilio_SID__c from user where Id = :userTest.Id].CORE_FLEX_Twilio_SID__c);
        System.assertEquals(0, nbQueue);
    }

    @IsTest
    public static void afterInsert_should_not_execute_api_if_event_is_not_about_email_update(){
        String lastnameFilter = '%' + USER_UNIQUE_KEY + '%';

        User userTest = [SELECT Id,IsActive, FirstName, LastName, Name, CallcenterId, Email, CORE_FLEX_Twilio_SID__c FROM user WHERE LastName LIKE :lastnameFilter ];

        Integer nbQueue = 0;
        Test.startTest();
        Test.enableChangeDataCapture();
        userTest.LastName = 'utestLastNameUpdate';
        update userTest;
        Test.getEventBus().deliver();
        nbQueue = Limits.getQueueableJobs();
        Test.stopTest();

        System.assertEquals(null, [Select Id,CORE_FLEX_Twilio_SID__c from user where Id = :userTest.Id].CORE_FLEX_Twilio_SID__c);
        System.assertEquals(0, nbQueue);
    }

    @IsTest
    public static void afterInsert_should_execute_api_when_provisioning_is_activated_and_update_is_related_to_an_email(){
        String lastnameFilter = '%' + USER_UNIQUE_KEY + '%';

        User userTest = [SELECT Id,IsActive, FirstName, LastName, Name, CallcenterId, Email, CORE_FLEX_Twilio_SID__c FROM user WHERE LastName LIKE :lastnameFilter ];

        Integer nbQueue = 0;
        Test.setMock(HttpCalloutMock.class, new UpsertMock());

        Test.startTest();
        CORE_UserChangeEventTriggerHandler.forceProvisioningForTest = true;
        Test.enableChangeDataCapture();
        userTest.email = 'updatedEmail@gmail.com';
        update userTest;
        Test.getEventBus().deliver();
        Test.stopTest();

        User user = [Select Id,CORE_FLEX_Twilio_SID__c, Email from user where Id = :userTest.Id];
        System.assertEquals(SID_TEST, [Select Id,CORE_FLEX_Twilio_SID__c from user where Id = :userTest.Id].CORE_FLEX_Twilio_SID__c);
    }
}