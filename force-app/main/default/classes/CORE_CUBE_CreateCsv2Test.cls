@IsTest
global with sharing class CORE_CUBE_CreateCsv2Test {
    @isTest
    public static void executeTest() {
        CORE_CUBE_CreateCsv2 testCoreCube = new CORE_CUBE_CreateCsv2();
        SchedulableContext ctx;
        Test.setMock(HttpCalloutMock.class, new CubeCalloutMock());
        Test.startTest();
        testCoreCube.execute(ctx);
        Test.stopTest();
        List<CORE_CUBE_Log__c> myLog = [SELECT Id,Name,IsPassed__c,ErrorMessage__c,ObjectName__c,date_donne_extraite__c,dateOk__c FROM CORE_CUBE_Log__c];
        System.assertEquals(3, myLog.size());
    }

    global class CubeCalloutMock implements HttpCalloutMock {
        global HttpResponse respond(HttpRequest req) {
        // Create a fake response
        HttpResponse res = new HttpResponse();
        res.setHeader('Content-Type', 'application/json');
        res.setBody('{"status":"success"}');
        res.setStatusCode(200);
        return res;
        }
    }
}