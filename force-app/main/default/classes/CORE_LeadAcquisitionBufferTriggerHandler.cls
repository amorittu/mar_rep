public without sharing class CORE_LeadAcquisitionBufferTriggerHandler { 
    public static void executeLFAWorkers(List<CORE_LeadAcquisitionBuffer__c> buffers) 
    {
        Set<Id> onlineShopBufferIds = new Set<Id>();
        Set<Id> standardBufferIds = new Set<Id>();

        for(CORE_LeadAcquisitionBuffer__c currentRecord : buffers){
            CORE_AsyncLFABatch.BUFFER_TYPE bufferType = getBufferProcess(currentRecord);
            switch on bufferType {
                when ONLINE_SHOP {
                    onlineShopBufferIds.add(currentRecord.Id);
                }
                when else {
                    standardBufferIds.add(currentRecord.Id);
                }
            }
        }

        if(!onlineShopBufferIds.isEmpty()){
            CORE_AsyncLFABatch.BUFFER_TYPE bufferType = CORE_AsyncLFABatch.BUFFER_TYPE.ONLINE_SHOP;
            CORE_AsyncLFABatch lfaProcessBatch = new CORE_AsyncLFABatch(onlineShopBufferIds,bufferType);
            Database.executeBatch(lfaProcessBatch,100);

        }

        if(!standardBufferIds.isEmpty()){
            CORE_AsyncLFABatch.BUFFER_TYPE bufferType = CORE_AsyncLFABatch.BUFFER_TYPE.STANDARD;
            CORE_AsyncLFABatch lfaProcessBatch = new CORE_AsyncLFABatch(standardBufferIds,bufferType);
            Database.executeBatch(lfaProcessBatch,100);

        }
    }

    private static CORE_AsyncLFABatch.BUFFER_TYPE getBufferProcess(CORE_LeadAcquisitionBuffer__c buffer){
        if(!String.isBlank(buffer.CORE_MI_OnlineShop_ExternalId__c) || !String.isBlank(buffer.CORE_SI_OnlineShop_ExternalId__c) || !String.isBlank(buffer.CORE_TI_OnlineShop_ExternalId__c)){
            return CORE_AsyncLFABatch.BUFFER_TYPE.ONLINE_SHOP;
        } else if(!String.isBlank(buffer.CORE_MI_AOL_ExternalId__c) || !String.isBlank(buffer.CORE_SI_AOL_ExternalId__c) || !String.isBlank(buffer.CORE_TI_AOL_ExternalId__c)){
            return CORE_AsyncLFABatch.BUFFER_TYPE.AOL;
        } else {
            return CORE_AsyncLFABatch.BUFFER_TYPE.STANDARD;
        }
    }
}