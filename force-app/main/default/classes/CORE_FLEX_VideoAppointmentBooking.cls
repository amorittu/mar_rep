/**
* @author Zameer Khodabocus - Almavia
* @date 2022-04-12
* @group Twilio
* @description Class used create and send request to Twilio API for videa appointment booking
* @TestClass CORE_ServiceAppointmentTriggerTest
*/
public without sharing class CORE_FLEX_VideoAppointmentBooking {

    private static final String PROCESS_NAME = 'Twilio_VideoAppointmentBooking';

    public static void videoAppointmentBookingProcess(ServiceAppointment serviceApp){
        String pathName = '/sfdc/video/schedule';
        String phoneNumbers = '';
        String twilioPhone = '';
        String contactPhone = '';
        String workerEmail = '';
        Boolean isUpdate = false;
        LogData newLogData = new LogData();
        Long processDuration = null;
        Integer statusCode = null;
        String reqBody = '';
        String respBody = '';

        try {

            //Get Parent Opportunity
            try{
                Opportunity opp = [SELECT Id, CORE_OPP_Business_Unit__r.CORE_Phone_numbers__c, Account.PersonMobilePhone, Account.CORE_Mobile_2__pc,
                        Account.CORE_Mobile_3__pc
                FROM Opportunity
                WHERE Id = :serviceApp.ParentRecordId LIMIT 1];

                phoneNumbers = opp.CORE_OPP_Business_Unit__r.CORE_Phone_numbers__c;

                //Get Contact Phone number
                contactPhone = (opp.Account.PersonMobilePhone != null) ? opp.Account.PersonMobilePhone
                        : (opp.Account.CORE_Mobile_2__pc != null) ? opp.Account.CORE_Mobile_2__pc
                                : (opp.Account.CORE_Mobile_3__pc != null) ? opp.Account.CORE_Mobile_3__pc
                                        : null;
            }
            catch(Exception e){
                phoneNumbers = serviceApp.WorkType.CORE_Business_Unit__r.CORE_Phone_numbers__c;
            }

            //Get first twilio phone available
            for(String phoneNum : phoneNumbers.split(';')){
                twilioPhone = phoneNum;
                break;
            }

            //Get worker email
            AssignedResource currentAssignedRes = [SELECT Id, ServiceResourceId, ServiceResource.RelatedRecord.Email
                                                    FROM AssignedResource
                                                    WHERE ServiceAppointmentId = :serviceApp.Id
                                                    AND ServiceResource.IsActive = TRUE LIMIT 1];

            system.debug('@videoAppointmentBookingProcess > currentAssignedRes: ' + currentAssignedRes);
            workerEmail = currentAssignedRes.ServiceResource.RelatedRecord.Email;

            HttpRequest req = CORE_FLEX_Service.createRequestTwilio(pathName);

            //For testing - to be removed
            //twilioPhone = '+33757597171';
            //contactPhone = '+33666864041';
            //workerEmail = 'zameer.khodabocus@almaviacx.com';

            JSONGenerator gen = JSON.createGenerator(true);
            gen.writeStartObject();
            gen.writeStringField('camp', twilioPhone);//Twilio phone from BU

            if(contactPhone != null
               && contactPhone != ''){

                gen.writeStringField('contact', contactPhone);//contact Phone number of the client
            }

            gen.writeStringField('emailWorker', workerEmail);//assigned resource email address userinfo.getUserEmail()
            gen.writeStringField('dateRangeStart', serviceApp.SchedStartTime.formatGMT('YYYY-MM-dd\'T\'kk:mm:ss\'z\''));//datetime.now().addHours(1).format('YYYY-MM-dd\'T\'hh:mm:ss\'z\'')
            gen.writeStringField('dateRangeEnd', serviceApp.SchedEndTime.formatGMT('YYYY-MM-dd\'T\'kk:mm:ss\'z\''));//datetime.now().addHours(2).format('YYYY-MM-dd\'T\'hh:mm:ss\'z\'')

            if(serviceApp.CORE_VideoMeetingId__c != null){
                gen.writeStringField('key', serviceApp.CORE_VideoMeetingId__c);
                isUpdate = true;
            }

            gen.writeEndObject();

            reqBody = gen.getAsString();
            system.debug('@videoAppointmentBookingProcess > reqBody: ' + reqBody);

            req.setBody(reqBody);

            system.debug('@videoAppointmentBookingProcess > req: ' + req);

            Long sentTime = datetime.now().getTime();
            Http http = new Http();
            HTTPResponse res = http.send(req);
            Long receivedTime = datetime.now().getTime();
            processDuration = receivedTime - sentTime;

            respBody = res.getBody();
            system.debug('Result : ' + JSON.serializePretty(respBody) + ', status:' + res.getStatusCode());

            statusCode = res.getStatusCode();

            if(statusCode == 200) {
                VideoInfoWrapper videoInfoWrap = (VideoInfoWrapper)JSON.deserialize(respBody, VideoInfoWrapper.class);
                system.debug('@videoAppointmentBookingProcess > videoInfoWrap: ' + videoInfoWrap);

                //Update Service Appointment with meeting details
                serviceApp.CORE_VideoMeetingId__c = videoInfoWrap.key;
                serviceApp.CORE_VideoMeetingUrl__c = videoInfoWrap.clientUrl;
                serviceApp.Status = 'Scheduled';
                serviceApp.ServiceNote = '';

                database.update(serviceApp, true);

                //Update generated Event with meeting details
                Event evt = [SELECT Id, CORE_VideoMeetingId__c, CORE_VideoMeetingUrl__c
                FROM Event
                WHERE ServiceAppointmentId = :serviceApp.Id
                AND StartDateTime = :serviceApp.SchedStartTime
                AND EndDateTime = :serviceApp.SchedEndTime LIMIT 1];

                evt.CORE_VideoMeetingId__c = serviceApp.CORE_VideoMeetingId__c;
                evt.CORE_VideoMeetingUrl__c = serviceApp.CORE_VideoMeetingUrl__c;

                database.update(evt, true);
            }
            else{
                newLogData.processDuration = processDuration;
                newLogData.endpoint = pathName;
                newLogData.statusCode = statusCode;
                newLogData.reqBody = reqBody;
                newLogData.respBody = respBody;
                newLogData.recordId = serviceApp.Id;

                logProcess(newLogData);

                serviceApp.Status = 'CORE_PKL_Error';
                serviceApp.ServiceNote = respBody;
                database.update(serviceApp, true);

                sendErrorEmail(newLogData.recordId, respBody);
            }
        }
        catch(Exception e){
            system.debug('Exception error in videoAppointmentBookingProcess: ' + e.getMessage() + ' => ' + e.getStackTraceString());

            newLogData.processDuration = processDuration;
            newLogData.endpoint = pathName;
            newLogData.statusCode = statusCode;
            newLogData.reqBody = reqBody;
            newLogData.respBody = respBody;
            newLogData.apexException = e.getMessage() + '\n' + e.getStackTraceString();
            newLogData.recordId = serviceApp.Id;

            logProcess(newLogData);

            serviceApp.Status = 'CORE_PKL_Error';
            serviceApp.ServiceNote = newLogData.apexException;
            database.update(serviceApp, true);

            sendErrorEmail(newLogData.recordId, newLogData.apexException);
        }
    }

    @TestVisible
    private static void logProcess(LogData newLogData){
        CORE_LogManager logSettings = new CORE_LogManager(PROCESS_NAME);

        if(!logSettings.isNone()){

            CORE_LOG__c log = new CORE_LOG__c(
                    CORE_API_Name__c = PROCESS_NAME,
                    CORE_Processing_Duration__c = newLogData.processDuration,
                    CORE_HTTP_Status_Code__c = newLogData.statusCode,
                    CORE_Sent_Body__c = newLogData.reqBody,
                    CORE_Received_Body__c = newLogData.respBody,
                    CORE_Outgoing_API__c = true,
                    CORE_Endpoint__c = newLogData.endpoint,
                    CORE_ApexExceptionError__c = newLogData.apexException,
                    CORE_RecordId__c = newLogData.recordId
            );

            if(logSettings.isAllLogged()
                    || logSettings.isErrorLogged()){ // is All logged

                database.insert(log, true);
            }
        }
    }

    @TestVisible
    private static void sendErrorEmail(Id recordId, String errMsg){
        CORE_CountrySpecificSettings__c countrySetting = CORE_CountrySpecificSettings__c.getOrgDefaults();

        if(countrySetting.CORE_TwilioErrorEmailRecipients__c != null){
            String[] toAddresses = new String[] {};

            for(String em : countrySetting.CORE_TwilioErrorEmailRecipients__c.split(';')){
                toAddresses.add(em);
            }

            String body = 'Hello, an error occurred when finalizing the appointment booking : ' + URL.getSalesforceBaseUrl().toExternalForm()
                    + '/lightning/r/ServiceAppointment/' + recordId + '/view'
                    + '\n\nError detail :\n' + errMsg;

            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            mail.setToAddresses(toAddresses);
            mail.setSubject('Appointment Booking Error');
            mail.setPlainTextBody(body);
            mail.setSaveAsActivity(false);

            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }

    }

    class VideoInfoWrapper {
        public String clientUrl { get; set; }
        public String room { get; set; }
        public String key { get; set; }
    }

    class LogData {
        public Long processDuration {get;set;}
        public Integer statusCode { get; set; }
        public String reqBody { get; set; }
        public String respBody { get; set; }
        public String endpoint { get; set; }
        public String apexException { get; set; }
        public String recordId { get; set; }
    }
}