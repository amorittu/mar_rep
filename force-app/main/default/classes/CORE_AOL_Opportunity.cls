@RestResource(urlMapping='/aol/opportunity')
global class CORE_AOL_Opportunity {  
    
    public static CORE_AOL_Wrapper.ResultWrapper result { get; set; }
    public static Id overrideOwnerId;

    @HttpPost
    global static CORE_AOL_Wrapper.ResultWrapper opportunity(String aolExternalId, 
                                                             //CORE_AOL_Wrapper.OpportunityWrapper opp,
                                                             CORE_AOL_Wrapper.InterestWrapper interest, 
                                                             CORE_AOL_Wrapper.AOLProgressionFields aolProgressionFields) {

        Savepoint sp = Database.setSavepoint();
        RestResponse res = RestContext.response;

        // Initiating logs
        Long dt1 = DateTime.now().getTime();
        Long dt2;
        CORE_LogManager logSettings;
        CORE_LOG__c log = new CORE_LOG__c(CORE_API_Name__c = 'AOL');
        Boolean isErrorLog = false;
        logSettings = new CORE_LogManager('AOL');

        try {

            res.addHeader('Content-Type', 'application/json');

            Opportunity opp_aol = createOpportunityProcess(aolExternalId, interest, aolProgressionFields);

            CORE_AOL_Wrapper.OpportunityWrapper opp = new CORE_AOL_Wrapper.OpportunityWrapper();

            result = new CORE_AOL_Wrapper.ResultWrapper(CORE_AOL_Constants.SUCCESS_STATUS, '', opp);

            //result.statut = 'OK';
            result.opportunity.Id = opp_aol.Id;
            result.opportunity.applicationOnlineId = opp_aol?.CORE_OPP_Application_Online_ID__c;
            result.opportunity.sisOpportunityId = opp_aol?.CORE_OPP_SIS_Opportunity_Id__c;
            result.opportunity.sisStudentAcademicRecordId = opp_aol?.CORE_OPP_SIS_Student_Academic_Record_ID__c;
            result.opportunity.accountId = opp_aol.AccountId;
            res.responseBody = Blob.valueOf(JSON.serialize(result, false));
            res.statusCode = 200;

            system.debug('result: ' + result);
            //system.debug('res: ' + res);

        }
        catch(Exception e){

            result = new CORE_AOL_Wrapper.ResultWrapper(CORE_AOL_Constants.ERROR_STATUS, e.getMessage(), new CORE_AOL_Wrapper.OpportunityWrapper());
            res.statusCode = 400;
            system.debug('@Error Exception: ' + e.getMessage() + ' at ' + e.getStackTraceString());

            Database.rollback(sp);
        } finally{
            dt2 = DateTime.now().getTime();
            System.debug('CORE_AOL_AcademicDiplomaHistory(urlMapping=\'/aol/academic-diploma-history\') : T0TAL TIME OF EXECUTION = ' + (dt2-dt1) + ' ms');
            // creating log
            log.CORE_Processing_Duration__c =(dt2-dt1);
            log.CORE_HTTP_Status_Code__c = res.statusCode;
            log.CORE_Sent_Body__c = JSON.serialize(result);
            String reponseJSON = '{"aolExternalId" : "' + aolExternalId + '",';
            reponseJSON += '"interest" : ' + JSON.serialize(interest) + ',"aolProgressionFields" : ' + JSON.serialize(aolProgressionFields) + '}';
            log.CORE_Received_Body__c =  reponseJSON;
            
            if(!logSettings.isNone()){
                if(logSettings.isAllLogged()){ // is All logged
                    CORE_LogManager.createLog(log);
                }else if(logSettings.isErrorLogged() && isErrorLog){ // is Error
                    CORE_LogManager.createLog(log);
                }
            }
        }

        return result;
    }

    public static Opportunity createOpportunityProcess(String aolExternalId, 
                                                        CORE_AOL_Wrapper.InterestWrapper interest, 
                                                        CORE_AOL_Wrapper.AOLProgressionFields aolProgressionFields){
        
        Boolean isInterestFull = checkFullInterest(interest);

        Opportunity opp_aol = new Opportunity();
        //opp_aol.AccountId = opp.accountId;
        opp_aol.AccountId = interest.personAccountId;
        opp_aol.CORE_OPP_AOL_Opportunity_Id__c = aolExternalId;

        if(!isInterestFull){//Partial Interest sent
            opp_aol.StageName = CORE_AOL_Constants.PKL_OPP_STAGENAME_PROSPECT;
            opp_aol.CORE_OPP_Sub_Status__c = CORE_AOL_Constants.PKL_OPP_SUBSTATUS_PROSPECTAPPSTARTED;
        }
        else{//Full Interest sent
            opp_aol.StageName = CORE_AOL_Constants.PKL_OPP_STAGENAME_LEAD;
            opp_aol.CORE_OPP_Sub_Status__c = CORE_AOL_Constants.PKL_OPP_SUBSTATUS_LEADAPPSTARTED; 
        }

        CORE_Business_Unit__c bu = (CORE_Business_Unit__c)getRelatedObj('CORE_Business_Unit__c', 'Id, CORE_Parent_Division__c', 'CORE_Business_Unit_ExternalId__c', interest.businessUnit);
        CORE_Study_Area__c studyArea = (CORE_Study_Area__c)getRelatedObj('CORE_Study_Area__c', 'Id', 'CORE_Study_Area_ExternalId__c', interest.studyArea);
        CORE_Curriculum__c curriculum = (CORE_Curriculum__c)getRelatedObj('CORE_Curriculum__c', 'Id', 'CORE_Curriculum_ExternalId__c', interest.curriculum);
        CORE_Level__c level = (CORE_Level__c)getRelatedObj('CORE_Level__c', 'Id', 'CORE_Level_ExternalId__c', interest.level);
        CORE_Intake__c intake = (CORE_Intake__c)getRelatedObj('CORE_Intake__c', 'Id', 'CORE_Intake_ExternalId__c', interest.intake);

        opp_aol.CORE_OPP_Division__c = bu?.CORE_Parent_Division__c;
        opp_aol.CORE_OPP_Business_Unit__c = bu?.Id;
        opp_aol.CORE_OPP_Study_Area__c = studyArea?.Id;
        opp_aol.CORE_OPP_Curriculum__c = curriculum?.Id;
        opp_aol.CORE_OPP_Level__c = level?.Id;
        opp_aol.CORE_OPP_Intake__c = intake?.Id;

        opp_aol.CORE_OPP_Main_Opportunity__c = (interest.isMain == null) ? false :interest.isMain;
        opp_aol.CORE_Is_AOL_Retail_Register__c = (interest.isAolRetailRegistered == null) ? false :interest.isAolRetailRegistered;

        //AOL Progression
        opp_aol.CORE_Document_progression__c = aolProgressionFields.documentProgression;
        opp_aol.CORE_First_document_upload_date__c = aolProgressionFields.firstDocumentUploadDate;
        opp_aol.CORE_Last_AOL_event_date__c = aolProgressionFields.lastAolEventDate;
        opp_aol.CORE_OPP_Application_Online_Advancement__c = aolProgressionFields.aolProgression;
        opp_aol.CORE_Is_AOL_Register__c = (aolProgressionFields.isAolOnlineRegistered == null) ? false :aolProgressionFields.isAolOnlineRegistered;

        opp_aol.CORE_OPP_Academic_Year__c = CORE_AcademicYearProcess.getAcademicYear();
        opp_aol.CORE_OPP_Application_started__c = date.today();

        //system.debug('opp_aol 1: ' + JSON.serializePretty(opp_aol));

        //Get default Pricebook for current BU & academic year
        try {
            CORE_PriceBook_BU__c pbBU = [SELECT Id, CORE_Price_Book__c, CORE_Business_Unit__r.CORE_BU_Technical_User__c
                                         FROM CORE_PriceBook_BU__c
                                         WHERE CORE_Academic_Year__c = :opp_aol.CORE_OPP_Academic_Year__c
                                         AND CORE_Business_Unit__c = :bu.Id LIMIT 1];

            opp_aol.Pricebook2Id = pbBU.CORE_Price_Book__c;
            opp_aol.OwnerId = (overrideOwnerId != null) ? overrideOwnerId :pbBU.CORE_Business_Unit__r.CORE_BU_Technical_User__c;
        }
        catch(Exception e){
            QueryException error = new QueryException();
            error.setMessage('Pricebook BU not found for Business Unit ' + interest.businessUnit + ' and academic year ' + opp_aol.CORE_OPP_Academic_Year__c);
            throw error;
        }

        opp_aol.CORE_OPP_Application_Online__c = true;
        opp_aol.CORE_OPP_Application_Online_ID__c = aolExternalId;

        if(isInterestFull){//Full Interest sent

            CORE_OpportunityDataMaker dataMaker = new CORE_OpportunityDataMaker();

            Account account = new Account(Id = interest.personAccountId);

            CORE_CatalogHierarchyModel catalogHierarchy = new CORE_CatalogHierarchyModel(
                null, 
                opp_aol.CORE_OPP_Business_Unit__c, 
                opp_aol.CORE_OPP_Study_Area__c,//studyAreaId, 
                opp_aol.CORE_OPP_Curriculum__c,//curriculumId, 
                opp_aol.CORE_OPP_Level__c,//levelId, 
                opp_aol.CORE_OPP_Intake__c,//intakeId, 
                null//productId
            );

            catalogHierarchy.priceBook2Id = opp_aol.Pricebook2Id;

            catalogHierarchy = CORE_OpportunityDataMaker.getCatalogHierarchy(catalogHierarchy, false);

            Boolean mainOpportunityExist = CORE_OpportunityUtils.isExistingMainOpportunity(catalogHierarchy.businessUnit.Id, account.Id, opp_aol.CORE_OPP_Academic_Year__c);

            Opportunity init_opp = dataMaker.initOpportunity(account, opp_aol.CORE_OPP_Academic_Year__c, catalogHierarchy, mainOpportunityExist);

            opp_aol.CORE_Main_Product_Interest__c = init_opp.CORE_Main_Product_Interest__c;
            opp_aol.CORE_OPP_Division__c = init_opp.CORE_OPP_Division__c;

            CORE_OpportunityMetadata metadata = new CORE_OpportunityMetadata();
            opp_aol.CloseDate = metadata.closeDate;

            insert opp_aol;

            //system.debug('opp_aol 2: ' + JSON.serializePretty(opp_aol));

            if(!String.IsBlank(opp_aol.CORE_Main_Product_Interest__c) 
               && !String.IsBlank(opp_aol.Pricebook2Id)){

                PricebookEntry pricebookEntry = dataMaker.getProductPricebookEntryByPb(opp_aol.Pricebook2Id, opp_aol.CORE_Main_Product_Interest__c);
                //system.debug('pricebookEntry: ' + JSON.serializePretty(pricebookEntry));

                if(pricebookEntry != NULL){
                    OpportunityLineItem oppLineItem = dataMaker.initOppLineItem(opp_aol, pricebookEntry);

                    //system.debug('oppLineItem: ' + JSON.serializePretty(oppLineItem));
                    insert oppLineItem;
                }
            }
        }
        else{
            CORE_OpportunityMetadata metadata = new CORE_OpportunityMetadata();
            opp_aol.CloseDate = metadata.closeDate;
            insert opp_aol;
        }

        CORE_Point_of_Contact__c poc = new CORE_Point_of_Contact__c(
            CORE_Enrollment_Channel__c = (interest.isAolRetailRegistered) ? CORE_AOL_Constants.PKL_ACQPOC_ENROLCHANNEL_RETAIL :CORE_AOL_Constants.PKL_ACQPOC_ENROLCHANNEL_DIRECT,
            CORE_Insertion_Mode__c = CORE_AOL_Constants.PKL_ACQPOC_INSERTMODE_AUTOMATIC,
            CORE_Insertion_Date__c = date.today(),
            CORE_Nature__c = CORE_AOL_Constants.PKL_ACQPOC_NATURE_ONLINE,
            CORE_Capture_Channel__c = CORE_AOL_Constants.PKL_ACQPOC_CAPCHANNEL_AOL,
            CORE_Origin__c = '',//to confirm
            CORE_Source__c = '',//to confirm
            CORE_Opportunity__c = opp_aol.Id
        );

        System.debug('@createOpportunityProcess > poc = ' + poc);

        insert poc;

        opp_aol = [SELECT Id, AccountId, CORE_OPP_Academic_Year__c, CORE_OPP_Business_Unit__c, CORE_OPP_Application_Online_ID__c, CORE_OPP_SIS_Opportunity_Id__c,
                   CORE_OPP_SIS_Student_Academic_Record_ID__c
                   FROM Opportunity 
                   WHERE Id = :opp_aol.Id LIMIT 1];

        if(interest.isMain){
            updateMainOpps(opp_aol.Id, opp_aol.CORE_OPP_Business_Unit__c, interest.personAccountId, opp_aol.CORE_OPP_Academic_Year__c);
        }

        return opp_aol;
    }
    public static boolean checkFullInterest(CORE_AOL_Wrapper.InterestWrapper interest){
        if(interest.businessUnit != null 
           && interest.curriculum != null
           && interest.intake != null
           && interest.level != null
           && interest.studyArea != null
           && interest.isAolRetailRegistered != null
           && interest.isMain != null){

            return true;
        }
        else{
            return false;
        }
    }

    public static sObject getRelatedObj(String objAPI, String fields, String externalField, String externalID){
        List<sObject> results = database.query('SELECT ' + fields + ' FROM ' + objAPI + ' WHERE ' + externalField + ' = :externalID LIMIT 1');
        if(results.isEmpty()){
            return null;
        } else {
            return results.get(0);
        }
    }

    public static void updateMainOpps(Id currentOppId, Id buId, Id accountId, String academicYear){
        List<Opportunity> oppsList = [SELECT Id, CORE_OPP_Main_Opportunity__c 
                                      FROM Opportunity
                                      WHERE Id != :currentOppId
                                      AND AccountId = :accountId
                                      AND CORE_OPP_Academic_Year__c = :academicYear
                                      AND CORE_OPP_Business_Unit__c = :buId
                                      AND CORE_OPP_Main_Opportunity__c = TRUE];

        for(Opportunity opp : oppsList){
            opp.CORE_OPP_Main_Opportunity__c = false;
        }

         update oppsList;
    }
}