/*************************************************************************************************************
 * @name			IT_EmailPlatformEventTriggerHandler
 * @author			Andrea Morittu <amorittu@atlantic-technologies.com>
 * @created			19 / 04 / 2022
 * @description		Trigger Handler of platform event record
 *
 * Changes (version)
 * -----------------------------------------------------------------------------------------------------------
 * 				No.		Date			Author					Description
 * 				----	------------	--------------------	----------------------------------------------
 * @version		1.0		2022-19-04		Andrea Morittu			Creation
 *
**************************************************************************************************************/
public without sharing class IT_EmailPlatformEventTriggerHandler {
    public static final String CLASS_NAME = IT_EmailPlatformEventTriggerHandler.class.getName();

    /*********************************************************************************************************
     * @name			afterInsert
     * @author			Andrea Morittu <amorittu@atlantic-technologies.com>
     * @created			19 / 04 / 2022
     * @description		after insert trigger handler
     * @param			new List record
     * @return			NA
    **********************************************************************************************************/
    public void afterInsert(List<IT_EmailPlatformEvent__e> newRecordsList) {
        System.debug(CLASS_NAME + ' - afterInsert - START');
        loginAndSendMCIntegration(JSON.serialize(newRecordsList) );
        System.debug(CLASS_NAME + ' - afterInsert - END');
    }

    /*********************************************************************************************************
     * @name			fireMCemailJourney
     * @author			Andrea Morittu <amorittu@atlantic-technologies.com>
     * @created			19 / 04 / 2022
     * @description		fire marketing cloud login and post http call
     * @param			new List record
     * @return			NA
    **********************************************************************************************************/
    @Future(Callout=true)
    public static void loginAndSendMCIntegration ( String newRecordsListJSON ) {
        System.debug(CLASS_NAME + ' - loginAndSendMCIntegration - START');
        IT_CalloutLogger__c[] calloutLoggers = new IT_CalloutLogger__c[]{};
        
        List<IT_EmailPlatformEvent__e> newRecordsList = (List<IT_EmailPlatformEvent__e>) JSON.deserialize(newRecordsListJSON, List<IT_EmailPlatformEvent__e>.class );

        for (IT_EmailPlatformEvent__e record : newRecordsList) {
            IT_CalloutLogger__c calloutLogger = new IT_CalloutLogger__c();

            IT_MarketingCloudIntegration mcIntegration = loginMCmethod(record.IT_DivisionExternalId__c);
            JSONGenerator jsonGen = IT_MarketingCloudJSON.createJSONTemplateEmail(record);
            String body = jsonGen.getAsString();

            calloutLogger.IT_RequestBody__c         = body;
            calloutLogger.IT_RequestDateTime__c     = System.now();
            calloutLogger.IT_OpportunityId__c       = record.IT_OpportunityId__c;

            
            System.debug(CLASS_NAME +  '- body :  ' + body);
            IT_MarketingCloudAPI__mdt marketingCloudApi = mcIntegration.getMarketingCloudApi();
            if (marketingCloudApi != null) {
                IT_CalloutLogger__c logger = fireMCemailJourney(body, marketingCloudApi.IT_EndpointRest__c + marketingCloudApi.IT_FireEventAPI__c ,  mcIntegration.getAccessToken(), calloutLogger );
                calloutLoggers.add(logger);
            }

        }

        if ( ! calloutLoggers.isEmpty() ) {
            Database.SaveResult[] results = Database.insert(calloutLoggers, false);
        }
        
        System.debug(CLASS_NAME + ' - loginAndSendMCIntegration - END');
    }

    /*********************************************************************************************************
     * @name			fireMCemailJourney
     * @author			Andrea Morittu <amorittu@atlantic-technologies.com>
     * @created			19 / 04 / 2022
     * @description		fire marketing cloud post http call
     * @param			new List record
     * @return			NA
    **********************************************************************************************************/
    public static IT_CalloutLogger__c fireMCemailJourney (String body, String endPoint, String token, IT_CalloutLogger__c calloutLogger ) {


        HttpRequest request = prepareRequest(body, endPoint, token);
        
        Http http1 = new Http();
        try {
            HttpResponse res1 = http1.send(request);
            System.debug('### response: ' + res1.getBody());
            System.debug('### res1.getStatus(): ' + res1.getStatus());
            System.debug('### getStatusCode(): ' + res1.getStatusCode());

            if ( res1.getStatusCode() != 200) {
                calloutLogger.IT_isError__c = true;
            }
            
            calloutLogger.IT_ResponseBody__c = res1.getBody() ;
            calloutLogger.IT_ResponseDateTime__c = System.now();
            calloutLogger.IT_ResponseStatus__c = String.valueOf(res1.getStatus());
            calloutLogger.IT_ResponseStatusCode__c = Integer.valueOf(res1.getStatusCode());
        } catch (Exception ex) {
            System.debug('### exception: ' + ex.getMessage() + ', lineNumber: ' + ex.getLineNumber());
            calloutLogger.IT_isError__c = true;
            calloutLogger.IT_ResponseBody__c = ex.getStackTraceString();
            calloutLogger.IT_ResponseDateTime__c = System.now();
        } finally {
            return calloutLogger;
        }
        return null;
    }

    /*********************************************************************************************************
     * @name			prepareRequest
     * @author			Andrea Morittu <amorittu@atlantic-technologies.com>
     * @created			24 / 05 / 2022
     * @description		prepare the http request to call marketing cloud
     * @param			String param : Explanation
     * @return			HttpRequest -> return the request to Marketing cloud
    **********************************************************************************************************/
    private static HttpRequest prepareRequest(String body, String endPoint, String token) {
        HttpRequest toReturn = new HttpRequest();
        toReturn.setMethod('POST');
        toReturn.setHeader('Content-Type', 'application/json');
        toReturn.setEndpoint(endPoint);
        toReturn.setHeader('Authorization', 'Bearer ' + token);
        toReturn.setBody(body);

        return toReturn;
    }
    
    /*********************************************************************************************************
     * @name			fireMCemailJourney
     * @author			Andrea Morittu <amorittu@atlantic-technologies.com>
     * @created			19 / 04 / 2022
     * @description		fire marketing cloud login http call
     * @param			new List record
     * @return			NA
    **********************************************************************************************************/
	public static IT_MarketingCloudIntegration loginMCmethod ( String divisionExternalId ) {
        return IT_MarketingCloudIntegration.getInstance(
            divisionExternalId
        );
    }
}