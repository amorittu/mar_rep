/*******************************************************/
/**     @Author: Valentin Pitel                       **/
/**     @OriginClass: CORE_OpportunityDataMaker      **/
/*******************************************************/
@IsTest
public class CORE_OpportunityDataMakerTest {

    @TestSetup
    static void makeData(){
        CORE_DataFaker_CatalogHierarchy catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test');
        Account account = CORE_DataFaker_Account.getStudentAccount('test');
        Opportunity Opportunity = CORE_DataFaker_Opportunity.getOpportunity(catalogHierarchy.division.Id, 
            catalogHierarchy.businessUnit.Id, 
            catalogHierarchy.studyArea.Id,
            catalogHierarchy.curriculum.Id,
            catalogHierarchy.level.Id,
            catalogHierarchy.intake.Id,
            account.Id, 
            'Lead', 
            'Lead - New'
        );
    }

    @IsTest
    public static void CORE_OpportunityDataMaker_constructor_should_initialize_attribute_from_custom_metadata() {
        Test.startTest();
        CORE_OpportunityDataMaker dataMaker = new CORE_OpportunityDataMaker();
        Test.stopTest();

        CORE_Opportunity_creation_setting__mdt opportunityMetadata = dataMaker.metadata.opportunityMetadata;

        System.assertNotEquals(NULL, opportunityMetadata);
        System.assertNotEquals(NULL, opportunityMetadata.CORE_OPP_Full_mode_Status__c);

        System.assertEquals(opportunityMetadata.CORE_OPP_Full_mode_Status__c , dataMaker.metadata.statusFull);
        System.assertEquals(opportunityMetadata.CORE_OPP_Full_mode_SubStatus__c , dataMaker.metadata.subStatusFull);
        System.assertEquals(opportunityMetadata.CORE_OPP_Partial_mode_Status__c , dataMaker.metadata.statusPartial);
        System.assertEquals(opportunityMetadata.CORE_OPP_Partial_mode_SubStatus__c , dataMaker.metadata.subStatusPartial);
        System.assertEquals(Schema.SObjectType.Opportunity.getRecordTypeInfosByDeveloperName().get(opportunityMetadata.CORE_OPP_RecordType__c).getRecordTypeId() , dataMaker.metadata.oppRecordTypeId);
        
        Date closeDate = Date.today().addYears((Integer) opportunityMetadata.CORE_OPP_Close_date_nb_year__c);
        System.assertEquals(closeDate , dataMaker.metadata.closeDate);
        System.assertEquals(opportunityMetadata.CORE_POC_Insertion_Mode__c , dataMaker.metadata.pocInsertionMode);
    }

    @IsTest
    public static void getCatalogHierarchy_Should_return_an_initiaized_catalogHierarchy_using_productId() {
        CORE_LeadFormAcquisitionWrapper.GlobalWrapper globalWrapper = CORE_DataFaker_LeadFormAcquWrapper.getGlobalWrapper();
        CORE_CatalogHierarchyModel catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test2').catalogHierarchy;
        CORE_Intake__c intake = [Select Id, CORE_Academic_Year__c From CORE_Intake__c where id = :catalogHierarchy.intake.Id];
        CORE_Level__c level = [Select Id From CORE_Level__c where id = :catalogHierarchy.level.Id];
        CORE_Curriculum__c curriculum = [Select Id From CORE_Curriculum__c where id = :catalogHierarchy.curriculum.Id];
        CORE_Study_Area__c studyArea = [Select Id From CORE_Study_Area__c where id = :catalogHierarchy.studyArea.Id];
        CORE_Business_Unit__c businessUnit = [Select Id From CORE_Business_Unit__c where id = :catalogHierarchy.businessUnit.Id];
        CORE_Division__c division = [Select Id, CORE_Division_ExternalId__c From CORE_Division__c where id = :catalogHierarchy.division.Id];
        catalogHierarchy.division.Id = null;
        catalogHierarchy.businessUnit.Id = null;
        catalogHierarchy.curriculum.Id = null;
        catalogHierarchy.level.Id = null;
        catalogHierarchy.isFullHierarchy = false;

        Test.startTest();
        CORE_CatalogHierarchyModel result = CORE_OpportunityDataMaker.getCatalogHierarchy(catalogHierarchy,true);
        Test.stopTest();

        System.assertNotEquals(null, result);

        System.assertNotEquals(null, result.division);
        System.assertEquals(division.CORE_Division_ExternalId__c, result.division.CORE_Division_ExternalId__c);
        System.assertNotEquals(null, result.businessUnit);
        System.assertEquals(businessUnit.Id, result.businessUnit.Id);
        System.assertNotEquals(null, result.studyArea);
        System.assertEquals(studyArea.Id, result.studyArea.Id);
        System.assertEquals(true, result.isFullHierarchy);
    }

    @IsTest
    public static void getCatalogHierarchy_Should_return_an_initiaized_catalogHierarchy_using_intake_externalId() {
        CORE_LeadFormAcquisitionWrapper.GlobalWrapper globalWrapper = CORE_DataFaker_LeadFormAcquWrapper.getGlobalWrapper();
        CORE_CatalogHierarchyModel catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test2').catalogHierarchy;
        CORE_Intake__c intake = [Select Id, CORE_Academic_Year__c From CORE_Intake__c where id = :catalogHierarchy.intake.Id];
        CORE_Level__c level = [Select Id From CORE_Level__c where id = :catalogHierarchy.level.Id];
        CORE_Curriculum__c curriculum = [Select Id From CORE_Curriculum__c where id = :catalogHierarchy.curriculum.Id];
        CORE_Study_Area__c studyArea = [Select Id From CORE_Study_Area__c where id = :catalogHierarchy.studyArea.Id];
        CORE_Business_Unit__c businessUnit = [Select Id From CORE_Business_Unit__c where id = :catalogHierarchy.businessUnit.Id];
        CORE_Division__c division = [Select Id, CORE_Division_ExternalId__c From CORE_Division__c where id = :catalogHierarchy.division.Id];
        catalogHierarchy.division.Id = null;
        catalogHierarchy.businessUnit.Id = null;
        catalogHierarchy.curriculum.Id = null;
        catalogHierarchy.level.Id = null;
        catalogHierarchy.isFullHierarchy = false;
        catalogHierarchy.product.Id = null;
        Test.startTest();
        CORE_CatalogHierarchyModel result = CORE_OpportunityDataMaker.getCatalogHierarchy(catalogHierarchy,true);
        Test.stopTest();

        System.assertNotEquals(null, result);

        System.assertNotEquals(null, result.division);
        System.assertEquals(division.CORE_Division_ExternalId__c, result.division.CORE_Division_ExternalId__c);
        System.assertNotEquals(null, result.businessUnit);
        System.assertEquals(businessUnit.Id, result.businessUnit.Id);
        System.assertNotEquals(null, result.studyArea);
        System.assertEquals(studyArea.Id, result.studyArea.Id);
        System.assertEquals(true, result.isFullHierarchy);
    }

    @IsTest
    public static void getCatalogHierarchy_Should_return_an_initiaized_catalogHierarchy_using_businessUnit_id() {
        CORE_LeadFormAcquisitionWrapper.GlobalWrapper globalWrapper = CORE_DataFaker_LeadFormAcquWrapper.getGlobalWrapper();
        CORE_CatalogHierarchyModel catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test2').catalogHierarchy;
        CORE_Intake__c intake = [Select Id, CORE_Academic_Year__c From CORE_Intake__c where id = :catalogHierarchy.intake.Id];
        CORE_Level__c level = [Select Id From CORE_Level__c where id = :catalogHierarchy.level.Id];
        CORE_Curriculum__c curriculum = [Select Id From CORE_Curriculum__c where id = :catalogHierarchy.curriculum.Id];
        CORE_Study_Area__c studyArea = [Select Id From CORE_Study_Area__c where id = :catalogHierarchy.studyArea.Id];
        CORE_Business_Unit__c businessUnit = [Select Id From CORE_Business_Unit__c where id = :catalogHierarchy.businessUnit.Id];
        CORE_Division__c division = [Select Id, CORE_Division_ExternalId__c From CORE_Division__c where id = :catalogHierarchy.division.Id];
        catalogHierarchy.division = null;
        catalogHierarchy.curriculum = null;
        catalogHierarchy.intake = null;
        catalogHierarchy.level = null;
        catalogHierarchy.isFullHierarchy = false;
        catalogHierarchy.product.Id = null;
        Test.startTest();
        CORE_CatalogHierarchyModel result = CORE_OpportunityDataMaker.getCatalogHierarchy(catalogHierarchy,false);
        Test.stopTest();

        System.assertNotEquals(null, result);

        System.assertNotEquals(null, result.division);
        System.assertEquals(division.CORE_Division_ExternalId__c, result.division.CORE_Division_ExternalId__c);
        System.assertNotEquals(null , result.division.CORE_Default_Academic_Year__c);
        System.assertNotEquals(null, result.businessUnit);
        System.assertEquals(businessUnit.Id, result.businessUnit.Id);
        System.assertNotEquals(null, result.studyArea);
        System.assertEquals(studyArea.Id, result.studyArea.Id);
        System.assertEquals(false, result.isFullHierarchy);
    }

    @IsTest
    public static void getCatalogHierarchies_Should_return_a_map_of_catalogHierarchy() {
        CORE_LeadFormAcquisitionWrapper.GlobalWrapper globalWrapper = CORE_DataFaker_LeadFormAcquWrapper.getGlobalWrapper();
        CORE_CatalogHierarchyModel catalogHierarchy2 = new CORE_DataFaker_CatalogHierarchy('test2').catalogHierarchy;
        CORE_CatalogHierarchyModel catalogHierarchy3 = new CORE_DataFaker_CatalogHierarchy('test3').catalogHierarchy;
        
        CORE_CatalogHierarchyModel catalogHierarchy4 = new CORE_DataFaker_CatalogHierarchy('test4').catalogHierarchy;
        catalogHierarchy3.intake = null;
        catalogHierarchy4.intake = null;
        catalogHierarchy4.curriculum = null;

        Map<Integer, CORE_CatalogHierarchyModel> catalogHierarchiesToUpdate = new Map<Integer, CORE_CatalogHierarchyModel>();
        catalogHierarchiesToUpdate.put(0,catalogHierarchy2);
        catalogHierarchiesToUpdate.put(1,catalogHierarchy3);
        catalogHierarchiesToUpdate.put(2,catalogHierarchy4);
        Test.startTest();
        Map<Integer,CORE_CatalogHierarchyModel> results = CORE_OpportunityDataMaker.getCatalogHierarchies(catalogHierarchiesToUpdate);
        Test.stopTest();

        System.assertNotEquals(null, results);
        System.assertNotEquals(0, results.size());
        System.assertNotEquals(null, results.get(0));
    }

    @IsTest
    public static void getCatalogFromStudyArea_Should_return_a_map_of_catalogHierarchy_initialized_using_studyArea_externalIds() {
        CORE_LeadFormAcquisitionWrapper.GlobalWrapper globalWrapper = CORE_DataFaker_LeadFormAcquWrapper.getGlobalWrapper();
        CORE_CatalogHierarchyModel catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test2').catalogHierarchy;
        CORE_Intake__c intake = [Select Id, CORE_Academic_Year__c From CORE_Intake__c where id = :catalogHierarchy.intake.Id];
        CORE_Level__c level = [Select Id From CORE_Level__c where id = :catalogHierarchy.level.Id];
        CORE_Curriculum__c curriculum = [Select Id From CORE_Curriculum__c where id = :catalogHierarchy.curriculum.Id];
        CORE_Study_Area__c studyArea = [Select Id From CORE_Study_Area__c where id = :catalogHierarchy.studyArea.Id];
        CORE_Business_Unit__c businessUnit = [Select Id From CORE_Business_Unit__c where id = :catalogHierarchy.businessUnit.Id];
        CORE_Division__c division = [Select Id, CORE_Division_ExternalId__c From CORE_Division__c where id = :catalogHierarchy.division.Id];
        catalogHierarchy.division.Id = null;
        catalogHierarchy.businessUnit.Id = null;
        catalogHierarchy.curriculum.Id = null;
        catalogHierarchy.level.Id = null;
        catalogHierarchy.isFullHierarchy = false;

        Map<Integer, CORE_CatalogHierarchyModel> catalogHierarchiesToUpdate = new Map<Integer, CORE_CatalogHierarchyModel>();
        catalogHierarchiesToUpdate.put(0,catalogHierarchy);
        Test.startTest();
        Map<Integer,CORE_CatalogHierarchyModel> results = CORE_OpportunityDataMaker.getCatalogFromStudyArea(catalogHierarchiesToUpdate);
        Test.stopTest();

        System.assertNotEquals(null, results);
        System.assertNotEquals(0, results.size());
        CORE_CatalogHierarchyModel result = results.get(0);

        System.assertNotEquals(null, result.division);
        System.assertEquals(division.CORE_Division_ExternalId__c, result.division.CORE_Division_ExternalId__c);
        System.assertNotEquals(null, result.businessUnit);
        System.assertEquals(businessUnit.Id, result.businessUnit.Id);
        System.assertNotEquals(null, result.studyArea);
        System.assertEquals(studyArea.Id, result.studyArea.Id);
        System.assertEquals(false, result.isFullHierarchy);
    }

    @IsTest
    public static void getCatalogFromCurriculums_Should_return_a_map_of_catalogHierarchy_initialized_using_curriculum_externalIds() {
        CORE_LeadFormAcquisitionWrapper.GlobalWrapper globalWrapper = CORE_DataFaker_LeadFormAcquWrapper.getGlobalWrapper();
        CORE_CatalogHierarchyModel catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test2').catalogHierarchy;
        CORE_Intake__c intake = [Select Id, CORE_Academic_Year__c From CORE_Intake__c where id = :catalogHierarchy.intake.Id];
        CORE_Level__c level = [Select Id From CORE_Level__c where id = :catalogHierarchy.level.Id];
        CORE_Curriculum__c curriculum = [Select Id From CORE_Curriculum__c where id = :catalogHierarchy.curriculum.Id];
        CORE_Study_Area__c studyArea = [Select Id From CORE_Study_Area__c where id = :catalogHierarchy.studyArea.Id];
        CORE_Business_Unit__c businessUnit = [Select Id From CORE_Business_Unit__c where id = :catalogHierarchy.businessUnit.Id];
        CORE_Division__c division = [Select Id, CORE_Division_ExternalId__c From CORE_Division__c where id = :catalogHierarchy.division.Id];
        catalogHierarchy.division = null;
        catalogHierarchy.studyArea = null;

        catalogHierarchy.businessUnit = null;
        catalogHierarchy.level = null;
        catalogHierarchy.isFullHierarchy = false;

        Map<Integer, CORE_CatalogHierarchyModel> catalogHierarchiesToUpdate = new Map<Integer, CORE_CatalogHierarchyModel>();
        catalogHierarchiesToUpdate.put(0,catalogHierarchy);
        Test.startTest();
        Map<Integer,CORE_CatalogHierarchyModel> results = CORE_OpportunityDataMaker.getCatalogFromCurriculums(catalogHierarchiesToUpdate);
        Test.stopTest();

        System.assertNotEquals(null, results);
        System.assertNotEquals(0, results.size());
        CORE_CatalogHierarchyModel result = results.get(0);

        System.assertNotEquals(null, result.division);
        System.assertEquals(division.CORE_Division_ExternalId__c, result.division.CORE_Division_ExternalId__c);
        System.assertNotEquals(null, result.businessUnit);
        System.assertEquals(businessUnit.Id, result.businessUnit.Id);
        System.assertNotEquals(null, result.studyArea);
        System.assertEquals(studyArea.Id, result.studyArea.Id);
        System.assertNotEquals(null, result.curriculum);
        System.assertEquals(curriculum.Id, result.curriculum.Id);
        System.assertEquals(false, result.isFullHierarchy);
    }

    @IsTest
    public static void getCatalogFromIntakes_Should_return_a_map_of_catalogHierarchy_initialized_using_intake_externalIds() {
        CORE_LeadFormAcquisitionWrapper.GlobalWrapper globalWrapper = CORE_DataFaker_LeadFormAcquWrapper.getGlobalWrapper();
        CORE_CatalogHierarchyModel catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test2').catalogHierarchy;
        CORE_Intake__c intake = [Select Id, CORE_Academic_Year__c From CORE_Intake__c where id = :catalogHierarchy.intake.Id];
        CORE_Level__c level = [Select Id From CORE_Level__c where id = :catalogHierarchy.level.Id];
        CORE_Curriculum__c curriculum = [Select Id From CORE_Curriculum__c where id = :catalogHierarchy.curriculum.Id];
        CORE_Study_Area__c studyArea = [Select Id From CORE_Study_Area__c where id = :catalogHierarchy.studyArea.Id];
        CORE_Business_Unit__c businessUnit = [Select Id From CORE_Business_Unit__c where id = :catalogHierarchy.businessUnit.Id];
        CORE_Division__c division = [Select Id, CORE_Division_ExternalId__c From CORE_Division__c where id = :catalogHierarchy.division.Id];
        catalogHierarchy.division.Id = null;
        catalogHierarchy.businessUnit.Id = null;
        catalogHierarchy.studyArea.Id = null;
        catalogHierarchy.curriculum.Id = null;
        catalogHierarchy.level.Id = null;
        catalogHierarchy.isFullHierarchy = false;

        Map<Integer, CORE_CatalogHierarchyModel> catalogHierarchiesToUpdate = new Map<Integer, CORE_CatalogHierarchyModel>();
        catalogHierarchiesToUpdate.put(0,catalogHierarchy);
        Test.startTest();
        Map<Integer,CORE_CatalogHierarchyModel> results = CORE_OpportunityDataMaker.getCatalogFromIntakes(catalogHierarchiesToUpdate);
        Test.stopTest();

        System.assertNotEquals(null, results);
        System.assertNotEquals(0, results.size());
        CORE_CatalogHierarchyModel result = results.get(0);

        System.assertNotEquals(null, result.division);
        System.assertEquals(division.CORE_Division_ExternalId__c, result.division.CORE_Division_ExternalId__c);
        System.assertNotEquals(null, result.businessUnit);
        System.assertEquals(businessUnit.Id, result.businessUnit.Id);
        System.assertNotEquals(null, result.studyArea);
        System.assertEquals(studyArea.Id, result.studyArea.Id);
        System.assertNotEquals(null, result.curriculum);
        System.assertEquals(curriculum.Id, result.curriculum.Id);
        System.assertNotEquals(null, result.level);
        System.assertEquals(level.Id, result.level.Id);
        System.assertNotEquals(null, result.intake);
        System.assertEquals(intake.Id, result.intake.Id);
        System.assertEquals(true, result.isFullHierarchy);
    }

    @IsTest
    public static void initOpportunityDefaultValues_Should_return_an_opportunity_initialized_using_default_values() {
        CORE_OpportunityDataMaker dataMaker = new CORE_OpportunityDataMaker();
        CORE_LeadFormAcquisitionWrapper.GlobalWrapper globalWrapper = CORE_DataFaker_LeadFormAcquWrapper.getGlobalWrapper();
        Account account = [Select Id from account];
        CORE_CatalogHierarchyModel catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test2').catalogHierarchy;

        Test.startTest();
        Opportunity result = dataMaker.initOpportunityDefaultValues(account, String.valueOf(opportunity.CORE_OPP_Academic_Year__c), catalogHierarchy, false, false);
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(dataMaker.metadata.oppRecordTypeId, result.RecordTypeId);
        System.assertEquals(account.Id, result.AccountId);
        System.assert(String.isNotBlank(result.StageName));
        System.assert(String.isNotBlank(result.CORE_OPP_Sub_Status__c));
        System.assertEquals(dataMaker.metadata.closeDate, result.CloseDate);
        System.assertEquals(true, result.CORE_OPP_Main_Opportunity__c);
        System.assertEquals(String.valueOf(opportunity.CORE_OPP_Academic_Year__c), result.CORE_OPP_Academic_Year__c);
    }

    @IsTest
    public static void initOpportunity_Should_return_an_opportunity_initialized_using_cataloghierachy_values() {
        CORE_OpportunityDataMaker dataMaker = new CORE_OpportunityDataMaker();
        CORE_LeadFormAcquisitionWrapper.GlobalWrapper globalWrapper = CORE_DataFaker_LeadFormAcquWrapper.getGlobalWrapper();
        Account account = [Select Id from account];
        CORE_CatalogHierarchyModel catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test2').catalogHierarchy;

        Test.startTest();
        Opportunity result = dataMaker.initOpportunity(account, String.valueOf(opportunity.CORE_OPP_Academic_Year__c), catalogHierarchy, false);
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(catalogHierarchy.division.Id, result.CORE_OPP_Division__c);
        System.assertEquals(catalogHierarchy.businessUnit.Id, result.CORE_OPP_Business_Unit__c);
        System.assertEquals(catalogHierarchy.studyArea.Id, result.CORE_OPP_Study_Area__c );
        System.assertEquals(catalogHierarchy.curriculum.Id, result.CORE_OPP_Curriculum__c);
        System.assertEquals(catalogHierarchy.level.Id, result.CORE_OPP_Level__c);
        System.assertEquals(catalogHierarchy.intake.Id, result.CORE_OPP_Intake__c);
        System.assertEquals(catalogHierarchy.product.Id, result.CORE_Main_Product_Interest__c);
        System.assertEquals(catalogHierarchy.priceBook2Id, result.Pricebook2Id);
    }

    @IsTest
    public static void initAPIOpportunity_Should_return_an_opportunity_initialized_with_catalogHierachy_values_when_there_is_no_mainOpportunity() {
        CORE_OpportunityDataMaker dataMaker = new CORE_OpportunityDataMaker();
        CORE_LeadFormAcquisitionWrapper.GlobalWrapper globalWrapper = CORE_DataFaker_LeadFormAcquWrapper.getGlobalWrapper();
        Account account = [Select Id from account];
        Opportunity mainOpportunity;
        CORE_CatalogHierarchyModel catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test2').catalogHierarchy;
        catalogHierarchy.interest = globalWrapper.mainInterest;
        Test.startTest();
        Opportunity result = dataMaker.initAPIOpportunity(account, String.valueOf(opportunity.CORE_OPP_Academic_Year__c), catalogHierarchy, mainOpportunity);
        
        System.assertNotEquals(null, result);
        System.assertEquals(catalogHierarchy.businessUnit.CORE_BU_Technical_User__c, result.OwnerId);
        System.assertEquals(catalogHierarchy.division.Id, result.CORE_OPP_Division__c);
        System.assertEquals(catalogHierarchy.businessUnit.Id, result.CORE_OPP_Business_Unit__c);
        System.assertEquals(catalogHierarchy.studyArea.Id, result.CORE_OPP_Study_Area__c );
        System.assertEquals(catalogHierarchy.curriculum.Id, result.CORE_OPP_Curriculum__c);
        System.assertEquals(catalogHierarchy.level.Id, result.CORE_OPP_Level__c);
        System.assertEquals(catalogHierarchy.intake.Id, result.CORE_OPP_Intake__c);
        System.assertEquals(catalogHierarchy.product.Id, result.CORE_Main_Product_Interest__c);

        catalogHierarchy.division.Id = null;
        catalogHierarchy.businessUnit.Id = null;
        catalogHierarchy.studyArea.Id = null;
        catalogHierarchy.curriculum.Id = null;
        catalogHierarchy.level.Id = null;
        catalogHierarchy.intake.Id = null;

        result = dataMaker.initAPIOpportunity(account, String.valueOf(opportunity.CORE_OPP_Academic_Year__c), catalogHierarchy, mainOpportunity);
        
        System.assertNotEquals(null, result);
        System.assertEquals(catalogHierarchy.businessUnit.CORE_BU_Technical_User__c, result.OwnerId);
        System.assertEquals(catalogHierarchy.division.CORE_Division_ExternalId__c, result.CORE_OPP_Division__r.CORE_Division_ExternalId__c);
        System.assertEquals(catalogHierarchy.businessUnit.CORE_Business_Unit_ExternalId__c, result.CORE_OPP_Business_Unit__r.CORE_Business_Unit_ExternalId__c);
        System.assertEquals(catalogHierarchy.studyArea.CORE_Study_Area_ExternalId__c, result.CORE_OPP_Study_Area__r.CORE_Study_Area_ExternalId__c );
        System.assertEquals(catalogHierarchy.curriculum.CORE_Curriculum_ExternalId__c, result.CORE_OPP_Curriculum__r.CORE_Curriculum_ExternalId__c);
        System.assertEquals(catalogHierarchy.level.CORE_Level_ExternalId__c, result.CORE_OPP_Level__r.CORE_Level_ExternalId__c);
        System.assertEquals(catalogHierarchy.intake.CORE_Intake_ExternalId__c, result.CORE_OPP_Intake__r.CORE_Intake_ExternalId__c);
        System.assertEquals(catalogHierarchy.product.Id, result.CORE_Main_Product_Interest__c);
        Test.stopTest();

    }

    @IsTest
    public static void initAPIOpportunity_Should_return_an_updated_opportunity_when_main_opportunity_is_provided() {
        CORE_OpportunityDataMaker dataMaker = new CORE_OpportunityDataMaker();
        CORE_LeadFormAcquisitionWrapper.GlobalWrapper globalWrapper = CORE_DataFaker_LeadFormAcquWrapper.getGlobalWrapper();
        Account account = [Select Id, OwnerId from account];
        CORE_DataFaker_CatalogHierarchy catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test2');
        Opportunity opportunity = [Select Id, OwnerId, CORE_OPP_Academic_Year__c from Opportunity];

        Test.startTest();
        Opportunity result = dataMaker.initAPIOpportunity(account, String.valueOf(opportunity.CORE_OPP_Academic_Year__c), catalogHierarchy.catalogHierarchy, opportunity);
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(opportunity.OwnerId, result.OwnerId);
        System.assertEquals(true, result.CORE_ReturningLead__c);
        System.assertNotEquals(null, result.CORE_ReturningLeadDate__c);
    }

    @IsTest
    public static void initConsentsFromAPI_Should_return_a_list_of_initialized_pointOfContacts() {
        CORE_OpportunityDataMaker dataMaker = new CORE_OpportunityDataMaker();
        CORE_LeadFormAcquisitionWrapper.GlobalWrapper globalWrapper = CORE_DataFaker_LeadFormAcquWrapper.getGlobalWrapper();
        Account account = [Select Id, OwnerId from account];

        CORE_DataFaker_CatalogHierarchy catalogHierarchy = new CORE_DataFaker_CatalogHierarchy('test2');
        for(CORE_LeadFormAcquisitionWrapper.ConsentWrapper consent :  globalWrapper.consents){
            consent.businessUnit = catalogHierarchy.businessUnit.Id;
            consent.division = catalogHierarchy.division.Id;
        }
        
        Test.startTest();
        List<CORE_Consent__c> results = dataMaker.initConsentsFromAPI(account, globalWrapper.consents, true);
        Test.stopTest();

        System.assertNotEquals(null, results);
        System.assertNotEquals(0, results.size());

        CORE_Consent__c consent = results.get(0);
        CORE_LeadFormAcquisitionWrapper.ConsentWrapper consentWrapper = globalWrapper.consents.get(0);

        System.assertEquals(account.Id, consent.CORE_Person_Account__c);
        System.assertEquals(consentWrapper.consentType, consent.CORE_Consent_type__c);
        System.assertEquals(consentWrapper.channel, consent.CORE_Consent_channel__c);
        System.assertEquals(consentWrapper.description, consent.CORE_Consent_description__c);
        System.assertEquals(consentWrapper.optIn, consent.CORE_Opt_in__c);
        System.assertEquals(consentWrapper.optOut, consent.CORE_Opt_out__c);
        System.assertEquals(consentWrapper.division, consent.CORE_Division__c);
        System.assertEquals(consentWrapper.businessUnit, consent.CORE_Business_Unit__c);
    }

    @IsTest
    public static void initPocDefaultValues_Should_return_an_initialized_pointOfContact() {
        CORE_OpportunityDataMaker dataMaker = new CORE_OpportunityDataMaker();
        CORE_LeadFormAcquisitionWrapper.GlobalWrapper globalWrapper = CORE_DataFaker_LeadFormAcquWrapper.getGlobalWrapper();
        Opportunity opportunity = [Select Id, AccountId, CORE_Main_Product_Interest__c From Opportunity];

        Test.startTest();
        String captureChannel = 'captureChannel';
        String nature = 'nature';
        String origin = 'origin';
        CORE_Point_of_Contact__c result = dataMaker.initPocDefaultValues(opportunity,nature,captureChannel,origin);
        Test.stopTest();

        System.assertNotEquals(null, result);
        System.assertEquals(opportunity.Id, result.CORE_Opportunity__c);
        System.assertEquals(opportunity.AccountId, result.CORE_Account__c);
        System.assertEquals(dataMaker.metadata.pocInsertionMode, result.CORE_Insertion_Mode__c);
        System.assertEquals(nature, result.CORE_Nature__c);
        System.assertEquals(captureChannel, result.CORE_Capture_Channel__c);
        System.assertEquals(origin, result.CORE_Origin__c);
    }

    @IsTest
    public static void initPocFromAPI_Should_return_a_list_of_initialized_pointOfContacts() {
        CORE_OpportunityDataMaker dataMaker = new CORE_OpportunityDataMaker();
        CORE_LeadFormAcquisitionWrapper.GlobalWrapper globalWrapper = CORE_DataFaker_LeadFormAcquWrapper.getGlobalWrapper();
        Opportunity opportunity = [Select Id, AccountId, OwnerId, CORE_Main_Product_Interest__c From Opportunity];

        Test.startTest();
        List<CORE_Point_of_Contact__c> results = dataMaker.initPocFromAPI(new List<Opportunity>{opportunity},globalWrapper.pointOfContact);
        Test.stopTest();

        System.assertNotEquals(null, results);
        System.assertNotEquals(0, results.size());

        CORE_Point_of_Contact__c result = results.get(0);
        System.assertEquals(opportunity.Id, result.CORE_Opportunity__c);
        System.assertEquals(opportunity.AccountId, result.CORE_Account__c);
        System.assertEquals('CORE_PKL_Automatic', result.CORE_Insertion_Mode__c);
        System.assertEquals(globalWrapper.pointOfContact.sourceId ,result.CORE_Source__c);
        System.assertEquals(globalWrapper.pointOfContact.nature, result.CORE_Nature__c);
        System.assertEquals(globalWrapper.pointOfContact.captureChannel, result.CORE_Capture_Channel__c);
        System.assertEquals(globalWrapper.pointOfContact.origin, result.CORE_Origin__c);
        System.assertEquals(globalWrapper.pointOfContact.campaignMedium, result.Campaign_Medium__c);
        System.assertEquals(globalWrapper.pointOfContact.campaignSource, result.CORE_Campaign_Source__c);
        System.assertEquals(globalWrapper.pointOfContact.campaignTerm, result.CORE_Campaign_Term__c);
        System.assertEquals(globalWrapper.pointOfContact.campaignContent, result.CORE_Campaign_Content__c);
        System.assertEquals(globalWrapper.pointOfContact.campaignName, result.CORE_Campaign_Name__c);
        System.assertEquals(globalWrapper.pointOfContact.device, result.CORE_Device__c);
        System.assertEquals(globalWrapper.pointOfContact.firstVisitDate, result.CORE_First_visit_datetime__c);
        System.assertEquals(globalWrapper.pointOfContact.firstPage, result.CORE_First_page__c);
        System.assertEquals(globalWrapper.pointOfContact.ipAddress, result.CORE_IP_address__c);
        System.assertEquals(globalWrapper.pointOfContact.latitute, result.CORE_Latitude__c);
        System.assertEquals(globalWrapper.pointOfContact.longitude, result.CORE_Longitude__c);
    }

    @IsTest
    public static void initOppLineItem_Should_return_an_initialized_OpportunityLineItem() {
        CORE_OpportunityDataMaker dataMaker = new CORE_OpportunityDataMaker();
        CORE_LeadFormAcquisitionWrapper.GlobalWrapper globalWrapper = CORE_DataFaker_LeadFormAcquWrapper.getGlobalWrapper();
        Opportunity opportunity = [Select Id, CORE_Main_Product_Interest__c From Opportunity];
        Product2 product = [Select Id from Product2];
        Id pricebookId = CORE_DataFaker_PriceBook.getStandardPricebookId();
        PricebookEntry pricebookEntry = CORE_DataFaker_PriceBook.getPriceBookEntry(product.Id, pricebookId, 10.0);

        Test.startTest();
        OpportunityLineItem result = dataMaker.initOppLineItem(opportunity,pricebookEntry);
        System.assertNotEquals(null, result);
        System.assertEquals(opportunity.Id, result.OpportunityId);
        System.assertEquals(opportunity.CORE_Main_Product_Interest__c, result.Product2Id);
        System.assertEquals(1, result.Quantity);
        System.assertEquals(pricebookEntry.UnitPrice, result.UnitPrice);
        System.assertEquals(pricebookEntry.Id, result.PricebookEntryId);

        Test.stopTest();
    }

    @IsTest
    public static void initCampaignMemberFromAPI_Should_return_an_initialized_campaignMember() {
        CORE_OpportunityDataMaker dataMaker = new CORE_OpportunityDataMaker();
        CORE_LeadFormAcquisitionWrapper.GlobalWrapper globalWrapper = CORE_DataFaker_LeadFormAcquWrapper.getGlobalWrapper();
        Account account = [Select Id, PersonContactId From Account];
        Opportunity opportunity = [Select Id From Opportunity];

        Test.startTest();
        CampaignMember result = dataMaker.initCampaignMemberFromAPI(account,opportunity,null);
        System.assertEquals(null, result);

        result = dataMaker.initCampaignMemberFromAPI(account, opportunity, globalWrapper.campaignSubscription);
        System.assertNotEquals(null, result);
        System.assertNotEquals(null, result.campaign);
        System.assertEquals(globalWrapper.campaignSubscription.campaignId, result.campaign.CORE_External_Id__c);
        System.assertEquals(account.PersonContactId, result.ContactId);

        Test.stopTest();
    }

    /*
    @IsTest
    public static void getProductPricebookEntry_should_return_pricebookEntry_when_exist() {
        CORE_OpportunityDataMaker dataMaker = new CORE_OpportunityDataMaker();
        CORE_Business_Unit__c businessUnit = [Select Id From CORE_Business_Unit__c];
        Product2 product = [Select Id from Product2];

        Test.startTest();
        
        PricebookEntry result = dataMaker.getProductPricebookEntry(businessUnit.Id, product.Id);
        System.assertEquals(null, result);
        
        Id pricebookId = CORE_DataFaker_PriceBook.getStandardPricebookId();
        CORE_PriceBook_BU__c pricebookBu =  CORE_DataFaker_PriceBook.getPriceBookBu(pricebookId, businessUnit.Id, 'test');
        PricebookEntry PricebookEntry = CORE_DataFaker_PriceBook.getPriceBookEntry(product.Id, pricebookId, 10.0);
        
        result = dataMaker.getProductPricebookEntry(businessUnit.Id, product.Id);
        System.assertNotEquals(null, result);
        System.assertEquals(pricebookEntry.Id, result.Id);

        Test.stopTest();
    }
*/
    @IsTest
    public static void getProductPricebookEntryByPb_should_return_pricebookEntry_when_exist() {
        CORE_OpportunityDataMaker dataMaker = new CORE_OpportunityDataMaker();
        Product2 product = [Select Id from Product2];

        Test.startTest();
        Id pricebookId = CORE_DataFaker_PriceBook.getStandardPricebookId();

        PricebookEntry result = dataMaker.getProductPricebookEntryByPb(pricebookId, product.Id);
        System.assertEquals(null, result);
        
        PricebookEntry pricebookEntry = CORE_DataFaker_PriceBook.getPriceBookEntry(product.Id, pricebookId, 10.0);
        
        result = dataMaker.getProductPricebookEntryByPb(pricebookId, product.Id);
        System.assertNotEquals(null, result);
        System.assertEquals(pricebookEntry.Id, result.Id);

        Test.stopTest();
    }

    @IsTest
    public static void getPricebookEntryByProducts_Should_return_map_of_pricebookEntry_by_products_if_exist() {
        CORE_OpportunityDataMaker dataMaker = new CORE_OpportunityDataMaker();
        CORE_Business_Unit__c businessUnit = [Select Id,CORE_Business_Unit_ExternalId__c from CORE_Business_Unit__c];
        Product2 product = [Select Id from Product2];

        Test.startTest();
        Map<Id,Id> businessUnitByProducts = new Map<Id,Id>();
        businessUnitByProducts.put(product.Id, businessUnit.Id);

        Map<Id,PricebookEntry> result = dataMaker.getPricebookEntryByProducts(businessUnitByProducts);
        System.assertNotEquals(null, result);
        System.assertEquals(0, result.size());
        
        Id pricebookId = CORE_DataFaker_PriceBook.getStandardPricebookId();
        CORE_PriceBook_BU__c pricebookBu =  CORE_DataFaker_PriceBook.getPriceBookBu(pricebookId, businessUnit.Id, 'test');
        PricebookEntry PricebookEntry = CORE_DataFaker_PriceBook.getPriceBookEntry(product.Id, pricebookId, 10.0);

        result = dataMaker.getPricebookEntryByProducts(businessUnitByProducts);
        System.assertNotEquals(null, result);
        System.assertEquals(1, result.size());
        System.assert(result.keySet().contains(product.Id));
        System.assertNotEquals(null, result.get(product.Id));
        System.assertEquals(PricebookEntry.Id, result.get(product.Id).Id);

        Test.stopTest();
    }

    @IsTest
    public static void getDefaultPriceBookIdFromBusinessUnit_Should_return_Map_of_pricebookBuId_from_businessUnit_ids() {
        CORE_OpportunityDataMaker dataMaker = new CORE_OpportunityDataMaker();
        CORE_Business_Unit__c businessUnit = [Select Id,CORE_Business_Unit_ExternalId__c from CORE_Business_Unit__c];

        Test.startTest();
        Set<Id> buIds = new Set<Id>{businessUnit.Id};
        Map<Id,Id> result = dataMaker.getDefaultPriceBookIdFromBusinessUnit(buIds);
        System.assertNotEquals(null, result);
        System.assertEquals(0, result.size());
        
        Id pricebookId = CORE_DataFaker_PriceBook.getStandardPricebookId();
        CORE_PriceBook_BU__c pricebookBu =  CORE_DataFaker_PriceBook.getPriceBookBu(pricebookId, businessUnit.Id, 'test');

        result = dataMaker.getDefaultPriceBookIdFromBusinessUnit(buIds);
        System.assertNotEquals(null, result);
        System.assertEquals(1, result.size());
        System.assert(result.keySet().contains(businessUnit.Id));
        System.assertEquals(pricebookId, result.get(businessUnit.Id));

        Test.stopTest();
    }

    /*
    @IsTest
    public static void getDefaultPriceBookIdFromBusinessUnit_Should_return_pricebookBuId_from_businessUnit_id() {
        CORE_OpportunityDataMaker dataMaker = new CORE_OpportunityDataMaker();
        CORE_Business_Unit__c businessUnit = [Select Id,CORE_Business_Unit_ExternalId__c from CORE_Business_Unit__c];

        Test.startTest();
        Id result = dataMaker.getDefaultPriceBookIdFromBusinessUnit(businessUnit.Id);
        System.assertEquals(null, result, 'Should be null because there is no pricebook_bu configured for this bu');

        Id pricebookId = CORE_DataFaker_PriceBook.getStandardPricebookId();
        CORE_PriceBook_BU__c pricebookBu =  CORE_DataFaker_PriceBook.getPriceBookBu(pricebookId, businessUnit.Id, 'test');

        result = dataMaker.getDefaultPriceBookIdFromBusinessUnit(businessUnit.Id);
        System.assertEquals(pricebookId, result, 'Should not be null because there is pricebook_bu configured for this bu');
        Test.stopTest();
    }*/

}