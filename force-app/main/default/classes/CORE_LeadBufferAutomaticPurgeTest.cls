@IsTest
public class CORE_LeadBufferAutomaticPurgeTest {
    
    @TestSetup
    public static void setup(){
        CORE_CountrySpecificSettings__c countrySetting = new CORE_CountrySpecificSettings__c(
                CORE_Retention_Delay__c= 3,
            	CORE_System_admins_email_adresses__c = 'test@email.com;'
        );
        database.insert(countrySetting, true);
    	List<CORE_CatalogHierarchyModel> catalogs = new List<CORE_CatalogHierarchyModel>();
        List<CORE_DataFaker_CatalogHierarchy> fakeCatalogs =  CORE_DataFaker_CatalogHierarchy.createMultipleCatalogs('uniqueKey', 50);
        for(CORE_DataFaker_CatalogHierarchy catalog : fakeCatalogs){
        	catalogs.add(catalog.catalogHierarchy);
        }
		List<CORE_LeadAcquisitionBuffer__c> buffers = new List<CORE_LeadAcquisitionBuffer__c>();
        for(Integer i = 0 ; i <100 ; i++){
            String uniqueKey = 'uniqueKey' + i;
            String num = String.valueOf(i).leftPad(3, '0');
            String mobile = '+33600000' + num;
            String phone = '+33200000' + num;
            String email = 'testEmail' + i + '@test.com';
            Integer randomNumber = Integer.valueof((Math.random() * catalogs.size()));
            CORE_LeadAcquisitionBuffer__c buffer = CORE_DataFaker_LeadAcquisitionBuffer.getBufferWhithoutInsert(uniqueKey, mobile, phone, email, catalogs.get(randomNumber));
            buffer.CORE_Status__c ='OK';
            buffers.add(buffer);
        }

        insert buffers;
        List<CORE_LeadAcquisitionBuffer__c> newbuffers =[Select Id, CreatedDate FROM CORE_LeadAcquisitionBuffer__c];
        for(Integer i=0;i<3;i++){
            Test.setCreatedDate(newbuffers[i].Id, DateTime.Now().AddDays(-4));
       	}
    
    }
    @IsTest
    public static void bufferautomaticpurgesuccess() {
        List<CORE_LeadAcquisitionBuffer__c> buffers =[Select Id FROM CORE_LeadAcquisitionBuffer__c];
        System.debug('size folder ' + buffers.size());
        test.starttest();
        CORE_LeadBufferAutomaticPurge leadBufferAutomaticPurge = new CORE_LeadBufferAutomaticPurge();
		database.executebatch(leadBufferAutomaticPurge,10);//FTandoi, 07/09/2022, reduced to 10
        test.stopTest();
        List<CORE_LeadAcquisitionBuffer__c> newbuffers =[Select Id FROM CORE_LeadAcquisitionBuffer__c];
        System.debug('size new' + newbuffers.size());
        System.assertEquals(buffers.size()-3, newbuffers.size());
        
        
    }
}