<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Default Value</label>
    <protected>false</protected>
    <values>
        <field>country__c</field>
        <value xsi:type="xsd:string">country=MEX</value>
    </values>
    <values>
        <field>headerBusinessUnit__c</field>
        <value xsi:type="xsd:string">Id,Uuid,Code,Label,Parent_uuid,CreatedOn,ModifiedOn</value>
    </values>
    <values>
        <field>headerCurriculum__c</field>
        <value xsi:type="xsd:string">Id,Uuid,Code,Label,Parent_uuid,CreatedOn,ModifiedOn</value>
    </values>
    <values>
        <field>headerDeleted__c</field>
        <value xsi:type="xsd:string">Type,Object_id,Deletion_date</value>
    </values>
    <values>
        <field>headerDivision__c</field>
        <value xsi:type="xsd:string">Id,Uuid,Code,Label,CreatedOn,ModifiedOn</value>
    </values>
    <values>
        <field>headerIntake__c</field>
        <value xsi:type="xsd:string">Id,Uuid,Code,Label,Parent_uuid,Session,Academic_year,CreatedOn,ModifiedOn</value>
    </values>
    <values>
        <field>headerLevel__c</field>
        <value xsi:type="xsd:string">Id,Uuid,Code,Label,Parent_uuid,Study_cycle,Year,CreatedOn,ModifiedOn</value>
    </values>
    <values>
        <field>headerOpportunity__c</field>
        <value xsi:type="xsd:string">Opportunity_id,Opportunity_legacy_id,Main,Processus_code,Academic_year,Study_mode,CreatedOn,ModifiedOn,application_online,Contact_id,Business_unit_uuid,Study_Area_uuid,Curriculum_uuid,Level_uuid,Intake_uuid,Reached_lead,qualification</value>
    </values>
    <values>
        <field>headerPersonAccount__c</field>
        <value xsi:type="xsd:string">Person_account_id,Person_account_legacy_id,Domain,Country,Nationality,Firstname,Gender,CreatedOn,ModifiedOn</value>
    </values>
    <values>
        <field>headerPicklist__c</field>
        <value xsi:type="xsd:string">List,Language,Code,Label</value>
    </values>
    <values>
        <field>headerPointOfContact2__c</field>
        <value xsi:type="xsd:string">Campaign_medium,Campaign_source,Campaign_term,Campaign_content,Campaign_name,Device,First_visit,First_page,IP,Latitude,Longitude,CreatedOn,ModifiedOn,proactivePrompt,proactiveEngaged,chatWith</value>
    </values>
    <values>
        <field>headerPointOfContact__c</field>
        <value xsi:type="xsd:string">Point_contact_id,Opportunity_id,Source_uuid,Insertion_mode,Nature,Enrollment_Channel,Capture_Channel,Origin,</value>
    </values>
    <values>
        <field>headerStatusJourney__c</field>
        <value xsi:type="xsd:string">Opportunity_ID,Status,Datetime,CreatedOn,ModifiedOn</value>
    </values>
    <values>
        <field>headerStudyArea__c</field>
        <value xsi:type="xsd:string">Id,Uuid,Code,Label,Parent_uuid,CreatedOn,ModifiedOn</value>
    </values>
    <values>
        <field>headerSubStatusJourney__c</field>
        <value xsi:type="xsd:string">Opportunity_ID,Status,Sub_Status,Datetime,CreatedOn,ModifiedOn</value>
    </values>
    <values>
        <field>key__c</field>
        <value xsi:type="xsd:string">gR7LgKrxZ35IqSV0cofSahgS1Hp1N85yFMiAWJasCKNdkDSJn/6t0g==</value>
    </values>
    <values>
        <field>source__c</field>
        <value xsi:type="xsd:string">source=SAF</value>
    </values>
    <values>
        <field>token__c</field>
        <value xsi:type="xsd:string">2588eb05f5166665c26f021b1f69cadb14a40950f7d97efde453d2104cd1461e</value>
    </values>
    <values>
        <field>url__c</field>
        <value xsi:type="xsd:string">https://azfun-prjapi-ww-uat.azurewebsites.net/api/file-upload?</value>
    </values>
</CustomMetadata>
