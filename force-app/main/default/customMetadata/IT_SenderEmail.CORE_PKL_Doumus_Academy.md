<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>DOMUS</label>
    <protected>false</protected>
    <values>
        <field>IT_EmailAddress__c</field>
        <value xsi:type="xsd:string">info@domusacademy.it</value>
    </values>
</CustomMetadata>
