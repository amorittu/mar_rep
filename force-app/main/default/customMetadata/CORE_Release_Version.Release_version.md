<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Release version</label>
    <protected>false</protected>
    <values>
        <field>Core_Version__c</field>
        <value xsi:type="xsd:string">1.5</value>
    </values>
    <values>
        <field>Country_version__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>Twilio_Version__c</field>
        <value xsi:type="xsd:string">1.6</value>
    </values>
</CustomMetadata>
