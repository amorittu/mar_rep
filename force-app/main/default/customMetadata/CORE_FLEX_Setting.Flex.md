<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Flex</label>
    <protected>false</protected>
    <values>
        <field>TokenFlex__c</field>
        <value xsi:type="xsd:string">4396680c9a999c820775b48172bc08a5</value>
    </values>
    <values>
        <field>UrlFlex__c</field>
        <value xsi:type="xsd:string">https://gge-service-2631-uat.twil.io</value>
    </values>
</CustomMetadata>
