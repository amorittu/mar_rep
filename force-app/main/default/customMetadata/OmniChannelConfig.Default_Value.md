<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Default Value</label>
    <protected>false</protected>
    <values>
        <field>Missed_call_default_queue__c</field>
        <value xsi:type="xsd:string">test_VPI_2</value>
    </values>
    <values>
        <field>hideCall__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>hideSMS__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>hideVideo__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>hideWhatsapp__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
</CustomMetadata>
