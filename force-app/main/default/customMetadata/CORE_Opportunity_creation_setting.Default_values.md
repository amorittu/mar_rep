<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Default values</label>
    <protected>false</protected>
    <values>
        <field>CORE_AllowManualProduct_search_selection__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>CORE_Consents_for_marketing_activity__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>CORE_Default_catalog_selection__c</field>
        <value xsi:type="xsd:string">CORE_Full_hierarchy</value>
    </values>
    <values>
        <field>CORE_OPP_Close_date_nb_days__c</field>
        <value xsi:type="xsd:double">60.0</value>
    </values>
    <values>
        <field>CORE_OPP_Close_date_nb_year__c</field>
        <value xsi:type="xsd:double">0.0</value>
    </values>
    <values>
        <field>CORE_OPP_Full_mode_Status__c</field>
        <value xsi:type="xsd:string">Lead</value>
    </values>
    <values>
        <field>CORE_OPP_Full_mode_SubStatus__c</field>
        <value xsi:type="xsd:string">Lead - New</value>
    </values>
    <values>
        <field>CORE_OPP_Partial_mode_Status__c</field>
        <value xsi:type="xsd:string">Prospect</value>
    </values>
    <values>
        <field>CORE_OPP_Partial_mode_SubStatus__c</field>
        <value xsi:type="xsd:string">Prospect - New</value>
    </values>
    <values>
        <field>CORE_OPP_RecordType__c</field>
        <value xsi:type="xsd:string">CORE_Enrollment</value>
    </values>
    <values>
        <field>CORE_OPP_Suspect_mode_Status__c</field>
        <value xsi:type="xsd:string">Suspect</value>
    </values>
    <values>
        <field>CORE_OPP_Suspect_mode_SubStatus__c</field>
        <value xsi:type="xsd:string">Suspect - New</value>
    </values>
    <values>
        <field>CORE_OPP_displayRetailAgency__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>CORE_POC_Insertion_Mode__c</field>
        <value xsi:type="xsd:string">CORE_PKL_Manual</value>
    </values>
</CustomMetadata>
