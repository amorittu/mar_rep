<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Chinese</label>
    <protected>false</protected>
    <values>
        <field>Language_ISO2__c</field>
        <value xsi:type="xsd:string">ZH</value>
    </values>
    <values>
        <field>language_ISO3__c</field>
        <value xsi:type="xsd:string">ZHO</value>
    </values>
</CustomMetadata>
