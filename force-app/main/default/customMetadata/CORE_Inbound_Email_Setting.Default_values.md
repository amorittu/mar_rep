<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Default values</label>
    <protected>false</protected>
    <values>
        <field>CORE_AssignToOppOwner__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>CORE_Create_OPP_from_Inbound_email__c</field>
        <value xsi:type="xsd:boolean">false</value>
    </values>
    <values>
        <field>CORE_Custom_OPP_User_Field__c</field>
        <value xsi:nil="true"/>
    </values>
    <values>
        <field>CORE_Internal_domains__c</field>
        <value xsi:type="xsd:string">pfh.de</value>
    </values>
</CustomMetadata>
