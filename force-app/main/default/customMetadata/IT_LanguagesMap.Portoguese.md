<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Portoguese</label>
    <protected>false</protected>
    <values>
        <field>Language_ISO2__c</field>
        <value xsi:type="xsd:string">PT</value>
    </values>
    <values>
        <field>language_ISO3__c</field>
        <value xsi:type="xsd:string">POR</value>
    </values>
</CustomMetadata>
