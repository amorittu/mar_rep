<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Mass_Opportunity_AcademicYearConfirm</label>
    <protected>false</protected>
    <values>
        <field>CORE_MailBody__c</field>
        <value xsi:type="xsd:string">Total # of Opportunities processed: {0}

Successful: {1}
Errors: {2}</value>
    </values>
    <values>
        <field>CORE_MailSubject__c</field>
        <value xsi:type="xsd:string">Result: Mass Opportunity Academic Year Confirmation</value>
    </values>
    <values>
        <field>CORE_RecipientsEmailAddresses__c</field>
        <value xsi:type="xsd:string">zamkhoda@gmail.com;</value>
    </values>
    <values>
        <field>CORE_RecipientsIncludeRunningUser__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
    <values>
        <field>CORE_SendErrorsList__c</field>
        <value xsi:type="xsd:boolean">true</value>
    </values>
</CustomMetadata>
