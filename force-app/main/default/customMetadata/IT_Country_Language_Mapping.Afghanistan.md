<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Afghanistan</label>
    <protected>false</protected>
    <values>
        <field>IT_CountryISO__c</field>
        <value xsi:type="xsd:string">AF</value>
    </values>
    <values>
        <field>IT_LanguageSF__c</field>
        <value xsi:type="xsd:string">ENG</value>
    </values>
    <values>
        <field>IT_Language__c</field>
        <value xsi:type="xsd:string">En-US</value>
    </values>
    <values>
        <field>IT_PhonePrefix__c</field>
        <value xsi:type="xsd:string">+93</value>
    </values>
</CustomMetadata>
