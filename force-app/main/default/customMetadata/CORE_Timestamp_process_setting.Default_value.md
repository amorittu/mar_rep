<?xml version="1.0" encoding="UTF-8"?>
<CustomMetadata xmlns="http://soap.sforce.com/2006/04/metadata" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">
    <label>Default value</label>
    <protected>false</protected>
    <values>
        <field>CORE_Opportunity_status_order__c</field>
        <value xsi:type="xsd:string">{&quot;CORE_Enrollment&quot;:[&quot;Suspect&quot;,&quot;Prospect&quot;,&quot;Lead&quot;,&quot;Applicant&quot;,&quot;Admitted&quot;,&quot;Registered&quot;,&quot;Enrolled&quot;,&quot;Withdraw&quot;,&quot;Closed Won&quot;,&quot;Closed Lost&quot;],&quot;CORE_Re_registration&quot;:[&quot;Up&quot;,&quot;To Finalize&quot;,&quot;Re-registered&quot;,&quot;Year Out&quot;,&quot;Re-enrolled&quot;,&quot;OutGoing&quot;,&quot;Withdraw&quot;,&quot;Closed Won&quot;,&quot;Closed Lost&quot;]}</value>
    </values>
</CustomMetadata>
