import { LightningElement, api, wire } from 'lwc';

// Apex Methods
import fetchLookupData from '@salesforce/apex/IT_DynamicLookupController.fetchLookupData';
import fetchDefaultRecord from '@salesforce/apex/IT_DynamicLookupController.fetchDefaultRecord';

const DELAY = 300; // dealy apex callout timing in miliseconds  

export default class It_dynamicLookupLwc extends LightningElement {
    //public properties start
    
    //INPUT PARAMS
    // id passed as default 
    @api defaultLookupId = '';
    
    // default icon displayed in close to the input(https://www.lightningdesignsystem.com/icons/#site-main-content)
    @api iconName = 'standard:account';
    
    // api name of the lookup Object (the parent API Name)
    @api sObjectApiName = 'Account';
    
    // this attribute show a "*" close to the input
    @api isRequired = false;
    
    // this attribute make the input disabled                   
    @api isDisabled = false;    
    
    // label of the input                   
    @api inputLabel = '';    
    
    // placheholder of the input 
    @api placeholder = '';
    
    // if true perform the query in 'With Sharing' mode
    @api useSharing = false;

    //OUTPUT PARAMS

    //output attribute that store the id of the selected record
    @api selectedRecordId = '';

    lstResult = []; // to store list of returned records   
    hasRecords = true; 
    searchKey=''; // to store input field value    
    isSearchLoading = false; // to control loading spinner  
    delayTimeout;
    selectedRecord = {}; // to store selected lookup record in object formate 

    _initialized = false;

    connectedCallback(){
        try {
            console.log('defaultLookupId is : ' + this.defaultLookupId);
            if (!this._initialized) {
                this._initialized = true;
                if(this.defaultLookupId != '' && !this.selectedRecordId){
                    fetchDefaultRecord({ recordId: this.defaultLookupId , 'sObjectApiName' : this.sObjectApiName, 'useSharing' : this.useSharing })
                    .then((result) => {
                        if(result != null){
                            console.log('result fetchDefaultRecord is: ' + JSON.stringify(result) );
                            this.selectedRecord = result;
                            this.selectedRecordId = result.Id;
                            this.handleSelectRecordHelper(); // helper function to show/hide lookup result container on UI
                        }
                    })
                    .catch((error) => {
                        this.error = error;
                        this.selectedRecord = {};
                    });
                 }
            }
        } catch(e) {
            console.log(e.message);
        }
        
   }
    // wire function property to fetch search record based on user input
    @wire(
        fetchLookupData, { searchKey: '$searchKey' , sObjectApiName : '$sObjectApiName', 'useSharing' :  '$useSharing'  }
    )
    searchResult(value) {
        try {
            const { data, error } = value; // destructure the provisioned value
            this.isSearchLoading = false;
            if (data) {
                this.hasRecords = data.length == 0 ? false : true; 
                this.lstResult = JSON.parse(JSON.stringify(data)); 
            }
            else if (error) {
                console.log('(searchResult error---> ' + JSON.stringify(error));
            }
        } catch(e) {
            console.log('searchResult exception: ' + e.message);
        }
    };
        
    // update searchKey property on input field change  
    handleKeyChange(event) {
        try {
            // Debouncing this method: Do not update the reactive property as long as this function is
            // being called within a delay of DELAY. This is to avoid a very large number of Apex method calls.
            this.isSearchLoading = true;
            window.clearTimeout(this.delayTimeout);
            const searchKey = event.target.value;
            this.delayTimeout = setTimeout(() => {
                this.searchKey = searchKey;
            }, DELAY);

            console.log('this.searchKey is : ' + this.searchKey);
        } catch(e) {
            console.log(e.message);
        }
    }
    // method to toggle lookup result section on UI 
    toggleResult(event){
        const lookupInputContainer = this.template.querySelector('.lookupInputContainer');
        const clsList = lookupInputContainer.classList;
        const whichEvent = event.target.getAttribute('data-source');
        switch(whichEvent) {
            case 'searchInputField':
                clsList.add('slds-is-open');
                break;
            case 'lookupContainer':
                clsList.remove('slds-is-open');    
            break;                    
        }
    }
    // method to clear selected lookup record  
    handleRemove(){
        this.searchKey = '';    
        this.selectedRecord = {};
        this.lookupUpdatehandler(undefined); // update value on parent component as well from helper function 
        
        // remove selected pill and display input field again 
        const searchBoxWrapper = this.template.querySelector('.searchBoxWrapper');
            searchBoxWrapper.classList.remove('slds-hide');
            searchBoxWrapper.classList.add('slds-show');
            const pillDiv = this.template.querySelector('.pillDiv');
            pillDiv.classList.remove('slds-show');
            pillDiv.classList.add('slds-hide');
    }
    // method to update selected record from search result 
    handelSelectedRecord(event){   
        try {
            var objId = event.target.getAttribute('data-recid'); // get selected record Id 
            this.selectedRecord = this.lstResult.find(data => data.Id === objId); // find selected record from list 
            this.lookupUpdatehandler(this.selectedRecord); // update value on parent component as well from helper function 
            this.handleSelectRecordHelper(); // helper function to show/hide lookup result container on UI
        } catch(e) {
            console.log('handelSelectedRecord exception: ' + e.message);
        }
    }
    /*COMMON HELPER METHOD STARTED*/
    handleSelectRecordHelper(){
        try {
            // console.log('result fetchDefaultRecord is: ' + JSON.stringify(result) );
            this.template.querySelector('.lookupInputContainer').classList.remove('slds-is-open');
            const searchBoxWrapper = this.template.querySelector('.searchBoxWrapper');
            searchBoxWrapper.classList.remove('slds-show');
            searchBoxWrapper.classList.add('slds-hide');
            const pillDiv = this.template.querySelector('.pillDiv');
            pillDiv.classList.remove('slds-hide');
            pillDiv.classList.add('slds-show');  
        } catch(e) {
            console.log('handleSelectRecordHelper exception: ' + e.message);
        }
    }
    // send selected lookup record to parent component using custom event
    lookupUpdatehandler(value){
        try {
            this.selectedRecordId = value;
            const oEvent = new CustomEvent(
                'lookupupdate', {
                    'detail': {selectedRecord: value}
                }
            );
            this.dispatchEvent(oEvent);
        } catch(e) {
            console.log('lookupUpdatehandler exception: ' + e.message);
        }
        
    }
}