import { LightningElement, api } from 'lwc';
import getDependendValues from '@salesforce/apex/IT_DynamicDependentPicklistController.getWrapperToDisplay';


export default class It_dynamicDependentPicklist extends LightningElement {
    //INPUT

    @api sObjectApiName;
    @api controllingFieldApiName;
    @api dependentFieldApiName;
    @api controllingFieldValue;

    @api inputLabel;
    @api inputPlaceholder;
    @api dropdownAlignment;
    @api isDisabled = false;
    @api minPixelHeight=10;
    @api isRequired;
    @api includeNoneOption = false;
    

    // OUTPUT
    @api selectedValue;


    // PRIVATE ATTRIBUTES

    comboboxOptions = [];

    get showCombobox() {
        return this.comboboxOptions.length > 0;
    }

    get componentStyle() {
        return `min-height:${this.minPixelHeight}px;`;
    }
    
    connectedCallback() {
        // console.log('## retrieveDependendValues result is : ' + JSON.stringify(result));
        try {
            // this.comboboxName = this.dependentFieldApiName.includes('__c') ? this.dependentFieldApiName.replace('__c', '') + 'Input' : this.dependentFieldApiName + 'Input';
            this.retrieveDependendValues(
                    this.sObjectApiName,
                    this.controllingFieldApiName, 
                    this.dependentFieldApiName
                );
    
        } catch(e) {
            console.log('It_dynamicDependentPicklist - exception : ' + e.message);
        }
        
    }

    retrieveDependendValues(sObjectName, controllingFieldApiName, dependentFieldApiName) {
        console.log('retrieveDepentPicklist - sObjectName is' + sObjectName);
        console.log('retrieveDepentPicklist - controllingFieldApiName is' + controllingFieldApiName);
        console.log('retrieveDepentPicklist - dependentFieldApiName is' + dependentFieldApiName);
        getDependendValues({
            sObjectApiName              : sObjectName, 
            controllingFieldApiName     : controllingFieldApiName,
            dependentFieldApiName       : dependentFieldApiName
        }) .then(result => {
            this.prepareDataForPicklist(result);
        })
        .catch(error => {
            console.error('It_dynamicDependentPicklist - retrieveDependendValues - exception ' + error);
        });
    }

    prepareDataForPicklist(wrappedData) {
        let comboboxOptions = [];
        if(this.includeNoneOption === true) {
            // TO DO -> Manage null value
            this.comboboxOptions.push({'--None--' : '--None--' } );
        }
        if(this.controllingFieldValue) {
            if (this.controllingFieldValue.includes(',')) {
                // to do, manage multiple values
            } else {
                comboboxOptions = [... wrappedData[this.controllingFieldValue] ];
            }
        }
        console.table('comboboxOptions are : ' + comboboxOptions );
        this.comboboxOptions = comboboxOptions;
    }

    setDynamicCssClass(cssClassContainer) {
        if(cssClassContainer) {
            cssClassContainer.style.minHeight = this.minPixelHeight + 'px';
        }
    }

    comboboxChangeHandler(event) {
        this.selectedValue = event.detail.value;
        console.log('this.selectedValue is : ' + this.selectedValue);
    }

}