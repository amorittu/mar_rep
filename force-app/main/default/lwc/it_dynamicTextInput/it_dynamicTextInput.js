import { LightningElement, api } from 'lwc';

export default class It_dynamicTextInput extends LightningElement {
    @api inputLabel;
    @api inputPlaceholder;
    @api isRequired = false;
    @api isDisabled = false;
    @api inputMaxLength = 255;
    @api defaultValue = '';
    
    @api outputValue;

    connectedCallback() {
        try {
            if(this.defaultValue) {
                this.outputValue = this.defaultValue;
            }
            console.log(this.inputLabel + ' cc is : ' + this.outputValue); 
        } catch(e) {
            console.log('dynamic text - connectedCallback exception ' + e);
        }
    }

    getOutputValue(event) {
        try {
            console.log(this.inputLabel + ' is : ' + event.detail.value);
            this.outputValue = event.detail.value;
        } catch(e) {
            console.log('dynamic date - getOutputValue exception ' + e);
        }
    }
}