import { LightningElement,api } from 'lwc';

export default class core_ImageControl extends LightningElement {
    @api htmlvalue;
    @api altText;
    @api url;
}