import { LightningElement, wire, api } from 'lwc';
import { ShowToastEvent } from "lightning/platformShowToastEvent";
import { refreshApex } from '@salesforce/apex';

//Custom labels
import lbl_PageTitle from '@salesforce/label/c.CORE_MTU_LBL_PAGE_TITLE';
import lbl_SearchTask from '@salesforce/label/c.CORE_MTU_LBL_SEARCH_TASK';
import lbl_SearchTaskPlaceholder from '@salesforce/label/c.CORE_MTU_LBL_SEARCH_TASK_PLACE_HOLDER';;
import lbl_No_Tasks from '@salesforce/label/c.CORE_MTU_LBL_BTN_NO_OF_TASKS';
import lbl_Confirm_AY from '@salesforce/label/c.CORE_MTU_LBL_CONFIRM_AY';
import lbl_Confirm_BU from '@salesforce/label/c.CORE_MTU_LBL_CONFIRM_BU';
import lbl_Confirm_Subtype from '@salesforce/label/c.CORE_MTU_LBL_CONFIRM_SUBTYPE';
import lbl_Select_Loss_Reason from '@salesforce/label/c.CORE_MTU_LBL_SELECT_LOSS_REASON';
import lbl_Select_Result from '@salesforce/label/c.CORE_MTU_LBL_SELECT_RESULT';
import lbl_Select_Status from '@salesforce/label/c.CORE_MTU_LBL_SELECT_STATUS';
import msg_Complete_Task_Confirm from '@salesforce/label/c.CORE_MTU_LBL_MSG_COMPLETE_TASK_CONFIRM';
import msg_Cancel_Task_Confirm from '@salesforce/label/c.CORE_MTU_LBL_MSG_CANCEL_TASK_CONFIRM';
import lbl_btn_Confirm from '@salesforce/label/c.CORE_MTU_LBL_BTN_CONFIRM';
import lbl_btn_Save from '@salesforce/label/c.CORE_MTU_LBL_BTN_Save';
import lbl_btn_Cancel from '@salesforce/label/c.CORE_MTU_LBL_BTN_CANCEL';
import msg_Job_Async from '@salesforce/label/c.CORE_MTU_MSG_JOB_ASYNC';
import msg_Job_Sync from '@salesforce/label/c.CORE_MTU_MSG_JOB_SYNC';



//Apex Methods
import getSetting from "@salesforce/apex/CORE_LC_MassTaskUpdate.getOrgSetting";
//import getListViews from "@salesforce/apex/CORE_LC_MassTaskUpdate.getListViews";
import getListViewData from "@salesforce/apex/CORE_LC_MassTaskUpdate.getListViewData";
import getBUs from "@salesforce/apex/CORE_LC_MassTaskUpdate.getBUs";
import getAYs from "@salesforce/apex/CORE_LC_MassTaskUpdate.getAYs";
import getTaskSubtype from "@salesforce/apex/CORE_LC_MassTaskUpdate.getTaskSubtype";
import runUpdateTasks from "@salesforce/apex/CORE_LC_MassTaskUpdate.runUpdateTasks";
import getPicklistValues from '@salesforce/apex/CORE_LC_MassTaskUpdate.getPicklistValues';
import getNumberOfTasks from '@salesforce/apex/CORE_LC_MassTaskUpdate.getNumberOfTasks';

const STATUS_COMPLETED = "Completed";
const STATUS_CANCELLED = "Cancelled";

export default class CoreMassTaskUpdate extends LightningElement {
    labels = {
        lbl_PageTitle, lbl_SearchTask, lbl_SearchTaskPlaceholder,lbl_Confirm_AY, lbl_Confirm_BU, lbl_btn_Confirm,lbl_btn_Cancel,lbl_btn_Save,lbl_Confirm_Subtype,
        lbl_Select_Loss_Reason,lbl_Select_Result,lbl_Select_Status,lbl_No_Tasks
    };

    //Array (List) variables
    @api viewsListFromVFP;
    viewsList = [];
    busList = [];
    aysList = [];
    subtypeList = [];
    lrList = [];
    resList = [];
    //subtypeList =[];
    //resultList = [];
   // lossreasonList = [];
    statusList = [
        { label : "Completed", value : STATUS_COMPLETED },
        { label : "Cancelled", value : STATUS_CANCELLED }
    ];

    //Object variables
    viewSelected = {};
    viewsQueryMap = {};

    //Refresh Array variables
    wiredSetting = {};

    //String
    noOfTaskText = "";
    selectedView = "";
    confirmMsg = "";
    selectedBU = "";
    selectedAcademicYear = "";
    selectedSubtype = "";
    selectedstatus = "";
    selectedLossReason = '';
    selectedResult = '';

    //Boolean variables
    hasViews = false;
    isViewSelected = false;
    isViewNotSelected = true;
    loading = true;
    inProgress = false;
    showConfirmMsg = false;
    allFiltersDisabled = true;
    showTotal = false;

    //Number variables
    defaultBatchSize = 100;
    totalSelected = 0;


    @wire(getSetting, { })
    wiredGetSetting(response) { 
        debugger;
        console.log('@getSetting > response: ' + JSON.stringify(response));
        this.wiredSetting = response;

        if(response.error){ 
            this.showToastError("", response.error.body.message);
        }
        else if(response.data != null){

            this.defaultBatchSize = (response.data.batchSize == null) ? this.defaultBatchSize :response.data.batchSize;
            this.inProgress = (response.data.jobInProgress == null) ? false :response.data.jobInProgress;

            if(this.inProgress == true){
                this.disableScreen();
            }
        }

        this.loading = false;
    }

    connectedCallback() {
        console.log("@connectedCallback > viewsListFromVFP: " + JSON.stringify(this.viewsListFromVFP));

        for(var view of this.viewsListFromVFP){
            this.viewsList = [...this.viewsList , { value: view.viewId, label: view.viewLabel, selected: false } ];
            this.viewsQueryMap[view.viewId] = view.viewQuery;
        }

        console.log("@connectedCallback > viewsList: " + JSON.stringify(this.viewsList));
        console.log("@connectedCallback > viewsQueryMap: " + JSON.stringify(this.viewsQueryMap));

        this.loading = false;
    }



    handleChangeView(event){
        this.isViewNotSelected = true;
        this.isViewSelected = false;
        this.selectedView = event.detail.value;
        console.log('@wiredListViews > selectedView: ' + this.selectedView);

        if(this.selectedView != null
           && this.selectedView != ""){

            this.getTasks(this.selectedView, this.viewsQueryMap[this.selectedView]);
        }
        else{
            this.selectedBU = "";
            this.selectedAcademicYear = "";
            this.selectedSubtype = "";
            this.selectedstatus = "";
            this.selectedResult = "";
            this.selectedLossReason = "";
            this.allFiltersDisabled = true;
        }
    }

    getTasks(viewId, viewQuery){
        this.loading = true;
        this.noOfTasksText = "";
        
        getListViewData({ 
            //viewId: this.selectedView
            view: { value : viewId, query : viewQuery }
        })
        .then((result) => {

            this.viewSelected["viewId"] = result.viewId;
            this.viewSelected["viewQuery"] = result.viewQuery;
            this.viewSelected["viewRecordsSize"] = result.viewRecordsSize;

            this.noOfTaskText = lbl_No_Tasks.replace("#", this.viewSelected["viewRecordsSize"]);

            this.loading = false;
            this.isViewSelected = true;
            this.isViewNotSelected = false;
            
            this.getBusinessUnits();
            
        })
        .catch((error) => {

            this.showToastError("", error.body.message);

            this.loading = false;
        });
        
    }

    getBusinessUnits(){
        console.log("getBUsinessunits: " + this.selectedBU);
        console.log("getVIEWSELECTED " + this.viewSelected);
        this.loading = true;
        this.busList = [];
        
        getBUs({ 
            viewSelected: this.viewSelected
        })
        .then((result) => {
            debugger;
            
            for(var bu of result){
                
                this.busList = [...this.busList , { value: bu.buId, label: bu.buName } ];
                console.log("getBUsinessunits: " + JSON.stringify(this.busList));
            }

            /*if(result
               && result.length == 1){

                this.selectedBU = result[0].buId;
                
                
            }*/

            this.allFiltersDisabled = true;

            this.loading = false;
            
        })
        .catch((error) => {
            debugger;
            console.log('Error : ' + JSON.stringify(error));
            this.showToastError("", error.body.message);

            this.loading = false;
        });
        
    }
    getAcademicYear(){
        this.loading = true;
        this.aysList = [];
        getAYs({ 
            viewSelected: this.viewSelected,
            bu : this.selectedBU
        })
        .then((result) => {
            debugger;
            for(var ay of result){

                this.aysList = [...this.aysList , { value: ay.ayapi, label: ay.aylabel } ];
                console.log("getAY: " + JSON.stringify(this.aysList));
            }

            /*if(result
               && result.length == 1){

                this.selectedAcademicYear = result[0].ayapi;
                
            }*/

            
            this.allFiltersDisabled = true;

            this.loading = false;
        })
        .catch((error) => {
            debugger;
            console.log('Error : ' + JSON.stringify(error));
            this.showToastError("", error.body.message);

            this.loading = false;
        });
        
    }

    handleChangeBU(event){
        this.selectedBU = event.detail.value;
        console.log("@handleChangeBU > selectedBU: " + this.selectedBU);
        this.getAcademicYear();
        if(this.selectedAcademicYear
            && this.selectedBU
            && this.selectedSubtype){
            this.getNumberOfTask();
            this.getSubtype();
        }
        if(this.selectedAcademicYear
           && this.selectedBU
           && this.selectedSubtype && this.selectedStatus){
            if(this.selectedStatus == STATUS_COMPLETED){
                if(this.selectedResult.includes('abandon')){
                    if(this.selectedLossReason){
                        this.allFiltersDisabled = false
                    }
                    else {
                        this.allFiltersDisabled = true
                    }
                }
                else{
                    this.allFiltersDisabled = false 
                }
            }
        }
        else{
            this.allFiltersDisabled = true;
        }

    }
    getSubtype(){
        this.loading = true;
        this.subtypeList = [];
        
        getTaskSubtype({ 
            viewSelected: this.viewSelected,
            bu : this.selectedBU,
            ay : this.selectedAcademicYear
        })
        .then((result) => {
            for(var subtype of result){
    
                this.subtypeList = [...this.subtypeList , { value: subtype.apiname, label: subtype.label } ];
                
            }

            if(result
               && result.length == 1){

                this.selectedSubtype = result[0].apiname;
                
            }
             
            
            this.allFiltersDisabled = true;

            this.loading = false;
        })
        .catch((error) => {
            debugger;
            console.log('Error : ' + JSON.stringify(error));
            this.showToastError("", error.body.message);

            this.loading = false;
        });
        
    }
    getResult(){
        this.loading = true;
        this.resList = [];
        getPicklistValues({ 
            objectName: "Task",
            fieldName: "CORE_Task_Result__c"
        })
        .then((result) => {
            //debugger;
         
            for(var res of result){
                
                this.resList = [...this.resList , { value: res.apiname, label: res.labelname } ];
                
            }

            if(result
               && result.length == 1){

                this.selectedResult = result[0].apiname;
                
            }

            
            this.allFiltersDisabled = true;

            this.loading = false;
        })
        .catch((error) => {
            debugger;
            console.log('Error : ' + JSON.stringify(error));
            this.showToastError("", error.body.message);

            this.loading = false;
        });
        
    }
    getLossReason(){
        this.loading = true;
        this.lrList = [];
        getPicklistValues({ 
            objectName: "Task",
            fieldName: "CORE_Loss_Reason__c"
        })
        .then((result) => {
            debugger;
            for(var lr of result){
                
                this.lrList = [...this.lrList , { value: lr.apiname, label: lr.labelname } ];
                
            }

            if(result
               && result.length == 1){

                this.selectedLossReason = result[0].apiname;
                
            }

            
            
            this.allFiltersDisabled = true;

            this.loading = false;
        })
        .catch((error) => {
            debugger;
            console.log('Error : ' + JSON.stringify(error));
            this.showToastError("", error.body.message);

            this.loading = false;
        });
        
    }

    handleChangeAY(event){
        this.selectedAcademicYear = event.detail.value;
        if(this.selectedAcademicYear){
            this.getSubtype();
        }
        console.log("@handleChangeAY > selectedAcademicYear: " + this.selectedAcademicYear);
        if(this.selectedAcademicYear
            && this.selectedBU
            && this.selectedSubtype){
            this.getNumberOfTask();
            }
        if(this.selectedAcademicYear
            && this.selectedBU
            && this.selectedSubtype && this.selectedStatus){
             if(this.selectedStatus == STATUS_COMPLETED){
                 if(this.selectedResult.includes('abandon')){
                     if(this.selectedLossReason){
                         this.allFiltersDisabled = false
                     }
                     else {
                         this.allFiltersDisabled = true
                     }
                 }
                 else{
                     this.allFiltersDisabled = false 
                 }
             }
         }
        else{
            this.allFiltersDisabled = true;
        }
    }
    getNumberOfTask(){
        this.totalSelected = 0;
        getNumberOfTasks({ 
            viewSelected : this.viewSelected,
            buId : this.selectedBU,
            ay : this.selectedAcademicYear,
            subtype : this.selectedSubtype
        })
        .then((result) => {
            debugger;
            
            this.totalSelected = parseInt(result);
                

        })
    }
    handleChangeSubtype(event){
        this.selectedSubtype = event.detail.value;
        console.log("@handleChangeSubtype > selectedSubtype: " + this.selectedSubtype);
        this.getNumberOfTask();
        this.showTotal = true;
        if(this.selectedAcademicYear
            && this.selectedBU
            && this.selectedSubtype && this.selectedStatus){
             if(this.selectedStatus == STATUS_COMPLETED){
                 if(this.selectedResult.includes('abandon')){
                     if(this.selectedLossReason){
                         this.allFiltersDisabled = false
                     }
                     else {
                         this.allFiltersDisabled = true
                     }
                 }
                 else{
                     this.allFiltersDisabled = false 
                 }
             }
        }

        else{
            this.allFiltersDisabled = true;
        }
    }
    handleChangeStatus(event){
        this.selectedStatus = event.detail.value;
        console.log("@handleChangeStatus > selectedStatus: " + this.selectedStatus);
        if(this.selectedAcademicYear
            && this.selectedBU
            && this.selectedSubtype && this.selectedStatus){
            if(this.selectedStatus == STATUS_COMPLETED){
                this.getResult();
                
                if(this.selectedResult.includes('abandon')){
                     if(this.selectedLossReason){
                         this.allFiltersDisabled = false
                     }
                     else {
                        this.selectedLossReason = '';
                         this.allFiltersDisabled = true
                     }
                 }
                 else{
                     this.allFiltersDisabled = false 
                 }
            }
            else{
                this.resList = [];
                this.selectedResult = '';
                this.selectedLossReason = '';
                this.allFiltersDisabled = false
             }
        }

        else{
            this.allFiltersDisabled = true;
        }
    }
    handleChangeResult(event){
        this.selectedResult = event.detail.value;
        console.log("@handleChangeResult > selectedAcademicYear: " + this.selectedAcademicYear)
        console.log("@handleChangeResult > selectedBU: " + this.selectedBU);
        console.log("@handleChangeResult > selectedStatus: " + this.selectedStatus);
        console.log("@handleChangeResult > selectedSubtype: " + this.selectedSubtype);
        console.log("@handleChangeResult > selectedResult: " + this.selectedResult);
        if(this.selectedAcademicYear
            && this.selectedBU
            && this.selectedSubtype && this.selectedStatus){
            
             if(this.selectedStatus == STATUS_COMPLETED){
                 if(this.selectedResult.includes('abandon')){
                    this.getLossReason();
                     if(this.selectedLossReason){
                         this.allFiltersDisabled = false
                     }
                     else {
                         this.allFiltersDisabled = true
                     }
                 }
                 else{
                     this.allFiltersDisabled = false 
                 }
             }
            else{
                this.allFiltersDisabled = false 
            }
        }

        else{
            this.allFiltersDisabled = true;
        }
    }
    handleChangeLossreason(event){
        this.selectedLossReason = event.detail.value;
        console.log("@handleChangeLossreason > selectedAcademicYear: " + this.selectedLossReason);
        if(this.selectedAcademicYear
            && this.selectedBU
            && this.selectedSubtype && this.selectedStatus){
             if(this.selectedStatus == STATUS_COMPLETED){
                 if(this.selectedResult.includes('abandon')){
                     if(this.selectedLossReason){
                         this.allFiltersDisabled = false
                     }
                     else {
                         this.allFiltersDisabled = true
                     }
                 }
                 else{
                     this.allFiltersDisabled = false 
                 }
             }
        }

        else{
            this.allFiltersDisabled = true;
        }
    }

    updateTask(){
        if(this.selectedStatus == STATUS_COMPLETED){
            this.allFiltersDisabled = true
            this.confirmMsg = msg_Complete_Task_Confirm;
        }
        else if(this.selectedStatus == STATUS_CANCELLED){
            this.confirmMsg = msg_Cancel_Task_Confirm;
        }
        this.confirmMsg = this.confirmMsg.replace("#", this.totalSelected);
        this.showConfirmMsg = true
       
        
    }

    confirmUpdateTask(){
        this.showConfirmMsg = false;
        this.loading = true;

        runUpdateTasks({ 
            viewSelected: this.viewSelected,
            buId: this.selectedBU,
            academicYear: this.selectedAcademicYear,
            subtype:  this.selectedSubtype,
            status: 'CORE_PKL_'+ this.selectedStatus,
            result : this.selectedResult,
            lossreason : this.selectedLossReason

        })
        .then((result) => {
            console.log("@runUpdateTasks > result: " + result);

            if(result == 'ASYNC'){
                refreshApex(this.wiredSetting);
                this.disableScreen();
                this.showToastSuccess("", msg_Job_Async);
            }
            else{
                this.showToastSuccess("", msg_Job_Sync);
            }

            this.loading = false;

        })
        .catch((error) => {

            this.showToastError("", error.body.message);

            this.loading = false;
        });
    }

    cancelUpdateTask(){
        this.showConfirmMsg = false;
    }

    showToastSuccess(title, msg){
        console.log("@showToastSuccess");
        this.showToast = true;
        this.isSuccess = true;
        this.isError = false;
        this.message = msg;
        this.header = title;
    }

    showToastError(title, msg){
        console.log("@showToastError");
        this.showToast = true;
        this.isSuccess = false;
        this.isError = true;
        this.message = msg;
        this.header = title;
    }

    closeToast(){
        this.showToast = false;
    }

    disableScreen(){
        this.isViewSelected = false;
        this.isViewNotSelected = true;
        this.inProgress = true;
        this.selectedView = "";
    }
}