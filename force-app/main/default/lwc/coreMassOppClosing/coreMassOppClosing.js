import { LightningElement, wire, api } from 'lwc';
import { ShowToastEvent } from "lightning/platformShowToastEvent";
import { refreshApex } from '@salesforce/apex';

//Custom labels
import lbl_PageTitle from '@salesforce/label/c.CORE_MOC_LBL_PAGE_TITLE';
import lbl_SearchOpp from '@salesforce/label/c.CORE_MOC_LBL_SEARCH_OPP';
import lbl_SearchOppPlaceholder from '@salesforce/label/c.CORE_MOC_LBL_SEARCH_OPP_PLACEHOLDER';
import lbl_btn_Close from '@salesforce/label/c.CORE_MOC_LBL_BTN_CLOSE';
import lbl_No_Opp from '@salesforce/label/c.CORE_MOC_LBL_NO_OF_OPP';
import lbl_Select_Close_Status from '@salesforce/label/c.CORE_MOC_LBL_SELECT_CLOSE_STATUS';
import msg_Close_Won_Confirm from '@salesforce/label/c.CORE_MOC_MSG_CLOSE_WON_CONFIRM';
import msg_Close_Lost_Confirm from '@salesforce/label/c.CORE_MOC_MSG_CLOSE_LOST_CONFIRM';
import lbl_btn_Confirm from '@salesforce/label/c.CORE_MOC_LBL_BTN_CONFIRM';
import lbl_btn_Cancel from '@salesforce/label/c.CORE_MOC_LBL_BTN_CANCEL';
import msg_Job_Async from '@salesforce/label/c.CORE_MOC_MSG_JOB_ASYNC';
import msg_Job_Sync from '@salesforce/label/c.CORE_MOC_MSG_JOB_SYNC';

//Apex Methods
import getSetting from "@salesforce/apex/CORE_LC_MassOpportunityClosing.getOrgSetting";
//import getListViews from "@salesforce/apex/CORE_LC_MassOpportunityClosing.getListViews";
import getListViewData from "@salesforce/apex/CORE_LC_MassOpportunityClosing.getListViewData";
import runCloseOpps from "@salesforce/apex/CORE_LC_MassOpportunityClosing.runCloseOpps";

const STATUS_CLOSE_LOST = "Closed Lost";
const STATUS_CLOSE_WON = "Closed Won";
 
export default class CoreMassOppClosing extends LightningElement {

    //Custom label
    labels = {
        lbl_PageTitle, lbl_SearchOpp, lbl_SearchOppPlaceholder, lbl_btn_Close, lbl_Select_Close_Status, lbl_btn_Confirm, lbl_btn_Cancel
    };

    //Array (List) variables
    @api viewsListFromVFP;
    viewsList = [];
    statusList = [
        { label : "Closed Won", value : STATUS_CLOSE_WON },
        { label : "Closed Lost", value : STATUS_CLOSE_LOST }
    ];

    //Object variables
    viewSelected = {};
    viewsQueryMap = {};

    //Refresh Array variables
    wiredSetting = {};

    //String
    noOfOppText = "";
    selectedView = "";
    selectedStatus = STATUS_CLOSE_WON;
    confirmMsg = "";

    //Boolean variables
    hasViews = false;
    isViewSelected = false;
    isViewNotSelected = true;
    loading = true;
    inProgress = false;
    showConfirmMsg = false;

    //Number variables
    defaultBatchSize = 100;

    //For custom toast message
    showToast = false;
    isSuccess = false;
    message = "";
    header = "";

    @wire(getSetting, { })
    wiredGetSetting(response) { 

        console.log('@getSetting > response: ' + JSON.stringify(response));
        this.wiredSetting = response;

        if(response.error){ 
            this.showToastError("", response.error.body.message);
        }
        else if(response.data != null){

            this.defaultBatchSize = (response.data.batchSize == null) ? this.defaultBatchSize :response.data.batchSize;
            this.inProgress = (response.data.jobInProgress == null) ? false :response.data.jobInProgress;

            if(this.inProgress == true){
                this.disableScreen();
            }
        }

        this.loading = false;
    }

    connectedCallback() {
        console.log("@connectedCallback > viewsListFromVFP: " + JSON.stringify(this.viewsListFromVFP));

        for(var view of this.viewsListFromVFP){
            this.viewsList = [...this.viewsList , { value: view.viewId, label: view.viewLabel, selected: false } ];
            this.viewsQueryMap[view.viewId] = view.viewQuery;
        }

        console.log("@connectedCallback > viewsList: " + JSON.stringify(this.viewsList));
        console.log("@connectedCallback > viewsQueryMap: " + JSON.stringify(this.viewsQueryMap));

        this.loading = false;
    }

    /*@wire(getListViews, { })
    wiredListViews(response) { 

        this.wiredViewsList = response;

        console.log('@wiredListViews > response: ' + JSON.stringify(response));

        if(response.error){ 
            this.showToastError("", response.error.body.message);
        }
        else if(response.data != null){

            for(var view of response.data){
                this.viewsList = [...this.viewsList , { value: view.viewId, label: view.viewLabel, selected: false } ];
            }
        }

        this.loading = false;
    }*/

    handleChangeView(event){
        this.isViewNotSelected = true;
        this.isViewSelected = false;
        this.selectedView = event.detail.value;
        console.log('@wiredListViews > selectedView: ' + this.selectedView);

        if(this.selectedView != null
           && this.selectedView != ""){

            this.getOpportunities(this.selectedView, this.viewsQueryMap[this.selectedView]);
        }
    }

    handleChangeStatus(event){
        this.selectedStatus = event.detail.value;
    }

    getOpportunities(viewId, viewQuery){
        this.loading = true;
        this.noOfOppText = "";
        
        getListViewData({ 
            //viewId: this.selectedView
            view: { value : viewId, query : viewQuery }
        })
        .then((result) => {

            this.viewSelected["viewId"] = result.viewId;
            this.viewSelected["viewQuery"] = result.viewQuery;
            this.viewSelected["viewRecordsSize"] = result.viewRecordsSize;

            this.noOfOppText = lbl_No_Opp.replace("#", this.viewSelected["viewRecordsSize"]);

            this.loading = false;
            this.isViewSelected = true;
            this.isViewNotSelected = false;
        })
        .catch((error) => {

            this.showToastError("", error.body.message);

            this.loading = false;
        });
        
    }

    closeOpps(){

        if(this.selectedStatus == STATUS_CLOSE_LOST){
            this.confirmMsg = msg_Close_Lost_Confirm;
        }
        else if(this.selectedStatus == STATUS_CLOSE_WON){
            this.confirmMsg = msg_Close_Won_Confirm;
        }
        
        this.confirmMsg = this.confirmMsg.replace("#", this.viewSelected["viewRecordsSize"]);
        this.showConfirmMsg = true;
    }

    confirmClose(){
        this.showConfirmMsg = false;
        this.loading = true;

        runCloseOpps({ 
            viewSelected: this.viewSelected,
            status: this.selectedStatus
        })
        .then((result) => {
            console.log("@runCloseOpps > result: " + result);

            if(result == 'ASYNC'){
                refreshApex(this.wiredSetting);
                this.disableScreen();
                this.showToastSuccess("", msg_Job_Async);
            }
            else{
                this.showToastSuccess("", msg_Job_Sync);
            }

            this.loading = false;

        })
        .catch((error) => {

            this.showToastError("", error.body.message);

            this.loading = false;
        });
    }

    cancelClose(){
        this.showConfirmMsg = false;
    }

    showToastSuccess(title, msg){
        console.log("@showToastSuccess");
        this.showToast = true;
        this.isSuccess = true;
        this.isError = false;
        this.message = msg;
        this.header = title;
    }

    showToastError(title, msg){
        console.log("@showToastError");
        this.showToast = true;
        this.isSuccess = false;
        this.isError = true;
        this.message = msg;
        this.header = title;
    }

    closeToast(){
        this.showToast = false;
    }

    disableScreen(){
        this.isViewSelected = false;
        this.isViewNotSelected = true;
        this.inProgress = true;
        this.selectedView = "";
    }
}