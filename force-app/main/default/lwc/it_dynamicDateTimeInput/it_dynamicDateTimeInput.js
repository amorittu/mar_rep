import { LightningElement, api} from 'lwc';

export default class It_dynamicDateTimeInput extends LightningElement {
    @api inputLabel;
    @api inputPlaceholder;
    @api isRequired = false;
    @api isDisabled = false;
    @api defaultValue;
    
    @api outputValue;

    connectedCallback() {
        try {
            console.log('it_dynamicDateInput - defaultValue: ' + this.defaultValue);
            if(this.defaultValue) {
                this.outputValue = this.defaultValue;
            } 
        } catch(e) {
            console.log('it_dynamicDateInput - connectedCallback exception : ' + e.message);
        }
    }

    getOutputValue(event) {
        try {
            this.outputValue = event.detail.value;
            console.log(this.inputLabel + ' is : ' + this.outputValue);
        } catch(e) {
            console.log('it_dynamicDateInput - getOutputValue exception ' + e.message);
        }
    }
}