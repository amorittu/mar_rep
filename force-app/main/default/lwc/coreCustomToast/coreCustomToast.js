import { LightningElement, api } from 'lwc';
 
export default class CoreCustomToast extends LightningElement {

    @api isSuccess;
    @api isError;
    @api header;
    @api message;

    connectedCallback(){
        console.log("@connectedCallback > isSuccess: " + this.isSuccess);
        console.log("@connectedCallback > isError: " + this.isError);
        console.log("@connectedCallback > header: " + this.header);
        console.log("@connectedCallback > message: " + this.message);
    }

    handleToastClose(){
        const evt = new CustomEvent('closetoast', {});
        this.dispatchEvent(evt);
    }
}