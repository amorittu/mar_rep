import { LightningElement, wire, api } from 'lwc';
import { ShowToastEvent } from "lightning/platformShowToastEvent";
import { refreshApex } from '@salesforce/apex';

//Custom labels
import lbl_PageTitle from '@salesforce/label/c.CORE_MAYC_LBL_PAGE_TITLE';
import lbl_SearchOpp from '@salesforce/label/c.CORE_MAYC_LBL_SEARCH_OPP';
import lbl_SearchOppPlaceholder from '@salesforce/label/c.CORE_MAYC_LBL_SEARCH_OPP_PLACEHOLDER';
import lbl_btn_ConfirmAcademicYear from '@salesforce/label/c.CORE_MAYC_LBL_BTN_CONFIRM_ACADEMIC_YEAR';
import lbl_No_Opp from '@salesforce/label/c.CORE_MAYC_LBL_NO_OF_OPP';
import lbl_Confirm_BU_Opp from '@salesforce/label/c.CORE_MAYC_LBL_CONFIRM_BU_OPP';
import lbl_Select_New_Academic_Year from '@salesforce/label/c.CORE_MAYC_LBL_SELECT_NEW_ACADEMIC_YEAR';
import lbl_Select_Pricebook from '@salesforce/label/c.CORE_MAYC_LBL_SELECT_PRICEBOOK';
import msg_Change_Confirm from '@salesforce/label/c.CORE_MAYC_CHANGE_CONFIRM';
import lbl_btn_Confirm from '@salesforce/label/c.CORE_MAYC_LBL_BTN_CONFIRM';
import lbl_btn_Cancel from '@salesforce/label/c.CORE_MAYC_LBL_BTN_CANCEL';
import msg_Job_Async from '@salesforce/label/c.CORE_MAYC_MSG_JOB_ASYNC';
import msg_Job_Sync from '@salesforce/label/c.CORE_MAYC_MSG_JOB_SYNC';

// By GH 03/06/2022
import lbl_btn_Close from '@salesforce/label/c.CORE_MAYC_LBL_BTN_CLOSE';
import msg_processing from '@salesforce/label/c.CORE_MAYC_MSG_PROCESSING';
import msg_Done from '@salesforce/label/c.CORE_MAYC_DONE_PROCESSING';

//STD Methods
import { getPicklistValues, getObjectInfo } from 'lightning/uiObjectInfoApi';
import ACADEMIC_YEAR_FIELD from '@salesforce/schema/Opportunity.CORE_OPP_Academic_Year__c';

//Apex Methods
import getSetting from "@salesforce/apex/CORE_LC_MassOppAcademicYearConfirmation.getOrgSetting";
//import getListViews from "@salesforce/apex/CORE_LC_MassOppAcademicYearConfirmation.getListViews";
import getListViewData from "@salesforce/apex/CORE_LC_MassOppAcademicYearConfirmation.getListViewData";
import getBUs from "@salesforce/apex/CORE_LC_MassOppAcademicYearConfirmation.getBUs";
import getPriceBooks from "@salesforce/apex/CORE_LC_MassOppAcademicYearConfirmation.getPriceBooks";
import changeAcademicYear from "@salesforce/apex/CORE_LC_MassOppAcademicYearConfirmation.runChangeAcademicYear";
 
export default class CoreMassOppAcademicYearConfirmation extends LightningElement {
    labels = {
        lbl_PageTitle, lbl_SearchOpp, lbl_SearchOppPlaceholder, lbl_btn_ConfirmAcademicYear, lbl_Confirm_BU_Opp, lbl_Select_New_Academic_Year, 
        lbl_Select_Pricebook, lbl_btn_Confirm, lbl_btn_Cancel, lbl_btn_Close
    };

    //Array (List) variables
    @api viewsListFromVFP;
    viewsList = [];
    busList = [];
    pbsList = [];

    //Object variables
    viewSelected = {};
    viewsQueryMap = {};
    buDivMap = {};

    //Refresh Array variables
    wiredSetting = {};

    //String
    noOfOppText = "";
    selectedView = "";
    confirmMsg = "";
    selectedBU = "";
    selectedAcademicYear = "";
    selectedPB = "";

    //Boolean variables
    hasViews = false;
    isViewSelected = false;
    isViewNotSelected = true;
    loading = true;
    inProgress = false;
    showConfirmMsg = false;
    allFiltersDisabled = true;

    //By GH 03/06/2022
    showCloseMsg = false;
    processingMsg = "";

    //Number variables
    defaultBatchSize = 100;
    totalSelected = 0;

    @wire(getObjectInfo, { objectApiName: 'Opportunity' })
    objectInfo;

    @wire(getPicklistValues, { recordTypeId: '$objectInfo.data.defaultRecordTypeId', fieldApiName: ACADEMIC_YEAR_FIELD})
    academicYearPicklistValues;

    @wire(getSetting, { })
    wiredGetSetting(response) { 

        console.log('@getSetting > response: ' + JSON.stringify(response));
        this.wiredSetting = response;

        if(response.error){ 
            this.showToastError("", response.error.body.message);
        }
        else if(response.data != null){

            this.defaultBatchSize = (response.data.batchSize == null) ? this.defaultBatchSize :response.data.batchSize;
            /* commented by GH (replaced by close popup)
            
            this.inProgress = (response.data.jobInProgress == null) ? false :response.data.jobInProgress;

            if(this.inProgress == true){
                this.disableScreen();
            }*/ 
        }

        this.loading = false;
    }

    connectedCallback() {
        console.log("@connectedCallback > viewsListFromVFP: " + JSON.stringify(this.viewsListFromVFP));

        for(var view of this.viewsListFromVFP){
            this.viewsList = [...this.viewsList , { value: view.viewId, label: view.viewLabel, selected: false } ];
            this.viewsQueryMap[view.viewId] = view.viewQuery;
        }

        console.log("@connectedCallback > viewsList: " + JSON.stringify(this.viewsList));
        console.log("@connectedCallback > viewsQueryMap: " + JSON.stringify(this.viewsQueryMap));

        this.loading = false;
    }

    /*@wire(getListViews, { })
    wiredListViews(response) { 

        this.wiredViewsList = response;

        console.log('@wiredListViews > response: ' + JSON.stringify(response));

        if(response.error){ 
            this.showToastError("", response.error.body.message);
        }
        else if(response.data != null){

            for(var view of response.data){
                this.viewsList = [...this.viewsList , { value: view.viewId, label: view.viewLabel, selected: false } ];
            }
        }

        this.loading = false;
    }*/

    handleChangeView(event){
        this.isViewNotSelected = true;
        this.isViewSelected = false;
        this.selectedView = event.detail.value;
        console.log('@wiredListViews > selectedView: ' + this.selectedView);

        if(this.selectedView != null
           && this.selectedView != ""){

            this.getOpportunities(this.selectedView, this.viewsQueryMap[this.selectedView]);
        }
        else{
            this.selectedAcademicYear = "";
            this.selectedBU = "";
            this.selectedPB = "";
            this.allFiltersDisabled = true;
        }
    }

    getOpportunities(viewId, viewQuery){
        this.loading = true;
        this.noOfOppText = "";
        
        getListViewData({ 
            //viewId: this.selectedView
            view: { value : viewId, query : viewQuery }
        })
        .then((result) => {

            this.viewSelected["viewId"] = result.viewId;
            this.viewSelected["viewQuery"] = result.viewQuery;
            this.viewSelected["viewRecordsSize"] = result.viewRecordsSize;

            this.noOfOppText = lbl_No_Opp.replace("#", this.viewSelected["viewRecordsSize"]);

            this.loading = false;
            this.isViewSelected = true;
            this.isViewNotSelected = false;

            this.getBusinessUnits();
        })
        .catch((error) => {

            this.showToastError("", error.body.message);

            this.loading = false;
        });
        
    }

    getBusinessUnits(){
        this.loading = true;
        this.busList = [];
        
        getBUs({ 
            viewSelected: this.viewSelected
        })
        .then((result) => {

            for(var bu of result){
                this.busList = [...this.busList , { value: bu.buId, label: bu.buName } ];
                this.buDivMap[bu.buId] = bu.divId;
            }

            if(result
               && result.length == 1){

                this.selectedBU = result[0].buId;
                
            }

            this.selectedAcademicYear = "";
            this.selectedPB = "";
            this.allFiltersDisabled = true;

            this.loading = false;
        })
        .catch((error) => {

            this.showToastError("", error.body.message);

            this.loading = false;
        });
        
    }

    handleChangeBU(event){
        this.selectedBU = event.detail.value;
        console.log("@handleChangeBU > selectedBU: " + this.selectedBU);

        if(this.selectedAcademicYear){
            this.getPBooks();
        }

        if(this.selectedAcademicYear
           && this.selectedBU
           && this.selectedPB){

            this.allFiltersDisabled = false;
        }
        else{
            this.allFiltersDisabled = true;
        }
    }

    handleChangeAY(event){
        this.selectedAcademicYear = event.detail.value;
        console.log("@handleChangeAY > selectedAcademicYear: " + this.selectedAcademicYear);

        if(this.selectedBU){
            this.getPBooks();
        }

        if(this.selectedAcademicYear
            && this.selectedBU
            && this.selectedPB){
 
            this.allFiltersDisabled = false;
        }
        else{
            this.allFiltersDisabled = true;
        }
    }

    handleChangePB(event){
        this.selectedPB = event.detail.value;
        console.log("@handleChangePB > selectedPB: " + this.selectedPB);

        if(this.selectedAcademicYear
            && this.selectedBU
            && this.selectedPB){
 
            this.allFiltersDisabled = false;
        }
        else{
            this.allFiltersDisabled = true;
        }
    }

    getPBooks(){
        console.log("@getPBooks > selectedBU: " + this.selectedBU);
        console.log("@getPBooks > selectedAcademicYear: " + this.selectedAcademicYear);
        this.pbsList = [];
        this.loading = true;
        this.totalSelected = 0;

        getPriceBooks({ 
            viewSelected: this.viewSelected,
            buId: '"' + this.selectedBU + '"',
            academicYear: this.selectedAcademicYear
        })
        .then((result) => {
            console.log("@getPBooks > result: " + JSON.stringify(result));
            console.log("@getPBooks > result: " + result.length);

            if(result.length === 0){
                this.showToastError("", "No pricebook found");
            }

            for(var pb of result){
                this.pbsList = [...this.pbsList , { value: pb.pbId, label: pb.pbName } ];

                if(pb.numOpps){
                    this.totalSelected = parseInt(pb.numOpps);
                }
            }

            if(result
               && result.length == 1){

                this.selectedPB = result[0].pbId;

                if(this.selectedAcademicYear
                    && this.selectedBU
                    && this.selectedPB){
         
                    this.allFiltersDisabled = false;
                }
                else{
                    this.allFiltersDisabled = true;
                }
            }
            else{
                this.allFiltersDisabled = true;
            }

            this.loading = false;

        })
        .catch((error) => {

            this.showToastError("", error.body.message);
            this.loading = false;
        });
    }

    changeAY(){
        this.confirmMsg = msg_Change_Confirm;       
        this.confirmMsg = this.confirmMsg.replace("#", this.totalSelected);//this.viewSelected["viewRecordsSize"]
        this.showConfirmMsg = true;
    }

    confirmChangeAY(){
        
        this.showConfirmMsg = false;
        this.loading = true;

        changeAcademicYear({ 
            viewSelected: this.viewSelected,
            divId: '"' + this.buDivMap[this.selectedBU] + '"',
            buId: '"' + this.selectedBU + '"',
            academicYear: this.selectedAcademicYear,
            pbId: '"' + this.selectedPB + '"'
        })
        .then((result) => {
            console.log("@runCloseOpps > result: " + result);

            this.loading = false;
            if(result == 'ASYNC'){
                //BY GH 03/06/2022
                this.processingMsg = msg_processing;
                this.showCloseMsg = true;
                refreshApex(this.wiredSetting);
                //this.disableScreen(); commented by GH (replaced by closeing Popup)
                //this.showToastSuccess("", msg_Job_Async);
            }
            else{
                //BY GH 03/06/2022
                this.processingMsg = msg_Done;
                this.showCloseMsg = true;
                //this.showToastSuccess("", msg_Job_Sync);
            }
        })
        .catch((error) => {

            this.showToastError("", error.body.message);

            this.loading = false;
        });
    }

    cancelChangeAY(){
        this.showConfirmMsg = false;
    }

    showToastSuccess(title, msg){
        this.dispatchEvent(
            new ShowToastEvent({
                title: title,
                message: msg,
                variant: "success",
                mode: "sticky"
            })
        );
    }

    showToastError(title, msg){
        this.dispatchEvent(
            new ShowToastEvent({
                title: title,
                message: msg,
                variant: "error",
                mode: "sticky"
            })
        );
    }

    disableScreen(){
        this.isViewSelected = false;
        this.isViewNotSelected = true;
        this.inProgress = true;
        this.selectedView = "";
    }
    closePopup(){
        this.showCloseMsg = false;
        this.selectedView = "";
        this.selectedBU = "";
        this.selectedAcademicYear = "";
        this.selectedPB = "";
        this.noOfOppText = "";
        this.totalSelected = "";
        console.log("this.showCloseMsg ----> " , this.showCloseMsg);
        console.log("this.processingMsg ----> " , this.processingMsg);
    }
}