import { LightningElement, wire, api } from 'lwc';
import { ShowToastEvent } from "lightning/platformShowToastEvent";
import { refreshApex } from '@salesforce/apex';

//Custom labels
import lbl_PageTitle from '@salesforce/label/c.CORE_MORO_LBL_PAGE_TITLE';
import lbl_SearchOpp from '@salesforce/label/c.CORE_MORO_LBL_SEARCH_OPP';
import lbl_SearchOppPlaceholder from '@salesforce/label/c.CORE_MORO_LBL_SEARCH_OPP_PLACEHOLDER';
import lbl_btn_ConfirmOpportunitiesReregistration from '@salesforce/label/c.CORE_MORO_LBL_BTN_CONFIRM_OPPS_REREGISTER';
import lbl_No_Opp from '@salesforce/label/c.CORE_MORO_LBL_NO_OF_OPP';
import lbl_Confirm_BU_Opp from '@salesforce/label/c.CORE_MORO_LBL_CONFIRM_BU_OPP';
import lbl_Select_New_Academic_Year from '@salesforce/label/c.CORE_MORO_LBL_SELECT_ACADEMIC_YEAR';
import msg_Change_Confirm from '@salesforce/label/c.CORE_MORO_CHANGE_CONFIRM';
import lbl_btn_Confirm from '@salesforce/label/c.CORE_MORO_LBL_BTN_CONFIRM';
import lbl_btn_Cancel from '@salesforce/label/c.CORE_MORO_LBL_BTN_CANCEL';
import msg_Job_Async from '@salesforce/label/c.CORE_MORO_MSG_JOB_ASYNC';
import msg_Job_Sync from '@salesforce/label/c.CORE_MORO_MSG_JOB_SYNC';

//Apex Methods
import getSetting from "@salesforce/apex/CORE_LC_MassOpportunityReopening.getOrgSetting";
import getListViews from "@salesforce/apex/CORE_LC_MassOpportunityReopening.getListViews";
import getListViewRecordsCount from "@salesforce/apex/CORE_LC_MassOpportunityReopening.getListViewRecordsCount";
import getBusinessUnits from "@salesforce/apex/CORE_LC_MassOpportunityReopening.getBusinessUnits";
import getFilteredOpportunitiesCount from "@salesforce/apex/CORE_LC_MassOpportunityReopening.getOpportunitiesTotal";
import launchBatch from "@salesforce/apex/CORE_LC_MassOpportunityReopening.launchBatch";
import getCurrentAcademicYear from "@salesforce/apex/CORE_LC_MassOpportunityReopening.getCurrentAcademicYear";

export default class CoreMassOpportunityReopening extends LightningElement {
    labels = {
        lbl_PageTitle, lbl_SearchOpp, lbl_SearchOppPlaceholder, lbl_btn_ConfirmOpportunitiesReregistration, lbl_Confirm_BU_Opp, lbl_Select_New_Academic_Year, 
        lbl_btn_Confirm, lbl_btn_Cancel
    };

    //Array (List) variables
    viewsList = [];
    busList = [];

    //Object variables
    viewSelected = {};
    viewsQueryMap = {};
    buDivMap = {};

    //String
    noOfOppText = "";
    selectedView = "";
    confirmMsg = "";
    selectedBU = "";
    selectedAcademicYear = "";
    currentValue = "";

    //Boolean variables
    hasViews = false;
    isViewSelected = false;
    isViewNotSelected = true;
    loading = true;
    inProgress = false;
    showConfirmMsg = false;
    allFiltersDisabled = true;

    //Number variables
    defaultBatchSize = 100;
    totalSelected = 0;

    @wire(getSetting, { })
    wiredGetSetting(response) { 

        console.log('@getSetting > response: ' + JSON.stringify(response));

        if(response.error){ 
            this.showToastError("", response.error.body.message);
        }
        else if(response.data != null){

            this.inProgress = (response.data.jobInProgress == null) ? false :response.data.jobInProgress;

            if(this.inProgress == true){
                this.disableScreen();
            }
        }

        this.loading = false;
    }

    connectedCallback() {
        this.loading = false;
        getListViews().then((result)=>{
            
            this.viewsList = result.map((x)=>{
               return {label : x.Name,
               value : x.Id} ;
            });
        })
    }

    handleChangeView(event){
        this.isViewNotSelected = true;
        this.isViewSelected = false;
        this.selectedView = event.detail.value;
        console.log('@wiredListViews > selectedView: ' + this.selectedView);
        this.getOpportunitiesCount();
    }

    getOpportunitiesCount(){
        this.loading = true;
        this.noOfOppText = "";
        
        getListViewRecordsCount({ 
            //viewId: this.selectedView
            listViewId: this.selectedView
        })
        .then((result) => {
            console.log(result);
            
            this.noOfOppText = (result[0]?.Total>2) ? lbl_No_Opp.replace("#",result[0]?.Total) : lbl_No_Opp.replace("#",result[0]?.Total).replace('opportunities','opportunity');
            this.loading = false;
            this.isViewSelected = true;
            this.isViewNotSelected = false;

            this.retrieveBusinessUnits();
            this.retrieveCurrentAcademicYearPicklistValue();
        })
        .catch((error) => {

            this.showToastError("error", error.body.message);
            this.loading = false;
        });
        
    }

    retrieveCurrentAcademicYearPicklistValue(){
        this.loading=true;
        getCurrentAcademicYear().then(result=>{
            this.currentValue=result;
            this.loading=false;

        }).catch(error=>{
            console.log(error);
        })
    }

    retrieveBusinessUnits(){
        this.loading = true;
        this.busList = [];
        
        getBusinessUnits()
        .then((result) => {
            
            this.busList = result.map(x=>{
                return {'label':x.Name,
                'value':x.Id
            };
            })

            if(this.busList
               && this.busList.length > 0){

                this.selectedBU = this.busList[0].value;
                
            }
            this.allFiltersDisabled = true;

            this.loading = false;
        })
        .catch((error) => {
            this.loading = false;
            console.log('error - '+error);         

        });
        
    }

    handleChangeBU(event){
        this.selectedBU = event.detail.value;
        console.log("@handleChangeBU > selectedBU: " + this.selectedBU);

        if(this.selectedAcademicYear
           && this.selectedBU){
            this.loading = true;

            getFilteredOpportunitiesCount(
                {
                    'listViewId': this.selectedView ,
                    'buId': this.selectedBU  ,
                    'aY': this.selectedAcademicYear
            }
            ).then((result)=>{
                if(result.length > 0){
                    this.totalSelected = result[0]?.Total;
                }
                this.loading = false;
                
            })
            .catch((error)=>{
                this.loading = false;
                
                console.log('error',error)
            })
            this.allFiltersDisabled = false;
        }
        else{
            this.allFiltersDisabled = true;
        }
    }

    handleChangeAY(event){
        this.selectedAcademicYear = event.detail.value;
        console.log("@handleChangeAY > selectedAcademicYear: " + this.selectedAcademicYear);

        if(this.selectedAcademicYear
            && this.selectedBU){
                this.loading = true;

                getFilteredOpportunitiesCount(
                    {
                        'listViewId': this.selectedView ,
                        'buId': this.selectedBU  ,
                        'aY': this.selectedAcademicYear
                }
                ).then((result)=>{
                    if(result.length > 0){
                        this.totalSelected = result[0]?.Total;
                    }
                    this.loading = false;

                })
                .catch((error)=>{
                    this.loading = false;
                    console.log('error',error)
                })
            this.allFiltersDisabled = false;

        }
        else{
            this.allFiltersDisabled = true;
        }
    }
    
    changeAY(){
        this.confirmMsg = msg_Change_Confirm;
        this.confirmMsg = this.confirmMsg.replace("#", this.totalSelected);//this.viewSelected["viewRecordsSize"]
        this.showConfirmMsg = true;
    }

    confirmReregistration(){
        this.showConfirmMsg = false;
        this.loading = true;
        launchBatch({
            recordSize:this.totalSelected,
            academicYear: this.selectedAcademicYear,
            businessUnitId:   this.selectedBU,
            listViewId: this.selectedView
        }).then(result=>{
            console.log(result);
            if(result =='SYNC'){
                this.showToastSuccess("", msg_Job_Sync);
            } else if(result =='ASYNC')
            {
                this.showToastSuccess("", msg_Job_Async);
            }
            this.loading = false;

        })
        .catch(error=>{
            console.log('error',error);
            this.loading = false;

        })
    }

    cancelReregistration(){
        this.showConfirmMsg = false;
    }

    showToastSuccess(title, msg){
        this.dispatchEvent(
            new ShowToastEvent({
                title: title,
                message: msg,
                variant: "success",
                mode: "sticky"
            })
        );
    }

    showToastError(title, msg){
        this.dispatchEvent(
            new ShowToastEvent({
                title: title,
                message: msg,
                variant: "error",
                mode: "sticky"
            })
        );
    }

    disableScreen(){
        this.isViewSelected = false;
        this.isViewNotSelected = true;
        this.inProgress = true;
        this.selectedView = "";
    }

}