import { LightningElement, api, wire, track } from 'lwc'; 
import fetchRecords from '@salesforce/apex/CORE_LC_RelatedListController.fetchRecords'; 
import { NavigationMixin } from 'lightning/navigation';
import SourceLbl from '@salesforce/label/c.CORE_LWC_RL_Act_Source';
import CompletedDateLbl from '@salesforce/label/c.CORE_LWC_RL_Act_CompletedDate';
import OriginLbl from '@salesforce/label/c.CORE_LWC_RL_Act_Origin';
import CallDurationLbl from '@salesforce/label/c.CORE_LWC_RL_Act_CallDuration';
import CallOutputLbl from '@salesforce/label/c.CORE_LWC_RL_Act_CallOutput';
import ApocLbl from '@salesforce/label/c.CORE_LWC_RL_Act_APOC';
import ChannelLbl from '@salesforce/label/c.CORE_LWC_RL_Act_Channel';
import IsAutomaticLbl from '@salesforce/label/c.CORE_LWC_RL_Act_IsAutomatic';
import InOutLbl from '@salesforce/label/c.CORE_LWC_RL_Act_InOut';
import CreatedDateLbl from '@salesforce/label/c.CORE_LWC_RL_Act_CreatedDate';
import SubjectLbl from '@salesforce/label/c.CORE_LWC_RL_Act_Subject';
import SearchTextLbl from '@salesforce/label/c.CORE_LWC_RL_Act_SearchText';
import TitleLbl from '@salesforce/label/c.CORE_LWC_RL_Act_Title';

const SPECIAL_SORT = {
    "activityUrl" : "Subject",
    "pocUrl": "CORE_TECH_PocName__c"
};

export default class core_RelatedListActivities extends NavigationMixin( LightningElement ) { 
    @api objectName; 
    @api recordId;
    @api tabSizeMax; 
    @api maxNumberOfRecord;
    @api hideCancelledTask;
    @track searchByName='';
    @track listRecords;
    @track titleWithCount;
    @track error; //Standard error handler for errorPanel component
    @track sortBy='CompletedDateTime';
    @track sortDirection='desc';
    
    label = {
        CompletedDateLbl,
        OriginLbl,
        SourceLbl,
        CallDurationLbl,
        CallOutputLbl,
        ApocLbl,
        ChannelLbl,
        IsAutomaticLbl,
        InOutLbl,
        CreatedDateLbl,
        SubjectLbl,
        SearchTextLbl,
        TitleLbl
    }
    
    @track columns = [
        {
            label: this.label.SubjectLbl,
            fieldName: 'activityUrl',
            type: 'url',
            typeAttributes: {
                label: { 
                    fieldName: 'Subject' 
                }, 
                target: '_self'
            },
            sortable: true
        },
        {
            label: this.label.CreatedDateLbl,
            fieldName: 'CreatedDate',
            type: 'date',
            typeAttributes:{
                year: "numeric",
                month: "2-digit",
                day: "2-digit",
                hour: "2-digit",
                minute: "2-digit",
                hour12: false
            },
            sortable: true
        },
        {
            label: this.label.InOutLbl,
            fieldName: 'CORE_TECH_In_Out__c',
            type: 'image',
            sortable: true
        },
        {
            label: this.label.IsAutomaticLbl,
            fieldName: 'CORE_Is_Automatic__c',
            type: 'boolean',
            sortable: true
        },
        {
            label: this.label.ChannelLbl,
            fieldName: 'CORE_TECH_Channel__c',
            type: 'image',
            sortable: true
        },
        {
            label: this.label.ApocLbl,
            fieldName: 'pocUrl',
            type: 'url',
            typeAttributes: {
                label: { 
                    fieldName: 'CORE_TECH_PocName__c' 
                }, 
                target: '_self'
            },
            sortable: true
        },
        {
            label: this.label.CallOutputLbl,
            fieldName: 'CORE_Call_Output__c',
            type: 'text',
            sortable: true
        },
        {
            label: this.label.CallDurationLbl,
            fieldName: 'CallDurationInSeconds',
            type: 'text',
            sortable: true
        },
        {
            label: this.label.OriginLbl,
            fieldName: 'CORE_Origin__c',
            type: 'text',
            sortable: true
        },
        {
            label: this.label.SourceLbl,
            fieldName: 'CORE_Source__c',
            type: 'text',
            sortable: true
        },
        {
            label: this.label.CompletedDateLbl,
            fieldName: 'CompletedDateTime',
            type: 'date',
            typeAttributes:{
                year: "numeric",
                month: "2-digit",
                day: "2-digit",
                hour: "2-digit",
                minute: "2-digit",
                hour12: false
            },
            sortable: true
        }
    ];
    
    doSorting(event) {
        // calling sortdata function to sort the data based on direction and selected field
        let fieldNameTmp = event.detail.fieldName;
        let sortDirectionTmp = event.detail.sortDirection;
        
        if(fieldNameTmp === 'activityUrl' || fieldNameTmp === 'pocUrl'){
            fieldNameTmp = SPECIAL_SORT[fieldNameTmp];
            if(this.sortBy === fieldNameTmp){
                if(this.sortDirection.includes('desc')){
                    sortDirectionTmp = 'asc';
                } else {
                    sortDirectionTmp = 'desc';
                }
            }
        }

        this.sortBy = fieldNameTmp;
        this.sortDirection = sortDirectionTmp;
    }

    get datatableHeight() { 
        if(this.tabSizeMax === null){
            this.tabSizeMax = 250;
        }
        if (this.listRecords.length > 7) {
            return 'height: '+ this.tabSizeMax +'px;';
        } else {
            return '';
        }
    }

    handleKeyChange( event ) {  
        this.searchByName = event.target.value; 
    }
    refreshComponent( event ) {  
        this.searchByName = '';
        this.sortBy='CompletedDateTime';
        this.sortDirection='desc';
    }
    
    @wire(fetchRecords, { recordId:'$recordId', objectName: '$objectName',maxNumberOfRecord: '$maxNumberOfRecord', sortField: '$sortBy',sortOrder : '$sortDirection', search: '$searchByName', hideCancelledTask: '$hideCancelledTask'}) 
    accountData( { error, data } ) {
        debugger;
        if ( data ) {
            this.listRecords = data.listRecords;
            let activityUrl;
            let pocUrl;
            this.listRecords = data.listRecords.map(row => { 
                activityUrl = `/${row.Id}`;
                if(row.CORE_Point_of_contact__c != null){
                    pocUrl = `/${row.CORE_Point_of_contact__c}`;
                } else {
                    pocUrl =null;
                }
                return {...row , activityUrl, pocUrl} 
            })
            if ( data.recordCount ) {
                this.titleWithCount = this.label.TitleLbl + ' (' + data.recordCount + ')';
            } else {
                this.titleWithCount = this.label.TitleLbl + ' (0)';
            }
            this.error=null;
        }else if(error){
            this.error = 'An error occurred : Please contact your administrator';

            if(error.hasOwnProperty('body') && error.body.hasOwnProperty('message')){
                this.error = error.body.message;
            }
            
            this.titleWithCount = this.label.TitleLbl + ' (0)';
            this.listRecords = null;
        }
    }


}