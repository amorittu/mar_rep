import { LightningElement, api } from 'lwc';

export default class It_dynamicCheckboxInput extends LightningElement {
    @api inputLabel;
    @api inputPlaceholder;
    @api isRequired = false;
    @api isDisabled = false;
    @api defaultValue = false;
    
    @api outputValue;

    connectedCallback() {
        try {
            this.outputValue = this.defaultValue;
            console.log('it_dynamicDateInput - defaultValue: ' + this.defaultValue);
        } catch(e) {
            console.log('it_dynamicDateInput - connectedCallback exception : ' + e.message);
        }
    }

    getOutputValue(event) {
        try {
            this.outputValue = event.target.checked;
            console.log(this.inputLabel + ' is : ' + this.outputValue);
        } catch(e) {
            console.log('it_dynamicDateInput - getOutputValue exception ' + e.message);
        }
    }
}