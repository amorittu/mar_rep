import LightningDatatable from 'lightning/datatable';
import coreImageControl from './core_ImageControl.html';

export default class core_customDataTable extends LightningDatatable  {
    static customTypes = {
        image: {
            template: coreImageControl
        }
    };
}