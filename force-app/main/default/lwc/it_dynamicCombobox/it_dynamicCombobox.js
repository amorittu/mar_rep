import  { LightningElement, api, track, wire } from 'lwc';

import getPicklistOptionsFromApex from '@salesforce/apex/IT_Utility.getPicklistOptions';

export default class It_dynamicCombobox extends LightningElement {
    //INPUT ATTRIBUTES
    
    // combobox input name
    @api comboboxName;
    // combobox input label
    @api comboboxLabel;
    //combobox input placeholder   
    @api comboboxPlaceholder;
    // combobox input variants. Default is standard
    @api comboboxVariant = 'standard'; // label-hidden, label-inline, label-stacked, standard, label-inline
    // if present (not empty), it won't fire the apex action to find the picklist values --> split , 
    @api incomingOptions;
    // combobox input read only
    @api isReadOnly = false;
    // combobox input disabled
    @api isDisabled = false;
    // combobox input required
    @api isRequired = false;

    @api dropdownAlignment = 'left' // bottom-left, auto 

    @api objectApiName;
    @api objectFieldApiName;

    @track comboboxOptions = [];
    // RETURN OF THE LWC
    @api selectedValue;

    @track error;

    // method to render the free html combobox
    get isNotROandDisabled() {
        console.log('this.isReadOnly is : ' + this.isReadOnly );
        console.log('this.isDisabled is : ' + this.isDisabled );
        return (!this.isReadOnly && !this.isDisabled);
    }
    _initialized = false;

    connectedCallback() {
        console.log('connectedCallback has been fired');
        if (!this._initialized) {
            this._initialized = true;

            if ( (!this.incomingOptions || this.incomingOptions.length == 0) && this.objectApiName && this.objectFieldApiName) {
                this.retrivePicklistOptionsFromApex();
            } else {
                console.log('this.incomingOptions are ; ' + JSON.stringify(this.incomingOptions) );
                var incomingOptions = this.incomingOptions;

                var splittedList = incomingOptions.split(',');
                var tempList =  [];
                for(let i in splittedList) {
                    if (splittedList[i] != "") {
                        tempList.push(splittedList[i]);
                    }
                }
                console.log('tempList is : ' + console.log(tempList));
                for (let i = 0; i <= tempList.length; i++ ) {
                    console.log('i is : ' + i);
                    console.log('tempList.length : ' + tempList.length);
                    if (i < tempList.length) {
                        let tempObj = {};
                        tempObj.label = tempList[i];
                        tempObj.value = tempList[i];
                        this.comboboxOptions.push(tempObj); //0061j00000Inqu5AAB
                    }
                }
            }
        }
    }

    retrivePicklistOptionsFromApex(event) {
        console.log('retrivePicklistOptionsFromApex has been fired');
        var apiNameFields = [];
        apiNameFields = this.formatListForApexMethod();
        console.log('apiNameFields is : ' + JSON.stringify(apiNameFields) );
        try {
            getPicklistOptionsFromApex({    objectApiName : this.objectApiName, 
                                            fieldApiNames: apiNameFields
                                        })
            .then(result => {
                console.log('## result is : ' + JSON.stringify(result));
                this.comboboxOptions = result;
                for (let i in result) {
                    for (let z in result[i].picklistOptions) {
                        let tempObj = {};
                        
                        if (result[i].picklistOptions[z].isActive) {
                            tempObj.label = result[i].picklistOptions[z].label;
                            tempObj.value = result[i].picklistOptions[z].value;
                            this.comboboxOptions.push(tempObj);
                        }
                        
                    }
                }
            })
            .catch(error => {
                this.error = error;
            });
        } catch(e) {
            console.error(e.stack);
        }
        
    }

    formatListForApexMethod() {
        var apiNameFields  = [];
        if (this.objectFieldApiName) {
            if( this.objectFieldApiName.includes(',') ) {
                apiNameFields = this.objectFieldApiName.split(',');
            } else {
                apiNameFields = this.objectFieldApiName;
            }
        }
        return apiNameFields;
    }
    comboboxChangeHandler(event) {
        this.selectedValue = event.detail.value;
        console.log('this.selectedValue is : ' + this.selectedValue);
    }
}