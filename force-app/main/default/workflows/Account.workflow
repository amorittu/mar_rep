<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>CORE_UpdateProfesionnalSituationDate</fullName>
        <field>CORE_Last_Professional_Situation_Update__pc</field>
        <formula>NOW()</formula>
        <name>CORE_UpdateProfesionnalSituationDate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateEmail</fullName>
        <field>PersonEmail</field>
        <formula>IF(ISBLANK( PersonEmail ) , CORE_Email_2__pc, CORE_Email_3__pc)</formula>
        <name>UpdateEmail</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateMobile</fullName>
        <field>PersonMobilePhone</field>
        <formula>IF(ISBLANK(PersonMobilePhone) ,  CORE_Mobile_2__pc ,  CORE_Mobile_3__pc )</formula>
        <name>UpdateMobile</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>CoreFilledEmailPersonAccount</fullName>
        <actions>
            <name>UpdateEmail</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND ( ISBLANK(PersonEmail),     OR(RecordType.DeveloperName=&apos;Core_Sponsor&apos;, RecordType.DeveloperName=&apos;Core_Student&apos;)  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>CoreFilledPhonePersonAccount</fullName>
        <actions>
            <name>UpdateMobile</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND ( ISBLANK(PersonMobilePhone ), OR(RecordType.DeveloperName=&apos;Core_Sponsor&apos;, RecordType.DeveloperName=&apos;Core_Student&apos;)  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Test2</fullName>
        <actions>
            <name>CORE_UpdateProfesionnalSituationDate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Define the last update date for a professionnal situation</description>
        <formula>NOT(ISBLANK(CORE_Professional_situation__pc))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
