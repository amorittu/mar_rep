<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Business_Unit_name</fullName>
        <field>CORE_OPP_TBusiness_Unit__c</field>
        <formula>CORE_OPP_Commercial_Business_Unit_Name__c</formula>
        <name>Business Unit name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CORe_ToBedeleted</fullName>
        <field>CORE_ToBeDeleted__c</field>
        <literalValue>1</literalValue>
        <name>ToBedeleted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OPP_FU001_OppNameUpdate</fullName>
        <description>Update Opportunity Name with Study Area - Curriculum - Intake</description>
        <field>Name</field>
        <formula>IF(!ISBLANK( CORE_OPP_TBusiness_Unit__c ) &amp;&amp; !ISBLANK( CORE_OPP_TStudy_Area__c ),
	IF(!ISBLANK( CORE_OPP_TCurriculum__c ) &amp;&amp; !ISBLANK( CORE_OPP_TIntake__c ),
		 CORE_OPP_TBusiness_Unit__c + &quot; - &quot; +  CORE_OPP_TCurriculum__c + &quot; - &quot; +  CORE_OPP_TIntake__c 
	,
		 CORE_OPP_TBusiness_Unit__c + &quot; - &quot; +  CORE_OPP_TStudy_Area__c 
	)
,
	&quot;New Interest&quot;
)</formula>
        <name>OPP_FU001_OppNameUpdate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>CORE_Opp_tobeassigned</fullName>
        <actions>
            <name>Business_Unit_name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <description>Calculation of “Business Unit_Text” from Commercial Business Unit</description>
        <formula>!ISNULL(CORE_OPP_Commercial_Business_Unit_Name__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
