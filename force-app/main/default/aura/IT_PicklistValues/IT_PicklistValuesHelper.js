/**
 * Created by mmarinelli on 14/07/2021.
 */

({

    setInfoText: function(component, values) {

        if (values.length == 0) {
        component.set("v.infoText", "Select an option...");
        }
        if (values.length == 1) {
        component.set("v.infoText", values[0]);
        }
        else if (values.length > 1) {
        component.set("v.infoText", values.length + " options selected");
        }
    },

    getSelectedValues: function(component){
        var options = component.get("v.picklistValues");
        var values = [];
        options.forEach(function(element) {
        if (element.selected) {
            values.push(element.value);
        }
        });
        return values;
    },

    getSelectedLabels: function(component){
        var options = component.get("v.picklistValues");
        var labels = [];
        options.forEach(function(element) {
        if (element.selected) {
            labels.push(element.label);
        }
        });
        return labels;
    },

    showHidetooltip : function(component, event, helper){
		// console.log("event.clientY: " + event.clientY);
		var dropdown=component.find('dropdown');
		// console.log("dropdown: " + dropdown);

		if(event.clientY > 350){
			$A.util.addClass(dropdown, 'slds-dropdown--bottom');
			// console.log("ABOVE");
		} else {
			$A.util.removeClass(dropdown, 'slds-dropdown--bottom');
            // console.log("BELOW");
		}
    },

    checkError : function(component, event, helper){
        var required = component.get("v.required");
        var value = component.get("v.value");
        var disabled = component.get("v.disabled");
        var disab = component.get("v.disab");
        var mainDiv = component.find('main-div');

        if(required && !value && !disabled && !disab) {
            $A.util.addClass(mainDiv, 'slds-has-error');
            component.set("v.hasError", true);
            return false;
        } else {
            $A.util.removeClass(mainDiv, 'slds-has-error');
            component.set("v.hasError", false);
            return true;
        }
    },

    getPicklist : function(component, event, helper){
        console.log('###ZOCCHI::'+JSON.stringify(component.get("v.excludedValues")));
        var action = component.get("c.getPickListValuesIntoList");
        var showNone = component.get("v.showNoneOption");
        //alert(component.get("v.fieldName")+" : "+component.get("v.sObjectName"));
        action.setParams({
            objectType: component.get("v.sObjectName"),
            selectedField: component.get("v.fieldName")
        });
        action.setCallback(this, function(response) {
            var list = response.getReturnValue();
            var map = [];

            var noneObj = new Object();
            noneObj.value = '';
            noneObj.label = ' --None-- ';
            noneObj.selected = true;
            if(showNone){
                map.push(noneObj);
            }
            for (let index = 0; index < JSON.parse(list).length; index++) {
                //alert(JSON.stringify(JSON.parse(list)[index].label)+" : "+JSON.parse(list)[index].value);
                var obj = new Object();
                obj.value = JSON.parse(list)[index].value;
                obj.label = JSON.parse(list)[index].label;
                obj.selected = false;
                if(!component.get("v.excludedValues").includes(obj.value)){
                    map.push(obj);
                }
            }
            //alert(JSON.stringify(map));
            component.set("v.picklistValues", map);

            helper.predefaultMultiPicklistCustom(component, event, helper);
        })
        $A.enqueueAction(action);
    },

    predefaultMultiPicklistCustom : function(component, event, helper){
        var values = !component.get("v.value") ? '' : component.get("v.value");
        var pickValues = component.get("v.picklistValues");

        if(component.get("v.multiPicklist") && component.get("v.isMobile")){
            var listValues = values.split(";");
            pickValues.forEach(function(element){
                if(listValues.includes(element.value)){
                    element.selected = true;
                } else {
                    element.selected = false;
                }
            });
            component.set("v.picklistValues",pickValues);

            var labels = helper.getSelectedLabels(component);
            helper.setInfoText(component, labels);
        }
    },


    getDepPickVals : function(component, event, helper) {
        var action = component.get("c.getWrapperToDisplayJSON");
        action.setParams({
            "objectName": component.get("v.sObjectName"),
            "controllingField": component.get("v.ParentAPIName"),
            "dependentField": component.get("v.fieldName")
        });

        action.setCallback(this, function(a) {
            var state=a.getState();
            if(component.isValid() && state === "SUCCESS"){
				var result = a.getReturnValue();
            	// console.log("dep map: scopes callback - " + result);
				component.set("v.controlMap", JSON.parse(result));
                this.onChangeControlling(component, helper, true);
            } else {
				console.log("ERROR retrieving dependencies");
            }
		});
		//console.log("RELATED ACTIVITIES: calling APEX | recId: " + component.get("v.recordId"));
		$A.enqueueAction(action);

    },


    onChangeControlling: function(component, helper, isInit){
        var val1 = component.get("v.parentValue");
        var mappa = component.get("v.controlMap");
        // console.log("CHANGED PARENT: " + val1);
        if(mappa != null){
            var list2 = mappa[val1];
            if(!list2){
                list2 = []
            }
            var list3 = [];
            component.set("v.disab", val1 === '' || list2.length == 0);
            list2.forEach(function(element){
                if(!component.get("v.excludedValues").includes(element.value)){
                    list3.push(element);
                }
            });
            component.set("v.picklistValues", list3);
            if(!(isInit && val1)){
                //console.log("sbianca value");
                component.set("v.value", "");
                component.set("v.multiPicklistValues", []);
            }

            helper.predefaultMultiPicklistCustom(component, event, helper);

        }
    }

})