/**
 * Created by mmarinelli on 14/07/2021.
 */

({
    doInit : function(component, event, helper) {
        console.log('disabled attribute is : ' + component.get('v.disabled') );
        console.log('### ULTIMA PROVA:::'+component.get("v.excludedValuesStr"));
        var excludedStr = component.get("v.excludedValuesStr");
        console.log('2');
        if(excludedStr){
            var excludedList = excludedStr.split(";");
            component.set("v.excludedValues", excludedList);
            console.log('3');
        }
        if(component.get("v.sObjectName") && component.get("v.fieldName")){
            if(component.get("v.ParentAPIName")){
                console.log("IS DEPENDENT");
                component.set("v.disab", true);
                helper.getDepPickVals(component,event, helper);
            } else{
                helper.getPicklist(component,event, helper );
            }
        }

        console.log('###MIRKO:::'+component.get("v.multiPicklist"));
        console.log('###MIRKO:::'+component.get("v.isMobile"));
        if(component.get("v.multiPicklist") && !component.get("v.isMobile")){
            var values = component.get("v.value");
            console.log('###MIRKO:::'+component.get("v.value"));
            if(values){
                var valuesList = values.split(";");
                component.set("v.multiPicklistValues", valuesList);
            } else {
                component.set("v.multiPicklistValues", []);
            }
        }
    },

    onChangeParentValue: function(component, event, helper) {
        helper.onChangeControlling(component, helper, false);
    },

    onChangeMultiPicklistValues: function(component, event, helper) {
        var valuesList =  component.get("v.multiPicklistValues");
        var values= valuesList.join(";");
        component.set("v.value", values);
    },

    validateFields : function(component, event, helper) {
        console.log("validation");

        if(component.get("v.compact") && component.get("v.multiPicklist")){
            return helper.checkError(component, event, helper);
        }
        var picklist = component.find("picklist");
        if (!component.get("v.isMobile")) {
            if(picklist.checkValidity()){
                console.log("Valid");
            }else{
                console.log("Not Valid");
                picklist.reportValidity();
            }
            return picklist.checkValidity();
        }else if(!component.get("v.multiPicklist")) {
            if (!component.find("picklist").checkValidity()) {
                component.find("picklist").showHelpMessageIfInvalid();
            }
            return component.find("picklist").checkValidity();
        } else if (component.get("v.multiPicklist")) {
            return helper.checkError(component, event, helper);
        } else {
            component.find("picklist").set("v.errors", [{message:"fff"}]);
            return component.find("picklist").checkValidity();
        }
    },

    handleClick: function(component, event, helper) {
        if(!component.get("v.disabled")){
            var mainDiv = component.find('main-div');
            $A.util.addClass(mainDiv, 'slds-is-open');
            helper.showHidetooltip(component, event, helper);
        }
    },

    handleSelection: function(component, event, helper) {
        var item = event.currentTarget;
        if (item && item.dataset) {
            var value = item.dataset.value;
            var selected = item.dataset.selected;

            var options = component.get("v.picklistValues");

            options.forEach(function(element) {
                if (element.value == value) {
                    element.selected = selected == "true" ? false : true;
                }
            });
            component.set("v.picklistValues", options);
            var values = helper.getSelectedValues(component);
            var labels = helper.getSelectedLabels(component);

            helper.setInfoText(component, labels);

            var selectedString = values.join(';');
            component.set('v.value', selectedString);
        }
    },

    handleMouseLeave: function(component, event, helper) {
        if(!component.get("v.disabled") && !component.get("v.disab")){
            component.set("v.dropdownOver", false);
            var mainDiv = component.find('main-div');
            $A.util.removeClass(mainDiv, 'slds-is-open');
            helper.checkError(component, event, helper);
        }
    },

    handleMouseEnter: function(component, event, helper) {
        if(!component.get("v.disabled") && !component.get("v.disab")){
            component.set("v.dropdownOver", true);
        }
    },

    handleMouseOutButton: function(component, event, helper) {
        if(!component.get("v.disabled")  && !component.get("v.disab")){
            window.setTimeout(
            $A.getCallback(function() {
                if (component.isValid()) {
                //if dropdown over, user has hovered over the dropdown, so don't close.
                if (component.get("v.dropdownOver")) {
                    return;
                }
                var mainDiv = component.find('main-div');
                $A.util.removeClass(mainDiv, 'slds-is-open');
                helper.checkError(component, event, helper);
                }
            }), 200
            );
        }
    }

})