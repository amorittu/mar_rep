/**
    * Created by Gabriele Vitanza on 02/11/2021.
    */

   ({
       doInit : function(component, event, helper){
           helper.retrieveExtInfoIdsHelper(component, event, helper);
           helper.setDataTable(component, event, helper);
   //        helper.retrieveExtInfoChildFieldsAndSection(component, event, helper);
       }
   });