/**
 * Created by Gabriele Vitanza on 02/11/2021.
 */

({
    retrieveExtInfoIdsHelper : function(component, event, helper){
        var action = component.get('c.getExtInformationsIds');
        var rId = component.get('v.recordId');

        action.setParams({
            'recordId' : rId
        });

        action.setCallback(this, function(response){
            var extInfoReturned = response.getReturnValue();
           component.set('v.extInfo', extInfoReturned);

            //this.retrieveRecordsAcces(extInfoReturned,component, event, helper);

            this.retrieveExtInfoChildFieldsAndSection(component, event, helper);
            this.retrieveExtInfoFieldsAll(component, event, helper);
            this.retrieveRoleType(extInfoReturned,component, event, helper);
            //console.log('ext info list ' + JSON.stringify(response.getReturnValue()));
        })
        $A.enqueueAction(action);
    },

    retrieveExtInfoChildFieldsAndSection : function(component, event, helper){
        //console.log('retrieveExtInfoChildFieldsAndSection enter');
        var action = component.get('c.getFieldsByLayout');
        action.setParams({
            'layoutName' : 'IT_ExtendedInfo__c-Extended Information Layout'
        });
        action.setCallback(this, function(response){
            //console.log('first debug ' + JSON.stringify(response.getReturnValue()));
            var listValue = response.getReturnValue();

            component.set('v.PageLayout',listValue);

        })
        $A.enqueueAction(action);
    },

    retrieveExtInfoFieldsAll : function(component, event, helper){
            //console.log('retrieveExtInfoFieldsAll enter');
            var action = component.get('c.getFieldsByLayout');
            action.setParams({
                'layoutName' : 'IT_ExtendedInfo__c-Ext Info All Lightning Bundle'
            });
            action.setCallback(this, function(response){
                //console.log('first debug ' + JSON.stringify(response.getReturnValue()));
                var listValue = response.getReturnValue();

                component.set('v.PageLayoutAll',listValue);

                //console.log('obj PageLayout lite ' + component.get('v.PageLayoutAll'));
            })
            $A.enqueueAction(action);
        },

    retrieveRoleType: function(extInfoReturned,component, event, helper){
        console.log('retrieveRoleType enter');
        var action = component.get('c.getWrapperMethod');

        action.setCallback(this, function(response){

            var wrapRoleType = response.getReturnValue();


            component.set('v.wrapRoleType',wrapRoleType);

                 for(var i in extInfoReturned){
                     var isDOMUS = false;
                     var isNABA = false;
                     var isMARANGONI = false;

                     if(extInfoReturned[i].IT_Brand__c=='CORE_PKL_NABA'){
                         isNABA = true;
                     } else if(extInfoReturned[i].IT_Brand__c=='CORE_PKL_Istituto_Marangoni'){
                         isMARANGONI = true;
                     } else if(extInfoReturned[i].IT_Brand__c=='CORE_PKL_Domus_Academy'){
                         isDOMUS = true;
                     }
                     if((wrapRoleType.isDomus && isDOMUS) || (wrapRoleType.isNABA && isNABA) || (wrapRoleType.isMarangoni && isMARANGONI)){
                          component.set('v.extInfoId', extInfoReturned[i].Id);
                          component.set('v.currentExtInfo', extInfoReturned[i])
                          component.set('v.showDetail', true);

                         break;
                     }
                }


        })
        $A.enqueueAction(action);


    },


    setDataTable : function(component, event, helper){
        component.set('v.columnsExtInfo', [
            {
                label: 'Brand Name',
                type: 'button',
                typeAttributes: {
                    label: { fieldName: 'Name' },
                    name: 'view_details',
                    title: 'Click to View Details'
                }

            },
            {
                label: 'Last Inbound Contact Date',
                fieldName: 'IT_LastInboundcontactDate__c',
                type: 'date'
            },
        	{ 
              label: 'First Contact Date', 
              fieldName: 'IT_FirstContactDate__c', 
              type: 'date', 
              typeAttributes: {
                day: 'numeric',
                month: 'short',
                year: 'numeric',
                hour: '2-digit',
                minute: '2-digit',
                second: '2-digit',
                hour12: false
              }
            },
            {
                label: 'Study Area',
                fieldName: 'IT_StudyOfAreaName__c',
                type: 'text'
            },
             {
                 label: 'Customer Status Actual',
                 fieldName: 'IT_CustomerStatusActual__c',
                 type: 'text'
             }



        ]);
    }
});