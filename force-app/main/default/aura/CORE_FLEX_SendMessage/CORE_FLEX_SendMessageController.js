({
  init: function (component) {
    var initButton = component.get("c.hide");
    initButton.setCallback(this, function (response) {
      var result = response.getReturnValue();
      //component.set("v.hideCall", result.hideCall);
      component.set("v.hideSms", result.hideSms);
      component.set("v.hideWhatsapp", result.hideWhatsapp);
    });
    $A.enqueueAction(initButton);
    var action = component.get("c.loadPhones");
    action.setParams({ recordId: component.get("v.recordId") });
    action.setCallback(this, function (response) {
      var result = response.getReturnValue();

      component.set("v.customerName", result.Name);
      component.set("v.customerPhones", result.CustomerPhones);
      if (result.CustomerPhones.length > 0) component.set("v.customerPhone", result.CustomerPhones[0].value);

      component.set("v.twilioPhones", result.TwilioPhones);
      if (result.TwilioPhones.length > 0) component.set("v.twilioPhone", result.TwilioPhones[0].value);

      component.find("coreFlexChannel").publish({ recordData: { action: "isActive" } });

      component.loadConversation();
    });
    $A.enqueueAction(action);
  },
  handleChanged: function (component, message, helper) {
    if (message != null && message.getParam("recordData") != null) {
      var data = message.getParam("recordData");
      var action = data.action;
      switch (action) {
        case "activeCTI":
          component.set("v.activeCTI", data.available);
          component.activeButton();
          break;
        case "refreshView":
          component.loadConversation();
          break;
        case "errorInvalidPhone":
          component.set("v.hasError", true);
          var message = $A.get("$Label.c.CORE_FLEX_invalidPhone");
          component.set("v.errorMessage", message);
          window.setTimeout(
            $A.getCallback(function () {
              component.set("v.hasError", false);
            }),
            5000
          );
          break;
      }
    }
  },
  activeButton: function (component, event, helper) {
    var activeCTI = component.get("v.activeCTI");

    var phones = component.get("v.customerPhones");
    var phone = component.get("v.customerPhone");
    var phoneSelected = phones.find(function (p) {
      return p.value == phone;
    });
    var isMobile = phoneSelected && phoneSelected.isMobile;
    if (component.get("v.hideCall") != true) {
      var callButton = component.find("callButton");
      callButton.set("v.disabled", !activeCTI);
    }
    if (component.get("v.hideSms") != true) {
      var smsButton = component.find("smsButton");
      if (isMobile && activeCTI) {
        smsButton.set("v.disabled", false);
      } else {
        smsButton.set("v.disabled", true);
      }
    }
    if (component.get("v.hideWhatsapp") != true) {
      var whatsappButton = component.find("whatsappButton");
      if (isMobile && activeCTI) {
        whatsappButton.set("v.disabled", false);
      } else {
        whatsappButton.set("v.disabled", true);
      }
    }
    $A.get("e.force:refreshView").fire();
  },
  loadConversation: function (component, event, helper) {
    var conversation = component.find("conversation");
    conversation.set("v.messages", []);
    conversation.set("v.offset", 0);

    component.activeButton();
    var displayAll = component.get("v.variantAll");
    var ischecked = displayAll == "brand";

    conversation.findConversation(
      component.get("v.recordId"),
      ischecked ? "All" : component.get("v.customerPhone"),
      component.get("v.customerPhones"),
      ischecked ? "All" : component.get("v.twilioPhone"),
      component.get("v.twilioPhones")
    );
  },
  sendCall: function (component) {
    var accountId;
    var opportunityId;
    if (component.get("v.sObjectName") == "Account") {
      accountId = component.get("v.recordId");
    } else {
      opportunityId = component.get("v.recordId");
    }
    component.find("coreFlexChannel").publish({
      recordData: {
        action: "makeCall",
        phone: component.get("v.customerPhone"),
        twilioPhone: component.get("v.twilioPhone"),
        accountId: accountId,
        opportunityId: opportunityId,
        name: component.get("v.customerName"),
      },
    });
  },
  sendSms: function (component) {
    var smsButton = component.find("smsButton");
    smsButton.set("v.disabled", true);
    var action = component.get("c.sendFlexSms");
    action.setParams({
      recordId: component.get("v.recordId"),
      customerPhone: component.get("v.customerPhone"),
      twilioPhone: component.get("v.twilioPhone"),
    });
    action.setCallback(this, function (response) {
      var result = response.getReturnValue();
      console.log("result", result);
      smsButton.set("v.disabled", false);
      if (result != "ok") {
        var message = $A.get("$Label.c.CORE_FLEX_smsNotSent");
        if (result == "KO") {
          message = $A.get("$Label.c.CORE_FLEX_smsNotSentInvalidPhone");
        }
        component.find("notifLib").showToast({
          variant: "warning",
          title: "Warning",
          message: message,
        });
      }
    });
    $A.enqueueAction(action);
  },
  sendWhatsapp: function (component) {
    var whatsappButton = component.find("whatsappButton");
    whatsappButton.set("v.disabled", true);

    var action = component.get("c.sendFlexWhatsapp");
    action.setParams({
      recordId: component.get("v.recordId"),
      customerPhone: component.get("v.customerPhone"),
      twilioPhone: component.get("v.twilioPhone"),
    });
    action.setCallback(this, function (response) {
      var result = response.getReturnValue();
      console.log("result", result);
      whatsappButton.set("v.disabled", false);
      if (result != "ok") {
        component.find("notifLib").showToast({
          variant: "warning",
          title: "Warning",
          message: $A.get("$Label.c.CORE_FLEX_whatsappNotSent"),
        });
      }
    });
    $A.enqueueAction(action);
  },
  displayAll: function (component) {
    var displayAll = component.get("v.variantAll");
    component.set("v.variantAll", displayAll == "border-filled" ? "brand" : "border-filled");
    const action = component.get("c.loadConversation");
    $A.enqueueAction(action);
  },
});