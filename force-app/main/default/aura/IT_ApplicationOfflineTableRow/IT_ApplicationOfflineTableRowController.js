/**
 * Created by SaverioTrovatoNigith on 15/02/2022.
 */

({
    setCreatedDateController : function (component, event, helper){
       var today = new Date();
       var dd = String(today.getDate()).padStart(2, '0');
       var mm = String(today.getMonth() + 1).padStart(2, '0');
       var yyyy = today.getFullYear();
       today = yyyy + '-' + mm + '-' + dd;

        component.set('v.children.isReceived', event.getSource().get("v.checked") );
       
       component.set('v.children.createdDoc.CORE_Date_Created__c',today);
       
    }
});