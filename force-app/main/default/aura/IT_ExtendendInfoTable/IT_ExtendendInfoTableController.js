/**
 * Created by ftrapani on 03/11/2021.
 */

({
    handleRowAction : function(component, event, helper){
        var row = event.getParam('row');
        var wrapRoleType = component.get('v.wrapRoleType');

        var isDOMUS = false;
        var isNABA = false;
        var isMARANGONI = false;

        if(row.IT_Brand__c=='CORE_PKL_NABA'){
            isNABA = true;
        } else if(row.IT_Brand__c=='CORE_PKL_Istituto_Marangoni'){
            isMARANGONI = true;
        } else if(row.IT_Brand__c=='CORE_PKL_Domus_Academy'){
            isDOMUS = true;
        }
        var foundRecord = false;

        if((isNABA && wrapRoleType.isNABA) || (isDOMUS && wrapRoleType.isDomus) || (isMARANGONI && wrapRoleType.isMarangoni) ){
            foundRecord = true;
        }

        //console.log('foundRecord ' + foundRecord);

        //console.log('row ' + JSON.stringify(row));
        // component.set('v.showDetail', true);
        // component.set('v.showDetailAll', true);

        if(foundRecord){
            component.set('v.showDetail', true);
            component.set('v.showDetailAll', false);
            //console.log('showTable row.Id::: ' + row.Id);
        } else {
            component.set('v.showDetailAll', true);
            component.set('v.showDetail', false);
            //console.log('showTable row.Id::: ' + row.Id);
        }
        component.set('v.currentExtInfo', row);
        component.set('v.extInfoId', row.Id);
        component.set('v.reloadForm', false);
        component.set('v.reloadForm', true);
    },
});