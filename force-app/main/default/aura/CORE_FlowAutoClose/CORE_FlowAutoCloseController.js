({
    doInit: function(component, event, helper) {
        setTimeout(() => {
            var navigate = component.get("v.navigateFlow");
            navigate("FINISH");
        }, 3000);
    }
 })