/**
 * Created by SaverioTrovatoNigith on 15/02/2022.
 */

({

   inputDisabler : function (component, event, helper) {
      let opportunity = component.get('v.opty');

      if (! opportunity.hasOwnProperty('CORE_OPP_Application_completed__c') ) {
         component.set('v.disableApplicationCompleted', false);
         component.set("v.appCompleted", false);
      } else {
         if (opportunity.CORE_OPP_Application_completed__c != null) {
            component.set('v.disableApplicationCompleted', true);
            component.set("v.appCompleted", true);
         } else {
            component.set('v.disableApplicationCompleted', false);
            component.set("v.appCompleted", false);
         }
      }

      if (! opportunity.hasOwnProperty('CORE_Document_progression__c') ) {
         component.set('v.disableDocumentCompleted', false);
         component.set("v.docCompleted", false);
      } else {
         component.set('v.disableDocumentCompleted', true);
         component.set("v.docCompleted", true);
      }
   },

   retrieveOptyHelper : function(component, event, helper){
      var action = component.get('c.getOpty');
      var opp = component.get('v.opty');

      action.setParams({
            'recordId' : opp.Id
      });

      action.setCallback(this, function(response){
         var optyReturned = response.getReturnValue();
         component.set("v.opty", optyReturned);
      })
      $A.enqueueAction(action);
   },

   setApplicationCompletedDateHelper : function(component, event, helper){
      
      let opty = component.get("v.opty");
      // let isAppCompleted = component.get("v.appCompleted");
      let isChecked = event.getSource().get("v.checked")
      if (isChecked) {
         let dateTimeNow = new Date().toISOString();
         console.log('dateTimeNow is : '+ dateTimeNow);
         opty.CORE_OPP_Application_completed__c = dateTimeNow;
      } else {
         opty.CORE_OPP_Application_completed__c = null;
      }
   },

   manageCallExceptions : function(cmp, event, helper, state, response) {
      if (state === "INCOMPLETE") {
         // handle the incomplete state
         console.error("[HLPR] User is offline, device doesn't support drafts.");
         // show the Toast Message
         let toastEvent = $A.get("e.force:showToast");
         toastEvent.setParams({title: 'Incomplete', message: 'Incomplete!', type: 'warning'});
         toastEvent.fire();
      }
      else if (state === "ERROR") {
         let errors = response.getError();
         if (errors) {
            console.error("[HLPR] Error: "+JSON.stringify(errors));
            var errorMessage = '';
            for(let i=0; i<errors.length; i++) {
               if (errors[i] && errors[i].message) {
                  errorMessage += '\n• ' + errors[i].message;
               }
            }
            // show the Toast Message
            let toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({title: 'Error', mode: 'sticky', message: errorMessage, type: 'error'});
            toastEvent.fire();
         }
         else {
            console.error("Unknown error");
         }
      }
   },
    
   setProgressionOnOptHelper : function(component, event, helper, boolVal) { 
      var opty = component.get("v.opty");
      if (boolVal) {
         opty.CORE_Document_progression__c = 'Completed';
      } else {
         opty.CORE_Document_progression__c = '';
      }
      component.set("v.opty", opty);
	},
   
});