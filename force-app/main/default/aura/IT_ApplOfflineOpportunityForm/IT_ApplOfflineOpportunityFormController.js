/**
 * Created by SaverioTrovatoNigith on 15/02/2022.
 */

({

  doInit : function(component, event, helper){
    helper.inputDisabler(component, event, helper);
  },
        
  setApplicationCompletedDateController : function(component, event, helper){
		helper.setApplicationCompletedDateHelper(component, event, helper);
	},
    
    setProgressionOnOpt: function(component, event, helper){
      let boolValue = event.getSource().get("v.checked");
      helper.setProgressionOnOptHelper(component, event, helper, boolValue);
    },
    
});