/**
 * Created by Gabriele Vitanza on 03/02/2022.
 */

({
    doInit : function(component, event, helper){
        helper.getPicklistValues(component, event);
        helper.retrieveOptyHelper(component, event, helper);
        helper.getLabelsHelper(component, event, helper);
        component.set('v.hasLoaded', true);
    },

    handleSave : function(component, event, helper){
        component.set('v.hasLoaded', false);
        helper.saveAll(component, event, helper)
            .then(function(res) {
                if (res == 'ok') {
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                    "type" : "success",
                        "title": 'Success',
                        "message": "Records have been updated."
                    });
                    toastEvent.fire();
                    
                    
                    window.setTimeout(
                        $A.getCallback(function() {
                            // component.set('v.hasLoaded', true);

                            helper.refreshPage(component, event);
                        }), 3500
                    );
                }
               
            });
    }
});