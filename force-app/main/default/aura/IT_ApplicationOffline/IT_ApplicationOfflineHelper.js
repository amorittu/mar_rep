/**
 * Created by Gabriele Vitanza on 03/02/2022.
 */

({
    retrieveOptyHelper : function(component, event, helper){
        var action = component.get('c.getOpty');
        var rId = component.get('v.recordId');

        action.setParams({
            'recordId' : rId
        });

        action.setCallback(this, function(response){
           var optyReturned = response.getReturnValue();
           component.set("v.opty", optyReturned);

           console.log('optyReturned:: ' + JSON.stringify(optyReturned));
           console.log('getOptyHelper - opty cmp:: ' + JSON.stringify(component.get("v.opty")));
           this.retrieveTableData(component, event, helper);
        })
        $A.enqueueAction(action);
    },

    refreshPage : function(component, event) {
        location.reload(true);
    },

    getLabelsHelper : function(component, event, helper){
        var action = component.get('c.getLabels');
        action.setCallback(this, function(response){
           var labelsReturned = response.getReturnValue();
           component.set('v.labels', labelsReturned);

        })
        $A.enqueueAction(action);
    },

    retrieveTableData : function(component, event, helper){
        console.log('retrieveTableData[START]');
        var self = this;
        var action = component.get("c.retrieveDocConfig");
        var rId = component.get('v.recordId');
        var opty = component.get("v.opty");
        action.setParams({
            recordId : rId,
            division : opty.CORE_OPP_Division__r.CORE_Division_ExternalId__c
        });

        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var wrapReturned = response.getReturnValue();
                console.log('wrapReturned:: ' + JSON.stringify(wrapReturned));
                component.set("v.docWrapList", wrapReturned);
            }
            else {
                self.manageCallExceptions(component, event, helper, state, response);
            }
        });
        $A.enqueueAction(action);
    },

    saveAll : function(component, event, helper){
        return new Promise($A.getCallback(function(resolve, reject) {
            
            console.log('saveAll[START]');
            var self = this;
            var action = component.get("c.saveOptyAndDocs");
            var opty = component.get("v.opty");
            var docWrapList = component.get("v.docWrapList")
            action.setParams({
                opty : opty,
                docWrapList : JSON.stringify(docWrapList)
            });

            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") {
                    let savedChildren = component.get("v.docWrapList");
                    let optyUpdated = component.get("v.opty");

                    console.log('saveAll - savedChildren:: ' + savedChildren);
                    console.log('optyUpdated - optyUpdated::' + optyUpdated);
                    resolve('ok');
                    component.set('v.hasLoaded', true);

                }
                else if (state === "ERROR") {
                    var errors = response.getError();
                    console.log("Error message: " + errors[0].pageErrors[0].message);
                    if (errors) {
                        if (errors[0] && errors[0].pageErrors[0].message) {
                            let toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams({title: 'Error', mode: 'sticky', message: errors[0].pageErrors[0].message, type: 'error'});
                            toastEvent.fire();
                            reject(errors[0].pageErrors[0].message);
                            component.set('v.hasLoaded', true);
                        }
                    }
                    else {
                        console.log("Unknown error");
                    }
                }
            });
            $A.enqueueAction(action);
        }));
    },

    getPicklistValues: function(component, event) {
        var action = component.get("c.getEntryAssessmentTestTypeValue");
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var result = response.getReturnValue();
                var fieldMap = [];
                for(var key in result){
                    fieldMap.push({key: key, value: result[key]});
                }
                component.set("v.fieldMap", fieldMap);
            }
        });
        $A.enqueueAction(action);
    },

    //function for manage the call exception
    manageCallExceptions : function(cmp, event, helper, state, response) {
       if (state === "INCOMPLETE") {
          // handle the incomplete state
          console.error("[HLPR] User is offline, device doesn't support drafts.");
          // show the Toast Message
          let toastEvent = $A.get("e.force:showToast");
          toastEvent.setParams({title: 'Incomplete', message: 'Incomplete!', type: 'warning'});
          toastEvent.fire();
       }
       else if (state === "ERROR") {
          let errors = response.getError();
          if (errors) {
             console.error("[HLPR] Error: "+JSON.stringify(errors));
             var errorMessage = '';
             for(let i=0; i<errors.length; i++) {
                if (errors[i] && errors[i].message) {
                   errorMessage += '\n• ' + errors[i].message;
                }
             }
             // show the Toast Message
             let toastEvent = $A.get("e.force:showToast");
             toastEvent.setParams({title: 'Error', mode: 'sticky', message: errorMessage, type: 'error'});
             toastEvent.fire();
          }
          else {
             console.error("Unknown error");
          }
       }
    }
});