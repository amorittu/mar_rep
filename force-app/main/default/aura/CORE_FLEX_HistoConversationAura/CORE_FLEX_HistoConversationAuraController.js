({
  findConversation: function (component, event) {
    var params = event.getParam("arguments");
    var action = component.get("c.find");
    action.setParams({
      recordId: params.id,
      customerPhone: params.customerPhone,
      twilioPhone: params.twilioPhone,
    });
    action.setCallback(this, function (response) {
      component.set("v.recordId", response.getReturnValue());
      component.set("v.customerPhone", params.customerPhone);
      component.set("v.customerPhones", params.customerPhones);
      component.set("v.twilioPhone", params.twilioPhone);
      component.set("v.twilioPhones", params.twilioPhones);
      component.reload();
    });
    $A.enqueueAction(action);

    var messages = component.get("v.messages");
    if (messages == null) messages = [];
    var list = [];
    list.push({
      typeAuthor: "conversation",
      body: $A.get("$Label.c.CORE_FLEX_noConversation"),
    });
    Array.prototype.push.apply(list, messages);
    component.set("v.messages", list);
  },
  loadData: function (component) {
    moment.locale($A.get("$Locale.language"));
    if (!component.get("v.recordId")) return;

    var action = component.get("c.loadContent");
    var messages = component.get("v.messages");
    var twilioPhones = component.get("v.twilioPhones");
    var customerPhones = component.get("v.customerPhones");
    if (messages == null) messages = [];
    var offset = component.get("v.offset");
    if (!offset) offset = 0;
    if (offset == 0) messages = [];

    var MAX = 100;
    var lastLength = 0;

    function callback(response) {
      var ms = response.getReturnValue().reverse();
      var firstCall = offset == 0;
      offset += ms.length;
      lastLength = ms.length;

      var list = [];
      for (var i = 0; i < ms.length; i++) {
        var date = new Date(ms[i].CreatedDate);
        var m;
        try {
          m = JSON.parse(ms[i].Content);
        } catch (er) {
          continue;
        }
        var isInvalid = !twilioPhones.find((t) => t.value == ms[i].TwilioPhone);
        var classTwilio = isInvalid ? "twilioPhone invalid" : "twilioPhone";
        isInvalid = !customerPhones.find((t) => t.value == ms[i].CustomerPhone);
        var classCustomer = isInvalid ? "customerPhone invalid" : "customerPhone";
        var urlFlex = ms[i].UrlFlex;
        if (m.length > 0) {
          list.push({
            typeAuthor: "conversation",
            body:
              moment(date).format("ddd LLL") +
              " <span class='" +
              classCustomer +
              "'>" +
              ms[i].CustomerPhone +
              " </span><span class='" +
              classTwilio +
              "'>" +
              ms[i].TwilioPhone +
              "</span>",
          });
          for (var j = 0; j < m.length; j++) {
            m[j].type = ms[i].Type;
            m[j].callDisposition = ms[i].CallDisposition;
            m[j].className = `${ms[i].Type} ${ms[i].CallDisposition}`;
            m[j].timestamp = moment(m[j].timestamp || date).format("LTS");
            if (m[j].typeAuthor == "record") {
              m[j].agent = ms[i].Agent;
              m[j].duration = moment.duration(ms[i].Duration, "seconds").humanize();
            }
            if ((m[j].typeAuthor == "guest" && m[j].author == "Bot") || !m[j].typeAuthor) {
              m[j].typeAuthor = "agent";
            }
            if (m[j].typeAuthor == "agent" || m[j].typeAuthor == "supervisor") {
              m[j].className += " agent";
            }
            if (m[j].typeAuthor == "agent" && m[j].type == "CORE_PKL_SMS_Marketing" && m[j].callDisposition != "delivered") {
              m[j].author += " - " + $A.get("$Label.c.CORE_FLEX_notDelivered");
            }
            m[j].hasImage = m[j].mediaType == "image/jpeg" || m[j].mediaType == "image/jpeg";
            m[j].hasPdf = m[j].mediaType == "application/pdf";
            m[j].hasVideo = m[j].mediaType == "video/mp4";
            m[j].hasAudio = m[j].mediaType == "audio/mpeg" || m[j].mediaType == "audio/ogg" || m[j].mediaType == "audio/amr";
          }
          Array.prototype.push.apply(list, m);
        }
      }

      if (firstCall && ms.length == 0) {
        list.push({
          typeAuthor: "conversation",
          body: "No conversation found",
        });
      }
      Array.prototype.push.apply(list, messages);
      component.set("v.messages", list);
      component.set("v.offset", offset);

      if (firstCall) {
        window.setTimeout(
          $A.getCallback(function () {
            var scroller = component.find("scroller");
            var dom = scroller.getElement();
            dom.scrollTop = dom.scrollHeight;
            dom.addEventListener("scroll", function () {
              if (dom.scrollTop == 0 && lastLength == MAX) {
                component.set("v.loading", true);
                component.loadData();
                /*var load = component.get("c.load");
                $A.enqueueAction(load);*/
              }
            });
          }),
          10
        );
      }
      component.set("v.loading", false);
    }

    action.setParams({
      convId: component.get("v.recordId"),
      customerPhone: component.get("v.customerPhone"),
      twilioPhone: component.get("v.twilioPhone"),
      offset: offset,
      max: MAX,
    });
    action.setCallback(this, callback);
    component.set("v.loading", true);
    console.log("LOAD ACTION");
    $A.enqueueAction(action);
  },
  openMedia: function (component, event) {
    window.open(event.target.src);
  },
  openMediaId: function (component, event) {
    window.open(event.getSource().get("v.value"));
  },
});