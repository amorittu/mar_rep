trigger CORE_PointOfContactTrigger on CORE_Point_of_Contact__c (after insert) {
    System.debug('CORE_PointOfContactTrigger : START');
    new CORE_PointOfContactTriggerHandler().run();
    System.debug('CORE_PointOfContactTrigger : END');
}