trigger CORE_ServiceAppointmentTrigger on ServiceAppointment (before insert, after insert, after update, after delete) {
    System.debug('CORE_ServiceAppointmentTrigger : START');
    CORE_ServiceAppointmentTriggerHandler.handleTrigger(Trigger.oldMap, Trigger.new, Trigger.operationType);
    System.debug('CORE_ServiceAppointmentTrigger : END');
}