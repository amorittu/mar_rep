trigger CORE_LeadAcquisitionBufferTrigger on CORE_LeadAcquisitionBuffer__c (after insert, after update) {
    if(!System.isBatch() && CORE_TriggerUtils.shouldRunTrigger()){
        CORE_LeadAcquisitionBufferTriggerHandler.executeLFAWorkers(Trigger.new);
    }   
}