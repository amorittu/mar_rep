trigger IT_EmailMessageTrigger on EmailMessage (after insert) {
    IT_EmailMessageTriggerHandler triggerHandler = new IT_EmailMessageTriggerHandler();

    switch on Trigger.operationType {
        when AFTER_INSERT {
            System.debug('IT_EmailMessageTrigger - Bypass after insert is : ' + triggerHandler.isBypassAfterInsert(Trigger.new) );
            if ( triggerHandler.isBypassAfterInsert(Trigger.new)  == null || !triggerHandler.isBypassAfterInsert(Trigger.new) ) {
                System.debug('IT_OpportunityTrigger.afterInsert : START');
                triggerHandler.onAfterInsert(Trigger.new);
                System.debug('IT_OpportunityTrigger.afterInsert : END');
            }
        }
    }
}