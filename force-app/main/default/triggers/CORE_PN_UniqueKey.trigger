trigger CORE_PN_UniqueKey on CORE_Phone_Number__c (before insert) {
 
    System.debug('@CORE_PN_UniqueKey START');
    new CORE_PNTriggerHandler().run();
    System.debug('@CORE_PN_UniqueKey END');
}