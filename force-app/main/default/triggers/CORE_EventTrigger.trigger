trigger CORE_EventTrigger on Event (before insert) {
    System.debug('CORE_EventTrigger : START');
    CORE_EventTriggerHandler.handleTrigger(Trigger.old, Trigger.new, Trigger.operationType);
    System.debug('CORE_EventTrigger : END');
}