/**
 * Created by ftrapani on 19/10/2021.
 * Test class: IT_OpportunityTriggerHandlerTest
 */

trigger IT_OpportunityTrigger on Opportunity (before insert, before update, after insert, after update) {
    
    IT_OpportunityTriggerHandler triggerHandler = new IT_OpportunityTriggerHandler();
    switch on Trigger.operationType {
        when BEFORE_INSERT {
            System.debug('Bypass before insert is : ' + triggerHandler.isBypassBeforeInsert(Trigger.new) );
            if ( !triggerHandler.isBypassBeforeInsert(Trigger.new) ) {
                System.debug('IT_OpportunityTrigger.beforeInsert : START');
                triggerHandler.beforeInsert(Trigger.new );
                System.debug('IT_OpportunityTrigger.beforeInsert : END');
            } 
        }
        when AFTER_INSERT {
            System.debug('Bypass after insert is : ' + triggerHandler.isBypassAfterInsert(Trigger.new) );
            if ( !triggerHandler.isBypassAfterInsert(Trigger.new) ) {
                System.debug('IT_OpportunityTrigger.afterInsert : START');
                triggerHandler.afterInsert(Trigger.new);
                System.debug('IT_OpportunityTrigger.afterInsert : END');
            }
        }
        when BEFORE_UPDATE {
            System.debug('Bypass before update is : ' + triggerHandler.isBypassBeforeUpdate(Trigger.new) );
            if ( !triggerHandler.isBypassBeforeUpdate(Trigger.new) ) {
                System.debug('IT_OpportunityTrigger.beforeUpdate : START');
                triggerHandler.beforeUpdate(Trigger.new,Trigger.oldMap);
                System.debug('IT_OpportunityTrigger.beforeUpdate : END');
            }
        }
        when AFTER_UPDATE {
            System.debug('Bypass after update is : ' + triggerHandler.isBypassAfterUpdate(Trigger.new) );
            if ( !triggerHandler.isBypassAfterUpdate(Trigger.new) ) {
                System.debug('IT_OpportunityTrigger.afterUpdate : START');
                triggerHandler.afterUpdate(Trigger.new,Trigger.oldMap);
                System.debug('IT_OpportunityTrigger.afterUpdate : END');
            }
        }
    }
}