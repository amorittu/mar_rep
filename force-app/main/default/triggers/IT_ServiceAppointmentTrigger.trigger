trigger IT_ServiceAppointmentTrigger on ServiceAppointment (after insert, after update) {
	
    IT_ServiceAppointmentTriggerHandler triggerHandler = new IT_ServiceAppointmentTriggerHandler();
   
    switch on Trigger.operationType {
        when AFTER_INSERT {
            System.debug('Bypass after insert is : ' + triggerHandler.isBypassAfterInsert(Trigger.new) );
            if ( !triggerHandler.isBypassAfterInsert(Trigger.new) ) {
                System.debug('IT_OpportunityTrigger.afterInsert : START');
                triggerHandler.afterInsert(Trigger.new);
                System.debug('IT_OpportunityTrigger.afterInsert : END');
            }
        }
        when AFTER_UPDATE {
            System.debug('Bypass after update is : ' + triggerHandler.isBypassAfterUpdate(Trigger.new) );
            if ( !triggerHandler.isBypassAfterUpdate(Trigger.new) ) {
                System.debug('IT_OpportunityTrigger.afterUpdate : START');
                triggerHandler.afterUpdate(Trigger.new, Trigger.oldMap);
                System.debug('IT_OpportunityTrigger.afterUpdate : END');
            }
        }
    }
    
}