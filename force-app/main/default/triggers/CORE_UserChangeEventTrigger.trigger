trigger CORE_UserChangeEventTrigger on UserChangeEvent (after insert) {
    System.debug('CORE_UserChangeEventTrigger : START');
    new CORE_UserChangeEventTriggerHandler().run();
    System.debug('CORE_UserChangeEventTrigger : END');
}