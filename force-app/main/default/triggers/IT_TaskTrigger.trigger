/**
 * Created by ftrapani on 29/10/2021.
 */

trigger IT_TaskTrigger on Task (before insert, before update, after insert, after update) {
    switch on Trigger.operationType {
        when BEFORE_INSERT {
            System.debug('IT_TaskTrigger.beforeInsert : START');
            IT_TaskTriggerHandler.beforeInsert(Trigger.new);
            System.debug('IT_TaskTrigger.beforeInsert : END');
        }
        when AFTER_INSERT {
            System.debug('IT_TaskTrigger.afterInsert : START');
            IT_TaskTriggerHandler.afterInsert(Trigger.new);
            System.debug('IT_TaskTrigger.afterInsert : END');
        }
        when BEFORE_UPDATE {
            System.debug('IT_TaskTrigger.beforeUpdate : START');
            IT_TaskTriggerHandler.beforeUpdate(Trigger.new,Trigger.oldMap);
            System.debug('IT_TaskTrigger.beforeUpdate : END');
        }
        when AFTER_UPDATE {
            System.debug('IT_TaskTrigger.afterUpdate : START');
            IT_TaskTriggerHandler.afterUpdate(Trigger.new,Trigger.oldMap);
            System.debug('IT_TaskTrigger.afterUpdate : END');
        }
    }
}