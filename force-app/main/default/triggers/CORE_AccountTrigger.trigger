trigger CORE_AccountTrigger on Account (before insert, before update,after delete) {
    System.debug('CORE_AccountTrigger : START');
    CORE_AccountTriggerHandler.handleTrigger(Trigger.old, Trigger.new, Trigger.operationType);
    System.debug('CORE_AccountTrigger : END');
}