/**
 * Created by ftrapani on 11/11/2021.
 */

trigger IT_AccountTrigger on Account (before insert, before update, after insert, after update, after delete) {
    IT_AccountTriggerHandler triggerHandler = new IT_AccountTriggerHandler();
   
    switch on Trigger.operationType {
        when BEFORE_INSERT {
            if ( !triggerHandler.isBypassBeforeInsert(Trigger.new) ) {
                System.debug('IT_AccountTrigger.beforeInsert : START');
                triggerHandler.beforeInsert(Trigger.new);
                System.debug('IT_AccountTrigger.beforeInsert : END');
            }
        }
        when AFTER_INSERT {
            if ( !triggerHandler.isBypassAfterInsert(Trigger.new) ) {
                System.debug('IT_AccountTrigger.afterInsert : START');
                triggerHandler.afterInsert(Trigger.new);
                System.debug('IT_AccountTrigger.afterInsert : END');
            }
           
        }
        when BEFORE_UPDATE {
            if ( !triggerHandler.isBypassBeforeUpdate(Trigger.new) ) {
                System.debug('IT_AccountTrigger.beforeUpdate : START');
                triggerHandler.beforeUpdate(Trigger.new,Trigger.oldMap);
                System.debug('IT_AccountTrigger.beforeUpdate : END');
            }
        }
        when AFTER_UPDATE {
            if ( !triggerHandler.isBypassAfterUpdate(Trigger.new) ) {
                System.debug('IT_AccountTrigger.afterUpdate : START');
                triggerHandler.afterUpdate(Trigger.new,Trigger.oldMap);
                System.debug('IT_AccountTrigger.afterUpdate : END');
            }
        }
        // TK-000485 - 03/08/2022 - Adding before delete in case of merge account
        when AFTER_DELETE {
            if ( !triggerHandler.isBypassAfterDelete(Trigger.old) ) {
                System.debug('IT_AccountTrigger.afterDelete : START');
                triggerHandler.afterDelete(Trigger.old);
                System.debug('IT_AccountTrigger.afterDelete : END');
            }
        }
    }
}