trigger CORE_UserTrigger on User (after insert, after update) {
    System.debug('@CORE_UserTrigger START');
    new CORE_UserTriggerHandler().run();
    System.debug('@CORE_UserTrigger END');
}