trigger IT_EmailPlatformEventTrigger on IT_EmailPlatformEvent__e (after insert) {
    IT_EmailPlatformEventTriggerHandler triggerHandler = new IT_EmailPlatformEventTriggerHandler();
    
    switch on Trigger.operationType {
        when AFTER_INSERT {
            // System.debug('IT_EmailPlatformEventTrigger - Bypass after insert is : ' + triggerHandler.isBypassAfterInsert(Trigger.new) );
            // if ( !triggerHandler.isBypassAfterInsert(Trigger.new) ) {
                System.debug('IT_EmailPlatformEventTrigger.afterInsert : START');
                triggerHandler.afterInsert(Trigger.new);
                System.debug('IT_EmailPlatformEventTrigger.afterInsert : END');
            // }
        }
    }
    
    
}