trigger CORE_EmailMessageTrigger on EmailMessage (after insert, before delete) {
    System.debug('CORE_EmailMessageTrigger : START');
    CORE_EmailMessageTriggerHandler.handleTrigger(Trigger.old, Trigger.new, Trigger.operationType);
    System.debug('CORE_EmailMessageTrigger : END');
}