/*************************************************************************************************************
 * @name			IT_EventTrigger
 * @author			Andrea Morittu <amorittu@atlantic-technologies.com>
 * @created			20 / 05 / 2022
 * @description		Trigger on Event sObject
 *
 * Changes (version)
 * -----------------------------------------------------------------------------------------------------------
 * 				No.		Date			Author					Description
 * 				----	------------	--------------------	----------------------------------------------
 * @version		1.0		2022-05-20		Andrea Morittu			Creation
 *
**************************************************************************************************************/
trigger IT_EventTrigger on Event (after insert) {

    IT_EventTriggerHandler triggerHandler = new IT_EventTriggerHandler();

    switch on Trigger.operationType {
        when AFTER_INSERT {
            if ( !triggerHandler.isBypassBeforeInsert(Trigger.new) ) {
                System.debug('IT_EventTrigger.beforeInsert : START');
                triggerHandler.afterInsert(Trigger.new );
                System.debug('IT_EventTrigger.beforeInsert : END');
            } 
        }
    }

}