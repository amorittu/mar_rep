trigger IT_CampaignMemberTrigger on CampaignMember (before insert, before update, after insert, after update) {
	
    IT_CampaignMemberTriggerHandler triggerHandler = new IT_CampaignMemberTriggerHandler();
    
    switch on Trigger.operationType {
        when AFTER_INSERT {
            System.debug('Bypass after insert is : ' + triggerHandler.isBypassAfterInsert(Trigger.new) );
            if ( !triggerHandler.isBypassAfterInsert(Trigger.new) ) {
                System.debug('IT_CampaignMemberTrigger.afterInsert : START');
                triggerHandler.afterInsert(Trigger.new, Trigger.oldMap);
                System.debug('IT_CampaignMemberTrigger.afterInsert : END');
            }
        }
        when AFTER_UPDATE {
            System.debug('Bypass after update is : ' + triggerHandler.isBypassAfterUpdate(Trigger.new) );
            if ( !triggerHandler.isBypassAfterUpdate(Trigger.new) ) {
                System.debug('IT_CampaignMemberTrigger.afterUpdate : START');
                triggerHandler.afterUpdate(Trigger.new, Trigger.oldMap);
                System.debug('IT_CampaignMemberTrigger.afterUpdate : END');
            }
        }
    }
}