trigger CORE_ConsentTrigger on CORE_Consent__c (before insert, after insert, before update, after update) {
    System.debug('CORE_ConsentTrigger : START');
    CORE_ConsentTriggerHandler.handleTrigger(Trigger.old, Trigger.new, Trigger.operationType);
    System.debug('CORE_ConsentTrigger : END');
}