trigger CORE_OpportunityTrigger on Opportunity (before insert,before update,after insert, after update, after delete) {
    boolean runTrigger = false;
    if(CORE_TriggerUtils.shouldRunTrigger() && Trigger.isUpdate){
        runTrigger = true;
    } else if(!Trigger.isUpdate){
        runTrigger = true;
    }

    if(runTrigger){
        System.debug('CORE_OpportunityTrigger : START');
        CORE_OppotunityTriggerHandler.handleTrigger(Trigger.old, Trigger.new, Trigger.operationType);
        System.debug('CORE_OpportunityTrigger : END');
    }
}