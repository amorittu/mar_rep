trigger CORE_OppTimestampTrigger on Opportunity (after insert, after update) {
    boolean runTrigger = false;
    if(CORE_TriggerUtils.shouldRunTrigger() && Trigger.isUpdate){
        runTrigger = true;
    } else if(!Trigger.isUpdate){
        runTrigger = true;
    }

    if(runTrigger){
        List<Opportunity> oldRecords = Trigger.old;
        List<Opportunity> newRecords = Trigger.new;

        switch on Trigger.operationType {
            when AFTER_INSERT {
                CORE_OpportunityTimestampProcess.insertionProcess(newRecords);
            }
            when AFTER_UPDATE {
                CORE_OpportunityTimestampProcess.updateProcess(oldRecords,newRecords);
            }
        }
    }
}