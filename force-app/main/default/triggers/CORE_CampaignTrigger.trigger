trigger CORE_CampaignTrigger on Campaign (before insert, after insert, before update, after update, after delete) {
    boolean runTrigger = false;
    if(CORE_TriggerUtils.shouldRunTrigger() && Trigger.isUpdate){
        runTrigger = true;
    } else if(!Trigger.isUpdate){
        runTrigger = true;
    }
    
    if(runTrigger){
    	System.debug('CORE_CampaignTrigger : START');
    	CORE_CampaignTriggerHandler.handleTrigger(Trigger.old, Trigger.new, Trigger.operationType);
    	System.debug('CORE_CampaignTrigger : END');
    }
}