trigger CORE_TaskTrigger on Task (before insert, before update, after update, before delete) {
    System.debug('CORE_TaskTrigger : START');
    if(CORE_TriggerUtils.shouldRunTrigger()){
        CORE_TaskTriggerHandler.handleTrigger(Trigger.old, Trigger.new, Trigger.operationType);
    }
    System.debug('CORE_TaskTrigger : START');
}